#!/bin/zsh

curl https://swgoh.gg/api/abilities/ > SWGOH/Reference\ Data/abilities.json
curl https://swgoh.gg/api/gear/ > SWGOH/Reference\ Data/gear.json
curl https://swgoh.gg/api/characters/ > SWGOH/Reference\ Data/characters.json
curl https://swgoh.gg/api/ships/ > SWGOH/Reference\ Data/ships.json

# crinolo data https://github.com/Crinolo/swgoh-stat-calc
curl https://swgoh-stat-calc.glitch.me/gameData.json > SWGOH/Reference\ Data/gameData.json

