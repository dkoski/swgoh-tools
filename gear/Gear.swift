//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation
import SWGOH

func readGear(url: URL) throws -> [String:Gear] {
    let jsonDecoder = JSONDecoder()

    let data = try Data(contentsOf: url)
    let gearList = try jsonDecoder.decode([Gear].self, from: data)
    
    var result = Dictionary(uniqueKeysWithValues: gearList.map { ($0.id, $0) })
    
    for (id, gear) in result {
        var copy = gear
        copy.finalize(result)
        result[id] = copy
    }
    
    return result
}

struct Gear : Codable {

    let name: String
    let id: String

    private let internalIngredients: [_Ingredient]
    
    var ingredients = CountedSet<String>()
        
    mutating internal func finalize(_ allGear:[String:Gear]) {
        if internalIngredients.isEmpty {
            // this is an atomic unit
            ingredients.add(self.name, count: 1)
        } else {
            for ingredient in internalIngredients {
                let otherGear = allGear[ingredient.gear]!
                ingredients.add(otherGear.name, count: ingredient.amount)
            }
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case name
        case id = "base_id"
        case internalIngredients = "ingredients"
    }
    
    struct _Ingredient : Codable {
        let amount: Int
        let gear: String
    }
}
