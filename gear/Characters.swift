//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation
import SWGOH

func readCharacters(url: URL) throws -> [Character] {
    let jsonDecoder = JSONDecoder()

    let data = try Data(contentsOf: url)
    let result = try jsonDecoder.decode([Character].self, from: data)
    return result
}

struct Character : Codable, Comparable {
    
    let name: String
    let id: String
    let gear: [GearLevel]
    
    enum CodingKeys: String, CodingKey {
        case name
        case id = "base_id"
        case gear = "gear_levels"
    }

    struct GearLevel : Codable {
        let tier: Int
        let gearIds: [String]
        
        enum CodingKeys: String, CodingKey {
            case tier
            case gearIds = "gear"
        }
    }
    
    static func < (lhs: Character, rhs: Character) -> Bool {
        return lhs.name.lowercased() < rhs.name.lowercased()
    }
    
    static func == (lhs: Character, rhs: Character) -> Bool {
        return lhs.id == rhs.id
    }

    func requiredGear(tiers: ClosedRange<Int>, gear allGear: [String:Gear]) -> CountedSet<String> {
        var result = CountedSet<String>()
        
        for tier in tiers {
            let gearLevel = gear[tier - 1]
            for gearId in gearLevel.gearIds {
                let gear = allGear[gearId]!
                result.add(gear.ingredients)
            }
        }
        
        return result
    }
}
