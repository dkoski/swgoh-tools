//
//  main.swift
//  gear
//
//  Created by David Koski on 11/10/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation
import SWGOH

var gearURL: URL?
var characterURL: URL?

// curl https://swgoh.gg/api/characters/ > characters.json
// curl https://swgoh.gg/api/gear/ > gear.json

var arguments = CommandLine.arguments.dropFirst()
while !arguments.isEmpty {
    let argument = arguments.removeFirst()
    
    switch argument {
    case "--gear":
        gearURL = URL(fileURLWithPath: arguments.removeFirst())

    case "--characters":
        characterURL = URL(fileURLWithPath: arguments.removeFirst())
        
    default:
        fatalError("unknown argument: \(argument)")
    }
}

guard let gearURL = gearURL, let characterURL = characterURL else {
    fatalError("must give both --gear and --characters")
}

do {
    let characters = try readCharacters(url: characterURL)
    let gear = try readGear(url: gearURL)
    
    var data = [(String, Int, CountedSet<String>)]()
    var allGearNames = Set<String>()
    for character in characters.sorted() {
        for gearLevel in 1 ... 12 {
            let gear = character.requiredGear(tiers: gearLevel ... gearLevel, gear: gear)
            
            allGearNames.formUnion(gear.keys)
            data.append((character.name, gearLevel, gear))
        }
    }
    
    let sortedGearNames = allGearNames.sorted()
    
    var result = ""
    result += "Name,Gear,"
    result += sortedGearNames.joined(separator: ",")
    result += "\n"
    
    for (name, gearLevel, gear) in data {
        result += name
        result += ",\(gearLevel),"
        result += sortedGearNames.map { gear[$0].description }.joined(separator: ",")
        result += "\n"
    }
    
    print(result)

//  print(characters[0].name)
//  print(characters[0].requiredGear(tiers: 1 ... 2, gear: gear))
//  print(characters[0].requiredGear(tiers: 12 ... 13, gear: gear))
    
    
} catch {
    fatalError("unexpected error: \(error)")
}

func printG1to11And12(characters: [Character], gear: [String:Gear]) {
    var data = [(String, CountedSet<String>, CountedSet<String>)]()
    var allGearNames = Set<String>()
    for character in characters.sorted() {
        let g12 = character.requiredGear(tiers: 1 ... 11, gear: gear)
        let g13 = character.requiredGear(tiers: 12 ... 12, gear: gear)
        
        allGearNames.formUnion(g12.keys)
        allGearNames.formUnion(g13.keys)
        
        data.append((character.name, g12, g13))
    }
    
    let sortedGearNames = allGearNames.sorted()
    
    var result = ""
    result += "Name,Gear,"
    result += sortedGearNames.joined(separator: ",")
    result += "\n"
    
    for (name, g12, g13) in data {
        result += name
        result += ",g12,"
        result += sortedGearNames.map { g12[$0].description }.joined(separator: ",")
        result += "\n"

        result += name
        result += ",g13,"
        result += sortedGearNames.map { g13[$0].description }.joined(separator: ",")
        result += "\n"
    }
    
    print(result)
}
