# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 20:49:07 2018

https://github.com/platzman/swgoh.help.python

@author: platzman
"""

import requests
import sys
from json import loads, dumps
import os
import os.path
import json

class SWGOHhelp():
    def __init__(self, settings):
        self.user = "username="+settings.username     
        self.user += "&password="+settings.password
        self.user += "&grant_type=password"
        self.user += "&client_id="+settings.client_id
        self.user += "&client_secret="+settings.client_secret
    	    	
        self.token = str()
        
        self.urlBase = 'https://api.swgoh.help'
        self.signin = '/auth/signin'
        self.data_type = {'guild':'/swgoh/guild/',
                          'player':'/swgoh/player/',
                          'data':'/swgoh/data/',
						  'units':'/swgoh/units',
						  'roster':'/swgoh/roster',
                          'battles':'/swgoh/battles'}

        
    def get_token(self):
        sign_url = self.urlBase+self.signin
        payload = self.user
        head = {"Content-type": "application/x-www-form-urlencoded",
                'Content-Length': str(len(payload))}
        r = requests.request('POST',sign_url, headers=head, data=payload, timeout = 10)
        if r.status_code != 200:
            error = 'Cannot login with these credentials'
            return  {"status_code" : r.status_code,
                     "message": error}
        _tok = loads(r.content.decode('utf-8'))['access_token']
        self.token = { 'Authorization':"Bearer "+_tok} 
        return(self.token)

    def get_data(self, data_type, spec):
        token = self.get_token()
        head = {'Method': 'POST','Content-Type': 'application/json','Authorization': token['Authorization']}
        if data_type == 'data':
            payload = {'collection': str(spec)}
        else:
            payload = {'allycode': spec}
        data_url = self.urlBase+self.data_type[data_type]
        try:
            r = requests.request('POST',data_url, headers=head, data = dumps(payload))
            if r.status_code != 200:
                error = 'Cannot fetch data - error code'
                data = {"status_code" : r.status_code,
                         "message": error}
                         
            # keep a copy of the response -- need to debug some mysterious happenings
            # debug_output = sys.argv[2] + ".response" if len(sys.argv) > 2 else "guild.response"
            # file = open(debug_output, "w")
            # file.write(r.content)
            # file.close()
            
            r.raise_for_status()
            
            data = loads(r.content.decode('utf-8'))
        except:
            data = {"message": 'Cannot fetch data'}
        return data

class settings():
    def __init__(self, _username, _password, _client_id, _client_secret):
        self.username = _username
        self.password = _password
        self.client_id = _client_id
        self.client_secret = _client_secret


# USAGE: python swgoh.py 114351351
# the number is an allycode of somebody in the opposing guild

creds = settings('dkoski','wazzok-7cyrco-zatwYq','123','abc')
client = SWGOHhelp(creds)
allycode = sys.argv[1] if len(sys.argv) > 1 else "369351434"
output = sys.argv[2] + ".json" if len(sys.argv) > 2 else "guild.json"

guild = client.get_data('guild',allycode)


# we don't need this any more
# f = open("opponent-guild.json", "w+")
# f.write(dumps(guild))
# f.close()

# sometimes we get a player with no allycode -- banned or something?
def player_filter(p):
    return p["allyCode"] is not None
    
requested_players = filter(player_filter, guild[0]["roster"])

# load our work
progress = []
work_path = "/tmp/work." + allycode + ".json"
if os.path.exists(work_path):
    with open (work_path, "r") as read_file:
        progress = json.load(read_file)
        print("loaded progress: " + str(len(progress)))

guild_players = map(lambda pl: int(pl["allyCode"]), requested_players)

request_guild_players = guild_players
for work_player in progress:    
    request_guild_players.remove(work_player["allyCode"])

print("requesting remaining: " + str(len(request_guild_players)))

# old style -- this does not get relic data
# key = 'units'
# roster = client.get_data('units',guild_players)

# new style -- much larger and a different structure, but has relic data
key = 'players'
roster = client.get_data('player',guild_players)

for work_player in progress:
    roster.append(work_player)

print("Requested " + str(len(requested_players)) + " received " + str(len(roster)))
if len(requested_players) != len(roster):
    f = open(work_path, "w+")
    f.write(dumps(roster))
    f.close()
    exit(1)

struct = {
    "name" : guild[0]["name"],
    "shortName" : sys.argv[2] if len(sys.argv) > 2 else "opponent",
    key : roster,
}

f = open(output, "w+")
f.write(dumps(struct))
f.close()

if os.path.exists(work_path):
    os.remove(work_path)
