//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
import XCTest

@testable
import SWGOH

struct TestResources {
    
    static var guild = { () -> Model.Guild in
        DataFileManager.isOffline = true
        return try! DataFileManager.guilds.newest!.value()
    }()
 
    static var player = {
        guild.players.first(where: { $0.name == "Gorgatron" })!
    }()
    
    static var unitProvider: UnitProvider { player }
    
    static let jtr: Model.Unit = {
       return player.units["REYJEDITRAINING"]!
    }()
    
    // 7 stars, g8
    static let pao: Model.Unit = {
        return player.units["PAO"]!
    }()

    // 2 star
    static let kitFisto: Model.Unit = {
        return player.units["KITFISTO"]!
    }()

}
