//
//  Copyright © 2020 David Koski. All rights reserved.
//

import XCTest
import SWGOH

class SwgohHelpSerializationTests: XCTestCase {

    static let data: Data = {
        // a download via swgoh.help.py of a single player from swgoh.help -- this has the structure we expect in terms of roster and guild info
        let url = Bundle(for: DataFileManager.self).url(forResource: "Offline/swgoh.json", withExtension: nil)!
        return try! Data(contentsOf: url)
    }()
    
    func testDecode() throws {
        let guild = try SWGOHHelp.load(SwgohHelpSerializationTests.data)
        
        // jango doesn't have any zetas
        let jango = guild.players[0].units["JANGOFETT"]!
        XCTAssertTrue(jango.zetas.isEmpty)
        XCTAssertTrue(jango.zetaIds.isEmpty)

        // c3po has 2
        let c3po = guild.players[0].units["C3POLEGENDARY"]!
        XCTAssertEqual(c3po.zetas.count, 2)
        XCTAssertEqual(c3po.zetaIds.count, 2)

    }
}
