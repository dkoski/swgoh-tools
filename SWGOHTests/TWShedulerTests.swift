//
//  Copyright © 2020 David Koski. All rights reserved.
//

import XCTest

@testable
import SWGOH

class TWShedulerTests: XCTestCase {

    static var evaluations: [Model.Player: [SquadEvaluation]] = {
        DataFileManager.isOffline = true
        return try! DataFileManager.guilds.newest!.squadEvaluations().await()
    }()
    
    func run(_ name: String, squads: Int) -> (String, TWTrace) {
        let evaluations = TWShedulerTests.evaluations
        let config = try! DataFileManager.twConfigs.find(name: name)!.value()
        
        let scheduler = RandomScheduler3()
        RandomScheduler3.resetSeed()

        let trace = TWTrace()
        let schedule = scheduler.schedule(data: evaluations, config: config.compile(), squadsPerSection: squads, trace: trace)
        trace.enabled = true

        let orders = TWPlayerReport.report(schedule: schedule, mentions: [:], squadFormatter: TWPlayerReport.squadPlusGPFormatter)
        
        // useful for rebuilding the benchmark data
        print("ORDER \(name) {\n\(orders)\n}")
        
        return (orders, trace)
    }
    
    func XCTAssertEqualLines(_ s1: String, _ s2: String, file: StaticString = #file, line: UInt = #line) {
        let lines1 = s1.split(separator: "\n")
        let lines2 = s2.split(separator: "\n")
        XCTAssertEqual(lines1.count, lines2.count, "lines should be equal: \(lines1.count) != \(lines2.count)", file: (file), line: line)
        
        for (l1, l2) in zip(lines1, lines2) {
            XCTAssertEqual(l1, l2, file: (file), line: line)
        }
    }
    
    func testBHNoBackfill() {
        // BH are enabled but are not assigned to a section AND do not allow backfill -- should be all "any"
        let (orders, _) = run("BH no backfill", squads: 3)
        
        let expected =
        """
        **Ashe Karrde** score: 124
        B3: Any Squad
        B4: Any Squad
        F1: Fleet: **BEST** defensive fleet
        T2: Any Squad


        **Beats4dayz** score: 60
        B1: Any Squad
        B3: Any Squad


        **Busan Kaz** score: 124
        B3: Any Squad
        B4: Any Squad
        F2: Fleet: **BEST** defensive fleet
        T1: Any Squad


        **Easy E** score: 60
        T3: Any Squad
        T4: Any Squad


        **Gorgatron** score: 60
        T3: Any Squad
        T3: Any Squad


        **Greatfuljed I** score: 124
        B1: Any Squad
        B4: Any Squad
        F1: Fleet: **BEST** defensive fleet
        T2: Any Squad


        **Mc1631** score: 124
        B1: Any Squad
        F1: Fleet: **BEST** defensive fleet
        T1: Any Squad
        T4: Any Squad


        **Raiders** score: 124
        B2: Any Squad
        B2: Any Squad
        F2: Fleet: **BEST** defensive fleet
        T1: Any Squad


        **Yoda4President** score: 124
        B2: Any Squad
        F2: Fleet: **BEST** defensive fleet
        T2: Any Squad
        T4: Any Squad
        """
        
        XCTAssertEqualLines(orders, expected)
    }
    
    func testBHFront() {
        // BH placed in the front two sections, even spread across them
        let (orders, _) = run("BH front", squads: 3)
        
        let expected =
        """
        **Ashe Karrde** score: 124
        B2: Any Squad
        F1: Fleet: **BEST** defensive fleet
        T1: BH - zzJango zBoba zDengar zzBossk zNest 121483
        T4: Any Squad


        **Beats4dayz** score: 90
        B3: Any Squad
        T1: BH - zzBossk zBoba zDengar zGreedo zJango 109244
        T3: Any Squad


        **Busan Kaz** score: 94
        B3: Any Squad
        B3: Any Squad
        F2: Fleet: **BEST** defensive fleet


        **Easy E** score: 90
        B1: BH - zzBossk zBoba zDengar zGreedo zIG-88 113154
        B2: Any Squad
        B4: Any Squad


        **Gorgatron** score: 90
        B1: BH - zzJango zBoba Dengar zzBossk zNest 121483
        B2: Any Squad
        T3: Any Squad


        **Greatfuljed I** score: 124
        B1: BH - zzBossk zBoba Dengar Greedo Jango 112044
        B4: Any Squad
        F2: Fleet: **BEST** defensive fleet
        T2: Any Squad


        **Mc1631** score: 94
        F2: Fleet: **BEST** defensive fleet
        T3: Any Squad
        T4: Any Squad


        **Raiders** score: 124
        F1: Fleet: **BEST** defensive fleet
        T1: BH - zzBossk zBoba Dengar zGreedo zIG-88 109896
        T2: Any Squad
        T4: Any Squad


        **Yoda4President** score: 94
        B4: Any Squad
        F1: Fleet: **BEST** defensive fleet
        T2: Any Squad
        """
        
        XCTAssertEqualLines(orders, expected)

    }
    
    func testBHT1First() {
        // BH place in the front two sections, top section given priority (should have the best units)
        
        let (orders, _) = run("BH T1 first", squads: 3)
        
        let expected =
        """
        **Ashe Karrde** score: 124
        B2: Any Squad
        F2: Fleet: **BEST** defensive fleet
        T1: BH - zzJango zBoba zDengar zzBossk zNest 121483
        T2: Any Squad


        **Beats4dayz** score: 90
        B1: BH - zzBossk zBoba zDengar zGreedo zJango 109244
        T3: Any Squad
        T3: Any Squad


        **Busan Kaz** score: 94
        B2: Any Squad
        B4: Any Squad
        F2: Fleet: **BEST** defensive fleet


        **Easy E** score: 90
        T1: BH - zzBossk zBoba zDengar zGreedo zIG-88 113154
        T4: Any Squad
        T4: Any Squad


        **Gorgatron** score: 90
        B2: Any Squad
        T1: BH - zzJango zBoba Dengar zzBossk zNest 121483
        T4: Any Squad


        **Greatfuljed I** score: 124
        B1: BH - zzBossk zBoba Dengar Greedo Jango 112044
        B4: Any Squad
        F2: Fleet: **BEST** defensive fleet
        T3: Any Squad


        **Mc1631** score: 94
        F1: Fleet: **BEST** defensive fleet
        T2: Any Squad
        T2: Any Squad


        **Raiders** score: 124
        B1: BH - zzBossk zBoba Dengar zGreedo zIG-88 109896
        B3: Any Squad
        B3: Any Squad
        F1: Fleet: **BEST** defensive fleet


        **Yoda4President** score: 94
        B3: Any Squad
        B4: Any Squad
        F1: Fleet: **BEST** defensive fleet
        """
        
        XCTAssertEqualLines(orders, expected)
    }
    
    func testBHBackfill() {
        // BH placed in T1 -- should get the best, then backfill
        
        let (orders, _) = run("BH backfill", squads: 3)
        
        let expected =
        """
        **Ashe Karrde** score: 124
        B4: Any Squad
        B4: Any Squad
        F2: Fleet: **BEST** defensive fleet
        T1: BH - zzJango zBoba zDengar zzBossk zNest 121483


        **Beats4dayz** score: 60
        B1: BH - zzBossk zBoba zDengar zGreedo zJango 109244
        B2: Any Squad


        **Busan Kaz** score: 124
        B4: Any Squad
        F1: Fleet: **BEST** defensive fleet
        T2: Any Squad
        T4: BH - zzBossk Boba Dengar Jango zNest 97422


        **Easy E** score: 60
        B3: Any Squad
        T1: BH - zzBossk zBoba zDengar zGreedo zIG-88 113154


        **Gorgatron** score: 60
        B2: Any Squad
        T1: BH - zzJango zBoba Dengar zzBossk zNest 121483


        **Greatfuljed I** score: 124
        B1: BH - zzBossk zBoba Dengar Greedo Jango 112044
        B2: Any Squad
        F2: Fleet: **BEST** defensive fleet
        T2: Any Squad


        **Mc1631** score: 124
        F1: Fleet: **BEST** defensive fleet
        T2: Any Squad
        T3: Any Squad
        T4: BH - zzBossk Boba Cad Dengar IG-88 105074


        **Raiders** score: 124
        B1: BH - zzBossk zBoba Dengar zGreedo zIG-88 109896
        F1: Fleet: **BEST** defensive fleet
        T3: Any Squad
        T3: Any Squad


        **Yoda4President** score: 124
        B3: Any Squad
        B3: Any Squad
        F2: Fleet: **BEST** defensive fleet
        T4: BH - zzBossk zBoba zGreedo zIG-88 zNest 109028
        """
        
        XCTAssertEqualLines(orders, expected)
    }

    func testDefensive() {
        // one player is set as defensive -- they should be the only one to place
        
        let (orders, _) = run("defensive", squads: 3)
        
        let expected =
        """
        **Ashe Karrde** score: 94
        F2: Fleet: **BEST** defensive fleet
        T2: Any Squad
        T2: Any Squad


        **Beats4dayz** score: 30
        B3: Any Squad


        **Busan Kaz** score: 94
        B4: Any Squad
        F2: Fleet: **BEST** defensive fleet
        T3: Any Squad


        **Easy E** score: 30
        B3: Any Squad


        **Gorgatron** score: 300
        B1: FO - zzKRU zKylo zHux FOE zFOST 127100
        B1: Revan Offense - zzzJKR zGMY BS zJolee zHYoda 118415
        B2: BH - zzJango zBoba Dengar zzBossk zNest 121483
        B2: ShaakClones - zzShaak Ti, zRex zEcho zzFives zARC Trooper 114351
        B2: Geonosians - zzGBA Poggle Geo Soldier, Geo Spy, Sun Fac 100042
        B3: DRMalak - zzzDR zHK-47 zBSF zzMalak zThrawn 112081
        B4: Carth - zCarth Onasi, zMission zZaalbar Juhani zCanderous 88723
        T1: Grievous - zzGG zB2 zB1 zDroideka zMagnaGuard 118096
        T1: CLS - zzzCLS zHan Wedge Biggs zzChewbacca 104561
        T3: NS - zzMT zzAV zDaka Zombie Spirit 110853


        **Greatfuljed I** score: 94
        F2: Fleet: **BEST** defensive fleet
        T4: Any Squad
        T4: Any Squad


        **Mc1631** score: 94
        B4: Any Squad
        F1: Fleet: **BEST** defensive fleet
        T3: Any Squad


        **Raiders** score: 94
        F1: Fleet: **BEST** defensive fleet
        T1: Any Squad
        T4: Any Squad


        **Yoda4President** score: 94
        B1: Any Squad
        F1: Fleet: **BEST** defensive fleet
        T2: Any Squad
        """
        
        XCTAssertEqualLines(orders, expected)
    }

    func testRealistic() {
        // a realistics (but small) set of orders.  MC is on defense
        
        let (orders, _) = run("realistic", squads: 3)
        
        let expected =
        """
        **Ashe Karrde** score: 154
        B2: ShaakClones - zzShaak Ti, zRex zEcho zzFives zARC Trooper 116829
        B4: Geonosians - zzGBA Poggle Geo Soldier, Geo Spy, Sun Fac 122523
        F2: Negotiator - Negotiator HT Anakin's Eta-2 Starfighter, Umbaran Starfighter, Ahsoka Tano's Jedi Starfighter, BTL-B Y-wing Starfighter, Clone Sergeant's ARC-170, Plo Koon's Jedi Starfighter 538215
        T2: Grievous - zzGG zB2 zB1 zDroideka zMagnaGuard 121441
        T3: BH - zzJango zBoba zDengar zzBossk zNest 121483


        **Busan Kaz** score: 64
        F2: Negotiator - Negotiator HT Anakin's Eta-2 Starfighter, Umbaran Starfighter, Ahsoka Tano's Jedi Starfighter, Clone Sergeant's ARC-170, Plo Koon's Jedi Starfighter, Rex's ARC-170 501168
        T2: Grievous - zzGG zB2 zB1 zDroideka MagnaGuard 123553


        **Easy E** score: 30
        T1: Any Squad


        **Gorgatron** score: 154
        B2: ShaakClones - zzShaak Ti, zRex zEcho zzFives zARC Trooper 114351
        B3: NS - zzMT zzAV zDaka Zombie Spirit 110853
        F1: HansMF - Home One, Han's MF, Biggs X-wing, Bistan's U-wing, Phantom Ghost Cassian's U-wing, Wedge X-wing 391261
        T3: BH - zzJango zBoba Dengar zzBossk zNest 121483
        T4: FO - zzKRU zKylo zHux FOE zFOST 127100


        **Greatfuljed I** score: 94
        B4: Geonosians - zzGBA Poggle Geo Soldier, Geo Spy, Sun Fac 115536
        F2: Negotiator - Negotiator HT Anakin's Eta-2 Starfighter, Umbaran Starfighter, Ahsoka Tano's Jedi Starfighter, BTL-B Y-wing Starfighter, Clone Sergeant's ARC-170, Plo Koon's Jedi Starfighter 504334
        T1: Any Squad


        **Mc1631** score: 274
        B1: DRMalak - zzzDR zzHK-47 zBSF zzMalak Sith Empire Trooper 157266
        B1: CLS - zzzCLS zHan Leia zzOld Ben, zzChewbacca 122801
        B1: Carth - zCarth Onasi, Mission Zaalbar Juhani Canderous 90546
        B2: Revan Offense - zzzJKR zGMY BS zJolee zzHYoda 115071
        B4: Geonosians - zzGBA Poggle Geo Soldier, Geo Spy, Sun Fac 140257
        F1: HansMF - Home One, Han's MF, HT Biggs X-wing, Phantom Ghost Cassian's U-wing, Bistan's U-wing 433537
        T2: Grievous - zzGG zB2 zB1 zDroideka zMagnaGuard 148951
        T3: BH - zzBossk Boba Cad Dengar IG-88 105074
        T4: FO - zzKRU Kylo zHux FOO FOE 95919


        **Raiders** score: 94
        B3: NS - zzMT zzAV zDaka Zombie Spirit 117804
        F1: HansMF - Home One, Han's MF, HT Biggs X-wing, Phantom Ghost Tie Adv, Wedge X-wing 422528
        T4: FO - zzKRU zKylo zHux FOO FOE 121661


        **Yoda4President** score: 60
        B3: NS - zzMT zzAV zDaka Zombie Spirit 114413
        T1: Revan - zzzJKR zBS zzGMY zJolee zGK 136800
        """
        
        XCTAssertEqualLines(orders, expected)
    }

}
