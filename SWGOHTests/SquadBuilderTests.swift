//
//  Copyright © 2020 David Koski. All rights reserved.
//

import XCTest

@testable
import SWGOH

class SquadBuilderTests: XCTestCase {

    func testPadme() {
        let player = TestResources.player
        
        let evaluationFilter = PreferredViabilitySortFilter().compose(ViableSquadFilter()).compose(SquadListFilter())
        let squadEvaluator = SquadEvaluator(rules: [:])
        let squadBuilder = MultipleSquadBuilder([TW.padmeBuilder1, TW.padmeBuilder2])
        
        let squads = squadBuilder.generateSquads(units: player)
        
        let evaluations = squadEvaluator.evaluate(squads: squads)
        let finalEvaluations = evaluationFilter.filter(evaluations)

        print(finalEvaluations)
        
    }
    
}
