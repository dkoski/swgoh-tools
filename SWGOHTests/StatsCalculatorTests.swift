//
//  Copyright © 2020 David Koski. All rights reserved.
//

import XCTest

@testable
import SWGOH

class StatsCalculatorTests: XCTestCase {
    
    func compareStats(actual: UnitStats, expected: UnitStats) {
        XCTAssertEqual(actual.health, expected.health)
        XCTAssertEqual(actual.protection, expected.protection)
        XCTAssertEqual(actual.speed, expected.speed)

        XCTAssertEqual(actual.criticalDamage, expected.criticalDamage)
        XCTAssertEqual(actual.potency, expected.potency, accuracy: 0.01)
        XCTAssertEqual(actual.tenacity, expected.tenacity, accuracy: 0.01)
        XCTAssertEqual(actual.healthSteal, expected.healthSteal)

        XCTAssertEqual(actual.physicalDamage, expected.physicalDamage)
        XCTAssertEqual(actual.physicalCritChance, expected.physicalCritChance, accuracy: 0.01)
        XCTAssertEqual(actual.armorPenetration, expected.armorPenetration)
        XCTAssertEqual(actual.physicalAccuracy, expected.physicalAccuracy, accuracy: 0.01)

        XCTAssertEqual(actual.armor, expected.armor, accuracy: 0.01)
        XCTAssertEqual(actual.dodge, expected.dodge, accuracy: 0.01)
        XCTAssertEqual(actual.physicalCritAvoidance, expected.physicalCritAvoidance, accuracy: 0.01)

        XCTAssertEqual(actual.specialDamage, expected.specialDamage)
        XCTAssertEqual(actual.specialCritChance, expected.specialCritChance, accuracy: 0.01)
        XCTAssertEqual(actual.resistancePenetration, expected.resistancePenetration)
        XCTAssertEqual(actual.specialAccuracy, expected.specialAccuracy, accuracy: 0.01)

        XCTAssertEqual(actual.resistance, expected.resistance, accuracy: 0.01)
        XCTAssertEqual(actual.deflectionChance, expected.deflectionChance, accuracy: 0.01)
        XCTAssertEqual(actual.specialCritAvoidance, expected.specialCritAvoidance, accuracy: 0.01)
    }

    func testJKAStats() {
        // ANAKINKNIGHT - R7
        let player = TestResources.player
        let unit = player.units["ANAKINKNIGHT"]!
        let stats = calculateStats(unit: unit)
        
        // note: swgoh.gg says 6762 for special damage, app says 6764 as does the stat calculator
        let expected = SWGOH.UnitStats(health: 69010, protection: 41441, speed: 253, criticalDamage: 222, potency: 1.08, tenacity: 0.48, healthSteal: 0.1, physicalDamage: 7498, physicalCritChance: 0.83, armorPenetration: 290, physicalAccuracy: 0.135, armor: 0.54, dodge: 0.02, physicalCritAvoidance: 0, specialDamage: 6764, specialCritChance: 0.25, resistancePenetration: 105, specialAccuracy: 0.135, resistance: 0.305, deflectionChance: 0.02, specialCritAvoidance: 0)
        
        compareStats(actual: stats, expected: expected)
    }

    func testCLSStats() {
        // COMMANDERLUKESKYWALKER - G12 + gear
        let player = TestResources.player
        let unit = player.units["COMMANDERLUKESKYWALKER"]!
        let stats = calculateStats(unit: unit)
        
        // note: swgoh.gg says 6762 for special damage, app says 6764 as does the stat calculator
        let expected = SWGOH.UnitStats(health: 26589, protection: 38153, speed: 248, criticalDamage: 192, potency: 0.41, tenacity: 0.60, healthSteal: 0.1, physicalDamage: 3527, physicalCritChance: 0.619, armorPenetration: 151, physicalAccuracy: 0, armor: 0.316, dodge: 0.02, physicalCritAvoidance: 0, specialDamage: 2141, specialCritChance: 0.258, resistancePenetration: 0, specialAccuracy: 0, resistance: 0.20, deflectionChance: 0.02, specialCritAvoidance: 0)
        
        compareStats(actual: stats, expected: expected)
    }

    func testCloneSergeantStats() {
        // CLONESERGEANTPHASEI - G12 no gear
        let player = TestResources.player
        let unit = player.units["CLONESERGEANTPHASEI"]!
        let stats = calculateStats(unit: unit)
        
        // note: swgoh.gg says 6762 for special damage, app says 6764 as does the stat calculator
        let expected = SWGOH.UnitStats(health: 20136, protection: 35373, speed: 186, criticalDamage: 216, potency: 0.47, tenacity: 0.25, healthSteal: 0.1, physicalDamage: 2802, physicalCritChance: 0.60, armorPenetration: 33, physicalAccuracy: 0, armor: 0.30, dodge: 0.02, physicalCritAvoidance: 0, specialDamage: 1873, specialCritChance: 0.32, resistancePenetration: 5, specialAccuracy: 0, resistance: 0.20, deflectionChance: 0.02, specialCritAvoidance: 0)
        
        compareStats(actual: stats, expected: expected)
    }
    
    func testJKAShipStats() {
        // JEDISTARFIGHTERANAKIN - ship with R7 pilot
        let player = TestResources.player
        let unit = player.units["JEDISTARFIGHTERANAKIN"]!
        let stats = calculateStats(unit: unit, crew: player.crew(unit: unit))

        let expected = SWGOH.UnitStats(health: 101636, protection: 29030, speed: 189, criticalDamage: 150, potency: 0.50, tenacity: 0.65, healthSteal: 0, physicalDamage: 12747, physicalCritChance: 0.39, armorPenetration: 0, physicalAccuracy: 0, armor: 0.23, dodge: 0.02, physicalCritAvoidance: 0, specialDamage: 1939, specialCritChance: 0.10, resistancePenetration: 0, specialAccuracy: 0, resistance: 0.10, deflectionChance: 0.02, specialCritAvoidance: 0)
        
        compareStats(actual: stats, expected: expected)
    }
    
    func testYWingStats() {
        // YWINGCLONEWARS - pilotless ship
        let player = TestResources.player
        let unit = player.units["YWINGCLONEWARS"]!
        let stats = calculateStats(unit: unit, crew: player.crew(unit: unit))
        
        // computes 70753 protection, actual is 70755
        // computes 88211 health, actual is 88213
        let expected = SWGOH.UnitStats(health: 88211, protection: 70753, speed: 174, criticalDamage: 150, potency: 0.38, tenacity: 0.53, healthSteal: 0, physicalDamage: 6739, physicalCritChance: 0.23, armorPenetration: 0, physicalAccuracy: 0, armor: 0.43, dodge: 0.02, physicalCritAvoidance: 0, specialDamage: 1939, specialCritChance: 0.10, resistancePenetration: 0, specialAccuracy: 0, resistance: 0.34, deflectionChance: 0.02, specialCritAvoidance: 0)

        compareStats(actual: stats, expected: expected)
    }

    func testPhantomShipStats() {
        // PHANTOM2 - multiple pilots
        let player = TestResources.player
        let unit = player.units["PHANTOM2"]!
        let stats = calculateStats(unit: unit, crew: player.crew(unit: unit))

        // computes 67833 health, actual is 67834
        let expected = SWGOH.UnitStats(health: 67833, protection: 17763, speed: 157, criticalDamage: 150, potency: 0.31, tenacity: 0.46, healthSteal: 0, physicalDamage: 8682, physicalCritChance: 0.39, armorPenetration: 0, physicalAccuracy: 0, armor: 0.23, dodge: 0.02, physicalCritAvoidance: 0, specialDamage: 1939, specialCritChance: 0.10, resistancePenetration: 0, specialAccuracy: 0, resistance: 0.10, deflectionChance: 0.02, specialCritAvoidance: 0)

        compareStats(actual: stats, expected: expected)
    }

    func testXWingShipStats() {
        // XWINGRED3 - normal pilot
        let player = TestResources.player
        let unit = player.units["XWINGRED3"]!
        let stats = calculateStats(unit: unit, crew: player.crew(unit: unit))

        let expected = SWGOH.UnitStats(health: 83274, protection: 63834, speed: 173, criticalDamage: 150, potency: 0.41, tenacity: 0.57, healthSteal: 0, physicalDamage: 8465, physicalCritChance: 0.32, armorPenetration: 0, physicalAccuracy: 0, armor: 0.34, dodge: 0.02, physicalCritAvoidance: 0, specialDamage: 1939, specialCritChance: 0.10, resistancePenetration: 0, specialAccuracy: 0, resistance: 0.26, deflectionChance: 0.02, specialCritAvoidance: 0)

        compareStats(actual: stats, expected: expected)
    }


    func testJKAGP() {
        // ANAKINKNIGHT - R7
        let player = TestResources.player
        let unit = player.units["ANAKINKNIGHT"]!
        let gp = calculateGP(unit: unit)
        
        XCTAssertEqual(gp, 29347)
    }

    func testCLSGP() {
        // COMMANDERLUKESKYWALKER - G12 + gear
        let player = TestResources.player
        let unit = player.units["COMMANDERLUKESKYWALKER"]!
        let gp = calculateGP(unit: unit)
        
        XCTAssertEqual(gp, 26484)
    }

    func testCloneSergeantGP() {
        // CLONESERGEANTPHASEI - G12 no gear
        let player = TestResources.player
        let unit = player.units["CLONESERGEANTPHASEI"]!
        let gp = calculateGP(unit: unit)
        
        XCTAssertEqual(gp, 16821)
    }

    func testJKAShipGP() {
        // JEDISTARFIGHTERANAKIN - ship with R7 pilot
        let player = TestResources.player
        let unit = player.units["JEDISTARFIGHTERANAKIN"]!
        let gp = calculateGP(unit: unit, crew: player.crew(unit: unit))
        
        // note: swgoh.gg reports 65082 (but it looks stale?), game reports 73340
        XCTAssertEqual(gp, 73339)
    }
    
    func testYWingGP() {
        // YWINGCLONEWARS - pilotless ship
        let player = TestResources.player
        let unit = player.units["YWINGCLONEWARS"]!
        let gp = calculateGP(unit: unit, crew: player.crew(unit: unit))

        // note: game reports 45142 as does swgoh.gg
        XCTAssertEqual(gp, 45141)
    }

    func testPhantomShipGP() {
        // PHANTOM2 - multiple pilots
        let player = TestResources.player
        let unit = player.units["PHANTOM2"]!
        let gp = calculateGP(unit: unit, crew: player.crew(unit: unit))

        // note: swgoh.gg reports 44193, game reports 44212
        XCTAssertEqual(gp, 44212)
    }

    func testXWingShipGP() {
        // XWINGRED3 - normal pilot
        let player = TestResources.player
        let unit = player.units["XWINGRED3"]!
        let gp = calculateGP(unit: unit, crew: player.crew(unit: unit))

        // note: swgoh.gg reports 46306, game reports 46360
        XCTAssertEqual(gp, 46360)
    }

}
