//
//  Copyright © 2020 David Koski. All rights reserved.
//

import XCTest

@testable
import SWGOH

class ModRecommendationsTest: XCTestCase {
    
    func testMods() {
        let guild = TestResources.guild
        let id = "COMMANDERLUKESKYWALKER"
        let units = guild.players.compactMap { $0.units[id] }.filter { $0.gear > 10 && $0.mods.count == 6 }
        print("count: \(units.count)")
        
        let mods = units.map { $0.mods }
        let modHistograms = units.map { ModHistogram(mods: $0.mods) }
        let cs = 30
        let centers = kMeans(seeds: [], samples: modHistograms, clusterSize: cs)
        
        for c in centers.sorted(by: { $0.count > $1.count }) {
            print("size: \(c.count)\n\(c.average))")
            for mh in modHistograms {
                print("\(mh.distance(from: c.average)) \(mh.distance(from: c.average))")
            }
            for (m, mh) in zip(mods, modHistograms) where mh.distance(from: c.average) <= cs {
                print(mh)
                print(m)
            }
            print("")
        }
    }
    
    func testMods2() {
        let guild = TestResources.guild
        let id = "DAKA"
        let units = guild.players.compactMap { $0.units[id] }.filter { $0.gear > 10 && $0.mods.count == 6 }
        print("count: \(units.count)")
        
        let modHistograms = units.map { ModHistogram(mods: $0.mods).normalized }
        let cs = 90
        let centers = kMeans(seeds: [], samples: modHistograms, clusterSize: cs)
        
        for c in centers.sorted(by: { $0.count > $1.count }) {
            print("size: \(c.count)\n\(c.average.normalized.priorities()))")
            for mh in modHistograms where mh.distance(from: c.average) <= cs * 2 {
                print("distance: \(mh.distance(from: c.average)) -- \(mh.normalized.priorities())")
            }
            print("")
        }
    }

    
    func testDescribe() {
        let player = TestResources.player
        let id = Model.Character.find("Emperor Palpatine").id
        let unit = player.units[id]!
        let mh = ModHistogram(mods: unit.mods)
        
        print(unit.mods)
        
        print(mh)
        print(mh.priorities(threshold: 10))
        print(mh.normalized.priorities())
    }

}
