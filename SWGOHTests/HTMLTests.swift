//
//  HTMLTests.swift
//  Test
//
//  Created by David Koski on 9/6/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import XCTest
import SWGOH

class HTMLTests: XCTestCase {

    func testHTML() {
        
        let guilds = [ TestResources.guild ]
        
        let x =
            H.table().id("data") {
                H.th {
                    H.tr {
                        H.th { "Guild" }
                        H.th { "Members" }
                        H.th { "Units GP" }
                        H.th { "Ships GP" }
                        H.th { "Total GP" }
                    }
                }
                H.foreach(guilds) { guild in
                    H.tr {
                        H.td().order(guild.name) {
                            H.a(href: "") { guild.name }
                        }
                        H.td().order(guild.players.count) {
                            H.a(href: "../player/\(guild.shortName).html") { guild.players.count }
                        }
                        H.td { guild.players.reduce(0) { $0 + $1.stats.characterGP }.millions }
                        H.td { guild.players.reduce(0) { $0 + $1.stats.shipGP }.millions }
                        H.td { guild.info.gp.millions }
                    }
                }
                H.tfoot {
                    H.tr {
                        H.td { guilds.count }
                        H.td { guilds.reduce(0) { $0 + $1.players.count } }
                        H.td { guilds.reduce(0) { $0 + $1.players.reduce(0) { $0 + $1.stats.characterGP } }.millions }
                        H.td { guilds.reduce(0) { $0 + $1.players.reduce(0) { $0 + $1.stats.shipGP } }.millions }
                        H.td { guilds.reduce(0) { $0 + $1.players.reduce(0) { $0 + $1.stats.gp } }.millions }
                    }
                }
            }
        
        print(x)

    }

}
