//
//  Copyright © 2020 David Koski. All rights reserved.
//

import XCTest

@testable
import SWGOH

class ModelTests: XCTestCase {

    func testCharacterMatchesCategory() {
        // exact match on category
        let list = Model.Character.matchesCategory("Tusken")
        XCTAssertEqual(list.count, 3)
    }

    func testCharacterMatches() {
        // the word is required to be lower case
        let list1 = Model.Character.all.filter { $0.matches(["Tusken"]) }
        XCTAssertEqual(list1.count, 0)

        let list2 = Model.Character.all.filter { $0.matches(["tusken"]) }
        XCTAssertEqual(list2.count, 3)
    }
    
    func testCharacterImageURL() {
        let c = Model.Character.find("CLS")
        XCTAssertEqual(c.imageURL, URL(string: "https://swgoh.gg/game-asset/u/COMMANDERLUKESKYWALKER/"))
    }

}
