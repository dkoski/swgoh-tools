//
//  LabelRow.swift
//  BRGTool
//
//  Created by David Koski on 3/17/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI

struct LabelRow <Content: View>: View {
    
    let label: String
    let width: Int
    
    let content: Content
    
    init(label: String, width: Int = 100, @ViewBuilder content: () -> Content) {
        self.label = label
        self.width = width
        self.content = content()
    }

    var body: some View {
        HStack(alignment: .top) {
            Group {
                Text(label)
            }.frame(width: CGFloat(width), alignment: .topLeading)
            content
        }
    }
}
