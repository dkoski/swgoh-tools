//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Cocoa

struct TableView<T: AnyObject & Equatable> : NSViewRepresentable {

    struct Configuration : Equatable {
        let columns: [Column]
        let colors: ((T) -> (NSColor?, NSColor?))?
        let collator: ((T, T) -> Bool)?
        
        static func == (lhs: TableView<T>.Configuration, rhs: TableView<T>.Configuration) -> Bool {
            lhs.columns == rhs.columns
        }
    }
    
    struct Column : Equatable {
        let title: String
        let tooltip: String?
        let width: Int
        
        let format: (T) -> String
        
        var compare: (T, T) -> Bool
        
        init(title: String, tooltip: String? = nil, width: Int, compare: ((T, T) -> Bool)? = nil, format: @escaping (T) -> String) {
            self.title = title
            self.tooltip = tooltip
            self.width = width
            self.format = format
            self.compare = compare ?? { lhs, rhs in
                format(lhs).lowercased() < format(rhs).lowercased()
            }
        }
        
        init(title: String, tooltip: String? = nil, width: Int, path: KeyPath<T, String>) {
            self.title = title
            self.tooltip = tooltip
            self.width = width
            self.format = { obj in obj[keyPath: path].description }
            self.compare = { lhs, rhs in
                lhs[keyPath: path].lowercased() < rhs[keyPath: path].lowercased()
            }
        }
        
        init<C>(title: String, tooltip: String? = nil, width: Int, path: KeyPath<T, C>) where C : CustomStringConvertible & Comparable {
            self.title = title
            self.tooltip = tooltip
            self.width = width
            self.format = { obj in obj[keyPath: path].description }
            self.compare = { lhs, rhs in
                lhs[keyPath: path] < rhs[keyPath: path]
            }
        }

        init(title: String, tooltip: String? = nil, width: Int, path: KeyPath<T, Date?>, none: String = "", dateFormatter: DateFormatter) {
            self.title = title
            self.tooltip = tooltip
            self.width = width
            
            self.format = { obj in
                if let date = obj[keyPath: path] {
                    return dateFormatter.string(from: date)
                } else {
                    return none
                }
            }
            self.compare = { lhs, rhs in
                if let d1 = lhs[keyPath: path], let d2 = rhs[keyPath: path] {
                    return d1 < d2
                } else {
                    return false
                }
            }
        }

        init(title: String, tooltip: String? = nil, width: Int, path: KeyPath<T, Double>, none: String = "", doubleFormatter: NumberFormatter) {
            self.title = title
            self.tooltip = tooltip
            self.width = width
            
            self.format = { obj in
                let value = obj[keyPath: path]
                return doubleFormatter.string(from: value, missing: none)
            }
            self.compare = { lhs, rhs in
                let d1 = lhs[keyPath: path]
                let d2 = rhs[keyPath: path]
                return d1 < d2
            }
        }
        
        init(title: String, tooltip: String? = nil, width: Int, path: KeyPath<T, Int>, none: String = "", intFormatter: NumberFormatter) {
            self.title = title
            self.tooltip = tooltip
            self.width = width
            
            self.format = { obj in
                let value = obj[keyPath: path]
                return intFormatter.string(from: value, missing: none)
            }
            self.compare = { lhs, rhs in
                let d1 = lhs[keyPath: path]
                let d2 = rhs[keyPath: path]
                return d1 < d2
            }
        }


        init<C>(title: String, tooltip: String? = nil, width: Int, path: KeyPath<T, C>) where C : CustomStringConvertible {
            self.title = title
            self.tooltip = tooltip
            self.width = width
            self.format = { obj in obj[keyPath: path].description }
            self.compare = { lhs, rhs in
                lhs[keyPath: path].description < rhs[keyPath: path].description
            }
        }
        
        init<C>(title: String, tooltip: String? = nil, width: Int, path: KeyPath<T, C?>, none: String = "None") where C : CustomStringConvertible {
            self.title = title
            self.tooltip = tooltip
            self.width = width
            self.format = { obj in obj[keyPath: path]?.description ?? none }
            self.compare = { lhs, rhs in
                (lhs[keyPath: path]?.description ?? none) < (rhs[keyPath: path]?.description ?? none)
            }
        }

        static func == (lhs: TableView<T>.Column, rhs: TableView<T>.Column) -> Bool {
            lhs.title == rhs.title && lhs.width == rhs.width
        }
    }
    
    var configuration: Configuration
    var list: [T]
    @Binding var selection: T?
    
    final class Coordinator : NSObject, NSTableViewDataSource, NSTableViewDelegate {
        
        var snapshot: TableView
        
        var sortedColumn: Int?
        var ascending = true
        var comparator: ((T, T) -> Bool)?
        var sortedList: [T]

        init(_ tableView: TableView) {
            self.snapshot = tableView
            self.sortedColumn = 0
            let comparator = snapshot.configuration.columns[0].compare
            self.comparator = comparator
            self.sortedList = snapshot.list.sorted(by: comparator)
            if let collator = snapshot.configuration.collator {
                self.sortedList = self.sortedList.sorted(by: collator)
            }
        }
        
        func updateList(list: [T], tableView: NSTableView) {
            self.snapshot.list = list
            sort(tableView: tableView)
        }
        
        func sort(tableView: NSTableView) {
            if let comparator = comparator {
                func directionalComparator(lhs: T, rhs: T) -> Bool {
                    ascending ? comparator(lhs, rhs) : !comparator(lhs, rhs)
                }
                sortedList = snapshot.list.sorted(by: directionalComparator)
            } else {
                sortedList = snapshot.list
            }
            if let collator = snapshot.configuration.collator {
                self.sortedList = self.sortedList.sorted(by: collator)
            }
            tableView.reloadData()
        }

        func numberOfRows(in tableView: NSTableView) -> Int {
            return sortedList.count
        }
        
        func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any? {
            if let tableColumn = tableColumn {
                if let index = Int(tableColumn.identifier.rawValue) {
                    return snapshot.configuration.columns[index].format(sortedList[row])
                }
            }
            return nil
        }
        
        func tableViewSelectionDidChange(_ notification: Notification) {
            if let tableView = notification.object as? NSTableView {
                if tableView.selectedRow == -1 {
                    snapshot.selection = nil
                } else {
                    snapshot.selection = sortedList[tableView.selectedRow]
                }
            }
        }
        
        func tableView(_ tableView: NSTableView, didClick tableColumn: NSTableColumn) {
            if let index = Int(tableColumn.identifier.rawValue) {
                if let sortedColumn = sortedColumn {
                    if sortedColumn == index {
                        // toggle direction
                        ascending = !ascending
                    } else {
                        // sort by a different column
                        tableView.setIndicatorImage(nil, in: tableView.tableColumns[sortedColumn])
                    }
                }
                
                tableView.setIndicatorImage(ascending ? NSImage(named: "NSAscendingSortIndicator") : NSImage(named: "NSDescendingSortIndicator"), in: tableColumn)
                
                sortedColumn = index
                comparator = snapshot.configuration.columns[index].compare
                
                sort(tableView: tableView)
            }
        }
        
        func tableView(_ tableView: NSTableView, willDisplayCell cell: Any, for tableColumn: NSTableColumn?, row: Int) {
            if let colorFunction = snapshot.configuration.colors,
                let cell = cell as? NSTextFieldCell {
                let data = sortedList[row]
                let (fg, bg) = colorFunction(data)
                cell.backgroundColor = bg
                cell.textColor = fg
                cell.drawsBackground = true
            }
        }
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func configure(tableView: NSTableView, configuration: Configuration) {
        for (index, column) in configuration.columns.enumerated() {
            let tableColumn = NSTableColumn(identifier: NSUserInterfaceItemIdentifier(rawValue: index.description))
            tableColumn.width = CGFloat(column.width)
            tableColumn.title = column.title
            tableColumn.headerToolTip = column.tooltip
            tableColumn.minWidth = CGFloat(column.width)
            tableColumn.resizingMask = [ .autoresizingMask, .userResizingMask ]
            
            tableView.addTableColumn(tableColumn)
        }
    }
    
    func makeNSView(context: NSViewRepresentableContext<TableView>) -> NSScrollView {
        let tableView = NSTableView()
        
        configure(tableView: tableView, configuration: configuration)
                
        tableView.delegate = context.coordinator
        tableView.dataSource = context.coordinator
        tableView.reloadData()

        tableView.autoresizingMask = [ .height, .width ]
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.columnAutoresizingStyle = .uniformColumnAutoresizingStyle
        
        tableView.sizeToFit()

        let scrollView = NSScrollView()
        scrollView.documentView = tableView
        scrollView.hasVerticalScroller = true
        scrollView.hasHorizontalScroller = true

        scrollView.autoresizingMask = [ .height, .width ]
        scrollView.translatesAutoresizingMaskIntoConstraints = true
        
        return scrollView
    }

    func updateNSView(_ nsView: NSScrollView, context: NSViewRepresentableContext<TableView>) {
        if let tableView = nsView.documentView as? NSTableView {
            if context.coordinator.snapshot.configuration != configuration {
                for column in tableView.tableColumns {
                    tableView.removeTableColumn(column)
                }
                configure(tableView: tableView, configuration: configuration)
                context.coordinator.snapshot.configuration = configuration
            }
            
            if context.coordinator.snapshot.list != list {
                context.coordinator.updateList(list: list, tableView: tableView)
                
                // if needed, enlarge the columns to be a more realistic size
                if !list.isEmpty {
                    DispatchQueue.main.async {
                        for column in tableView.tableColumns {
                            let cell = column.dataCell(forRow: 0) as! NSCell
                            let width = cell.cellSize.width
                            if column.width < width {
                                column.width = width
                            }
                        }
                    }
                }
            }
        }
    }
  
}
