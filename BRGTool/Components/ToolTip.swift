//
//  ToolTip.swift
//  BRGTool
//
//  Created by David Koski on 3/17/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI

extension View {
    
    func tooltip(_ text: String?) -> some View {
        text == nil ? AnyView(self) : AnyView(self.overlay(TooltipView(tooltip: text!)))
    }
    
}

struct TooltipView: NSViewRepresentable {
    let tooltip: String
    
    func makeNSView(context: Context) -> NSView {
        let view = NSView()
        view.toolTip = tooltip
        return view
    }
    
    func updateNSView(_ nsView: NSView, context: Context) {
    }
}

