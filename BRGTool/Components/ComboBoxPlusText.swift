//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI

/// SwiftUI hosted NSComboBox -- show a list of items, but let the user type in a custom String as well
struct ComboBoxPlusText<T: CustomStringConvertible & Equatable & NSCopying> : NSViewRepresentable {

    var list: [T]
    var size = 10
    var restrict = false
    
    @Binding var selection: T?
    @Binding var string: String?
    
    var onCommit: () -> Void = { }

    final class Coordinator : NSObject, NSComboBoxDelegate, NSComboBoxDataSource {
        
        var config: ComboBoxPlusText

        init(_ config: ComboBoxPlusText) {
            self.config = config
        }
        
        func control(_ control: NSControl, isValidObject obj: Any?) -> Bool {
            if config.restrict {
                if let obj = obj as? T {
                    return config.list.contains(where: { $0 == obj })
                } else {
                    return false
                }
            } else {
                return true
            }
        }
        
        func controlTextDidChange(_ notification: Notification) {
            if let comboBox = notification.object as? NSComboBox {
                let index = self.comboBox(comboBox, indexOfItemWithStringValue: comboBox.stringValue)
                if config.restrict {
                    if index != NSNotFound {
                        config.selection = config.list[index]
                    }
                } else {
                    if index != NSNotFound {
                        config.selection = config.list[index]
                        config.string = nil
                    } else {
                        config.selection = nil
                        config.string = comboBox.stringValue
                    }
                }
            }
        }

        func comboBoxSelectionDidChange(_ notification: Notification) {
            if let comboBox = notification.object as? NSComboBox {
                config.selection = config.list[comboBox.indexOfSelectedItem]
                config.string = nil
            }
        }
        
        func numberOfItems(in comboBox: NSComboBox) -> Int {
            config.list.count
        }
        
        func comboBox(_ comboBox: NSComboBox, objectValueForItemAt index: Int) -> Any? {
            config.list[index]
        }
        
        func comboBox(_ comboBox: NSComboBox, indexOfItemWithStringValue string: String) -> Int {
            config.list.firstIndex { $0.description == string } ?? NSNotFound
        }
                
        func comboBox(_ comboBox: NSComboBox, completedString string: String) -> String? {
            config.list.first {
                $0.description.lowercased().starts(with: string.lowercased())
            }?.description
        }
                        
        func controlTextDidEndEditing(_ obj: Notification) {
            config.onCommit()
        }

    }

    func makeCoordinator() -> ComboBoxPlusText.Coordinator {
        return Coordinator(self)
    }

    func makeNSView(context: NSViewRepresentableContext<ComboBoxPlusText>) -> NSComboBox {
        let view = NSComboBox()
        
        view.numberOfVisibleItems = size
        view.completes = true
        view.hasVerticalScroller = true
        view.usesDataSource = true
        view.dataSource = context.coordinator
        view.delegate = context.coordinator
        
        view.stringValue = selection?.description ?? ""
                
        return view
    }

    func updateNSView(_ view: NSComboBox, context:  NSViewRepresentableContext<ComboBoxPlusText>) {
        if list != context.coordinator.config.list {
            context.coordinator.config.list = list
            view.reloadData()
        }
        let index = view.indexOfSelectedItem
        if index == -1 && selection != nil {
            view.objectValue = selection
        } else if index != -1 && selection != context.coordinator.config.list[index] {
            view.objectValue = selection
        }
    }
}
