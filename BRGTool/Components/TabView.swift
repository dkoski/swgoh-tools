//
//  TabView.swift
//  BRGTool
//
//  Created by David Koski on 3/25/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI

import SwiftUI

struct Tab : View {
    
    let index: Int
    let title: String
    var defaultIndex: Int? = nil
    @Binding var selection: Int?
    
    var body: some View {
        Button(action: { self.selection = self.selection == self.index ? self.defaultIndex : self.index }) {
            if self.selection == self.index {
                Text(title).bold()
            } else {
                Text(title)
            }
        }.buttonStyle(BorderlessButtonStyle())
    }
    
}

struct TabContents <Content: View>: View {
    
    let index: Int
    let selection: Int?
    let content: () -> Content
    
    init(index: Int, selection: Int?, @ViewBuilder content: @escaping () -> Content) {
        self.index = index
        self.selection = selection
        self.content = content
    }

    var body: some View {
        Group {
            if index == selection {
                content()
            }
        }
    }
}

