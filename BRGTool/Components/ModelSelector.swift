//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Combine
import SWGOH

struct ModelSelector<T: TopLevelModel>: View {
    
    @Binding var selection: DataFile<T>?
    
    @ObservedObject var dataFileManager: DataFileManager.Storage<T>
    @EnvironmentObject var waiting: WaitingModel

    // I have to do this dance to get something I can observe and push back into selection
    class Model : NSObject, ObservableObject {
        @Published var selectedIndex: Int = 0
        @Published var allyCodeStorage: String = ""
    }
    
    @State var model = Model()
    
    @State var downloadNew = false
    @State var allyCode: Int?
        
    var body: some View {
        let allDataFiles = dataFileManager.values
        
        if let selection = selection, model.selectedIndex < allDataFiles.count && allDataFiles[model.selectedIndex] != selection {
            model.selectedIndex = allDataFiles.firstIndex(of: selection) ?? model.selectedIndex
        }
        
        return HStack {
            if downloadNew {
                // I want to do this
                // TextField("###-###-### (you can paste in a swgoh url)", value: $allyCode, formatter: AllyCodeFormatter(), onCommit: self.download)
                // but I found that the AllyCodeFormatter() keeps getting called with nsnull (I presume the nil value of allyCode) and it never pushes a new value until commit time
                
                // instead we store it in something we can listen to
                TextField("###-###-### (you can paste in a swgoh url)", text: $model.allyCodeStorage, onCommit: self.download)
                    .onReceive(model.$allyCodeStorage) { (value) in
                        self.allyCode = Int(allyCode: value)
                    }
                
                IconButton(.cancel) {
                    self.downloadNew = false
                }
            } else {
                Picker(selection: $model.selectedIndex, label: EmptyView()) {
                    ForEach(0 ..< allDataFiles.count, id: \.self) { index in
                        Text("\(allDataFiles[index].name) (\(allDataFiles[index].allyCode.asAllyCode)) - \(allDataFiles[index].dateDescription)").tag(index)
                    }
                }
                .onReceive(model.$selectedIndex) { (index) in
                    if index < self.dataFileManager.values.count {
                        self.selection = self.dataFileManager.values[index]
                    }
                }

                // I want to do this, but the selection doesn't seem to work that way
//                Picker(selection: $selection, label: EmptyView()) {
//                    ForEach(allDataFiles, id: \.self) { dataFile in
//                        Text("\(dataFile.name) (\(dataFile.allyCode.asAllyCode)) - \(dataFile.dateDescription)")
//                    }
//                }
                
                IconButton(.refresh, action: self.refresh)
                .disabled(selection == nil || !selection!.isOutOfDate)

                IconButton(.add) {
                    self.downloadNew = true
                }
            }
        }
    }
    
    @State var cancellable: AnyCancellable?
    func download() {
        if let allyCode = allyCode {
            model.allyCodeStorage = allyCode.asAllyCode
            
            waiting.activity = "Downloading \(allyCode.asAllyCode)"
            let publisher = dataFileManager.download(allyCode: allyCode)
                .share().eraseToAnyPublisher()
            
            cancellable = publisher
                .receive(on: DispatchQueue.main)
                .sink(receiveCompletion: { _ in
                    self.cancellable = nil
                }) { (dataFile) in
                    self.downloadNew = false
                    self.selection = dataFile
                    self.model.selectedIndex = self.dataFileManager.values.firstIndex(of: dataFile) ?? 0
                }
            
            waiting.waitFor(publisher)
        }
    }
    
    func refresh() {
        if let selection = selection {
            downloadNew = false
            waiting.activity = "Refreshing \(selection.name)"
            let publisher = dataFileManager.update(selection)
                .share().eraseToAnyPublisher()
            
            cancellable = publisher
                .receive(on: DispatchQueue.main)
                .sink(receiveCompletion: { _ in
                    self.cancellable = nil
                }) { (dataFile) in
                    self.selection = dataFile
                    self.model.selectedIndex = self.dataFileManager.values.firstIndex(of: dataFile) ?? 0
                }
            
            waiting.waitFor(publisher)
        }
    }
}

struct ModelSelector_Previews: PreviewProvider {
    
    struct Stateful : View {
        
        @State var guild: DataFile<Model.Guild>?
        
        var body: some View {
            ModelSelector(selection: $guild, dataFileManager: DataFileManager.guilds)
        }
        
    }
    
    static var previews: some View {
        Stateful()
            .environmentObject(WaitingModel())
            .environmentObject(DataFileManager.guilds)
    }
}
