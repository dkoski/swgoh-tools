//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Combine

struct URLImageView: View {
    
    let url: URL
    
    var size: Int = 0

    @State var loadedURL: URL? = nil
    @State var image: NSImage? = nil
    
    static let emptyImage: CGImage = {
        CGContext(data: nil, width: 64, height: 64, bitsPerComponent: 8, bytesPerRow: 0, space: CGColorSpace(name: CGColorSpace.sRGB)!, bitmapInfo: CGImageAlphaInfo.premultipliedLast.rawValue)!.makeImage()!
        
    }()
    
    var body: some View {
        if image == nil || loadedURL != url {
            let capturedURL = url
            return AnyView(
                Image(URLImageView.emptyImage, scale: 1.0, label: Text("Loading"))
                .resizable()
                .aspectRatio(contentMode: .fit)
                .onReceive(ImageManager.shared.fetch(url: self.url)
                    .receive(on: DispatchQueue.main)
                    .replaceError(with: nil), perform: { (image) in
                    self.image = image
                    self.loadedURL = capturedURL
                }))
        } else {
            return AnyView(
                Image(nsImage: image!)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
            )
        }
    }
}

struct URLImageView_Previews: PreviewProvider {
    static var previews: some View {
        URLImageView(url: URL(string: "https://swgoh.gg/game-asset/u/JEDIKNIGHTREVAN/")!)
        .clipShape(Circle())
    }
}
