//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI

struct TextArea : NSViewRepresentable {
    
    @Binding var text : String
    
    final class Coordinator : NSObject, NSTextViewDelegate {

        var text : Binding<String>
        
        init(text: Binding<String>) {
            self.text = text
        }
        
        func textDidChange(_ notification: Notification) {
            if let view = notification.object as? NSTextView {
                text.wrappedValue = view.string
            }
        }
        
        func textDidEndEditing(_ notification: Notification) {
            if let view = notification.object as? NSTextView {
                text.wrappedValue = view.string
            }
        }
        
        func textView(_ textView: NSTextView, doCommandBy commandSelector: Selector) -> Bool {
            switch commandSelector.description {
            case "insertNewline:":
                textView.insertNewlineIgnoringFieldEditor(true)
                return true
                
            default:
                return false
            }
        }
    }

    func makeCoordinator() -> Coordinator {
        return Coordinator(text: $text)
    }

    func makeNSView(context: NSViewRepresentableContext<TextArea>) -> NSScrollView {
        let scrollView = NSScrollView()
        scrollView.drawsBackground = true
        scrollView.borderType = .noBorder
        scrollView.hasVerticalScroller = true
        scrollView.hasHorizontalRuler = false
        scrollView.autoresizingMask = [.width, .height]
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        
        let contentSize = scrollView.contentSize
        let textStorage = NSTextStorage()

        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        
        let textContainer = NSTextContainer(containerSize: scrollView.frame.size)
        textContainer.widthTracksTextView = true
        textContainer.containerSize = NSSize(width: contentSize.width, height: CGFloat.greatestFiniteMagnitude)
        
        layoutManager.addTextContainer(textContainer)

        let textView = NSTextView(frame: .zero, textContainer: textContainer)
        textView.isSelectable = true
        textView.isEditable = false
        textView.isFieldEditor = true
        textView.isRichText = false
        textView.importsGraphics = false
        textView.string = text
        textView.delegate = context.coordinator
        textView.isVerticallyResizable = true
        textView.isHorizontallyResizable = false
        textView.minSize = NSSize(width: 0, height: 20)
        textView.maxSize = NSSize(width: CGFloat.infinity, height: CGFloat.infinity)
        textView.autoresizingMask = [ .height, .width ]
        
        textView.unregisterDraggedTypes()
        
        scrollView.documentView = textView
        
        return scrollView
    }

    func updateNSView(_ view: NSScrollView, context:  NSViewRepresentableContext<TextArea>) {
        if let textView = view.documentView as? NSTextView {
            textView.string = text
        }
    }
}

struct TextArea_Previews: PreviewProvider {
    
    struct StatefulView : View {
        
        @State var value = "Initial text"

        var body: some View {
            VStack {
                TextArea(text: $value)
                Divider()
                Text("Value: \(value)")
            }
            .frame(width: 300, height: 300)
        }
    }
    

    static var previews: some View {
        StatefulView()
    }
}
