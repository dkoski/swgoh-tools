//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
import AppKit
import SwiftUI
import Combine

class KeyboardEventModel : ObservableObject {
    
    fileprivate let subject = PassthroughSubject<String, Never>()
    public let events: Publishers.Share<PassthroughSubject<String, Never>>
    
    init() {
        events = subject.share()
    }
    
}

class KeyboardEventWindow : NSWindow {
    
    let keyboardEventModel = KeyboardEventModel()

    override func keyDown(with event: NSEvent) {
        if let keys = event.characters {
            keyboardEventModel.subject.send(keys)
        }
    }

}
