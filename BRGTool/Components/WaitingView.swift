//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Combine
import Foundation

class WaitingModel : NSObject, ObservableObject {
    
    @Published var showSpinner = false
    @Published var error: Error?
    @Published var activity: String?

    private var waitingOn: AnyCancellable?
    
    func waitFor<T>(_ publisher: AnyPublisher<T, Error>) {
        // run without the spinner for a short time in case it is fast
        var running = true
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
            if running {
                print("still running")
                self.showSpinner = true
            } else {
                print("already done")
            }
        }
        
        waitingOn = publisher
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { (completion) in
                // if we receive this early enough, don't bring up the spinner
                running = false
                
                switch completion {
                case .failure(let error):
                    self.error = error
                case .finished:
                    self.showSpinner = false
                    self.activity = nil
                    break
                }
                self.waitingOn = nil

            }) { _ in }
    }
}

struct Spinner: NSViewRepresentable {

    func makeNSView(context: NSViewRepresentableContext<Spinner>) -> NSProgressIndicator {
        let view = NSProgressIndicator()
        view.isIndeterminate = true
        view.style = .spinning
        view.startAnimation(nil)
        return view
    }

    func updateNSView(_ nsView: NSProgressIndicator, context: NSViewRepresentableContext<Spinner>) {
    }
}

struct WaitingView<Content: View>: View {
    
    @EnvironmentObject var model: WaitingModel

    var content: Content
    
    init(@ViewBuilder content: () -> Content) {
        self.content = content()
    }

    var body: some View {
        ZStack(alignment: .center) {
            
            self.content
                .environmentObject(model)
                .disabled(model.showSpinner)
                .blur(radius: model.showSpinner ? 3 : 0)
            
            VStack {
                if model.showSpinner && model.error == nil {
                    Spinner()
                }
                
                if model.activity != nil && model.error == nil {
                    Text(model.activity!)
                }
                
                if model.showSpinner && model.error != nil {
                    Text(model.error!.localizedDescription)
                        .font(.title)
                    Button(action: {
                        self.model.showSpinner = false
                        self.model.error = nil
                    }) {
                        Text("OK")
                    }
                }
            }
        }
    }
}
