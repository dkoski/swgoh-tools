//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI

struct IconButton: View {
    
    enum Icon : String {
        case add
        case cancel
        case cancelCircle
        case refresh
        case delete
        case gear
        
        var imageLiteralResourceName: String {
            switch self {
            case .add:
                return NSImage.addTemplateName
            case .cancel:
                return NSImage.stopProgressTemplateName
            case .cancelCircle:
                return NSImage.stopProgressFreestandingTemplateName
            case .refresh:
                return NSImage.refreshTemplateName
            case .delete:
                return NSImage.removeTemplateName
            case .gear:
                return NSImage.smartBadgeTemplateName
            }
        }
    }
    
    let icon: Icon
    let action: () -> Void
    
    init(_ icon: Icon, action: @escaping () -> Void) {
        self.icon = icon
        self.action = action
    }
    
    var body: some View {
        Button(action: action) {
            Image(nsImage: NSImage(imageLiteralResourceName: icon.imageLiteralResourceName))
        }
        .buttonStyle(PlainButtonStyle())
    }
}

struct IconButton_Previews: PreviewProvider {
    static var previews: some View {
        IconButton(.add) { }
    }
}
