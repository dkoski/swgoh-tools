//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import SWGOH

struct CharacterIcon: View {
    
    let character: Model.Character
    
    var size = 48
    var borderWidth = 4
    var borderColor = Color.blue
    
    var body: some View {
        URLImageView(url: character.imageURL)
            .frame(width: CGFloat(size), height: CGFloat(size))
            .clipShape(Circle())
            .overlay(Circle()
                .stroke(borderColor, lineWidth: CGFloat(borderWidth)))
            .offset(x: 3, y: 0)
    }
}

extension Model.Character {

    func icon(size: Int = 48, borderWidth: Int = 4, borderColor: Color = Color.blue) -> CharacterIcon {
        CharacterIcon(character: self, size: size, borderWidth: borderWidth, borderColor: borderColor)
    }
    
}

extension Model.Unit {
    
    func icon(size: Int = 48, borderWidth: Int = 4, borderColor: Color = Color.blue) -> CharacterIcon {
        CharacterIcon(character: self.character, size: size, borderWidth: borderWidth, borderColor: borderColor)
    }

}

extension Squad {
    
    func leaderIcon(size: Int = 48, borderWidth: Int = 4, borderColor: Color = Color.blue) -> CharacterIcon {
        CharacterIcon(character: self.units[0].character, size: size, borderWidth: borderWidth, borderColor: borderColor)
    }

}
