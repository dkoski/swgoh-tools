//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import SWGOH

struct HistogramView: View {
    
    let histograms: [Histogram]
    var maxCount: Int { histograms.map { $0.maxCount }.max() ?? 1 }

    func path(size: CGSize, histogram: Histogram, offset: Int) -> Path {
        Path { p in
            let bucketWidth = size.width / CGFloat(histogram.count - 1)
            let height = size.width > 200 ? size.height - 7 : size.height
            let verticalScale = height / CGFloat(self.maxCount)
            
            p.move(to: .init(x: 0, y: height - CGFloat(histogram.buckets[0]) * verticalScale))
            for (index, bucket) in histogram.buckets.dropFirst().enumerated() {
                p.addLine(to: .init(x: CGFloat(index + 1) * bucketWidth, y: height - CGFloat(bucket) * verticalScale))
            }
            
            if size.width > 200 {
                if let markers = histogram.markers {
                    for marker in markers.values {
                        let x = CGFloat((marker - histogram.start) / histogram.bucketSize) * bucketWidth + CGFloat(offset)
                        p.move(to: .init(x: x, y: size.height))
                        p.addLine(to: .init(x: x, y: 0))
                    }
                }
            }
        }
    }
    
    var body: some View {
        GeometryReader { reader in
            self.path(size: reader.size, histogram: self.histograms[0], offset: 0)
                .stroke(Color.blue, lineWidth: 2)
            self.path(size: reader.size, histogram: self.histograms[1], offset: 2)
                .stroke(Color(red: 0.5, green: 0.5, blue: 0), lineWidth: 2)
            
            if reader.size.width > 200 {
                Text(NumberFormatter.decimal.string(from: self.histograms[0].start))
                    .font(Font.system(size: 8))
                    .offset(x: 0, y: reader.size.height - 10)
                Text(NumberFormatter.decimal.string(from: self.histograms[0].start + self.histograms[0].count * self.histograms[0].bucketSize))
                    .font(Font.system(size: 8))
                    .offset(x: reader.size.width - 50, y: reader.size.height - 10)
            }
        }
    }
}

struct HistogramView_Previews: PreviewProvider {
    
    static var previews: some View {
        let data1 = [ 500, 500, 550, 600, 610, 700, 900, 1100, 1105, 1095, 1101, 1102]
        let data2 = [ 500, 500, 550, 600, 610, 700, 900, 1100, 1105, 1095, 1101, 1102].map { $0 + Int.random(in: 0 ..< 200)}
        let params = Histogram.analyze(data: [data1, data2])
        let h1 = Histogram(data: data1, start: params.start, bucketSize: params.bucketSize, count: params.count)
        let h2 = Histogram(data: data2, start: params.start, bucketSize: params.bucketSize, count: params.count)
        
        return HistogramView(histograms: [h1, h2]).frame(width: 70, height: 20).border(Color.black).padding().foregroundColor(Color.yellow).background(Color.white)
    }
}
