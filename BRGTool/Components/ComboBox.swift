//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI

/// SwiftUI hosted NSComboBox -- show a list of items
struct ComboBox<T: CustomStringConvertible & Equatable & NSCopying> : NSViewRepresentable {

    var list: [T]
    var size = 10
    
    // if this requires a selection to finish editing
    var required = false
    
    @Binding var selection: T?
    
    var onCommit: () -> Void = { }
    var onChange: () -> Void = { }

    final class Coordinator : NSObject, NSComboBoxDelegate, NSComboBoxDataSource {
        
        var config: ComboBox

        init(_ config: ComboBox) {
            self.config = config
        }
        
        func control(_ control: NSControl, isValidObject obj: Any?) -> Bool {
            if config.required {
                if let obj = obj as? T {
                    return config.list.contains(where: { $0 == obj })
                } else {
                    return false
                }
            } else {
                return true
            }
        }
        
        func controlTextDidChange(_ notification: Notification) {
            if let comboBox = notification.object as? NSComboBox {
                let index = self.comboBox(comboBox, indexOfItemWithStringValue: comboBox.stringValue)
                if index != NSNotFound {
                    config.selection = config.list[index]
                    config.onChange()
                } else {
                    config.selection = nil
                }
            }
        }

        func comboBoxSelectionDidChange(_ notification: Notification) {
            if let comboBox = notification.object as? NSComboBox {
                config.selection = config.list[comboBox.indexOfSelectedItem]
                config.onChange()
            }
        }
        
        func numberOfItems(in comboBox: NSComboBox) -> Int {
            config.list.count
        }
        
        func comboBox(_ comboBox: NSComboBox, objectValueForItemAt index: Int) -> Any? {
            config.list[index]
        }
        
        func comboBox(_ comboBox: NSComboBox, indexOfItemWithStringValue string: String) -> Int {
            config.list.firstIndex { $0.description == string } ?? NSNotFound
        }
                
        func comboBox(_ comboBox: NSComboBox, completedString string: String) -> String? {
            config.list.first {
                $0.description.lowercased().starts(with: string.lowercased())
            }?.description
        }
                        
        func controlTextDidEndEditing(_ obj: Notification) {
            config.onCommit()
        }

    }

    func makeCoordinator() -> ComboBox.Coordinator {
        return Coordinator(self)
    }

    func makeNSView(context: NSViewRepresentableContext<ComboBox>) -> NSComboBox {
        let view = NSComboBox()
        
        view.numberOfVisibleItems = size
        view.completes = true
        view.hasVerticalScroller = true
        view.usesDataSource = true
        view.dataSource = context.coordinator
        view.delegate = context.coordinator
        
        view.stringValue = selection?.description ?? ""
                
        return view
    }

    func updateNSView(_ view: NSComboBox, context:  NSViewRepresentableContext<ComboBox>) {
        if list != context.coordinator.config.list {
            context.coordinator.config.list = list
            view.reloadData()
        }
        let index = view.indexOfSelectedItem
        if index == -1 && selection != nil {
            view.objectValue = selection
        } else if index != -1 && selection != context.coordinator.config.list[index] {
            view.objectValue = selection
        }
    }
}

struct ComboBox_Previews: PreviewProvider {
    
    class Item : NSObject, NSCopying {
        
        let name: String
        
        init(_ name: String) {
            self.name = name
        }

        func copy(with zone: NSZone? = nil) -> Any {
            self
        }

        override var description: String {
            name
        }

    }
    
    struct Wrapper : View {
        
        let list = [ Item("Car"), Item("Train") ]
        
        @State var selection: Item?

        var body: some View {
            VStack {
                ComboBox(list: list, selection: $selection)
                Divider()
                Text("Selection: item = \(selection?.description ?? "nil")")
            }
            .frame(width: 300, height: 300)
        }
    }
    

    static var previews: some View {
        Wrapper()
    }
}
