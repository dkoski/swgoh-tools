//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Combine
import SWGOH

struct LocalModelSelector<T: TopLevelModel & LocalModel>: View {
    
    @Binding var selection: DataFile<T>?
    @ObservedObject var dataFileManager: DataFileManager.Storage<T>
    
    let newModelFactory: (String) -> T
    let dataFile: (T) -> DataFile<T>
    
    @State var newFile = false
    
    // a dance to work around Picker not working with anything other than ints
    class Model : NSObject, ObservableObject {
        @Published var selectedIndex: Int = 0
    }

    @State var model = Model()
    
    var body: some View {
        let files = dataFileManager.values

        return HStack {
            if newFile {
                Text(self.selection!.name).bold()
            } else {
                Picker("", selection: $model.selectedIndex) {
                    ForEach(0 ..< files.count, id: \.self) { index in
                        Text("\(files[index].name) - \(files[index].dateDescription)").tag(index)
                    }
                }
                .onReceive(model.$selectedIndex) { newIndex in
                    if newIndex < self.dataFileManager.values.count {
                        self.selection = self.dataFileManager.values[newIndex]
                    }
                }

                IconButton(.add) {
                    self.selection = self.dataFile(self.newModelFactory("New \(self.dataFileManager.values.count + 1)"))
                    self.newFile = true
                }
                
                Button(action: importFile) {
                    Text("Import")
                }
            }
        }
    }
    
    func importFile() {
        let openPanel = NSOpenPanel()
        openPanel.allowedFileTypes = [ T.sharingFileExtension ]
        openPanel.beginSheetModal(for: NSApplication.shared.mainWindow!) { (response) in
            if response == .OK {
                if let url = openPanel.url,
                    let data = try? Data(contentsOf: url),
                    let config = try? JSONDecoder().decode(T.self, from: data) {
                    self.selection = self.dataFile(config)
                    self.newFile = true
                }
            }
        }
    }
}
