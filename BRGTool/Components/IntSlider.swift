//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import SWGOH

struct IntSlider: View {
    
    var value: Binding<Int>
    var range: ClosedRange<Double>
    
    class IntSliderModel : NSObject, ObservableObject {
        
        @Published var value: Double
        
        init(value: Int) {
            self.value = Double(value)
        }
        
    }
    
    @ObservedObject
    private var model: IntSliderModel
    
    init(value: Binding<Int>, in range: ClosedRange<Int>) {
        self.value = value
        self.range = Double(range.lowerBound) ... Double(range.upperBound)
        self.model = IntSliderModel(value: value.wrappedValue)
    }
    
    var body: some View {
        HStack {
            Text(NumberFormatter.integer.string(from: model.value))
            Slider(value: self.$model.value, in: range)
                .onReceive(model.objectWillChange
                    // if we handle it right away, I find that the slider interaction gets interrupted and the slider doesn't work properly
                    .debounce(for: 0.25, scheduler: DispatchQueue.main)) { _ in
                        self.value.wrappedValue = Int(self.model.value)
                    }
        }
    }
}

