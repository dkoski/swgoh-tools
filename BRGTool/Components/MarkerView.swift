//
//  MarkerView.swift
//  BRGTool
//
//  Created by David Koski on 3/17/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI

/// Based on ideas from https://github.com/siteline/SwiftUI-Introspect
///
/// A way to mark a View for later instrospection / manipulation.
///
/// If you need to find a particular NSView from a View hierarchy, you can mark it and find it later.  For example, you might
/// want to make something `becomeFirstResponder()` (SwiftUI does not have this functionality right now).  You
/// would mark the view you want to find like this:
///
/// ```
///     TextField(...).mark("Title")
/// ```
///
/// To manipulate it, you can as a root view to find the marker.  Note that a NSHostingView will not have any children until
/// it is displayed.  You can use code like this:
///
/// ```
///         let contentView = ...
///         let hostingView = NSHostingView(rootView: contentView)
///
///         enagagementWindow.contentView = hostingView
///         enagagementWindow.makeKeyAndOrderFront(nil)
///
///         DispatchQueue.main.async {
///             if let titleView: NSTextField = hostingView.findMarkedView("Title") {
///                 titleView.becomeFirstResponder()
///             }
///         }
/// ```
///
/// The `DispatchQueue.main.async` runs on the main run loop after the next event -- this gives the NSHostingView
/// time to set itself up.
///
/// Note:  The `.tag()` view modifier doesn't actually set tags on views in general -- it is meant for use with combo boxes, etc.
struct MarkerView: NSViewRepresentable {
    
    private static let queue = DispatchQueue(label: "MarkerView")
    private static var registry = [String:Int]()
    
    static func find<T: NSView>(name: String, in view: NSView) -> T? {
        let tag = queue.sync { () -> Int in
            if let tag = registry[name] {
                return tag
            } else {
                fatalError("Unable to find tag for \(name)")
            }
        }
        
        guard let markerView = view.viewWithTag(tag) as? MarkerNSView else {
            // not in the view hierarchy -- this is the only way we return nil
            return nil
        }

        // The views are structured like this:
        //
        // some parent view
        //     view hierarchy we want
        //          ... the target view
        //     view host
        //         MarkerView (the tagged view)
            
        guard let viewHost = markerView.superview, let parent = viewHost.superview else {
            fatalError("Unable to find parent/view host for \(name)")
        }
        
        guard let index = parent.subviews.firstIndex(of: viewHost), index > 0 else {
            fatalError("Unable to find view host in parent for \(name)")
        }
        
        let previousSibling = parent.subviews[index - 1]
        if let targetView: T = previousSibling.findSubView() {
            return targetView
        } else {
            fatalError("Target view is wrong type for \(name)")
        }
    }
    
    private let tag: Int
    
    init(_ name: String) {
        self.tag = MarkerView.queue.sync { () -> Int in
            if let tag = MarkerView.registry[name] {
                return tag
            } else {
                let tag = 55000 + MarkerView.registry.count
                MarkerView.registry[name] = tag
                return tag
            }
        }
    }
    
    class MarkerNSView: NSView {
        
        private var tagStorage = 0
        
        override var tag: Int {
            get { return tagStorage }
            set { tagStorage = newValue }
        }
        
        init(tag: Int) {
            self.tagStorage = tag
            super.init(frame: .zero)
            isHidden = true
        }
        
        required init?(coder: NSCoder) {
            super.init(coder: coder)
        }
        
        override func hitTest(_ point: NSPoint) -> NSView? {
            return nil
        }
    }

    func makeNSView(context: NSViewRepresentableContext<MarkerView>) -> MarkerNSView {
        let view = MarkerNSView(tag: tag)
        return view
    }

    func updateNSView(_ nsView: MarkerNSView, context: NSViewRepresentableContext<MarkerView>) {
    }

}

extension View {
    
    /// Mark a view with a name, e.g.: `TextField(...).mark("Title")` will allow you to find it later with
    /// `let tf: NSTextField = nsview.findMarkedView("Title")`
    func mark(_ name: String) -> some View {
        overlay(MarkerView(name).frame(width: 0, height: 0))
    }
    
}

extension NSView {
    
    func findMarkedView<T: NSView>(_ name: String) -> T? {
        MarkerView.find(name: name, in: self)
    }
    
    fileprivate func findSubView<T: NSView>() -> T? {
        if let view = self as? T {
            return view
        }
        
        for subview in subviews {
            if let view: T = subview.findSubView() {
                return view
            }
        }
        
        return nil
    }
    
}
