//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Combine
import SWGOH

struct PlayerComparisonView: View {
    
    @EnvironmentObject var dataFileManager: DataFileManager.Storage<Model.Player>
    @EnvironmentObject var waiting: WaitingModel
    

    @State var player1: DataFile<Model.Player>? = DataFileManager.defaultPlayer
    @State var player2: DataFile<Model.Player>? = DataFileManager.players.newest
    
    @State var data = [TWCompare.Row]()
    
    @State var gacModel = GACView.GACModel()
    
    @State var tab: Int? = 1
    
    var body: some View {
        WaitingView {
            VStack {
                HStack {
                    ModelSelector(selection: self.$player1, dataFileManager: DataFileManager.players)
                    Text("vs")
                    ModelSelector(selection: self.$player2, dataFileManager: DataFileManager.players)
                    
                    Button("swgoh", action: {
                        let url = URL(string: "https://swgoh.gg/p/\(self.player2!.allyCode)/gac-history/?a=d")!
                        NSWorkspace.shared.open(url)
                    })
                        .disabled(self.player2 == nil)

                    Button("Compare", action: self.compare)
                }
                Divider()
                HStack {
                    Tab(index: 0, title: "Compare", defaultIndex: 0, selection: self.$tab)
                    Tab(index: 1, title: "GAC", defaultIndex: 0, selection: self.$tab)
                }
                Divider()
                TabContents(index: 0, selection: self.tab) {
                    ComparisonView(data: self.$data)
                }
                TabContents(index: 1, selection: self.tab) {
                    if self.player1 != nil && self.player2 != nil {
                        GACView(model: self.gacModel)
                    } else {
                        Text("Select players")
                    }
                }
            }
            .padding()
        }
    }
    
    @State var cancellable: AnyCancellable? = nil

    fileprivate func compare(_ guild1: DataFile<Model.Player>, _ guild2: DataFile<Model.Player>) -> AnyPublisher<[TWCompare.Row], Error> {
        return guild1
            .squadEvaluations()
            .append(guild2.squadEvaluations())
            .collect()
            .map { guilds -> [TWCompare.Row] in
                let g1 = guilds[0]
                let g2 = guilds[1]
                
                let report = TWCompare(side1: g1, side2: g2)
                
                var rows = [TWCompare.Row]()
                rows.append(contentsOf: report.unitRows())
                rows.append(contentsOf: report.squadRows(isKey: true))
                rows.append(contentsOf: report.squadRows(isKey: false))

                return rows
            }
            .eraseToAnyPublisher()
    }
    
    func compare() {
        if let left = player1, let right = player2 {
            let future = compare(left, right).share()
            
            cancellable = future
                .receive(on: DispatchQueue.main)
                .sink(receiveCompletion: { _ in
                    self.cancellable = nil
                }, receiveValue: { (rows) in
                    self.data = rows
                    self.gacModel.setPlayers(player1: left, player2: right)
                })
            
            waiting.activity = "Preparing squads ..."
            waiting.waitFor(
                future
                    .receive(on: DispatchQueue.main)
                    .eraseToAnyPublisher()
            )
        }
    }
    
    /// ugh.  I would prever to have ColumnValueProvider produce a View, but the associated types wrecks that
    enum ColumnType {
        case normal
        case histogramScores
        case cumulativeHistogramScores
    }
    
    fileprivate struct Column : Hashable {
        let index: Int
        
        let label: String
        let tooltip: String?
        let width: CGFloat
        let value: ColumnValueProvider
        
        let type: ColumnType
        
        var isVisible: Bool
        let isSortable: Bool
        
        internal init(_ index: Int, label: String, tooltip: String? = nil, width: CGFloat, value: ColumnValueProvider, type: ColumnType = .normal, isVisible: Bool = true, isSortable: Bool = true) {
            self.index = index
            self.label = label
            self.tooltip = tooltip
            self.width = width
            self.value = value
            self.type = type
            self.isVisible = isVisible
            self.isSortable = type == .normal ? isSortable : false
        }
                
        func hash(into hasher: inout Hasher) {
            hasher.combine(label)
        }
        
        static func == (lhs: Column, rhs: Column) -> Bool {
            lhs.index == rhs.index && lhs.isVisible == rhs.isVisible
        }
    }

    struct ComparisonView : View {
        
        @Binding var data: [TWCompare.Row]
        
        @State private var columns = [
            Column(0, label: "Name", tooltip: "Squad name", width: 100, value: StringValueProvider(keyPath: \.label)),
            
            Column(1, label: "Count", width: 100, value: IntValueProvider(keyPath: \.team.count)),
            Column(2, label: "%", width: 50, value: PercentValueProvider(keyPath: \.percentCount)),
            Column(3, label: "Opponent", width: 100, value: IntValueProvider(keyPath: \.opponent.count)),
            
            Column(4, label: "GP", tooltip: "75th percentile", width: 100, value: IntValueProvider(keyPath: \.team.score)),
            Column(5, label: "%", width: 50, value: PercentValueProvider(keyPath: \.percentScore)),
            Column(6, label: "Opponent", width: 100, value: IntValueProvider(keyPath: \.opponent.score)),
            
            Column(7, label: "Histogram", width: 120, value: NOPValueProvider(), type: .histogramScores),
            Column(8, label: "Cum. Histogram", width: 120, value: NOPValueProvider(), type: .cumulativeHistogramScores, isVisible: false),
        ]
        
        var body: some View {
            VStack(alignment: .leading) {
                TableHeader(data: $data, columns: $columns)
                
                Divider().background(Color.blue)
                
                // using a scrollview instead of a list so I get more control over the vertical spacing
                ScrollView(.vertical, showsIndicators: true) {
                    HStack {
                        VStack(alignment: .leading, spacing: 4) {
                            ForEach(data) { row in
                                TableRow(row: row, columns: self.columns)
                            }
                        }
                        
                        Spacer()
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                    }
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            }
        }
                
    }
    
    fileprivate struct TableRow : View {
        
        let row: TWCompare.Row
        let columns: [Column]
        
        @State var showBig = false
        
        func item(column: Column) -> some View {
            if !column.isVisible {
                return AnyView(EmptyView())
            }
            
            var histograms: [Histogram]
            switch column.type {
            case .normal:
                return AnyView(HStack {
                    Text(column.value.value(row: self.row))
                        .frame(minWidth: column.width, maxWidth: column.width, alignment: .trailing)
                    Divider()
                })
                
            case .histogramScores:
                histograms = row.histograms
                
            case .cumulativeHistogramScores:
                histograms = row.cumulativeHistograms
            }
            
            if showBig {
                return AnyView(HStack {
                    HistogramView(histograms: histograms)
                    .frame(width: 300, height: 100)
                    .frame(minWidth: column.width, maxWidth: column.width, alignment: .trailing)
                })
            } else {
                return AnyView(HStack {
                    HistogramView(histograms: histograms)
                    .frame(minWidth: column.width, maxWidth: column.width, alignment: .trailing)
                })
            }
        }

        var body: some View {
            let (fg, bg) = colors()
            
            if row.teams[0].count <= 1 && row.teams[1].count <= 1 {
                return AnyView(HStack {
                    self.item(column: columns[0])
                    
                    HStack {
                        if row.teams[0].count > 0 {
                            SquadView(squad: row.teams[0].data.values.first!.first!)
                            Text(NumberFormatter.integer.string(from: row.teams[0].score))
                                .frame(width: 50)
                                .font(Font.system(size: 10))
                        } else {
                            Text("")
                        }
                    }
                    .frame(width: 357)
                    
                    HStack {
                        if row.teams[1].count > 0 {
                            Text(NumberFormatter.integer.string(from: row.teams[1].score))
                                .frame(width: 50)
                                .font(Font.system(size: 10))
                            SquadView(squad: row.teams[1].data.values.first!.first!)
                        } else {
                            Text("")
                        }
                    }
                    .frame(width: 357)
                }
                .foregroundColor(Color(fg))
                .background(Color(bg)))
            } else {
                return AnyView(HStack {
                    ForEach(self.columns, id: \.self) { column in
                        self.item(column: column)
                    }
                }
                .contentShape(Rectangle())
                .onTapGesture {
                    self.showBig.toggle()
                }
                .padding(EdgeInsets(top: 1, leading: 0, bottom: 1, trailing: 0))
                .foregroundColor(Color(fg))
                .background(Color(bg)))
            }
        }
        
        func colors() -> (NSColor, NSColor) {
            func lerp(_ a: Double, _ b: Double, mix: Double) -> CGFloat {
                CGFloat(a + (b - a) * mix)
            }
            
            let rating = row.rating(scores: [(row.team.count, row.opponent.count), (row.team.score, row.opponent.score)])
            
            let ok = 0.98
            if rating >= ok {
                return (NSColor.textColor, NSColor.textBackgroundColor)
            }
            
            return (NSColor.black, NSColor(displayP3Red: 1.0, green: lerp(0.0, 1.0, mix: rating), blue: 0, alpha: 1))
        }

    }
    
    fileprivate struct TableHeader : View {
        
        @State var sortColumn = 0
        @State var sortAscending = true

        @Binding var data: [TWCompare.Row]
        @Binding var columns: [Column]
        
        func singleHeader(column: Column) -> some View {
            if !column.isVisible {
                return AnyView(EmptyView())
            }
            switch column.type {
            case .normal:
                if column.isSortable {
                    return AnyView(HStack {
                        Button(action: {
                            self.sort(index: column.index)
                        }) {
                            if column.index == self.sortColumn {
                                Text(column.label).bold()
                            } else {
                                Text(column.label)
                            }
                        }
                        .buttonStyle(PlainButtonStyle())
                        .frame(minWidth: column.width, maxWidth: column.width)
                        .tooltip(column.tooltip)
                        
                        Divider().frame(maxHeight: 10)
                    })
                } else {
                    return AnyView(HStack {
                        Text(column.label).frame(minWidth: column.width, maxWidth: column.width)
                        .frame(minWidth: column.width, maxWidth: column.width)
                        .tooltip(column.tooltip)
                        
                        Divider().frame(maxHeight: 10)
                    })
                }
            case .histogramScores, .cumulativeHistogramScores:
                return AnyView(HStack {
                    Button(action: {
                        self.columns[7].isVisible.toggle()
                        self.columns[8].isVisible.toggle()
                    }) {
                        Text(column.label)
                    }
                    .buttonStyle(PlainButtonStyle())
                    .frame(minWidth: column.width - 10, maxWidth: column.width - 10)
                    .tooltip(column.tooltip)
                    
                    Button(action: {
                        NSWorkspace.shared.open(URL(string: "https://gorgatron1.github.io/brg/help/histograms.png")!)
                    }) {
                        Text("?")
                    }
                    .buttonStyle(LinkButtonStyle())
                    .frame(minWidth: 10, maxWidth: 10)
                })
            }
        }

        var body: some View {
            HStack {
                ForEach(columns, id: \.self) { column in
                    self.singleHeader(column: column)
                }
            }
        }
        
        func sort(index: Int) {
            if sortColumn == index {
                sortAscending.toggle()
            }
            sortColumn = index
            
            data.sort { (lhs, rhs) -> Bool in
                lhs.group == rhs.group ? columns[index].value.compare(lhs: lhs, rhs: rhs) == sortAscending : lhs.group < rhs.group
            }
        }

    }
    
}

private protocol ColumnValueProvider {
    func value(row: TWCompare.Row) -> String
    func compare(lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool
}

private struct StringValueProvider : ColumnValueProvider {
    
    let keyPath: KeyPath<TWCompare.Row, String>
    
    func value(row: TWCompare.Row) -> String {
        row[keyPath: keyPath]
    }
    
    func compare(lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool {
        let lv = lhs[keyPath: keyPath]
        let rv = rhs[keyPath: keyPath]
        return lv < rv
    }
}

private struct IntValueProvider : ColumnValueProvider {
    
    let keyPath: KeyPath<TWCompare.Row, Int>
    let formatter = NumberFormatter.decimal
    
    func value(row: TWCompare.Row) -> String {
        formatter.string(from: row[keyPath: keyPath])
    }
    
    func compare(lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool {
        let lv = lhs[keyPath: keyPath]
        let rv = rhs[keyPath: keyPath]
        return lv < rv
    }
}

private struct PercentValueProvider : ColumnValueProvider {
    
    let keyPath: KeyPath<TWCompare.Row, Double>
    let formatter = NumberFormatter.percent
    
    func value(row: TWCompare.Row) -> String {
        formatter.string(from: row[keyPath: keyPath])
    }
    
    func compare(lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool {
        let lv = lhs[keyPath: keyPath]
        let rv = rhs[keyPath: keyPath]
        return lv < rv
    }
}

private struct NOPValueProvider : ColumnValueProvider {
    
    func value(row: TWCompare.Row) -> String {
        return ""
    }
    
    func compare(lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool {
        return false
    }
    
}

struct GACView : View {
    
    class GACModel : NSObject, ObservableObject {
        
        var player1: Model.Player!
        
        @Published
        var squads1 = [Entry]() {
            didSet {
                let current = squads1.map { $0.status }
                if current != squads1Snapshot {
                    squads1Snapshot = current
                    rebuildSquads1Statuses()
                    rebuildSquads2Statuses()
                }
            }
        }

        var squads1Snapshot = [Status]()
        
        var player2: Model.Player!
        
        @Published
        var squads2 = [Entry]() {
            didSet {
                let current = squads2.map { $0.status }
                if current != squads2Snapshot {
                    squads2Snapshot = current
                    rebuildSquads1Statuses()
                }
            }
        }

        var squads2Snapshot = [Status]()
        
        func rebuildSquads1Statuses() {
            for index in squads1.indices {
                squads1[index].possibleStatuses = [ .none, .defend ].adding(targets(attacker: squads1[index].squad, opponent: squads2, onlyDefenders: true))
            }
        }

        func rebuildSquads2Statuses() {
            for index in squads2.indices {
                squads2[index].possibleStatuses = [ .none, .defend ].adding(targets(attacker: squads2[index].squad, opponent: squads1, onlyDefenders: true))
            }
        }

        struct Entry : Hashable {
            let squad: Squad
            var status: Status {
                didSet {
                    if let newIndex = possibleStatuses.firstIndex(of: status) {
                        if newIndex != statusIndex {
                            statusIndex = newIndex
                        }
                    }
                }
            }
            var statusIndex: Int = 0 {
                didSet {
                    let newStatus = possibleStatuses[statusIndex]
                    if newStatus != status {
                        status = newStatus
                    }
                }
            }
            var possibleStatuses = [Status.none] {
                didSet {
                    if let newIndex = possibleStatuses.firstIndex(of: status) {
                        if newIndex != statusIndex {
                            statusIndex = newIndex
                        }
                    } else {
                        statusIndex = 0
                    }
                }
            }
        }
                
        enum Status: CustomStringConvertible, Hashable {
            case none
            case defend
            case hardCounter(Squad)
            case softCounter(Squad)
            case gpCounter(Squad)
            
            var squad: Squad? {
                switch self {
                case .none, .defend: return nil
                case .hardCounter(let squad), .softCounter(let squad), .gpCounter(let squad): return squad
                }
            }

            var description: String {
                switch self {
                case .none:
                    return "None"
                case .defend:
                    return "Defend"
                    
                case .hardCounter(let squad):
                    return "Attack (hard) \(squad.name) \(NumberFormatter.integer.string(from: squad.gp))"

                case .softCounter(let squad):
                    return "Attack (soft) \(squad.name) \(NumberFormatter.integer.string(from: squad.gp))"

                case .gpCounter(let squad):
                    return "Attack (gp) \(squad.name) \(NumberFormatter.integer.string(from: squad.gp))"
                }
            }
            
            func replace(squad: Squad) -> Status {
                switch self {
                case .none, .defend:
                    fatalError()
                case .hardCounter:
                    return .hardCounter(squad)
                case .softCounter:
                    return .softCounter(squad)
                case .gpCounter:
                    return .gpCounter(squad)
                }
            }
        }
        
        struct SquadPair : Hashable {
            let index: Int
            let squad1: Squad?
            let squad2: Squad?
        }
        
        var squadPairs: [SquadPair] {
            zip(squads1, squads2)
        }
        
        var leftovers: [SquadPair] {
            zip(
                squads1.filter { $0.status == .none },
                squads2.filter { $0.status == .none }
            )
        }
        
        func zip(_ left: [Entry], _ right: [Entry]) -> [SquadPair] {
            var result = [SquadPair]()
            
            for index in 0 ..< max(left.count, right.count) {
                result.append(SquadPair(
                    index: index,
                    squad1: index < left.count ? left[index].squad : nil,
                    squad2: index < right.count ? right[index].squad : nil
                ))
            }
            
            return result
        }
                
        // squads that are this much more powerful can potentially win even if not a counter
        let overkillFactor = 1.2
        let hardCounterFactor = 1.3
        let softCounterFactor = 1.1

        /// teams that the given team can beat
        func targets(attacker: Squad, opponent: [GACModel.Entry], onlyDefenders: Bool) -> [Status] {
            var result = [Status]()
            
            // teams that this is a hard or soft counter for
            let hardCounters = Set(attacker.attackTeam?.reverseCounters().filter { $0.hard }.map { $0.team.id } ?? [])
            let softCounters = Set(attacker.attackTeam?.reverseCounters().filter { !$0.hard }.map { $0.team.id } ?? [])
            
            for entry in opponent {
                if onlyDefenders && entry.status != .defend {
                    continue
                }
                
                let squad = entry.squad
                var added = false
                
                if let id = squad.properties.defenderId {
                    if hardCounters.contains(id) {
                        let effectiveGP = Int(Double(attacker.gp) * hardCounterFactor)
                        if effectiveGP >= squad.gp {
                            result.append(.hardCounter(squad))
                            added = true
                        }
                    } else if softCounters.contains(id) {
                        let effectiveGP = Int(Double(attacker.gp) * softCounterFactor)
                        if effectiveGP >= squad.gp {
                            result.append(.softCounter(squad))
                            added = true
                        }
                    }
                }
                if !added {
                    // if we are high enough gp then we can probably still take them
                    let effectiveGP = Int(Double(attacker.gp) / overkillFactor)
                    if effectiveGP >= squad.gp {
                        result.append(.gpCounter(squad))
                    }
                }
            }
            
            return result
        }
        
        /// teams that can beat the given squad
        func counters(defender: Squad, opponent: [GACModel.Entry]) -> [Status] {
            for entry in opponent {
                if entry.status.squad == defender {
                    // we already have a squad assigned to beat this
                    return []
                }
            }
            
            var result = [Status]()
            
            // teams that this is a hard or soft counter for
            let hardCounters = Set(defender.counterTeam?.counters().filter { $0.hard }.map { $0.counter.id } ?? [])
            let softCounters = Set(defender.counterTeam?.counters().filter { !$0.hard }.map { $0.counter.id } ?? [])
            
            for entry in opponent {
                if entry.status != .none {
                    continue
                }

                let squad = entry.squad
                var added = false
                
                if let id = squad.properties.attackerId {
                    if hardCounters.contains(id) {
                        let effectiveGP = Int(Double(squad.gp) * hardCounterFactor)
                        if effectiveGP >= defender.gp {
                            result.append(.hardCounter(squad))
                            added = true
                        }
                        added = true
                    } else if softCounters.contains(id) {
                        let effectiveGP = Int(Double(squad.gp) * softCounterFactor)
                        if effectiveGP >= defender.gp {
                            result.append(.softCounter(squad))
                            added = true
                        }
                    }
                }
                if !added {
                    let effectiveGP = Int(Double(squad.gp) / overkillFactor)
                    if effectiveGP >= defender.gp {
                        result.append(.gpCounter(squad))
                    }
                }
            }
            
            return result
        }

        
        func setPlayers(player1: DataFile<Model.Player>, player2: DataFile<Model.Player>) {
            self.player1 = try! player1.value()
            self.squads1 = try! player1.squadEvaluations().await().values.flatMap { $0 }.map { $0.squad }.filter { !$0.isFleet }.sorted {
                $0.gp > $1.gp
            }.map { Entry(squad: $0, status: .none) }
            
            self.player2 = try! player2.value()
            self.squads2 = try! player2.squadEvaluations().await().values.flatMap { $0 }.map { $0.squad }.filter { !$0.isFleet }.sorted {
                $0.gp > $1.gp
            }.map { Entry(squad: $0, status: .none) }
                        
            rebuildSquads1Statuses()
            rebuildSquads2Statuses()
        }
    }

    @ObservedObject var model: GACModel
    
    @State var showDetails = false
    
    func bgColor(entry: GACModel.Entry, opposite: [GACModel.Entry]) -> Color {
        if entry.status == .defend {
            if opposite.contains(where: { $0.status.squad == entry.squad }) {
                // we have an allocated attacker -- done
                return Color(NSColor.highlightColor.shadow(withLevel: 0.7)!)
            } else {
                // we need an attacker assigned
                return Color(NSColor.highlightColor)
            }
        }
        
        if entry.status == .none && entry.possibleStatuses.count > 2 {
            // this squad can attack something
            return Color(NSColor.selectedTextBackgroundColor)
        }
        
        if entry.status != .none && entry.status != .defend {
            // we are attacking, done!
            return Color(NSColor.highlightColor.shadow(withLevel: 0.7)!)
        }
        
        return Color.clear
    }
    
    func bgColorLeft(index: Int) -> Color {
        if index < model.squads1.count {
            return bgColor(entry: model.squads1[index], opposite: model.squads2)
        } else {
            return Color.clear
        }
    }
    
    func bgColorRight(index: Int) -> Color {
        if index < model.squads2.count {
            return bgColor(entry: model.squads2[index], opposite: model.squads1)
        } else {
            return Color.clear
        }
    }
    
    var body: some View {
        VStack {
            HStack {
                Toggle("Details", isOn: $showDetails)
                
            }
            ScrollView(.vertical, showsIndicators: true) {
                VStack {
                    ForEach(model.squadPairs, id: \.self) { pair in
                        HStack {
                            if pair.squad1 != nil {
                                TeamView(entry: self.$model.squads1[pair.index], opponents: self.model.squads2, model: self.model, showDetails: self.showDetails, assignOpponent: { self.assignOpponent(squad: $0, status: $1, modifyLeft: false) })
                                    .frame(width: 400)
                                    .background(self.bgColorLeft(index: pair.index))
                            } else {
                                Text("")
                                .frame(width: 400)
                            }

                            if pair.squad2 != nil {
                                TeamView(entry: self.$model.squads2[pair.index], opponents: self.model.squads1, model: self.model, showDetails: self.showDetails, assignOpponent: { self.assignOpponent(squad: $0, status: $1, modifyLeft: true) })
                                    .frame(width: 400)
                                    .background(self.bgColorRight(index: pair.index))
                            } else {
                                Text("")
                                .frame(width: 400)
                            }
                        }
                    }
                }
                .frame(width: 850)
            }
        }
    }
    
    func assignOpponent(squad: Squad, status: GACModel.Status, modifyLeft: Bool) {
        if let index = (modifyLeft ? model.squads1 : model.squads2).firstIndex(where: { $0.squad == squad }) {
            if modifyLeft {
                model.squads1[index].status = status
            } else {
                model.squads2[index].status = status
            }
        }
    }
    
    struct TeamView : View {
        @Binding var entry: GACModel.Entry
        let opponents: [GACModel.Entry]
        let model: GACModel
        
        let showDetails: Bool
        
        let assignOpponent: (Squad, GACModel.Status) -> Void
        
        var body: some View {
            let attackers = self.model.counters(defender: entry.squad, opponent: opponents)
            let targets = self.model.targets(attacker: entry.squad, opponent: opponents, onlyDefenders: false)
            
            return HStack(alignment: .center) {
                Text(NumberFormatter.integer.string(from: entry.squad.gp))
                    .frame(width: 50)
                    .font(Font.system(size: 10))

                if showDetails {
                    VStack(alignment: .leading) {
                        SquadView(squad: entry.squad)
                        
                        Group {
                            if (entry.status == .none || entry.status == .defend) && !attackers.isEmpty {
                                Text("Attackers (might beat this)").bold()
                                ForEach(attackers, id: \.self) { attacker in
                                    Text(attacker.description)
                                        .onTapGesture {
                                            self.assignOpponent(attacker.squad!, attacker.replace(squad: self.entry.squad))
                                        }
                                }
                            }

                            if entry.status != .defend && !targets.isEmpty {
                                Text("Targets (might beat)").bold()
                                ForEach(targets, id: \.self) { target in
                                    Text(target.description)
                                        .onTapGesture {
                                            self.entry.status = target
                                        }
                                }
                            }
                        }
                        .frame(width: 200, alignment: Alignment.leading)
                        .fixedSize()
                        .font(Font.system(size: 12))
                    }
                } else {
                    SquadView(squad: entry.squad)
                }

                VStack {
                    StatusPicker(entry: $entry)
                }
            }
        }
    }
    
    struct StatusPicker : View {
        @Binding var entry: GACModel.Entry
                
        var body: some View {
            Picker(selection: $entry.statusIndex, label: EmptyView()) {
                ForEach(0 ..< self.entry.possibleStatuses.count, id: \.self) { index in
                    Text(self.entry.possibleStatuses[index].description).tag(index)
                }
            }
        }
    }
}

struct SquadView : View {
    let squad: Squad
    
    var body: some View {
        HStack {
            if squad.isFleet {
                Text("\(squad.description()) \(squad.gp)").font(Font.system(size: 11))
            } else {
                ForEach(squad.units, id: \.self) { unit in
                    VStack {
                        unit.icon(size: 32, borderWidth: 1)
                            .frame(width: 40)
                        Text("\(unit.zetasDescription) \(unit.gearLevelDescription)")
                            .font(Font.system(size: 8))
                    }
                }
            }
        }
        .layoutPriority(-1)
    }
}
