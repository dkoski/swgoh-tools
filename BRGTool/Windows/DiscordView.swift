//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import SWGOH

class DiscordViewModel : NSObject, ObservableObject {
    
    @Published var discord: DiscordModel

    @Published var guild: DataFile<Model.Guild>? = DataFileManager.defaultGuild {
        didSet {
            if let guild = try? guild?.value() {
                players = Dictionary(uniqueKeysWithValues: guild.players.map { ($0.stats.allyCode, $0) })
            } else {
                players.removeAll()
            }
            
            updateAllyCodes()
        }
    }
    
    @Published var players = [Int:Model.Player]()
    
    @Published var allyCodes = [Int]()
    
    func updateAllyCodes() {
        var allyCodes = Set(players.keys)
        allyCodes.formUnion(discord.mentions.keys)
        self.allyCodes = allyCodes.sorted { (lhs, rhs) -> Bool in
            if let lhs = players[lhs] {
                if let rhs = players[rhs] {
                    return lhs.name.lowercased() < rhs.name.lowercased()
                } else {
                    return true
                }
            } else {
                if let _ = players[rhs] {
                    return false
                } else {
                    return lhs < rhs
                }
            }
        }
    }
    
    override init() {
        discord = (try? DataFileManager.discordMentions?.value()) ?? DiscordModel()
        super.init()
        updateAllyCodes()
    }
    
}

struct DiscordView: View {

    @EnvironmentObject var model: DiscordViewModel
    
    @State var saveQueue = DispatchQueue(label: "DiscordView.save")
    
    @State var text = ""
    
    var body: some View {
        VStack {
            VStack {
                ModelSelector(selection: $model.guild, dataFileManager: DataFileManager.guilds)

                HStack {
                    Text("Select Numbers table with ally code and @ mention and paste to import.")
                    Button("Remove All") {
                        self.model.discord.mentions.removeAll()
                    }
                }
            }
            .padding()

            Divider()

            ScrollView(.vertical) {
                VStack {
                    ForEach(model.allyCodes, id: \.self) { allyCode in
                        HStack {
                            Text("\(self.model.players[allyCode]?.name ?? "")")
                                .frame(width: 100)
                            Text(allyCode.asAllyCode)
                                .frame(width: 100)
                            TextField("Mention", text: self.$model.discord[allyCode])
                                .frame(width: 200)

                            IconButton(.cancelCircle) {
                                self.model.discord.mentions.removeValue(forKey: allyCode)
                            }
                        }
                    }
                }
                .padding()
            }
        }
        .frame(width: 500, height: 700)
        .onReceive(model.$discord.debounce(for: 0.5, scheduler: DispatchQueue.main)) { _ in
            self.saveQueue.async {
                _ = try? DataFileManager.discord.save(dataFile: DataFile(discord: self.model.discord))
            }
        }
    }
}

