//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Combine
import SWGOH

/// controls for filtering the visible squads
struct TWFilterControl : View {
    
    let config: EditableTWConfiguration
    let schedule: TWE.Schedule
    
    @Binding var filteredIndices: [Int]

    @State var selectedFilter: Filter? = defaultFilter
    
    static let defaultFilter = Filter("All", { _ in true })

    func filters() -> [Filter] {
        var result = [Filter]()
        
        result.append(Filter("All", { _ in true }))
        result.append(Filter("All Defense", { $0.isEnabled }))
        result.append(Filter("All Offense", { !$0.isEnabled }))
        result.append(Filter("Fleet", { $0.isFleet }))

        result.append(Filter("Backfill", { $0.isEnabled && $0.allowRandomFill }))
        result.append(Filter("Placed Only", { $0.isEnabled && !$0.allowRandomFill && $0.count > 0 }))
        result.append(Filter("Not Placed", { $0.isEnabled && $0.sections.count == 0 }))
        result.append(Filter("Multiple Sections", { $0.isEnabled && $0.sections.count > 1 }))

        let available = Set(schedule.remainingCounts.filter { $0.value > 0 }.map { $0.key })
        result.append(Filter("Has Available", { $0.isEnabled && available.contains($0.squadName) }))

        result.append(Filter("Top", { $0.isEnabled && !Set(TW.topSections).isDisjoint(with: $0.sections) }))
        result.append(Filter("Bottom", { $0.isEnabled && !Set(TW.bottomSections).isDisjoint(with: $0.sections) }))
        result.append(Filter("Front", { $0.isEnabled && !Set(TW.frontSections).isDisjoint(with: $0.sections) }))
        result.append(Filter("Middle", { $0.isEnabled && !Set(TW.middleSections).isDisjoint(with: $0.sections) }))
        result.append(Filter("Back", { $0.isEnabled && !Set(TW.backSections).isDisjoint(with: $0.sections) }))
        
        for section in TW.allGroundSections {
            result.append(Filter(section, { $0.isEnabled && $0.sections.contains(section) }))
        }
        
        // This variant does all the squads
        // for squadName in Set(TW.keySquads.map(TW.canonicalName)).sorted() {
        
        for squadName in TW.keySquads.sorted() {
            result.append(Filter(squadName, { TW.canonicalName($0.squadName) == squadName }))
        }
        
        return result
    }
    
    func apply() -> [Int] {
        let filter = self.selectedFilter ?? TWFilterControl.defaultFilter
        return config.instructions.enumerated().filter { filter.filter($0.1) }.map { $0.0 }
    }
    
    var body: some View {
        ComboBox(list: filters(), required: true, selection: $selectedFilter, onChange: {
            self.filteredIndices = self.apply()
        })
            .onAppear {
                // when it first appears we need to populate the filteredIndices
                self.filteredIndices = self.apply()
            }
    }

    class Filter : CustomStringConvertible, Hashable, NSCopying {
        let name: String
        let filter: (EditableTWConfiguration.Instruction) -> Bool
        
        init(_ name: String, _ filter: @escaping (EditableTWConfiguration.Instruction) -> Bool) {
            self.name = name
            self.filter = filter
        }
        
        var description: String { name }
        
        func copy(with zone: NSZone? = nil) -> Any {
            self
        }
        
        func hash(into hasher: inout Hasher) {
            hasher.combine(name)
        }
        
        static func == (lhs: TWFilterControl.Filter, rhs: TWFilterControl.Filter) -> Bool {
            lhs.name == rhs.name
        }
    }
}

struct TWDefendersRuleEditorView : View {
    
    let players: [Model.Player]
    let squads: [String]
    
    @Binding var defensivePlayerTeamCount: Int
    @Binding var defensivePlayers: Set<Model.Player>
    @Binding var defensivePlayerTeams: Set<String>

    var body: some View {
        return VStack {
            Text("Defensive Players").bold()
            Divider()
            
            Text("Some players primarily want to defend.  Select the players and the allowed squads.").frame(alignment: Alignment.topLeading)
            
            HStack {
                Text("Squads per player: ")
                TextField("# of squads", value: $defensivePlayerTeamCount, formatter: NumberFormatter.generic)
            }
            HStack {
                VStack {
                    Text("Defense Players")
                    List(players, id: \.self, selection: $defensivePlayers) { player in
                        Text("\(player.name) - \(player.stats.allyCode.asAllyCode)")
                    }
                    .frame(minHeight: 200)
                }
                VStack {
                    Text("Allowed Squads")
                    List(squads, id: \.self, selection: $defensivePlayerTeams) { team in
                        Text(team)
                    }
                    .frame(minHeight: 200)
                }
            }
        }
    }
}

struct TWAttackersRuleEditorView : View {
    
    let players: [Model.Player]

    @Binding var offensivePlayerTeamCount: Int
    @Binding var offensivePlayers: Set<Model.Player>

    var body: some View {
        return VStack {
            Text("Offensive Players").bold()
            Divider()
            
            Text("Some players excel on offense.  You can limit the number of defense teams for these players.").frame(alignment: Alignment.topLeading)

            HStack {
                Text("Squads per player: ")
                TextField("# of squads", value: $offensivePlayerTeamCount, formatter: NumberFormatter.generic)
            }
            HStack {
                VStack {
                    Text("Offense Players")
                    List(players, id: \.self, selection: $offensivePlayers) { player in
                        Text("\(player.name) - \(player.stats.allyCode.asAllyCode)")
                    }
                    .frame(minHeight: 300)
                }
            }
        }
    }
}

struct TWSquadSelectionView : View {
    
    @ObservedObject var model: TWE.OrdersModel
    let schedule: TWE.Schedule
    
    @State var filteredIndices = [Int]()
    
    @State var detailState = 0
        
    struct Counts {
        let available: Int
        let filtered: Int
        let placed: Int
        let remaining: Int
    }
    
    func counts(_ instruction: EditableTWConfiguration.Instruction) -> Counts {
        let squadName = instruction.squadName
        
        let available = self.model.inventory[squadName, default: []].count

        let min = instruction.minimumGP
        let max = instruction.maximumGP
        let filtered = model.inventory[squadName, default: []].filter { $0.gp >= min && $0.gp <= max }.count
        
        let remaining = schedule.remainingCounts[squadName, default: available] - (available - filtered)
        let placed = filtered - remaining
        
        return Counts(available: available, filtered: filtered, placed: placed, remaining: remaining)
    }
    
    func filteredInstructions() -> [(Int, EditableTWConfiguration.Instruction)] {
        filteredIndices.map { ($0, model.config.instructions[$0] )}
    }

    var body: some View {
        let filteredInstructions = self.filteredInstructions()
        return VStack {
            Text("Select which squads will participate in defense.  You can drag the squads to change their priority in placement.")
            
            HStack {
                TWFilterControl(config: model.config, schedule: schedule, filteredIndices: $filteredIndices)
                RowDetailControls(detailState: $detailState)
            }
            
            Divider()
            
            List() {
                ForEach(filteredInstructions, id: \.self.1) { (index, instruction) in
                    Row(squad: self.model.squads[instruction.squadName],
                        counts: self.counts(instruction),
                        detailState: DetailState(rawValue: self.detailState)!,
                        instruction: self.$model.config.instructions[index])
                }
                .onMove { (source: IndexSet, destination: Int) in
                    // I have to manually send this -- for some reason the move() doesn't signal a mutation
                    self.model.objectWillChange.send()
                    
                    // remap the indices back through the filteredInstructions (captured)
                    self.model.config.instructions.move(fromOffsets: IndexSet(source.map { filteredInstructions[$0].0 }), toOffset: filteredInstructions[destination].0)
                    
                    // update the indices to match
                    let squadNames = Set(filteredInstructions.map { $0.1.squadName })
                    self.filteredIndices = self.model.config.instructions.enumerated().filter { squadNames.contains($0.1.squadName) }.map { $0.0 }
                }
            }
            .frame(minHeight: 450, maxHeight: .infinity)
                
            TWMapView(summary: self.schedule.summary, target: self.model.squadsPerSection)
                .frame(minHeight: 100, maxHeight: 500, alignment: .top)
                .padding()
        }
    }
    
    enum DetailState : Int {
        case sections
        case weights
        case ranges
        case counts
    }

    struct RowDetailControls : View {
        
        @Binding var detailState: Int
        
        var body: some View {
            Picker("", selection: $detailState) {
                Text("Sections").tag(DetailState.sections.rawValue)
                Text("Weights").tag(DetailState.weights.rawValue)
                Text("Ranges").tag(DetailState.ranges.rawValue)
                Text("Counts").tag(DetailState.counts.rawValue)
            }
            .pickerStyle(SegmentedPickerStyle())
        }
        
    }
    
    struct Row : View {
        
        let squad: Squad?
        let counts: Counts
        let detailState: DetailState

        @Binding var instruction: EditableTWConfiguration.Instruction
        
        @State var f = 10.0
        
        var body: some View {
            HStack {
                VStack {
                    HStack {
                        // use this as a drag indicator
                        Image(nsImage: NSImage(imageLiteralResourceName: NSImage.listViewTemplateName))
                        
                        if squad != nil {
                            Button(action: {
                                self.$instruction.isEnabled.wrappedValue.toggle()
                            }) {
                                squad!.leaderIcon(borderColor: self.instruction.isEnabled ? Color.yellow : Color.blue)
                                    .tooltip("Click to \(self.instruction.isEnabled ? "disable" : "enable")")
                            }
                            .buttonStyle(PlainButtonStyle())
                        } else {
                            Toggle("", isOn: $instruction.isEnabled)
                        }
                    }
                    Text(instruction.squadName).font(Font.system(size: 10))
                }
                .frame(width: 80)
                
                VStack {
                    Text("Available: \(counts.available)")
                    Text("Filtered: \(counts.filtered)")
                    Text("Placed: \(counts.placed)")
                    Text("Remaining: \(counts.remaining)")
                }
                .frame(width: 100, alignment: .leading)
                .font(Font.system(size: 10))
                
                if detailState == .sections {
                    if instruction.isFleet {
                        Toggle("F2", isOn: $instruction.isF2).toggleStyle(SectionToggleStyle())
                        Toggle("F1", isOn: $instruction.isF1).toggleStyle(SectionToggleStyle())
                        
                    } else {
                        VStack(spacing: 4) {
                            HStack {
                                Toggle("T4", isOn: $instruction.isT4).toggleStyle(SectionToggleStyle())
                                Toggle("T3", isOn: $instruction.isT3).toggleStyle(SectionToggleStyle())
                                Toggle("T2", isOn: $instruction.isT2).toggleStyle(SectionToggleStyle())
                                Toggle("T1", isOn: $instruction.isT1).toggleStyle(SectionToggleStyle())
                            }
                            HStack {
                                Toggle("B4", isOn: $instruction.isB4).toggleStyle(SectionToggleStyle())
                                Toggle("B3", isOn: $instruction.isB3).toggleStyle(SectionToggleStyle())
                                Toggle("B2", isOn: $instruction.isB2).toggleStyle(SectionToggleStyle())
                                Toggle("B1", isOn: $instruction.isB1).toggleStyle(SectionToggleStyle())
                            }
                        }
                    }
                } else if detailState == .weights {
                    VStack {
                        HStack {
                            WeightSlider("T4", self.$instruction.weightT4)
                            WeightSlider("T3", self.$instruction.weightT3)
                            WeightSlider("T2", self.$instruction.weightT2)
                            WeightSlider("T1", self.$instruction.weightT1)
                        }
                        .padding(EdgeInsets(top: 2, leading: 3, bottom: 2, trailing: 5))
                        
                        HStack {
                            WeightSlider("B4", self.$instruction.weightB4)
                            WeightSlider("B3", self.$instruction.weightB3)
                            WeightSlider("B2", self.$instruction.weightB2)
                            WeightSlider("B1", self.$instruction.weightB1)
                        }
                        .padding(EdgeInsets(top: 2, leading: 3, bottom: 2, trailing: 5))
                    }
                    .tooltip("Placement weight")

                } else if detailState == .ranges {
                    VStack(alignment: .leading) {
                        HStack {
                            Text("Minimum GP:")
                            IntSlider(value: $instruction.minimumGP, in: instruction.minumimGPRange)
                        }
                        .frame(width: 500)

                        HStack {
                            Text("Maximum GP:")
                            IntSlider(value: $instruction.maximumGP, in: instruction.maximumGPRange)
                        }
                        .frame(width: 500)
                    }
                } else if detailState == .counts {
                    HStack {
                        HStack {
                            Text("Place:")
                            IntSlider(value: $instruction.count, in: 0 ... 75)
                        }
                        .frame(width: 250)

                        Spacer().frame(width: 50)
                        
                        Toggle("Place remaining squads to fill gaps", isOn: $instruction.allowRandomFill)
                        
                        Spacer().frame(width: 50)
                        Toggle("Place in order", isOn: $instruction.placeInOrder)
                    }

                }
            }
        }
        
        static func bgColor(_ bool: Bool) -> Color {
            bool ? Color(NSColor.selectedTextBackgroundColor) : Color(NSColor.textBackgroundColor)
        }

        static func fgColor(_ bool: Bool) -> Color {
            bool ? Color(NSColor.selectedTextColor) : Color(NSColor.textColor)
        }
        
        struct SectionToggleStyle : ToggleStyle {

            func makeBody(configuration: Configuration) -> some View {
                configuration.label
                    .background(bgColor(configuration.isOn))
                    .foregroundColor(fgColor(configuration.isOn))
                    .onTapGesture { configuration.isOn.toggle() }
            }
        }
        
        struct WeightSlider : View {
            let label: String
            var value: Binding<Int>
            
            init(_ label: String, _ value: Binding<Int>) {
                self.label = label
                self.value = value
            }
            
            var body: some View {
                Stepper("\(label)  \(value.wrappedValue)", value: value, in: 0 ... 9)
                    .frame(width: 55)
                .background(bgColor(value.wrappedValue > 0))
                .foregroundColor(fgColor(value.wrappedValue > 0))
            }
        }

    }
    
}
