//
//  Copyright © 2021 David Koski. All rights reserved.
//

import SwiftUI
import SWGOH

struct TWCountsEditor: View {
    
    @Binding var config: EditableTWConfiguration
    
    let players: [Model.Player]
    
    private let columns: [GridItem] = Array(repeating: .init(.flexible()), count: 3)

    func thresholdRow(_ counts: Binding<EditableTWConfiguration.SquadCounts>) -> some View {
        HStack {
            Text("GP ≤")
            TextField("Threshold", value: counts.threshold, formatter: NumberFormatter.integer)
            
            Text("Squads")
            TextField("Squads", value: counts.counts.named, formatter: NumberFormatter.integer)
            
            Text("Fluff")
            TextField("Fluff", value: counts.counts.fluff, formatter: NumberFormatter.integer)

            Text("Fleets")
            TextField("Fleets", value: counts.counts.fleet, formatter: NumberFormatter.integer)
        }
    }
    
    func overrideBinding(_ player: Model.Player) -> Binding<String> {
        Binding {
            if let v = config.squadCountOverrides[player] {
                return v.description
            } else {
                return ""
            }
        } set: { newValue in
            if let v = Int(newValue) {
                config.squadCountOverrides[player] = v
            } else {
                config.squadCountOverrides[player] = nil
            }
        }
    }
        
    var body: some View {
        LabelRow(label: "Squad Counts") {
            VStack {
                ForEach(0 ..< config.squadCounts.count) { index in
                    self.thresholdRow($config.squadCounts[index])
                }
            }
        }
                
        LabelRow(label: "Overrides") {
            ScrollView {
                LazyVGrid(columns: columns) {
                    ForEach(players, id: \.stats.allyCode) { player in
                        Text("\(player.name)")
                        
                        Text("\(player.stats.gp.millions)")
                        
                        TextField("", text: overrideBinding(player))
                    }
                }
            }
            .frame(maxWidth: .infinity)
        }

    }
}

struct TWHoldbackEditor: View {
    
    @Binding var config: EditableTWConfiguration
    
    let players: [Model.Player]
    let evaluations: [Model.Player: [SquadEvaluation]]
    
    @State private var selectedPlayer: Model.Player
    
    internal init(config: Binding<EditableTWConfiguration>, players: [Model.Player], evaluations: [Model.Player : [SquadEvaluation]]) {
        print("init")
        self._config = config
        self.players = players
        self.evaluations = evaluations
        self._selectedPlayer = State(initialValue: players[0])
    }
    
    var holdbacks: [(String, Model.Player, String)] {
        config.holdbacks
            .flatMap { (player, squadNames) in
                squadNames.map { (player, $0) }
            }
            .sorted { h1, h2 in
                if h1.0 == h2.0 {
                    return h1.1 < h2.1
                }
                return h1.0.name < h2.0.name
            }
            .map { player, squadName in
                ("\(player.stats.allyCode)-\(squadName)", player, squadName)
            }
    }
        
    private let holdbackColumns: [GridItem] = Array(repeating: .init(.flexible()), count: 3)

    private let configureColumns: [GridItem] = Array(repeating: .init(.flexible()), count: 2)

    var body: some View {
        LabelRow(label: "Holdbacks") {
            ScrollView {
                LazyVGrid(columns: holdbackColumns) {
                    ForEach(holdbacks, id: \.0) { (_, player, squadName) in
                        Text("\(player.name)")
                        Text("\(squadName)")
                        Button(action: { remove(player, squadName) }) {
                            Label("Remove", image: "trash")
                        }
                    }
                }
            }
            .frame(maxWidth: .infinity)
        }
        
        LabelRow(label: "Setup") {
            VStack {
                Picker("Player:", selection: $selectedPlayer) {
                    ForEach(players) { player in
                        Text(player.name).tag(player)
                    }
                }

                ScrollView {
                    LazyVGrid(columns: configureColumns) {
                        ForEach(evaluations[selectedPlayer, default: []], id: \.self) { ev in
                            Text(ev.squad.name)
                            Group {
                                if config.holdbacks[selectedPlayer, default: []].contains(ev.squad.name) {
                                    Button(action: { remove(selectedPlayer, ev.squad.name) }) {
                                        Image(systemName: "trash")
                                    }
                                } else {
                                    Button(action: { add(selectedPlayer, ev.squad.name) }) {
                                        Image(systemName: "plus.square.fill")
                                    }
                                }
                            }
                        }
                    }
                }
                .frame(maxWidth: .infinity)
            }
        }
    }
    
    func remove(_ player: Model.Player, _ squadName: String) {
        config.holdbacks[player, default: []].remove(squadName)
    }
    
    func add(_ player: Model.Player, _ squadName: String) {
        config.holdbacks[player, default: []].insert(squadName)
    }
}
