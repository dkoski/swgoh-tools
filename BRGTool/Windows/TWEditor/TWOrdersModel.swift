//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
import Combine
import SWGOH

struct TWE {
    
}

extension TWE {
        
    /// model for managing evaluations and orders
    class OrdersModel : NSObject, ObservableObject {
        
        /// which set of rules to use when building squads
        @Published var squadRuleName: String = TW.defaultSquadRuleName
        
        @Published var evaluations: [Model.Player: [SquadEvaluation]] = [:] {
            didSet {
                // summarize the data
                inventory.removeAll()
                for evaluations in evaluations.values {
                    for evaluation in evaluations {
                        inventory[evaluation.squad.name, default: []].append(evaluation.squad)
                    }
                }
                
                // sort them in descending order
                for key in inventory.keys {
                    inventory[key] = inventory[key]?.sorted { $0.gp > $1.gp }
                }
                
                // example squads
                squads = inventory.mapValues { $0[0] }
                
                // prepare the example squads
                var squadNames = Set<String>()
                for evaluations in evaluations.values {
                    for evaluation in evaluations {
                        let name = evaluation.squad.name
                        squadNames.insert(name)
                    }
                }
                self.squadNames = squadNames.sorted()
                
                // list of players
                players = evaluations.keys.sorted()
                
                // set up the not playing set
                let savedNotPlaying = Set(Defaults.shared.twNotPlaying)
                if !savedNotPlaying.isEmpty {
                    self.notPlaying = Set(evaluations.keys.filter { savedNotPlaying.contains($0.stats.allyCode) })
                }

                config.evaluationsDidChange(inventory: self.inventory)
            }
        }
        
        /// names of available squads
        @Published var squadNames = [String]()
        
        /// example squads -- good for getting images for the squad
        @Published var squads = [String:Squad]()

        /// how many squads you start with
        @Published var inventory = [String:[Squad]]()

        /// how many squads per section
        @Published var squadsPerSection = 25
        
        /// players in the guild
        @Published var players = [Model.Player]()
        
        @Published var notPlaying = Set<Model.Player>() {
            didSet {
                if oldValue != notPlaying {
                    Defaults.shared.twNotPlaying = notPlaying.map { $0.stats.allyCode }
                }
            }
        }

        @Published var config = EditableTWConfiguration(name: "Not Set") {
            didSet {
                if !config.hasInventory {
                    config.evaluationsDidChange(inventory: self.inventory)
                }
            }
        }
    }
    
    /// a snapshot of the key pieces of the OrdersModel required to generate a Schedule
    struct Snapshot {
        /// the compiled TWConfig
        let config: TWConfig
        let squadsPerSection: Int
        
        let evaluations: [Model.Player: [SquadEvaluation]]
        let inventory: [String: [Squad]]
        
        init(ordersModel: OrdersModel) {
            self.config = ordersModel.config.compile()
            self.squadsPerSection = ordersModel.squadsPerSection
            
            var evaluations = ordersModel.evaluations
            for player in ordersModel.notPlaying {
                evaluations[player] = nil
            }
            self.evaluations = evaluations
            
            self.inventory = ordersModel.inventory
        }
        
        func prepare(trace: TWTrace = TWTrace()) -> TWE.Schedule {
            let scheduler = RandomScheduler3()
            RandomScheduler3.resetSeed()
            
            let schedule = scheduler.schedule(data: evaluations, config: config, squadsPerSection: squadsPerSection, trace: trace)
            let orders = schedule.placedSquads
            
            // count them up
            var summary = [String:[String:Int]]()            
            for (_, evaluation, section) in orders {
                summary[section, default: [:]][evaluation.squad.name, default:0] += 1
            }
            
            // figure out how many are left
            var remainingCounts = CountedSet<String>()
            for (_, evaluation) in schedule.remainder {
                remainingCounts.add(evaluation.squad.name)
            }
            
            return TWE.Schedule(summary: summary, remainingCounts: remainingCounts.asDictionary, orders: orders, remainder: schedule.remainder)
        }
    }
    
    /// The output of scheduling the squads.  Not that this is a class so that it can be captured weakly when pushing values to it.
    class Schedule : ObservableObject {
        
        /// map of squad counts per section (section -> squad -> count)
        @Published
        var summary: [String:[String:Int]]
        
        /// how many squads remain of each type
        @Published
        var remainingCounts: [String:Int]
        
        /// the detailed orders produced
        @Published
        var orders: [(Model.Player, SquadEvaluation, String)]

        /// the actual squads left over
        @Published var remainder: [(Model.Player, SquadEvaluation)]
        
        internal init(summary: [String : [String : Int]] = [String:[String:Int]](), remainingCounts: [String : Int] = [String:Int](), orders: [(Model.Player, SquadEvaluation, String)] = [(Model.Player, SquadEvaluation, String)](), remainder: [(Model.Player, SquadEvaluation)] = []) {
            self.summary = summary
            self.remainingCounts = remainingCounts
            self.orders = orders
            self.remainder = remainder
        }
        
        func assign(other: Schedule) {
            self.summary = other.summary
            self.remainingCounts = other.remainingCounts
            self.orders = other.orders
            self.remainder = other.remainder
        }
        
        var mentions: [Int:String] {
            var mentions = siteTWMentions
            if let moreMentions = try? DataFileManager.discordMentions?.value().mentions {
                for (code, value) in moreMentions {
                    mentions[code] = value
                }
            }
            return mentions
        }
        
        /// convert the orders into text that can be pasted into discord
        func ordersText(anyPlacedFleet: String, details: Bool) -> String {
            return TWPlayerReport.report(schedule: orders, mentions: self.mentions, squadFormatter: details ? TWPlayerReport.detailSquadFormatter : TWPlayerReport.standardSquadFormatter)
        }
        
        func remainderByPlayerText() -> String {
            return twRemainderByPlayer(remainder: self.remainder, mentions: self.mentions)
        }
        
        func remainderByTeamText() -> String {
            return twRemainderByTeam(remainder: self.remainder, mentions: self.mentions)
        }
    }

}

