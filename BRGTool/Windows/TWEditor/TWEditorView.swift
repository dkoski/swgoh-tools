//
//  TWOrdersView.swift
//  BRGTool
//
//  Created by David Koski on 3/25/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Combine
import SWGOH

/// useful for testing -- inject tw editor environment objects into the view
public func twEditorEvironment<V: View>(_ view: V) -> some View {
    let guild = DataFileManager.defaultGuild!
    let orders = TWE.OrdersModel()
    orders.squadRuleName = TW.defaultSquadRuleName
    orders.config = try! DataFileManager.twConfigs.find(name: "realistic")!.value()
    orders.evaluations = try! guild.squadEvaluations().await()
    
    return view
        .environmentObject(orders)
        .environmentObject(TWE.Schedule())
}

private enum TWEditorState {
    case setup
    case edit(DataFile<Model.Guild>, EditableTWConfiguration)
}

struct TWView: View {
    
    @State private var state: TWEditorState = .setup

    var body: some View {
        switch state {
        case .setup:
            TWSelectInputs() { guild, config in
                state = .edit(guild, config)
            }
            .frame(minWidth: 600, maxWidth: .infinity)
            .padding()
        
        case let .edit(guild, config):
            TWEditorView(guild: guild, config: config)
        }
    }
}

/// The top level view for the TW Editor / Orders window
struct TWEditorView: View {
    
    @EnvironmentObject var waiting: WaitingModel
    
    let guild: DataFile<Model.Guild>
    let config: EditableTWConfiguration
        
    // holds all state related to preparing TW orders
    @StateObject private var model = TWE.OrdersModel()
    
    // output of scheduling the OrdersModel
    @StateObject private var schedule = TWE.Schedule()

    @State var tab: Int? = 0
    
    enum OrdersStyle : String, CaseIterable {
        case normal = "Normal"
        case detailed = "Detailed"
        case remainderTeams = "Remainder by Teams"
        case remainderPlayer = "Remainder by Player"
    }
    
    @State var orderStyle = OrdersStyle.normal
        
    var body: some View {
        WaitingView {
            VStack(alignment: .leading) {
                HStack {
                    Group {
                        Tab(index: 0, title: "Setup", defaultIndex: 0, selection: self.$tab)
                        Tab(index: 1, title: "Players", defaultIndex: 0, selection: self.$tab)
                        Tab(index: 7, title: "Counts", defaultIndex: 0, selection: self.$tab)
                        Tab(index: 8, title: "Hold", defaultIndex: 0, selection: self.$tab)
                        Tab(index: 2, title: "Squads", defaultIndex: 0, selection: self.$tab)
                        Tab(index: 3, title: "Overview", defaultIndex: 0, selection: self.$tab)
                        Tab(index: 4, title: "Orders", defaultIndex: 0, selection: self.$tab)
                        Tab(index: 5, title: "Explain", defaultIndex: 0, selection: self.$tab)
                        Tab(index: 6, title: "Debug", defaultIndex: 0, selection: self.$tab)
                    }
                    
                    Spacer()
                    
                    Picker("", selection: self.$model.squadRuleName) {
                        ForEach(TW.configurations.keys.sorted(), id: \.self) { name in
                            Text(name)
                        }
                    }
                    .onReceive(self.model.$squadRuleName
                        // skip the first notify
                        .dropFirst()
                        .receive(on: DispatchQueue.main)) { value in
                            self.prepareSquads(squadRuleName: value)
                        }
                    
                    Spacer()
                    
                    TWConfigSaveView(config: self.$model.config)
                }
                
                Divider()
                
                TabContents(index: 1, selection: self.tab) {
                    HSplitView {
                        TWDefendersRuleEditorView(players: self.model.players, squads: self.model.squadNames, defensivePlayerTeamCount: self.$model.config.defensivePlayerTeamCount, defensivePlayers: self.$model.config.defensivePlayers, defensivePlayerTeams: self.$model.config.defensivePlayerTeams)
                            .frame(minWidth: 450)
                            .padding()
                        
                        TWAttackersRuleEditorView(players: self.model.players, offensivePlayerTeamCount: self.$model.config.offensivePlayerTeamCount, offensivePlayers: self.$model.config.offensivePlayers)
                            .frame(minWidth: 450)
                            .padding()
                    }
                }
                Group {
                    TabContents(index: 0, selection: self.tab) {
                        TWSetupView(model: self.model)
                    }
                    TabContents(index: 7, selection: self.tab) {
                        TWCountsEditor(config: $model.config, players: model.players)
                    }
                    TabContents(index: 8, selection: self.tab) {
                        TWHoldbackEditor(config: $model.config, players: model.players, evaluations: model.evaluations)
                    }
                }
                TabContents(index: 2, selection: self.tab) {
                    TWSquadSelectionView(model: self.model, schedule: self.schedule)
                }
                TabContents(index: 3, selection: self.tab) {
                    TWMapView(summary: self.schedule.summary, target: self.model.squadsPerSection)
                }
                TabContents(index: 4, selection: self.tab) {
                    VStack {
                        Picker("Style: ", selection: $orderStyle) {
                            ForEach(OrdersStyle.allCases, id: \.self) { style in
                                Text(style.rawValue)
                            }
                        }
                        .frame(width: 200)
                        TextArea(text: .constant(self.orders()))
                            .frame(minWidth: 200, maxWidth: .infinity, minHeight: 200, maxHeight: .infinity)
                    }
                }
                TabContents(index: 5, selection: self.tab) {
                    TWExplainView(details: self.details())
                }
                TabContents(index: 6, selection: self.tab) {
                    TextArea(text: .constant(self.trace()))
                        .frame(minWidth: 200, maxWidth: .infinity, minHeight: 200, maxHeight: .infinity)
                }
            }
            .padding()
            
            // when something in the model changes, produce a new Schedule.  Note that this
            // is attached to part of the view that is only active when we have selected inputs --
            // we need to make sure to trigger this manually when the inputs are set
            .onReceive(self.model.objectWillChange) {
                self.rebuildSchedule()
            }
            
            .onAppear {
                self.model.config = self.config
                self.prepareSquads(squadRuleName: "default")
            }
        }
        .frame(minWidth: 1000, minHeight: 800, alignment: .topLeading)
    }
    
    @State var triggerRebuildPublisher = PassthroughSubject<Void, Never>()
    @State var scheduleBuildCancellable: AnyCancellable? = nil
    func rebuildSchedule() {
        if scheduleBuildCancellable == nil {
            let model = self.model
            let queue = DispatchQueue(label: "TWOrdersView.schedule")
            let schedule = self.schedule
            scheduleBuildCancellable = triggerRebuildPublisher
                .debounce(for: 0.1, scheduler: DispatchQueue.main)
                .map { _ in TWE.Snapshot(ordersModel: model) }
                .receive(on: queue)
                .map { payload in payload.prepare() }
                .receive(on: DispatchQueue.main)
                
                // note that I have captured the instance of Schedule here to prevent
                // retain cycle on self (e.g. no .assign(schedule))
                .sink { schedule.assign(other: $0) }
        }
        
        triggerRebuildPublisher.send()
    }
    
    @State var squadBuildCancellable: AnyCancellable? = nil
    func prepareSquads(squadRuleName: String) {
        let publisher = guild
            .squadEvaluations(name: squadRuleName)
            .share()
        
        squadBuildCancellable = publisher
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in
                self.squadBuildCancellable = nil
            }, receiveValue: { result in
                self.model.evaluations = result
                self.rebuildSchedule()
            })
        
        waiting.activity = "Preparing squads ..."
        waiting.waitFor(publisher.receive(on: DispatchQueue.main).eraseToAnyPublisher())
    }
    
    func orders() -> String {
        let details = self.orderStyle == .detailed
        let trace = TWTrace()
        trace.enabled = true
        
        let snapshot = TWE.Snapshot(ordersModel: model)
        let schedule = snapshot.prepare(trace: trace)

        switch self.orderStyle {
        case .normal, .detailed:
            return "GENERATING ORDERS FOR \(snapshot.evaluations.count) PLAYERS\n\n" + schedule.ordersText(anyPlacedFleet: self.model.config.anyPlacedFleet, details: details)
            
        case .remainderPlayer:
            return schedule.remainderByPlayerText()
            
        case .remainderTeams:
            return schedule.remainderByTeamText()
        }
    }
    
    func trace() -> String {
        let trace = TWTrace()
        trace.enabled = true
        
        let snapshot = TWE.Snapshot(ordersModel: model)
        let schedule = snapshot.prepare(trace: trace)
        
        return "GENERATING ORDERS FOR \(snapshot.evaluations.count) PLAYERS\n\n" + schedule.summary.description + "\n\n" + trace.details.map { "\($0.player.name) : \($0.squad?.name ?? "") : \($0.status)" }.joined(separator: "\n") + "\n\n" + trace.messages.joined(separator: "\n")
    }
    
    func details() -> [TWTrace.Detail] {
        let trace = TWTrace()
        trace.enabled = true
        _ = TWE.Snapshot(ordersModel: model).prepare(trace: trace)
        return trace.details
    }
}

struct TWSelectInputs : View {
    
    @State private var guild: DataFile<Model.Guild>? = DataFileManager.defaultGuild
    @State private var config: DataFile<EditableTWConfiguration>? = DataFileManager.twConfigs.values.first

    let complete: (DataFile<Model.Guild>, EditableTWConfiguration) -> Void
    
    var body: some View {
        VStack(alignment: .center) {
            
            HStack(alignment: .center) {
                Text("Select a guild: ")
                ModelSelector(selection: $guild, dataFileManager: DataFileManager.guilds)
            }
            .frame(width: 600)
            
            HStack(alignment: .center) {
                Text("and a configuration: ")
                LocalModelSelector(selection: $config, dataFileManager: DataFileManager.twConfigs, newModelFactory: { EditableTWConfiguration(name: $0) }, dataFile: { DataFile(config: $0) })
            }
            .frame(width: 600)

            Button(action: {
                if let guild = guild, let config = try? config?.value() {
                    complete(guild, config)
                }
            }) {
                Text("Prepare")
            }
            .disabled(guild == nil || config == nil)
        }
    }
}

/// display summary data (see TWE.Schedule) in a map-ish view.
struct TWMapView : View, Equatable {
    let summary: [String:[String:Int]]
    let target: Int
    
    func section(_ name: String) -> SectionView {
        SectionView(squads: summary[name, default:[:]], section: name, target: target)
    }
    
    var body: some View {
        HStack {
            VStack {
                section("F2")
                section("T4")
                section("B4")
            }
            VStack {
                section("F1")
                section("T3")
                section("B3")
            }
            VStack {
                section("T2")
                section("B2")
            }
            VStack {
                section("T1")
                section("B1")
            }
        }
        .frame(maxWidth: .infinity)
    }
    
    struct SectionView : View {
        let squads: [String:Int]
        let section: String
        let target: Int
        
        var count: Int {
            squads.values.reduce(0, +)
        }
        
        var body: some View {
            VStack {
                Text("\(section) - \(count)")
                    .bold()
                ForEach(squads.keys.sorted(), id: \.self) { squadName in
                    HStack {
                        Text(squadName).frame(width: 80, alignment: Alignment.leading)
                        Text(self.squads[squadName]!.description).frame(width: 40, alignment: Alignment.leading)
                    }
                }
            }
            .font(Font.system(size: 11))
            .frame(minWidth: 120)
            .padding(EdgeInsets(top: 2, leading: 4, bottom: 3, trailing: 4))
            .border(count < target ? Color.red : Color.blue)
            .padding(2)
        }
    }
}

/// view that collects info on who is playing and how many squads per section
struct TWSetupView : View {
    
    @ObservedObject var model: TWE.OrdersModel
        
    var body: some View {
        VStack {
            LabelRow(label: "Section Size") {
                TextField("Number of squads per section", value: $model.squadsPerSection, formatter: NumberFormatter.integer)
            }.tooltip("Number of squads per section")

            LabelRow(label: "Fleet Message") {
                TextField("Fleet backfill message -- blank means no backfill", text: $model.config.anyPlacedFleet)
            }.tooltip("Message for backfill fleet placement -- leave blank to prevent backfill")
            
            LabelRow(label: "Not Participating \(model.notPlaying.count)") {
                VStack {
                    Text("select players who should not receive orders").font(Font.system(size: 8))
                    
                    List(model.players, id: \.self, selection: $model.notPlaying) { player in
                        Text(player.name)
                    }
                }
            }
        }

    }
    
}

/// save controls for the EditableConfig
struct TWConfigSaveView : View {

    @Binding var config: EditableTWConfiguration
    
    var body: some View {
        HStack {
            TextField("Configuration name", text: $config.name)
                .frame(minWidth: 300)
            Button(action: save) { Text("Save") }
            Button(action: export) { Text("Export") }
        }
    }
    
    func save() {
        try? DataFileManager.twConfigs.save(dataFile: DataFile(config: config))
    }
    
    func export() {
        let panel = NSSavePanel()
        panel.allowedFileTypes = [ "twconfig" ]
        panel.nameFieldStringValue = config.name
        panel.beginSheetModal(for: NSApplication.shared.mainWindow!) { (response) in
            if response == .OK {
                if let url = panel.url,
                    let data = try? JSONEncoder().encode(self.config) {
                    try? data.write(to: url)
                }
            }
        }
    }

}

struct TWExplainView : View {
    
    let details: [TWTrace.Detail]
    
    enum ExplainType : String, Hashable, CaseIterable {
        case players = "Players"
        case squads = "Squads"
        case sections = "Sections"
    }
    
    class ExplainModel : ObservableObject {
        
        @Published var type: ExplainType = .squads {
            didSet {
                selectedIndex = 0
            }
        }
        
        @Published var selectedIndex = 0
    }
    
    @ObservedObject var model = ExplainModel()
    
    var players: [Model.Player] {
        Set(details.map { $0.player }).sorted()
    }
    
    var squads: [String] {
        Set(details.compactMap { $0.squad?.name }).sorted()
    }
    
    var sections: [String] {
        Set(details.compactMap { $0.status.section }).sorted()
    }
    
    @State var selectedIndex: Int = 0
    
    var filtered: [TWTrace.Detail] {
        switch model.type {
        case .players:
            let selectedPlayer = players[model.selectedIndex]
            return details.filter { $0.player == selectedPlayer }
        case .squads:
            let selectedSquadName = squads[model.selectedIndex]
            return details.filter { $0.squad?.name == selectedSquadName }
        case .sections:
            let selectedSection = sections[model.selectedIndex]
            return details.filter { $0.status.section == selectedSection }
        }
    }
    
    var playerSelector: some View {
        let players = self.players
        
        return ForEach(0 ..< players.count, id: \.self) { index in
            Text(players[index].name)
        }
    }
    
    var squadSelector: some View {
        let squads = self.squads
        
        return ForEach(0 ..< squads.count, id: \.self) { index in
            Text(squads[index])
        }
    }
    
    var sectionSelector: some View {
        let sections = self.sections
        
        return ForEach(0 ..< sections.count, id: \.self) { index in
            Text(sections[index])
        }
    }
    
    var body: some View {
        return VStack {
            HStack {
                Picker("", selection: $model.type) {
                    ForEach(ExplainType.allCases, id: \.self) { t in
                        Text(t.rawValue)
                    }
                }
                Picker("", selection: $model.selectedIndex) {
                    if model.type == .players {
                        playerSelector
                    } else if model.type == .squads {
                        squadSelector
                    } else {
                        sectionSelector
                    }
                }
            }
            
            ScrollView(.vertical) {
                ForEach(filtered, id: \.self) { detail in
                    Row(detail: detail, type: self.model.type, select: self.select(type:detail:))
                }
            }
        }
    }
    
    func select(type: ExplainType, detail: TWTrace.Detail) {
        model.type = type
        switch type {
        case .players:
            model.selectedIndex = players.firstIndex(of: detail.player) ?? 0
        case .sections:
            model.selectedIndex = sections.firstIndex(of: detail.status.section!) ?? 0
        case .squads:
            model.selectedIndex = squads.firstIndex(of: detail.squad!.name) ?? 0
        }
    }
    
    struct Row : View {
        let detail: TWTrace.Detail
        let type: ExplainType
        let select : (ExplainType, TWTrace.Detail) -> Void
        
        @State var showHelp = false
        
        var body: some View {
            VStack {
                HStack {
                    Button(detail.player.name, action: { self.select(.players, self.detail) })
                        .frame(width: 100, alignment: Alignment.leading)
                        .buttonStyle(LinkButtonStyle())
                        .disabled(type == .players)
                    Button("\(detail.squad?.name ?? "") \(detail.squad?.gp.millions ?? "")", action: { self.select(.squads, self.detail) })
                        .frame(width: 150, alignment: Alignment.leading)
                        .buttonStyle(LinkButtonStyle())
                        .disabled(type == .squads || detail.squad == nil)
                    Button(detail.status.section ?? "", action: { self.select(.sections, self.detail) })
                        .frame(width: 150, alignment: Alignment.leading)
                        .buttonStyle(LinkButtonStyle())
                        .disabled(type == .sections || detail.status.section == nil)
                    Text(detail.status.verb)
                        .frame(width: 60)
                    Text(detail.status.description)
                        .frame(width: 200, alignment: Alignment.leading)
                    
                    Button("?", action: { self.showHelp.toggle() })
                        .buttonStyle(LinkButtonStyle())
                }
                .frame(minWidth: 800, maxWidth: .infinity, alignment: .leading)
                
                if showHelp {
                    HStack {
                        Text(detail.status.help).bold()
                    }
                }
            }
        }
    }
}

//struct TWEditorView_Previews: PreviewProvider {
//
//    static var previews: some View {
//        twEditorEvironment(TWEditorView())
//            .environmentObject(WaitingModel())
//    }
//}
