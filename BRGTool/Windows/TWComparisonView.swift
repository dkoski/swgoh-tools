//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Combine
import SWGOH

struct TWComparisonView: View {
    
    @ObservedObject var dataFileManager = DataFileManager.guilds
    @EnvironmentObject var waiting: WaitingModel

    @State var guild1: DataFile<Model.Guild>? = DataFileManager.defaultGuild
    @State var guild2: DataFile<Model.Guild>? = DataFileManager.guilds.newest
    
    @State var data = [TWCompare.Row]()
    
    var body: some View {
        WaitingView {
            VStack {
                HStack {
                    ModelSelector(selection: self.$guild1, dataFileManager: dataFileManager)
                    Text("vs")
                    ModelSelector(selection: self.$guild2, dataFileManager: dataFileManager)
                    Button("Compare", action: self.compare)
                }
                Divider()
                ComparisonView(data: self.$data)
            }
            .padding()
        }
    }
    
    @State var cancellable: AnyCancellable? = nil

    fileprivate func compare(_ guild1: DataFile<Model.Guild>, _ guild2: DataFile<Model.Guild>) -> AnyPublisher<[TWCompare.Row], Error> {
        return guild1
            .squadEvaluations()
            .append(guild2.squadEvaluations())
            .collect()
            .map { guilds -> [TWCompare.Row] in
                let g1 = guilds[0]
                let g2 = guilds[1]
                
                let report = TWCompare(side1: g1, side2: g2)
                
                var rows = [TWCompare.Row]()
                rows.append(contentsOf: report.unitRows())
                rows.append(contentsOf: report.squadRows(isKey: true))
                rows.append(contentsOf: report.squadRows(isKey: false))

                return rows
            }
            .eraseToAnyPublisher()
    }
    
    func compare() {
        if let guild1 = guild1, let guild2 = guild2 {
            let future = compare(guild1, guild2).share()
            
            cancellable = future
                .receive(on: DispatchQueue.main)
                .sink(receiveCompletion: { _ in
                    self.cancellable = nil
                }, receiveValue: { (rows) in
                    self.data = rows
                })
            
            waiting.activity = "Preparing squads ..."
            waiting.waitFor(
                future
                    .receive(on: DispatchQueue.main)
                    .eraseToAnyPublisher()
            )
        }
    }
    
    /// ugh.  I would prever to have ColumnValueProvider produce a View, but the associated types wrecks that
    enum ColumnType {
        case normal
        case histogramScores
        case cumulativeHistogramScores
    }
    
    fileprivate struct Column : Hashable {
        let index: Int
        
        let label: String
        let tooltip: String?
        let width: CGFloat
        let value: ColumnValueProvider
        
        let type: ColumnType
        
        var isVisible: Bool
        let isSortable: Bool
        
        internal init(_ index: Int, label: String, tooltip: String? = nil, width: CGFloat, value: ColumnValueProvider, type: ColumnType = .normal, isVisible: Bool = true, isSortable: Bool = true) {
            self.index = index
            self.label = label
            self.tooltip = tooltip
            self.width = width
            self.value = value
            self.type = type
            self.isVisible = isVisible
            self.isSortable = type == .normal ? isSortable : false
        }
                
        func hash(into hasher: inout Hasher) {
            hasher.combine(label)
        }
        
        static func == (lhs: Column, rhs: Column) -> Bool {
            lhs.index == rhs.index && lhs.isVisible == rhs.isVisible
        }
    }

    struct ComparisonView : View {
        
        @Binding var data: [TWCompare.Row]
        
        @State private var columns = [
            Column(0, label: "Name", tooltip: "Squad name", width: 100, value: StringValueProvider(keyPath: \.label)),
            
            Column(1, label: "Count", width: 100, value: IntValueProvider(keyPath: \.team.count)),
            Column(2, label: "%", width: 50, value: PercentValueProvider(keyPath: \.percentCount)),
            Column(3, label: "Opponent", width: 100, value: IntValueProvider(keyPath: \.opponent.count)),
            
            Column(4, label: "GP", tooltip: "75th percentile", width: 100, value: IntValueProvider(keyPath: \.team.score)),
            Column(5, label: "%", width: 50, value: PercentValueProvider(keyPath: \.percentScore)),
            Column(6, label: "Opponent", width: 100, value: IntValueProvider(keyPath: \.opponent.score)),
            
            Column(7, label: "Histogram", width: 120, value: NOPValueProvider(), type: .histogramScores),
            Column(8, label: "Cum. Histogram", width: 120, value: NOPValueProvider(), type: .cumulativeHistogramScores, isVisible: false),
        ]
        
        var body: some View {
            VStack(alignment: .leading) {
                TableHeader(data: $data, columns: $columns)
                
                Divider().background(Color.blue)
                
                // using a scrollview instead of a list so I get more control over the vertical spacing
                ScrollView(.vertical, showsIndicators: true) {
                    HStack {
                        VStack(alignment: .leading, spacing: 4) {
                            ForEach(data) { row in
                                TableRow(row: row, columns: self.columns)
                            }
                        }
                        
                        Spacer()
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                    }
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity)
            }
        }
                
    }
    
    fileprivate struct TableRow : View {
        
        let row: TWCompare.Row
        let columns: [Column]
        
        @State var showBig = false
        
        func item(column: Column) -> some View {
            if !column.isVisible {
                return AnyView(EmptyView())
            }
            
            var histograms: [Histogram]
            switch column.type {
            case .normal:
                return AnyView(HStack {
                    Text(column.value.value(row: self.row))
                        .frame(minWidth: column.width, maxWidth: column.width, alignment: .trailing)
                    Divider()
                })
                
            case .histogramScores:
                histograms = row.histograms
                
            case .cumulativeHistogramScores:
                histograms = row.cumulativeHistograms
            }
            
            if showBig {
                return AnyView(HStack {
                    HistogramView(histograms: histograms)
                    .frame(width: 300, height: 100)
                    .frame(minWidth: column.width, maxWidth: column.width, alignment: .trailing)
                })
            } else {
                return AnyView(HStack {
                    HistogramView(histograms: histograms)
                    .frame(minWidth: column.width, maxWidth: column.width, alignment: .trailing)
                })
            }
        }

        var body: some View {
            let (fg, bg) = colors()
            
            return HStack {
                ForEach(self.columns, id: \.self) { column in
                    self.item(column: column)
                }
            }
            .contentShape(Rectangle())
            .onTapGesture {
                self.showBig.toggle()
            }
            .padding(EdgeInsets(top: 1, leading: 0, bottom: 1, trailing: 0))
            .foregroundColor(Color(fg))
            .background(Color(bg))
        }
        
        func colors() -> (NSColor, NSColor) {
            func lerp(_ a: Double, _ b: Double, mix: Double) -> CGFloat {
                CGFloat(a + (b - a) * mix)
            }
            
            let rating = row.rating(scores: [(row.team.count, row.opponent.count), (row.team.score, row.opponent.score)])
            
            let ok = 0.98
            if rating >= ok {
                return (NSColor.textColor, NSColor.textBackgroundColor)
            }
            
            return (NSColor.black, NSColor(displayP3Red: 1.0, green: lerp(0.0, 1.0, mix: rating), blue: 0, alpha: 1))
        }

    }
    
    fileprivate struct TableHeader : View {
        
        @State var sortColumn = 0
        @State var sortAscending = true

        @Binding var data: [TWCompare.Row]
        @Binding var columns: [Column]
        
        func singleHeader(column: Column) -> some View {
            if !column.isVisible {
                return AnyView(EmptyView())
            }
            switch column.type {
            case .normal:
                if column.isSortable {
                    return AnyView(HStack {
                        Button(action: {
                            self.sort(index: column.index)
                        }) {
                            if column.index == self.sortColumn {
                                Text(column.label).bold()
                            } else {
                                Text(column.label)
                            }
                        }
                        .buttonStyle(PlainButtonStyle())
                        .frame(minWidth: column.width, maxWidth: column.width)
                        .tooltip(column.tooltip)
                        
                        Divider().frame(maxHeight: 10)
                    })
                } else {
                    return AnyView(HStack {
                        Text(column.label).frame(minWidth: column.width, maxWidth: column.width)
                        .frame(minWidth: column.width, maxWidth: column.width)
                        .tooltip(column.tooltip)
                        
                        Divider().frame(maxHeight: 10)
                    })
                }
            case .histogramScores, .cumulativeHistogramScores:
                return AnyView(HStack {
                    Button(action: {
                        self.columns[7].isVisible.toggle()
                        self.columns[8].isVisible.toggle()
                    }) {
                        Text(column.label)
                    }
                    .buttonStyle(PlainButtonStyle())
                    .frame(minWidth: column.width - 10, maxWidth: column.width - 10)
                    .tooltip(column.tooltip)
                    
                    Button(action: {
                        NSWorkspace.shared.open(URL(string: "https://gorgatron1.github.io/brg/help/histograms.png")!)
                    }) {
                        Text("?")
                    }
                    .buttonStyle(LinkButtonStyle())
                    .frame(minWidth: 10, maxWidth: 10)
                })
            }
        }

        var body: some View {
            HStack {
                ForEach(columns, id: \.self) { column in
                    self.singleHeader(column: column)
                }
            }
        }
        
        func sort(index: Int) {
            if sortColumn == index {
                sortAscending.toggle()
            }
            sortColumn = index
            
            data.sort { (lhs, rhs) -> Bool in
                lhs.group == rhs.group ? columns[index].value.compare(lhs: lhs, rhs: rhs) == sortAscending : lhs.group < rhs.group
            }
        }

    }
    
}

private protocol ColumnValueProvider {
    func value(row: TWCompare.Row) -> String
    func compare(lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool
}

private struct StringValueProvider : ColumnValueProvider {
    
    let keyPath: KeyPath<TWCompare.Row, String>
    
    func value(row: TWCompare.Row) -> String {
        row[keyPath: keyPath]
    }
    
    func compare(lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool {
        let lv = lhs[keyPath: keyPath]
        let rv = rhs[keyPath: keyPath]
        return lv < rv
    }
}

private struct IntValueProvider : ColumnValueProvider {
    
    let keyPath: KeyPath<TWCompare.Row, Int>
    let formatter = NumberFormatter.decimal
    
    func value(row: TWCompare.Row) -> String {
        formatter.string(from: row[keyPath: keyPath])
    }
    
    func compare(lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool {
        let lv = lhs[keyPath: keyPath]
        let rv = rhs[keyPath: keyPath]
        return lv < rv
    }
}

private struct PercentValueProvider : ColumnValueProvider {
    
    let keyPath: KeyPath<TWCompare.Row, Double>
    let formatter = NumberFormatter.percent
    
    func value(row: TWCompare.Row) -> String {
        formatter.string(from: row[keyPath: keyPath])
    }
    
    func compare(lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool {
        let lv = lhs[keyPath: keyPath]
        let rv = rhs[keyPath: keyPath]
        return lv < rv
    }
}

private struct NOPValueProvider : ColumnValueProvider {
    
    func value(row: TWCompare.Row) -> String {
        return ""
    }
    
    func compare(lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool {
        return false
    }
    
}
