//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Combine
import SWGOH

struct ManageFilesView: View {
    
    @EnvironmentObject var waiting: WaitingModel
    
    @State var tab: Int? = 0

    var body: some View {
        WaitingView {
            VStack {
                HStack {
                    Tab(index: 0, title: "Guilds", defaultIndex: 0, selection: self.$tab)
                    Tab(index: 1, title: "Players", defaultIndex: 0, selection: self.$tab)
                    Spacer()
                }
                .padding(EdgeInsets(top: 2, leading: 5, bottom: 0, trailing: 2))
                
                Divider()

                TabContents(index: 0, selection: self.tab) {
                    FileListView(manager: DataFileManager.guilds)
                }
                TabContents(index: 1, selection: self.tab) {
                    FileListView(manager: DataFileManager.players)
                }
            }
        }
    }
    
    struct FileListView<T: TopLevelModel> : View {
        
        @ObservedObject var manager: DataFileManager.Storage<T>
        
        @EnvironmentObject var waiting: WaitingModel
        
        let allyCodeWidth: CGFloat = 100
        let nameWidth: CGFloat = 200
        let dateWidth: CGFloat = 150
        let controlsWidth: CGFloat = 100
        
        class Model : NSObject, ObservableObject {
            @Published var allyCodeStorage: String = ""
        }

        @State var model = Model()
        @State var allyCode: Int?

        var body: some View {
            VStack(alignment: .leading) {
                HStack {
                    Text("Ally Code").frame(width: allyCodeWidth, alignment: Alignment.topLeading)
                    Text("Name").frame(width: nameWidth, alignment: Alignment.topLeading)
                    Text("Date").frame(width: dateWidth / 2, alignment: Alignment.topLeading)
                    
                    TextField("###-###-### (you can paste in a swgoh url)", text: $model.allyCodeStorage, onCommit: self.download)
                        .onReceive(model.$allyCodeStorage) { (value) in
                            self.allyCode = Int(allyCode: value)
                        }
                }
                .padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 2))
                
                Divider()
                ScrollView(.vertical, showsIndicators: true) {
                    ForEach(manager.values) { file in
                        HStack {
                            Text(file.allyCode.asAllyCode)
                                .frame(width: self.allyCodeWidth, alignment: .leading)
                            Text(file.name)
                                .frame(width: self.nameWidth, alignment: .leading)
                            Text(file.dateDescription)
                                .frame(width: self.dateWidth, alignment: .leading)
                            
                            HStack {
                                IconButton(.refresh) {
                                    self.refresh(file: file)
                                }
                                IconButton(.delete) {
                                    self.delete(file: file)
                                }
                            }
                            .frame(width: self.controlsWidth, alignment: .leading)
                        }
                    }
                    .padding(EdgeInsets(top: 0, leading: 15, bottom: 0, trailing: 2))
                }
            }
            .frame(minWidth: 500, maxWidth: .infinity, minHeight: 300, maxHeight: .infinity, alignment: Alignment.topLeading)
        }
        
        func refresh(file: DataFile<T>) {
            waiting.waitFor(manager.update(file))
        }
        
        func delete(file: DataFile<T>) {
            manager.delete(file)
        }
        
        @State var cancellable: AnyCancellable?
        func download() {
            if let allyCode = allyCode {
                model.allyCodeStorage = allyCode.asAllyCode
                
                waiting.activity = "Downloading \(allyCode.asAllyCode)"
                let publisher = manager.download(allyCode: allyCode)
                    .share().eraseToAnyPublisher()
                
                cancellable = publisher
                    .receive(on: DispatchQueue.main)
                    .sink(receiveCompletion: { _ in
                        self.cancellable = nil
                    }) { (dataFile) in
                        self.model.allyCodeStorage = ""
                        self.allyCode = nil
                    }
                
                waiting.waitFor(publisher)
            }
        }
        
    }
}

struct ManageFilesView_Previews: PreviewProvider {
    static var previews: some View {
        ManageFilesView().environmentObject(WaitingModel())
    }
}

