//
//  Preferences.swift
//  BRGTool
//
//  Created by David Koski on 3/17/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import Combine
import SWGOH

struct PreferencesView: View {
    
    let signUpURL = URL(string: "https://api.swgoh.help/signup")!
    
    class PreferencesModel : NSObject, ObservableObject {
        
        @Published var username: String {
            didSet {
                Defaults.shared.credentials.username = username
            }
        }
        
        @Published var password: String {
            didSet {
                Defaults.shared.credentials.password = password
            }
        }
                
        @Published var allyCode: Int = 0 {
            didSet {
                Defaults.shared.allyCode = allyCode
            }
        }

        override init() {
            self.username = Defaults.shared.credentials.username
            self.password = Defaults.shared.credentials.password
            self.allyCode = Defaults.shared.allyCode
        }
        
        func commitCredentialsIfPossible() {
            if Defaults.shared.credentials.isComplete {
                Defaults.shared.credentials.store()
            }
        }
    }

    @ObservedObject var model = PreferencesModel()
    @EnvironmentObject var waiting: WaitingModel

    var body: some View {
        WaitingView() {
            VStack(alignment: .leading) {
                VStack(alignment: .leading) {
                    Text("Account from swgoh.help -- this is used to fetch player/guild information.")
                    HStack {
                        Text("If you don't have an account, you can")
                        Button(action: { NSWorkspace.shared.open(self.signUpURL) })
                            { Text("click here.")}
                            .buttonStyle(LinkButtonStyle())
                    }
                    
                    LabelRow(label: "Username") {
                        TextField("swgoh.help username", text: self.$model.username, onCommit: {
                            self.model.commitCredentialsIfPossible()
                        })
                    }
                    LabelRow(label: "Password") {
                        SecureField("swgoh.help password", text: self.$model.password, onCommit: {
                            self.model.commitCredentialsIfPossible()
                        })
                    }
                }
                
                LabelRow(label: "Ally Code") {
                    TextField("###-###-### (you can paste in a swgoh url)", value: self.$model.allyCode, formatter: AllyCodeFormatter(), onCommit: self.fetch)
                }

            }
            .padding()
        }
    }
    
    func fetch() {
        if model.allyCode.isValidAllyCode {
            let fetchGuild = DataFileManager.guilds.find(allyCode: model.allyCode)?.isOutOfDate ?? true
            let fetchPlayer = DataFileManager.players.find(allyCode: model.allyCode)?.isOutOfDate ?? true
            
            if fetchGuild && fetchPlayer {
                waiting.activity = "Fetching guild and player"
                waiting.waitFor(
                    DataFileManager.guilds.download(allyCode: model.allyCode)
                        .flatMap { _ in DataFileManager.players.download(allyCode: self.model.allyCode) }
                    .eraseToAnyPublisher()
                )
            } else if fetchGuild {
                waiting.activity = "Fetching guild"
                waiting.waitFor(DataFileManager.guilds.download(allyCode: model.allyCode))
            } else if fetchPlayer {
                waiting.activity = "Fetching player"
                waiting.waitFor(DataFileManager.players.download(allyCode: model.allyCode))
            }
        }
    }
}

struct PreferencesView_Previews: PreviewProvider {
    static var previews: some View {
        PreferencesView()
    }
}
