//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import SWGOH

struct SquadRuleView_Preview: PreviewProvider {
    
    struct Test : View {

        let c = Model.Character.find("Aurra")
        
        var body: some View {
            VStack {
                ZStack {
                    c.icon(size: 48, borderWidth: 1)
                        .offset(x: -3)

                    Text(c.commonName)
                        .font(Font.system(size: 10))
                        .foregroundColor(Color.black)
                        .background(Color(red: 0.9, green: 0.9, blue: 0.9))
                        .lineLimit(1)
                        .fixedSize()
                        .frame(minWidth: 55)
                        .offset(y: 24)

                }
                .border(Color.red)
            }
            .frame(width: 150, height: 150)
        }
    }
    

    static var previews: some View {
        VStack {
            Test()
            .environment(\.colorScheme, .dark)
            
            Test()
            .environment(\.colorScheme, .light)
        }
    }
}

