//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import SWGOH

/// The "Rules" tab of the squad editor view
struct SquadRuleView : View {
 
    @ObservedObject var model: SquadEditorViewModel
    
    @State var tab = 1
    
    var tabs: some View {
        Picker("", selection: $tab) {
            Text("Info").tag(0)
            Text("Layout").tag(1)
            Text("Rules").tag(2)
        }
        .pickerStyle(SegmentedPickerStyle())
    }
    
    var contents: some View {
        let squadRule = $model.ruleSet.squadRules[model.selectedRuleIndex]
        
        switch tab {
        case 0:
            return AnyView(SquadRuleInfoView(squadRule: squadRule))
        case 1:
            return AnyView(SquadRuleLayoutView(squadRule: squadRule, model: model))
        case 2:
            return AnyView(SquadRuleUnitView(squadRule: squadRule))
        default:
            fatalError()
        }
    }
    
    var body: some View {
        // TODO show the squad rule
        // TODO maybe show all the rules with the same name?
        // TODO show the top N variations?
        
        // select different rule
        // squad info: name, tier, etc., preference
        // search units, add to squad
        // squad layout
        // unit rules
        // counter information
        
        // 5 or 7 (+ cap) spots
        
        // search units
        
        let currentKind = self.model.ruleSet.squadRules[model.selectedRuleIndex].kind
        
        return VStack {
            HStack {
                Picker("", selection: $model.selectedRuleIndex) {
                    ForEach(0 ..< model.ruleSet.squadRules.count, id: \.self) { index in
                        // only show the same kind of rules in the dropdown
                        if self.model.ruleSet.squadRules[index].kind == currentKind {
                            Text(self.model.ruleSet.squadRules[index].displayName)
                        }
                    }
                }
                Spacer()
                tabs
            }
            
            contents
            
            // TODO sample of squads
        }
    }
    
}

struct SquadRuleInfoView : View {
    
    @Binding var squadRule: SquadRule

    var body: some View {
        VStack {
            LabelRow(label: "Name") {
                TextField("name", text: $squadRule.canonicalName)
            }
            LabelRow(label: "Variant") {
                TextField("variant", text: $squadRule.name)
            }
            LabelRow(label: "Notes") {
                TextField("notes", text: $squadRule.notes)
            }
            LabelRow(label: "Preference") {
                TextField("preference", value: $squadRule.preference, formatter: NumberFormatter.integer)
            }
            LabelRow(label: "Tier") {
                Picker("", selection: $squadRule.tier) {
                    ForEach(1 ..< 4) { n in
                        Text(n.description)
                    }
                }
                
                Toggle(isOn: $squadRule.defense) {
                    Text("Defense")
                }
            }
            Divider()
            
            VStack {
                Text("Identifiers for counters (internal).  Attacker id should be specific to this variant.  Defender id may be left blank or can be the id of the generic variant.")
                LabelRow(label: "Attacker ID") {
                    TextField("attacker id", text: $squadRule.attackerId)
                }
                LabelRow(label: "Defender ID") {
                    TextField("defender id", text: $squadRule.defenderId)
                }
            }
        }
    }
}

struct SquadRuleLayoutView : View {
    
    /// the rule we are editing
    @Binding var squadRule: SquadRule
    
    /// used for the unit search field
    @ObservedObject var model: SquadEditorViewModel

    @State var searchText = ""
    @State var matchingCharacters = [Model.Character]()
    @State var selectedCharacters = Set<Model.Character>()
    
    var allCapitalShips: [Model.Character] {
        Model.Character.all.filter { $0.role == "Capital Ship" }.sorted()
    }
    
    private func search(searchText: String) {
        let words = searchText.lowercased().split(separator: " ")
        
        switch squadRule.kind {
        case .fleet:
            self.matchingCharacters = Model.Character.all.filter { $0.ship && $0.role != "Capital Ship" && $0.matches(words) }.sorted()
            
        case .squad:
            self.matchingCharacters = Model.Character.all.filter { !$0.ship && $0.matches(words) }.sorted()
        }
        
        self.selectedCharacters.removeAll()
    }

    var unitPicker: some View {
        VStack {
            TextField("filter", text: $model.characterSearchText)
                .onReceive(model.$characterSearchText.debounce(for: 0.1, scheduler: DispatchQueue.main), perform: search)
            Text("\(matchingCharacters.count)")
            List(selection: $selectedCharacters) {
                ForEach(matchingCharacters, id: \.self) { c in
                    HStack {
                        Text(c.commonName)
                            .frame(width: 150, alignment: .leading)
                            .onDrag {
                                if self.selectedCharacters.contains(c) {
                                    // dragging a selected character, let's drag multiple
                                    return NSItemProvider(object: DraggableCharacter(characters: Array(self.selectedCharacters)))
                                } else {
                                    return NSItemProvider(object: DraggableCharacter(characters: [c]))
                                }
                            }
                    }
                }
            }
        }
        .frame(width: 200)
    }
    
    func unitRow(index: Int) -> some View {
        Group {
            // TODO maybe context menu to clear?  or copy previous?
            IconButton(.add, action: {
                self.squadRule.possibleCharacters[index].formUnion(self.selectedCharacters)
            }).disabled(self.selectedCharacters.isEmpty)
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack {
                    if self.squadRule.possibleCharacters[index].isEmpty {
                        // TODO maybe put the "copy previous" button here?
                        Text("Drag characters here or select and click the +")
                            .frame(height: 55)
                    } else {
                        ForEach(self.squadRule.possibleCharacters[index].sorted(), id: \.self) { c in
                            ZStack {
                                c.icon(size: 48, borderWidth: 1)
                                    .offset(x: -3)
                                    .contextMenu {
                                        Button(action: {
                                            self.squadRule.possibleCharacters[index].remove(c)
                                        }) {
                                            Text("Remove")
                                        }
                                    }

                                Text(c.commonName)
                                    .font(Font.system(size: 10))
                                    .foregroundColor(Color.black)
                                    .background(Color(red: 0.9, green: 0.9, blue: 0.9))
                                    .lineLimit(1)
                                    .frame(maxWidth: 55)
                                    .offset(y: 24)
                            }
                            .offset(y: -2)
                            .frame(height: 55)
                        }
                    }
                }
            }
            .frame(minWidth: 80, maxWidth: .infinity, minHeight: 55)
        }
    }
    
    // TODO selection for rule edit
    // TODO selection for delete
    // TODO keys to assign (squad + fleet)
    // TODO drag & drop (squad + fleet)
        
    /// special binding for the Picker used to select capital ships -- a Picker wouldn't normally work with a Set
    var capitalShipBinding: Binding<Model.Character> {
        Binding(get: {
            self.squadRule.possibleCharacters[0].first ?? self.allCapitalShips[0]
        }) { v in
            self.squadRule.possibleCharacters[0] = [ v ]
        }
    }
    
    var fleetSelector: some View {
        VStack {
            ForEach(0 ..< 8, id: \.self) { index in
                HStack {
                    if index == 0 {
                        
                        Text("Capital Ship")
                            .frame(width: 80)
                        
                        Picker("", selection: self.capitalShipBinding) {
                            ForEach(self.allCapitalShips, id: \.self) { c in
                                HStack {
                                    Text(c.commonName)
                                }
                            }
                        }
                        .frame(width: 120)
                                                
                        self.capitalShipBinding.wrappedValue.icon(size: 48, borderWidth: 1)
                        
                        Spacer()
                    } else {
                        Text((index + 1).description)
                            .frame(width: 80)
                        
                        self.unitRow(index: index)
                            .onDrop(of: [ DraggableCharacter.type ], isTargeted: nil) { providers in
                                self.drop(index: index, providers: providers)
                                return true
                            }
                    }
                }
            }
        }
    }
    
    var squadSelector: some View {
        VStack {
            ForEach(0 ..< 5, id: \.self) { index in
                HStack {
                    if index == 0 {
                        Text("Leader")
                            .frame(width: 50)
                    } else {
                        Text((index + 1).description)
                            .frame(width: 50)
                    }

                    self.unitRow(index: index)
                }
                .onDrop(of: [ DraggableCharacter.type ], isTargeted: nil) { providers in
                    self.drop(index: index, providers: providers)
                    return true
                }
            }
        }
        .frame(maxWidth: .infinity)
    }
    
    var body: some View {
        VStack {
            HStack {
                unitPicker
            
                if squadRule.kind == .fleet {
                    fleetSelector
                } else {
                    squadSelector
                }
            }
        }
    }
    
    func drop(index: Int, providers: [NSItemProvider]) {
        providers[0].loadDataRepresentation(forTypeIdentifier: DraggableCharacter.type) { data, _ in
            DispatchQueue.main.async {
                if let data = data, let unitIds = String(data: data, encoding: .utf8) {
                    for unitId in unitIds.split(separator: ",") {
                        let c = Model.Character.find(String(unitId))
                        self.squadRule.possibleCharacters[index].insert(c)
                    }
                }
            }
        }
    }
}

struct SquadRuleUnitView : View {
    
    @Binding var squadRule: SquadRule

    var body: some View {
        Text("foo")
    }
}

/// A representation of Model.Character that can be dragged & dropped
class DraggableCharacter : NSObject, NSItemProviderWriting {
    
    // using public.text -- i tried other values and the drop wouldn't recognize them.  i wonder if I have to register them somewhere?
    static let type = "public.text"
    
    let characters: [Model.Character]
    
    init(characters: [Model.Character]) {
        self.characters = characters
    }
    
    static let writableTypeIdentifiersForItemProvider = [ type ]
    
    func loadData(withTypeIdentifier typeIdentifier: String, forItemProviderCompletionHandler completionHandler: @escaping (Data?, Error?) -> Void) -> Progress? {
        let data = characters.map { $0.id }.joined(separator: ",").data(using: .utf8)!
        completionHandler(data, nil)
        return nil
    }
    
}
