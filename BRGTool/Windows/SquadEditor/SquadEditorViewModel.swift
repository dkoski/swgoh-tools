//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
import Combine
import SWGOH

class SquadEditorViewModel : NSObject, ObservableObject {
    let guild: Model.Guild
    
    @Published var ruleSet: SquadRuleSet
    @Published var player: Model.Player
    
    /// for filtering rules by unit type, etc.
    @Published var filterText = ""
    @Published var filteredIndices: [Int]
    
    func filter() {
        if filterText.isEmpty {
            filteredIndices = ruleSet.squadRules.enumerated().filter { !$0.1.disabled }.map { $0.0 }
        } else {
            filteredIndices.removeAll()
            let words = self.filterText.lowercased().split(separator: " ")
            for (index, rule) in ruleSet.squadRules.enumerated() where !rule.disabled {
                if rule.matches(words) {
                    filteredIndices.append(index)
                }
            }
        }
    }

    @Published var selectedRuleIndex = 0
    
    /// for filtering characters by name, type, etc.
    @Published var characterSearchText = ""
        
    init(guild: Model.Guild, ruleSet: SquadRuleSet) {
        self.guild = guild
        self.player = guild.players.first(where: { $0.stats.allyCode == Defaults.shared.allyCode }) ?? guild.players[0]
        self.ruleSet = ruleSet
        self.filteredIndices = Array(ruleSet.squadRules.indices)
    }
    
    func add(rule: SquadRule) {
        ruleSet.add(rule: rule)
        filter()
    }
    
    func removeRule(at index: Int) {
        self.ruleSet.squadRules[index].disabled = true
        self.filter()
    }
}
