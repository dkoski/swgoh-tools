//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import SWGOH

struct SquadEditorView: View {
    
    @State var guild: Model.Guild?
    @State var rules: SquadRuleSet?
    
    var body: some View {
        WaitingView {
            if guild == nil || rules == nil {
                SquadEditorSelectInputs(guild: $guild, rules: $rules)
                    .padding()
            } else {
                SquadEditorContentsView(model: SquadEditorViewModel(guild: guild!, ruleSet: rules!))
                .padding()
            }
        }
        .frame(minWidth: 800, minHeight: 800)
    }
}

struct SquadEditorSelectInputs : View {

    @Binding var guild: Model.Guild?
    @Binding var rules: SquadRuleSet?
    
    @State var guildFile: DataFile<Model.Guild>? = DataFileManager.defaultGuild
    @State var ruleFile: DataFile<SquadRuleSet>? = DataFileManager.squadRules.newest

    var body: some View {
        VStack(alignment: .center) {
            
            HStack(alignment: .center) {
                Text("Select a guild: ")
                ModelSelector(selection: $guildFile, dataFileManager: DataFileManager.guilds)
            }
            .frame(width: 600)
            
            HStack(alignment: .center) {
                Text("and a rule set: ")
                LocalModelSelector(selection: $ruleFile, dataFileManager: DataFileManager.squadRules, newModelFactory: { (name: String) in SquadRuleSet(name: name) }, dataFile: { DataFile(squadRules: $0) })
            }
            .frame(width: 600)

            Button(action: edit) {
                Text("Edit")
            }
            .disabled(guildFile == nil || ruleFile == nil )
        }

    }
    
    func edit() {
        if let guild = try? guildFile?.value(), let rules = try? ruleFile?.value() {
            self.guild = guild
            self.rules = rules
        }
    }
    
}

struct SquadEditorContentsView : View {
    
    @ObservedObject var model: SquadEditorViewModel
    
    @State var tab = 0
    
    var tabs: some View {
        Picker("", selection: $tab) {
            Text("Squads").tag(0)
            Text("Rules").tag(1)
            Text("Variations").tag(2)
            Text("Combinations").tag(3)
        }
        .pickerStyle(SegmentedPickerStyle())
    }
    
    var contents: some View {
        switch tab {
        case 0:
            return AnyView(SquadsListView(model: model, tab: $tab))
        case 1:
            return AnyView(SquadRuleView(model: model))
        case 2:
            return AnyView(SquadVariationsView(model: model))
        case 3:
            return AnyView(SquadCombinationsView(model: model))
        default:
            fatalError()
        }
    }
    
    var body: some View {
        VStack {
            HStack {
                tabs
                
                Spacer()
                TextField("Name", text: $model.ruleSet.name)
                Button("Save", action: save)
                
                Picker("", selection: $model.player) {
                    ForEach(model.guild.players, id: \.self) { player in
                        Text(player.name)
                    }
                }
                
                // TODO add a player selector
            }
            Divider()
            contents
        }
    }
    
    func save() {
        // TODO toss the disabled rules when we save
        try? DataFileManager.squadRules.save(dataFile: DataFile(squadRules: model.ruleSet))
    }
}

struct SquadsListView : View {
 
    @ObservedObject var model: SquadEditorViewModel
    @Binding var tab: Int
    
    /// some jankery because the List needs an optional Int and the Picker in the SquadRuleView needs an Int
    @State var optionalRuleIndexBinding: Int?
    var selectedRuleIndexBinding: Binding<Int?> {
        Binding(get: {
            self.optionalRuleIndexBinding
        }) { value in
            self.optionalRuleIndexBinding = value
            if let value = value {
                self.model.selectedRuleIndex = value
            }
        }
    }
        
    var body: some View {
        VStack {
            HStack {
                TextField("Filter", text: $model.filterText)
                    .onReceive(model.$filterText.debounce(for: 0.25, scheduler: DispatchQueue.main)) { text in
                        self.model.filter()
                }
                
                Button("New Squad", action: newSquad)
                Button("New Fleet", action: newFleet)

                // TODO: new, duplicate, delete?
            }
            
            Divider()
            
            List(model.filteredIndices, id: \.self, selection: selectedRuleIndexBinding) { index in
                Entry(model: self.model, index: index, squadRule: self.$model.ruleSet.squadRules[index], tab: self.$tab)
            }
            
            // TODO show squad details
            // TODO show a squad based on the selection
        }
    }
    
    func newSquad() {
        let rule = SquadRule(squadName: "New \(model.ruleSet.squadRules.count + 1)")
        model.add(rule: rule)
    }
    
    func newFleet() {
        let rule = SquadRule(kind: .fleet, squadName: "New \(model.ruleSet.squadRules.count + 1)")
        model.add(rule: rule)
    }
    
    struct Entry : View {
        
        @ObservedObject var model: SquadEditorViewModel
        let index: Int
        
        @Binding var squadRule: SquadRule
        @Binding var tab: Int

        var body: some View {
            HStack {
                IconButton(.gear, action: edit)
                IconButton(.add, action: duplicate)
                IconButton(.delete, action: delete)

                Text(squadRule.canonicalName)
                    .frame(width: 80, alignment: Alignment.leading)
                
                ForEach(squadRule.representativeCharacters, id: \.self.id) { c in
                    c.icon()
                }
            }
        }
        
        func edit() {
            model.selectedRuleIndex = index
            tab = 1
        }
        
        func duplicate() {
            model.add(rule: squadRule.copy)
        }
        
        func delete() {
            model.removeRule(at: index)
        }
    }
}

struct SquadVariationsView : View {
 
    @ObservedObject var model: SquadEditorViewModel
    
    var body: some View {
        // TODO show the variations
        // TODO maybe have a priority editor?
        Text("foo")
    }
    
}

struct SquadCombinationsView : View {
 
    @ObservedObject var model: SquadEditorViewModel
    
    var body: some View {
        // TODO show all the squads for a particular player
        // TODO maybe have a priority editor?
        Text("foo")
    }
    
}

struct SquadEditorView_Previews: PreviewProvider {
    
    static let squadRuleSet: SquadRuleSet = {
        var ruleSet = SquadRuleSet(name: "TW.default")
        for squadBuilder in TW.configurations[TW.defaultSquadRuleName]! {
            for r in squadBuilder.asSquadRules() {
                ruleSet.add(rule: r)
            }
        }
        return ruleSet
    }()
    
    static var previews: some View {
        SquadEditorView(guild: try! DataFileManager.defaultGuild?.value(),
                        rules: squadRuleSet)
            .environmentObject(WaitingModel())
    }
}
