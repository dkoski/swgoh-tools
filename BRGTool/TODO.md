#  TODO

- if you paste in a bad url for a player (e.g. a guild url) give a reasonable message

- version check

- TW compare
- GAC tool

- squad editor
- guild browser

- add expiration on the Token

- compare: filter sections
- compare: divider between sections

- view player
- compare player

- order edit: swap sections

- order edit: trace a particular squad, explain why things were place or not (e.g. filtered by gp, too many placed, full, on defense)

# DONE

- add logging
- compare: gear level is missing score (should be total gp)
- orders: slider for section size
- downloaded files manager

- orders: set status per user (active, defend, no orders)
- save that in defaults
- save the config in defaults

- order edit: one row of menus
- order edit: filter by section
- order edit: filter by squad
- order edit: filter by toon
- order edit: better move up/down
- order edit: fleet sections
