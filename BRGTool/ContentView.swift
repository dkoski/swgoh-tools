//
//  ContentView.swift
//  BRGTool
//
//  Created by David Koski on 3/17/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import SwiftUI
import SWGOH

struct ContentView: View {
    
    @State var guild1: DataFile<Model.Guild>?
    
    @EnvironmentObject var waiting: WaitingModel

    var body: some View {
        WaitingView {
            VStack {
                Text("Hello, World!")
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                
                Image("download")
                
                Form {
                    ModelSelector(selection: self.$guild1, dataFileManager: DataFileManager.guilds)
                }
                
                Text(self.guild1?.description ?? "None")
            }
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
