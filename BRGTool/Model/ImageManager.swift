//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
import Combine
import AppKit

class ImageManager {
    
    static let shared = ImageManager()
    
    private let queue = DispatchQueue(label: "ImageManager")
    private var cache = [URL:NSImage]()
    
    private init() {
    }
    
    func fetch(url: URL) -> AnyPublisher<NSImage?, Error> {
        if let image = queue.sync(execute: { cache[url] }) {
            return Just(image)
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        }
        
        var request = URLRequest(url: url)
        request.timeoutInterval = 60
        request.cachePolicy = .returnCacheDataElseLoad

        return URLSession.shared.dataTaskPublisher(for: request)
            .checkHTTPResponse()
            .compactMap { NSImage(data: $0) }
            .observe { image in
                self.queue.async {
                    self.cache[url] = image
                }
            }
            .eraseToAnyPublisher()
    }
    
}
