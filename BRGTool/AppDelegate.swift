//
//  AppDelegate.swift
//  BRGTool
//
//  Created by David Koski on 3/17/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import Cocoa
import SwiftUI
import SWGOH

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var windows = [NSWindow]()

    var preferencesWindow: NSWindow!
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        if ProcessInfo.isTesting {
            DataFileManager.isOffline = true
        }
        
        if Defaults.shared.isMissingData {
            showPreferences(self)
        }

        if !ProcessInfo.isTesting {
            showTWOrders(self)
            showTWComparison(self)
        }
    }
    
    @IBAction
    func showManageFiles(_ sender: Any) {
        let contentView = ManageFilesView()
            .environmentObject(WaitingModel())

        let window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
            backing: .buffered, defer: false)
        window.delegate = self
        window.title = "Manage Files"
        window.setFrameAutosaveName("Manage Files")
        window.isReleasedWhenClosed = false
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)
        
        windows.append(window)
    }
    
    @IBAction
    func showSquadEditor(_ sender: Any) {
        let window = KeyboardEventWindow(
            contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
            backing: .buffered, defer: false)
        
        let contentView = SquadEditorView()
            .environmentObject(WaitingModel())
            .environmentObject(window.keyboardEventModel)

        window.delegate = self
        window.title = "Squad Editor"
        window.setFrameAutosaveName("Squad Editor")
        window.isReleasedWhenClosed = false
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)
        
        windows.append(window)
    }
    
    
    @IBAction
    func showPlayerComparison(_ sender: Any) {
        let contentView = PlayerComparisonView()
            .environmentObject(WaitingModel())

        // Create the window and set the content view.
        let window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 480, height: 750),
            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
            backing: .buffered, defer: false)
        window.delegate = self
        window.title = "Player Comparison"
        window.setFrameAutosaveName("Player Comparison")
        window.isReleasedWhenClosed = false
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)
        
        windows.append(window)
    }

    @IBAction
    func showTWComparison(_ sender: Any) {
        let contentView = TWComparisonView()
            .environmentObject(WaitingModel())

        // Create the window and set the content view.
        let window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 480, height: 750),
            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
            backing: .buffered, defer: false)
        window.delegate = self
        window.title = "TW Comparison"
        window.setFrameAutosaveName("TW Comparison")
        window.isReleasedWhenClosed = false
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)
        
        windows.append(window)
    }
    
    @IBAction
    func showDiscord(_ sender: Any) {
        let window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
            backing: .buffered, defer: false)
        
        let viewModel = DiscordViewModel()
        let view = DiscordView()
            .environmentObject(viewModel)
            .environmentObject(DataFileManager.guilds)
        
        window.delegate = self
        window.title = "Discord"
        window.setFrameAutosaveName("Discord")
        window.isReleasedWhenClosed = false
        window.contentView = NSHostingView(rootView: view)
        window.makeKeyAndOrderFront(nil)
        
        windows.append(window)
    }

    @IBAction
    func showTWOrders(_ sender: Any) {
        let contentView = TWView()
            .environmentObject(WaitingModel())

        // Create the window and set the content view.
        let window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
            backing: .buffered, defer: false)
        window.delegate = self
        window.title = "TW Orders"
        window.setFrameAutosaveName("TW Orders")
        window.isReleasedWhenClosed = false
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)
        
        windows.append(window)
    }

    
    func applicationWillResignActive(_ notification: Notification) {
        Defaults.shared.credentials.store()
    }
    
    func applicationWillTerminate(_ aNotification: Notification) {
        Defaults.shared.credentials.store()
    }
    
    @IBAction
    func showPreferences(_ sender: Any) {
        if preferencesWindow == nil {
            // Create the SwiftUI view that provides the window contents.
            let contentView = PreferencesView()
                .environmentObject(WaitingModel())

            // Create the window and set the content view.
            preferencesWindow = NSWindow(
                contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
                styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
                backing: .buffered, defer: false)
            preferencesWindow.center()
            preferencesWindow.setFrameAutosaveName("Preferences")
            preferencesWindow.contentView = NSHostingView(rootView: contentView)
            preferencesWindow.isReleasedWhenClosed = false
        }
        preferencesWindow.makeKeyAndOrderFront(nil)
    }
    

}

extension AppDelegate : NSWindowDelegate {
    
    func windowWillClose(_ notification: Notification) {
        if let window = notification.object as? NSWindow {
            windows.removeAll(where: { $0 === window })
        }
    }
}
