//
//  main.swift
//  geoTB
//
//  Created by David Koski on 8/18/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation
import SWGOH

// NOTE: this builds some internal bits of swgoh-tools

let guildURL = URL(fileURLWithPath: CommandLine.arguments[1])
let guildData = try! Data(contentsOf: guildURL)
var guild = try! JSONDecoder().decode(Model.Guild.self, from: guildData)

//let boardConfiguration = DSGeoTB()
//let squadConfiguration = DSGeoTBSquads()

let boardConfiguration = LSGeoTB()
let squadConfiguration = LSGeoTBSquads()

let s1 = Date.timeIntervalSinceReferenceDate

let preparedMissions = squadConfiguration.prepareMissionConfigurations(players: guild.players)
let missionEstimator = TBSquadMissionEstimator(guild: guild, boardConfiguration: boardConfiguration, squadConfiguration: squadConfiguration, preparedMissions: preparedMissions)

let toonsGP = guild.players.reduce(0) { $0 + $1.stats.characterGP } / 1_000_000
let shipsGP = guild.players.reduce(0) { $0 + $1.stats.shipGP } / 1_000_000

// can be used below to require the KAM mission
let activityContains3S = { (board: TB.Board) in
    board.activity.contains {
        if let section = $0.section {
            return section.phase == 3 && section.name == "Bottom"
        } else {
            return false
        }
    }
}

func compare(b1: TB.Board, b2: TB.Board) -> Bool? {
//    if !activityContains3S(b1) { return false }
    return nil
}

let s = Date.timeIntervalSinceReferenceDate
let tb = TB(toonGP: toonsGP, shipGP: shipsGP, configuration: boardConfiguration, missionEstimator: missionEstimator, planComparator: compare)
let result = tb.simulate()

//print(result)
//print(result.stars)
//print(Date.timeIntervalSinceReferenceDate - s)
//print(Date.timeIntervalSinceReferenceDate - s1)
//exit(0)

// simulate things manually if a mistake is made
if false {
    var result = TB.Board(configuration: boardConfiguration)
    var active = result.activeSections()
    
    result.activity.append(.active(1, active))
    result.deploy(section: active[0], gp: 110)
    result.deploy(section: active[1], gp: 110)
    result = result.nextPhase()
    
    active = result.activeSections()
    result.activity.append(.active(2, active))
    result.deploy(section: active[0], gp: 107)
    result.deploy(section: active[1], gp: 153)
    result.deploy(section: active[2], gp: 153)
    result = result.nextPhase()

    result = tb.simulate(board: result, phase: 3)
    print(result)
    print(result.stars)
    exit(0)
}


var totalGp = [Model.Player:Int]()
var combatWaves = [Model.Player:Int]()
var missionGp = [Model.Player:Int]()
var deployedGp = [Model.Player:Int]()

for phase in 1 ... 4 {
    let active = result.activity.first(where: { $0.phase == phase })!
    let sections: [TB.Key]
    switch active {
    case .active(_, let s):
        sections = s.map { $0.key }
    default:
        fatalError("found wrong activity: \(active)")
    }
    
    for player in guild.players {
        let result = missionEstimator.missionFor(sectors: sections, player: player)
        
        deployedGp[player, default: 0] += player.stats.characterGP
        if sections.contains(where: { $0.isFleet }) {
            deployedGp[player, default: 0] += player.stats.shipGP
        }
        
        for (_, info) in result {
            combatWaves[player, default: 0] += info.stats.waves
            missionGp[player, default: 0] += Int(info.stats.points * 1_000_000)
        }
    }
}

for player in guild.players {
    totalGp[player] = missionGp[player, default: 0] + deployedGp[player, default: 0]
}

for (player, gp) in totalGp.sorted(by: { $0.1 > $1.1 }) {
    func f(_ v: Int) -> Double {
        Double(Int(Double(v) / 1_000_000 * 10)) / 10.0
    }
    print("\(player.name), \(f(gp)), \(f(deployedGp[player, default: 0])), \(f(missionGp[player, default: 0]))")
}

//var board1 = TB.Board(configuration: configuration)
//
//for section in board1.activeSections() {
//  if section.name == "Top" {
//      board1.deploy(section: section, gp: 0)
//  } else if section.name == "Middle" {
//      board1.deploy(section: section, gp: 0)
//    } else if section.name == "Bottom" {
//        board1.deploy(section: section, gp: 0)
//    }
//}
//var board2 = board1.nextPhase()
//
//for section in board2.activeSections() {
//    if section.name == "Top" {
//        board2.deploy(section: section, gp: 85)
//    } else if section.name == "Middle" {
//        board2.deploy(section: section, gp: 167)
//    } else if section.name == "Bottom" {
//        board2.deploy(section: section, gp: 85)
//    }
//}
//let board3 = board2.nextPhase()
//
//let result = tb.simulate(board: board3, phase: 3)
//print(result)
