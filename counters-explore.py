#!/usr/bin/python

# curl https://docs.google.com/spreadsheets/d/1RVo7ej1PE06FKkwS1q5_slB9YLLQX3EF-dN98MkFmOM/export\?exportFormat\=csv > counters.csv

# mess around with the counters data -- for example, show the teams that have significant counter overlap

import csv

c = {}

with open('counters.csv') as f:
    reader = csv.reader(f)
    
    # skip the header
    next(reader)
    
    for row in reader:
        # print(row)
        if row[1] in c:
            c[row[1]].add(row[2])
        else:
            c[row[1]] = { row[2] }
        
for t1 in c:
    for t2 in c:
        if t1 == t2 or t1 < t2:
            continue
            
        if len(c[t1] & c[t2]) > 3:
            print(t1, t2, len(c[t1] & c[t2]), c[t1] & c[t2])
            # print("{0} -- {1} [penwidth={2}]".format(t1, t2, len(c[t1] & c[t2]) - 3))
