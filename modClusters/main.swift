// a variant of modStats used to test clustering, etc.

import Foundation
import SWGOH

let character = Model.Character.find("Darth Vader")

// usage:
// modStats *.json

let serializedModsURL = URL(fileURLWithPath: "/tmp/mods.json")
if (try? !serializedModsURL.checkResourceIsReachable()) ?? true {
    let urls = Array(CommandLine.arguments.dropFirst())
    let guilds = DispatchQueue.concurrentPerform(iterations: urls.count) { index -> (Int, Model.Guild) in
        let url = URL(fileURLWithPath: urls[index])
        let data = try! Data(contentsOf: url)
        return (index, try! JSONDecoder().decode(Model.Guild.self, from: data))
    }

    let players = guilds.values.flatMap { $0.players }
    let units = players.compactMap { $0.units[character.id] }.filter { $0.mods.count == 6 && $0.gear >= 10 }
    
    let data = try! JSONEncoder().encode(units)
    try! data.write(to: serializedModsURL)
}

let serializedRecsURL = URL(fileURLWithPath: "/tmp/mods-rec.json")
let seeds = try? JSONDecoder().decode([String:ModRecommendation].self, from: Data(contentsOf: serializedRecsURL))


let units = try! JSONDecoder().decode([Model.Unit].self, from: Data(contentsOf: serializedModsURL))

let players = units.map { Model.Player(name: "test", allyCode: 0, units: [ $0.id: $0 ])  }

let recommendations = ModRecommendation.build(players: players, seedClusters: seeds, threshold: 0.05)

try! JSONEncoder().encode(recommendations).write(to: serializedRecsURL)

let d = CharactersPages().contents(modRecommendations: recommendations[character.id]!, modHistograms: units.map { ModHistogram(mods: $0.mods).normalized })
try! d.write(to: URL(fileURLWithPath: "/tmp/mods.html"), atomically: false, encoding: .utf8)


extension Sequence where Element: AdditiveArithmetic {
    func cumulative() -> [Element] {
        var sum = Element.zero
        return self.map { v in
            sum += v
            return sum
        }
    }
}

struct CharactersPages {
    
    func showPriorities(_ priorities: [ModHistogram.Priority], counts: Bool = false) -> HTMLFramgment {
        priorities.map { "\($0.c) (\($0.v))"}.joined(separator: ", ")
    }

    func showStats(_ stats: [ModHistogram.Value]) -> HTMLFramgment {
        H.block {
            H.foreach(stats.enumerated()) { (index, stat) in
                if index > 0 {
                    ", "
                }
                stat.c.description
                " +\(stat.v)"
            }
        }
    }
    
    func showMod(mod: Model.Mod, priorities: Set<Model.Mod.Characteristic>) -> HTMLFramgment {
        H.div().class("mod") {
            if priorities.contains(mod.characteristic) {
                H.b() { mod.characteristic.description }
            } else {
                mod.characteristic.description
            }
            
            if priorities.contains(mod.primaryStat.characteristic) {
                H.b { mod.primaryStat.description }
            } else {
                mod.primaryStat.description
            }
            
            H.ul {
                H.foreach(mod.secondaryStat) { stat in
                    H.li {
                        if priorities.contains(stat.characteristic) {
                            H.b { stat.description }
                        } else {
                            stat.description
                        }
                        if let roll = stat.roll {
                            " (\(roll))"
                        }
                    }
                }
            }
        }
    }
        
    func showMods(mods: [Model.Mod], priorities: Set<Model.Mod.Characteristic>) -> HTMLFramgment {
        func find(_ index: Int) -> Model.Mod {
            mods.first { $0.slot == index }!
        }
                
        return H.table().class("mods") {
            H.foreach(stride(from: 1, to: 6, by: 2)) { index in
                H.tr {
                    H.td().class("mod-cell") {
                        showMod(mod: find(index), priorities: priorities)
                    }
                    H.td().class("mod-cell") {
                        showMod(mod: find(index + 1), priorities: priorities)
                    }
                }
            }
        }
    }

    func contents(modRecommendations: ModRecommendation, modHistograms: [ModHistogram]) -> String {
        var result = ""
                
        result +=
            """
            <style>
            #zetas {
                width: 800;
                margin-left: 10;
            }
            #gearLevels {
                width: 400;
                margin-left: 10;
            }
            #picture {
                float: right;
            }
            table.mods {
                margin-top: 5px;
            }
            td.examples {
                padding: 10px;
                font-size: 90%;
                width: 430px;
            }
            .mod-set {
                width: 24px;
                height: 24px;
            }
            .priority {
                border: 1px solid black;
                margin: 2px;
                margin-right: 10px;
                background-color: #c0c0c0;
            }
            .mod-cell {
                border: 1px solid black;
                padding: 2px;
                font-size: 85%;
            }
            </style>
            """

                
        func q<V>(_ value: V) -> String {
            "\"\(value)\""
        }
                
        result += H.block {
            
            """
            <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
            """

            H.h3 {
                "Mod Stat Distributions"
            }
            
            """
            Mod stat distributions across \(modRecommendations.totalCount) units from the
            alliance and top guilds.  The bonuses are shown as applied to an R5 unit.
            Specific distributions are shown below by common mod configurations.
            """
            
            H.h3 {
                "Mod Recommendations (Alliance + Top Guilds)"
            }
            
            """
            <p>
            Based on \(modRecommendations.totalCount) units.  This shows the priorities
            used in assigning mods to the characters.  For example: <b>speed, offense</b>
            would prefer speed over all else and then offense next.  These are not
            necessarily good mod setups, but they are commonly used.

            <p>
            The example stats are shown for these mods being applied to an R5 character.
            """
            
            H.foreach(modRecommendations.clusters.enumerated()) { (index, cluster) in
                let prioritiesSet = Set(cluster.priorities.map { $0.c })
                
                H.h4 {
                    "#\(index + 1) - \(NumberFormatter.percent.string(from: Double(cluster.count) / Double(modRecommendations.totalCount))) - "
                    showPriorities(cluster.priorities)
                }
                
                H.table() {
                    H.tr {
                        H.td().class("examples") {
                            H.b { "Typical Mods: " }
                            "<br/>"
                            showStats(cluster.center.stats)
                            showMods(mods: cluster.center.mods, priorities: prioritiesSet)
                            "score: "
                            modScore(priorities: cluster.priorities, stats: cluster.center.stats)
                        }
                        H.td().class("examples") {
                            H.b { "Great Mods: " }
                            "<br/>"
                            showStats(cluster.maximal.stats)
                            showMods(mods: cluster.maximal.mods, priorities: prioritiesSet)
                            "score: "
                            modScore(priorities: cluster.priorities, stats: cluster.maximal.stats)
                        }
                    }
                }
                
                H.div().id("histo-\(index)").style("height:800px") { }
                
                let (range, distances) = { () -> (StrideTo<Int>, CountedSet<Int>) in
                    var distances = CountedSet<Int>()
                    for mh in modHistograms {
                        let d = cluster.centroidNormalized.distance(from: mh, threshold: 25)
                        distances.add((d / 10) * 10)
                    }
                    let dmin = distances.keys.min() ?? 0
                    let dmax = min(1000, distances.keys.max() ?? 0)
                    return (stride(from: dmin, to: dmax, by: 10), distances)
                }()
                
                """
                <script>
                Plotly.newPlot('histo-\(index)', [
                    {
                        type: "bar",
                        y: [ \(
                            range.map { distances[$0].description }.joined(separator: ", ")
                        )],
                        x: [ \(range.map { $0.description }.joined(separator: ", ")) ],
                    },
                    {
                        type: "line",
                        y: [ \(
                            range.map { distances[$0] }.cumulative().map { Int(Double($0) / Double(distances.count) * 100).description }.joined(separator: ", ")
                        )],
                        x: [ \(range.map { $0.description }.joined(separator: ", ")) ],
                        yaxis: "y2",
                    },
                ], {
                    yaxis2: { overlaying: "y" }
                })
                </script>
                """
            }

        }.render()
                
        return result
    }

}
