//
//  main.swift
//  modTest
//
//  Created by David Koski on 2/1/21.
//  Copyright © 2021 David Koski. All rights reserved.
//

import SWGOH

let guildURL = URL(fileURLWithPath: CommandLine.arguments[1])
let guild = try! JSONDecoder().decode(Model.Guild.self, from: Data(contentsOf: guildURL))

let id = Model.Character.find("Fives").id
let cindex = 0
let p = guild.players.first { $0.stats.allyCode == 369351434 }!
let u = p.units[id]!

let cluster = referenceRecommendations[id]!.clusters[cindex]

let priorities = cluster.priorities
let stats = modStats(character: u.character, gearLevel: u.combinedGearLevel.value, priorities: priorities, mods: u.mods)
let score = modScore(priorities: priorities, stats: stats, primaryBoost: 110)

let targetStats = modStats(character: u.character, gearLevel: u.combinedGearLevel.value, priorities: priorities, mods: cluster.center.mods)
let targetScore = modScore(priorities: priorities, stats: targetStats, primaryBoost: 110)

print(stats)
print(targetStats)
print(priorities)
print(score)
print(targetScore)
print(score - targetScore)

let score2 = modScore(priorities: priorities, stats: stats, targets: targetStats)
print(score2)

exit(0)

let modReference = SiteModReferenceData(recommendations: referenceRecommendations)
try! JSONEncoder().encode(modReference).write(to: URL(fileURLWithPath: "/Users/dkoski/personal/brg/docs/modReference.json"))

let guildModData = SiteGuildModData(guild: guild, recommendations: referenceRecommendations)
try! JSONEncoder().encode(guildModData).write(to: URL(fileURLWithPath: "/Users/dkoski/personal/brg/docs/guild/\(guild.shortName)/modData.json"))
