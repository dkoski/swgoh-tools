// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "MyLibrary",
    platforms: [
        .macOS(.v10_13),
        .linux,
    ],
    products: [
        .executable(name: "swgoh", targets: ["swgoh"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(name: "swgoh", dependencies: []),
    ]
)
