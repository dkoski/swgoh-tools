#!/usr/bin/python

# curl https://docs.google.com/spreadsheets/d/1RVo7ej1PE06FKkwS1q5_slB9YLLQX3EF-dN98MkFmOM/export\?exportFormat\=csv > counters.csv
#
# python counters.py > SWGOH/Generated/Counters+Pairs.swift


import csv

def q(x):
    if len(x) > 0:
        return '\n"""\n{x}\n"""'.format(x = x)
    else:
        return '""'

def convert(type):
    with open('counters.csv') as f:
        reader = csv.reader(f)
    
        # skip the header
        next(reader)
    
        for row in reader:
            if row[4] == type:
                print('Counter(team: "{team}", counter: "{counter}", hard: {hard}, description: {description}, video: #"{video}"#),\n'.format(
                        team = row[1],
                        counter = row[2],
                        hard = "true" if row[3] == "TRUE" else "false",
                        description = q(row[5]),
                        video = row[6],
                    ))

print("// see counters.py")
print("extension Counter {")

print("static let squad5v5 = [")
convert("5v5")
print("]")
print("static let squad3v3 = [")
convert("3v3")
print("]")

print("}")
