// based on https://gist.github.com/Nocross/87b1570f5c1b1fb49fd07136dd8bd8eb

public class OSUnfairLock {
    
    private var unfairLock : UnsafeMutablePointer<os_unfair_lock>
        
    public init() {
        unfairLock = UnsafeMutablePointer.allocate(capacity: 1)
        unfairLock.initialize(to: os_unfair_lock())
    }
        
    deinit {
        unfairLock.deallocate()
    }
    
    public func lock() {
        os_unfair_lock_lock(unfairLock)
    }
    
    public func unlock() {
        os_unfair_lock_unlock(unfairLock)
    }
}
