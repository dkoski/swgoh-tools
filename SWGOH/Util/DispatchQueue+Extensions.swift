//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation

public extension DispatchQueue {
    
    class func concurrentPerform<T>(iterations: Int, execute work: (Int) -> T) -> Set<T> {
        let sem = DispatchSemaphore(value: 1)
        var result = Set<T>()
        
        DispatchQueue.concurrentPerform(iterations: iterations, execute: { index -> Void in
            let v = work(index)
            sem.wait()
            result.insert(v)
            sem.signal()
        })
        
        return result
    }
    
    class func concurrentPerform<K, V>(iterations: Int, execute work: (Int) -> (K, V)) -> [K:V] {
        let sem = DispatchSemaphore(value: 1)
        var result = [K:V]()
        
        DispatchQueue.concurrentPerform(iterations: iterations, execute: { index -> Void in
            let (k, v) = work(index)
            sem.wait()
            result[k] = v
            sem.signal()
        })
        
        return result
    }

}
