//
//  HTML.swift
//  swgoh
//
//  Created by David Koski on 9/1/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

/// how to combine multiple attributes with the same name
public enum HTMLAttributeCombine {
    case replace
    case append(String)
    
    public static func combiner(attribute: String) -> HTMLAttributeCombine {
        switch attribute {
        case "class":
            return .append(" ")
        case "style":
            return .append("; ")
        default:
            return replace
        }
    }
}

public struct HTMLAttribute {
    let key: String
    let value: String
    let combine: HTMLAttributeCombine

    public init(_ key: String, _ value: String, combine: HTMLAttributeCombine? = nil) {
        self.key = key
        self.value = value
        self.combine = combine ?? HTMLAttributeCombine.combiner(attribute: key)
    }
}

public protocol HTMLFramgment : CustomStringConvertible {
    func render() -> String
    
    func parentAttributes() -> [HTMLAttribute]
    
    var isEmpty: Bool { get }
}

public extension HTMLFramgment {
    func parentAttributes() -> [HTMLAttribute] {
        return []
    }
}

extension String : HTMLFramgment {

    public func render() -> String {
        return self
    }
    
}

extension Int : HTMLFramgment {
    
    public var isEmpty: Bool {
        return false
    }
    
    public func render() -> String {
        return description
    }
    
}

public struct HTMLForeach<Data> where Data : Sequence {
    
    var data: Data
    
    var content: (Data.Element) -> HTMLFramgment
    
}

extension HTMLForeach : HTMLFramgment, CustomStringConvertible {
    
    public func render() -> String {
        var result = ""
        
        for element in data {
            result += content(element).render()
        }
        
        return result
    }
    
    public var description: String { return render() }
    
    public var isEmpty: Bool { false }
    
}

public struct HTMLIfLet<Data> {
    
    var data: Data?
    
    var content: (Data) -> HTMLFramgment

}

extension HTMLIfLet : HTMLFramgment, CustomStringConvertible {
    
    public func render() -> String {
        if let value = data {
            return content(value).render()
        } else {
            return ""
        }
    }
    
    public var description: String { return render() }
    
    public var isEmpty: Bool { data == nil }
    
}

public struct HTMLElement : HTMLFramgment {
        
    let element: String
    let attributes: [HTMLAttribute]
    let contents: HTMLFramgment

    public let isEmpty = false
    
    init(element: String, baseAttributes: [String:String] = [:], attributes: [HTMLAttribute], contents: HTMLFramgment) {
        self.element = element
        self.attributes = baseAttributes.isEmpty ? attributes : baseAttributes.map { HTMLAttribute($0, $1) }.adding(attributes)
        self.contents = contents
    }
    
    public func renderAttributes(_ attributes: [HTMLAttribute]) -> String {
        if attributes.isEmpty {
            return ""
        }

        var result = [String:String]()
        
        // TODO move to the enum?
        for attribute in attributes {
            if let current = result[attribute.key] {
                switch attribute.combine {
                case .replace:
                    result[attribute.key] = attribute.value
                    
                case .append(let sep):
                    result[attribute.key] = current + sep + attribute.value
                }
            } else {
                result[attribute.key] = attribute.value
            }
        }
        
        return " " + result.sorted { $0.key < $1.key }.map { "\($0)=\"\($1)\"" }.joined(separator: " ")
    }
    
    public func render() -> String {
        let renderedContent = contents.render()
        if renderedContent.isEmpty {
            return "<\(element)\(renderAttributes(attributes)) />"
        } else {
            return "<\(element)\(renderAttributes(attributes))>" + renderedContent + "</\(element)>"
        }
    }
    
    public var description: String { return render() }

    private func _chainedAttribute(_ key: String, _ value: String, _ content: () -> HTMLFramgment) -> HTMLElement {
        let contents = self.contents.isEmpty ? content() : self.contents
        let newAttributes = attributes.adding(HTMLAttribute(key, value)).adding(contents.parentAttributes())
        return HTMLElement(element: element, attributes: newAttributes, contents: contents)
    }
    
    // MARK:- attributes

    public func align(_ name: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("align", name, content)
    }

    public func `class`(_ name: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("class", name, content)
    }
    
    public func colspan(_ value: Int, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("colspan", value.description, content)
    }

    public func href(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("href", value, content)
    }
    
    public func id(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("id", value, content)
    }

    public func onclick(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("onclick", value, content)
    }

    public func src(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("src", value, content)
    }
    
    public func style(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("style", value, content)
    }

    public func title(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("title", value, content)
    }

    public func valign(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("valign", value, content)
    }

    public func width(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("width", value, content)
    }

    public func order(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("data-order", value, content)
    }

    public func order(_ value: Int, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("data-order", value.description, content)
    }

    public func filter(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("data-filter", value, content)
    }

    public func target(_ value: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _chainedAttribute("target", value.description, content)
    }

}

struct HTMLFragmentSequence : HTMLFramgment {
    
    var children: [HTMLFramgment]
    
    var isEmpty: Bool {
        return !children.contains { !$0.isEmpty }
    }
    
    init(children: [HTMLFramgment]) {
        self.children = children
    }
    
    init() {
        self.children = []
    }
    
    mutating func append(_ fragment: HTMLFramgment) {
        children.append(fragment)
    }

    mutating func append(@HTMLBuilder content: () -> HTMLFramgment) {
        children.append(content())
    }

    public func render() -> String {
        return children.map { $0.render() }.joined(separator: "")
    }
    
    var description: String { return render() }

    public func parentAttributes() -> [HTMLAttribute] {
        return children.flatMap { child -> [HTMLAttribute] in
            if let parentModifer = child as? ParentHTMLModifier {
                return parentModifer.attributes
            } else if let sequence = child as? HTMLFragmentSequence {
                return sequence.parentAttributes()
            } else {
                return []
            }
        }
    }
}

public struct ParentHTMLModifier : HTMLFramgment {
    
    let attributes: [HTMLAttribute]
    
    public let isEmpty = true
    
    public func render() -> String {
        return ""
    }
    
    public var description: String { return render() }
    
    public func parentAttributes() -> [HTMLAttribute] {
        return attributes
    }
}

@resultBuilder
public struct HTMLBuilder {
    
    public static func buildBlock() -> HTMLFramgment {
        return ""
    }

    public static func buildBlock(_ args: HTMLFramgment...) -> HTMLFramgment {
        return HTMLFragmentSequence(children: args)
    }
    
    public static func buildBlock(_ args: HTMLAttribute...) -> [HTMLAttribute] {
        return args
    }

    public static func buildIf(_ arg0: HTMLFramgment?) -> HTMLFramgment {
        return arg0 ?? ""
    }

    public static func buildOptional(_ arg0: HTMLFramgment?) -> HTMLFramgment {
        return arg0 ?? ""
    }

    public static func buildEither(first: HTMLFramgment) -> HTMLFramgment {
        return first
    }
    
    public static func buildEither(second: HTMLFramgment) -> HTMLFramgment {
        return second
    }

}

public struct H {

    //MARK:- structural

    /// add an attribute to the parent HTMLElement
    public static func parent(@HTMLBuilder content: () -> [HTMLAttribute]) -> ParentHTMLModifier {
        return ParentHTMLModifier(attributes: content())
    }
    
    public static func parent(_ attributes: [HTMLAttribute]) -> ParentHTMLModifier {
        return ParentHTMLModifier(attributes: attributes)
    }

    
    public static func foreach<Data, Element>(_ data: Data, @HTMLBuilder content: @escaping (Element) -> HTMLFramgment) -> HTMLForeach<Data> where Data: Sequence, Element == Data.Element {
        return HTMLForeach(data: data, content: content)
    }
    
    public static func ifLet<Data>(_ data: Data?, @HTMLBuilder content: @escaping (Data) -> HTMLFramgment) -> HTMLIfLet<Data> {
        return HTMLIfLet(data: data, content: content)
    }

    public static func block(@HTMLBuilder content: () -> HTMLFramgment) -> HTMLFramgment {
        return content()
    }

    //MARK:- private
    private static func _element(_ name: String, _ attributes: [String:String] = [:], _ content: () -> HTMLFramgment) -> HTMLElement {
        let contents = content()
        return HTMLElement(element: name, baseAttributes: attributes, attributes: contents.parentAttributes(), contents: contents)
    }

    //MARK:- elements
    
    public static func a(href: String, @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("a", ["href" : href], content)
    }

    public static func b(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("b", attributes, content)
    }
    
    static var br: HTMLElement {
        return _element("br", [:], { "" })
    }

    public static func div(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("div", attributes, content)
    }
    
    public static func span(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("span", attributes, content)
    }

    public static func pre(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("pre", attributes, content)
    }

    public static func h1(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("h1", attributes, content)
    }

    public static func h2(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("h2", attributes, content)
    }

    public static func h3(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("h3", attributes, content)
    }

    public static func h4(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("h4", attributes, content)
    }

    public static func i(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("i", attributes, content)
    }

    public static func img(src: String) -> HTMLElement {
        return _element("img", ["src" : src], { "" })
    }

    public static func img(src: URL) -> HTMLElement {
        return _element("img", ["src" : src.description], { "" })
    }

    public static func p(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("p", attributes, content)
    }

    public static func table(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("table", attributes, content)
    }
    
    public static func thead(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("thead", attributes, content)
    }
    
    public static func tfoot(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("tfoot", attributes, content)
    }
    
    public static func th(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("th", attributes, content)
    }

    public static func tr(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment) -> HTMLElement {
        return _element("tr", attributes, content)
    }
    
    public static func td(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("td", attributes, content)
    }

    public static func ul(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("ul", attributes, content)
    }

    public static func ol(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("ol", attributes, content)
    }

    public static func li(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("li", attributes, content)
    }

    public static func script(_ attributes: [String:String] = [:], @HTMLBuilder content: () -> HTMLFramgment = { "" }) -> HTMLElement {
        return _element("script", attributes, content)
    }

    public static func script(src: String) -> HTMLFramgment {
        // need to suppress the <script/> -- browser doesn't like that
        "<script src=\"\(src)\"></script>"
    }

}
