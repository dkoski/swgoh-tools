//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation

public extension String {
    
    func matchesWord<T: StringProtocol>(_ lowerCaseWord:T) -> Bool {
        let lower = self.lowercased()
        
        if lower.hasPrefix(lowerCaseWord) {
            return true
        }
        if lower.contains(" " + lowerCaseWord) {
            return true
        }
        
        return false
    }
    
}
