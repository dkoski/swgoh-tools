//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation

public extension ProcessInfo {
    
    static var isRunningXCTest: Bool = {
        NSClassFromString("XCTest") != nil
    }()

    static var isRunningSwiftUIPreviews: Bool = {
        // look for XCODE_RUNNING_FOR_PREVIEWS
        ProcessInfo.processInfo.environment["XCODE_RUNNING_FOR_PREVIEWS"] == "1"
    }()

    /// set the OFFLINE evironment variable to 1 to trigger offline (testing) mode
    static var isOffline: Bool = {
        ProcessInfo.processInfo.environment["OFFLINE"] == "1"
    }()
    
    static var isTesting: Bool = {
        isRunningXCTest || isRunningSwiftUIPreviews || isOffline
    }()

}
