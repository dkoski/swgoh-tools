//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation

public extension Int {
    
    var asAllyCode: String {
        if self == 0 {
            return ""
        }
        
        var string = self.description
        if string.count >= 3 {
            string.insert("-", at: string.index(string.startIndex, offsetBy: 3))
        }
        if string.count >= 7 {
            string.insert("-", at: string.index(string.startIndex, offsetBy: 7))
        }
        return string
    }
    
    var isValidAllyCode: Bool {
        self >= 100_000_000 && self <= 999_999_999
    }

    init?(allyCode: String) {
        var allyCode = allyCode
        allyCode.removeAll(where: { !$0.isNumber })
        self.init(allyCode)
    }
}

public extension String {
    
    var isValidAllyCode: Bool {
        Int(self)?.isValidAllyCode ?? false
    }
    
}

public class AllyCodeFormatter : Formatter {
    
    public override func string(for obj: Any?) -> String? {
        if let value = obj as? Int {
            return value.asAllyCode
            
        } else if let value = obj as? String {
            return value
        }
        return nil
    }
    
    public override func getObjectValue(_ obj: AutoreleasingUnsafeMutablePointer<AnyObject?>?, for string: String, errorDescription error: AutoreleasingUnsafeMutablePointer<NSString?>?) -> Bool {
        if let value = Int(allyCode: string) {
            obj?.pointee = NSNumber(value: value)
            return true
        }
        
        error?.pointee = "Unable to convert allycode: \(string)" as NSString
        return false
    }
    
}
