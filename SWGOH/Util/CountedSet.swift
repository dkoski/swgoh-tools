//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation

private var defaultRandomNumberGenerator = SystemRandomNumberGenerator()

public struct CountedSet<Element> where Element: Hashable {
    
    var contents: [Element:Int]
    public private(set) var count: Int
    
    public init() {
        contents = [:]
        count = 0
    }
    
    public init(keysAndValues: [(Element, Int)]) {
        contents = Dictionary(uniqueKeysWithValues: keysAndValues)
        count = contents.reduce(0, { $0 + $1.value })
    }
    
    public init(contents: [Element:Int]) {
        self.contents = contents
        count = contents.reduce(0, { $0 + $1.value })
    }
    
    public init(items: [Element]) {
        contents = [:]
        count = 0
        for item in items {
            add(item)
        }
    }
    
    public var isEmpty: Bool {
        return count == 0
    }
    
    public mutating func remove(_ item: Element, count: Int = 1) {
        if contents[item, default:0] < count {
            fatalError("unable to remove \(item) -- not enough present")
        }
        
        contents[item, default:0] -= count
        self.count -= count
    }
    
    public mutating func add(_ item: Element, count: Int = 1) {
        contents[item, default:0] += count
        self.count += count
    }
    
    public mutating func add(_ other: CountedSet<Element>) {
        for (key, count) in other.contents {
            contents[key, default:0] += count
            self.count += count
        }
    }

    public func anyElement() -> Element {
        return anyElement(using: &defaultRandomNumberGenerator)
    }
    
    public func anyElement<G: RandomNumberGenerator>(using generator: inout G) -> Element {
        var i = Int.random(in: 0 ..< count, using: &generator)
        for (key, count) in contents {
            if i < count {
                return key
            } else {
                i -= count
            }
        }
        fatalError("anyElement failed")
    }
    
    public var asArray: [Element] {
        var result = [Element]()
        
        for (key, count) in contents {
            for _ in 0 ..< count {
                result.append(key)
            }
        }

        return result
    }
    
    public var asDictionary: [Element:Int] {
        return contents
    }
    
    public var keys: [Element] {
        return contents.keys.map { $0 }
    }
    
    public func randomSubset(count: Int) -> CountedSet<Element> {
        return randomSubset(count: count, using: &defaultRandomNumberGenerator)
    }
    
    public func randomSubset<G: RandomNumberGenerator>(count: Int, using generator: inout G) -> CountedSet<Element> {
        if count > self.count {
            fatalError("asking for too many elements")
        }
        
        if count == self.count {
            return self
        }
        
        if count < (self.count - count) {
            // faster to build it up
            var result = CountedSet<Element>()
            
            for _ in 0 ..< count {
                let item = self.anyElement(using: &generator)
                result.add(item)
            }
            
            return result
            
        } else {
            // faster to cut it down
            var result = self
            
            for _ in 0 ..< (self.count - count) {
                let item = result.anyElement(using: &generator)
                result.remove(item)
            }
            
            return result
        }
    }
    
    public func subset(_ keys:[Element]) -> CountedSet<Element> {
        return CountedSet(contents: contents.filter { keys.contains($0.0) })
    }
    
    public subscript(_ key: Element) -> Int {
        return contents[key, default:0]
    }
    
    public func subtracting(_ other:CountedSet<Element>) -> CountedSet<Element> {
        var result = self
        
        for (key, value) in other.contents {
            result.remove(key, count: value)
        }
        
        return result
    }
}

extension CountedSet : Codable where Element : Codable {
    
}
