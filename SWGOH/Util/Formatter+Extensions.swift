//
//  Formatter+Extensions.swift
//  swgoh
//
//  Created by David Koski on 3/29/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation

public extension NumberFormatter {
    
    static let generic = NumberFormatter()
    
    static let integer: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        nf.maximumFractionDigits = 0
        nf.hasThousandSeparators = true
        return nf
    }()
    
    static let decimal: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .decimal
        return nf
    }()

    static let percent: NumberFormatter = {
        let nf = NumberFormatter()
        nf.numberStyle = .percent
        return nf
    }()

    func string(from obj: Int, missing: String = "") -> String {
        string(from: NSNumber(value: obj)) ?? missing
    }

    func string(from obj: Double, missing: String = "") -> String {
        string(from: NSNumber(value: obj)) ?? missing
    }
    
}
