//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

// TODO split

public extension Int {
    
    var millions: String {
        if self < 10_000 {
            return "\(self)"
        }
        if self < 1_000_000 {
            return "\(self / 1_000)k"
        }
        if self < 50_000_000 {
            return "\(self / 1_000_000).\((self % 1_000_000)/100_000)m"
        }
        return "\(self / 1_000_000)m"
    }
    
}

public extension Array where Element: Equatable {
    
    func removing(_ value: Element) -> [Element] {
        var copy = self
        copy.removeAll { $0 == value }
        return copy
    }
    
    func removing(_ values: Element...) -> [Element] {
        var copy = self
        copy.removeAll { values.contains($0) }
        return copy
    }
    
    func removing(_ values: [Element]) -> [Element] {
        var copy = self
        copy.removeAll { values.contains($0) }
        return copy
    }

}

public extension Array {
    
    func adding(_ values: Element...) -> [Element] {
        var copy = self
        copy.append(contentsOf: values)
        return copy
    }
    
    func adding(_ values: [Element]) -> [Element] {
        var copy = self
        copy.append(contentsOf: values)
        return copy
    }

}

public extension Dictionary {
    
    func union(_ other: [Key:Value]) -> [Key:Value] {
        return self.merging(other, uniquingKeysWith: { v1, v2 in v1 })
    }
    
}

public class MemoizeCache<Key, Value> where Key : Hashable {
    
    let lock = OSUnfairLock()
    
    public init() {
    }
    
    var cache = [Key:Value]()
    
    public subscript(_ key: Key) -> Value? {
        get {
            lock.lock()
            let result = cache[key]
            lock.unlock()
            return result
        }
        
        set {
            lock.lock()
            cache[key] = newValue
            lock.unlock()
        }
    }
    
    public subscript(_ key: Key, default defaultValue: @autoclosure () -> Value) -> Value {
        get {
            lock.lock()
            let result = cache[key, default: defaultValue()]
            lock.unlock()
            return result
        }
    }
    
    public var values: [Key:Value] {
        lock.lock()
        let result = cache
        lock.unlock()
        return result
    }

}
