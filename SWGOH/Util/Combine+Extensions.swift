//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
import Combine

import os

private let logger = OSLog(subsystem: "com.koski.swgoh", category: "Combine")

enum AwaitError : Error {
    /// we got a completion but there was no value and no error
    case noValueNoError
}

public extension Publisher where Failure : Error {
    
    func await() throws -> Self.Output {
        let group = DispatchGroup()
        let queue = DispatchQueue(label: "await")
        
        var result: Result<Output, Failure>!
        
        var sawValue = false
        var c: Any?
        
        group.enter()
        let cancellable = self
            .receive(on: queue)
            .sink(receiveCompletion: { (completion) in
                c = completion
                if case .failure(let error) = completion {
                    result = .failure(error)
                }
                group.leave()
            }) { value in
                sawValue = true
                result = .success(value)
            }
        group.wait()
        cancellable.cancel()
        
        if result == nil {
            Swift.print("sawValue = \(sawValue), completion = \(String(describing: c))")
            throw AwaitError.noValueNoError
        }
        
        return try result.get()
    }
    
}

public extension Publisher where Failure == Never {
    
    func await() -> Self.Output {
        let group = DispatchGroup()
        let queue = DispatchQueue(label: "await")

        var result: Output!
        
        group.enter()
        let cancellable = self
            .receive(on: queue)
            .sink(receiveCompletion: { (completion) in
                group.leave()
            }) { value in
                result = value
            }
        group.wait()
        cancellable.cancel()
        
        return result
    }

}

public extension Publisher {
    
    func memoize(in box: UpdatableBox<Output>) -> AnyPublisher<Output, Failure> {
        if let value = box.value {
            return Just(value)
                .setFailureType(to: Failure.self)
                .eraseToAnyPublisher()
        } else {
            return map { v -> Output in
                box.value = v
                return v
            }
            .eraseToAnyPublisher()
        }
    }
    
}

/// a boxed holder of values that can be written from any thread.  e.g. you could use this in a Combine chain to memoize a login token
public class UpdatableBox<T> {
    let queue = DispatchQueue(label: "UpdatableBox")
    private var storage: T?
    var value: T? {
        get { queue.sync { storage } }
        set { queue.sync { storage = newValue }}
    }
}

public extension Publisher where Output == URLSession.DataTaskPublisher.Output {

    func checkHTTPResponse() -> Publishers.TryMap<Self, Data> {
        tryMap { output -> Data in
            guard let httpResponse = output.response as? HTTPURLResponse else {
                throw DownloadError.generic("Unable to cast \(output.response) to HTTPURLResponse")
            }
            guard 200...299 ~= httpResponse.statusCode else {
                os_log(.error, log: logger, "Bad response: %d, %@", httpResponse.statusCode, httpResponse)
                if httpResponse.statusCode == 429 {
                    throw DownloadError.rateLimit
                } else {
                    throw DownloadError.responseCode(httpResponse.statusCode)
                }
            }
            return output.data
        }
    }
}

public extension Publisher {
    
    func observe(block: @escaping (Output) -> Void) -> Publishers.Map<Self, Output> {
        map { value -> Output in
            block(value)
            return value
        }
    }
    
}

extension Publisher where Output == Data {

    func writeToFile(_ path: String) -> AnyPublisher<Data, Failure> {
        map { output -> Data in
            try! output.write(to: URL(fileURLWithPath: path))
            return output
        }
        .eraseToAnyPublisher()
    }
}
