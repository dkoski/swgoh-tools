//
//  Zip.swift
//  SWGOH
//
//  Created by David Koski on 12/27/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

public func zip3<A: Sequence, B: Sequence, C: Sequence>(_ a: A, _ b: B, _ c: C)
    -> Zip3Sequence<A, B, C>
{
    return Zip3Sequence(a, b, c)
}

public struct Zip3Sequence<A: Sequence, B: Sequence, C: Sequence>: Sequence {
    private var a: A
    private var b: B
    private var c: C
    
    public typealias Element = (A.Element, B.Element, C.Element)
    
    public init (_ a: A, _ b: B, _ c: C) {
        self.a = a
        self.b = b
        self.c = c
    }
    
    public struct Iterator: IteratorProtocol {
        private var a: A.Iterator
        private var b: B.Iterator
        private var c: C.Iterator
        
        public init(_ a: A.Iterator, _ b: B.Iterator, _ c: C.Iterator) {
            self.a = a
            self.b = b
            self.c = c
        }
        
        mutating public func next() -> (A.Element, B.Element, C.Element)? {
            switch (a.next(), b.next(), c.next()) {
            case let (.some(aValue), .some(bValue), .some(cValue)):
                return (aValue, bValue, cValue)
            default:
                return nil
            }
        }
    }
    
    public func makeIterator() -> Zip3Sequence<A, B, C>.Iterator {
        Iterator(a.makeIterator(), b.makeIterator(), c.makeIterator())
    }
}
