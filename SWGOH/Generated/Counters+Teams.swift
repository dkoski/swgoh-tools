// see counters.py
extension CounterTeam {
 static let counterTeams = [
"501ST_ECHO": CounterTeam(id: "501ST_ECHO", name: "501st (Gen. Skywalker L w/ Echo)", type: .offense, squad: [#"General Skywalker"#, #"CT-21-0408 "Echo""#, #""#, #""#, #""#], zetas: [true, true, false, false, false], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"501ST_3v3_FIVESECHO": CounterTeam(id: "501ST_3v3_FIVESECHO", name: "501st (Gen. Skywalker L w/ Fives & Echo)", type: .both, squad: [#"General Skywalker"#, #"CT-5555 "Fives""#, #"CT-21-0408 "Echo""#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"501ST": CounterTeam(id: "501ST", name: "501st (Gen. Skywalker L)", type: .both, squad: [#"General Skywalker"#, #"ARC Trooper"#, #"CT-7567 "Rex""#, #"CT-21-0408 "Echo""#, #"CT-5555 "Fives""#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:
"""
General Skywalker's leadership uses the Advance and Cover mechanics.  In Advance, if GAS has 501st allies, they can't lose health and he gets an undispellable taunt until his protection is dropped to 0%.  With 501st allies present and GAS's protection at 0%, then he goes into Cover where he can't be targeted and is immune to damage and status effects.
""", strategy:
"""
Ideally, you want to knock down General Skywalker by dropping his protection to 0%, then take out the clones before he returns from Cover.<br/><br/>

Try to defeat CT-5555 "Fives" first.  Or else, he will sacrifice himself with his Tactical Awareness zeta, which will buff the remaining 501st Clone Troopers and may make it impossible to defeat the remaining allies before GAS returns from his first round of Cover.  Next take out CT-7567 "Rex" before he can get a big hit with Aerial Advantage., CT-24-0408 "Echo", the last ally, then General Skywalker last.<br/><br/>

Kill order: Fives, Rex, Echo, last ally, GAS
"""),
"501ST_3v3": CounterTeam(id: "501ST_3v3", name: "501st (Gen. Skywalker L)", type: .defense, squad: [#"General Skywalker"#, #"Ahsoka Tano"#, #"ARC Trooper"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:
"""
General Skywalker's leadership uses the Advance and Cover mechanics.  In Advance, if GAS has 501st allies, they can't lose health and he gets an undispellable taunt until his protection is dropped to 0%.  With 501st allies present and GAS's protection at 0%, then he goes into Cover where he can't be targeted and is immune to damage and status effects.
""", strategy:
"""
Ideally, you want to knock down General Skywalker by dropping his protection to 0%, then take out the clones before he returns from Cover.<br/><br/>

Try to defeat CT-5555 "Fives" first.  Or else, he will sacrifice himself with his Tactical Awareness zeta, which will buff the remaining 501st Clone Troopers and may make it impossible to defeat the remaining allies before GAS returns from his first round of Cover.  Next take out CT-7567 "Rex" before he can get a big hit with Aerial Advantage., CT-24-0408 "Echo", the last ally, then General Skywalker last.<br/><br/>

Kill order: Fives, Rex, Ahsoka Tano, any other possible ally, GAS
"""),
"BH_BOSSK_3v3_BOBACAD": CounterTeam(id: "BH_BOSSK_3v3_BOBACAD", name: "Bounty Hunters (Bossk L w/ Boba and Cad)", type: .both, squad: [#"Bossk"#, #"Boba Fett"#, #"Cad Bane"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"BH_BOSSK_3v3_MANDO": CounterTeam(id: "BH_BOSSK_3v3_MANDO", name: "Bounty Hunters (Bossk L w/ Mando)", type: .offense, squad: [#"Bossk"#, #"The Mandalorian"#, #"Boba Fett"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"BH_BOSSK": CounterTeam(id: "BH_BOSSK", name: "Bounty Hunters (Bossk L)", type: .defense, squad: [#"Bossk"#, #"Boba Fett"#, #"Dengar"#, #"Jango Fett"#, #""#], zetas: [true, true, true, false, false], subs: #"Any leftover Bounty Hunters, B2 Super Battle Droid, zBarriss Offee, L3-37"#, description:
"""
Bossk's leadership gives all Bounty Hunters extra protection, tenacity, and defence (while at full health).  They recover health and protection when an enemy suffers or resists a debuff.  When they fulfill their contract, all Bounty Hunters gain +50 speed for the rest of the battle on top of their own payouts.
""", strategy:
"""
Bossk's contract is exploitable.  They need to damage the weaknes enemy 10 times, so you can slow this down using taunters.<br/><br/>

You'll want to deal with Bossk by having someone that can either dispel his taunt or apply ability block/buff immunity before he can taunt.<br/><br/>

You'll need good single target damage because this team will heal frequently and the Fetts will revive, so you'll need to kill them twice.<br/><br/>

Kill order: Boba Fett, Jango Fett, and other Bounty Hunters, Bossk, then Dengar last (because he'll probably be in stealth until the end).
"""),
"BH_BOSSK_3v3": CounterTeam(id: "BH_BOSSK_3v3", name: "Bounty Hunters (Bossk L)", type: .both, squad: [#"Bossk"#, #"Boba Fett"#, #"Dengar"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:
"""
Bossk's leadership gives all Bounty Hunters extra protection, tenacity, and defence (while at full health).  They recover health and protection when an enemy suffers or resists a debuff.  When they fulfill their contract, all Bounty Hunters gain +50 speed for the rest of the battle on top of their own payouts.
""", strategy:
"""
Bossk's contract is exploitable.  They need to damage the weaknes enemy 10 times, so you can slow this down using taunters.<br/><br/>

You'll want to deal with Bossk by having someone that can either dispel his taunt or apply ability block/buff immunity before he can taunt.<br/><br/>

You'll need good single target damage because this team will heal frequently and the Fetts will revive, so you'll need to kill them twice.<br/><br/>

Kill order: Boba Fett, Jango Fett, and other Bounty Hunters, Bossk, then Dengar last (because he'll probably be in stealth until the end).
"""),
"BH_DENGAR_3v3_MANDOGREEF": CounterTeam(id: "BH_DENGAR_3v3_MANDOGREEF", name: "Bounty Hunters (Dengar L w/ Mando and Greef)", type: .offense, squad: [#"Dengar"#, #"The Mandalorian"#, #"Greef Karga"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"BH_JANGO": CounterTeam(id: "BH_JANGO", name: "Bounty Hunters (Jango L)", type: .defense, squad: [#"Jango Fett"#, #"Bossk"#, #"Boba Fett"#, #"Dengar"#, #"Zam Wesell"#], zetas: [true, true, true, false, false], subs: #"Any leftover Bounty Hunters, Enfys Nest, L3-37"#, description:
"""
Jango Fett's leadership gives Scoundrel allies +30 speed, punishes your team for putting debuffs them by lowering your potency and tenacity, and prevents turn meter for anyone with the Burning effect.  When they fulfill their contract, all Bounty Hunters gain offense on top of their own payouts.
""", strategy:
"""
You want to counter with a team that doesn't rely on debuffs because it will cripple your team.  Jango Fett's contract is fulfilled by damaging debuffed enemies.  So to slow down or even prevent their contract from being fulfilled, use a team that has high tenacity, feeds off of debuffs, or can easily cleanse debuffs before being attacked.<br/><br/>

You'll want to deal with Bossk by having someone that can either dispel his taunt or apply ability block/buff immunity before he can taunt.<br/><br/>

You'll need good single target damage because this team will heal frequently and the Fetts will revive, so you'll need to kill them twice.<br/><br/>

Kill order: Boba Fett, Jango Fett, and other Bounty Hunters, Bossk, then Dengar last (because he'll probably be in stealth until the end).
"""),
"CLONES_CODY": CounterTeam(id: "CLONES_CODY", name: "Clones (Cody L)", type: .offense, squad: [#"CC-2224 "Cody""#, #"Clone Sergeant - Phase I"#, #"CT-7567 "Rex""#, #"CT-21-0408 "Echo""#, #"CT-5555 "Fives""#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"CLONES_SHAAKTI_3v3_ECHOFIVES": CounterTeam(id: "CLONES_SHAAKTI_3v3_ECHOFIVES", name: "Clones (Shaak Ti L w/ Echo and Fives)", type: .offense, squad: [#"Shaak Ti"#, #"CT-21-0408 "Echo""#, #"CT-5555 "Fives""#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"CLONES_SHAAKTI_JKA": CounterTeam(id: "CLONES_SHAAKTI_JKA", name: "Clones (Shaak Ti L w/ JKA)", type: .offense, squad: [#"Shaak Ti"#, #"CT-21-0408 "Echo""#, #"CT-7567 "Rex""#, #"Jedi Knight Anakin"#, #"CT-5555 "Fives""#], zetas: [true, true, true, true, true], subs: #""#, description:"", strategy:""),
"CLONES_SHAAKTI": CounterTeam(id: "CLONES_SHAAKTI", name: "Clones (Shaak Ti L)", type: .offense, squad: [#"Shaak Ti"#, #"Clone Sergeant - Phase I"#, #"CT-7567 "Rex""#, #"CT-21-0408 "Echo""#, #"CT-5555 "Fives""#], zetas: [true, true, false, false, false], subs: #"ARC Trooper, CC-2224 "Cody", Barriss Offee, Ahsoka Tano, R2-D2"#, description:
"""
Shaak Ti's leadership gives Clone Troopers speed, health and protection recovery when attacking out of turn, and an increase in max health, protection, and offense.  She also gives Retribution to herself or Clones when they take damage.
""", strategy:
"""
The best counters are teams that exploit assists/attacks out-of turn, apply mass buff immunity, or can withstand the high damage.<br/><br/>

Try to defeat CT-5555 "Fives" first.  Or else, he will sacrifice himself with his Tactical Awareness zeta, which will buff the remaining 501st Clone Troopers.  Next take out CT-7567 "Rex" before he can get a big hit with Aerial Advantage.<br/><br/>

Kill order: Fives, Rex, Shaak Ti, the remaining allies
"""),
"CLONES_SHAAKTI_3v3": CounterTeam(id: "CLONES_SHAAKTI_3v3", name: "Clones (Shaak Ti L)", type: .both, squad: [#"Shaak Ti"#, #"CT-7567 "Rex""#, #"CT-5555 "Fives""#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:
"""
Shaak Ti's leadership gives Clone Troopers speed, health and protection recovery when attacking out of turn, and an increase in max health, protection, and offense.  She also gives Retribution to herself or Clones when they take damage.
""", strategy:
"""
The best counters are teams that exploit assists/attacks out-of turn, apply mass buff immunity, or can withstand the high damage.<br/><br/>

Try to defeat CT-5555 "Fives" first.  Or else, he will sacrifice himself with his Tactical Awareness zeta, which will buff CT-7567 "Rex".  Next take out Rex before he can get a big hit with Aerial Advantage.<br/><br/>

Kill order: Fives, Rex, Shaak Ti
"""),
"MALAK_SOLO": CounterTeam(id: "MALAK_SOLO", name: "Darth Malak (solo)", type: .offense, squad: [#"Darth Malak"#, #""#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"SION_SOLO": CounterTeam(id: "SION_SOLO", name: "Darth Sion (solo)", type: .offense, squad: [#"Darth Sion"#, #""#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"VADER_3v3": CounterTeam(id: "VADER_3v3", name: "Darth Vader L", type: .defense, squad: [#"Darth Vader"#, #"Mission Vao"#, #"Zaalbar"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:
"""
Darth Vader's leadership gives Empire and Sith allies offense and a chance to remove turn meter.  While Vader is alive, enemies immediately regain Damage Over Time for 2 turns whenever it expires on them.
""", strategy:
"""
The best counters will be able to either exploit or cleanse the massive amounts of Damage Over Time that Darth Vader, Mission Vao, and Zaalbar will be applying, not including the other status effects they apply.<br/><br/>

You'll also need a way to either dispel or attack around Zaalbar's taunt, or else you'll be stuck getting counterattacked from him while the others continuously debuff you.<br/><br/>

Kill order: Vader or Vao, Zaalbar
"""),
"VADER": CounterTeam(id: "VADER", name: "Darth Vader L", type: .both, squad: [#"Darth Vader"#, #"Emperor Palpatine"#, #"Grand Moff Tarkin"#, #"TIE Fighter Pilot"#, #"Imperial Probe Droid"#], zetas: [true, false, false, false, false], subs: #""#, description:
"""
Darth Vader's leadership gives Empire and Sith allies offense and a chance to remove turn meter.  While Vader is alive, enemies immediately regain Damage Over Time for 2 turns whenever it expires on them.
""", strategy:""),
"VADER_3v3_GEOKILL": CounterTeam(id: "VADER_3v3_GEOKILL", name: "Darth Vader L (Geo Killer)", type: .defense, squad: [#"Darth Vader"#, #"Sith Trooper"#, #"Darth Sidious"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:
"""
Darth Vader's leadership gives Empire and Sith allies offense and a chance to remove turn meter.  While Vader is alive, enemies immediately regain Damage Over Time for 2 turns whenever it expires on them.
""", strategy:""),
"VADER_SLKRKILLER": CounterTeam(id: "VADER_SLKRKILLER", name: "Darth Vader L (SLKR Killer)", type: .offense, squad: [#"Darth Vader"#, #"Darth Malak"#, #"Kylo Ren (Unmasked)"#, #"Grand Admiral Thrawn"#, #"Wat Tambor"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"VADER_3v3_MALAKRG": CounterTeam(id: "VADER_3v3_MALAKRG", name: "Darth Vader L (w/ Malak and Royal Guard)", type: .offense, squad: [#"Darth Vader"#, #"Darth Malak"#, #"Royal Guard"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"DK_ANTI_REBEL": CounterTeam(id: "DK_ANTI_REBEL", name: "Director Krennic (Anti-Rebel)", type: .offense, squad: [#"Director Krennic"#, #"Death Trooper"#, #"Shoretrooper"#, #"Range Trooper"#, #"Colonel Starck"#], zetas: [true, true, true, true, false], subs: #"Enfys Nest, Wampa, Stormtrooper"#, description:"", strategy:""),
"DROIDS_ANTI_JEDI_3v3": CounterTeam(id: "DROIDS_ANTI_JEDI_3v3", name: "Droids (Anti-Jedi)", type: .offense, squad: [#"HK-47"#, #"IG-88"#, #"L3-37"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"DROIDS_ANTI_MALAK": CounterTeam(id: "DROIDS_ANTI_MALAK", name: "Droids (Anti-Malak)", type: .offense, squad: [#"IG-88"#, #"General Grievous"#, #"BB-8"#, #"T3-M4"#, #"L3-37"#], zetas: [true, false, false, false, false], subs: #"R2-D2, Chopper, Imperial Probe Droid"#, description:"", strategy:""),
"EMPIRE_TARKIN_3v3": CounterTeam(id: "EMPIRE_TARKIN_3v3", name: "Empire (Tarkin L)", type: .offense, squad: [#"Grand Moff Tarkin"#, #"TIE Fighter Pilot"#, #"Death Trooper"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"EMPIRE_THRAWN_3v3": CounterTeam(id: "EMPIRE_THRAWN_3v3", name: "Empire (Thrawn L)", type: .offense, squad: [#"Grand Admiral Thrawn"#, #""#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"NEST_SOLO": CounterTeam(id: "NEST_SOLO", name: "Enfys Nest (solo)", type: .offense, squad: [#"Enfys Nest"#, #""#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #"Qi'ra (L), Hermit Yoda"#, description:
"""
Enfys Nest (solo) excels against teams that have a high attack rate but low power output.  She has a high counter chance and ignores taunts.  She gains stacking crit chance and protection every time she is damaged before her turn (these reset at the end of her turn).  And throughout the encounter, she also gains crit damage and health steal whenever an enemy loses a buff or debuff.
""", strategy:
"""
You want teams/characters with Daze, Stun, Stealth, Thermal Detonators, and/or possibly Tenacity Down.  You don't want to use teams that reduce her turn meter, as it'll delay her from shedding the undispellable Protection Up on her turn, which could cause a time-out situation.<br/><br/>

The ideal strategy for most teams is to either apply debuffs or use a basic attack (tryin not to reduce turn meter or prompt assists) until she takes a turn, then the next time you deal damage should be a high-damage single target attack.  It may be beneficial to save abilities that don't deal damage for this moment, so you don't waste your first damage-dealing attack after her turn with a low-damage attack that she could easily heal from.
"""),
"EP_ANTI_REBEL": CounterTeam(id: "EP_ANTI_REBEL", name: "EP (Anti-Rebel)", type: .offense, squad: [#"Emperor Palpatine"#, #"Shoretrooper"#, #"Director Krennic"#, #"Death Trooper"#, #"Wampa"#], zetas: [true, true, true, true, false], subs: #"Enfys Nest, Range Trooper, Stormtrooper, Colonel Starck"#, description:"", strategy:""),
"EP_ANTI-TRAYA": CounterTeam(id: "EP_ANTI-TRAYA", name: "EP (Anti-Traya)", type: .offense, squad: [#"Emperor Palpatine"#, #"Darth Vader"#, #"Grand Moff Tarkin"#, #"Death Trooper"#, #"Imperial Probe Droid"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"EP_TRIO": CounterTeam(id: "EP_TRIO", name: "EP (Sith Triumvirate)", type: .offense, squad: [#"Emperor Palpatine"#, #"Darth Traya"#, #"Darth Nihilus"#, #"Darth Sion"#, #"Grand Admiral Thrawn"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"EP_TRAYA": CounterTeam(id: "EP_TRAYA", name: "EP (w/ Traya)", type: .offense, squad: [#"Emperor Palpatine"#, #"Darth Traya"#, #"Darth Nihilus"#, #"Sith Empire Trooper"#, #""#], zetas: [true, true, true, false, false], subs: #"Another pre-taunting Tank for Sith Empire Trooper, Darth Vader, Bastila Shan (Fallen), Darth Revan"#, description:"", strategy:""),
"EP": CounterTeam(id: "EP", name: "EP L", type: .both, squad: [#"Emperor Palpatine"#, #"Darth Vader"#, #"Grand Moff Tarkin"#, #"TIE Fighter Pilot"#, #"Imperial Probe Droid"#], zetas: [true, true, false, false, false], subs: #"Count Dooku, Death Trooper, Director Krennic"#, description:
"""
Emperor Palpatine's leadership grants Empire and Sith allies potency and max health.  It reduces potency and evasion for Jedi and Rebel enemies.  This is a debuff-driven team, granting Empire allies turn meter for applying debuffs, Sith allies health for applying debuffs, and turn meter for both when enemy debuffs expire.
""", strategy:
"""
You'll need to stop the turn meter gain with Daze, Tenacity Up, and/or by outspeeding them and killing the biggest debuffers before the unstoppable train.<br/><br/>

Kill order: Grand Admiral Thrawn or Darth Vader, Emperor Palpatine, Grand Moff Tarkin or TIE Fighter Pilot
"""),
"EP_3v3": CounterTeam(id: "EP_3v3", name: "EP L", type: .defense, squad: [#"Emperor Palpatine"#, #"Darth Vader"#, #"Sith Empire Trooper"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:
"""
Emperor Palpatine's leadership grants Empire and Sith allies potency and max health.  It reduces potency and evasion for Jedi and Rebel enemies.  This is a debuff-driven team, granting Empire allies turn meter for applying debuffs, Sith allies health for applying debuffs, and turn meter for both when enemy debuffs expire.
""", strategy:
"""
You'll need to stop the turn meter gain with Daze, Tenacity Up, and/or by outspeeding them and killing the biggest debuffers before the unstoppable train.<br/><br/>

You may also need to consider having a character that can dispel a taunt, so you don't get stuck behind a tank before the turn meter train starts.<br/><br/>

Kill order: Grand Admiral Thrawn or Darth Vader, Emperor Palpatine, any others
"""),
"EP_3v3_BSFSET": CounterTeam(id: "EP_3v3_BSFSET", name: "EP L (w/ BSF and SET)", type: .offense, squad: [#"Emperor Palpatine"#, #"Bastila Shan (Fallen)"#, #"Sith Empire Trooper"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"EP_3v3_BSFTHRAWN": CounterTeam(id: "EP_3v3_BSFTHRAWN", name: "EP L (w/ BSF and Thrawn)", type: .offense, squad: [#"Emperor Palpatine"#, #"Bastila Shan (Fallen)"#, #"Grand Admiral Thrawn"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"EP_MALAK_3v3": CounterTeam(id: "EP_MALAK_3v3", name: "EP L (w/ Malak)", type: .defense, squad: [#"Emperor Palpatine"#, #"Bastila Shan (Fallen)"#, #"Darth Malak"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:
"""
Emperor Palpatine's leadership grants Empire and Sith allies potency and max health.  It reduces potency and evasion for Jedi and Rebel enemies.  This is a debuff-driven team, granting Empire allies turn meter for applying debuffs, Sith allies health for applying debuffs, and turn meter for both when enemy debuffs expire.
""", strategy:
"""
You'll need to stop the turn meter gain with Daze, Tenacity Up, and/or by outspeeding them.<br/><br/>

This is a tough battle that could easily take two or more teams to clear.<br/><br/.

Kill order: BSF, Emperor Palpatine, Darth Malak
"""),
"EP_SHORETROOPER": CounterTeam(id: "EP_SHORETROOPER", name: "EP L (w/ Shoretrooper)", type: .offense, squad: [#"Emperor Palpatine"#, #"Darth Vader"#, #"Shoretrooper"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"other Sith or Empire characters"#, description:"", strategy:""),
"EWOKS": CounterTeam(id: "EWOKS", name: "Ewoks", type: .both, squad: [#"Chief Chirpa"#, #"Wicket"#, #"Ewok Elder"#, #"Paploo"#, #"Logray"#], zetas: [true, true, true, true, false], subs: #"C-3PO, Ewok Scout, Teebo"#, description:
"""
Crit team with lot of assists, buffs, and usually daze and revive. High TM gain from Chief Chirpa's zeta lead.
""", strategy:""),
"EWOKS_3v3": CounterTeam(id: "EWOKS_3v3", name: "Ewoks", type: .defense, squad: [#"Chief Chirpa"#, #"Paploo"#, #"Wicket"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"EWOKS_C3PO": CounterTeam(id: "EWOKS_C3PO", name: "Ewoks (w/ C-3PO)", type: .both, squad: [#"Chief Chirpa"#, #"Wicket"#, #"Ewok Elder"#, #"Paploo"#, #"C-3PO"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"FO_HUX_3v3": CounterTeam(id: "FO_HUX_3v3", name: "First Order (Hux L)", type: .offense, squad: [#"General Hux"#, #"Sith Trooper"#, #"First Order Officer"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"FO_KRU_ST": CounterTeam(id: "FO_KRU_ST", name: "First Order (KRU L w/ Sith Trooper)", type: .offense, squad: [#"Kylo Ren (Unmasked)"#, #"Kylo Ren"#, #"First Order Executioner"#, #"First Order Officer"#, #"Sith Trooper"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"FO_KRU_3v3_SLKR": CounterTeam(id: "FO_KRU_3v3_SLKR", name: "First Order (KRU L w/ SLKR)", type: .defense, squad: [#"Kylo Ren (Unmasked)"#, #"Supreme Leader Kylo Ren"#, #"Sith Trooper"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"FO_KRU": CounterTeam(id: "FO_KRU", name: "First Order (KRU L)", type: .defense, squad: [#"Kylo Ren (Unmasked)"#, #"Kylo Ren"#, #"First Order Executioner"#, #"First Order Officer"#, #"First Order Stormtrooper"#], zetas: [true, true, true, false, false], subs: #"Enfys Nest, zBarriss Offee, Captain Phasma, First Order SF TIE Pilot"#, description:
"""
Synergy team with counter attacking, dispelling (or loving the debuffs), healing + protection recovery, and high crit damage.
""", strategy:
"""
Avoid getting countered frequently (daze/attack from stealth and assists), dont rely on debuffs, and deal damage that surpass their heals with single big-hitters or turn meter gains.  Kill order: First Order Executioner, First Order Officer, First Order TIE Pilot.
"""),
"FO_KRU_3v3": CounterTeam(id: "FO_KRU_3v3", name: "First Order (KRU L)", type: .defense, squad: [#"Kylo Ren (Unmasked)"#, #"Kylo Ren"#, #"First Order Executioner"#, #""#, #""#], zetas: [true, true, false, false, false], subs: #""#, description:"", strategy:""),
"FO_SLKR_THRAWN": CounterTeam(id: "FO_SLKR_THRAWN", name: "First Order (SLKR L w/ Thrawn)", type: .both, squad: [#"Supreme Leader Kylo Ren"#, #"Kylo Ren (Unmasked)"#, #"General Hux"#, #"First Order Officer"#, #"Grand Admiral Thrawn"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"FO_SLKR": CounterTeam(id: "FO_SLKR", name: "First Order (SLKR L)", type: .both, squad: [#"Supreme Leader Kylo Ren"#, #"Kylo Ren (Unmasked)"#, #"General Hux"#, #"Sith Trooper"#, #"First Order Officer"#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"GALACTIC_REPUBLIC": CounterTeam(id: "GALACTIC_REPUBLIC", name: "Galactic Republic", type: .defense, squad: [#"Padmé Amidala"#, #"Jedi Knight Anakin"#, #"Ahsoka Tano"#, #"General Kenobi"#, #"Barriss Offee"#], zetas: [true, true, true, false, false], subs: #"Grand Master Yoda, C-3PO, CT-7567 "Rex", Clone Wars Chewbacca, CT-5555 "Fives","#, description:
"""
Super tanky team, immunity to debuffs (with many options to dispel), protection-up spamming, damage based on % of target's health, and benefits when enemies attack out of turn.
""", strategy:
"""
Buff immunities (+Isolate + Fracture + zeta Imperial Probe Droid) to deny buffing and stacking of protection-up and bonus % damage.  Kill order: Padmé Amidala, Jedi Knight Anakin.
"""),
"GALACTIC_REPUBLIC_3v3": CounterTeam(id: "GALACTIC_REPUBLIC_3v3", name: "Galactic Republic", type: .both, squad: [#"Padmé Amidala"#, #"Jedi Knight Anakin"#, #"General Kenobi"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"GALACTIC_REPUBLIC_3v3_PADMEANAKINAHSOKA": CounterTeam(id: "GALACTIC_REPUBLIC_3v3_PADMEANAKINAHSOKA", name: "Galactic Republic (w/ Anakin and Ahsoka)", type: .offense, squad: [#"Padmé Amidala"#, #"Jedi Knight Anakin"#, #"Ahsoka Tano"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:
"""
Kill order: Anakin, Ahsoka, Padmé
"""),
"GALACTIC_REPUBLIC_C3PO": CounterTeam(id: "GALACTIC_REPUBLIC_C3PO", name: "Galactic Republic (w/ C-3PO)", type: .offense, squad: [#"Padmé Amidala"#, #"Jedi Knight Anakin"#, #"Ahsoka Tano"#, #"General Kenobi"#, #"C-3PO"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"GALACTIC_REPUBLIC_SLKRKILLER": CounterTeam(id: "GALACTIC_REPUBLIC_SLKRKILLER", name: "Galactic Republic SLKR-Killer", type: .offense, squad: [#"Padmé Amidala"#, #"General Skywalker"#, #"Wat Tambor"#, #"CT-5555 "Fives""#, #"CT-21-0408 "Echo""#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"GK": CounterTeam(id: "GK", name: "General Kenobi L", type: .both, squad: [#"General Kenobi"#, #"Barriss Offee"#, #"Wampa"#, #"Darth Nihilus"#, #"Mother Talzin"#], zetas: [true, true, false, false, false], subs: #""#, description:"", strategy:""),
"GK_ZARRISS_3v3": CounterTeam(id: "GK_ZARRISS_3v3", name: "General Kenobi L (w/ Barriss Offee)", type: .both, squad: [#"General Kenobi"#, #"Barriss Offee"#, #"CT-5555 "Fives""#, #""#, #""#], zetas: [true, true, false, false, false], subs: #"CT-5555 "Fives" can be subbed for another damage dealer"#, description:"", strategy:""),
"GK_WAMPA_3v3": CounterTeam(id: "GK_WAMPA_3v3", name: "General Kenobi L (w/ Wampa)", type: .offense, squad: [#"General Kenobi"#, #"Barriss Offee"#, #"Wampa"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"Enfys Nest for Wampa"#, description:"", strategy:""),
"GEONOSIANS": CounterTeam(id: "GEONOSIANS", name: "Geonosians", type: .defense, squad: [#"Geonosian Brood Alpha"#, #"Sun Fac"#, #"Geonosian Soldier"#, #"Geonosian Spy"#, #"Poggle the Lesser"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:
"""
Effective counter teams will have strong AoEs, ways of taking big chunks of health (e.g. Expose), and/or mechanics that use assists against the Geonosians.  Kill order: Geonosian Brood Alpha, Geonosian Spy, Geonosian Soldier/Poggle the Lesser, Sun Fac.
"""),
"GEONOSIANS_3v3": CounterTeam(id: "GEONOSIANS_3v3", name: "Geonosians", type: .defense, squad: [#"Geonosian Brood Alpha"#, #"Geonosian Spy"#, #"Sun Fac"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"GEONOSIANS_3v3_POGGLEGS": CounterTeam(id: "GEONOSIANS_3v3_POGGLEGS", name: "Geonosians (GBA L w/ Poggle and GS)", type: .defense, squad: [#"Geonosian Brood Alpha"#, #"Poggle the Lesser"#, #"Geonosian Soldier"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"IG-88_HI": CounterTeam(id: "IG-88_HI", name: "IG-88 (Healing Immunity)", type: .offense, squad: [#"IG-88"#, #"Wampa"#, #"Cassian Andor"#, #"Boba Fett"#, #"Kylo Ren"#], zetas: [true, true, true, true, true], subs: #""#, description:"", strategy:""),
"IT": CounterTeam(id: "IT", name: "Imperial Troopers", type: .offense, squad: [#"General Veers"#, #"Colonel Starck"#, #"Range Trooper"#, #"Snowtrooper"#, #"Death Trooper"#], zetas: [true, true, true, true, false], subs: #"Grand Admiral Thrawn, Stormtrooper, Shoretrooper"#, description:
"""
Fast, offensive team that's focused on getting a kill to gain massive turn meter.  Death Trooper's Deathmark makes sequential kills easier.  This team isn't effective against tankier squads or squads that require more than two dispels.
""", strategy:
"""
Don't let the turn meter train run or stop it with a tank, good healer, or daze.  Go after Snowtrooper first.  If Shoretrooper is present, bring a dispeller in.  Also counter toons can be effective due to lots of AoE attacks.  Kill order: General Veers/Colonel Starck, Snowtrooper/Death Trooper, Range Trooper.
"""),
"IT_3v3": CounterTeam(id: "IT_3v3", name: "Imperial Troopers", type: .both, squad: [#"General Veers"#, #"Colonel Starck"#, #"Range Trooper"#, #""#, #""#], zetas: [true, true, false, false, false], subs: #""#, description:"", strategy:""),
"JANGO_FETT": CounterTeam(id: "JANGO_FETT", name: "Jango Fett", type: .offense, squad: [#"Jango Fett"#, #""#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #"NOTE: This is not a full team"#, description:"", strategy:""),
"JAWAS": CounterTeam(id: "JAWAS", name: "Jawas", type: .offense, squad: [#"Chief Nebit"#, #"Jawa Engineer"#, #"Jawa Scavenger"#, #"Jawa"#, #"Dathcha"#], zetas: [true, true, true, false, false], subs: #"Droids"#, description:
"""
High damage if using IG-86 or IG-88,  turn meter gain, crit chance/damage gain, and healing from Jawa Engineer.
""", strategy:
"""
Daze and AoE, having crit avoidance.  Kill Jawa Engineer first, then IG-88, then the rest.
"""),
"JAWAS_3v3": CounterTeam(id: "JAWAS_3v3", name: "Jawas", type: .offense, squad: [#"Chief Nebit"#, #"Jawa Engineer"#, #"Jawa Scavenger"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"JEDI_BASTILA_3v3_EZRAHODA": CounterTeam(id: "JEDI_BASTILA_3v3_EZRAHODA", name: "Jedi (Bastila L w/ Ezra and Hoda)", type: .offense, squad: [#"Bastila Shan"#, #"Ezra Bridger"#, #"Hermit Yoda"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"JEDI_BASTILA_REVAN": CounterTeam(id: "JEDI_BASTILA_REVAN", name: "Jedi (Bastila L w/ Revan)", type: .offense, squad: [#"Bastila Shan"#, #"Jedi Knight Revan"#, #"Jolee Bindo"#, #"Grand Master Yoda"#, #"General Kenobi"#], zetas: [true, true, false, false, false], subs: #"Ezra Bridger, Any leftover Jedi"#, description:"", strategy:""),
"JEDI_BASTILA_REVAN_3v3": CounterTeam(id: "JEDI_BASTILA_REVAN_3v3", name: "Jedi (Bastila L w/ Revan)", type: .offense, squad: [#"Bastila Shan"#, #"Jedi Knight Revan"#, #"Grand Master Yoda"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"JEDI_BASTILA": CounterTeam(id: "JEDI_BASTILA", name: "Jedi (Bastila L)", type: .defense, squad: [#"Bastila Shan"#, #"Jolee Bindo"#, #"Grand Master Yoda"#, #"General Kenobi"#, #"Ezra Bridger"#], zetas: [true, false, false, false, false], subs: #"Aayla Secura, Jedi Knight Anakin, Obi-Wan Kenobi (Old Ben), Any leftover Jedi"#, description:
"""
Buffs, high protection pool, and high tenacity.
""", strategy:
"""
Stop the spreading buffs by Grand Master Yoda. Taunt to soak up the damage while you working on removing the Jedi protection.  Need to have enough damage to finish in 5 mins, this team will require strong teams to beat.  Defensive leads that buy you time with passive healings are best options (Bossk, KRU, Traya).  Kill high damage characters first, like Ezra Bridger.
"""),
"JEDI_BASTILA_3v3": CounterTeam(id: "JEDI_BASTILA_3v3", name: "Jedi (Bastila L)", type: .defense, squad: [#"Bastila Shan"#, #"Jolee Bindo"#, #"Grand Master Yoda"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"JEDI_QGJ": CounterTeam(id: "JEDI_QGJ", name: "Jedi (QGJ L)", type: .offense, squad: [#"Qui-Gon Jinn"#, #"Jedi Knight Anakin"#, #"Aayla Secura"#, #"Ezra Bridger"#, #"Hermit Yoda"#], zetas: [true, false, false, false, false], subs: #"Any leftover Jedi"#, description:"", strategy:""),
"JEDI_REVAN_3v3_AHSOKASHAAKTI": CounterTeam(id: "JEDI_REVAN_3v3_AHSOKASHAAKTI", name: "Jedi (Revan L w/ Ahsoka and Shaak Ti)", type: .offense, squad: [#"Jedi Knight Revan"#, #"Ahsoka Tano"#, #"Shaak Ti"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"JEDI_REVAN_HODA": CounterTeam(id: "JEDI_REVAN_HODA", name: "Jedi (Revan L w/ Hoda)", type: .both, squad: [#"Jedi Knight Revan"#, #"Jolee Bindo"#, #"Grand Master Yoda"#, #"Bastila Shan"#, #"Hermit Yoda"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"JEDI_REVAN_3v3_GMYJKA": CounterTeam(id: "JEDI_REVAN_3v3_GMYJKA", name: "Jedi (Revan L w/ JKA)", type: .offense, squad: [#"Jedi Knight Revan"#, #"Grand Master Yoda"#, #"Jedi Knight Anakin"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"Enfys Nest for Wampa"#, description:"", strategy:""),
"JEDI_REVAN_REYKILLER2": CounterTeam(id: "JEDI_REVAN_REYKILLER2", name: "Jedi (Revan L w/ Thrawn) Rey-Killer", type: .offense, squad: [#"Jedi Knight Revan"#, #"Bastila Shan"#, #"General Skywalker"#, #"Hermit Yoda"#, #"Grand Admiral Thrawn"#], zetas: [true, false, true, true, true], subs: #"For Bastila Shan: Shaak Ti"#, description:"", strategy:""),
"JEDI_REVAN_REYKILLER": CounterTeam(id: "JEDI_REVAN_REYKILLER", name: "Jedi (Revan L w/ Wat) Rey-Killer", type: .offense, squad: [#"Jedi Knight Revan"#, #"Shaak Ti"#, #"General Skywalker"#, #"Hermit Yoda"#, #"Wat Tambor"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"JEDI_REVAN": CounterTeam(id: "JEDI_REVAN", name: "Jedi (Revan L)", type: .both, squad: [#"Jedi Knight Revan"#, #"Jolee Bindo"#, #"Grand Master Yoda"#, #"Bastila Shan"#, #"General Kenobi"#], zetas: [true, true, true, false, false], subs: #"Ezra Bridger, Hermit Yoda, Obi-Wan Kenobi (Old Ben), Juhani, Plo Koon"#, description:
"""
Buffs, heals, protection regeneration, revive, and lot of damage.
""", strategy:
"""
Stop the revive and buffs [isolate/ healing immunity/ fracture], outburst their heals [Annihilate/ Plague/ crazy damage], have someone to soak up the damage [Nightsister Zombie, Nest, Sith Empire Trooper, Kylo Ren Unmasked].  Kill a low health enemy first, like Grand Master Yoda, to get rid of Revan's Savior ability, kill Jolee Bindo to stop the revives, kill Jedi Knight Revan, then kill the other two.
"""),
"JEDI_REVAN_3v3": CounterTeam(id: "JEDI_REVAN_3v3", name: "Jedi (Revan L)", type: .offense, squad: [#"Jedi Knight Revan"#, #"Jolee Bindo"#, #"Grand Master Yoda"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"MAGMA": CounterTeam(id: "MAGMA", name: "Magma", type: .offense, squad: [#"Grand Admiral Thrawn"#, #"Magmatrooper"#, #"Death Trooper"#, #"Director Krennic"#, #"Grand Moff Tarkin"#], zetas: [true, true, true, false, false], subs: #"Imperial Probe Droid, Stormtrooper, Shoretrooper"#, description:"", strategy:""),
"MAGMA_3v3": CounterTeam(id: "MAGMA_3v3", name: "Magma", type: .offense, squad: [#"Grand Admiral Thrawn"#, #"Magmatrooper"#, #"Death Trooper"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"NS_ASAJJ_3v3_OLDDAKAZOMBIE": CounterTeam(id: "NS_ASAJJ_3v3_OLDDAKAZOMBIE", name: "Nightsisters (Asajj L w/ Old Daka and Zombie)", type: .both, squad: [#"Asajj Ventress"#, #"Old Daka"#, #"Nightsister Zombie"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"NS_ASAJJ": CounterTeam(id: "NS_ASAJJ", name: "Nightsisters (Asajj L)", type: .both, squad: [#"Asajj Ventress"#, #"Mother Talzin"#, #"Old Daka"#, #"Nightsister Zombie"#, #"Nightsister Spirit"#], zetas: [true, true, true, true, false], subs: #"Enfys Nest, Barriss Offee, Nightsister Acolyte, Hermit Yoda"#, description:
"""
Infinite revives potential with Nightsister Zombie, damage through protection, healing, and cleansing.
""", strategy:
"""
Avoid Nightsister Zombie's constant taunting [dispel / buff immunity] or work around it [strong AoEs].  Having healers or high health steal is effective.  You want to kill Old Daka 3 times (to stop the auto revives and her revives), kill Mother Talzin, then kill Asajj Ventress (because she's more powerful now due to the deaths).  If no Enfys Nest, then kill whichever 4th character is used, then finally Nightsister Zombie.  If 4th is Nightsister Acolyte, you definitely want a dispeller or you can easily get caught between her stealth and Nightsister Zombie's taunt.  If Enfys Nest, save her for last.
"""),
"NS_ASAJJ_3v3": CounterTeam(id: "NS_ASAJJ_3v3", name: "Nightsisters (Asajj L)", type: .defense, squad: [#"Asajj Ventress"#, #"Old Daka"#, #"Nightsister Spirit"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"NS_DAKA_TALIA": CounterTeam(id: "NS_DAKA_TALIA", name: "Nightsisters (Daka L w/ Talia)", type: .offense, squad: [#"Old Daka"#, #"Asajj Ventress"#, #"Mother Talzin"#, #"Nightsister Zombie"#, #"Talia"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"NS_DOOKU_3v3_ASAJJZOMBIE": CounterTeam(id: "NS_DOOKU_3v3_ASAJJZOMBIE", name: "Nightsisters (Dooku L)", type: .offense, squad: [#"Count Dooku"#, #"Asajj Ventress"#, #"Nightsister Zombie"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"NS_MT_NA": CounterTeam(id: "NS_MT_NA", name: "Nightsisters (Talzin L w/ Acolyte)", type: .both, squad: [#"Mother Talzin"#, #"Asajj Ventress"#, #"Old Daka"#, #"Nightsister Zombie"#, #"Nightsister Acolyte"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:
"""
Infinite revives potential with Nightsister Zombie, damage through protection, healing, and cleansing.
""", strategy:
"""
Avoid Nightsister Zombie's constant taunting [dispel / buff immunity] or work around it [strong AoEs].  Having healers or high health steal is effective.  You want to kill Old Daka 3 times (to stop the auto revives and her revives), kill Mother Talzin, kill Asajj Ventress (because she's more powerful now due to the deaths), dispel and kill Nightsister Acolyte to stop the stealth, then kill Nightsister Zombie.  If opponent has Enfys Nest, save her for last.
"""),
"NS_MT_3v3_ASAJJZOMBIE": CounterTeam(id: "NS_MT_3v3_ASAJJZOMBIE", name: "Nightsisters (Talzin L w/ Asajj and Zombie)", type: .offense, squad: [#"Mother Talzin"#, #"Asajj Ventress"#, #"Nightsister Zombie"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"NS_MT": CounterTeam(id: "NS_MT", name: "Nightsisters (Talzin L)", type: .both, squad: [#"Mother Talzin"#, #"Asajj Ventress"#, #"Old Daka"#, #"Nightsister Zombie"#, #"Nightsister Spirit"#], zetas: [true, true, true, true, false], subs: #"Enfys Nest, Barriss Offee, Nightsister Acolyte, Hermit Yoda"#, description:
"""
Infinite revives potential with Nightsister Zombie, damage through protection, healing, and cleansing.
""", strategy:
"""
Avoid Nightsister Zombie's constant taunting [dispel / buff immunity] or work around it [strong AoEs].  Having healers or high health steal is effective.  You want to kill Old Daka 3 times (to stop the auto revives and her revives), kill Mother Talzin, then kill Asajj Ventress (because she's more powerful now due to the deaths).  If no Enfys Nest, then kill whichever 4th character is used, then finally Nightsister Zombie.  If 4th is Nightsister Acolyte, you definitely want a dispeller or you can easily get caught between her stealth and Nightsister Zombie's taunt.  If Enfys Nest, save her for last.
"""),
"NS_MT_3v3": CounterTeam(id: "NS_MT_3v3", name: "Nightsisters (Talzin L)", type: .defense, squad: [#"Mother Talzin"#, #"Nightsister Acolyte"#, #"Nightsister Zombie"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"NS_NEST": CounterTeam(id: "NS_NEST", name: "Nightsisters (w/ Nest)", type: .both, squad: [#"Asajj Ventress"#, #"Mother Talzin"#, #"Old Daka"#, #"Nightsister Zombie"#, #"Enfys Nest"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:
"""
Infinite revives potential with Nightsister Zombie, damage through protection, healing, and cleansing.
""", strategy:
"""
Avoid Nightsister Zombie's constant taunting [dispel / buff immunity] or work around it [strong AoEs].  Having healers or high health steal is effective.  You want to kill Old Daka 3 times (to stop the auto revives and her revives), kill Mother Talzin, kill Asajj Ventress (because she's more powerful now due to the deaths), kill 4th, then kill Enfys Nest last.
"""),
"OLDBEN_WAMPA_3v3": CounterTeam(id: "OLDBEN_WAMPA_3v3", name: "Old Ben/Wampa", type: .offense, squad: [#"Obi-Wan Kenobi (Old Ben)"#, #"Wampa"#, #"Visas Marr"#, #""#, #""#], zetas: [true, true, false, false, false], subs: #"Enfys Nest for Wampa"#, description:"", strategy:""),
"OR_CARTH_3v3_MISSIONVAOZAALBAR": CounterTeam(id: "OR_CARTH_3v3_MISSIONVAOZAALBAR", name: "Old Republic (Carth L w/ Mission Vao and Zaalbar)", type: .both, squad: [#"Carth Onasi"#, #"Mission Vao"#, #"Zaalbar"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:
"""
Kill order: Carth or Mission, then Zaalbar
"""),
"OR_CARTH": CounterTeam(id: "OR_CARTH", name: "Old Republic (Carth L)", type: .both, squad: [#"Carth Onasi"#, #"Canderous Ordo"#, #"Mission Vao"#, #"Zaalbar"#, #"Juhani"#], zetas: [true, true, true, true, false], subs: #"Juhani, Wampa, Boba Fett, Darth Sidious, Chirrut Îmwe + Baze Malbus"#, description:
"""
Lot of damage over time and nice bonus synergy shared between each other [offense, tenacity, assists and defense/crit avoidance, protection recovery].
""", strategy:
"""
Stop the damage over time from spreading, dispel Zaalbar frequently from his taunt to focus on attackers/support first.  Can't rely on debuffs/have really high potency (Bastila Shan (Fallen) and Carth will have 80% tenacity, rest 30%).  Bypass the crit avoidance (Zaalbar+Mission have 55% Crit Avoidance, rest 30%).  Use their slow speed against them with turn meter gain, you should have speed advantage.  Kill order: Canderous Ordo, Carth Onasi or Mission Vao.
"""),
"OR_CARTH_3v3": CounterTeam(id: "OR_CARTH_3v3", name: "Old Republic (Carth L)", type: .defense, squad: [#"Carth Onasi"#, #"Canderous Ordo"#, #"Juhani"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"PHOENIX": CounterTeam(id: "PHOENIX", name: "Phoenix", type: .defense, squad: [#"Hera Syndulla"#, #"Kanan Jarrus"#, #"Garazeb "Zeb" Orrelios"#, #"Sabine Wren"#, #"Ezra Bridger"#], zetas: [true, true, true, false, false], subs: #"Chopper"#, description:
"""
High synergy team with a lot of counter attacking, dispelling (or loving the debuffs), and HP + protection recovery. Be careful with G12 zeta Phoenix, use equally geared zeta'd teams.
""", strategy:
"""
Avoid getting countered [daze or attacking from stealth/assists]. Deal good damage in a single turn that surpasses their heals (calling assist/simply high damage/high turn meter gain).  There's no point in debuffing because of Kanan's unique (besides Daze/Stun).  Focus on one character at a time to remove the unique ability they are sharing with the team.  Killing Chopper removes health/protection regeneration.  Killing Ezra removes stacking offense up.  Killing Sabine removes increased crit chance and crit damage.  Killing Kanan removes counter chance and health regen on debuff.  Killing Zeb removes protection regen.  Killing Hera removes turn meter gain on using special ability.
"""),
"PHOENIX_3v3": CounterTeam(id: "PHOENIX_3v3", name: "Phoenix", type: .defense, squad: [#"Hera Syndulla"#, #"Kanan Jarrus"#, #"Ezra Bridger"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"Chopper"#, description:"", strategy:""),
"QIRA": CounterTeam(id: "QIRA", name: "Qi'ra L", type: .defense, squad: [#"Qi'ra"#, #"Vandor Chewbacca"#, #"L3-37"#, #"Mission Vao"#, #"Zaalbar"#], zetas: [true, true, true, false, false], subs: #"Enfys Nest, Young Han Solo, Han Solo + Chewbacca, Young Lando Calrissian, Cad Bane, Barriss Offee, Wat Tambor"#, description:
"""
Heals, revives, protection regeneration, and lot of taunts.  This is a very hard team to beat.
""", strategy:
"""
Requires dispelling, healing immunity, lot of turn meter gain, and AoE attackers.  Kill Vandor Chewbacca first and don't let him revive his allies.  Keep Zaalbar and L3-37 shutdown by dispelling them.  Kill order: Vandor Chewbacca, Qi'ra/Mission Vao/Young Han Solo/Young Lando Calrissian, L3-37, Enfys Nest, Zaalbar.
"""),
"QIRA_PREPAREDSMUGGLERS": CounterTeam(id: "QIRA_PREPAREDSMUGGLERS", name: "Qi'ra L (Prepared Smugglers)", type: .offense, squad: [#"Qi'ra"#, #"Vandor Chewbacca"#, #"Young Han Solo"#, #"Young Lando Calrissian"#, #"L3-37"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"QIRA_3v3_LANDOL3": CounterTeam(id: "QIRA_3v3_LANDOL3", name: "Qi'ra L (w/ Lando and L3-37)", type: .offense, squad: [#"Qi'ra"#, #"L3-37"#, #"Lando Calrissian"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"QIRA_NEST_HODA": CounterTeam(id: "QIRA_NEST_HODA", name: "Qi'ra L (w/ Nest and Hoda)", type: .offense, squad: [#"Qi'ra"#, #"Enfys Nest"#, #"Hermit Yoda"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"QIRA_NEST_YOUNGHAN": CounterTeam(id: "QIRA_NEST_YOUNGHAN", name: "Qi'ra L (w/ Nest and Young Han)", type: .both, squad: [#"Qi'ra"#, #"Vandor Chewbacca"#, #"Enfys Nest"#, #"L3-37"#, #"Young Han Solo"#], zetas: [true, true, true, false, true], subs: #"Another Tank in place of L3-37 (Scoundrel Tank preferred)"#, description:"", strategy:""),
"QIRA_NEST_ZAALBAR": CounterTeam(id: "QIRA_NEST_ZAALBAR", name: "Qi'ra L (w/ Nest and Zaalbar)", type: .both, squad: [#"Qi'ra"#, #"Vandor Chewbacca"#, #"Enfys Nest"#, #"L3-37"#, #"Zaalbar"#], zetas: [true, true, true, false, true], subs: #"For L3-37: Mission Vao"#, description:"", strategy:""),
"QIRA_NEST_HODA_FULL": CounterTeam(id: "QIRA_NEST_HODA_FULL", name: "Qi'ra L (w/ Nest, Hoda, and others)", type: .offense, squad: [#"Qi'ra"#, #"Vandor Chewbacca"#, #"Enfys Nest"#, #"L3-37"#, #"Hermit Yoda"#], zetas: [true, true, true, true, false], subs: #"Zaalbar instead of Hermit Yoda"#, description:"", strategy:""),
"QIRA_NEST_3v3": CounterTeam(id: "QIRA_NEST_3v3", name: "Qi'ra L (w/ Nest)", type: .offense, squad: [#"Qi'ra"#, #"Enfys Nest"#, #"Hermit Yoda"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #"First Order Officer or Visas Mar for Hermit Yoda"#, description:"", strategy:""),
"QIRA_3v3": CounterTeam(id: "QIRA_3v3", name: "Qi'ra L (w/o Nest)", type: .offense, squad: [#"Qi'ra"#, #"L3-37"#, #"Vandor Chewbacca"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"REBELS_ACK_3v3": CounterTeam(id: "REBELS_ACK_3v3", name: "Rebels (Ackbar L)", type: .offense, squad: [#"Admiral Ackbar"#, #"C-3PO"#, #"Princess Leia"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"REBELS_CLS_3v3_CHAZE": CounterTeam(id: "REBELS_CLS_3v3_CHAZE", name: "Rebels (CLS + Chaze)", type: .offense, squad: [#"Commander Luke Skywalker"#, #"Chirrut Îmwe"#, #"Baze Malbus"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"REBELS_CLS": CounterTeam(id: "REBELS_CLS", name: "Rebels (CLS L)", type: .offense, squad: [#"Commander Luke Skywalker"#, #"Han Solo"#, #"Chewbacca"#, #"C-3PO"#, #"R2-D2"#], zetas: [true, true, true, false, false], subs: #"Captain Han Solo, Ahsoka Tano (Fulcrum), Obi-Wan Kenobi (Old Ben), General Kenobi, Grand Admiral Thrawn"#, description:
"""
Double (Han+CLS): offensive team, high crits, high damage, high TM gain, and lot of counter attacks. Trio - addition of Chewbacca brings crit immunity and can't be stunned/daze for Han and another toon. Quartet - addition of C3PO brings this team to whole new level and lot of attackers will do 40% HP damage (expose+chewbacca' unique).
""", strategy:
"""
Double & Trio - go for anti-crit team/anti-rebel team, don't get countered - no AoEs, attack from stealth/assist/have taunting tank while attacking, that can survive some hits.  Kill order: Chewbacca, Han Solo, Commander Luke Skywalker.
"""),
"REBELS_CLS_THRAWN": CounterTeam(id: "REBELS_CLS_THRAWN", name: "Rebels (CLS Quad w/ Thrawn)", type: .offense, squad: [#"Commander Luke Skywalker"#, #"Han Solo"#, #"Chewbacca"#, #"C-3PO"#, #"Grand Admiral Thrawn"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"REBELS_CLS_CHAZE": CounterTeam(id: "REBELS_CLS_CHAZE", name: "Rebels (CLS Trio + Chaze)", type: .offense, squad: [#"Commander Luke Skywalker"#, #"Han Solo"#, #"Chewbacca"#, #"Chirrut Îmwe"#, #"Baze Malbus"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"REBELS_CLS_3v3": CounterTeam(id: "REBELS_CLS_3v3", name: "Rebels (CLS Trio)", type: .offense, squad: [#"Commander Luke Skywalker"#, #"Han Solo"#, #"Chewbacca"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"REBELS_DANGER_ZONE": CounterTeam(id: "REBELS_DANGER_ZONE", name: "Rebels (Danger Zone)", type: .offense, squad: [#"Wedge Antilles"#, #"Biggs Darklighter"#, #"Commander Luke Skywalker"#, #"Han Solo"#, #"Chewbacca"#], zetas: [true, true, true, false, false], subs: #"Ahsoka Tano (Fulcrum), Ezra Bridger, C-3PO, Princess Leia"#, description:"", strategy:""),
"REBELS_FBLUKE_3v3": CounterTeam(id: "REBELS_FBLUKE_3v3", name: "Rebels (Farmboy L)", type: .offense, squad: [#"Luke Skywalker (Farmboy)"#, #"Lando Calrissian"#, #"Princess Leia"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"REBELS_WIGGS_3v3_C3PO": CounterTeam(id: "REBELS_WIGGS_3v3_C3PO", name: "Rebels (Wiggs w/ C-3PO)", type: .offense, squad: [#"Wedge Antilles"#, #"Biggs Darklighter"#, #"C-3PO"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"REBELS_WIGGS": CounterTeam(id: "REBELS_WIGGS", name: "Rebels (Wiggs)", type: .offense, squad: [#"Wedge Antilles"#, #"Biggs Darklighter"#, #"Ahsoka Tano (Fulcrum)"#, #"Chirrut Îmwe"#, #"Baze Malbus"#], zetas: [true, true, false, false, false], subs: #"Any leftover Rebels (a Tank is highly recommended)"#, description:"", strategy:""),
"REBELS_WIGGS_3v3": CounterTeam(id: "REBELS_WIGGS_3v3", name: "Rebels (Wiggs)", type: .offense, squad: [#"Wedge Antilles"#, #"Biggs Darklighter"#, #"Stormtrooper Han"#, #""#, #""#], zetas: [true, true, false, false, false], subs: #""#, description:"", strategy:""),
"RESISTANCE_FINN_3v3_POERT": CounterTeam(id: "RESISTANCE_FINN_3v3_POERT", name: "Resistance (Finn L w/ Poe and RT)", type: .offense, squad: [#"Finn"#, #"Poe Dameron"#, #"Resistance Trooper"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"RESISTANCE_FINN": CounterTeam(id: "RESISTANCE_FINN", name: "Resistance (Finn L)", type: .offense, squad: [#"Finn"#, #"Poe Dameron"#, #"Veteran Smuggler Han Solo"#, #"Veteran Smuggler Chewbacca"#, #"Amilyn Holdo"#], zetas: [true, true, true, true, false], subs: #""#, description:"", strategy:""),
"RESISTANCE_FINN_3v3": CounterTeam(id: "RESISTANCE_FINN_3v3", name: "Resistance (Finn L)", type: .offense, squad: [#"Finn"#, #"R2-D2"#, #"Rey (Scavenger)"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"RESISTANCE_HEROES": CounterTeam(id: "RESISTANCE_HEROES", name: "Resistance (Heroes)", type: .offense, squad: [#"Rey (Jedi Training)"#, #"C-3PO"#, #"R2-D2"#, #"Resistance Hero Finn"#, #"Resistance Hero Poe"#], zetas: [true, true, true, true, true], subs: #""#, description:"", strategy:""),
"RESISTANCE_HEROES_3v3": CounterTeam(id: "RESISTANCE_HEROES_3v3", name: "Resistance (Heroes)", type: .both, squad: [#"Rey (Jedi Training)"#, #"Resistance Hero Finn"#, #"Resistance Hero Poe"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"JTR_DROIDS": CounterTeam(id: "JTR_DROIDS", name: "Resistance (JTR L w/ Droids)", type: .both, squad: [#"Rey (Jedi Training)"#, #"BB-8"#, #"R2-D2"#, #"C-3PO"#, #"Resistance Trooper"#], zetas: [true, true, true, true, false], subs: #"for Resistance Trooper - Chopper, IG-88, L3-37, T3-M4"#, description:"", strategy:""),
"JTR_DROIDS_3v3": CounterTeam(id: "JTR_DROIDS_3v3", name: "Resistance (JTR L w/ Droids)", type: .offense, squad: [#"Rey (Jedi Training)"#, #"BB-8"#, #"R2-D2"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"JTR_HOLDO": CounterTeam(id: "JTR_HOLDO", name: "Resistance (JTR L w/ Holdo)", type: .offense, squad: [#"Rey (Jedi Training)"#, #"BB-8"#, #"R2-D2"#, #"C-3PO"#, #"Amilyn Holdo"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"JTR": CounterTeam(id: "JTR", name: "Resistance (JTR L)", type: .both, squad: [#"Rey (Jedi Training)"#, #"BB-8"#, #"Rey (Scavenger)"#, #"Finn"#, #"Resistance Trooper"#], zetas: [true, true, false, false, false], subs: #"Chopper, R2-D2, C-3PO, L3-37, General Kenobi, Enfys Nest, Any leftover Resistance"#, description:
"""
Turn meter gain, high damage and assists from BB-8, annoying foresights and recoveries from Rey (Jedi Training).  Crit team built on exposes for the turn meter.
""", strategy:
"""
Stopping BB-8 and Rey (Jedi Training) is key. The damage is from crits and exposes, so crit avoidance works well and reduced damage from exposes as well.  Hard to daze them because of foresights, so need to stop the turn meter train in other ways (disabling BB-8, crit avoidance) or exploit the high amount of turns (Plague).  Kill order: BB-8/Resistance Trooper, Rey (Scavenger), Rey (Jedi Training).
"""),
"JTR_3v3": CounterTeam(id: "JTR_3v3", name: "Resistance (JTR L)", type: .offense, squad: [#"Rey (Jedi Training)"#, #"BB-8"#, #"Resistance Trooper"#, #""#, #""#], zetas: [true, true, false, false, false], subs: #""#, description:"", strategy:""),
"REY_MALAK": CounterTeam(id: "REY_MALAK", name: "Resistance (Rey L w/ Malak)", type: .defense, squad: [#"Rey"#, #"Resistance Hero Finn"#, #"Resistance Hero Poe"#, #"Darth Malak"#, #""#], zetas: [true, true, true, true, false], subs: #"A tank in the 5th slot"#, description:"", strategy:""),
"REY": CounterTeam(id: "REY", name: "Resistance (Rey L)", type: .both, squad: [#"Rey"#, #"Resistance Hero Finn"#, #"Resistance Hero Poe"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #"A tank (e.g. General Kenobi, L3-37), one more (e.g. Wat Tambor, Rey (Jedi Training))"#, description:"", strategy:""),
"REX_501": CounterTeam(id: "REX_501", name: "Rex 501st", type: .offense, squad: [#"CT-7567 "Rex""#, #"Ahsoka Tano"#, #"Clone Sergeant - Phase I"#, #"CT-21-0408 "Echo""#, #"CT-5555 "Fives""#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"REX": CounterTeam(id: "REX", name: "Rex L", type: .offense, squad: [#"CT-7567 "Rex""#, #""#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #"Sith Triumvirate (Darth Traya, Darth Sion, Darth Nihilus), Grand Admiral Thrawn"#, description:"", strategy:""),
"REX_3v3": CounterTeam(id: "REX_3v3", name: "Rex L", type: .both, squad: [#"CT-7567 "Rex""#, #"CT-5555 "Fives""#, #"CT-21-0408 "Echo""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"REX_WAMPA": CounterTeam(id: "REX_WAMPA", name: "Rex/Wampa", type: .offense, squad: [#"CT-7567 "Rex""#, #"Wampa"#, #""#, #""#, #""#], zetas: [true, true, false, false, false], subs: #"Baze Malbus + Chirrut Îmwe, R2-D2, Enfys Nest, Darth Nihilus, Mother Talzin, Han Solo + Chewbacca, General Kenobi, Barriss Offee, Visas Marr, Obi-Wan Kenobi (Old Ben), Sun Fac, First Order Officer"#, description:"", strategy:""),
"ROGUE_ONE": CounterTeam(id: "ROGUE_ONE", name: "Rogue One", type: .both, squad: [#"Jyn Erso"#, #"Cassian Andor"#, #"K-2SO"#, #"Chirrut Îmwe"#, #"Baze Malbus"#], zetas: [true, true, true, false, false], subs: #"Bistan, C-3PO, Scarif Rebel Pathfinder, Han Solo + Chewbacca"#, description:
"""
Team with heal, protection recovery (if zJyn Erso), lot of debuffs, and Chaze (Chirrut Îmwe and Baze Malbus) make this a formidable team.
""", strategy:
"""
Killing Chirrut Îmwe will stop the heal over time [or stop with Buff Immunity using Jedi Knight Anakin].  Need to dispel Baze's taunt while removing Chirrut's protection and focusing on Chirrut hard. [Veers Mass attack] Also try to avoid the debuffs from Cassian, K-2SO and Baze. [tenacity up/ high tenacity] another option is simply anti-rebel power [Wampa, DK, DT] or let them stack their heal over time buffs then use Boba's Execute.
"""),
"ROGUE_ONE_3v3": CounterTeam(id: "ROGUE_ONE_3v3", name: "Rogue One", type: .defense, squad: [#"Jyn Erso"#, #"Cassian Andor"#, #"K-2SO"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"ROGUE_ONE_3v3_CHAZE": CounterTeam(id: "ROGUE_ONE_3v3_CHAZE", name: "Rogue One (w/ Chaze)", type: .defense, squad: [#"Jyn Erso"#, #"Chirrut Îmwe"#, #"Baze Malbus"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"SEP_DROIDS": CounterTeam(id: "SEP_DROIDS", name: "Separatist Droids", type: .defense, squad: [#"General Grievous"#, #"B1 Battle Droid"#, #"Droideka"#, #"B2 Super Battle Droid"#, #"IG-100 MagnaGuard"#], zetas: [true, true, true, true, false], subs: #"Nute Gunray, Count Dooku, IG-88, BB-8, Imperial Probe Droid, T3-M4, L3-37, Wat Tambor"#, description:
"""
Frequent dispels and target lock.  Kill General Grievous as early as possible, he has high damage, puts mark on other droid allies to make himself hard to focus on, and has stacking max health.  High turn meter gains with zetas from B2 and B1.  Can also have high armor penetration if built with IG88 and T3M4.
""", strategy:
"""
Kill B2 Super Battle Droid or General Grievous first.
"""),
"SEP_DROIDS_3v3": CounterTeam(id: "SEP_DROIDS_3v3", name: "Separatist Droids", type: .offense, squad: [#"General Grievous"#, #"B1 Battle Droid"#, #"B2 Super Battle Droid"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"SEP_DROIDS_NONGRIEVIOUS_3v3": CounterTeam(id: "SEP_DROIDS_NONGRIEVIOUS_3v3", name: "Separatist Droids (Non-Grievious L)", type: .offense, squad: [#"BB-8"#, #"General Grievous"#, #"T3-M4"#, #""#, #""#], zetas: [true, true, false, false, false], subs: #""#, description:"", strategy:""),
"SEP_DROIDS_3v3_HK47DROIDEKA": CounterTeam(id: "SEP_DROIDS_3v3_HK47DROIDEKA", name: "Separatist Droids (w/ HK-47 and Droideka)", type: .offense, squad: [#"General Grievous"#, #"HK-47"#, #"Droideka"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"SEP_DROIDS_WAT": CounterTeam(id: "SEP_DROIDS_WAT", name: "Separatist Droids (w/ Wat)", type: .both, squad: [#"General Grievous"#, #"B1 Battle Droid"#, #"Wat Tambor"#, #"B2 Super Battle Droid"#, #"IG-100 MagnaGuard"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"SEP_NUTE_3v3_GRIEVOUSDROIDEKA": CounterTeam(id: "SEP_NUTE_3v3_GRIEVOUSDROIDEKA", name: "Separatists (Nute L w/ Grievous)", type: .offense, squad: [#"Nute Gunray"#, #"General Grievous"#, #"Droideka"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"SEP_NUTE_NEST": CounterTeam(id: "SEP_NUTE_NEST", name: "Separatists (Nute L w/ Nest)", type: .both, squad: [#"Nute Gunray"#, #"Wat Tambor"#, #"Count Dooku"#, #"Enfys Nest"#, #"Jango Fett"#], zetas: [true, true, true, true, false], subs: #"For Jango Fett: Stormtrooper Han or Droideka"#, description:"", strategy:""),
"SEP_NUTE_3v3_JANGODOOKU": CounterTeam(id: "SEP_NUTE_3v3_JANGODOOKU", name: "Separatists (Nute L)", type: .offense, squad: [#"Nute Gunray"#, #"Jango Fett"#, #"Count Dooku"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"SEP_NUTE": CounterTeam(id: "SEP_NUTE", name: "Separatists (Nute L)", type: .both, squad: [#"Nute Gunray"#, #"Wat Tambor"#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #"Any leftover Separatists"#, description:"", strategy:""),
"SEP_NUTE_3v3": CounterTeam(id: "SEP_NUTE_3v3", name: "Separatists (Nute L)", type: .defense, squad: [#"Nute Gunray"#, #"Droideka"#, #"IG-100 MagnaGuard"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #""#, description:"", strategy:""),
"SITH_MAUL": CounterTeam(id: "SITH_MAUL", name: "Sith (Maul L)", type: .offense, squad: [#"Darth Maul"#, #"Savage Opress"#, #"Sith Empire Trooper"#, #"Sith Assassin"#, #"Count Dooku"#], zetas: [true, true, false, false, false], subs: #"Any leftover Sith"#, description:
"""
Stealthy team with mass dispel of debuffs, fast turn meter gain, first turn advantage, and lots of AoE.  Dazing Maul is effective.
""", strategy:
"""
Cleanse and deal big damage with single-hitters.
"""),
"SITH_MAUL_3v3": CounterTeam(id: "SITH_MAUL_3v3", name: "Sith (Maul L)", type: .defense, squad: [#"Darth Maul"#, #""#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #"Any leftover Sith"#, description:"", strategy:""),
"SITH_NIHILUS_3v3": CounterTeam(id: "SITH_NIHILUS_3v3", name: "Sith (Nihilus L)", type: .defense, squad: [#"Darth Nihilus"#, #""#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #"Any leftover Sith"#, description:"", strategy:""),
"SITH_DUO_3v3": CounterTeam(id: "SITH_DUO_3v3", name: "Sith Duumvirate (w/ Thrawn)", type: .offense, squad: [#"Darth Traya"#, #"Darth Nihilus"#, #""#, #""#, #""#], zetas: [true, true, false, false, false], subs: #""#, description:"", strategy:""),
"SITH_EMPIRE_3v3_JKRKILLER": CounterTeam(id: "SITH_EMPIRE_3v3_JKRKILLER", name: "Sith Empire (JKR Killer)", type: .offense, squad: [#"Darth Traya"#, #"Darth Nihilus"#, #"Grand Admiral Thrawn"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"SITH_EMPIRE_W_MALAKTHRAWN": CounterTeam(id: "SITH_EMPIRE_W_MALAKTHRAWN", name: "Sith Empire (w/ Malak & Thrawn)", type: .offense, squad: [#"Darth Revan"#, #"Darth Malak"#, #"Bastila Shan (Fallen)"#, #"HK-47"#, #"Grand Admiral Thrawn"#], zetas: [true, true, true, true, true], subs: #""#, description:"", strategy:""),
"SITH_EMPIRE_W_MALAK": CounterTeam(id: "SITH_EMPIRE_W_MALAK", name: "Sith Empire (w/ Malak)", type: .defense, squad: [#"Darth Revan"#, #"Darth Malak"#, #"Bastila Shan (Fallen)"#, #"HK-47"#, #"Sith Empire Trooper"#], zetas: [true, true, true, true, false], subs: #"Sith Assassin, Sith Marauder, Grand Admiral Thrawn, Grand Master Yoda, Kylo Ren (Unmasked), Nightsister Zombie + Mother Talzin, Shoretrooper"#, description:
"""
Immunity to turn meter reduction + stops turn meter gain = speed meta.  Super fast, built on lot of debuffs, high offensive power, anti-Jedi, and focuses on the leader often.
""", strategy:
"""
Outspeed them, use the mass debuffs for your own good, or just very high offensive capabilities.  On toons like Jedi Knight Anakin, it's crucial to use right timing.  Kill Bastila Shan (Fallen) first.
"""),
"SITH_EMPIRE_3v3_W_MALAK": CounterTeam(id: "SITH_EMPIRE_3v3_W_MALAK", name: "Sith Empire (w/ Malak)", type: .defense, squad: [#"Darth Revan"#, #"Bastila Shan (Fallen)"#, #"Darth Malak"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"SITH_EMPIRE_WO_MALAK": CounterTeam(id: "SITH_EMPIRE_WO_MALAK", name: "Sith Empire (w/o Malak)", type: .defense, squad: [#"Darth Revan"#, #"Bastila Shan (Fallen)"#, #"HK-47"#, #"Sith Empire Trooper"#, #"Sith Assassin"#], zetas: [true, true, true, false, false], subs: #"Sith Assassin, Sith Marauder, Grand Admiral Thrawn, Grand Master Yoda, Kylo Ren (Unmasked), Nightsister Zombie + Mother Talzin, Shoretrooper"#, description:
"""
Immunity to turn meter reduction + stops turn meter gain = speed meta.  Super fast, built on lot of debuffs, high offensive power, anti-Jedi, and focuses on the leader often.
""", strategy:
"""
Outspeed them, use the mass debuffs for your own good, or just very high offensive capabilities.  On toons like Jedi Knight Anakin, it's crucial to use right timing.  Kill Bastila Shan (Fallen) first.
"""),
"SITH_EMPIRE_3v3_WO_MALAK": CounterTeam(id: "SITH_EMPIRE_3v3_WO_MALAK", name: "Sith Empire (w/o Malak)", type: .offense, squad: [#"Darth Revan"#, #"HK-47"#, #"Sith Marauder"#, #""#, #""#], zetas: [true, true, false, false, false], subs: #""#, description:"", strategy:""),
"SITH_EMPIRE_REYKILLER": CounterTeam(id: "SITH_EMPIRE_REYKILLER", name: "Sith Empire Rey-Killer", type: .offense, squad: [#"Darth Revan"#, #"Bastila Shan (Fallen)"#, #"Grand Admiral Thrawn"#, #"Wat Tambor"#, #"Geonosian Brood Alpha"#], zetas: [true, true, true, true, true], subs: #"(no subs, this team comp is used for a specific purpose)"#, description:"", strategy:""),
"SITH_TRI": CounterTeam(id: "SITH_TRI", name: "Sith Triumvirate", type: .offense, squad: [#"Darth Traya"#, #"Darth Sion"#, #"Darth Nihilus"#, #"Sith Empire Trooper"#, #"Grand Admiral Thrawn"#], zetas: [true, true, true, false, false], subs: #"Bastila Shan (Fallen), Count Dooku, Emperor Palpatine, Sith Assassin, Enfys Nest, Grand Moff Tarkin (L), Darth Vader, Any leftover Sith"#, description:
"""
Defensive lead that cripples the other team (Isolate, Pain, debuff removal).
""", strategy:
"""
Need better offensive capabilities than Traya's defense capabilities. There's no escape from annihilate, isolate or fracture. Just kill them before Darth Nihilus starts his groove by dealing huge amount of damage.  This team has big damage with turn meter gain / turn meter removal.  Normal kill order should be stun Thrawn then kill Bastila Shan (Fallen) (if used), Traya, Thrawn, Palpatine (if used), Sion, Nihilus, then whoever is left.
"""),
"SITH_TRI_3v3": CounterTeam(id: "SITH_TRI_3v3", name: "Sith Triumvirate", type: .offense, squad: [#"Darth Traya"#, #"Darth Sion"#, #"Darth Nihilus"#, #""#, #""#], zetas: [true, true, false, false, false], subs: #""#, description:"", strategy:""),
"THRAWN_3v3": CounterTeam(id: "THRAWN_3v3", name: "Thrawn L", type: .offense, squad: [#"Grand Admiral Thrawn"#, #"Wampa"#, #"Death Trooper"#, #""#, #""#], zetas: [true, false, false, false, false], subs: #"Enfys Nest for Wampa, Shoretrooper for Death Trooper"#, description:"", strategy:""),
"THRAWN_3v3_SHOREDEATH": CounterTeam(id: "THRAWN_3v3_SHOREDEATH", name: "Thrawn L (w / Shoretrooper and Death Trooper)", type: .offense, squad: [#"Grand Admiral Thrawn"#, #"Shoretrooper"#, #"Death Trooper"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"THRAWN_3v3_HANCHEWIE": CounterTeam(id: "THRAWN_3v3_HANCHEWIE", name: "Thrawn L (w/ Han + Chewie)", type: .offense, squad: [#"Grand Admiral Thrawn"#, #"Han Solo"#, #"Chewbacca"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"IT_THRAWN": CounterTeam(id: "IT_THRAWN", name: "Thrawn Troopers", type: .offense, squad: [#"Grand Admiral Thrawn"#, #"Range Trooper"#, #"Death Trooper"#, #"Shoretrooper"#, #""#], zetas: [true, true, true, true, false], subs: #"Fill in last slot with an Imperial Trooper or Empire"#, description:"", strategy:""),
"VISAS_MARR": CounterTeam(id: "VISAS_MARR", name: "Visas Marr", type: .offense, squad: [#"Visas Marr"#, #""#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #"NOTE: This is not a full team"#, description:"", strategy:""),
"WAMPA_SOLO": CounterTeam(id: "WAMPA_SOLO", name: "Wampa (solo)", type: .offense, squad: [#"Wampa"#, #""#, #""#, #""#, #""#], zetas: [true, false, false, false, false], subs: #"Additions can include: CT-7567 "Rex" (L), Hermit Yoda, Darth Nihilus, Mother Talzin, Baze Malbus + Chirrut Îmwe, R2-D2, Enfys Nest, Han Solo + Chewbacca, General Kenobi, Barriss Offee, Visas Marr, Obi-Wan Kenobi (Old Ben), Sun Fac, First Order Officer"#, description:"", strategy:""),
"WAT_3v3_SUNFACWAMPA": CounterTeam(id: "WAT_3v3_SUNFACWAMPA", name: "Wat L (w/ Sun Fac and Wampa)", type: .offense, squad: [#"Wat Tambor"#, #"Sun Fac"#, #"Wampa"#, #""#, #""#], zetas: [true, true, true, false, false], subs: #""#, description:"", strategy:""),
"ZADER": CounterTeam(id: "ZADER", name: "Zader", type: .offense, squad: [#"Darth Vader"#, #"Wampa"#, #"Grand Moff Tarkin"#, #"Darth Sidious"#, #"Boba Fett"#], zetas: [true, true, false, false, false], subs: #"zBarriss Offee (for long fights), Mission + Zaalbar, Canderous Ordo, Sith Empire Trooper/Shoretrooper/Stormtrooper, Mother Talzin"#, description:"", strategy:""),
 ]
}
