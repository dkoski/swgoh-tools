public let characterDemographics = [
"ARCTROOPER501ST" : CharacterDeomographics(id: "ARCTROOPER501ST", viable: 48.68, zetas: ["uniqueskill_ARCTROOPER501ST01":100.0], g13: 64.03, r5: 40.62),
"AAYLASECURA" : CharacterDeomographics(id: "AAYLASECURA", viable: 16.6, zetas: [:], g13: 49.55, r5: 5.0),
"ADMIRALACKBAR" : CharacterDeomographics(id: "ADMIRALACKBAR", viable: 49.06, zetas: [:], g13: 4.15, r5: 1.85),
"ADMIRALPIETT" : CharacterDeomographics(id: "ADMIRALPIETT", viable: 20.53, zetas: ["specialskill_ADMIRALPIETT02":100.0, "uniqueskill_ADMIRALPIETT01":100.0], g13: 64.71, r5: 56.62),
"AHSOKATANO" : CharacterDeomographics(id: "AHSOKATANO", viable: 85.43, zetas: ["uniqueskill_AHSOKATANO01":87.19], g13: 59.45, r5: 30.83),
"FULCRUMAHSOKA" : CharacterDeomographics(id: "FULCRUMAHSOKA", viable: 11.92, zetas: ["specialskill_FULCRUMAHSOKA02":71.52], g13: 10.76, r5: 6.96),
"AMILYNHOLDO" : CharacterDeomographics(id: "AMILYNHOLDO", viable: 14.64, zetas: ["uniqueskill_AMILYNHOLDO01":69.59], g13: 59.28, r5: 54.12),
"ASAJVENTRESS" : CharacterDeomographics(id: "ASAJVENTRESS", viable: 78.64, zetas: ["uniqueskill_ASAJVENTRESS01":86.56, "leaderskill_ASAJVENTRESS":69.58], g13: 54.32, r5: 18.14),
"AURRA_SING" : CharacterDeomographics(id: "AURRA_SING", viable: 15.77, zetas: ["leaderskill_AURRA_SING":30.62, "specialskill_AURRA_SING02":29.19], g13: 6.7, r5: 3.83),
"B1BATTLEDROIDV2" : CharacterDeomographics(id: "B1BATTLEDROIDV2", viable: 61.21, zetas: ["uniqueskill_b1battledroidv2_02":100.0], g13: 79.41, r5: 36.62),
"B2SUPERBATTLEDROID" : CharacterDeomographics(id: "B2SUPERBATTLEDROID", viable: 59.17, zetas: ["uniqueskill_B2SUPERBATTLEDROID02":100.0], g13: 66.33, r5: 16.07),
"BB8" : CharacterDeomographics(id: "BB8", viable: 50.79, zetas: ["uniqueskill_BB802":69.39, "uniqueskill_BB801":100.0], g13: 31.05, r5: 21.1),
"BARRISSOFFEE" : CharacterDeomographics(id: "BARRISSOFFEE", viable: 28.91, zetas: ["uniqueskill_BARRISSOFFEE01":100.0], g13: 9.4, r5: 2.61),
"BASTILASHAN" : CharacterDeomographics(id: "BASTILASHAN", viable: 82.34, zetas: ["leaderskill_BASTILASHAN":84.33], g13: 33.27, r5: 13.11),
"BASTILASHANDARK" : CharacterDeomographics(id: "BASTILASHANDARK", viable: 75.02, zetas: ["uniqueskill_BASTILASHANDARK02":94.27], g13: 62.27, r5: 29.68),
"BAZEMALBUS" : CharacterDeomographics(id: "BAZEMALBUS", viable: 19.02, zetas: [:], g13: 2.78, r5: 1.59),
"BIGGSDARKLIGHTER" : CharacterDeomographics(id: "BIGGSDARKLIGHTER", viable: 52.0, zetas: [:], g13: 34.83, r5: 1.74),
"BISTAN" : CharacterDeomographics(id: "BISTAN", viable: 4.91, zetas: [:], g13: 15.38, r5: 12.31),
"BOKATAN" : CharacterDeomographics(id: "BOKATAN", viable: 9.13, zetas: ["uniqueskill_BOKATAN01":95.04], g13: 52.89, r5: 47.93),
"BOBAFETT" : CharacterDeomographics(id: "BOBAFETT", viable: 78.87, zetas: ["uniqueskill_BOBAFETT01":64.98], g13: 18.85, r5: 6.12),
"BODHIROOK" : CharacterDeomographics(id: "BODHIROOK", viable: 0.53, zetas: ["uniqueskill_BODHIROOK01":100.0], g13: 0.0, r5: 0.0),
"BOSSK" : CharacterDeomographics(id: "BOSSK", viable: 78.64, zetas: ["uniqueskill_BOSSK01":79.65, "leaderskill_BOSSK":100.0], g13: 51.73, r5: 17.75),
"C3POLEGENDARY" : CharacterDeomographics(id: "C3POLEGENDARY", viable: 65.74, zetas: ["specialskill_C3POLEGENDARY01":98.16, "uniqueskill_C3POLEGENDARY02":83.01], g13: 73.02, r5: 45.35),
"CC2224" : CharacterDeomographics(id: "CC2224", viable: 37.13, zetas: ["leaderskill_CC2224":58.13], g13: 25.61, r5: 17.28),
"CT210408" : CharacterDeomographics(id: "CT210408", viable: 63.25, zetas: ["uniqueskill_ct21040802":87.11], g13: 54.53, r5: 33.41),
"CT5555" : CharacterDeomographics(id: "CT5555", viable: 65.58, zetas: ["uniqueskill_CT555501":92.98, "uniqueskill_CT555502":100.0], g13: 66.86, r5: 45.8),
"CT7567" : CharacterDeomographics(id: "CT7567", viable: 73.36, zetas: ["uniqueskill_REX01":87.24], g13: 55.97, r5: 32.2),
"CADBANE" : CharacterDeomographics(id: "CADBANE", viable: 63.02, zetas: [:], g13: 14.37, r5: 10.42),
"CANDEROUSORDO" : CharacterDeomographics(id: "CANDEROUSORDO", viable: 59.4, zetas: ["uniqueskill_CANDEROUSORDO01":54.38], g13: 2.92, r5: 0.89),
"HOTHHAN" : CharacterDeomographics(id: "HOTHHAN", viable: 36.83, zetas: ["uniqueskill_HOTHHAN01":51.23], g13: 89.55, r5: 8.4),
"PHASMA" : CharacterDeomographics(id: "PHASMA", viable: 39.25, zetas: ["leaderskill_PHASMA":53.85], g13: 58.27, r5: 52.12),
"CARADUNE" : CharacterDeomographics(id: "CARADUNE", viable: 9.58, zetas: ["uniqueskill_CARADUNE02":100.0], g13: 12.6, r5: 4.72),
"CARTHONASI" : CharacterDeomographics(id: "CARTHONASI", viable: 70.72, zetas: ["leaderskill_carthonasi":90.39], g13: 2.24, r5: 0.43),
"CASSIANANDOR" : CharacterDeomographics(id: "CASSIANANDOR", viable: 22.79, zetas: ["uniqueskill_CASSIANANDOR01":32.12], g13: 2.32, r5: 1.32),
"CHEWBACCALEGENDARY" : CharacterDeomographics(id: "CHEWBACCALEGENDARY", viable: 63.85, zetas: ["uniqueskill_CHEWBACCALEGENDARY01":100.0, "uniqueskill_CHEWBACCALEGENDARY02":87.47], g13: 69.74, r5: 46.69),
"CHIEFCHIRPA" : CharacterDeomographics(id: "CHIEFCHIRPA", viable: 62.49, zetas: ["leaderskill_CHIEFCHIRPA":100.0], g13: 2.66, r5: 0.72),
"CHIEFNEBIT" : CharacterDeomographics(id: "CHIEFNEBIT", viable: 5.89, zetas: [:], g13: 3.85, r5: 2.56),
"CHIRRUTIMWE" : CharacterDeomographics(id: "CHIRRUTIMWE", viable: 24.45, zetas: [:], g13: 4.01, r5: 1.23),
"CHOPPERS3" : CharacterDeomographics(id: "CHOPPERS3", viable: 45.74, zetas: [:], g13: 0.99, r5: 0.5),
"CLONESERGEANTPHASEI" : CharacterDeomographics(id: "CLONESERGEANTPHASEI", viable: 54.87, zetas: [:], g13: 19.39, r5: 13.89),
"CLONEWARSCHEWBACCA" : CharacterDeomographics(id: "CLONEWARSCHEWBACCA", viable: 5.21, zetas: ["specialskill_CLONEWARSCHEWBACCA01":31.88], g13: 1.45, r5: 0.0),
"COLONELSTARCK" : CharacterDeomographics(id: "COLONELSTARCK", viable: 34.11, zetas: ["uniqueskill_COLONELSTARCK01":70.13], g13: 42.92, r5: 4.65),
"COMMANDERAHSOKA" : CharacterDeomographics(id: "COMMANDERAHSOKA", viable: 1.43, zetas: ["uniqueskill_COMMANDERAHSOKA01":100.0, "specialskill_COMMANDERAHSOKA01":100.0, "specialskill_COMMANDERAHSOKA02":100.0], g13: 78.95, r5: 78.95),
"COMMANDERLUKESKYWALKER" : CharacterDeomographics(id: "COMMANDERLUKESKYWALKER", viable: 76.75, zetas: ["uniqueskill_COMMANDERLUKESKYWALKER01":85.05, "leaderskill_COMMANDERLUKESKYWALKER":92.13, "uniqueskill_COMMANDERLUKESKYWALKER02":96.76], g13: 76.79, r5: 37.95),
"CORUSCANTUNDERWORLDPOLICE" : CharacterDeomographics(id: "CORUSCANTUNDERWORLDPOLICE", viable: 2.11, zetas: [:], g13: 7.14, r5: 3.57),
"COUNTDOOKU" : CharacterDeomographics(id: "COUNTDOOKU", viable: 73.43, zetas: ["uniqueskill_COUNTDOOKU01":79.96], g13: 26.93, r5: 18.4),
"DARKTROOPER" : CharacterDeomographics(id: "DARKTROOPER", viable: 6.72, zetas: ["specialskill_DARKTROOPER01":100.0], g13: 16.85, r5: 11.24),
"DARTHMALAK" : CharacterDeomographics(id: "DARTHMALAK", viable: 57.81, zetas: ["uniqueskill_DARTHMALAK01":100.0, "uniqueskill_DARTHMALAK02":100.0], g13: 75.2, r5: 54.83),
"MAUL" : CharacterDeomographics(id: "MAUL", viable: 49.43, zetas: ["leaderskill_MAUL":43.97], g13: 25.8, r5: 2.29),
"DARTHNIHILUS" : CharacterDeomographics(id: "DARTHNIHILUS", viable: 52.83, zetas: ["leaderskill_DARTHNIHILUS":31.43, "uniqueskill_DARTHNIHILUS01":83.0], g13: 21.43, r5: 5.29),
"DARTHREVAN" : CharacterDeomographics(id: "DARTHREVAN", viable: 67.7, zetas: ["uniqueskill_DARTHREVAN02":97.88, "leaderskill_DARTHREVAN":99.44, "uniqueskill_DARTHREVAN01":99.67], g13: 87.29, r5: 56.41),
"DARTHSIDIOUS" : CharacterDeomographics(id: "DARTHSIDIOUS", viable: 46.79, zetas: ["uniqueskill_DARTHSIDIOUS01":49.35], g13: 26.94, r5: 23.23),
"DARTHSION" : CharacterDeomographics(id: "DARTHSION", viable: 47.32, zetas: ["uniqueskill_DARTHSION01":100.0], g13: 22.49, r5: 6.38),
"DARTHTRAYA" : CharacterDeomographics(id: "DARTHTRAYA", viable: 49.28, zetas: ["uniqueskill_DARTHTRAYA01":100.0, "leaderskill_DARTHTRAYA":100.0], g13: 31.7, r5: 9.49),
"VADER" : CharacterDeomographics(id: "VADER", viable: 91.25, zetas: ["uniqueskill_VADER01":89.16, "leaderskill_VADER":79.82, "specialskill_VADER03":97.19], g13: 82.55, r5: 57.07),
"DATHCHA" : CharacterDeomographics(id: "DATHCHA", viable: 15.4, zetas: [:], g13: 0.98, r5: 0.98),
"DEATHTROOPER" : CharacterDeomographics(id: "DEATHTROOPER", viable: 25.89, zetas: ["uniqueskill_DEATHTROOPER01":26.82], g13: 7.87, r5: 2.04),
"DENGAR" : CharacterDeomographics(id: "DENGAR", viable: 48.68, zetas: ["uniqueskill_DENGAR01":48.84], g13: 8.06, r5: 2.33),
"DIRECTORKRENNIC" : CharacterDeomographics(id: "DIRECTORKRENNIC", viable: 15.32, zetas: ["leaderskill_DIRECTORKRENNIC":30.54], g13: 68.47, r5: 0.99),
"DROIDEKA" : CharacterDeomographics(id: "DROIDEKA", viable: 57.89, zetas: ["uniqueskill_DROIDEKA01":100.0], g13: 57.76, r5: 14.34),
"BADBATCHECHO" : CharacterDeomographics(id: "BADBATCHECHO", viable: 2.04, zetas: ["uniqueskill_BADBATCHECHO01":100.0], g13: 22.22, r5: 18.52),
"EETHKOTH" : CharacterDeomographics(id: "EETHKOTH", viable: 7.25, zetas: [:], g13: 0.0, r5: 0.0),
"EMBO" : CharacterDeomographics(id: "EMBO", viable: 13.81, zetas: ["uniqueskill_EMBO01":46.45, "leaderskill_EMBO":22.4], g13: 8.2, r5: 3.83),
"EMPERORPALPATINE" : CharacterDeomographics(id: "EMPERORPALPATINE", viable: 87.7, zetas: ["leaderskill_EMPERORPALPATINE":88.81, "uniqueskill_EMPERORPALPATINE01":80.55], g13: 63.51, r5: 45.01),
"ENFYSNEST" : CharacterDeomographics(id: "ENFYSNEST", viable: 49.51, zetas: ["uniqueskill_ENFYSNEST01":90.09], g13: 13.87, r5: 5.34),
"EWOKELDER" : CharacterDeomographics(id: "EWOKELDER", viable: 76.68, zetas: [:], g13: 1.97, r5: 0.39),
"EWOKSCOUT" : CharacterDeomographics(id: "EWOKSCOUT", viable: 39.02, zetas: [:], g13: 0.97, r5: 0.39),
"EZRABRIDGERS3" : CharacterDeomographics(id: "EZRABRIDGERS3", viable: 70.26, zetas: ["specialskill_EZRABRIDGERS301":56.93], g13: 5.8, r5: 2.15),
"FINN" : CharacterDeomographics(id: "FINN", viable: 45.13, zetas: ["leaderskill_FINN":54.01], g13: 28.93, r5: 24.08),
"FIRSTORDEREXECUTIONER" : CharacterDeomographics(id: "FIRSTORDEREXECUTIONER", viable: 50.72, zetas: [:], g13: 48.51, r5: 42.41),
"FIRSTORDEROFFICERMALE" : CharacterDeomographics(id: "FIRSTORDEROFFICERMALE", viable: 54.04, zetas: [:], g13: 43.02, r5: 38.69),
"FIRSTORDERSPECIALFORCESPILOT" : CharacterDeomographics(id: "FIRSTORDERSPECIALFORCESPILOT", viable: 27.62, zetas: [:], g13: 75.96, r5: 2.73),
"FIRSTORDERTROOPER" : CharacterDeomographics(id: "FIRSTORDERTROOPER", viable: 29.43, zetas: ["uniqueskill_FIRSTORDERTROOPER01":91.54], g13: 75.9, r5: 70.51),
"FIRSTORDERTIEPILOT" : CharacterDeomographics(id: "FIRSTORDERTIEPILOT", viable: 56.0, zetas: ["uniqueskill_FIRSTORDERTIEPILOT01":39.08], g13: 40.16, r5: 2.29),
"GAMORREANGUARD" : CharacterDeomographics(id: "GAMORREANGUARD", viable: 0.53, zetas: [:], g13: 14.29, r5: 14.29),
"GARSAXON" : CharacterDeomographics(id: "GARSAXON", viable: 3.32, zetas: ["leaderskill_GARSAXON":54.55], g13: 4.55, r5: 2.27),
"ZEBS3" : CharacterDeomographics(id: "ZEBS3", viable: 20.23, zetas: ["specialskill_ZEBS301":70.52], g13: 4.1, r5: 1.87),
"GRIEVOUS" : CharacterDeomographics(id: "GRIEVOUS", viable: 71.47, zetas: ["leaderskill_GRIEVOUS":93.45, "uniqueskill_GRIEVOUS01":95.99], g13: 66.0, r5: 44.14),
"GENERALHUX" : CharacterDeomographics(id: "GENERALHUX", viable: 34.26, zetas: ["uniqueskill_GENERALHUX01":100.0], g13: 72.69, r5: 64.98),
"GENERALKENOBI" : CharacterDeomographics(id: "GENERALKENOBI", viable: 77.66, zetas: ["uniqueskill_GENERALKENOBI01":98.25], g13: 87.95, r5: 65.01),
"GENERALSKYWALKER" : CharacterDeomographics(id: "GENERALSKYWALKER", viable: 42.72, zetas: ["leaderskill_GENERALSKYWALKER":100.0, "uniqueskill_GENERALSKYWALKER02":100.0, "basicskill_GENERALSKYWALKER":100.0, "uniqueskill_GENERALSKYWALKER01":100.0], g13: 85.87, r5: 77.03),
"VEERS" : CharacterDeomographics(id: "VEERS", viable: 49.43, zetas: ["uniqueskill_VEERS01":78.63], g13: 30.38, r5: 4.27),
"GEONOSIANBROODALPHA" : CharacterDeomographics(id: "GEONOSIANBROODALPHA", viable: 78.42, zetas: ["uniqueskill_GEONOSIANBROODALPHA01":100.0, "leaderskill_GEONOSIANBROODALPHA":90.86], g13: 47.45, r5: 16.75),
"GEONOSIANSOLDIER" : CharacterDeomographics(id: "GEONOSIANSOLDIER", viable: 82.49, zetas: [:], g13: 15.55, r5: 3.93),
"GEONOSIANSPY" : CharacterDeomographics(id: "GEONOSIANSPY", viable: 77.21, zetas: [:], g13: 26.69, r5: 8.02),
"GRANDADMIRALTHRAWN" : CharacterDeomographics(id: "GRANDADMIRALTHRAWN", viable: 71.32, zetas: ["uniqueskill_GRANDADMIRALTHRAWN01":90.48, "leaderskill_GRANDADMIRALTHRAWN":44.13], g13: 49.31, r5: 29.84),
"GRANDMASTERYODA" : CharacterDeomographics(id: "GRANDMASTERYODA", viable: 89.06, zetas: ["specialskill_GRANDMASTERYODA03":89.07, "leaderskill_GRANDMASTERYODA":40.85], g13: 65.85, r5: 37.03),
"GRANDMOFFTARKIN" : CharacterDeomographics(id: "GRANDMOFFTARKIN", viable: 72.68, zetas: ["uniqueskill_GRANDMOFFTARKIN01":33.96, "leaderskill_GRANDMOFFTARKIN":22.01], g13: 21.08, r5: 2.39),
"GREEDO" : CharacterDeomographics(id: "GREEDO", viable: 40.38, zetas: ["specialskill_GREEDO02":51.21], g13: 4.86, r5: 1.87),
"GREEFKARGA" : CharacterDeomographics(id: "GREEFKARGA", viable: 17.51, zetas: ["specialskill_GREEFKARGA01":100.0, "specialskill_GREEFKARGA02":100.0], g13: 14.66, r5: 4.31),
"HK47" : CharacterDeomographics(id: "HK47", viable: 72.6, zetas: ["uniqueskill_HK4702":73.91, "uniqueskill_HK4701":76.3], g13: 41.06, r5: 16.42),
"HANSOLO" : CharacterDeomographics(id: "HANSOLO", viable: 76.08, zetas: ["uniqueskill_HANSOLO01":96.63], g13: 52.38, r5: 34.23),
"HERASYNDULLAS3" : CharacterDeomographics(id: "HERASYNDULLAS3", viable: 39.4, zetas: ["specialskill_HERASYNDULLAS301":50.77], g13: 4.6, r5: 1.92),
"HERMITYODA" : CharacterDeomographics(id: "HERMITYODA", viable: 58.57, zetas: ["specialskill_HERMITYODA02":92.91, "uniqueskill_HERMITYODA02":64.82], g13: 61.86, r5: 37.24),
"HOTHREBELSCOUT" : CharacterDeomographics(id: "HOTHREBELSCOUT", viable: 7.62, zetas: [:], g13: 8.91, r5: 3.96),
"HOTHREBELSOLDIER" : CharacterDeomographics(id: "HOTHREBELSOLDIER", viable: 1.51, zetas: [:], g13: 0.0, r5: 0.0),
"BADBATCHHUNTER" : CharacterDeomographics(id: "BADBATCHHUNTER", viable: 2.87, zetas: ["leaderskill_BADBATCHHUNTER":100.0], g13: 18.42, r5: 13.16),
"MAGNAGUARD" : CharacterDeomographics(id: "MAGNAGUARD", viable: 58.26, zetas: ["specialskill_MAGNAGUARD02":63.99], g13: 62.56, r5: 28.24),
"IG11" : CharacterDeomographics(id: "IG11", viable: 10.87, zetas: ["uniqueskill_IG1101":100.0], g13: 11.81, r5: 4.17),
"IG86SENTINELDROID" : CharacterDeomographics(id: "IG86SENTINELDROID", viable: 8.08, zetas: [:], g13: 0.0, r5: 0.0),
"IG88" : CharacterDeomographics(id: "IG88", viable: 61.81, zetas: ["uniqueskill_IG8801":30.89], g13: 4.15, r5: 1.71),
"IMAGUNDI" : CharacterDeomographics(id: "IMAGUNDI", viable: 5.36, zetas: [:], g13: 0.0, r5: 0.0),
"IMPERIALPROBEDROID" : CharacterDeomographics(id: "IMPERIALPROBEDROID", viable: 7.17, zetas: ["uniqueskill_IMPERIALPROBEDROID01":100.0], g13: 5.26, r5: 2.11),
"IMPERIALSUPERCOMMANDO" : CharacterDeomographics(id: "IMPERIALSUPERCOMMANDO", viable: 1.81, zetas: [:], g13: 4.17, r5: 0.0),
"JANGOFETT" : CharacterDeomographics(id: "JANGOFETT", viable: 67.4, zetas: ["uniqueskill_JANGOFETT01":72.12, "leaderskill_JANGOFETT":51.18], g13: 27.1, r5: 14.56),
"JAWA" : CharacterDeomographics(id: "JAWA", viable: 4.83, zetas: [:], g13: 3.13, r5: 1.56),
"JAWAENGINEER" : CharacterDeomographics(id: "JAWAENGINEER", viable: 8.6, zetas: [:], g13: 1.75, r5: 1.75),
"JAWASCAVENGER" : CharacterDeomographics(id: "JAWASCAVENGER", viable: 6.34, zetas: [:], g13: 5.95, r5: 3.57),
"JEDIKNIGHTCONSULAR" : CharacterDeomographics(id: "JEDIKNIGHTCONSULAR", viable: 32.45, zetas: [:], g13: 0.47, r5: 0.0),
"ANAKINKNIGHT" : CharacterDeomographics(id: "ANAKINKNIGHT", viable: 81.74, zetas: ["uniqueskill_ANAKINKNIGHT01":90.49], g13: 69.99, r5: 52.35),
"JEDIKNIGHTGUARDIAN" : CharacterDeomographics(id: "JEDIKNIGHTGUARDIAN", viable: 0.45, zetas: [:], g13: 0.0, r5: 0.0),
"JEDIKNIGHTLUKE" : CharacterDeomographics(id: "JEDIKNIGHTLUKE", viable: 27.4, zetas: ["uniqueskill_JEDIKNIGHTLUKE01":100.0, "leaderskill_JEDIKNIGHTLUKE":100.0], g13: 90.91, r5: 82.37),
"JEDIKNIGHTREVAN" : CharacterDeomographics(id: "JEDIKNIGHTREVAN", viable: 78.42, zetas: ["leaderskill_JEDIKNIGHTREVAN":100.0, "uniqueskill_JEDIKNIGHTREVAN02":99.81, "specialskill_JEDIKNIGHTREVAN02":97.11], g13: 83.83, r5: 47.55),
"JEDIMASTERKENOBI" : CharacterDeomographics(id: "JEDIMASTERKENOBI", viable: 2.42, zetas: ["basicskill_JEDIMASTERKENOBI":100.0, "specialskill_JEDIMASTERKENOBI01":96.88, "leaderskill_JEDIMASTERKENOBI":100.0, "uniqueskill_JEDIMASTERKENOBI01":100.0, "specialskill_JEDIMASTERKENOBI02":100.0, "uniqueskill_GALACTICLEGEND01":100.0], g13: 93.75, r5: 90.63),
"GRANDMASTERLUKE" : CharacterDeomographics(id: "GRANDMASTERLUKE", viable: 12.6, zetas: ["specialskill_GRANDMASTERLUKE02":100.0, "leaderskill_GRANDMASTERLUKE":100.0, "uniqueskill_GRANDMASTERLUKE01":100.0, "uniqueskill_GALACTICLEGEND01":100.0, "basicskill_GRANDMASTERLUKE":100.0, "specialskill_GRANDMASTERLUKE01":100.0], g13: 96.41, r5: 94.01),
"JOLEEBINDO" : CharacterDeomographics(id: "JOLEEBINDO", viable: 77.58, zetas: ["specialskill_JOLEEBINDO02":100.0], g13: 39.01, r5: 14.69),
"JUHANI" : CharacterDeomographics(id: "JUHANI", viable: 51.85, zetas: ["uniqueskill_juhani01":53.86], g13: 3.64, r5: 0.87),
"JYNERSO" : CharacterDeomographics(id: "JYNERSO", viable: 20.15, zetas: ["uniqueskill_JYNERSO01":28.46, "leaderskill_JYNERSO":79.4], g13: 3.0, r5: 1.12),
"K2SO" : CharacterDeomographics(id: "K2SO", viable: 9.28, zetas: ["uniqueskill_K2SO02":53.66], g13: 4.88, r5: 2.44),
"KANANJARRUSS3" : CharacterDeomographics(id: "KANANJARRUSS3", viable: 28.68, zetas: ["specialskill_KANANJARRUSS302":76.05], g13: 4.74, r5: 1.84),
"KIADIMUNDI" : CharacterDeomographics(id: "KIADIMUNDI", viable: 0.38, zetas: ["uniqueskill_kiadimundi01":100.0], g13: 20.0, r5: 20.0),
"KITFISTO" : CharacterDeomographics(id: "KITFISTO", viable: 2.42, zetas: [:], g13: 6.25, r5: 3.13),
"KUIIL" : CharacterDeomographics(id: "KUIIL", viable: 8.3, zetas: ["uniqueskill_KUIIL01":100.0], g13: 14.55, r5: 4.55),
"KYLOREN" : CharacterDeomographics(id: "KYLOREN", viable: 56.83, zetas: ["specialskill_KYLOREN02":96.68], g13: 51.26, r5: 42.9),
"KYLORENUNMASKED" : CharacterDeomographics(id: "KYLORENUNMASKED", viable: 65.36, zetas: ["uniqueskill_KYLORENUNMASKED01":86.84, "leaderskill_KYLORENUNMASKED":99.88], g13: 62.24, r5: 45.61),
"L3_37" : CharacterDeomographics(id: "L3_37", viable: 10.11, zetas: ["uniqueskill_l3_37_01":28.36], g13: 11.19, r5: 5.97),
"ADMINISTRATORLANDO" : CharacterDeomographics(id: "ADMINISTRATORLANDO", viable: 49.06, zetas: [:], g13: 67.23, r5: 41.69),
"LOBOT" : CharacterDeomographics(id: "LOBOT", viable: 4.0, zetas: [:], g13: 0.0, r5: 0.0),
"LOGRAY" : CharacterDeomographics(id: "LOGRAY", viable: 51.4, zetas: ["uniqueskill_LOGRAY01":55.51], g13: 1.32, r5: 0.29),
"LUKESKYWALKER" : CharacterDeomographics(id: "LUKESKYWALKER", viable: 17.89, zetas: ["uniqueskill_LUKESKYWALKER01":18.14, "leaderskill_LUKESKYWALKER":16.46], g13: 2.95, r5: 0.42),
"LUMINARAUNDULI" : CharacterDeomographics(id: "LUMINARAUNDULI", viable: 37.51, zetas: ["leaderskill_LUMINARAUNDULI":11.07], g13: 1.01, r5: 0.0),
"MACEWINDU" : CharacterDeomographics(id: "MACEWINDU", viable: 69.89, zetas: [:], g13: 13.39, r5: 0.76),
"MAGMATROOPER" : CharacterDeomographics(id: "MAGMATROOPER", viable: 6.49, zetas: [:], g13: 1.16, r5: 0.0),
"MISSIONVAO" : CharacterDeomographics(id: "MISSIONVAO", viable: 65.06, zetas: ["uniqueskill_MISSIONVAO02":91.07], g13: 3.48, r5: 0.81),
"HUMANTHUG" : CharacterDeomographics(id: "HUMANTHUG", viable: 0.3, zetas: [:], g13: 0.0, r5: 0.0),
"MOFFGIDEONS1" : CharacterDeomographics(id: "MOFFGIDEONS1", viable: 13.21, zetas: ["specialskill_MOFFGIDEONS102":100.0, "leaderskill_MOFFGIDEONS1":88.0], g13: 28.57, r5: 13.71),
"MONMOTHMA" : CharacterDeomographics(id: "MONMOTHMA", viable: 20.3, zetas: ["leaderskill_MONMOTHMA":100.0, "uniqueskill_MONMOTHMA01":100.0], g13: 73.61, r5: 69.52),
"MOTHERTALZIN" : CharacterDeomographics(id: "MOTHERTALZIN", viable: 53.36, zetas: ["uniqueskill_MOTHERTALZIN01":94.91, "leaderskill_MOTHERTALZIN":100.0], g13: 21.78, r5: 8.06),
"NIGHTSISTERACOLYTE" : CharacterDeomographics(id: "NIGHTSISTERACOLYTE", viable: 25.89, zetas: [:], g13: 2.92, r5: 1.17),
"NIGHTSISTERINITIATE" : CharacterDeomographics(id: "NIGHTSISTERINITIATE", viable: 3.62, zetas: ["uniqueskill_NIGHTSISTERINITIATE01":70.83], g13: 14.58, r5: 4.17),
"NIGHTSISTERSPIRIT" : CharacterDeomographics(id: "NIGHTSISTERSPIRIT", viable: 21.74, zetas: [:], g13: 10.42, r5: 5.9),
"NIGHTSISTERZOMBIE" : CharacterDeomographics(id: "NIGHTSISTERZOMBIE", viable: 56.91, zetas: [:], g13: 14.59, r5: 8.62),
"NUTEGUNRAY" : CharacterDeomographics(id: "NUTEGUNRAY", viable: 64.0, zetas: ["uniqueskill_NUTEGUNRAY01":78.89], g13: 3.42, r5: 1.3),
"OLDBENKENOBI" : CharacterDeomographics(id: "OLDBENKENOBI", viable: 50.34, zetas: ["specialskill_OLDBENKENOBI02":57.12, "uniqueskill_OLDBENKENOBI01":65.37], g13: 43.18, r5: 35.83),
"DAKA" : CharacterDeomographics(id: "DAKA", viable: 60.53, zetas: ["uniqueskill_DAKA01":80.3], g13: 27.56, r5: 13.59),
"BADBATCHOMEGA" : CharacterDeomographics(id: "BADBATCHOMEGA", viable: 0.45, zetas: ["uniqueskill_BADBATCHOMEGA01":100.0, "specialskill_BADBATCHOMEGA01":100.0], g13: 50.0, r5: 50.0),
"PADMEAMIDALA" : CharacterDeomographics(id: "PADMEAMIDALA", viable: 73.81, zetas: ["leaderskill_PADMEAMIDALA":100.0, "uniqueskill_PADMEAMIDALA01":100.0], g13: 75.77, r5: 41.21),
"PAO" : CharacterDeomographics(id: "PAO", viable: 8.91, zetas: ["uniqueskill_PAO01":91.53], g13: 11.02, r5: 5.08),
"PAPLOO" : CharacterDeomographics(id: "PAPLOO", viable: 67.17, zetas: ["uniqueskill_PAPLOO01":44.04], g13: 1.24, r5: 0.11),
"PLOKOON" : CharacterDeomographics(id: "PLOKOON", viable: 34.34, zetas: [:], g13: 5.93, r5: 2.2),
"POE" : CharacterDeomographics(id: "POE", viable: 26.57, zetas: [:], g13: 35.23, r5: 32.1),
"POGGLETHELESSER" : CharacterDeomographics(id: "POGGLETHELESSER", viable: 81.43, zetas: [:], g13: 8.71, r5: 2.13),
"PRINCESSLEIA" : CharacterDeomographics(id: "PRINCESSLEIA", viable: 50.04, zetas: ["uniqueskill_PRINCESSLEIA01":42.68], g13: 34.09, r5: 1.36),
"QIRA" : CharacterDeomographics(id: "QIRA", viable: 25.51, zetas: ["uniqueskill_QIRA01":40.53], g13: 1.18, r5: 0.3),
"QUIGONJINN" : CharacterDeomographics(id: "QUIGONJINN", viable: 37.43, zetas: ["leaderskill_QUIGONJINN":47.58], g13: 22.38, r5: 2.02),
"R2D2_LEGENDARY" : CharacterDeomographics(id: "R2D2_LEGENDARY", viable: 68.15, zetas: ["uniqueskill_R2D2_LEGENDARY01":85.83, "uniqueskill_R2D2_LEGENDARY02":92.69], g13: 43.41, r5: 30.34),
"RANGETROOPER" : CharacterDeomographics(id: "RANGETROOPER", viable: 11.92, zetas: [:], g13: 24.68, r5: 9.49),
"HOTHLEIA" : CharacterDeomographics(id: "HOTHLEIA", viable: 41.28, zetas: ["uniqueskill_HOTHLEIA01":59.23], g13: 80.8, r5: 13.71),
"EPIXFINN" : CharacterDeomographics(id: "EPIXFINN", viable: 11.17, zetas: ["uniqueskill_EPIXFINN01":100.0], g13: 77.7, r5: 72.97),
"EPIXPOE" : CharacterDeomographics(id: "EPIXPOE", viable: 10.94, zetas: ["uniqueskill_EPIXPOE01":100.0], g13: 77.24, r5: 71.03),
"RESISTANCEPILOT" : CharacterDeomographics(id: "RESISTANCEPILOT", viable: 16.3, zetas: [:], g13: 53.7, r5: 2.31),
"RESISTANCETROOPER" : CharacterDeomographics(id: "RESISTANCETROOPER", viable: 38.94, zetas: [:], g13: 26.55, r5: 23.64),
"GLREY" : CharacterDeomographics(id: "GLREY", viable: 6.42, zetas: ["basicskill_GLREY":98.82, "specialskill_GLREY01":98.82, "specialskill_GLREY02":100.0, "uniqueskill_GLREY01":100.0, "uniqueskill_GALACTICLEGEND01":100.0, "leaderskill_GLREY":100.0], g13: 95.29, r5: 92.94),
"REYJEDITRAINING" : CharacterDeomographics(id: "REYJEDITRAINING", viable: 54.19, zetas: ["uniqueskill_REYJEDITRAINING01":81.75, "leaderskill_REYJEDITRAINING":100.0, "uniqueskill_REYJEDITRAINING02":91.78], g13: 69.64, r5: 48.61),
"REY" : CharacterDeomographics(id: "REY", viable: 60.75, zetas: ["uniqueskill_REY01":37.76], g13: 19.25, r5: 15.53),
"ROSETICO" : CharacterDeomographics(id: "ROSETICO", viable: 12.75, zetas: ["uniqueskill_ROSETICO01":19.53], g13: 62.72, r5: 57.4),
"ROYALGUARD" : CharacterDeomographics(id: "ROYALGUARD", viable: 23.7, zetas: [:], g13: 50.64, r5: 1.59),
"SABINEWRENS3" : CharacterDeomographics(id: "SABINEWRENS3", viable: 27.09, zetas: ["specialskill_SABINEWRENS302":65.46], g13: 5.57, r5: 2.51),
"SAVAGEOPRESS" : CharacterDeomographics(id: "SAVAGEOPRESS", viable: 30.79, zetas: ["uniqueskill_SAVAGEOPRESS01":58.33], g13: 2.21, r5: 0.98),
"SCARIFREBEL" : CharacterDeomographics(id: "SCARIFREBEL", viable: 5.43, zetas: [:], g13: 20.83, r5: 13.89),
"SHAAKTI" : CharacterDeomographics(id: "SHAAKTI", viable: 62.87, zetas: ["leaderskill_SHAAKTI":98.2, "uniqueskill_SHAAKTI01":100.0], g13: 63.99, r5: 33.85),
"SHORETROOPER" : CharacterDeomographics(id: "SHORETROOPER", viable: 22.57, zetas: [:], g13: 12.04, r5: 4.01),
"SITHASSASSIN" : CharacterDeomographics(id: "SITHASSASSIN", viable: 7.47, zetas: [:], g13: 11.11, r5: 2.02),
"SITHTROOPER" : CharacterDeomographics(id: "SITHTROOPER", viable: 41.06, zetas: [:], g13: 29.6, r5: 13.6),
"SITHPALPATINE" : CharacterDeomographics(id: "SITHPALPATINE", viable: 8.38, zetas: ["basicskill_SITHPALPATINE":100.0, "uniqueskill_SITHPALPATINE01":100.0, "uniqueskill_GALACTICLEGEND01":99.1, "leaderskill_SITHPALPATINE":100.0, "specialskill_SITHPALPATINE01":100.0, "specialskill_SITHPALPATINE02":97.3], g13: 94.59, r5: 93.69),
"SITHMARAUDER" : CharacterDeomographics(id: "SITHMARAUDER", viable: 25.13, zetas: [:], g13: 66.97, r5: 51.95),
"FOSITHTROOPER" : CharacterDeomographics(id: "FOSITHTROOPER", viable: 30.72, zetas: ["uniqueskill_FOSITHTROOPER01":100.0], g13: 73.96, r5: 70.52),
"SNOWTROOPER" : CharacterDeomographics(id: "SNOWTROOPER", viable: 21.06, zetas: [:], g13: 4.66, r5: 2.15),
"STORMTROOPER" : CharacterDeomographics(id: "STORMTROOPER", viable: 14.79, zetas: ["uniqueskill_STORMTROOPER01":34.69], g13: 7.65, r5: 4.59),
"STORMTROOPERHAN" : CharacterDeomographics(id: "STORMTROOPERHAN", viable: 33.81, zetas: ["uniqueskill_STORMTROOPERHAN01":22.54], g13: 2.01, r5: 1.12),
"SUNFAC" : CharacterDeomographics(id: "SUNFAC", viable: 69.06, zetas: [:], g13: 18.03, r5: 4.59),
"SUPREMELEADERKYLOREN" : CharacterDeomographics(id: "SUPREMELEADERKYLOREN", viable: 17.06, zetas: ["specialskill_SUPREMELEADERKYLOREN02":100.0, "uniqueskill_GALACTICLEGEND01":100.0, "specialskill_SUPREMELEADERKYLOREN01":100.0, "basicskill_SUPREMELEADERKYLOREN":100.0, "uniqueskill_SUPREMELEADERKYLOREN01":99.12, "leaderskill_SUPREMELEADERKYLOREN":100.0], g13: 96.9, r5: 96.02),
"T3_M4" : CharacterDeomographics(id: "T3_M4", viable: 28.15, zetas: ["uniqueskill_t3_m4_02":38.87, "uniqueskill_t3_m4_01":42.9], g13: 5.36, r5: 0.8),
"TIEFIGHTERPILOT" : CharacterDeomographics(id: "TIEFIGHTERPILOT", viable: 34.72, zetas: [:], g13: 2.83, r5: 0.87),
"TALIA" : CharacterDeomographics(id: "TALIA", viable: 40.23, zetas: [:], g13: 4.5, r5: 1.88),
"BADBATCHTECH" : CharacterDeomographics(id: "BADBATCHTECH", viable: 2.72, zetas: ["uniqueskill_BADBATCHTECH01":100.0], g13: 16.67, r5: 13.89),
"TEEBO" : CharacterDeomographics(id: "TEEBO", viable: 18.26, zetas: [:], g13: 3.72, r5: 0.83),
"ARMORER" : CharacterDeomographics(id: "ARMORER", viable: 2.79, zetas: ["specialskill_ARMORER01":100.0], g13: 29.73, r5: 18.92),
"THEMANDALORIAN" : CharacterDeomographics(id: "THEMANDALORIAN", viable: 32.38, zetas: ["uniqueskill_THEMANDALORIAN01":100.0, "leaderskill_THEMANDALORIAN":72.96], g13: 22.38, r5: 8.86),
"THEMANDALORIANBESKARARMOR" : CharacterDeomographics(id: "THEMANDALORIANBESKARARMOR", viable: 7.09, zetas: ["leaderskill_THEMANDALORIANBESKARARMOR":100.0, "uniqueskill_THEMANDALORIANBESKARARMOR01":100.0], g13: 43.62, r5: 21.28),
"C3POCHEWBACCA" : CharacterDeomographics(id: "C3POCHEWBACCA", viable: 29.58, zetas: ["specialskill_C3POCHEWBACCA02":100.0], g13: 63.27, r5: 56.38),
"TUSKENRAIDER" : CharacterDeomographics(id: "TUSKENRAIDER", viable: 0.68, zetas: [:], g13: 33.33, r5: 22.22),
"TUSKENSHAMAN" : CharacterDeomographics(id: "TUSKENSHAMAN", viable: 3.92, zetas: [:], g13: 0.0, r5: 0.0),
"URORRURRR" : CharacterDeomographics(id: "URORRURRR", viable: 2.11, zetas: [:], g13: 0.0, r5: 0.0),
"UGNAUGHT" : CharacterDeomographics(id: "UGNAUGHT", viable: 1.66, zetas: [:], g13: 0.0, r5: 0.0),
"YOUNGCHEWBACCA" : CharacterDeomographics(id: "YOUNGCHEWBACCA", viable: 6.72, zetas: ["uniqueskill_YOUNGCHEWBACCA02":100.0], g13: 5.62, r5: 3.37),
"SMUGGLERCHEWBACCA" : CharacterDeomographics(id: "SMUGGLERCHEWBACCA", viable: 13.28, zetas: ["uniqueskill_SMUGGLERCHEWBACCA01":46.02], g13: 60.23, r5: 2.84),
"SMUGGLERHAN" : CharacterDeomographics(id: "SMUGGLERHAN", viable: 28.38, zetas: ["uniqueskill_SMUGGLERHAN01":38.83], g13: 73.4, r5: 1.33),
"VISASMARR" : CharacterDeomographics(id: "VISASMARR", viable: 8.45, zetas: ["uniqueskill_VISASMARR01":70.54], g13: 2.68, r5: 0.0),
"WAMPA" : CharacterDeomographics(id: "WAMPA", viable: 48.45, zetas: ["specialskill_WAMPA02":81.78, "uniqueskill_WAMPA02":97.2], g13: 86.14, r5: 26.17),
"WATTAMBOR" : CharacterDeomographics(id: "WATTAMBOR", viable: 44.6, zetas: ["uniqueskill_WATTAMBOR02":100.0], g13: 38.75, r5: 28.43),
"WEDGEANTILLES" : CharacterDeomographics(id: "WEDGEANTILLES", viable: 43.17, zetas: [:], g13: 41.43, r5: 0.52),
"WICKET" : CharacterDeomographics(id: "WICKET", viable: 65.89, zetas: ["uniqueskill_WICKET01":55.21], g13: 1.83, r5: 0.34),
"BADBATCHWRECKER" : CharacterDeomographics(id: "BADBATCHWRECKER", viable: 0.98, zetas: ["uniqueskill_BADBATCHWRECKER01":100.0], g13: 46.15, r5: 38.46),
"YOUNGHAN" : CharacterDeomographics(id: "YOUNGHAN", viable: 5.74, zetas: ["uniqueskill_YOUNGHAN01":68.42], g13: 3.95, r5: 2.63),
"YOUNGLANDO" : CharacterDeomographics(id: "YOUNGLANDO", viable: 8.83, zetas: ["uniqueskill_YOUNGLANDO_01":21.37], g13: 2.56, r5: 1.71),
"ZAALBAR" : CharacterDeomographics(id: "ZAALBAR", viable: 65.66, zetas: ["uniqueskill_ZAALBAR02":85.52], g13: 3.91, r5: 0.69),
"ZAMWESELL" : CharacterDeomographics(id: "ZAMWESELL", viable: 18.19, zetas: ["uniqueskill_ZAMWESELL01":50.62], g13: 4.98, r5: 1.24),
]