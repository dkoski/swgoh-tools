// see zetas.py
extension ZetaPriority {
static let data = [

            ZetaPriority(character: #"Ahsoka Tano (Fulcrum)"#, type: "Special", name: "Whirlwind", priorities: [6.43, 5.14, 5.54, 4.57, 5.29]),
        

            ZetaPriority(character: #"Amilyn Holdo"#, type: "Unique", name: "Quiet Confidence", priorities: [7.33, 6.1, 7.25, 5.4, 6.1]),
        

            ZetaPriority(character: #"Asajj Ventress"#, type: "Leader", name: "Nightsister Swiftness", priorities: [2.15, 2.54, 2.75, 2.15, 2.62]),
        

            ZetaPriority(character: #"Asajj Ventress"#, type: "Unique", name: "Rampage", priorities: [2.08, 3.15, 2.58, 2.77, 3.92]),
        

            ZetaPriority(character: #"Barriss Offee"#, type: "Unique", name: "Swift Recovery", priorities: [5.6, 5.64, 4.8, 3, 5.27]),
        

            ZetaPriority(character: #"BB-8"#, type: "Unique", name: "Roll with the Punches", priorities: [2.69, 2.54, 3.08, 2.62, 3.92]),
        

            ZetaPriority(character: #"BB-8"#, type: "Unique", name: "Self-Preservation Protocol", priorities: [3.55, 5.22, 4.89, 4.7, 6.3]),
        

            ZetaPriority(character: #"Boba Fett"#, type: "Unique", name: "Bounty Hunter's Resolve", priorities: [7.1, 7.27, 8, 6.09, 5.2]),
        

            ZetaPriority(character: #"Bodhi Rook"#, type: "Unique", name: "Double Duty", priorities: [9.88, 9.86, 9.71, 9.75, 8.88]),
        

            ZetaPriority(character: #"Bossk"#, type: "Leader", name: "On The Hunt", priorities: [3.9, 5.91, 4.7, 2.82, 2.55]),
        

            ZetaPriority(character: #"Bossk"#, type: "Unique", name: "Trandoshan Rage", priorities: [5.22, 6.09, 6, 4, 4.1]),
        

            ZetaPriority(character: #"Captain Han Solo"#, type: "Unique", name: "Nick of Time", priorities: [9.13, 7.88, 9.11, 7.56, 7.78]),
        

            ZetaPriority(character: #"Captain Phasma"#, type: "Leader", name: "Fire at Will", priorities: [7.8, 8.3, 8.44, 6, 6.7]),
        

            ZetaPriority(character: #"Cassian Andor"#, type: "Unique", name: "Groundwork", priorities: [9.25, 7.78, 8.78, 7.4, 6.5]),
        

            ZetaPriority(character: #"CC-2224 "Cody""#, type: "Leader", name: "Ghost Company Commander", priorities: [8.45, 8.64, 7.1, 7.09, 6.73]),
        

            ZetaPriority(character: #"Chief Chirpa"#, type: "Leader", name: "Simple Tactics", priorities: [3.9, 5.18, 4.1, 2.91, 4]),
        

            ZetaPriority(character: #"Clone Wars Chewbacca"#, type: "Special", name: "Defiant Roar", priorities: [9.9, 9.8, 9.78, 9.7, 9.7]),
        

            ZetaPriority(character: #"Colonel Starck"#, type: "Unique", name: "Imperial Intelligence", priorities: [7.22, 6.2, 5.22, 5, 4.4]),
        

            ZetaPriority(character: #"Commander Luke Skywalker"#, type: "Leader", name: "Rebel Maneuvers", priorities: [6.67, 4.2, 4.67, 3.5, 4.4]),
        

            ZetaPriority(character: #"Commander Luke Skywalker"#, type: "Unique", name: "Learn Control", priorities: [6.33, 4.1, 4.67, 3.7, 4.6]),
        

            ZetaPriority(character: #"Commander Luke Skywalker"#, type: "Unique", name: "It Binds All Things", priorities: [5.18, 2.5, 2.36, 2.42, 2.33]),
        

            ZetaPriority(character: #"Count Dooku"#, type: "Unique", name: "Flawless Riposte", priorities: [8, 7.7, 8.11, 6.8, 7.78]),
        

            ZetaPriority(character: #"CT-5555 "Fives""#, type: "Unique", name: "Tactical Awareness", priorities: [8.11, 9.11, 8.38, 7.78, 8.33]),
        

            ZetaPriority(character: #"Darth Maul"#, type: "Leader", name: "Dancing Shadows", priorities: [7, 4.7, 6, 3.67, 4.78]),
        

            ZetaPriority(character: #"Darth Nihilus"#, type: "Leader", name: "Strength of the Void", priorities: [8.22, 8.38, 8.63, 8.11, 6.78]),
        

            ZetaPriority(character: #"Darth Nihilus"#, type: "Unique", name: "Lord of Hunger", priorities: [6.7, 3.91, 5.2, 4.27, 6.36]),
        

            ZetaPriority(character: #"Darth Sidious"#, type: "Unique", name: "Sadistic Glee", priorities: [7.56, 8, 7.88, 6.25, 7]),
        

            ZetaPriority(character: #"Darth Sion"#, type: "Unique", name: "Lord of Pain", priorities: [5.45, 2.36, 3.9, 2.45, 4]),
        

            ZetaPriority(character: #"Darth Traya"#, type: "Leader", name: "Compassion is Weakness", priorities: [3.9, 1.91, 2.67, 2, 3]),
        

            ZetaPriority(character: #"Darth Traya"#, type: "Unique", name: "Lord of Betrayal", priorities: [4.22, 2.09, 2.33, 1.82, 3.09]),
        

            ZetaPriority(character: #"Darth Vader"#, type: "Leader", name: "Inspiring Through Fear", priorities: [5.75, 6.25, 5.86, 6.43, 5]),
        

            ZetaPriority(character: #"Darth Vader"#, type: "Unique", name: "No Escape", priorities: [7.86, 5.5, 7.29, 5.13, 6.57]),
        

            ZetaPriority(character: #"Death Trooper"#, type: "Unique", name: "Krennic's Guard", priorities: [9.13, 8.43, 9, 5.86, 7.43]),
        

            ZetaPriority(character: #"Dengar"#, type: "Unique", name: "Grizzled Veteran", priorities: [9, 8.75, 8.86, 8.13, 7.38]),
        

            ZetaPriority(character: #"Director Krennic"#, type: "Leader", name: "Director of Advanced Weapons Research", priorities: [9.29, 9.17, 9.67, 8.83, 8.67]),
        

            ZetaPriority(character: #"Emperor Palpatine"#, type: "Leader", name: "Emperor of the Galactic Empire", priorities: [5.11, 2.5, 3.67, 2.3, 2.6]),
        

            ZetaPriority(character: #"Emperor Palpatine"#, type: "Unique", name: "Crackling Doom", priorities: [6.14, 3.25, 5.29, 2.88, 5.13]),
        

            ZetaPriority(character: #"Ezra Bridger"#, type: "Special", name: "Flourish", priorities: [5.11, 5.22, 5.38, 5.22, 6.11]),
        

            ZetaPriority(character: #"Finn"#, type: "Leader", name: "Balanced Tactics", priorities: [7.22, 5.3, 3.89, 2.8, 2.5]),
        

            ZetaPriority(character: #"First Order Stormtrooper"#, type: "Unique", name: "Return Fire", priorities: [6.86, 6.63, 6.57, 3.75, 6.13]),
        

            ZetaPriority(character: #"First Order TIE Pilot"#, type: "Unique", name: "Keen Eye", priorities: [6.67, 7.5, 6.8, 5.17, 6.33]),
        

            ZetaPriority(character: #"Gar Saxon"#, type: "Leader", name: "Mandalorian Retaliation", priorities: [9, 9.43, 9.67, 8.83, 8.67]),
        

            ZetaPriority(character: #"Garazeb "Zeb" Orrelios"#, type: "Special", name: "Staggering Sweep", priorities: [6.4, 8.83, 8, 6.4, 6.33]),
        

            ZetaPriority(character: #"General Veers"#, type: "Unique", name: "Aggressive Tactician", priorities: [4.38, 5.56, 3.38, 2.78, 1.44]),
        

            ZetaPriority(character: #"Grand Admiral Thrawn"#, type: "Leader", name: "Legendary Strategist", priorities: [5.17, 6.33, 4.83, 3.71, 3.86]),
        

            ZetaPriority(character: #"Grand Admiral Thrawn"#, type: "Unique", name: "Ebb and Flow", priorities: [4, 4.63, 4, 4.25, 5.13]),
        

            ZetaPriority(character: #"Grand Master Yoda"#, type: "Special", name: "Battle Meditation", priorities: [3, 3.63, 3.29, 3.38, 4.25]),
        

            ZetaPriority(character: #"Grand Master Yoda"#, type: "Leader", name: "Grand Master's Guidance", priorities: [4.29, 5.43, 4.83, 5.29, 4.43]),
        

            ZetaPriority(character: #"Grand Moff Tarkin"#, type: "Leader", name: "Tighten the Grip", priorities: [9, 8.17, 9, 8.17, 7.67]),
        

            ZetaPriority(character: #"Grand Moff Tarkin"#, type: "Unique", name: "Callous Conviction", priorities: [8, 7.5, 8, 6.83, 6]),
        

            ZetaPriority(character: #"Greedo"#, type: "Special", name: "Threaten", priorities: [9.29, 9.43, 9.33, 8.14, 7.86]),
        

            ZetaPriority(character: #"Han Solo"#, type: "Unique", name: "Shoots First", priorities: [1.78, 1.11, 1.25, 1.33, 1.56]),
        

            ZetaPriority(character: #"Hera Syndulla"#, type: "Special", name: "Play to Strengths", priorities: [5.67, 8.29, 7.6, 6.67, 6]),
        

            ZetaPriority(character: #"Hermit Yoda"#, type: "Special", name: "Strength Flows From the Force", priorities: [2.22, 3.89, 3.5, 3.33, 4.67]),
        

            ZetaPriority(character: #"Hermit Yoda"#, type: "Unique", name: "Do or Do Not", priorities: [5.44, 5.33, 6.38, 4.89, 6.44]),
        

            ZetaPriority(character: #"IG-88"#, type: "Unique", name: "Adaptive Aim Algorithm", priorities: [8.38, 8.63, 8.33, 7.29, 6.71]),
        

            ZetaPriority(character: #"Imperial Probe Droid"#, type: "Unique", name: "Imperial Logistics", priorities: [8.33, 5.71, 7.5, 5.57, 5]),
        

            ZetaPriority(character: #"Jyn Erso"#, type: "Leader", name: "Into the Fray", priorities: [8.25, 8.13, 7, 5.29, 3.25]),
        

            ZetaPriority(character: #"Jyn Erso"#, type: "Unique", name: "Fierce Determination", priorities: [9, 8.25, 7.86, 7.63, 7]),
        

            ZetaPriority(character: #"K-2SO"#, type: "Unique", name: "Reprogrammed Imperial Droid", priorities: [9.33, 8.89, 8.71, 8, 8.13]),
        

            ZetaPriority(character: #"Kanan Jarrus"#, type: "Special", name: "Total Defense", priorities: [5, 6.78, 5.14, 3.89, 4.78]),
        

            ZetaPriority(character: #"Kylo Ren"#, type: "Special", name: "Outrage", priorities: [6.25, 6, 5.13, 4.33, 4.67]),
        

            ZetaPriority(character: #"Kylo Ren (Unmasked)"#, type: "Leader", name: "Merciless Pursuit", priorities: [3.78, 4.33, 3, 1.44, 2.44]),
        

            ZetaPriority(character: #"Kylo Ren (Unmasked)"#, type: "Unique", name: "Scarred", priorities: [6.71, 3.38, 4.29, 2, 5.63]),
        

            ZetaPriority(character: #"Logray"#, type: "Unique", name: "Shaman's Insight", priorities: [6.57, 7.29, 6.17, 6.63, 6.86]),
        

            ZetaPriority(character: #"Luke Skywalker (Farmboy)"#, type: "Leader", name: "A New Hope", priorities: [9.63, 9.75, 10, 9.75, 9.43]),
        

            ZetaPriority(character: #"Luke Skywalker (Farmboy)"#, type: "Unique", name: "Draw a Bead", priorities: [9.38, 9.75, 9.43, 8.75, 8.29]),
        

            ZetaPriority(character: #"Luminara Unduli"#, type: "Leader", name: "Elegant Steps", priorities: [9.63, 9.63, 10, 9.63, 9.63]),
        

            ZetaPriority(character: #"Mother Talzin"#, type: "Leader", name: "The Great Mother", priorities: [4.67, 2.56, 3, 1.78, 2.44]),
        

            ZetaPriority(character: #"Mother Talzin"#, type: "Unique", name: "Plaguebearer", priorities: [3.56, 2.78, 3, 2.56, 3.33]),
        

            ZetaPriority(character: #"Nightsister Initiate"#, type: "Unique", name: "Nightsister Retaliation", priorities: [8, 9.86, 9.67, 9.71, 9.86]),
        

            ZetaPriority(character: #"Obi-Wan Kenobi (Old Ben)"#, type: "Special", name: "Devoted Protector", priorities: [6.75, 3.13, 4.83, 2.88, 5.25]),
        

            ZetaPriority(character: #"Obi-Wan Kenobi (Old Ben)"#, type: "Unique", name: "If You Strike Me Down", priorities: [7.29, 5.83, 7, 5.33, 7.17]),
        

            ZetaPriority(character: #"Old Daka"#, type: "Unique", name: "Serve Again", priorities: [4.22, 4.56, 5.38, 4.78, 5.56]),
        

            ZetaPriority(character: #"Pao"#, type: "Unique", name: "For Pipada", priorities: [9.71, 9.71, 10, 9.71, 9.5]),
        

            ZetaPriority(character: #"Paploo"#, type: "Unique", name: "Don't Hold Back", priorities: [6.14, 6.43, 5.67, 5.63, 5.71]),
        

            ZetaPriority(character: #"Princess Leia"#, type: "Unique", name: "Against All Odds", priorities: [7.5, 8.44, 7.71, 7.5, 7.5]),
        

            ZetaPriority(character: #"Qui-Gon Jinn"#, type: "Leader", name: "Agility Training", priorities: [8.14, 7.71, 6.5, 4.71, 5.43]),
        

            ZetaPriority(character: #"R2-D2"#, type: "Unique", name: "Combat Analysis", priorities: [2.78, 2, 2.75, 2, 4.56]),
        

            ZetaPriority(character: #"R2-D2"#, type: "Unique", name: "Number Crunch", priorities: [1.56, 1.56, 1.5, 1.44, 2.44]),
        

            ZetaPriority(character: #"Rebel Officer Leia Organa"#, type: "Unique", name: "Dauntless", priorities: [7.83, 9, 9.4, 8.5, 8.33]),
        

            ZetaPriority(character: #"Rey (Jedi Training)"#, type: "Leader", name: "Inspirational Presence", priorities: [1, 1.22, 1, 1.11, 1.56]),
        

            ZetaPriority(character: #"Rey (Jedi Training)"#, type: "Unique", name: "Virtuous Protector", priorities: [5.33, 4.33, 6, 4.56, 6.56]),
        

            ZetaPriority(character: #"Rey (Jedi Training)"#, type: "Unique", name: "Insight", priorities: [2, 2.56, 2.75, 2.89, 3.78]),
        

            ZetaPriority(character: #"Rey (Scavenger)"#, type: "Unique", name: "Focused Strikes", priorities: [7.43, 8, 8.17, 7.29, 7.29]),
        

            ZetaPriority(character: #"Rose Tico"#, type: "Unique", name: "Valiant Spirit", priorities: [9.13, 8.88, 9.29, 8.38, 9.13]),
        

            ZetaPriority(character: #"Sabine Wren"#, type: "Special", name: "Demolish", priorities: [5.38, 7, 5.29, 3.63, 5.22]),
        

            ZetaPriority(character: #"Savage Opress"#, type: "Unique", name: "Brute", priorities: [8.33, 7.9, 7.22, 6.3, 6.88]),
        

            ZetaPriority(character: #"Stormtrooper"#, type: "Unique", name: "Wall of Stormtroopers", priorities: [9.75, 9.5, 9.86, 9.38, 8.75]),
        

            ZetaPriority(character: #"Stormtrooper Han"#, type: "Unique", name: "Bluff", priorities: [7.44, 9, 8.5, 7.67, 7.75]),
        

            ZetaPriority(character: #"Vandor Chewbacca"#, type: "Unique", name: "Ferocious Protector", priorities: [4.67, 5.57, 5, 3.29, 5.43]),
        

            ZetaPriority(character: #"Veteran Smuggler Chewbacca"#, type: "Unique", name: "Let the Wookiee Win", priorities: [9.63, 9.5, 9.57, 9.25, 8.86]),
        

            ZetaPriority(character: #"Veteran Smuggler Han Solo"#, type: "Unique", name: "Swindle", priorities: [9.63, 9.75, 9.71, 9.38, 9.14]),
        

            ZetaPriority(character: #"Visas Marr"#, type: "Unique", name: "Returned to the Light", priorities: [2.56, 6.3, 6.33, 5.1, 7.33]),
        

            ZetaPriority(character: #"Wampa"#, type: "Special", name: "Furious Foe", priorities: [8.5, 5.78, 7.5, 5.11, 6.13]),
        

            ZetaPriority(character: #"Wampa"#, type: "Unique", name: "Cornered Beast", priorities: [4.88, 4.78, 4.86, 4.78, 4.25]),
        

            ZetaPriority(character: #"Wicket"#, type: "Unique", name: "Furtive Tactics", priorities: [4.86, 6.5, 5.57, 5, 5.29]),
        

            ZetaPriority(character: #"Young Han Solo"#, type: "Unique", name: "Ready For Anything", priorities: [2.83, 5.33, 5.2, 4, 4]),
        

            ZetaPriority(character: #"Qi'Ra"#, type: "Unique", name: "Insult to Injury", priorities: [3.75, 7.25, 4.5, 4.75, 5.25]),
        

            ZetaPriority(character: #"Enfys Nest"#, type: "Unique", name: "Fighting Instinct", priorities: [6.4, 2, 3.5, 2.17, 3.5]),
        

            ZetaPriority(character: #"Young Lando Calrissian"#, type: "Unique", name: "Perfect Timing", priorities: [4.75, 5.25, 4.75, 5, 5.75]),
        

            ZetaPriority(character: #"L3-37"#, type: "Unique", name: "For the Droids", priorities: [8, 6.4, 7.2, 5.8, 6.8]),
        

            ZetaPriority(character: #"Bastila Shan"#, type: "Leader", name: "Initiative", priorities: [4.43, 2.71, 4.14, 2, 4.71]),
        

            ZetaPriority(character: #"Jolee Bindo"#, type: "Special", name: "That Looks Pretty Bad", priorities: [4.33, 2.5, 4.33, 2.67, 4]),
        

            ZetaPriority(character: #"T3-M4"#, type: "Unique", name: "Combat Logic Upgrade", priorities: [7.2, 7.4, 7.2, 5.6, 7]),
        

            ZetaPriority(character: #"T3-M4"#, type: "Unique", name: "Master Gearhead", priorities: [6.2, 7.4, 7, 6.2, 7.2]),
        

            ZetaPriority(character: #"Zaalbar"#, type: "Unique", name: "Mission's Guardian", priorities: [6, 7.43, 6.33, 5, 6.83]),
        

            ZetaPriority(character: #"Mission Vao"#, type: "Unique", name: "Me and Big Z Forever", priorities: [4.33, 6.83, 5.33, 4, 5.6]),
        

            ZetaPriority(character: #"Embo"#, type: "Leader", name: "The Quiet Assassin", priorities: [4.33, 7.67, 7.67, 6.67, 8]),
        

            ZetaPriority(character: #"Embo"#, type: "Unique", name: "Way of the Kyuzo", priorities: [7.25, 7.75, 8, 5.5, 7.5]),
        

            ZetaPriority(character: #"Aurra Sing"#, type: "Special", name: "Snipers Expertise", priorities: [4.33, 9, 8, 9, 8.67]),
        

            ZetaPriority(character: #"Aurra Sing"#, type: "Leader", name: "Game Plan", priorities: [4.67, 8.33, 7.33, 7, 7.33]),
        

            ZetaPriority(character: #"Jango Fett"#, type: "Leader", name: "Anything To Get Ahead", priorities: [8.5, 6, 6.67, 4, 5.67]),
        

            ZetaPriority(character: #"Jango Fett"#, type: "Unique", name: "Notorious Reputation", priorities: [9.5, 6, 8, 4, 8]),
        

            ZetaPriority(character: #"Chewbacca"#, type: "Unique", name: "Loyal Friend", priorities: [7, 2.25, 4.75, 2.25, 4.75]),
        

            ZetaPriority(character: #"Chewbacca"#, type: "Unique", name: "Raging Wookiee", priorities: [8.67, 3.75, 7, 4.5, 8]),
        

            ZetaPriority(character: #"Jedi Knight Revan"#, type: "Special", name: "Direct Focus", priorities: [6.75, 2.5, 3.75, 2, 6.67]),
        

            ZetaPriority(character: #"Jedi Knight Revan"#, type: "Leader", name: "General of the Old Republic", priorities: [1.25, 1, 2.75, 1, 3.75]),
        

            ZetaPriority(character: #"Jedi Knight Revan"#, type: "Unique", name: "Hero", priorities: [3, 1.75, 3.5, 1.75, 6]),
        

            ZetaPriority(character: #"Bastila Shan (Fallen)"#, type: "Unique", name: "Sith Apprentice", priorities: [8.67, 7, 8.33, 6.33, 8.67]),
        

            ZetaPriority(character: #"C-3PO"#, type: "Special", name: "Oh My Goodness!", priorities: [4.5, 3, 4, 3, 5]),
        

            ZetaPriority(character: #"C-3PO"#, type: "Unique", name: "Wait for Me!", priorities: [5.5, 4, 4.67, 3.67, 6]),
        

            ZetaPriority(character: #"Canderous Ordo"#, type: "Unique", name: "I Like a Challenge", priorities: [7, 7.5, 7.5, 4, 8]),
        

            ZetaPriority(character: #"Carth Onasi"#, type: "Leader", name: "Soldier of the Old Republic", priorities: [6.5, 5.67, 5, 3, 5.5]),
        

            ZetaPriority(character: #"Juhani"#, type: "Unique", name: "Cathar Resilience", priorities: [7, 5, 5.5, 4, 6]),
        

            ZetaPriority(character: #"Darth Revan"#, type: "Leader", name: "Lord of the Sith", priorities: [4.67, 1.33, 2, 1.33, 4.67]),
        

            ZetaPriority(character: #"Darth Revan"#, type: "Unique", name: "Conqueror", priorities: [5.67, 2, 3, 1.67, 4.33]),
        

            ZetaPriority(character: #"Darth Revan"#, type: "Unique", name: "Villain", priorities: [4.67, 2, 2.67, 2, 4.33]),
        

            ZetaPriority(character: #"Darth Malak"#, type: "Unique", name: "Gnawing Terror", priorities: [5, 2.33, 2.33, 2, 4.67]),
        

            ZetaPriority(character: #"Darth Malak"#, type: "Unique", name: "Jaws of Life", priorities: [4.67, 1.33, 2, 1.33, 4.33]),
        

            ZetaPriority(character: #"HK-47"#, type: "Unique", name: "Self-Reconstruction", priorities: [4.67, 4, 5.33, 5, 6]),
        

            ZetaPriority(character: #"HK-47"#, type: "Unique", name: "Loyalty to the Maker", priorities: [7.33, 3, 4.33, 3.33, 5.67]),
        

            ZetaPriority(character: #"General Kenobi"#, type: "Unique", name: "Soresu", priorities: [7, 3.5, 4.5, 3, 6.5]),
        

            ZetaPriority(character: #"Ahsoka Tano"#, type: "Unique", name: "Daring Padawan", priorities: [5, 2.67, 3, 2.33, 4.67]),
        

            ZetaPriority(character: #"Padmé Amidala"#, type: "Leader", name: "Unwavering Courage", priorities: [5.33, 1.67, 1.67, 1.33, 2.67]),
        

            ZetaPriority(character: #"Padmé Amidala"#, type: "Unique", name: "Always a Choice", priorities: [4.67, 3, 3.33, 2.67, 5]),
        

            ZetaPriority(character: #"Jedi Knight Anakin"#, type: "Unique", name: "Righteous Fury", priorities: [5.5, 3, 4.5, 3, 6]),
        

            ZetaPriority(character: #"IG-100 MagnaGuard"#, type: "Special", name: "Stunning Strike", priorities: [10, 6, 7, 5, 5]),
        

            ZetaPriority(character: #"General Grievous"#, type: "Leader", name: "Daunting Presence", priorities: [10, 3, 3, 3, 3]),
        

            ZetaPriority(character: #"General Grievous"#, type: "Unique", name: "Metalloid Monstrosity", priorities: [10, 3, 3, 3, 3]),
        

            ZetaPriority(character: #"Droideka"#, type: "Unique", name: "Deflector Shield Generator", priorities: [10, 6, 7, 5, 5]),
        

            ZetaPriority(character: #"B2 Super Battle Droid"#, type: "Unique", name: "Reactive Protocol", priorities: [10, 3, 3, 3, 3]),
        

            ZetaPriority(character: #"B1 Battle Droid"#, type: "Unique", name: "Droid Battalion", priorities: [10, 3, 3, 3, 3]),
        

            ZetaPriority(character: #"Shaak Ti"#, type: "Leader", name: "Unity Wins War", priorities: [4.5, 3.5, 4.5, 3.5, 3.5]),
        

            ZetaPriority(character: #"Shaak Ti"#, type: "Unique", name: "Heightened Reflexes", priorities: [4.5, 4, 4.5, 4, 4]),
        

            ZetaPriority(character: #"CT-5555 "Fives""#, type: "Unique", name: "Domino Squad", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"CT-7567 "Rex""#, type: "Unique", name: "Captain of the 501st", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"ARC Trooper"#, type: "Unique", name: "ARC Arsenal", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"CT-21-0408 "Echo""#, type: "Unique", name: "Domino Squad", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"Wat Tambor"#, type: "Unique", name: "Mass Manufacture", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"Ki-Adi-Mundi"#, type: "Unique", name: "Jedi Council", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"General Skywalker"#, type: "Basic", name: "Furious Slash", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"General Skywalker"#, type: "Leader", name: "General of the 501st", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"General Skywalker"#, type: "Unique", name: "The Chosen One", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"General Skywalker"#, type: "Unique", name: "Hero with no Fear", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"Geonosian Brood Alpha"#, type: "Leader", name: "Geonosian Swarm", priorities: [0, 0, 0, 0, 0]),
        

            ZetaPriority(character: #"Geonosian Brood Alpha"#, type: "Unique", name: "Queen's Will", priorities: [0, 0, 0, 0, 0]),
        
]
}
extension Model.Ability {
static let zetaDescriptions = [

            "Whirlwind" : #"Cooldown - 1, the target can't Evade and has -50% Armor against this attack"#,
        

            "Quiet Confidence" : #"When another Resistance ally loses Foresight, Amilyn Holdo Recovers 10% Health. While Amilyn Holdo doesn't have Taunt, all other Resistance allies have +20% Critical Avoidance."#,
        

            "Nightsister Swiftness" : #"+25% Turn Meter Reduction Chance, +10% Turn Meter Reduction, Nightsisters gain 50% Turn Meter when they fall below 100% Health"#,
        

            "Rampage" : #"+75% Turn Meter Gain Chance, +5% Offense, +5% Critical Chance, and when any ally or enemy is defeated Asajj gains 5% Max Health until the end of the encounter"#,
        

            "Swift Recovery" : #"Whenever an ally is Critically Hit they recover 20% of their Max Health"#,
        

            "Roll with the Punches" : #"When BB-8 attacks out of turn, he calls a random Resistance ally to Assist"#,
        

            "Self-Preservation Protocol" : #"When BB-8 Evades, Droid allies recover 8% Health and 8% Protection"#,
        

            "Bounty Hunter's Resolve" : #"Recover 100% Protection on a finishing blow"#,
        

            "Double Duty" : #"At the end of each of his turns, Bodhi grants Offense Up for 2 turns to a random ally who doesn't have it"#,
        

            "On The Hunt" : #"Whenever an enemy suffers a debuff or Resists, all Bounty Hunter allies recover 5% Health and Protection"#,
        

            "Trandoshan Rage" : #"When Bossk loses Taunt, he gains Taunt for 1 turn if the weakest enemy has Protection"#,
        

            "Nick of Time" : #"Whenever Han uses his Basic attack he gains +10% Max Health (stacking) for 3 turns"#,
        

            "Fire at Will" : #"-15% Damage Penalty, triple Assist chance for First Order, and First Order allies gain Advantage for 2 turns at the start of each encounter, can't be Critically Hit while they have Advantage, and gain 20% Potency"#,
        

            "Groundwork" : #"At the start of each encounter all Rebel allies gain Protection Up (20%) for 3 turns"#,
        

            "Ghost Company Commander" : #"+5% Critical Chance, +10% Defense, and Clone allies recover 5% of their Max Protection whenever they use a Basic ability"#,
        

            "Simple Tactics" : #"Whenever an Ewok ally uses a Special ability, 60% chance to call another random Ewok ally to Assist"#,
        

            "Defiant Roar" : #"+25% Turn Meter Gain Chance, +10% Heal, and dispel all debuffs from self"#,
        

            "Imperial Intelligence" : #"At the end of his turn, Starck recovers 3% Protection, and removes 1% Turn Meter from all enemies for each active Empire ally (doubled for Imperial Trooper allies)"#,
        

            "Rebel Maneuvers" : #"Whenever an enemy Resists a detrimental effect, Rebel allies gain 5% Turn Meter"#,
        

            "Learn Control" : #"While Luke doesn't have Call to Action he has +50% Critical Avoidance and +100% Tenacity"#,
        

            "It Binds All Things" : #"Whenever Luke Resists a detrimental effect he recovers 5% Protection and whenever Luke inflicts a debuff he gains 10% Turn Meter and other allies gain half that amount"#,
        

            "Flawless Riposte" : #"+15% Counter Damage, recover 10% Protection and gain Critical Hit Immunity for 1 turn on counterattack"#,
        

            "Tactical Awareness" : #"If Fives is defeated this way, 501st Clone Trooper allies gain his Max Health, Max Protection, Speed, and Offense stats in addition to their own until the end of battle"#,
        

            "Dancing Shadows" : #"Sith allies gain Stealth for 1 turn on Evade, can't be Critically Hit while Stealthed, gain Advantage for 2 turns whenever Stealth expires, and gain all "on Evade" bonuses at the start of each encounter"#,
        

            "Strength of the Void" : #"+50% Health Steal"#,
        

            "Lord of Hunger" : #"At the start of each enemy turn, Nihilus inflicts Health Down on them for 2 turns"#,
        

            "Sadistic Glee" : #"100% Turn Meter Gain Chance, +50% Potency, and gain Max Health equal to Potency percentage"#,
        

            "Lord of Pain" : #"At the end of each turn, Sion inflicts Pain for 3 turns on enemies who damaged him that turn"#,
        

            "Compassion is Weakness" : #"When an enemy uses an ability outside of their turn, they take damage equal to 35% of their Max Health. This damage can't defeat enemies. When an enemy gains a buff outside of their turn, they lose 50% Offense until the end of their next turn"#,
        

            "Lord of Betrayal" : #"At the start of each Sith ally's turn, Traya dispels all debuffs on them and deals damage equal to 5% of their Max Health for each debuff dispelled"#,
        

            "Inspiring Through Fear" : #"+25% Turn Meter Reduction Chance, +10% Turn Meter Reduction, enemies immediately regain Damage Over Time for 2 turns whenever Damage Over Time expires on them while Darth Vader is alive"#,
        

            "No Escape" : #"Darth Vader is immune to Turn Meter Reduction and recovers 5% Health when a Damage Over Time effect on an enemy expires"#,
        

            "Krennic's Guard" : #"+5% Heal, +50% Turn Meter Gain, and Director Krennic can't be Critically Hit while Death Trooper is alive"#,
        

            "Grizzled Veteran" : #"Add 3% Health and Protection when this ability's effects trigger"#,
        

            "Director of Advanced Weapons Research" : #"Empire allies recover 10% Protection whenever they score a Critical Hit"#,
        

            "Emperor of the Galactic Empire" : #"When a debuff on an enemy expires, Empire and Sith allies gain 5% Turn Meter"#,
        

            "Crackling Doom" : #"Palpatine has +50% Max Protection, and, when he inflicts Shock, he gains 5% Max Protection until the end of the encounter"#,
        

            "Flourish" : #"+50% Damage, +25% Turn Meter Gain, and gain Defense Up for 2 turns"#,
        

            "Balanced Tactics" : #"Whenever a Resistance ally loses Foresight, they gain Advantage for 2 turns and whenever an enemy takes damage from Expose, reduce the cooldowns of all Resistance allies by 1 and grant them 35% Turn Meter"#,
        

            "Return Fire" : #"Whenever First Order Stormtrooper uses any Ability he has a 50% chance to call a random ally to Assist, dealing 50% damage unless they are First Order, then grants them Advantage for 2 turns if they were First Order"#,
        

            "Keen Eye" : #"15% Critical Chance, +10% Critical Damage, and First Order TIE Pilot has a 70% chance to gain Foresight for 2 turns whenever he loses Advantage"#,
        

            "Mandalorian Retaliation" : #"Whenever an Empire ally uses a Basic attack, they recover 5% Health"#,
        

            "Staggering Sweep" : #"Cooldown - 1, this attack can't be Evaded, and Zeb gains 10% Turn Meter for each active Phoenix ally; if the target has no debuffs, deal +75% more damage plus bonus damage equal to 20% of the target's Max Health; if the target has any buffs, ignore their Armor; if the target has any debuffs, this attack has 50% Critical Damage"#,
        

            "Aggressive Tactician" : #"Recover 10% Protection whenever an enemy is defeated, +20% Offense Up Chance, and +60% Turn Meter Gain Chance"#,
        

            "Legendary Strategist" : #"Whenever an Empire ally gains or loses a status effect, they recover 2% Protection"#,
        

            "Ebb and Flow" : #"+50% Counter Chance and +50% Tenacity while any enemies are Fractured, and whenever another Empire ally uses a Special ability while Thrawn is active, if any enemies are Fractured, Fractured enemies lose 15% Turn Meter"#,
        

            "Battle Meditation" : #"Cooldown - 1 and gain Foresight for 2 turns"#,
        

            "Grand Master's Guidance" : #"+5% Tenacity, +5% Turn Meter Gain, and whenever a Jedi ally suffers a debuff they gain Tenacity Up for 1 turn at the end of that turn"#,
        

            "Tighten the Grip" : #"+2 Speed and also inflict Expose for 2 turns on enemies that fall below 100% Health during Empire allies turns."#,
        

            "Callous Conviction" : #"Gains 20% Potency for each debuffed enemy"#,
        

            "Threaten" : #"Add Stun for 1 turn if target is reduced to less than 20% Turn Meter, this Stun can't be Resisted"#,
        

            "Shoots First" : #"+10% Critical Chance and the first time each turn Han uses his Basic attack, he attacks again dealing 50% less damage"#,
        

            "Play to Strengths" : #"+25% Potency, Dispel all debuffs, and if the target is Phoenix reduce their cooldowns by 1 and grant them 50% Turn Meter"#,
        

            "Strength Flows From the Force" : #"Cooldown - 1, all allies recover 20% Protection, and Jedi allies gain Foresight for 2 turns"#,
        

            "Do or Do Not" : #"Whenever an ally with Master's Training is defeated, Yoda's cooldowns are reset"#,
        

            "Adaptive Aim Algorithm" : #"While IG-88 is active, Target Locked enemies can't recover Health or Protection"#,
        

            "Imperial Logistics" : #"While Imperial Probe Droid is active Target Locked enemies can't gain buffs"#,
        

            "Into the Fray" : #"Rebel allies recover 5% Protection whenever they gain a buff"#,
        

            "Fierce Determination" : #"Jyn is immune to Stun"#,
        

            "Reprogrammed Imperial Droid" : #"K-2SO gains 1% Max Protection whenever he takes damage"#,
        

            "Total Defense" : #"Dispel all debuffs on Phoenix allies and grant them Defense Up for 3 turns, grant Kanan and target ally Foresight for 2 turns, and when each of these Foresights expire Kanan gains 100% Turn Meter and other Phoenix allies gain 50% Turn Meter"#,
        

            "Outrage" : #"+15% Damage and recover Protection equal to the damage dealt"#,
        

            "Merciless Pursuit" : #"+15% Critical Damage, +5 Speed, +5% Turn Meter Gain, and when a First Order ally gains a status effect they recover 5% Health and 5% Protection"#,
        

            "Scarred" : #"Kylo gains Taunt for 1 turn at the start of each encounter and has double Counter Chance while Taunting"#,
        

            "Shaman's Insight" : #"While Logray is active, whenever an Ewok ally scores a Critical Hit, that ally gains Health Up for 2 turns, and then all Ewok allies with Health Up recover 10% Health"#,
        

            "A New Hope" : #"+5% Tenacity and whenever an ally Resists a detrimental effect they gain Advantage for 2 turns"#,
        

            "Draw a Bead" : #"At the start of each of his turns, Luke gains 10% Critical Damage (stacking) until the end of the encounter"#,
        

            "Elegant Steps" : #"+2% Heal and whenever any ally gains a buff they do not have, they gain a Heal Over Time effect for 2 turns"#,
        

            "The Great Mother" : #"When a Nightsister ally uses a Basic ability during their turn, a random defeated Nightsister ally is Revived with 50% Health and called to Assist, then the Revived ally is immediately defeated unless an enemy was defeated (the Revived ally can't be Revived by this effect again until the end of the next turn)"#,
        

            "Plaguebearer" : #"When any unit falls below half health, Talzin gains 35% Turn Meter"#,
        

            "Nightsister Retaliation" : #"+30% Counter Chance and +50% Offense"#,
        

            "Devoted Protector" : #"When this Taunt expires, Old Ben gains Taunt for 1 turn"#,
        

            "If You Strike Me Down" : #"+2% Turn Meter Gain, +20% Heal, +20% Protection Recovery, and the first time Old Ben is defeated all allies gain 25% Turn Meter"#,
        

            "Serve Again" : #"When another ally is Revived while Old Daka is active, Old Daka gains +10% Max Health (stacking) until the end of the encounter"#,
        

            "For Pipada" : #"Gains 5% Turn Meter whenever a Rebel ally uses Basic attack."#,
        

            "Don't Hold Back" : #"Whenever Paploo gains a status effect, he recovers 5% Protection"#,
        

            "Against All Odds" : #"+45% Chance to Heal and whenever Leia attacks she recovers 5% Protection"#,
        

            "Agility Training" : #"+5 Speed, Jedi allies gain Offense equal to 3 times their Speed, and gain Foresight for 2 turns at the start of each encounter and whenever any unit is defeated"#,
        

            "Combat Analysis" : #"While R2-D2 is active, whenever a Light Side ally scores a Critical Hit, dispel all debuffs on them"#,
        

            "Number Crunch" : #"At the start of battle, and when R2-D2 is revived, other Droid, Galactic Republic, Rebel, and Resistance allies gain 10% of R2-D2's Max Protection, Offense, Max Health, and Potency until R2-D2 is defeated"#,
        

            "Dauntless" : #"+10% Offense and the first time each turn Leia Resists a detrimental effect or suffers a debuff she gains 35% Offense (stacking) until the end of her next turn"#,
        

            "Inspirational Presence" : #"When a Resistance ally uses a Special ability, all Exposed enemies lose 5% Turn Meter, which can't be Evaded or Resisted; When a Resistance ally uses a Special ability, if they aren't debuffed, reduce their Cooldowns by 1"#,
        

            "Virtuous Protector" : #"When Rey suffers a debuff she has a 40% chance to Dispel all debuffs on herself"#,
        

            "Insight" : #"At the end of her turn, if Rey already had Critical Damage Up, she gains Offense Up until she is damaged; at the start of each encounter, Rey gains Foresight until she Evades; when Rey Evades, she recovers 5% Health and 5% Protection"#,
        

            "Focused Strikes" : #"Whenever Rey uses a Special ability, if she has no debuffs, she inflicts Daze for 2 turns which can't be Resisted"#,
        

            "Valiant Spirit" : #"When another Resistance ally scores a Critical Hit, Rose Tico gains 10% Turn Meter"#,
        

            "Demolish" : #"Phoenix allies gain Critical Chance Up and Offense Up for 2 turns, reduce Sabine's cooldowns by 2 on a Critical Hit, Stagger all enemies for 2 turns, and for each active Phoenix ally deal 15% more damage and Expose a random enemy"#,
        

            "Brute" : #"Gain Heal Over Time when damaged, Buff Duration + 1, +50% Turn Meter Gain Chance, Dispel all debuffs from Savage whenever he is Critically Hit"#,
        

            "Wall of Stormtroopers" : #"Stormtrooper gains 40% Offense for each defeated Empire ally"#,
        

            "Bluff" : #"Whenever Han takes damage he recovers 5% Protection"#,
        

            "Ferocious Protector" : #"When another light side Scoundrel ally is defeated, Revive that ally at 60% Health, if Chewbacca is Prepared. Then, Chewbacca is no longer Prepared."#,
        

            "Let the Wookiee Win" : #"Offense Duration + 1"#,
        

            "Swindle" : #"Whenever Han gains any status effect, he gains 50 Armor Penetration (stacking) until the end of his next turn"#,
        

            "Returned to the Light" : #"While Visas Marr has no debuffs, she has a 60% chance to Assist when another ally attacks Sith enemies once per turn."#,
        

            "Furious Foe" : #"Inflict Healing Immunity on all enemies for 2 turns, which can't be Evaded"#,
        

            "Cornered Beast" : #"Wampa has +10% Counter Chance, +5% Health Steal, and +5% Offense for each Damage Over Time effect on each enemy"#,
        

            "Furtive Tactics" : #"Whenever Wicket scores a Critical Hit, all Ewok allies recover 4% Health and 2% Protection"#,
        

            "Ready For Anything" : #"While Prepared, Han has 70% chance to Assist another Scoundrel ally's attack during their turn, limited once per turn"#,
        

            "Insult to Injury" : #"+5% Potency and other Scoundrel allies also gain half Potency and Critical Damage bonuses"#,
        

            "Fighting Instinct" : #"Add Taunt Ignore, add 2% Health Steal (stacking) when enemy loses buff or debuff"#,
        

            "Perfect Timing" : #"Add another 50% Chance for each debuffed enemy"#,
        

            "For the Droids" : #"Add The first time each other Droid ally is reduced to 1% Health, they equalize Health with L3-37"#,
        

            "Initiative" : #"Add 1 Turn Taunt on Jedi Tank allies at start of encounter, add +150% Tenacity with Protection Up "#,
        

            "That Looks Pretty Bad" : #"Add Revive all defeated Jedi allies at 80% Health with Crit Immunity for 5 turns"#,
        

            "Combat Logic Upgrade" : #"Add Droid allies also gain 8% Potency and Critical Hit Chance when T3-M4 uses an ability. Also Droid allies have +8% Critical Damage for each debuff on the enemy team."#,
        

            "Master Gearhead" : #"Droid allies gain 100% of T3-M4's Defense Penetration."#,
        

            "Mission's Guardian" : #"At the start of each of his turns, Zaalbar and Mission recover 10% Protection for each Damage Over Time effect on enemies"#,
        

            "Me and Big Z Forever" : #"Once per turn, Mission Assists when Zaalbar uses an ability"#,
        

            "The Quiet Assassin" : #"When a Bounty Hunter ally scores a Critical Hit during their turn, they gain Offense Up for 1 turn"#,
        

            "Way of the Kyuzo" : #"Embo Can't Be Critically Hit"#,
        

            "Snipers Expertise" : #"Add if the target is Toppled, this ability's cooldown is reset"#,
        

            "Game Plan" : #"Add whenever a Bounty Hunter ally defeats an enemy, Bounty Hunter Attackers gain Stealth for 2 turns"#,
        

            "Anything To Get Ahead" : #"Enemies have -20% Potency for each debuff on all Bounty Hunter allies; Bounty Hunters have +35% Offense while they have Bounty Hunter's Resolve"#,
        

            "Notorious Reputation" : #"Enemies defeated on his turn cannot be Revived"#,
        

            "Loyal Friend" : #"When Chewbacca deals damage to an enemy, Chewbacca and all Guarded allies recover 3% Health and 3% Protection"#,
        

            "Raging Wookiee" : #"Chewbacca is immune to Ability Block and Cooldown Increase"#,
        

            "Direct Focus" : #"Add Buff Immunity for 2 turns, which can't be Resisted, Copied, or Dispelled and increase their cooldowns by 1 and Stun the target for 1 turn"#,
        

            "General of the Old Republic" : #"Add whenever a Jedi ally attacks out of turn they recover 20% of their Max Protection and deal 35% more damage. When a Jedi ally uses a Basic ability on an enemy with Bonus Protection, reduce the target enemy's Max Health by 10% (stacking, doesn't work against Raid Bosses). Jedi allies begin the battle with Tenacity Up for 1 turn"#,
        

            "Hero" : #"Add Jedi Knight Revan is Immune to Stun and Ability Block and other Light Side allies also gain half of these bonuses. The bonuses are doubled while Revan is in the leader slot and not the ally slot"#,
        

            "Sith Apprentice" : #"Bastila and the character in the leader slot gain Foresight for 1 turn at the start of battle"#,
        

            "Oh My Goodness!" : #"Inflict Confuse twice on target enemy for 3 turns"#,
        

            "Wait for Me!" : #"At start of encounter, C-3PO and R2-D2 gain Translation for 3 turns"#,
        

            "I Like a Challenge" : #"Canderous gains Critical Damage Up for 2 turns when an enemy gains Taunt, or becomes Deathmarked or Marked"#,
        

            "Soldier of the Old Republic" : #"Whenever an enemy is inflicted with a Damage Over Time effect, all Old Republic allies recover 3% Health and Protection"#,
        

            "Cathar Resilience" : #"When Juhani drops below 100% Health, she recovers 5% Protection for each active Jedi or Old Republic ally, then gains Offense Up and Health Steal Up for 2 turns."#,
        

            "Lord of the Sith" : #"Sith Empire allies are immune to Turn Meter reduction while they are debuffed"#,
        

            "Conqueror" : #"Enemies can't assist or attack again if their attack is targeting Darth Revan"#,
        

            "Villain" : #"When Darth Revan's Health falls below 50%, it is equalized with the other healthiest ally"#,
        

            "Gnawing Terror" : #"Add at the end of each turn, enemies who inflicted Damage Over Time on Darth Malak or critically hit him this turn are inflicted with Fear for 1 turn, which can't be resisted or evaded"#,
        

            "Jaws of Life" : #"Add the first time Darth Malak reaches each of these thresholds, he gains a bonus turn and the cooldown of Drain Life is reset"#,
        

            "Self-Reconstruction" : #"When HK-47 uses an ability during his turn, it deals 25% more damage if the target has Deathmark or Fear"#,
        

            "Loyalty to the Maker" : #"If HK-47's leader is Darth Revan, enemies with Protection Up can't gain bonus Turn Meter"#,
        

            "Soresu" : #"If all allies are Galactic Republic, General Kenobi Taunts for 1 turn whenever another Galactic Republic ally loses any Protection Up"#,
        

            "Daring Padawan" : #"If the ally leader is Galactic Republic, Ahsoka is called to assist whenever another Galactic Republic ally uses a Special ability"#,
        

            "Unwavering Courage" : #"While they have Protection Up, Light Side allies resist all debuffs and can't be critically hit"#,
        

            "Always a Choice" : #"This Protection Up can't be prevented or dispelled and is increased to 40% if Padmé is their leader"#,
        

            "Righteous Fury" : #"If Padmé Amidala is an ally, she takes a bonus turn after each of Anakin's bonus turns"#,
        

            "Stunning Strike" : #"If no buffs were dispelled, reset the cooldown of this ability"#,
        

            "Daunting Presence" : #"When a Target Locked enemy is damaged, each Droid and each Separatist ally gains 2% Turn Meter, doubled for Grievous"#,
        

            "Metalloid Monstrosity" : #"Whenever a Droid or Separatist ally is defeated, Grievous dispels all debuffs on himself, resets all ability cooldowns and gains 1 bonus turn"#,
        

            "Deflector Shield Generator" : #"Add +25% Evasion, and if Droideka has Damage Immunity it has a 35% chance to assist dealing 50% less damage whenever another Separatist ally uses an ability (limit once per turn)"#,
        

            "Reactive Protocol" : #"Whenever B2 inflicts a debuff, all other Separatist Droid allies gain 5% Turn Meter"#,
        

            "Droid Battalion" : #"+50 stacks of Droid Battalion"#,
        

            "Unity Wins War" : #"Add Clone Trooper allies recover 5% Health and Protection when they attack out of turn"#,
        

            "Heightened Reflexes" : #"Add whenever Shaak Ti or a Clone Trooper ally takes damage from an attack they gain Retribution for 2 turns at the end of that turn"#,
        

            "Domino Squad" : #"Fives gains 15% Turn Meter whenever another Clone Trooper or 501st ally takes damage and half that amount for other allies"#,
        

            "Captain of the 501st" : #"Other 501st Clone Trooper allies reduce their own ability cooldowns by 1 at the start of each of their turns"#,
        

            "ARC Arsenal" : #"The Blaster Turret will assist when ARC Trooper attacks out of turn"#,
        

//            "Domino Squad" : #"Add when an enemy evades an attack from a Clone Trooper or 501st ally, Echo deals damage to them equal to 20% of their Max Health, which can't be evaded"#,
        

            "Mass Manufacture" : #"Wat takes a bonus turn instead"#,
        

            "Jedi Council" : #"+30 Speed"#,
        

            "Furious Slash" : #"If this is the first ability General Skywalker uses during his turn, he immediately uses the Special ability Telekinesis, if able"#,
        

            "General of the 501st" : #"When an enemy gains a buff, all 501st allies gain 2% Offense (stacking) until the end of the encounter"#,
        

            "The Chosen One" : #"If the target enemy has no Protection when General Skywalker uses an ability, reduce their current Max Health by 20% for rest of the battle (stacking, excluding raid bosses), which can't be resisted"#,
        

            "Hero with no Fear" : #"General Skywalker has +100% counter chance and +50% Critical Chance if all allies are 501st at the start of battle"#,
        

            "Geonosian Swarm" : #"Basic abilities of Geonosian allies deal 10% more damage for each buffed enemy"#,
        

            "Queen's Will" : #"At the start of the encounter, summon a Geonosian Brute who Taunts for 1 turn"#,
        
]
}
