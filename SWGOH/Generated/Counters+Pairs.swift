// see counters.py
extension Counter {
static let squad5v5 = [
Counter(team: "501ST", counter: "SITH_EMPIRE_W_MALAK", hard: false, description: 
"""
Try to keep Fear on General Skywalker.  Keep Dark Infusion on Darth Malak as much as possible to boost his damage output to quickly take out the Clone Troopers after you knock Gen. Skywalker down.  If you can't kill all of the Clones and Ahsoka fast enough and Gen. Skywalker goes into taunt mode more than one time, you will more than likely lose.<br/><br/> With Bastila Shan (Fallen), use Fear over Shock.  With Darth Revan, be careful not to use Wild Lightning if you feel it will kill a Clone Trooper before CT-5555 "Fives" dies, or else he'll sacrifice himself and buff the team.
""", video: #"https://www.youtube.com/watch?v=-NqDKeVHZCY&t=34s"#),

Counter(team: "501ST", counter: "GALACTIC_REPUBLIC_C3PO", hard: false, description: 
"""
Highly recommended to use against a 7 Relic General Skywalker.  All zetas needed, except C-3PO's not Galactic Republic faction zetas.  Protection up being counted as courage, critical hit immunity, and high single target damage are the perks to using this team.  Follow the general strategy, and hopefully by the time you have General Skywalker alone, you hopefully have enough Courage to kill him with big hits.
""", video: #"https://www.youtube.com/watch?v=-NqDKeVHZCY&t=34s"#),

Counter(team: "501ST", counter: "REBELS_CLS", hard: false, description: 
"""
Must match Gear and Relics for this to work.  You have a enough damage at the beginning of the match to put General Skywalker on his knees early.  Then you have to be able to put out enough damage to take the clones out before he gets back up.  After that, you need enough damage and sustain to take on Anakin alone.
""", video: #"https://www.youtube.com/watch?v=-NqDKeVHZCY&t=34s"#),

Counter(team: "501ST", counter: "SEP_DROIDS_WAT", hard: false, description: 
"""
Works against 7* General Skywalker, must have 7* General Grievous.
""", video: #""#),

Counter(team: "501ST", counter: "SEP_DROIDS", hard: false, description: 
"""
Does not work against a 7* General Skywalker.  Currently only recommended against a 5* GAS.
""", video: #""#),

Counter(team: "BH_BOSSK", counter: "SEP_NUTE_NEST", hard: true, description: 
"""
This team can easily punch up 10k+, even with a low-starred Wat Tambor.<br/><br/>

Give Wat's Baktoid Shield Generator to Enfys Nest to recover Protection and dispel debuffs.  This helps a lot since Count Dooku's Master of Makashi will stealth everyone else (all non-tank Separatists).<br/><br/>

Give Wat's BlasTech Weapon Mod to Jango Fett if he doesn't have the Notorious Reputation zeta or else Count Dooku.<br/><br/>

Give Wat's Chiewab Medpac to the weakest remaining ally.  I prefer Nute Gunray, to ensure he stays alive to keep applying Extortion.
""", video: #""#),

Counter(team: "BH_BOSSK", counter: "JTR", hard: true, description: 
"""
R2-D2 recommended.  Use Rey (Jedi Training)'s dispel/turn meter reduction on Bossk, and even R2-D2s stun on Bossk for extra assurance.  For JTR's assist attack, always call BB-8 (if he has Rolling with the Punches zeta) because he will call another Resistance ally to assist.  Avoid using AoE attacks until Dengar is in stealth, at which point, R2-D2's stealth will be useless.  Kill order: Boba Fett, Jango Fett, [whichever Bounty Hunter], Bossk, then Dengar.
""", video: #""#),

Counter(team: "BH_BOSSK", counter: "REBELS_CLS", hard: true, description: 
"""
Chewbacca recommended.  A fast Chirrut Îmwe is useful to apply tenacity up ASAP.  Kill Boba first.  If they are using Enfys Nest, stun her with Han Solo.
""", video: #""#),

Counter(team: "BH_BOSSK", counter: "JEDI_BASTILA", hard: true, description: 
"""
Best counter.  No debuffs can stick.  Kill Boba Fett first.  Remember Boba Fett and Jango Fett will revive.  If they are using Enfys Nest, kill her last.
""", video: #""#),

Counter(team: "BH_BOSSK", counter: "CLONES_SHAAKTI", hard: true, description: "", video: #"https://www.youtube.com/watch?v=QhwRfO6Wjl0&t=255s"#),

Counter(team: "BH_BOSSK", counter: "GEONOSIANS", hard: true, description: 
"""
You should have equal or higher gear.  Use Poggle the Lesser's ability block on Bossk first, to prevent his taunt.  Sun Fac and Geonosian Brood Alpha have dispel, in case Bossk taunts.  Geonosian Spy can one shot most enemies, if he is stealthed and the enemy has enough status effects.  Take out Boba Fett, then Jango Fett, then the rest.
""", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "BH_BOSSK", counter: "SEP_DROIDS", hard: true, description: "", video: #""#),

Counter(team: "BH_BOSSK", counter: "QIRA_NEST_YOUNGHAN", hard: true, description: 
"""
Enfys Nest must be faster than Bossk for this to work properly.<br/></br>

The AI Bounty Hunters will inherently have a problem attacking Nest with the correct strategy.  Vandor Chewbacca will revive fallen characters if Prepared.
""", video: #"https://www.youtube.com/watch?v=Kf65A7NaH7I"#),

Counter(team: "BH_BOSSK", counter: "EP", hard: false, description: 
"""
Use ability blocks and stuns to cripple the Bounty Hunters. Ability block priority is Bossk (to stop his taunt), then Boba Fett (to stop/delay his ability block), then anyone else who isn't initially stunned by Emperor Palpatine's shock.  Fracture Bossk if you have Grand Admiral Thrawn and focus on Boba Fett (first) then Jango Fett.
""", video: #""#),

Counter(team: "BH_BOSSK", counter: "IT", hard: false, description: 
"""
Make sure Colonel Starck goes first.  Focus on the weakest enemy first to get the turn meter rolling to be able to use Death Trooper's Deathmark ASAP.  Deathmark will negate Boba Fett's revive!  If Colonel Starck can't go before Bossk, and Bossk has his Unique Zeta - you need 2 dispels to starting working on the weaker Bounty Hunters (risky, but doable).
""", video: #""#),

Counter(team: "BH_BOSSK", counter: "FO_KRU", hard: false, description: 
"""
Kylo Ren (Unmasked) and First Order Stormtrooper defend your weak characters, which delay's the contract.  Stun Bossk with Kylo Ren (Unmasked), which gives you two turns to kill 1-3 weaker Bounty Hunters.  If your Kylo Ren (Unmasked) is slower than Bossk, use First Order Officer; Kylo Ren; and First Stormtrooper to give turn meter to Kylo Ren (Unmasked) to make sure Bossk is stunned.
""", video: #""#),

Counter(team: "BH_JANGO", counter: "SEP_NUTE_NEST", hard: true, description: 
"""
This team can easily punch up 10k+, even with a low-starred Wat Tambor.<br/><br/>

Give Wat's Baktoid Shield Generator to Enfys Nest to recover Protection and dispel debuffs.  This helps a lot since Count Dooku's Master of Makashi will stealth everyone else (all non-tank Separatists).<br/><br/>

Give Wat's BlasTech Weapon Mod to Jango Fett if he doesn't have the Notorious Reputation zeta or else Count Dooku.<br/><br/>

Give Wat's Chiewab Medpac to the weakest remaining ally.  I prefer Nute Gunray, to ensure he stays alive to keep applying Extortion.
""", video: #""#),

Counter(team: "BH_JANGO", counter: "JTR_DROIDS", hard: true, description: "", video: #""#),

Counter(team: "BH_JANGO", counter: "SITH_TRI", hard: true, description: "", video: #""#),

Counter(team: "BH_JANGO", counter: "CLONES_SHAAKTI", hard: true, description: "", video: #"https://www.youtube.com/watch?v=QhwRfO6Wjl0&t=255s"#),

Counter(team: "BH_JANGO", counter: "JEDI_BASTILA", hard: true, description: 
"""
Best counter.  No debuffs can stick.  Kill Boba Fett first.  Remember Boba Fett and Jango Fett will revive.  If they are using Enfys Nest, kill her last.
""", video: #""#),

Counter(team: "BH_JANGO", counter: "REBELS_CLS", hard: true, description: "", video: #""#),

Counter(team: "BH_JANGO", counter: "GALACTIC_REPUBLIC", hard: true, description: "", video: #""#),

Counter(team: "BH_JANGO", counter: "SEP_DROIDS", hard: true, description: "", video: #""#),

Counter(team: "BH_JANGO", counter: "GEONOSIANS", hard: false, description: "", video: #""#),

Counter(team: "BH_JANGO", counter: "EP", hard: false, description: 
"""
Use ability blocks and stuns to cripple the Bounty Hunters. Ability block priority is Bossk (to stop his taunt), then Boba Fett (to stop/delay his ability block), then anyone else who isn't initially stunned by Emperor Palpatine's shock.  Fracture Bossk if you have Grand Admiral Thrawn and focus on Boba Fett (first) then Jango Fett.
""", video: #""#),

Counter(team: "BH_JANGO", counter: "FO_KRU", hard: false, description: 
"""
Kylo Ren (Unmasked) and First Order Stormtrooper defend your weak characters, which delay's the contract.  Stun Bossk with Kylo Ren (Unmasked), which gives you two turns to kill 1-3 weaker Bounty Hunters.  If your Kylo Ren (Unmasked) is slower than Bossk, use First Order Officer; Kylo Ren; and First Stormtrooper to give turn meter to Kylo Ren (Unmasked) to make sure Bossk is stunned.
""", video: #""#),

Counter(team: "CLONES_SHAAKTI", counter: "SITH_TRI", hard: true, description: "", video: #""#),

Counter(team: "CLONES_SHAAKTI", counter: "GALACTIC_REPUBLIC", hard: true, description: 
"""
The Clones don't have much of a way to dispel all of the protection up from a Padmé led team.  Padmé's leadership and unique abilities work well against this team, as the Clones' assists will give their target extra protection, which gets converted to Courage on their turn (only if you don't have a zeta on Padmé's Always a Choice).  Eventually with enough Courage, your characters will be able to one-shot the Clones to win the match.
""", video: #"https://www.youtube.com/watch?v=QhwRfO6Wjl0&t=255s"#),

Counter(team: "CLONES_SHAAKTI", counter: "SEP_DROIDS", hard: true, description: 
"""
B2 Super Battle Droid's constant buff dispel and buff immunity are necessary to shut down Shaak Ti's mass buffs.  General Grevious' zeta lead is helps stop counter attacks from target-locked enemies.
""", video: #"https://www.youtube.com/watch?v=QhwRfO6Wjl0&t=255s"#),

Counter(team: "CLONES_SHAAKTI", counter: "REBELS_CLS", hard: true, description: 
"""
Keep CT-7567 "Rex" and Shaak Ti stunned.  Start the match by stunning Rex first, although it feed's Shaak Ti's turn meter it'll also delay the mass tenacity buff/turn meter/cleanse.  Use C-3PO's confuse as a daze to stop the counters and assists.  Be careful to kill CT-5555 "Fives" before CT-21-0408 "Echo" dies.
""", video: #"https://www.youtube.com/watch?v=QhwRfO6Wjl0&t=255s"#),

Counter(team: "CLONES_SHAAKTI", counter: "JEDI_REVAN", hard: true, description: 
"""
Use Jedi Knight Revan's Direct Focus to Mark Shaak Ti and take her out first.  If CT-5555 "Fives" has his zeta, you should consider taking him second or maybe even first, depending on the situation.
""", video: #"https://www.youtube.com/watch?v=QhwRfO6Wjl0&t=255s"#),

Counter(team: "CLONES_SHAAKTI", counter: "EP", hard: false, description: "", video: #""#),

Counter(team: "CLONES_SHAAKTI", counter: "GEONOSIANS", hard: false, description: 
"""
Keep CT-7567 "Rex" and Shaak Ti ability blocked with Poggle the Lesser, and dispel retribution with Sun Fac.  Kill order is: CT-5555 "Fives", Shaak Ti, then CT-21-0408 "Echo".
""", video: #""#),

Counter(team: "CLONES_SHAAKTI", counter: "NEST_SOLO", hard: false, description: 
"""
Be very cautious.  This can be done, but you MUST stun CT-7567 "Rex", as he can one-shot Enfys Nest at full protection with Aerial Assault.  It is currently reported that a Relic 2 Nest can lose to a G12 Rex.
""", video: #""#),

Counter(team: "CLONES_SHAAKTI", counter: "JTR", hard: false, description: 
"""
The turn meter gain and crowd control will make this a more manageable fight.  Use R2-D2's stealth to subvert the Clones' counter attacks.
""", video: #""#),

Counter(team: "EP", counter: "BH_JANGO", hard: true, description: 
"""
Note: this counter is subject to change due to the Darth Vader rework.<br/><br/>

You need a fast Jango Fett with high potency so you can land Conflagration, which will prevent them from gaining turn meter.  Stuns and ability block order:  Grand Admiral Thrawn, Bastila Shan (Fallen), Darth Nihilus.  You may want to avoid this counter if they have Darth Traya.
""", video: #""#),

Counter(team: "EP", counter: "JEDI_BASTILA", hard: true, description: 
"""
Note: this counter is subject to change due to the Darth Vader rework.<br/><br/>

Bastila Shan's leadership ability provides enough tenacity to stop Emperor Palpatine's leadership ability giving them tons of turn meters based on debuffs.  If you don't have Bastila Shan, your next best option would be a Grand Master Yoda lead (if leadership is zeta'd) for a soft counter.
""", video: #""#),

Counter(team: "EP", counter: "CLONES_SHAAKTI", hard: true, description: 
"""
Note: this counter is subject to change due to the Darth Vader rework.<br/><br/>

Use CT-7567 "Rex"'s Tenacity Up ability as soon as possible to prevent the EP turn meter train from starting.  Use Shaak Ti's Assault Team to mass assist on Grand Admiral Thrawn (if available), then either Emperor Palpatine or Grand Moff Tarkin.  Use CT-5555 "Fives"'s assist ability, calling either CT-21-0408 "Echo" or Rex.  Use Echo's EMP Grenade AoE.  Use Clone Sergeant - Phase I's Suppressive Fire AoE.<br/><br/>

At this point, Palpatine and Tarkin should be taken out.  Focus on either TIE Fighter Pilot or Darth Vader next, but be careful not to lower Imperial Probe Droid below 100% health, or he'll use his self destruction AoE.
""", video: #""#),

Counter(team: "EP", counter: "REBELS_CLS", hard: true, description: 
"""
Note: this counter is subject to change due to the Darth Vader rework.<br/><br/>
""", video: #""#),

Counter(team: "EP", counter: "JTR", hard: false, description: 
"""
Note: this counter is subject to change due to the Darth Vader rework.<br/><br/>

You need to outspeed them to win.  Amilyn Holdo is highly recommended for the AoE daze.  You want your BB-8 to go before their Darth Vader, otherwise you'll end up relying on RNG for a win.
""", video: #""#),

Counter(team: "EP", counter: "IT", hard: false, description: 
"""
Note: this counter is subject to change due to the Darth Vader rework.<br/><br/>

Do not use Imperial Troopers if they have an auto-taunter like Sith Empire Trooper or Shoretrooper. Your General Starck must outspeed their Darth Vader.  If you can get a kill before they start their debuff-based turn meter train rolling, you'll outspeed them.
""", video: #""#),

Counter(team: "EP", counter: "BH_BOSSK", hard: false, description: 
"""
Note: this counter is subject to change due to the Darth Vader rework.<br/><br/>
""", video: #""#),

Counter(team: "EP", counter: "REX", hard: false, description: 
"""
Note: this counter is subject to change due to the Darth Vader rework.<br/><br/>

You want to use Rex's tenacity up as early as possible to stop Emperor Palpatine's debuff-based leadership ability from feeding their team tons of turn meter.<br/><br/>

Having Chewbacca + Han Solo can help if Chewie's Loyal Friend is on Rex, which will make him immune to stun/daze.<br/><br/>

You could sub in Jyn Erso if she has a zeta on Fierce Determination (immune to stun), then on her turn grant Rex 100% turn meter to use his tenacity up.<br/><br/>

If using Mother Talzin, only inflict Plague on team after you have Rex's tenacity up on your team; otherwise, their debuffs on you can heal them to full health which will remove Plague.
""", video: #""#),

Counter(team: "EWOKS", counter: "NEST_SOLO", hard: true, description: 
"""
You must have high tenacity on Enfys Nest, or else you may time out due to dazes and stuns.  Nest has a useful dispel, and Ewoks don't have enough damage to kill her.  You can use Luke Skywalker (Farmboy) lead for extra tenacity or CT-7567 "Rex"s lead for extra health and turn meter when taking crit hits, and for his tenacity buff.  Can add Hermit Yoda for 25% offense, defence, tenacity, and potency.
""", video: #""#),

Counter(team: "EWOKS", counter: "IT", hard: true, description: "", video: #""#),

Counter(team: "EWOKS", counter: "SITH_TRI", hard: true, description: 
"""
Darth Traya's leadership ability will make the Ewoks kill themselves due to the high number of assists.  Use Traya's Isolate on Ewok Elder to prevent Stealth or the mass Cleanse.  Darth Sion's mass debuff will help keep the taunt off of Paploo.  If they are even-powered or under, consider leaving Nihilus out to use in another team.
""", video: #""#),

Counter(team: "EWOKS", counter: "GALACTIC_REPUBLIC", hard: true, description: 
"""
This is a very effective counter, but only use if you don't have a better use of your Galactic Republic team.  The Ewoks' high assist rate will easily feed Courage to your team so that you can one-shot kill them.
""", video: #"https://youtu.be/7eW5uY-mlVI"#),

Counter(team: "EWOKS", counter: "FO_KRU", hard: true, description: "", video: #""#),

Counter(team: "EWOKS", counter: "EP", hard: true, description: "", video: #""#),

Counter(team: "EWOKS", counter: "BH_BOSSK", hard: true, description: 
"""
Ewoks lack the damage to hurt Bossk badly.  Use Boba Fett's Execute to kill (which also prevents revive!).  Thermal detonators will help the fight.
""", video: #""#),

Counter(team: "EWOKS", counter: "NS_MT", hard: true, description: 
"""
Put Plague on the Ewoks and as they gain turn meter they will hurt themselves.  
""", video: #""#),

Counter(team: "EWOKS", counter: "BH_JANGO", hard: true, description: "", video: #""#),

Counter(team: "EWOKS", counter: "OR_CARTH", hard: false, description: "", video: #""#),

Counter(team: "EWOKS", counter: "REX_WAMPA", hard: false, description: 
"""
Chirrut Îmwe and Baze Malbus recommended (for tenacity up, dispel, and taunt).  A fifth needs to be a high damage attacker (e.g. Enfys Nest, Wampa, Mother Talzin, Darth Nihilus, Boba Fett, Jango Fett, or Darth Sion).
""", video: #""#),

Counter(team: "EWOKS", counter: "WAMPA_SOLO", hard: false, description: 
"""
Wampa will use counter attack and daze.  Can use Hermit Yoda for 25% offense, defense, tenacity, anc potency.  This is not as reliable against higher level Ewok teams (G12+)
""", video: #""#),

Counter(team: "EWOKS", counter: "GK", hard: false, description: 
"""
Can sub out Wampa, Darth Nihilus, or Mother Talzin for another high damage character like Enfys Nest, Boba Fett, Jango Fett, or Darth Sion
""", video: #""#),

Counter(team: "EWOKS", counter: "SITH_MAUL", hard: false, description: 
"""
Darth Maul and Savage Opress's zetas are needed to make this much easier, as Savage's zeta is tanky and takes away the dazes.
""", video: #""#),

Counter(team: "EWOKS", counter: "ZADER", hard: false, description: 
"""
Put damage on turn on the Ewoks, then watch them gain TM and kill themselves slowly.  Shoretrooper is highly recommended to ensure a win since he gives crit immunity to all and makes them take almost no damage.  Boba Fett's Execute can prevent revive and do huge damage against lots of buffs.
""", video: #""#),

Counter(team: "EWOKS", counter: "SION_SOLO", hard: false, description: 
"""
Useful dispel, and Ewoks don't have enough damage to kill him.  Can use a Luke Skywalker (Farmboy) lead for extra tenacity or CT-7567 "Rex"s lead for extra health and turn meter when taking crit hits. Can add Hermit Yoda for 25% offense, defence, tenacity, and potency.
""", video: #""#),

Counter(team: "FO_KRU", counter: "EWOKS", hard: true, description: 
"""
Does not work against relics.  With Logray, Wicket, and Scout, daze all with Logray.  Deal big damage attacking from stealth with Ewok Scout and Wicket since they aren't built on debuffs, but have turn meter gains, nice damage, and calls assists.
""", video: #""#),

Counter(team: "FO_KRU", counter: "JTR", hard: true, description: 
"""
Your first focus should be on toons that fuel turn meter (e.g. First Order Officer), then big damage dealers (e.g. First Order Executioner, First Order TIE Pilot).  Try to keep Kylo Ren (Unmasked) stunned and/or keep an ability ready to dispel his taunt.  Use Rey (Jedi Training)'s Defiant Slash on KRU to apply an undispellable healing immunity when trying to kill him.
""", video: #""#),

Counter(team: "FO_KRU", counter: "GALACTIC_REPUBLIC", hard: true, description: "", video: #"https://www.youtube.com/watch?v=zdoif0xIIqQ"#),

Counter(team: "FO_KRU", counter: "CLONES_SHAAKTI", hard: true, description: "", video: #""#),

Counter(team: "FO_KRU", counter: "REBELS_CLS", hard: true, description: 
"""
Stun Enfys Nest (if present), kill Kylo Ren (Unmasked), kill the other, hit Enfys Nest after each of her turns.  Another option is to add Wampa.  DON'T USE C-3PO, he only brings heals to the Kylos with his exposes and debuffs.
""", video: #""#),

Counter(team: "FO_KRU", counter: "SEP_DROIDS", hard: true, description: "", video: #""#),

Counter(team: "FO_KRU", counter: "IT", hard: false, description: "", video: #""#),

Counter(team: "FO_KRU", counter: "GEONOSIANS", hard: false, description: 
"""
Your biggest issue will be Kylo Ren (Unmasked)'s leadership ability, which could cause a time-out situation due to the 5% health and protection gain upon receiving a debuff.  Therefore, you'll want to use Geonosian Spy's Silent Strike to one-shot enemies.  To maximize his attack, you'll need him to be stealthed and a lot of status effects on your target.  Kill order should be First Order Executioner, First Order Stormtrooper, First Order Officer, Kylo Ren, Kylo Ren (Unmasked).  This way you remove the biggest damage dealers and/or turn meter generators first.
""", video: #""#),

Counter(team: "FO_KRU", counter: "NS_MT", hard: false, description: 
"""
Required zetas are Mother Talzin's lead and Asajj Ventress' Rampage.  Old Daka's Serve Again zeta is recommended.  Keep First Order Executioner and Sith Trooper under control as much as possible.  Once your Nightsisters have died twice, don't waste a revive unless it's necessary.  You'll need the constant revives on basic to ramp up Asajj's damage so she can take out Kylo Ren (Unmasked).  Kill order: Sith Trooper, First Order Executioner, General Hux, Kylo Ren, and Kylo Ren (Unmasked).
""", video: #""#),

Counter(team: "FO_KRU", counter: "REX_WAMPA", hard: false, description: 
"""
Can use three Geonosians as additions to this team.  Sun Fac can taunt to soak up some damage, Geonosian Spy can one-shot characters, there is a lot of turn meter gain under Rex lead, and Wampa will pick them apart with daze.  
""", video: #""#),

Counter(team: "FO_KRU", counter: "REBELS_DANGER_ZONE", hard: false, description: 
"""
Requires Gear 11+ with lots of assists and strong damage.  Don't even try without Han Solo + Chewbacca against a high tier First Order team.
""", video: #""#),

Counter(team: "FO_KRU", counter: "PHOENIX", hard: false, description: 
"""
Garazeb "Zeb" Orrelios' zeta recommended.  Most of the damage will come from this team's counter attacks and using Sabine Wren's Armor Shred on Kylo Ren and Kylo Ren (Unmasked).  Zeb's zeta will crush a zeta'd Kylo Ren from all of the buffs and debuffs.
""", video: #""#),

Counter(team: "FO_SLKR", counter: "501ST_ECHO", hard: false, description: 
"""
You'll want to mod General Skywalker for offense and health (not protection).  The purpose of having an easily killable 501st ally, is to proc his Hero with No Fear ability which gives him turn meter after every enemy turn and prevents him from being critically hit.<br/><br/>

Supreme Leader Kylo Ren will go first and probably put GAS into Cover immediately.  Once they kill Echo and GAS returns from Cover, you'll use his AoE Force Grip to increase their cooldowns.  At this point, GAS will kill all of SLKR's allies from counterattacks, and SLKR won't be able to build up his Ultimate ability fast enough before dying because GAS can't be critically hit.

""", video: #""#),

Counter(team: "FO_SLKR", counter: "SITH_EMPIRE_W_MALAKTHRAWN", hard: false, description: "", video: #""#),

Counter(team: "FO_SLKR", counter: "GALACTIC_REPUBLIC_SLKRKILLER", hard: false, description: "", video: #""#),

Counter(team: "FO_SLKR", counter: "VADER_SLKRKILLER", hard: false, description: 
"""
Use Wat Tambor's BlasTech Weapon Mod on Darth Vader, Chiewab Medpack to Grand Admiral Thrawn, and Baktoid Shield Generator to Darth Malak.<br/><br/>

Use Malak's Torture to remove KRU's taunt, shock him, and gain taunt on Malak.<br/><br/>

Vader's Merciless Massacre should target Supreme Leader Kylo Ren so that Wat Tambor will apply additional Damage Over Time to him.  Use Force Crush on KRU so he can't counter.  On Vader's second Merciless Massacre, use his Basic on the outlying toons until Force Crush comes available, then use it on SLKR, then use Sabre Throw on him.<br/><br/>

Remember not to use Darth Malak's Drain Life on KRU, as it doesn't do much.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC", counter: "SITH_TRI", hard: true, description: 
"""
This doesn't work if Shaak Ti is subbed in, as her cleanse will shut Darth Sion down.<br/><br/>

You'll control the battle with Darth Traya's Isolate and Grand Admiral Thrawn's Fracture, then kill everyone with Darth Nihilus' Annihilate.  Isolate and Fracture Padmé first.  After she's dead, do the same on General Kenobi to stop his taunts.  Then whoever else you want after that.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC", counter: "SEP_DROIDS_WAT", hard: true, description: 
"""
This is more appropriate in higher relic scenarios.  You want General Grievous' Metalloid Monstrosity zeta.  B2 Super Battle Droid controls and stalls with buff immunity, and when allies start dying General Grievous gets extra turns.  Ideally use GG's AoEs after enemy team is dispelled by B2 and have buff immunity.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC", counter: "SEP_DROIDS", hard: true, description: 
"""
You want General Grievous' Metalloid Monstrosity zeta.  B2 Super Battle Droid controls and stalls with buff immunity, and when allies start dying General Grievous gets extra turns.  Ideally use GG's AoEs after enemy team is dispelled by B2 and have buff immunity.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC", counter: "RESISTANCE_HEROES", hard: true, description: 
"""
Avoid using assist attacks!  With comparable gear levels, you should have enough damage without the assists.  You want to kill Jedi Knight Anakin as early as possible.  Target Rey (Jedi Training) for Resistance Hero Poe's ally buff.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC", counter: "JEDI_REVAN", hard: false, description: 
"""
This should work with comparatively powered squads.  Avoid using assist attacks, other than Jedi Knight Revan's Direct Focus and Bastila Shan's Rally.  Direct Focus Jedi Knight Anakin until he's dead, patiently work on Ahsoka Tano next, then kill Padmé third.  It's an easier fight if facing a team with C-3PO because Grand Master Yoda can steal his steal. 
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC", counter: "EP", hard: false, description: 
"""
Not recommened if they have C-3PO or have relics.  Highly recommended to have Imperial Probe Droid's zeta for buff immunities and TIE Fighter Pilot modded for high potency.  You want Darth Vader to be fast enough to go first and start the turn meter train.  You could possibly add Grand Admiral Thrawn to fracture Padmé, but it's not required if you are confident you can control them with stuns, buff immunities, and keep your turn meter rolling.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC", counter: "SITH_EMPIRE_W_MALAK", hard: false, description: 
"""
This is a tougher counter, as your debuffs can quickly be turned into high stacks of Courage.  This will lead to your team being one-shotted.  Use Darth Revan's Force Storm before Insanity, so you get a shock on General Kenobi.  Use Bastila Shan (Fallen)'s fear on General Kenobi next, so he doesn't cleanse.  Then try to kill Jedi Knight Anakin with either Sith Marauder or HK-47.  
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC", counter: "NEST_SOLO", hard: false, description: 
"""
Only top-modded Enfys Nest is viable.  Highly recommended to have a Qi'ra lead (for protection regeneration) and Hermit Yoda (for the Master's Training buffs).  She'll survive the hits and recover protection through counters.  The counters won't hurt Padmé, but Enfys Nest will eventually stack so much crit damage that she'll kill them in her turn (unlike with other teams where Nest kills them on the counter attack).  High tenacity is a must because getting stunned stops the strategy.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC", counter: "CLONES_SHAAKTI", hard: false, description: 
"""
Must use ARC Trooper and have equal or greater gear!  Either focus on General Kenobi until CT-7567 "Rex"'s Aerial Advantage is ready to use; then use it on Padmé Amidala, or focus all on Padmé and keep dispelling GK.
""", video: #""#),

Counter(team: "GEONOSIANS", counter: "RESISTANCE_FINN", hard: true, description: 
"""
Finn's leadership zeta is required.  Poe Dameron needs to be extremely fast, so he can use his Resistance Bravado taunt as soon as possible, which will put exposes on the whole team.  Then have Amilyn Holdo or Veteran Smuggler Han use an AoE attack, Holdo is preferred because hers can Daze.  You'll want to keep trying to reduce Poe's taunt by hitting any Exposes the enemy has on them, which will feed turn meter to your team and allow you to keep doing this Expose then AoE tactic.
""", video: #"https://www.youtube.com/watch?v=QV6fIqTdZgE&t=601s"#),

Counter(team: "GEONOSIANS", counter: "RESISTANCE_HEROES", hard: true, description: "", video: #""#),

Counter(team: "GEONOSIANS", counter: "SITH_TRI", hard: true, description: 
"""
Darth Traya leader ability will make them kill themselves, so let them get their assists in and kill them when they are low enough to kill.  It helps if Darth Sion and Darth Traya have tanky mod builds.  Can bring a Shoretrooper to slow them down with crit immunity for short time and another target to attack.
""", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "GEONOSIANS", counter: "FO_KRU_ST", hard: true, description: 
"""
Sith Trooper will gain a bonus turn and extra damage with Vengeant Blast whenever a FO ally is defeated.  This is not an ideal counter, as it relies on characters dying, which lowers the amount of banners you'll receive.
""", video: #""#),

Counter(team: "GEONOSIANS", counter: "SEP_DROIDS", hard: true, description: 
"""
Clear the taunt from Geonosian Brute using B2 Super Battle Droid's Mow Down ability.  Then focus on killing Geonosian Brood Alpha first, to stop the health and protection buff and respawning of Brute.  After Brood Alpha is dead you should be able to take the rest out in any order, although Geonosian Spy would be the next to worry about if you're worried about getting the win.
""", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "GEONOSIANS", counter: "DROIDS_ANTI_MALAK", hard: true, description: "", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "GEONOSIANS", counter: "GALACTIC_REPUBLIC", hard: false, description: 
"""
Their attacks give you more damage.  Have a few slow characters that can build up Courage then one-shot (Clone Wars Chewbacca, Barriss).  Shouldn't be too much of a problem if you have a high gear zzPadmé team.
""", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "GEONOSIANS", counter: "SEP_NUTE", hard: false, description: 
"""
Nute Gunray's Extortion works well against this team if you have Count Dooku stunning and shocking the Geonosians.  They won't be able to gain buffs and will keep spreading extortion, which will gradually increase your power against them.
""", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "GEONOSIANS", counter: "EP", hard: false, description: 
"""
Substitute your tanks for characters with daze or debuff (e.g. Wampa, Darth Maul).  You want a speed advantage (you need Vader to go first and feed turn meter to all), high potency (Geonosian Brood Alpha has high tenacity), and daze to shut down their assists.
""", video: #""#),

Counter(team: "GEONOSIANS", counter: "JAWAS", hard: false, description: 
"""
Jawa Scavenger's Line in the Sand ability is the main mechanic of this counter.  Mod your Jawas for survivability (e.g. Protection, Health, Crit Avoidance).  You want the Jawas to be slow and tanky and you want Jawa Scavenger stay alive as long as possible, since his unique ability has a 50% chance of placing a thermal detonator on an attacker.  This is very detrimental to the Geonosians since their strategy is based around assists.  Don't use Jawa's AoE stun attack, because this would prevent the stunned Geos from attacking you and picking up more thermal detonators.
""", video: #""#),

Counter(team: "GEONOSIANS", counter: "NS_MT", hard: false, description: 
"""
This is a tough fight, which has a high chance of timing out.  You want both zetas on Mother Talzin and Asajj's Rampage zeta.  You need the revive and potency from Mother Talzin's lead.  You'll die a lot, but you can chip away at their HP until you build enough damage to get kills.  Plague doesn't stick often, but keep trying since it will help you drain their HP.  Consider subbing in Enfys Nest.
""", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "GEONOSIANS", counter: "JEDI_REVAN", hard: false, description: 
"""
Exercise caution with this matchup!  The Geonosian rework has made this a very unreliable counter since the Geos can easily remove all of the buffs that this team needs to function correctly.  Use Jedi Knight Revan's Direct Focus to focus all initial attacks on Geonosian Brood Alpha.  This will stop major healing and buffs and the summoned taunter.  After GBA is killed, focus on Geonosian Spy next to avoid his one-shot kill.  Call Grand Master Yoda for as many assists as possible so he can gain turn meter and spread buffs and foresight to the rest of the team.
""", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "GEONOSIANS", counter: "BH_JANGO", hard: false, description: "", video: #""#),

Counter(team: "GEONOSIANS", counter: "JTR", hard: false, description: 
"""
Keep in mind that Geonosians are a hard counter to JTR.  If they are faster than you, it'll be tough to gain enough momentum to spread Exposes and defeat them.  That being said, you can beat similarly geared Geo teams if your speed and crit chance are high enough.  R2-D2's Combat Analysis will debuff light side characters on crit hit, and Rey (Jedi Training)'s leadership has a chance to inflict Expose on a crit hit, along with other benefits.
""", video: #""#),

Counter(team: "GK", counter: "REX_WAMPA", hard: false, description: 
"""
Requires the use of Darth Nihilus!  Use Nihilus' Annihilate on Barriss Offee, the rest should be easy.  You could also bring in Imperial Probe Droid to do the job.
""", video: #""#),

Counter(team: "GK", counter: "WAMPA_SOLO", hard: false, description: 
"""
You need good timing on the hits.  Boba Fett and Wampa can deal big damage and even kill a full HP General Kenobi.
""", video: #""#),

Counter(team: "GK", counter: "ZADER", hard: false, description: 
"""
Stacking dots and dealing big damage at once.
""", video: #""#),

Counter(team: "IT", counter: "EWOKS", hard: true, description: "", video: #""#),

Counter(team: "IT", counter: "CLONES_SHAAKTI", hard: true, description: "", video: #"https://www.youtube.com/watch?v=QhwRfO6Wjl0&t=255s"#),

Counter(team: "IT", counter: "JTR", hard: true, description: "", video: #""#),

Counter(team: "IT", counter: "REBELS_CLS_CHAZE", hard: true, description: 
"""
Let the troopers kill themselves from counterattacks.  If you don't have Chaze (Chirrut Îmwe + Baze Malbus), you'll want taunting tanks that can slow the enemy down, like Obi-Wan Kenobi (Old Ben).
""", video: #""#),

Counter(team: "IT", counter: "GALACTIC_REPUBLIC", hard: true, description: "", video: #""#),

Counter(team: "IT", counter: "SITH_TRI", hard: true, description: "", video: #""#),

Counter(team: "IT", counter: "NEST_SOLO", hard: false, description: "", video: #""#),

Counter(team: "IT", counter: "REX_WAMPA", hard: false, description: 
"""
Swap Wampa, Mother Talzin, or Darth Nihilus for a tank and let the other two do the work.
""", video: #""#),

Counter(team: "IT", counter: "SITH_MAUL", hard: false, description: "", video: #""#),

Counter(team: "IT", counter: "IT", hard: false, description: 
"""
Do this mirror match when you feel you have the upper hand on getting the first kill (turn meter will be on your side).  Concentrate on one toon (not Shoretrooper), and if you get the first kill, you'll win.
""", video: #""#),

Counter(team: "JEDI_BASTILA", counter: "JTR", hard: true, description: 
"""
Having a tank helps (Hold/L3-37).  Work slowly towards killing them all using crits, as exposes can't be resisted.
""", video: #""#),

Counter(team: "JEDI_BASTILA", counter: "SITH_TRI", hard: true, description: "", video: #""#),

Counter(team: "JEDI_BASTILA", counter: "CLONES_SHAAKTI", hard: true, description: "", video: #"https://www.youtube.com/watch?v=QhwRfO6Wjl0&t=255s"#),

Counter(team: "JEDI_BASTILA", counter: "GEONOSIANS", hard: true, description: 
"""
Avoid this counter if they have Jedi Knight Anakin!  Both zetas on Geonosian Brood Alpha are recommended, but not required.  Kill Jolee Bindo and Grand Master Yoda and the team falls apart pretty quickly.
""", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "JEDI_BASTILA", counter: "QIRA_NEST_HODA", hard: true, description: 
"""
These three characters should be enough to win against most Bastila Shan teams.  Enfys Nest counter-attacks, ignores taunts, and recovers protection under Qi'ra lead.  Hermit Yoda will heal, grant foresight, bonus 25% offense, and tenacity (which is crucial).
""", video: #""#),

Counter(team: "JEDI_BASTILA", counter: "NEST_SOLO", hard: false, description: 
"""
You'll want to add Hermit Yoda if facing Jedi Knight Anakin.
""", video: #""#),

Counter(team: "JEDI_BASTILA", counter: "NS_ASAJJ", hard: false, description: 
"""
The turn meter reduction on Asajj's lead will help you here.  Nightsister Spirit can help keep Jolee Bindo supressed with stuns and can clear the taunt from General Kenobi.
""", video: #""#),

Counter(team: "JEDI_BASTILA", counter: "NS_MT_NA", hard: false, description: 
"""
Mother Talzin Leadership zeta recommended.  Mother Talzin's Plague will slowly eat through the team while Nightsister Zombie soaks up damage and Nightsister Acolyte constantly attacks while stealthed.
""", video: #""#),

Counter(team: "JEDI_BASTILA", counter: "BH_BOSSK", hard: false, description: 
"""
Soak up damage with Bossk's taunt.  Use Boba Fett's Execute on Grand Master Yoda to stop the buff train.
""", video: #"https://youtu.be/Ouxex2p1ckE?list=WL&t=78"#),

Counter(team: "JEDI_BASTILA", counter: "REBELS_CLS", hard: false, description: "", video: #""#),

Counter(team: "JEDI_BASTILA", counter: "FO_KRU", hard: false, description: 
"""
Soak up damage with taunts from Kylo Ren (Unmasked) and First Order Stormtrooper.  Use Kylo Ren (Unmasked) stun on Grand Master Yoda to stop the buffs and slow him down.
""", video: #""#),

Counter(team: "JEDI_BASTILA", counter: "IT_THRAWN", hard: false, description: 
"""
Soak up damage with Shoretrooper.  Death Trooper's dispel is useful to control the buffs and his Deathmark helps beat the clock.
""", video: #""#),

Counter(team: "JEDI_REVAN", counter: "SITH_EMPIRE_W_MALAK", hard: true, description: 
"""
This counter can beat a mid-relic Jedi (Revan L) team without any relics.  Focus your attacks on General Kenobi until you shock or ability block him (try to save Bastila Shan (Fallen)'s Wild Lightning to use on someone already Shocked, to hit the entire team).  Once Kenobi's taunt is disabled, focus attacks on Grand Master Yoda (or the weakest enemy) to trigger Jedi Knight Revan's auto-revive.  Next, you'll focus all attacks on Jolee Bindo, to prevent him from reviving any Jedi.  Kill the rest in any order.
""", video: #""#),

Counter(team: "JEDI_REVAN", counter: "GALACTIC_REPUBLIC", hard: true, description: 
"""
Padmé and Barriss Offee's zetas are required for sustainability, Ahsoka Tano's zeta is highly suggested.  When using Jedi Knight Anakin's AoE attack, first target a character you want to dispel (i.e. General Kenobi's taunt) since Ahsoka Tano will assist (if she has her zeta ability).  Other than that, follow the general strategy.
""", video: #""#),

Counter(team: "JEDI_REVAN", counter: "SEP_DROIDS", hard: true, description: 
"""
Focus on the easiest Jedi to kill (usually Grand Master Yoda) to proc Jedi Knight Revan's Savior ability.  Then focus on Jolee Bindo to prevent him from reviving anyone.  Use B2 Super Battle Droid's Mow Down ability as much as possible to debuff and apply ability blocks.  Use B1 Battle Droid's Blast Them either on B2 or IG-100 MagnaGuard, depending on who needs the health and protection up.
""", video: #""#),

Counter(team: "JEDI_REVAN", counter: "JEDI_REVAN_HODA", hard: false, description: "", video: #""#),

Counter(team: "JEDI_REVAN", counter: "CLONES_SHAAKTI", hard: false, description: "", video: #""#),

Counter(team: "JEDI_REVAN", counter: "GEONOSIANS", hard: false, description: 
"""
Geonosian Brood Alpha's zeta is required.  The Geonosians need to be equally geared with the Jedi Knight Revan team so that they have the synergy to heal and strip buffs, and the damage output to finish the Jedi off before getting picked off by the mass assists.  With the proper gear level, the Geos are able to keep the taunt off of General Kenobi so that they can first focus on killing Grand Master Yoda to trigger the auto-revive, then focus on Jolee Bindo to prevent him reviving any other Jedi.
""", video: #""#),

Counter(team: "JEDI_REVAN", counter: "SITH_TRI", hard: false, description: 
"""
Option A which requires very fast Grand Admiral Thrawn: Sub Sith Empire Trooper for a Grand Moff Tarkin lead and outspeed them.  Use Darth Traya's Isolate then Thrawn's Fracture on Jedi Knight Revan.  Option B: Sub Sith Empire Trooper for Bastila Shan (Fallen), make Traya super tanky (lots of protection & crit avoidance arrow mod).  You'll survive the initial attack and slowly have Darth Nihilus Annihilate everyone.  In this case, Fracture Grand Master Yoda to stop the buffs and big damge, because Revan will get his turns anyways.  You want your Bastila Shan (Fallen) slower than your Darth Sion so she can apply debuffs after he uses his dispel.
""", video: #""#),

Counter(team: "JEDI_REVAN", counter: "NS_MT", hard: false, description: 
"""
Zeta on Asajj Ventress' Rampage and Mother Talzin's leadership ability and G10+ Nightsister Zombie required.  Barriss Offee with zeta is an optional 5th character.  The Jedi will keep attacking and you'll keep reviving.  Kill Yoda first to trigger the instant revive, then kill Jolee, then kill the rest in any order.
""", video: #""#),

Counter(team: "JEDI_REVAN", counter: "JTR", hard: false, description: 
"""
C-3PO substitution is highly recommended.  Apply healing immunity with Rey (Jedi Training) on Yoda and kill him.  This should trigger the instant revive, but he will not regain his health and protection.  Kill Jolee after the instant revive is done, then kill the rest in any order.
""", video: #""#),

Counter(team: "JEDI_REVAN", counter: "REBELS_CLS_THRAWN", hard: false, description: 
"""
This is a tough match.  Your Commander Luke Skywalker must be faster than their Jedi Knight Revan.  Shoot Jolee with Han, hit General Kenobi with CLS.  Use Han's buff while targeting Grand Master Yoda, so Chewbacca's assist hits him.  Stun GK with Chewie.  Use C-3PO's mass assist on Yoda (target your Thrawn so he gets translation).  Now either Thrawn or Revan will go next, and if Revan marks Thrawn or Chewie it'll be hard to win.  Fracture Yoda with Thrawn (which should trigger the instant revive).  Now kill Jolee then the rest in any order.
""", video: #""#),

Counter(team: "JEDI_REVAN", counter: "EP_TRIO", hard: false, description: 
"""
Use Grand Admiral Thrawn's Fracture on Jedi Knight Revan, then kill Yoda twice to trigger the instant revive and prevent the tenacity up, then kill Revan next.  Save Darth Nihilus' Annihilate for Jolee Bindo.
""", video: #""#),

Counter(team: "JTR", counter: "NS_MT", hard: true, description: 
"""
Use Talia.  Be careful of the daze if Amilyn Holdo is present.  Don't waste Mother Talzin's AoE when foresight is active.  Plague will play a huge role in winning.
""", video: #""#),

Counter(team: "JTR", counter: "SITH_TRI", hard: true, description: "", video: #""#),

Counter(team: "JTR", counter: "GEONOSIANS", hard: true, description: "", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "JTR", counter: "IT_THRAWN", hard: false, description: 
"""
Grand Admiral Thrawn's Fracture and Shoretrooper's crit immunity will significantly slow down the turn meter gains.  You could make Mother Talzin the 5th to help whittle down the health with Plague.
""", video: #""#),

Counter(team: "JTR", counter: "OR_CARTH", hard: false, description: 
"""
Do not use this counter if they have C-3PO, as they will likely be too fast.  It helps to have high potency, as JTR squads have low tenacity, therefore the damage over time wears them down while healing you.  Focus all attacks on Rey (Jedi Training), whenever possible.  Next, take out Rey (Scavenger).  The rest should be easy.
""", video: #""#),

Counter(team: "JTR", counter: "FO_KRU", hard: false, description: 
"""
Exposes do much less damage to the Kylos.  Kylo Ren (Unmasked) stuns and First Order Officer and First Order Executioner's turn meter reduction cripple Rey (Jedi Training) and BB-8.
""", video: #""#),

Counter(team: "JTR", counter: "REX", hard: false, description: 
"""
Can use Mother Talzin; or Wampa + General Kenobi; or Nightsister Acolyte + Nightsister Zombie.  Rex's lead turns the Resistance team's multiple crits against them.
""", video: #""#),

Counter(team: "JTR_DROIDS", counter: "SITH_MAUL", hard: false, description: 
"""
You want to make sure to have a fast Sith Assassin to grant your team turn meter and apply daze to the opponent before they start their turn meter train and kill all of you.  It may also help to substitute in a non-Sith character that can soak up some damage since they won't be hidden by Darth Maul's leadership.  (e.g. Enfys Nest, Old Ben, or Nightsister Zombie + Nightsister Acolyte)
""", video: #""#),

Counter(team: "NEST_SOLO", counter: "SITH_TRI", hard: true, description: 
"""
Use Darth Nihilus to Annihilate Enfys Nest.
""", video: #"https://forums.galaxy-of-heroes.starwars.ea.com/discussion/214412/heres-how-you-beat-nest"#),

Counter(team: "NEST_SOLO", counter: "JEDI_REVAN", hard: true, description: 
"""
Jedi Knight Revan's leadership zeta is required.  Hit Enfys Nest with Jedi basics while she has bonus protection, and JKR's leadership will keep reducing her max health by 10% until you can eventually one-shot her.
""", video: #"https://forums.galaxy-of-heroes.starwars.ea.com/discussion/214412/heres-how-you-beat-nest"#),

Counter(team: "NEST_SOLO", counter: "JTR", hard: true, description: 
"""
Don't try this team if Enfys Nest is paired with Nightsisters, as JTR is not a good counter for them.  You'll want Rey (Jedi Training), BB-8, and R2-D2, at least, the other two shouldn't matter (other than C-3PO).  For the most part, you want to only use basic attacks on her since BB-8 gives Tenacity Down on basic, R2-D2 stuns on basic, and JTR either reduces turn meter or provides an assist on attacks other than basic.  The only exception is when you can hit her with a big hit (e.g. Rey (Scavenger)'s Flurry of Blows) right after her turn, when she doesn't have Bonus Protection.
""", video: #"https://forums.galaxy-of-heroes.starwars.ea.com/discussion/214412/heres-how-you-beat-nest"#),

Counter(team: "NEST_SOLO", counter: "NS_MT", hard: true, description: 
"""
Don't try this team if Enfys Nest is paired with a fast Bounty Hunter or Old Republic (Carth L) team  - the AoEs will overwhelm you.  Highly recommended to use Talia as a 5th squad member, as she can cleanse Nest's AoE Daze.  Old Daka and Asajj Ventress both stun on basic, and Nightsister Zombie gives Tenacity Down.  Under Mother Talzin's lead, all specials will apply Plague, which ignores Protection and deals 5% HP damage.
""", video: #"https://forums.galaxy-of-heroes.starwars.ea.com/discussion/214412/heres-how-you-beat-nest"#),

Counter(team: "NEST_SOLO", counter: "SITH_MAUL", hard: true, description: 
"""
While Sith Assassin is under stealth, attack Enfys Nest with Electrocute to ignore protection.  When Nest is below 50% health, use Savage Opress's Overpower to finish her off.
""", video: #"https://forums.galaxy-of-heroes.starwars.ea.com/discussion/214412/heres-how-you-beat-nest"#),

Counter(team: "NEST_SOLO", counter: "EP", hard: true, description: 
"""
Apply as many debuffs as possible with Darth Vader and Grand Moff Tarkin.  Then after she takes a turn, use a big hit like Vader's Culling Blade, Tarkin's Ultimate Firepower (if he has Potency Up), or Imperial Probe Droid's Self-Destruct.
""", video: #""#),

Counter(team: "NEST_SOLO", counter: "VISAS_MARR", hard: false, description: 
"""
Consider adding Visas Marr to a team, as her Piercing Strike ignores Protection.
""", video: #"https://forums.galaxy-of-heroes.starwars.ea.com/discussion/214412/heres-how-you-beat-nest"#),

Counter(team: "NEST_SOLO", counter: "JANGO_FETT", hard: false, description: 
"""
Consider using Jango Fett in a Bounty Hunter team.  When Enfys Nest is debuffed, use Jango's Conflagaration, which will drop her max HP by 25%.  You should be able to do this one more time, and she'll be easy to kill.
""", video: #"https://forums.galaxy-of-heroes.starwars.ea.com/discussion/214412/heres-how-you-beat-nest"#),

Counter(team: "NS_MT", counter: "REBELS_CLS", hard: true, description: "", video: #""#),

Counter(team: "NS_MT", counter: "CLONES_SHAAKTI", hard: true, description: 
"""
Shaak Ti's zetas both required, the consistent healing will lessen the effects of Mother Talzin's Plague.  To make this an easier fight, sub in ARC Trooper and put his turret and Shaak Ti's ally taunt ability on CT-5555 "Fives", to constantly shut down Nightsister Zombie's taunt.
""", video: #""#),

Counter(team: "NS_MT", counter: "SITH_TRI", hard: true, description: "", video: #""#),

Counter(team: "NS_MT", counter: "JEDI_REVAN", hard: true, description: 
"""
Make sure you use Jedi Knight Revan's Direct Focus wisely: try not to use it on Nightsister Zombie or a Nightsister that is close to death and will auto-revive, and possibly use it to get around Nightsister Zombie's taunt. Also, consider saving Revan's Master Strategist to heal up the team if your health drops too low due to Mother Talzin's Plague.
""", video: #""#),

Counter(team: "NS_MT", counter: "JEDI_REVAN_HODA", hard: true, description: 
"""
Follow the general kill order. Make sure you use Jedi Knight Revan's Direct Focus wisely: try not to use it on Nightsister Zombie or a Nightsister that is close to death and will auto-revive, and possibly use it to get around Nightsister Zombie's taunt. Also, consider saving Revan's Master Strategist or Hermit Yoda's Strength Flows From the Force to heal up the team if your health drops too low due to Mother Talzin's Plague.
""", video: #""#),

Counter(team: "NS_MT", counter: "GALACTIC_REPUBLIC", hard: true, description: "", video: #""#),

Counter(team: "NS_MT", counter: "GEONOSIANS", hard: true, description: "", video: #""#),

Counter(team: "NS_MT", counter: "EP", hard: true, description: 
"""
Recommended to substitute in Wampa for his daze.  Darth Vader should go first, daze with Wampa to block turn meter gain, kill Old Daka first.  Use Emperor Palpatine's Shock to prevent Nightsister Zombie's taunt.  EP lead gives a lot of healing and turn meter for him and Darth Vader, and has lots of AoEs to work around Nightsister Zombie.  But you must ensure Emperor Palpatine is modded for potency.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_MT", counter: "IT_THRAWN", hard: true, description: 
"""
Grand Admiral Thrawn's leadership zeta is required.  Don't use Magmatrooper against Enfys Nest.  Use Thrawn's Fracture against Nightsister Zombie to stop the taunt.  Try to kill one enemy with Deathtrooper's Terminate, since they can't revive, and then use Deathmark to finish the rest.  Shoretrooper's cleanse helps remove Plague.  Director Krennic is a good choice because he fuel's Death Trooper's turn meter.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_MT", counter: "BH_JANGO", hard: true, description: 
"""
Use Boba Fett's Execute to prevent revives.  Also, if Jango kills a character, they can't revive (other than Nightsister Zombie, of course).
""", video: #""#),

Counter(team: "NS_MT", counter: "BH_BOSSK", hard: true, description: 
"""
Both of Bossk's zetas and Jango's Notorious Reputation zeta are required.  Bossk will soak up damage, Boba and Jango will kill enemies so there are no revives.  Heal back the damage you take by inflicting debuffs.  It would be best to mod your team for tenacity to protect yourself from stuns from Old Daka and Asajj Ventress.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_MT", counter: "JEDI_BASTILA", hard: true, description: 
"""
Use General Kenobi, Jolee Bindo, Obi-Wan Kenobi (Old Ben), and Grand Master Yoda.  This team will provide so much healing and taunting that the Nightsisters won't be able to get through Old Ben or General Kenobi before the revives.  If they are using Nightsister Acolyte, you can use Hermit Yoda, Ezra Bridger, Jedi Knight Anakin, and Ahsoka Tano.  JKA's AOE helps deal damage around the taunt, and Ahsoka/Ezra dispel the taunt.
""", video: #""#),

Counter(team: "NS_MT", counter: "SEP_DROIDS", hard: true, description: 
"""
General Grievous's leadership zeta and B2 Battle Droid's zeta are required.  High tenacity across the whole team will help from the high amount of stuns.  Use B2 to dispel Nightsister Zombie's taunt, then kill Daka first.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_MT", counter: "DROIDS_ANTI_MALAK", hard: true, description: 
"""
A fast BB-8 is required.  Use mass attacks, so the Nightsisters are overwhelmed with debuffs.  With a Relic 7 General Grievous, this team can punch up ~20k power, even with other droids around Gear 9.
""", video: #""#),

Counter(team: "NS_MT", counter: "FO_KRU", hard: true, description: 
"""
The Kylos take reduced damage from Plague.  Kylo Ren (Unmasked)'s leadership can heal the Plague, if zeta'd.  Nightsisters have no anti-crit mechanics and no way to stop the turn meter train.
""", video: #""#),

Counter(team: "NS_MT", counter: "NS_ASAJJ", hard: true, description: 
"""
Using Asajj Ventress lead should make you a bit faster if you're equal strength, due to the turn meter reduction.  When Mother Talzin uses her AoE, your squad will drop below 100% HP, which also give turn meter.  This should give you plenty of time to heal from Talzin's Plague.
""", video: #""#),

Counter(team: "NS_MT", counter: "IT", hard: false, description: 
"""
You need a strong, well-modded team.  Must be extremely careful against Barriss Offee if she has a zeta.  Troopers gain more from deaths than Old Daka or Asajj Ventress.  You need fast kills and a lot of them.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_MT", counter: "PHOENIX", hard: false, description: 
"""
Gear 12 and zeta on Kanan Jarrus and Sabine Wren required.  Kanan's cleanse will be useful to get rid of Mother Talzin's Plague.  Kill Old Daka 3 times to stop the initial revives, then work on the other characters.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_MT", counter: "SITH_MAUL", hard: false, description: 
"""
Zeta on Savage Opress required.  Savage's zeta is needed for the heal over time.  Darth Maul benefits from enemy death.  Additional character substitutions could include: B2 Super Battle Droid, Darth Sidious, Enfys Nest, Wampa (unreliable against G11+ NS teams)
""", video: #""#),

Counter(team: "NS_MT", counter: "EWOKS", hard: false, description: 
"""
Chief Chirpa and Wicket's zetas and very high gear levels are required.  You want to protect Ewok Elder, like calling him as Wicket's assist will give him stealth.  Control Asajj Ventress' turn meter as much as you can, since her AOE dispel is a strong counter to Ewoks.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_NEST", counter: "SITH_TRI", hard: true, description: "", video: #""#),

Counter(team: "NS_NEST", counter: "IT_THRAWN", hard: true, description: 
"""
Grand Admiral Thrawn's leadership zeta is required.  Don't use Magmatrooper against Enfys Nest.  Use Thrawn's Fracture against Nightsister Zombie to stop the taunt.  Try to kill one enemy with Deathtrooper's Terminate, since they can't revive, and then use Deathmark to finish the rest.  Shoretrooper's cleanse helps remove Plague.  Director Krennic is a good choice because he fuel's Death Trooper's turn meter.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_NEST", counter: "EP", hard: true, description: 
"""
Recommended to substitute in Wampa for his daze.  Darth Vader should go first, daze with Wampa to block turn meter gain, kill Old Daka first, kill Enfys Nest last.  Use Emperor Palpatine's Shock to prevent Nightsister Zombie's taunt.  EP lead gives a lot of healing and turn meter for him and Darth Vader, and has lots of AoEs to work around Nightsister Zombie.  But you must ensure Emperor Palpatine is modded for potency.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_NEST", counter: "BH_BOSSK", hard: true, description: 
"""
Recommended to use Bossk, Boba Fett, Jango Fett, Dengar, and Zam Wesell.  Both of Bossk's zetas and Jango's Notorious Reputation zeta are required.  Bossk will soak up damage, Boba and Jango will kill enemies so there are no revives.  Heal back the damage you take by inflicting debuffs.  It would be best to mod your team for tenacity to protect yourself from stuns from Old Daka, Asajj Ventress, and Enfys Nest.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_NEST", counter: "BH_JANGO", hard: true, description: 
"""
Use Boba Fett's Execute to prevent revives.  Also, if Jango kills a character, they can't revive (other than Nightsister Zombie, of course).  Leave Enfys Nest for last.
""", video: #""#),

Counter(team: "NS_NEST", counter: "JEDI_BASTILA", hard: true, description: 
"""
Use General Kenobi, Jolee Bindo, Obi-Wan Kenobi (Old Ben), and Grand Master Yoda.  This team will provide so much healing and taunting that the Nightsisters won't be able to get through Old Ben or General Kenobi before the revives.  If they are using Nightsister Acolyte, you can use Hermit Yoda, Ezra Bridger, Jedi Knight Anakin, and Ahsoka Tano.  JKA's AOE helps deal damage around the taunt, and Ahsoka/Ezra dispel the taunt.
""", video: #""#),

Counter(team: "NS_NEST", counter: "SEP_DROIDS", hard: true, description: 
"""
General Grievous's leadership zeta and B2 Battle Droid's zeta are required.  High tenacity across the whole team will help from the high amount of stuns.  Use B2 to dispel Nightsister Zombie's taunt, then kill Daka first.  Save Enfys Nest for last.
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_NEST", counter: "IT", hard: false, description: 
"""
You need a strong, well-modded team.  Must be extremely careful against Enfys Nest or Barriss Offee if they have a zeta.  Troopers gain more from deaths than Old Daka or Asajj Ventress.  You need fast kills and a lot of them.  
""", video: #"https://www.youtube.com/watch?v=R1OuAWmgQZc&t=61s"#),

Counter(team: "NS_NEST", counter: "REX_WAMPA", hard: false, description: "", video: #""#),

Counter(team: "OR_CARTH", counter: "FO_KRU", hard: true, description: 
"""
Kylo Ren (Unmasked) and Kylo Ren benefit from the damage over times.  First Order Officer can give tenacity up to himself and another character, which also helps with damage over time.  Dispel Zaalbar's taunt, and just attack a bunch to get the turn meter train rolling.  To help work around the crit avoidance, keep Advantage on your team with Kylo Ren's Lash Out, First Order Executioner's basic against debuffed enemies, First Order Stormtrooper's Return Fire if zeta'd.  Could also sub in Captain Phasma in place of First Order Stormtrooper to focus the enemy attacks on KRU which will spread Advantage to the team (if KRU's Scarred is zeta'd) and for Phasma's Victory March.  Using Phasma makes it harder if going against Enfys Nest or Wampa, so keep them stunned with KRU.
""", video: #"https://www.youtube.com/watch?v=D3jiJ3Cn2fQ"#),

Counter(team: "OR_CARTH", counter: "JTR", hard: true, description: 
"""
You need zetas on Rey (Jedi Training)'s leadership, and both of R2-D2 and BB-8's zetas.  Recommended to use Amilyn Holdo in place for Resistance Trooper, C-3PO, or Poe Dameron, her AOE daze prevents enemy counters and assists).  JTR's healing immunity will help prevent Zaalbar's taunt from constantly activating due to the team's constant healing.  Kill Canderous Ordo first, Carth Onasi next, then finish the rest.
""", video: #"https://www.youtube.com/watch?v=D3jiJ3Cn2fQ"#),

Counter(team: "OR_CARTH", counter: "PHOENIX", hard: true, description: 
"""
Kanan Jarrus's zeta is required.
""", video: #""#),

Counter(team: "OR_CARTH", counter: "MALAK_SOLO", hard: true, description: 
"""
Relic and at least one more character highly recommended.
""", video: #""#),

Counter(team: "OR_CARTH", counter: "EP", hard: true, description: 
"""
Emperor Palpatines leadership zeta is required.  You want to keep this team debuffed with Darth Vader and Grand Moff Tarkin, which will keep your turn meter train rolling.
""", video: #""#),

Counter(team: "OR_CARTH", counter: "SITH_TRI", hard: true, description: 
"""
You can 3v5 this team with only Darth Traya, Darth Sion, and Darth Nihilus.  You'll need both of Darth Traya's zetas and Darth Sion's Lord of Pain zeta.  You'll want to use Traya's Isolate on Zaalbar so that he doesn't taunt, then you'll use Darth Nihilus's annihilate to pick the rest of them off.
""", video: #"https://www.youtube.com/watch?v=D3jiJ3Cn2fQ"#),

Counter(team: "OR_CARTH", counter: "GALACTIC_REPUBLIC", hard: true, description: 
"""
You can 3v5 this team with only Padmé Amidala, Jedi Knight Anakin, and Ahsoka Tano.  You need both of Padmé's zetas and a zeta on Ahsoka's Daring Padawan.  Because of all of the assists, you'll gain a lot of protection up.  Dispel Zaalbar's taunt with Ahsoka's basic, and apply Buff and Healing immunity to Canderous Ordo with Anakin's basic.  Kill Canderous first, then Carth, then kill the rest.
""", video: #"https://www.youtube.com/watch?v=D3jiJ3Cn2fQ"#),

Counter(team: "OR_CARTH", counter: "REBELS_CLS", hard: true, description: 
"""
You can 3v5 this team with only Commander Luke Skywalker, Han Solo, and Chewbacca.  However, for a more assured win, you should add a fourth, like Scarif Rebel Pathfinder or C-3PO.  Use CLS's Use The Force to dispel Zaalbar's taunt and kill Canderous Ordo first, then Carth Onasi, then the rest.
""", video: #"https://www.youtube.com/watch?v=D3jiJ3Cn2fQ"#),

Counter(team: "OR_CARTH", counter: "JEDI_BASTILA", hard: true, description: 
"""
Bastila Shan's leadership ability provides enough tenacity to completely stop the damage over time at the beginning of the battle.  You want to dispel Zaalbar's taunt and focus on Canderous Ordo first, then Carth Onasi, then the rest.  To work around the crit avoidance, make sure you have high offense (e.g. Grand Master Yoda, Ezra Bridger, Hermit Yoda for his Master's Training)
""", video: #"https://www.youtube.com/watch?v=D3jiJ3Cn2fQ"#),

Counter(team: "OR_CARTH", counter: "REX_WAMPA", hard: true, description: 
"""
Rex's tenacity up will help stop the damage over time, which in turn kills their extra damage and healing.  Wampa is a bit handicapped in this matchup due to the Carth team's crit avoidance, but having Mother Talzin and Darth Nihilus should be enough to overcome that.
""", video: #""#),

Counter(team: "OR_CARTH", counter: "BH_JANGO", hard: true, description: "", video: #""#),

Counter(team: "OR_CARTH", counter: "SEP_DROIDS", hard: true, description: 
"""
Kill Canderous Ordo first, then Carth Onasi, then clean up.  B2 Battle Droid will reliably dispel Zaalbar and Juhani's taunts, and his buff immunity will also help.
""", video: #"https://www.youtube.com/watch?v=D3jiJ3Cn2fQ"#),

Counter(team: "OR_CARTH", counter: "GEONOSIANS", hard: true, description: "", video: #""#),

Counter(team: "OR_CARTH", counter: "IT", hard: false, description: 
"""
The goal here is to spam mass assists and AoEs.  The fight will get much easier after you're able to kill Canderous Ordo first.  Then you'll be able to use Death Trooper's Terminate to make taking out Carth Onasi then Mission Vao very quick.  Then kill the rest.
""", video: #""#),

Counter(team: "OR_CARTH", counter: "NS_MT", hard: false, description: 
"""
Mother Talzin's leadership is recommended for the revives and increased potency.  You won't be able to avoid the damage over time, so this will be a long battle but Talzin's Plague and the stuns will win it for you.  You could also consider adding Grand Admiral Thrawn as the fifth squad member to Fracture Zaalbar and make this a hard counter.
""", video: #"https://www.youtube.com/watch?v=D3jiJ3Cn2fQ"#),

Counter(team: "OR_CARTH", counter: "EWOKS", hard: false, description: 
"""
The recommended team is Chief Chirpa, Wicket, Paploo, Ewok Elder, and Ewok Scout.  You'll need a zeta on Chief Chirpa and Wicket.  Use Paploo to dispel the taunt on Zaalbar, then kill Canderous Ordo, then Mission, then Carth.  You'll want to make sure that your offense is high enough to get through Zaalbar.
""", video: #"https://www.youtube.com/watch?v=D3jiJ3Cn2fQ"#),

Counter(team: "OR_CARTH", counter: "IT_THRAWN", hard: false, description: 
"""
This can be a tough matchup because the team relies on you applying buff immunity to Zaalbar, so you'll want TIE Fighter Pilot as a 5th and he'll need to be modded for potency.  Once Zaalbar has buff immunity, fracture Canderous Ordo so that he doesn't keep applying damage over time debuffs.  Once he's dead, you can use Death Trooper's Deathmark to take the others out.
""", video: #"https://www.youtube.com/watch?v=D3jiJ3Cn2fQ"#),

Counter(team: "PHOENIX", counter: "SEP_NUTE_3v3_JANGODOOKU", hard: true, description: 
"""
This should be an easy 3v5, undersized win.  Use Count Dooku's Master of Makashi to put stealth on Jango Fett, so that Fett can use his Conflagaration AoE without being countered.  Unless Dooku is stealthed or Kanan Jarrus is dead, only use his basic to minimize being countered... the goal is to keep the whole team stunned.  Use Nute Gunray's Dubious Dealings as much as possible to keep Extortion on the team.  Try to use Nute's Motivate on enemies with Extortion, as it will cause the others to assist.  And since Dooku will be attacking out of turn, he'll gain turn meter.
""", video: #""#),

Counter(team: "PHOENIX", counter: "WAMPA_SOLO", hard: true, description: 
"""
Does not counter relics.  A strong Wampa can solo medium-high level Phoenix teams.  Wampa has extra offense against Rebels, inflicts healing immunity and daze, and kills Phoenix teams by counterattacking.  Normal additions include a Rex lead to add health and cleanse Wampa of daze, and Hermit Yoda to supercharge Wampa with Master's Training.  If you need even more help, you could add a Boba Fett lead to increase Wampa's crit chance/damage; a Director Krennic lead with Death Trooper pairs well due to their ability block, extra debuffs, and extra damage to Rebels; or you could include any other character that will cleanse Wampa.
""", video: #""#),

Counter(team: "PHOENIX", counter: "NEST_SOLO", hard: true, description: 
"""
You want your Enfys Nest to be modded with high Tenacity to prevent Daze and Stun.  Take out Sabine Wren first, then Garazeb "Zeb" Orrelios.
""", video: #""#),

Counter(team: "PHOENIX", counter: "FO_HUX_3v3", hard: true, description: 
"""
General Hux required with Bow to the First Order leadership omega ability; you can fill in the rest with First Order leftovers and possibly go for an undersized win.  Hux's leadership omega will prevent Phoenix's counterattacks when he has Dominance, and his Boundless Ambition zeta will prevent their turn meter gain when he has Advantage (which would also mean you wouldn't have to focus on killing Kanan Jarrus to remove counter chance from the team).
""", video: #""#),

Counter(team: "PHOENIX", counter: "IT", hard: true, description: "", video: #""#),

Counter(team: "PHOENIX", counter: "EP", hard: true, description: "", video: #""#),

Counter(team: "PHOENIX", counter: "REBELS_CLS", hard: true, description: "", video: #""#),

Counter(team: "PHOENIX", counter: "QIRA_PREPAREDSMUGGLERS", hard: false, description: 
"""
This team takes some time to ramp up, but if you keep Young Han Solo alive and Prepared, he'll eventually be buffed enough to one-shot everyone.  Since Phoenix doesn't have a lot of AoE attacks, it's easier for Vandor Chewbacca to stay Prepared, and since Young Han doesn't apply debuffs on his Just In Time, it won't trigger Kanan Jarrus's unique (which heals upon receiving debuffs).
""", video: #""#),

Counter(team: "PHOENIX", counter: "EWOKS", hard: false, description: 
"""
Works well since this team doesn't rely on debuffs and has high turn meter gain and assists.  Use Logray's daze to stop assists and counter attacks.  Without Logray, it's also possible to land big blows from stealth using Ewok Scout and Wicket.  Focus on one character at a time to remove the unique ability they are sharing with the team.
""", video: #""#),

Counter(team: "PHOENIX", counter: "SITH_MAUL", hard: false, description: 
"""
Works well since this team has daze all, turn meter gain, good damage, and doesn't rely on debuffs.  Focus on one character at a time to remove the unique ability they are sharing with the team.
""", video: #""#),

Counter(team: "PHOENIX", counter: "CLONES_CODY", hard: false, description: 
"""
Zeta on CC-2224 "Cody" and G11 clones required.  This team can work due to the high amount of assists on one character and turn meter gain. This will not work well against a high level Phoenix team.  Focus on one character at a time to remove the unique ability they are sharing with the team.
""", video: #""#),

Counter(team: "PHOENIX", counter: "JEDI_QGJ", hard: false, description: 
"""
Requires zeta on Qui-Gon Jinn's leadership and G11 Jedi.  This team works off of assists and dispels.  Use Qui-Gon Jinn and Ezra Bridger's dispels to get past the taunts, Jedi Knight Anakin has healing immunity on basic, and the others have assists which help focus on one character at a time to remove the unique ability they are sharing with the team.
""", video: #""#),

Counter(team: "PHOENIX", counter: "REBELS_WIGGS", hard: false, description: 
"""
Works well since this team has very high burst damage.  Consider subbing in Princess Leia, since her stealth stops counterattacks and she has very high multi-attack damage on basic.  Chirrut and Baze keep Wedge, Biggs, and Leia alive so they can use their high damage and assists to take the Phoenix squad out one at a time.
""", video: #""#),

Counter(team: "QIRA", counter: "CLONES_SHAAKTI", hard: true, description: "", video: #""#),

Counter(team: "QIRA", counter: "SITH_TRI", hard: true, description: "", video: #""#),

Counter(team: "QIRA", counter: "JEDI_BASTILA", hard: true, description: "", video: #""#),

Counter(team: "QIRA", counter: "JTR", hard: true, description: 
"""
Subbing in Amilyn Holdo works great due to her AoE daze and tankiness.  You can sub in a droid for Finn, if you need him for another squad.  Use JTR's dispel on L3, then don't hit her again or she'll cleanse the daze.  Kill Vandor Chewbacca first to shut down his revives.
""", video: #"https://www.youtube.com/watch?v=63_XR2xTjfg"#),

Counter(team: "QIRA", counter: "SEP_DROIDS", hard: true, description: "", video: #""#),

Counter(team: "QIRA", counter: "REBELS_CLS_CHAZE", hard: true, description: 
"""
Make sure Commander Luke Skywalker has less HP & Protection than Chirrut Îmwe, so he gets Chewbacca's guarding.  Kill Vandor Chewbacca first, then Mission Vao next.  You can attack around taunts with CLS, and you have a dispel with Baze, Chewie, and CLS.
""", video: #"https://www.youtube.com/watch?v=63_XR2xTjfg"#),

Counter(team: "QIRA", counter: "NS_MT_NA", hard: false, description: 
"""
Requires a zeta Mother Talzin's leadership ability.  Plague will eat through most of the Scoundrels.  Spam Nightsister Acolyte's basic ability, which will keep her stealthed and, if MT is still alive, will have a defeated Nightsister assist and revive on killing an enemy.  Kill Vandor Chewbacca first to shut down his revives.
""", video: #""#),

Counter(team: "QIRA", counter: "BH_BOSSK", hard: false, description: "", video: #""#),

Counter(team: "QIRA", counter: "BH_JANGO", hard: false, description: 
"""
You want Jango Fett's leadership zeta and  You want to fulfill Jango's contract as soon as possible, kill Vandor Chewbacca first, then Mission Vao next.  Bossk soaks up damage, while Zam Wesell and Dengar will land detonators (since L3-37 can't cleanse them).  Use Jango and Boba to hit Vandor Chewbacca.  Once you've killed Vandor Chewie, kill Mission, then L3.  Don't save L3 for last, because it can result in running out of time due to her unique ability giving her +100% defense and Qi'ra's leadership ability giving her +60% crit avoidance. 
""", video: #"https://www.youtube.com/watch?v=63_XR2xTjfg"#),

Counter(team: "QIRA", counter: "EP", hard: false, description: 
"""
If Qi'ra team is faster, sub in Grand Admiral Thrawn.
""", video: #""#),

Counter(team: "QIRA", counter: "FO_KRU", hard: false, description: 
"""
Kylo Ren (Unmasked)'s leadership zeta is mandatory, and First Order Stormtrooper's zeta is recommended.  Sub in Captain Phasma for Kylo Ren, to reduce the usage of an AOE so L3-37 won't cleanse.  The goal is to keep L3 stunned.  Tough matchup, but possible with a high-geared First Order team.  You want to dispel their taunts and kill Vandor Chewbacca first and save L3-37 and Zaalbar for last.
""", video: #"https://www.youtube.com/watch?v=63_XR2xTjfg"#),

Counter(team: "QIRA", counter: "ZADER", hard: false, description: 
"""
Requires subbing in Barriss Offee with zeta.  Darth Vader's leadership ability gives constant damage over time, which helps handle the tanks due to the increased damage.  This team also handles Enyfs Nest well with Vader's Saber Toss and Wampa's increased damage.  Kill Vandor Chewbacca first to shut down his revives.
""", video: #""#),

Counter(team: "QIRA_NEST_ZAALBAR", counter: "JEDI_BASTILA", hard: true, description: "", video: #""#),

Counter(team: "QIRA_NEST_ZAALBAR", counter: "GEONOSIANS", hard: true, description: "", video: #""#),

Counter(team: "QIRA_NEST_ZAALBAR", counter: "SITH_TRI", hard: true, description: "", video: #""#),

Counter(team: "QIRA_NEST_ZAALBAR", counter: "REBELS_CLS", hard: true, description: 
"""
Do not use C-3PO.
""", video: #""#),

Counter(team: "QIRA_NEST_ZAALBAR", counter: "JTR", hard: false, description: 
"""
Try not to use C-3PO, as his turn meter reduction is not ideal against Enfys Nest.  Use Rey (Jedi Training)'s Mind Trick on L3-37 to prevent her taunt.  Attack Vandor Chewbacca until Zaalbar autotaunts.  Use Resistance Trooper's Opportune Strike to dispel Zaalbar's taunt, then kill off Vandor Chewie to prevent his revive mechanic.  Kill Zaalbar second, to prevent constant taunting.  Then Kill off Qi'ra and L3-37.  Then kill Enfys Nest by attacking with basics only.
""", video: #""#),

Counter(team: "QIRA_NEST_ZAALBAR", counter: "BH_BOSSK", hard: false, description: "", video: #""#),

Counter(team: "QIRA_NEST_ZAALBAR", counter: "BH_JANGO", hard: false, description: "", video: #""#),

Counter(team: "QIRA_NEST_ZAALBAR", counter: "EP", hard: false, description: 
"""
If the enemy team is faster, add Grand Admiral Thrawn.
""", video: #""#),

Counter(team: "QIRA_NEST_ZAALBAR", counter: "FO_KRU", hard: false, description: 
"""
Add Grand Admiral Thrawn as a 5th to shut down Zaalbar's taunt and/or Vandor Chewbacca's revive.
""", video: #""#),

Counter(team: "REBELS_CLS", counter: "CLONES_SHAAKTI", hard: true, description: "", video: #""#),

Counter(team: "REBELS_CLS", counter: "JEDI_REVAN", hard: true, description: 
"""
Apply Jedi Knight Revan's Direct Focus to Chewbacca to remove their main damage source.  Avoid using AoE attacks until he's defeated because the attacks on him and the toons he's guarding will decrease his cooldowns.  Then kill Han Solo, then Commander Luke Skywalker, then the rest.
""", video: #""#),

Counter(team: "REBELS_CLS", counter: "JEDI_BASTILA", hard: true, description: 
"""
vs CLS Trio and Quartet:  For non-Revan users, use Grand Master Yoda and Hermit Yoda against CLS Quartet.  Focus on Chewbacca from the start.  Use Hermit Yoda's Master's Training on Grand Master Yoda (or another attacker if GMY is stunned).  This is a very hard match if facing CLS Quartet without Grand Master Yoda.
""", video: #""#),

Counter(team: "REBELS_CLS", counter: "GALACTIC_REPUBLIC", hard: true, description: "", video: #""#),

Counter(team: "REBELS_CLS", counter: "GEONOSIANS", hard: true, description: "", video: #"https://www.youtube.com/watch?v=padGqYGVRoI&t=327s"#),

Counter(team: "REBELS_CLS", counter: "EP_ANTI_REBEL", hard: true, description: 
"""
vs CLS Trio:  Shoretrooper brings crit hit immunity, Death Trooper brings daze and stuns, and Director Krennic brings a lot of damage for him and Death Trooper versus rebels (and a revive option of Death Trooper).
""", video: #""#),

Counter(team: "REBELS_CLS", counter: "SITH_TRI", hard: false, description: "", video: #""#),

Counter(team: "REBELS_CLS", counter: "NS_MT", hard: false, description: 
"""
vs CLS Trio and Quartet:  You can start with Mother Talzin calling a mass attack on Chewbacca.  You can also use Plague on the first turn, which will absolutely get her killed, but it will feed Old Daka's turn meter and Asajj Ventress' damage.
""", video: #""#),

Counter(team: "REBELS_CLS", counter: "REX_WAMPA", hard: false, description: 
"""
vs CLS Trio:  If using Mother Talzin, focus on non-AoE attacks against Chewbacca to minimize his counter attacks.  vs CLS Quartet: use Grand Admiral Thrawn, Chirrut Îmwe, and Baze Malbus.  Like always, focus on Chewbacca first.
""", video: #""#),

Counter(team: "REBELS_CLS", counter: "REX", hard: false, description: 
"""
vs CLS Quartet:  Add Sith Triumvirate (Darth Traya, Darth Sion, and Darth Nihilus) and Grand Admiral Thrawn.  Use Traya's Isolate on Chewbacca, use Grand Admiral Thrawn's Fracture on Han Solo.  Kill Chewbacca first.
""", video: #""#),

Counter(team: "REBELS_CLS", counter: "JTR", hard: false, description: 
"""
vs CLS Trio:  A tank is recommended (General Kenobi/Amilyn Holdo/L3-37).  If using R2-D2, use his stealth as much as possible to minimize the counter attacks.  For JTR's assist attack, always call BB-8 (if he has Rolling with the Punches zeta) because he will call another Resistance ally to assist.  vs CLS Quartet:  You must use a top tier JTR team (i.e. C-3PO, Enfys Nest, Grand Admiral Thrawn, and Amilyn Holdo)
""", video: #""#),

Counter(team: "REBELS_CLS", counter: "SEP_DROIDS", hard: false, description: "", video: #""#),

Counter(team: "REBELS_CLS", counter: "GK", hard: false, description: 
"""
vs CLS Trio:  This team stops crits.  If using Mother Talzin, focus on non-AoE attacks against Chewbacca to minimize his counter attacks.
""", video: #""#),

Counter(team: "REBELS_CLS", counter: "DK_ANTI_REBEL", hard: false, description: 
"""
vs CLS Trio:  Need high gear.  This isn't an Imperial Troopers team, Veers lead won't work here.  Shoretrooper brings crit hit immunity, Death Trooper brings daze and stuns, and Director Krennic brings a lot of damage for him and Death Trooper versus rebels (and a revive option of Death Trooper).
""", video: #""#),

Counter(team: "REY", counter: "FO_SLKR", hard: false, description: "", video: #""#),

Counter(team: "REY", counter: "JEDI_REVAN_REYKILLER", hard: false, description: 
"""
Give Wat Tambor's BlasTech Weapon Mod to General Skywalker to give him turn meter and damage.  Give Wat Tambor's Baktoid Shield Generator to JKR and his Chiewab Medpac to Shaak Ti.<br/><br/>

Give Hermit Yoda's Master's Training to GAS first, then (if possible) JKR, then Shaak Ti.<br/><br/>

The Rey team will kill GAS first, and Jedi Knight Revan's Savior ability will auto-revive him.  Kill L3-37 (or substitute Tank) first.  Use JKR's Direct Focus on Rey and continue focusing attacks on her until she gets damage immunity.  Then try to kill off Wat Tambor (if available) or another ally.<br/><br/>

Your Wat should help you withstand Rey's Ultimate Ability.  After the Ult, use Shaak-Ti's Assault Team to give GAS Stealth, so she doesn't kill him off.  You want GAS to stay alive because of his high damage output and because he'll reduce Rey's max health whenever he uses an ability while she doesn't have protection.
""", video: #""#),

Counter(team: "REY", counter: "JEDI_REVAN_REYKILLER2", hard: false, description: 
"""
Relic 7 on General Skywalker is required!.<br/><br/>

Use Grand Admiral Thrawn's Fracture on Rey.  Give Hermit Yoda's Master's Training to GAS first, which will probably cause him to attack Malak due to his taunt and get Fear.  If GAS has Fear, don't use Bastila Shan's Rally on him yet, or else it will waste an assist attack.  Instead, use Jedi Knight Revan's Master Strategist to swap turn meter with GAS, then Strategic Advantage with GAS to take Malak out.<br/><br/>

Use JKR's Direct Focus on Rey and continue focusing attacks on her until she gets damage immunity.  If timed correctly, by the time Fracture expires, so will her damage immunity.  This should allow GAS to kill her before her Ultimate Ability.  If it doesn't line up correctly, make sure you try to keep Direct Focus on Rey and Hoda's healing ability ready in case GAS's health is low.<br/><br/>

You want GAS to stay alive because of his high damage output and because he'll reduce Rey's max health whenever he uses an ability while she doesn't have protection.  If he dies, you'll more than likely lose.
""", video: #""#),

Counter(team: "REY", counter: "SITH_EMPIRE_REYKILLER", hard: false, description: 
"""
If they have a taunter, give Wat Tambor's BlasTech Weapon Mod to Bastila Shan (Fallen), so she can apply Fear to the team.  Then use Darth Revan's Force Storm to dispel the taunter and apply shock.<br/><br/>

Fracture Rey with Grand Admiral Thrawn.<br/><br/>

Given Wat's Baktoid Shield Generator to Geonosian Brute, to keep a taunt on him while he has protection.  Save Geonosian Brood Alpha's Conscription to revive Brute when he dies (or maybe to heal, depending on the situation).<br/><br/>

Now you should be able to use Revan's Insanity on Rey to apply Fear to the entire team.  Keep Shock on the tank to prevent taunting, keep fear on Rey's allies as much as possible to keep skipping their turns, and attack Rey as much as possible.
""", video: #""#),

Counter(team: "REY_MALAK", counter: "JEDI_REVAN_REYKILLER2", hard: false, description: 
"""
Relic 7 on General Skywalker is required!.<br/><br/>

On GAS's first turn, use his basic ability on Rey so Darth Malak doesn't apply Fear to him at the start of the battle.

Use Grand Admiral Thrawn's Fracture on Rey.  Give Hermit Yoda's Master's Training to GAS first, then (if possible) JKR, then Bastila Shan.  Also, give Bastila Shan's buffs to GAS.<br/><br/>

Use JKR's Direct Focus on Rey and continue focusing attacks on her until she gets damage immunity.  If timed correctly, by the time Fracture expires, so will her damage immunity.  This should allow GAS to kill her before her Ultimate Ability.  If it doesn't line up correctly, make sure you try to keep Direct Focus on Rey and Hoda's healing ability ready in case GAS's health is low.<br/><br/>

You want GAS to stay alive because of his high damage output and because he'll reduce Rey's max health whenever he uses an ability while she doesn't have protection.  If he dies, you'll more than likely lose.
""", video: #""#),

Counter(team: "ROGUE_ONE", counter: "IT", hard: true, description: "", video: #""#),

Counter(team: "ROGUE_ONE", counter: "MALAK_SOLO", hard: true, description: 
"""
Relic highly recommended.
""", video: #""#),

Counter(team: "ROGUE_ONE", counter: "BH_BOSSK", hard: true, description: 
"""
Boba Fett and Bossk are the key to winning this match.  To use Chirrut's unique ability against him, use Bossk's mass assist ability on Chirrut so he gains a lot of buffs/debuffs, then use Boba Fett's Execute.  After Chirrut is dead, the battle is basically won.
""", video: #""#),

Counter(team: "ROGUE_ONE", counter: "WAMPA_SOLO", hard: true, description: 
"""
Additional anti-rebel characters highly recommended.  Director Krennic + Death Trooper can easily win 3v5.  Strong Boba Fett + Wampa can will 2v5.  Optional leaders include: CT-7567 "Rex", Director Krennic, Boba Fett, Grand Moff Tarkin, Darth Sidious.  Optional additions: Death Trooper, Enfys Nest.  Kill Chirrut first.
""", video: #""#),

Counter(team: "ROGUE_ONE", counter: "OR_CARTH", hard: false, description: "", video: #""#),

Counter(team: "ROGUE_ONE", counter: "EP", hard: false, description: "", video: #""#),

Counter(team: "ROGUE_ONE", counter: "NEST_SOLO", hard: false, description: "", video: #""#),

Counter(team: "ROGUE_ONE", counter: "CLONES_CODY", hard: false, description: 
"""
Zeta on CC-2224 "Cody" is required.  Use Echo to dispel taunts. Use Rex's tenacity up.  Use Cody's mass assist to kill Chirrut first.
""", video: #""#),

Counter(team: "ROGUE_ONE", counter: "JEDI_QGJ", hard: false, description: 
"""
Requires zeta on Qui-Gon Jinn's leadership ability.  Use Qui-Gon Jinn and Ezra Bridger to dispel the taunts.  Jedi Knight Anakin's buff immunity should be very useful against Chirrut.  Kill Chirrut first.
""", video: #""#),

Counter(team: "ROGUE_ONE", counter: "BH_JANGO", hard: false, description: 
"""
Use if Bossk is not available.  Boba Fett is the key to winning this match.  To use Chirrut's unique ability against him, attack him a lot so he stacks his heal over time buffs then use Boba Fett's Execute.  After Chirrut is dead, the battle is basically won.
""", video: #""#),

Counter(team: "SEP_DROIDS", counter: "JEDI_REVAN", hard: true, description: 
"""
You want a very strong Jolee Bindo, preferably with Relics.
""", video: #""#),

Counter(team: "SEP_DROIDS", counter: "JEDI_BASTILA_REVAN", hard: true, description: 
"""
You want a very strong Jolee Bindo, preferably with Relics.  Bastila Shan's leadership will give you the necessary tenacity to avoid debuffs.  Kill B2 Battle Droid first, then apply Direct Focus to General Grievous to kill him.
""", video: #""#),

Counter(team: "SEP_DROIDS", counter: "REX_501", hard: false, description: 
"""
You essentially want Rex to stay alive for as long as possible so that he can use Aerial Advantage to one-shot General Grievous.  Use Ahsoka to dispel MagnaGuard's taunt so you can kill B2 Super Battle Droid first.  Work on B1 Battle Droid or Droideka next, making sure you don't attack MagnaGuard and trigger a taunt or knock GG below 100% health, which would cause him to put Deathmark on an ally droid.  Once Rex's Aerial Advantage is available, use it to one-shot GG.  If you made it this far, you've basically won the battle.
""", video: #""#),

Counter(team: "SEP_DROIDS", counter: "CLONES_SHAAKTI", hard: false, description: 
"""
You essentially want Rex to stay alive for as long as possible so that he can use Aerial Advantage to one-shot General Grievous.  With this team, you'll more than likely get stuck behind MagnaGuard and will have to kill him first.  You want to kill B2 Super Battle Droid next.  At this point, Rex's Aerial Advantage should be available to one-shot GG.  If you made it this far, you've basically won the battle.
""", video: #""#),

Counter(team: "SEP_DROIDS", counter: "GALACTIC_REPUBLIC", hard: false, description: "", video: #""#),

Counter(team: "SEP_DROIDS", counter: "EP", hard: false, description: 
"""
You'll need to sub in Bastila Shan (Fallen)
""", video: #""#),

Counter(team: "SEP_DROIDS", counter: "REBELS_CLS", hard: false, description: 
"""
Doesn't work well General Grievous is Gear 13.  The counterattacks will work against the Separatist Droids.  You want Chewbacca to be fast enough to dispel stealth, so when GG uses his AoE you'll all counterattack him.  If General Grievious' leadership ability has a zeta, you can avoid being target locked with a fast Chirrut instead of R2-D2, so he can apply tenacity after B2's AoE.  The crit avoidance on guarded allies should buy you enough time if you fail to kill General Grievous fast.  If you can use C-3PO to call assists on General Grevious before he does his AoE.
""", video: #""#),

Counter(team: "SEP_DROIDS", counter: "NS_DAKA_TALIA", hard: false, description: 
"""
**Possibly a hard counter if Old Daka has a high Relic level**<br/><br/>

Doesn't work well if General Grievous is Gear 13.<br/><br/>

Requires the team to be modded for high health and tenacity, as Old Daka's leadership gives 50% health to NS allies.  The higher Daka's gear level and health, the better chance you have to win.  Use Talia's Water of Life to cleanse any debuffs that land on Daka.
""", video: #""#),

Counter(team: "SEP_DROIDS", counter: "NS_MT", hard: false, description: 
"""
**Possibly a hard counter if Old Daka has a high Relic level**<br/><br/> 

Doesn't work well if General Grievous is Gear 13.<br/><br/>

Put a zeta on Mother Talzin's leadership ability to maximize Plague and have revives.  Old Daka and Nightsister Zombie need to have tanky mods because of General Grievous.  Daka's Serve Again zeta can help ensure survival.
""", video: #""#),

Counter(team: "SEP_DROIDS", counter: "SITH_EMPIRE_W_MALAK", hard: false, description: "", video: #""#),

Counter(team: "SEP_DROIDS", counter: "SITH_TRI", hard: false, description: 
"""
Does not work well if going against zeta'd Nute Gunray and B2 Super Battle Droid.  Put tanky mods and crit avoidance arrows on Darth Sion and Darth Traya.  Shoretrooper can soak up some damage and provide crit immunity for a turn.  Traya's leadership bring crit avoidance.  Don't bring General Grievous to less that 100% HP and try not to kill anyone until you can one-shot Grievous with Darth Nihilus' Annihilate.  Dispel before annihilate is ready.
""", video: #""#),

Counter(team: "SEP_NUTE", counter: "GEONOSIANS", hard: true, description: "", video: #""#),

Counter(team: "SEP_NUTE", counter: "REBELS_CLS", hard: true, description: 
"""
This is not an ideal counter if they have Enfys Nest, because a Commander Luke Skywalker leadership has a high probability of creating a time-out situation when one-on-one with Enfys Nest.<br/><br/>

If you decide to use this counter against Nest, kill Wat Tambor first to stop his buffs.  Then you should be very strategic about your attacks and try to kill Enfys Nest before she's the last one standing.  To do this, right after her turn (when her undispellable protection is down) try to only damage her with a high damage attack.  Avoid lower damage AoEs until you make this attack.  Also, try to avoid removing turn meter, which will delay your opportunities to make these big attacks. 
""", video: #""#),

Counter(team: "SEP_NUTE", counter: "EP", hard: false, description: "", video: #""#),

Counter(team: "SEP_NUTE", counter: "JTR", hard: false, description: "", video: #""#),

Counter(team: "SEP_NUTE_NEST", counter: "JEDI_REVAN", hard: true, description: "", video: #""#),

Counter(team: "SEP_NUTE_NEST", counter: "SITH_TRI", hard: false, description: 
"""
Your first target to kill is Wat Tambor, to stop the buffs.  You should try to use Darth Traya's Isolate on him to prevent team buffs and from Count Dooku giving him stealth.<br/><br/>

Your first target for Darth Nihilus' Annihilate should be Enfys Nest.
""", video: #""#),

Counter(team: "SEP_NUTE_NEST", counter: "REBELS_CLS", hard: false, description: 
"""
This is not an ideal counter, because a Commander Luke Skywalker leadership has a high probability of creating a time-out situation when one-on-one with Enfys Nest.<br/><br/>

If you decide to use this counter, kill Wat Tambor first to stop his buffs.  Then you should be very strategic about your attacks and try to kill Enfys Nest before she's the last one standing.  To do this, right after her turn (when her undispellable protection is down) try to only damage her with a high damage attack.  Avoid lower damage AoEs until you make this attack.  Also, try to avoid removing turn meter, which will delay your opportunities to make these big attacks.  
""", video: #""#),

Counter(team: "SEP_NUTE_NEST", counter: "NS_MT", hard: false, description: "", video: #""#),

Counter(team: "SEP_NUTE_NEST", counter: "BH_BOSSK", hard: false, description: 
"""
Using Dengar will help prevent Nute Gunray's stealthing.  Using Embo can help bypass Enfys Nest's Protection and he removes buffs on basic.  This is a soft counter, because your Bounty Hunters need to be fast enough to get the Payout before the stuns or spread of Extortion overwhelm your team.
""", video: #""#),

Counter(team: "SEP_NUTE_NEST", counter: "GEONOSIANS", hard: false, description: 
"""
Higher team power or relics are required to make this counter work!
""", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "501ST", hard: true, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "RESISTANCE_HEROES", hard: true, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "EP_SHORETROOPER", hard: false, description: 
"""
Darth Vader's Merciless Massacre zeta is required and your Vader must be faster than the enemy Bastila Shan (Fallen).<br/><br/>

On Vader's turn, use Merciless Massacre, then use the AoE while targeting Darth Malak.  Next, (hopefully) kill someone using Culling Blade.<br/><br/>

You'll then want to kill Darth Revan as soon as possible to prevent his AoE shock and to allow for ability blocks and stuns on other enemies.  If Revan is faster than your Vader, he will more than likely use Insanity on Shoretrooper, putting fear on him and Emperor Palpatine, losing the Foresight from Bastila and gaining five stacks of Ferocity, which will increase the damage from Vader's Saberthrow.  It's also ok if their Malak is faster than your Vader, you just want Vader to be faster than their Bastila.
""", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "GALACTIC_REPUBLIC_C3PO", hard: false, description: 
"""
Use a slow Padmé team and damage them with Courage.  Kill all besides Malak, then try to put healing immunity and slowly kill malak.  Need high gear, good 6E health mods, and a big of help from RNGesus.
""", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "DROIDS_ANTI_MALAK", hard: false, description: 
"""
Can sub L3-37 with Chopper.  Can also use IG-88 lead, General Grievous, Imperial Probe Droid, BB-8, and T3-M4.
""", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "CLONES_SHAAKTI_JKA", hard: false, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "SITH_EMPIRE_W_MALAK", hard: false, description: 
"""
When facing a faster Darth Revan or Bastila Shan (Fallen) you need pre-taunt.  Strategy 1: Using Grand Admiral Thrawn (must be faster than enemy Bastila), swap turn meter with your Bastila Shan (Fallen), apply fear, and also apply Corrupted Battle Meditation so the enemy Bastila doesn't apply fear (you'll want Bastila's zeta in this case so the increased potency helps Corrupted Battle Meditation.  Strategy 2: Using Grand Master Yoda (must be faster than enemy Bastila), you apply tenacity up, which denies Corrupted Battle Meditation. 
""", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "SITH_EMPIRE_WO_MALAK", hard: false, description: 
"""
When facing a faster Darth Revan or Bastila Shan (Fallen) you need pre-taunt.  Strategy 1: Using Grand Admiral Thrawn (must be faster than enemy Bastila), swap turn meter with your Bastila Shan (Fallen), apply fear, and also apply Corrupted Battle Meditation so the enemy Bastila doesn't apply fear (you'll want Bastila's zeta in this case so the increased potency helps Corrupted Battle Meditation.  Strategy 2: Using Grand Master Yoda (must be faster than enemy Bastila), you apply tenacity up, which denies Corrupted Battle Meditation. 
""", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "JEDI_REVAN", hard: false, description: 
"""
Mod Bastila Shan first.  If subbing in Grand Admiral Thrawn (must be faster than enemy Bastila):  Jedi Knight Revan goes first, put Direct Focus on Darth Revan, use Thrawn's Fracture on Darth Revan (before enemy Bastila applies Corrupted Battle Meditation).  If subbing in Hermit Yoda:  Have Hoda that goes first, use Revan's Strategic Advantage on Grand Master Yoda and use his tenacity up to block the fear.  When calling Grand Master Yoda to assist, apply it to Sith Empire Trooper.  On Grand Master Yoda's turns, try to hit Darth Revan, then when he's at 60-70% health use his special for a kill.  You need a pre-taunt on the enemy team (like Sith Empire Trooper) so you can attack someone who isn't Darth Revan until he's at 60-70% health.  Therefore, if enemy has Sith Marauder instead of Sith Empire Trooper, this strategy won't work.
""", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "SEP_DROIDS", hard: false, description: 
"""
General Grievous needs a super high HP build with a focus on crit.  zzT3-M4 can bring a lot of needed crit damage, but needs a crit chance triangle mod to be reliable.  However, you may still fail if Malak has a crit avoidance arrow mod.
""", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "REBELS_CLS", hard: false, description: 
"""
Will not work against a fast Bastila Shan (Fallen)!  Also, this is very mod-dependent.  Have Han Solo shoot Bastila first, use Commander Luke Skywalker's "Call to Action" then finish off Bastila.  Try to make sure R2-D2 keeps CLS stealthed, so the whole team doesn't get Fear from Darth Revan.  Leave Darth Malak for last.  When is the last one standing, strategically use your stuns so you delay his turns when you lower his health.  Or else, he'll meet the health threshold and immediately use Drain Life to heal himself.  You could also try subbing in B2 Super Battle Droid in place of C-3PO to help dispel the taunts and focus on killing Bastila or Revan.
""", video: #""#),

Counter(team: "SITH_EMPIRE_W_MALAK", counter: "MAGMA", hard: false, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "SITH_EMPIRE_WO_MALAK", hard: true, description: 
"""
When facing a faster Darth Revan or Bastila Shan (Fallen) you need pre-taunt.  Strategy 1: Using Grand Admiral Thrawn (must be faster than enemy Bastila), swap turn meter with your Bastila Shan (Fallen), apply fear, and also apply Corrupted Battle Meditation so the enemy Bastila doesn't apply fear (you'll want Bastila's zeta in this case so the increased potency helps Corrupted Battle Meditation.  Strategy 2: Using Grand Master Yoda (must be faster than enemy Bastila), you apply tenacity up, which denies Corrupted Battle Meditation. 
""", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "GALACTIC_REPUBLIC", hard: true, description: 
"""
Use a slow Padmé team and damage them with Courage.  Need high gear, good 6E health mods, and a big of help from RNGesus.
""", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "JEDI_REVAN", hard: true, description: 
"""
Throughout this battle, you'll want to keep Grand Master Yoda's Tenacity up as much as possible to avoid Shock and Corrupted Battle Meditation, and when that fails use General Kenobi's mass cleanse.  Start this battle by using Jedi Knight Revan's Direct Focus on Bastila Shan (Fallen) first, unless Sion is on the team.  Use Grand Master Yoda's Tenacity Buff immediately.  After BSF's mark is cleared (if she's killed or not), work on HK-47.  Feed Grand Master Yoda with Revan's AOE, and bounce Revan's Strategist buff back and forth between GMY and Revan.  NEVER waste any kind of assisted attacks on Darth Revan.  If your Jolee Bindo dies, you're more than likely going to lose.
""", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "EP_SHORETROOPER", hard: true, description: 
"""
Darth Vader's Merciless Massacre zeta is required and your Vader must be faster than the enemy Bastila Shan (Fallen).<br/><br/>

On Vader's turn, use Merciless Massacre and (hopefully) kill someone using Culling Blade.<br/><br/>

You'll then want to kill Darth Revan as soon as possible to prevent his AoE shock and to allow for ability blocks and stuns on other enemies.  If Revan is faster than your Vader, he will more than likely use Insanity on Shoretrooper, putting fear on him and Emperor Palpatine, losing the Foresight from Bastila and gaining five stacks of Ferocity, which will increase the damage from Vader's Saberthrow.
""", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "EP_TRAYA", hard: true, description: 
"""
You want to start a turn meter train using their debuffs.  Darth Nihilus is a must for annihilates, Darth Traya is used so Darth Nihilus won't be debuffed.  Then any support/tank that can help Darth Nihilus get his annihilates or slow the enemy is a good fifth.
""", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "CLONES_SHAAKTI", hard: false, description: 
"""
Make sure to mod CT-7567 "Rex" as fast as possible, and he should be able to go first since his unique adds speed for each 501st ally.
""", video: #"https://www.youtube.com/watch?v=QhwRfO6Wjl0&t=255s"#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "SITH_TRI", hard: false, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "SEP_DROIDS", hard: false, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "REBELS_CLS", hard: false, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "BH_JANGO", hard: false, description: 
"""
You must outspeed them, at least with Bossk.  Get the contract fast (use AoEs to get it done as fast as possible).
""", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "QIRA_NEST_HODA_FULL", hard: false, description: 
"""
The debuffs with buff Enfys Nest.  Revives should allow you to survive long enough for Enfys Nest and Vandor Chewbacca to do the damage and for Hermit Yoda to buff them.  This requires a top team in terms of gear and modding to beat a meta team Darth Revan.
""", video: #""#),

Counter(team: "SITH_EMPIRE_WO_MALAK", counter: "REBELS_CLS_THRAWN", hard: false, description: 
"""
You need a mod advange.  Commander Luke Skywalker needs to go first and the others need to go after him.  This way, CLS opens and grants the team 10% turn meter with his special, clearing the pretaunt int he process.  Thrawn can then Fracture Darth Revan (or alternatively he can swap turn meter with Han Solo, if he's slow).  Then focus on Bastila Shan (Fallen).
""", video: #""#),

Counter(team: "SITH_MAUL", counter: "BH_BOSSK", hard: true, description: "", video: #""#),

Counter(team: "SITH_MAUL", counter: "REX", hard: true, description: 
"""
You want any combination of the following: Boba Fett, Wampa, Shoretrooper, Darth Vader + Emperor Palpatine, Chirrut Îmwe + Baze Malbus, and/or R2-D2
""", video: #""#),

Counter(team: "SITH_MAUL", counter: "NS_MT", hard: true, description: "", video: #""#),

Counter(team: "SITH_MAUL", counter: "BH_JANGO", hard: true, description: "", video: #""#),

Counter(team: "SITH_MAUL", counter: "JEDI_BASTILA", hard: false, description: "", video: #""#),

Counter(team: "SITH_MAUL", counter: "GK", hard: false, description: 
"""
You want high damage toons (Wampa or Han Solo + Chewbacca).
""", video: #""#),

Counter(team: "SITH_MAUL", counter: "PHOENIX", hard: false, description: 
"""
If you have Sabine Wren and Garazeb "Zeb" Orrelios' zetas, this works a great counter to this high-dodge team, since their zeta'd attacks cannot be evaded.
""", video: #""#),

Counter(team: "SITH_TRI", counter: "REBELS_CLS", hard: true, description: 
"""
Must have C-3PO, or else this is a soft counter.
""", video: #""#),

Counter(team: "SITH_TRI", counter: "JEDI_REVAN", hard: true, description: "", video: #""#),

Counter(team: "SITH_TRI", counter: "EP_ANTI-TRAYA", hard: true, description: 
"""
You want Imperial Probe Droid to explode on Traya, followed by Death Trooper applying Deathmark for quick kills.  Emperor Palpatine can remove a bit of HP from IPD when healing himself, so IPD can detonate faster.
""", video: #""#),

Counter(team: "SITH_TRI", counter: "MAGMA", hard: true, description: 
"""
Use Grand Admiral Thrawn's Fracture on Darth Traya, and later use Thrawn's basic attack on Nihilus to keep him stunned.  Use Death Trooper's AoE to increase cooldowns, basics on Darth Sion until he's dead, then Terminate to Deathmark the other enemies.  Magmatrooper will only attack Darth Sion with his basic, because it causes two attacks and getting pain will gain a lot of turn meter.  Imperial Probe Droid isn't needed unless Magmatrooper fails to kill Sion.
""", video: #""#),

Counter(team: "SITH_TRI", counter: "JTR_DROIDS", hard: false, description: "", video: #""#),

Counter(team: "SITH_TRI", counter: "CLONES_SHAAKTI", hard: false, description: 
"""
While Darth Traya's leadership will deal damage to your team on assists, you should be able to overcome them if you focus correctly.  You'll want to kill Sith Trooper and Grand Admiral Thrawn ASAP.  Be mindful of Darth Nihilus' annihilate, as you don't want it to hit CT-7567 "Rex" or CT-21-0408 "Echo", your big damage dealers.  Kill Traya, then Nihilus, then finally Darth Sion.  You may want to use Rex's Aerial Advantage on Nihilus, as he will likely be the tankiest if he is able to Annihilate someone.
""", video: #"https://www.youtube.com/watch?v=QhwRfO6Wjl0&t=255s"#),

Counter(team: "SITH_TRI", counter: "REBELS_DANGER_ZONE", hard: false, description: 
"""
This is a touch match, especially if they have 6* mods and high crit avoidance/defense/tenacity.
""", video: #""#),

Counter(team: "SITH_TRI", counter: "JEDI_BASTILA", hard: false, description: 
"""
Use when you don't have Jedi Knight Revan.  This team has high offense and Bastila Shan's leadership will grant enough tenacity to help against debuffs.
""", video: #""#),

Counter(team: "SITH_TRI", counter: "BH_JANGO", hard: false, description: 
"""
You need the speed advantage (which shouldn't be too hard with good mods and Jango's leadership giving 30+ speed).  You must have Bossk to eat Grand Admiral Thrawn's Fracture and Darth Traya's Isolate.  Activate the contract ASAP using AoEs and Bossk's mass assist.  After that, you should have enough offensive power to win.
""", video: #""#),

Counter(team: "SITH_TRI", counter: "FO_KRU", hard: false, description: 
"""
A fast First Order Officer is recommended.  Spam Kylo Ren's AoE to continuously spread turn meter, and let FOO feed First Order Executioner turns.
""", video: #""#),

Counter(team: "SITH_TRI", counter: "EWOKS_C3PO", hard: false, description: "", video: #""#),

Counter(team: "VADER", counter: "SEP_DROIDS", hard: false, description: 
"""
Requires comparable gear and an IG-100 MagnaGuard that can keep Droideka alive long enough for him to attack after gaining sufficient Charge.<br/><br/>

Darth Vader's first attack will be Merciless Massacre, which can deal a lot of damage and feed turn meter to their team.  However, Droideka will gain Charge and turn meter after each enemy attack, so by the time Droideka gets a turn he could easily have 10-15 stacks of Charge.  Using Droideka's Destructive Armament with this many stacks of Charge should be able to one-shot Vader (or almost anyone else).
""", video: #""#),

]
static let squad3v3 = [
Counter(team: "501ST_3v3", counter: "REBELS_CLS_3v3", hard: true, description: 
"""
Stun CT-7567 "Rex" at the beginning of battle to prevent turn meter gain.  Then work on dropping General Skywalker.  Once he hits his knees, kill the clones, then kill him when he gets up.
""", video: #""#),

Counter(team: "501ST_3v3", counter: "SITH_EMPIRE_3v3_WO_MALAK", hard: false, description: "", video: #""#),

Counter(team: "501ST_3v3", counter: "GALACTIC_REPUBLIC_3v3", hard: false, description: "", video: #""#),

Counter(team: "BH_BOSSK_3v3", counter: "JTR_DROIDS_3v3", hard: true, description: 
"""
Use Rey (Jedi Training)'s dispel/turn meter reduction on Bossk. For JTR's assist attack, always call BB-8 (if he has Rolling with the Punches zeta) because he will call another Resistance ally to assist.  Kill order: [whichever Bounty Hunter], Bossk, then Dengar.
""", video: #""#),

Counter(team: "BH_BOSSK_3v3", counter: "REBELS_CLS_3v3", hard: true, description: 
"""
Stun Bossk with Han Shoots First, to prevent his taunt, then keep reducing his turn meter with Commander Luke Skywalker's attacks or Han Solo's Basic.
""", video: #""#),

Counter(team: "BH_BOSSK_3v3", counter: "WAT_3v3_SUNFACWAMPA", hard: true, description: 
"""
Although there is no leadership ability at play here, Wat Tambor's Mass Manufacture gives a great advantage.<br/><br/>

Give Wat's Baktoid Shield Generator to Sun Fac so he recovers protection every turn and keeps taunting while he has protection.  He also has counterattack and dispels buffs on his basic attack.<br/><br/>

Give Wat's BlasTech Weapon Mod to Wampa so his attacks ignore defense and he prevents Boba Fett from reviving.
""", video: #""#),

Counter(team: "BH_BOSSK_3v3", counter: "FO_HUX_3v3", hard: false, description: "", video: #""#),

Counter(team: "BH_BOSSK_3v3", counter: "FO_KRU_3v3", hard: false, description: 
"""
Make sure your First Order Executioner is properly geared for this matchup, as you will need a high damage output on him to take out Bossk, as he will more than likely receive healing from a stealthed Dengar.  This can also be done by modding him with high Health, as he gets bonus offense equal to 5% of his Max Health. Also consider using Kylo Ren's basic on Bossk, to apply healing immunity.
""", video: #""#),

Counter(team: "BH_BOSSK_3v3", counter: "GEONOSIANS_3v3", hard: false, description: 
"""
Sun Fac can dispel Bossk's taunt, Geonosian Spy provides exposes and high damage, and Geonosian Brood Alpha heals and has a summon that taunts.
""", video: #""#),

Counter(team: "CLONES_SHAAKTI_3v3", counter: "GALACTIC_REPUBLIC_3v3", hard: true, description: 
"""
Try to keep CT-7567 "Rex" stunned, if possible, to reduce his chance to add tenacity or one-shot a character with Aerial Advantage.
""", video: #""#),

Counter(team: "CLONES_SHAAKTI_3v3", counter: "JEDI_REVAN_3v3", hard: true, description: "", video: #""#),

Counter(team: "CLONES_SHAAKTI_3v3", counter: "GALACTIC_REPUBLIC_3v3_PADMEANAKINAHSOKA", hard: true, description: 
"""
Try to keep CT-7567 "Rex" stunned, if possible, to reduce his chance to add tenacity or one-shot a character with Aerial Advantage.
""", video: #""#),

Counter(team: "CLONES_SHAAKTI_3v3", counter: "BH_BOSSK_3v3_BOBACAD", hard: true, description: "", video: #""#),

Counter(team: "EP_3v3", counter: "JEDI_BASTILA_3v3", hard: true, description: "", video: #""#),

Counter(team: "EP_3v3", counter: "REX_3v3", hard: true, description: "", video: #""#),

Counter(team: "EP_3v3", counter: "REX_WAMPA", hard: true, description: "", video: #""#),

Counter(team: "EP_3v3", counter: "JTR_DROIDS", hard: false, description: "", video: #""#),

Counter(team: "EP_MALAK_3v3", counter: "SITH_DUO_3v3", hard: true, description: 
"""
Use Emperor Palpatine as a third!  Darth Nihilus' cooldown increase keeps Darth Malak from doing any serious damage, and EP's shock keeps him from healing.  Darth Traya's leadership should provide enough ally HP regen and debuff dispels to give Nihilus time to Annihilate Darth Malak first.  Kill the other two in any order.
""", video: #""#),

Counter(team: "EP_MALAK_3v3", counter: "SITH_EMPIRE_3v3_WO_MALAK", hard: false, description: "", video: #""#),

Counter(team: "EP_MALAK_3v3", counter: "RESISTANCE_HEROES_3v3", hard: false, description: "", video: #""#),

Counter(team: "EWOKS_3v3", counter: "GALACTIC_REPUBLIC_3v3", hard: true, description: "", video: #""#),

Counter(team: "EWOKS_3v3", counter: "NEST_SOLO", hard: true, description: 
"""
If your Enfys Nest has high tenacity, this should be an auto win.  You can also add a Luke Skywalker (Farmboy) lead to increase her tenacity, if needed.  Also consider adding Princess Leia since she'll always be stealthed, does great damage, and could avoid a time-out situation if the Ewoks are constantly stunning Nest.
""", video: #""#),

Counter(team: "EWOKS_3v3", counter: "QIRA_NEST_HODA", hard: true, description: "", video: #""#),

Counter(team: "EWOKS_3v3", counter: "REX_WAMPA", hard: true, description: "", video: #""#),

Counter(team: "EWOKS_3v3", counter: "WAMPA_SOLO", hard: true, description: "", video: #""#),

Counter(team: "EWOKS_3v3", counter: "GK_WAMPA_3v3", hard: true, description: "", video: #""#),

Counter(team: "FO_KRU_3v3", counter: "BH_DENGAR_3v3_MANDOGREEF", hard: true, description: 
"""
Can use a low-starred Greef Karga (gear must be comparable, though).<br/><br/>

Dengar's contract pays out when the healthiest enemy is attacked 10 times.  This is an easier contract when Kylo Ren (Unmasked) is more powerful than the other two enemies (e.g. he's the only one with a relic), because he will taunt but won't take much damage - which will usually keep him the healthiest.  Also, using Greef's Sweeten the Deal will give everyone Retribution, which should make the contract even easier to fulfill.

Once the contract is fulfilled, The Mandalorian will get the Disintegrate ability.  Use it on KRU.  Focus all attacks on First Order Executioner until Disintegrate is available again, then use it on Kylo Ren.
""", video: #"https://youtu.be/JxdZgaI1vMQ"#),

Counter(team: "FO_KRU_3v3", counter: "SITH_EMPIRE_3v3_W_MALAK", hard: true, description: "", video: #""#),

Counter(team: "FO_KRU_3v3", counter: "SEP_DROIDS_3v3_HK47DROIDEKA", hard: true, description: 
"""
Use General Grievous' basic attack to apply healing immunity to Kylo Ren (Unmasked).  GG's leadership will also prevent counterattacks if the enemy is target locked.
""", video: #""#),

Counter(team: "FO_KRU_3v3", counter: "EWOKS_3v3", hard: false, description: "", video: #""#),

Counter(team: "FO_KRU_3v3", counter: "JTR_DROIDS_3v3", hard: false, description: 
"""
Focus on First Order Executioner first, then Kylo Ren (Unmasked), then Kylo Ren.  Use Rey (Jedi Training)'s Mind Trick on KRU when he taunts to continue focusing on FOEX.  Use R2-D2's Smoke Screen on BB-8 so he prompts an ally assist when attacked.  
""", video: #""#),

Counter(team: "FO_KRU_3v3_SLKR", counter: "501ST_3v3_FIVESECHO", hard: false, description: "", video: #""#),

Counter(team: "GALACTIC_REPUBLIC_3v3", counter: "SITH_EMPIRE_3v3_WO_MALAK", hard: true, description: 
"""
Use Darth Revan's Force Storm to shock General Kenobi, then (if using her) Bastila Shan (Fallen)'s basic on Padmé to ability block, then DR's Insanity on Padmé.  You then want to take out Jedi Knight Anakin first.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC_3v3", counter: "EP_3v3_BSFSET", hard: true, description: 
"""
Emperor Palpatine keeps them stunned, Bastila Shan (Fallen) keeps debuffs on them, and Sith Empire Trooper soaks up any damage whenever an enemy isn't stunned.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC_3v3", counter: "SITH_DUO_3v3", hard: true, description: 
"""
Use Grand Admiral Thrawn as a third.  Use Thrawn's Fracture on General Kenobi to block his taunt.  Use Darth Traya's Isolate on Padmé Amidala to block her from spreading buffs.  Use Darth Nihilus' Annihilate on Jedi Knight Anakin first, then Padmé, then Kenobi.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC_3v3_PADMEANAKINAHSOKA", counter: "EP_3v3_BSFSET", hard: true, description: 
"""
Emperor Palpatine keeps them stunned, Bastila Shan (Fallen) keeps debuffs on them, and Sith Empire Trooper soaks up any damage whenever an enemy isn't stunned.
""", video: #""#),

Counter(team: "GALACTIC_REPUBLIC_3v3_PADMEANAKINAHSOKA", counter: "BH_BOSSK_3v3_BOBACAD", hard: true, description: "", video: #""#),

Counter(team: "GEONOSIANS_3v3", counter: "SITH_EMPIRE_3v3_WO_MALAK", hard: true, description: "", video: #""#),

Counter(team: "GEONOSIANS_3v3", counter: "SITH_TRI_3v3", hard: true, description: 
"""
If you have a Crit Avoidance Arrow mod on Darth Traya, or a higher-geared team, then you can beat this Geonosian team without Darth Nihilus.  Darth Traya's leadership ability will make the Geonosians kill themselves due to the amount of assists.
""", video: #""#),

Counter(team: "GEONOSIANS_3v3", counter: "FO_HUX_3v3", hard: true, description: "", video: #""#),

Counter(team: "GEONOSIANS_3v3", counter: "JAWAS_3v3", hard: true, description: 
"""
With a G11 Jawa team, you should be able to win on auto.  The Geonosian's multi attacks will make them kill themselves by adding so many thermal detonators.  You just want to make sure your Jawas can stay alive long enough for it to happen.
""", video: #""#),

Counter(team: "GEONOSIANS_3v3", counter: "VADER_3v3_GEOKILL", hard: true, description: "", video: #""#),

Counter(team: "GEONOSIANS_3v3", counter: "FO_KRU_3v3", hard: true, description: "", video: #""#),

Counter(team: "GEONOSIANS_3v3", counter: "NS_MT_3v3_ASAJJZOMBIE", hard: false, description: 
"""
You need to have a speed advantage here, specifically with Nightsister Zombie, or else the Geonosians will easily dispel her and you'll be open to Geonosian Spy's Silent Strike one-shotting you.<br/><br/>

Use Special attacks as early and as much as possible to spread Mother Talzin's Plague.
""", video: #""#),

Counter(team: "GEONOSIANS_3v3", counter: "GALACTIC_REPUBLIC_3v3", hard: false, description: "", video: #""#),

Counter(team: "GEONOSIANS_3v3", counter: "NS_ASAJJ_3v3_OLDDAKAZOMBIE", hard: false, description: "", video: #""#),

Counter(team: "GEONOSIANS_3v3", counter: "EP_3v3_BSFTHRAWN", hard: false, description: 
"""
Use Grand Admiral Thrawn's Fracture on Geonsian Brute, try to stun everyone with Emperor Palpatine's first attack, to give some time to land a shock on Geonosian Brute Alpha and Geonosian Brute.  Once EP's group stun comes back up, shocked enemies will be stunned.  Try to repeat this rhythm of stunning and shocking.
""", video: #""#),

Counter(team: "GEONOSIANS_3v3_POGGLEGS", counter: "JEDI_REVAN_3v3_AHSOKASHAAKTI", hard: false, description: 
"""
Take Poggle the Lesser out as fast as possible and keep Shaak Ti alive.
""", video: #""#),

Counter(team: "GK_ZARRISS_3v3", counter: "VADER_3v3", hard: false, description: "", video: #""#),

Counter(team: "GK_ZARRISS_3v3", counter: "JTR_DROIDS_3v3", hard: false, description: "", video: #""#),

Counter(team: "JAWAS_3v3", counter: "REBELS_FBLUKE_3v3", hard: true, description: "", video: #""#),

Counter(team: "JAWAS_3v3", counter: "REBELS_WIGGS_3v3", hard: true, description: 
"""
Going against Jawas won't be a clean win because the thermal detonators will catch up to you.  This team should have high enough single-character damage to kill the Jawas off before too many detonators explode.
""", video: #""#),

Counter(team: "JEDI_BASTILA_3v3", counter: "SEP_DROIDS_3v3", hard: true, description: 
"""
B2 Super Battle Droid's dispel helps prevent buffs.  This team should have enough offensive power to push through the extra protection that Bastila Shan's leadership provides.
""", video: #""#),

Counter(team: "JEDI_BASTILA_3v3", counter: "THRAWN_3v3", hard: false, description: "", video: #""#),

Counter(team: "JEDI_BASTILA_3v3", counter: "QIRA_NEST_3v3", hard: false, description: "", video: #""#),

Counter(team: "JEDI_BASTILA_REVAN_3v3", counter: "DROIDS_ANTI_JEDI_3v3", hard: true, description: "", video: #""#),

Counter(team: "JEDI_REVAN_3v3", counter: "SITH_EMPIRE_3v3_JKRKILLER", hard: false, description: "", video: #""#),

Counter(team: "JEDI_REVAN_3v3", counter: "REBELS_CLS_3v3", hard: false, description: 
"""
Kill Grand Master Yoda first, to trigger Jedi Knight Revan's auto-revive.  Then kill GMY again before killing Jolee, because he's too fast and will cost you banners if you let him buff the team and attack you while you're trying to tear down Jolee.
""", video: #""#),

Counter(team: "JTR_DROIDS_3v3", counter: "SITH_EMPIRE_WO_MALAK", hard: true, description: "", video: #""#),

Counter(team: "JTR_DROIDS_3v3", counter: "GALACTIC_REPUBLIC_3v3_PADMEANAKINAHSOKA", hard: true, description: "", video: #""#),

Counter(team: "JTR_DROIDS_3v3", counter: "CLONES_SHAAKTI_3v3_ECHOFIVES", hard: true, description: "", video: #""#),

Counter(team: "JTR_DROIDS_3v3", counter: "NS_MT_3v3", hard: false, description: "", video: #""#),

Counter(team: "JTR_DROIDS_3v3", counter: "REX_WAMPA", hard: false, description: "", video: #""#),

Counter(team: "NEST_SOLO", counter: "VADER_3v3_MALAKRG", hard: true, description: "", video: #""#),

Counter(team: "NS_ASAJJ_3v3", counter: "IT_3v3", hard: true, description: "", video: #""#),

Counter(team: "NS_ASAJJ_3v3", counter: "CLONES_SHAAKTI_3v3", hard: false, description: 
"""
You can sub in ARC Trooper in place of CT-7567 "Rex".
""", video: #""#),

Counter(team: "NS_ASAJJ_3v3", counter: "REX_WAMPA", hard: false, description: "", video: #""#),

Counter(team: "NS_ASAJJ_3v3", counter: "WAMPA_SOLO", hard: false, description: "", video: #""#),

Counter(team: "NS_ASAJJ_3v3", counter: "SITH_TRI_3v3", hard: false, description: "", video: #""#),

Counter(team: "NS_MT_3v3", counter: "EP_3v3", hard: true, description: "", video: #""#),

Counter(team: "NS_MT_3v3", counter: "SEP_NUTE_3v3_JANGODOOKU", hard: true, description: 
"""
Count Dooku isn't required as a third ally.<br/><br/>

Jango Fett's Notorious Reputation zeta is required, to prevent enemies defeated by Jango to be revived.<br/><br/>

With Nute Gunray as the leader, Jango's payout is activated at the beginning of battle and the Extortion mechanic will speed your team up and remove buffs from enemies if they don't pay to remove it.  This can help get around Nightsister Zombie's taunt.
""", video: #""#),

Counter(team: "NS_MT_3v3", counter: "GEONOSIANS_3v3", hard: true, description: 
"""
You want a comparable or stronger Geonosian Spy because he is your primary damage dealer.<br/><br/>

This team easily dispels Nightsister Zombie's taunt, Geonosian Brood Alpha supplies buffs and heals, and GBA's Geonosian Brute taunts so that you have time to take them out with big hits from Geo Spy's Silent Strike.
""", video: #""#),

Counter(team: "NS_MT_3v3", counter: "REX_WAMPA", hard: false, description: "", video: #""#),

Counter(team: "NS_MT_3v3", counter: "WAMPA_SOLO", hard: false, description: "", video: #""#),

Counter(team: "NS_MT_3v3", counter: "BH_BOSSK_3v3", hard: false, description: "", video: #""#),

Counter(team: "NS_MT_3v3", counter: "REBELS_WIGGS_3v3_C3PO", hard: false, description: 
"""
Requires comparable power.<br/><br/>

This team will get stronger as the fight goes on.  You want to use C-3PO's Protocol Droid on Biggs Darklighter to get him to three stacks of Translation, so whenever C-3PO uses a basic his cooldowns are reduced.  This will allow you to use Biggs' Comrade-in-Arms more often, which will add more assists and continue spreading debuffs to the enemy team.
""", video: #""#),

Counter(team: "NS_MT_3v3", counter: "QIRA_3v3_LANDOL3", hard: false, description: 
"""
Qi'ra's leadership gives the team increased Crit Chance and heals on an attack that crits.  Lando Calrissian's Double Down is an AoE that resets and deals 100% more damage on its next attack when scoring more than one crit hit.<br/><br/>

L3-37's taunt should keep Lando alive long enough to bombard them with his AoE.<br/><br/>

You also have Qi'ra's Scattering Blast to help dispel Nightsister Zombie's taunt.
""", video: #""#),

Counter(team: "OR_CARTH_3v3", counter: "SEP_NUTE_3v3_GRIEVOUSDROIDEKA", hard: true, description: "", video: #""#),

Counter(team: "OR_CARTH_3v3", counter: "JEDI_BASTILA_3v3_EZRAHODA", hard: true, description: "", video: #""#),

Counter(team: "OR_CARTH_3v3", counter: "EP_ANTI_REBEL", hard: true, description: "", video: #""#),

Counter(team: "OR_CARTH_3v3", counter: "JEDI_BASTILA_3v3", hard: false, description: "", video: #""#),

Counter(team: "OR_CARTH_3v3", counter: "FO_KRU_3v3", hard: false, description: "", video: #""#),

Counter(team: "OR_CARTH_3v3", counter: "REX_WAMPA", hard: false, description: "", video: #""#),

Counter(team: "OR_CARTH_3v3_MISSIONVAOZAALBAR", counter: "SEP_NUTE_3v3_GRIEVOUSDROIDEKA", hard: true, description: "", video: #""#),

Counter(team: "OR_CARTH_3v3_MISSIONVAOZAALBAR", counter: "RESISTANCE_FINN_3v3", hard: true, description: 
"""
Use Finn and R2-D2's stuns to control Carth Onasi and Mission Vao so Rey (Scavenger).  Use R2-D2's stealth early, so you can get Rey's offense up.  Rey (Scavenger) should have enough damage to take Zaalbar out at the end.  
""", video: #""#),

Counter(team: "OR_CARTH_3v3_MISSIONVAOZAALBAR", counter: "THRAWN_3v3_SHOREDEATH", hard: true, description: "", video: #""#),

Counter(team: "OR_CARTH_3v3_MISSIONVAOZAALBAR", counter: "JEDI_BASTILA_3v3", hard: false, description: "", video: #""#),

Counter(team: "OR_CARTH_3v3_MISSIONVAOZAALBAR", counter: "FO_KRU_3v3", hard: false, description: 
"""
You should have a slight advantage since the Carth team partially relies on applying debuffs.  Kylo Ren (Unmasked)'s leadership gives health and protection when FO gains status effects, and Kylo Ren gains speed based on the number of debuffs he has.

Use Kylo Ren's Outrage to Stun Mission Vao and KRU's Halt to Stun Zaalbar and remove his taunt/buffs.<br/><br/>

You'll ideally want to keep Zaalbar stunned the entire time so you can kill Vao first, then Carth next.
""", video: #""#),

Counter(team: "PHOENIX_3v3", counter: "IT_3v3", hard: true, description: "", video: #""#),

Counter(team: "PHOENIX_3v3", counter: "EWOKS_3v3", hard: true, description: "", video: #""#),

Counter(team: "PHOENIX_3v3", counter: "WAMPA_SOLO", hard: true, description: "", video: #""#),

Counter(team: "PHOENIX_3v3", counter: "REX_WAMPA", hard: true, description: "", video: #""#),

Counter(team: "PHOENIX_3v3", counter: "REBELS_ACK_3v3", hard: true, description: "", video: #""#),

Counter(team: "QIRA_3v3", counter: "NS_MT_3v3", hard: true, description: "", video: #""#),

Counter(team: "QIRA_3v3", counter: "JTR_DROIDS_3v3", hard: true, description: "", video: #""#),

Counter(team: "QIRA_3v3", counter: "FO_KRU_3v3", hard: true, description: "", video: #""#),

Counter(team: "REBELS_CLS_3v3", counter: "NS_ASAJJ_3v3_OLDDAKAZOMBIE", hard: true, description: "", video: #""#),

Counter(team: "REBELS_CLS_3v3", counter: "GK_WAMPA_3v3", hard: false, description: 
"""
Could also swap Enfys Nest for Wampa
""", video: #""#),

Counter(team: "REBELS_CLS_3v3", counter: "REX_WAMPA", hard: false, description: 
"""
Could also swap Enfys Nest for Wampa
""", video: #""#),

Counter(team: "REBELS_CLS_3v3", counter: "OLDBEN_WAMPA_3v3", hard: false, description: 
"""
Could also swap Enfys Nest for Wampa
""", video: #""#),

Counter(team: "REBELS_CLS_3v3", counter: "QIRA_NEST_HODA", hard: false, description: "", video: #""#),

Counter(team: "REBELS_CLS_3v3", counter: "NS_MT_3v3", hard: false, description: "", video: #""#),

Counter(team: "REBELS_WIGGS_3v3", counter: "IT_3v3", hard: true, description: "", video: #""#),

Counter(team: "RESISTANCE_FINN_3v3", counter: "EP_3v3_BSFTHRAWN", hard: false, description: 
"""
Note: this is untested!<br/><br/>

If you use this counter, please submit the results so we can confirm if this counter works or not!
""", video: #""#),

Counter(team: "RESISTANCE_FINN_3v3", counter: "NS_DOOKU_3v3_ASAJJZOMBIE", hard: false, description: 
"""
Note: this is untested!<br/><br/>

If you use this counter, please submit the results so we can confirm if this counter works or not!
""", video: #""#),

Counter(team: "RESISTANCE_FINN_3v3", counter: "JEDI_BASTILA_3v3_EZRAHODA", hard: false, description: 
"""
Note: this is untested!<br/><br/>

If you use this counter, please submit the results so we can confirm if this counter works or not!
""", video: #""#),

Counter(team: "REX_3v3", counter: "REBELS_CLS_3v3", hard: true, description: "", video: #""#),

Counter(team: "REX_3v3", counter: "JTR_3v3", hard: true, description: 
"""
Use Rey (Jedi Training)'s Mind Trick then Defiant Slash on CT-7567 "Rex".  The intention is to dispel him then apply healing immunity.  This will let you kill Rex before CT-5555 "Fives" and Fives will sacrifice himeself but Rex will be prevented from healing.  So you'll be able to kill him again with one shot, then clean up the last clone.
""", video: #""#),

Counter(team: "REX_3v3", counter: "FO_KRU_3v3", hard: true, description: 
"""
Kylo Ren (Unmasked)'s Merciless Pursuit zeta is recommended.
""", video: #""#),

Counter(team: "ROGUE_ONE_3v3", counter: "REX_WAMPA", hard: false, description: "", video: #""#),

Counter(team: "ROGUE_ONE_3v3_CHAZE", counter: "QIRA_NEST_3v3", hard: true, description: 
"""
Put Hermit Yoda's Master's Training on Enfys Nest and she'll easily clear this team.
""", video: #""#),

Counter(team: "ROGUE_ONE_3v3_CHAZE", counter: "EWOKS_3v3", hard: true, description: 
"""
Paploo's dispel constatly wipes all the buffs that Chirrut Îmwe adds, and takes the taunt from Baze Malbus.  Kill order: Jyn Erso, Chirrut, then Baze.
""", video: #""#),

Counter(team: "SEP_DROIDS_3v3", counter: "EP_3v3_BSFTHRAWN", hard: true, description: "", video: #""#),

Counter(team: "SEP_DROIDS_3v3", counter: "JEDI_BASTILA_REVAN_3v3", hard: false, description: "", video: #""#),

Counter(team: "SEP_DROIDS_3v3", counter: "REX_3v3", hard: false, description: "", video: #""#),

Counter(team: "SEP_DROIDS_3v3", counter: "GALACTIC_REPUBLIC_3v3", hard: false, description: "", video: #""#),

Counter(team: "SEP_DROIDS_3v3", counter: "GALACTIC_REPUBLIC_3v3_PADMEANAKINAHSOKA", hard: false, description: "", video: #""#),

Counter(team: "SEP_DROIDS_3v3", counter: "BH_BOSSK_3v3_MANDO", hard: false, description: 
"""
This requires the team to be a comparable level and The Mandalorian to possibly be higher.<br/><br/>

The strategy is to fulfill the contract as soon as possible to access Mando's Disintegrate.<br/><br/>

Fulfilling Bossk's contract should be fairly easy, as you have to damage the weakest enemy 10 times and you'll likely be able to do that on B2 Super Battle Droid at the beginning of battle.  Also, if you don't have Boba Fett as a third ally, choose another that has multi-attacks.<br/><br/>

Once the contract is fulfilled, use Mando's Disintegrate on General Grievous first.
""", video: #""#),

Counter(team: "SEP_NUTE_3v3", counter: "GEONOSIANS_3v3", hard: true, description: 
"""
Also works well against Wat Tambor.
""", video: #""#),

Counter(team: "SEP_NUTE_3v3", counter: "REBELS_CLS_3v3", hard: true, description: "", video: #""#),

Counter(team: "SEP_NUTE_3v3", counter: "RESISTANCE_FINN_3v3_POERT", hard: true, description: 
"""
Requires Finn's Balanced Tactics zeta and requires Poe Dameron to be fast enough to taunt early enough to get this team's Expose train rolling.
""", video: #""#),

Counter(team: "SEP_NUTE_3v3", counter: "EMPIRE_TARKIN_3v3", hard: false, description: "", video: #""#),

Counter(team: "SEP_NUTE_3v3", counter: "REBELS_CLS_3v3_CHAZE", hard: false, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_3v3_W_MALAK", counter: "THRAWN_3v3_HANCHEWIE", hard: false, description: 
"""
You need better relics on Han Solo than Bastila Shan (Fallen)  Hopefully, kill BSF with Han Solo's opening attack, he attacks through her foresight.  Use Grand Admiral Thrawn's Fracture on Darth Revan.  Then kill Darth Malak.
""", video: #""#),

Counter(team: "SITH_EMPIRE_3v3_WO_MALAK", counter: "NS_MT_3v3", hard: false, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_3v3_WO_MALAK", counter: "JEDI_REVAN_3v3_GMYJKA", hard: false, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_3v3_WO_MALAK", counter: "QIRA_NEST_HODA", hard: false, description: "", video: #""#),

Counter(team: "SITH_EMPIRE_3v3_WO_MALAK", counter: "REBELS_CLS_3v3", hard: false, description: "", video: #""#),

Counter(team: "SITH_MAUL_3v3", counter: "SEP_DROIDS_3v3", hard: true, description: "", video: #""#),

Counter(team: "SITH_NIHILUS_3v3", counter: "MALAK_SOLO", hard: false, description: 
"""
Requires high speed and/or a zeta on Darth Malak's Jaws of Life to get his Life Drain ability as much as possible.<br/><br/>

This should work because Darth Nihilus' leadership ability removes their Protection and adds it as Health, and Malak's Life Drain deals damage equal to 100% of an enemy's health.<br/><br/>

Also, since this is a 3v3 battle, you should be able to defeat them before Nihilus has a chance to use Annihilate.
""", video: #""#),

Counter(team: "SITH_TRI_3v3", counter: "MAGMA_3v3", hard: true, description: 
"""
Use Grand Admiral Thrawn's Fracture on Darth Traya, and later use Thrawn's basic attack on Nihilus to keep him stunned.  Use Death Trooper's AoE to increase cooldowns, basics on Darth Sion until he's dead, then Terminate to Deathmark the other enemies.  Magmatrooper will only attack Darth Sion with his basic, because it causes two attacks and getting pain will gain a lot of turn meter.
""", video: #""#),

Counter(team: "SITH_TRI_3v3", counter: "NS_ASAJJ_3v3_OLDDAKAZOMBIE", hard: true, description: "", video: #""#),

Counter(team: "SITH_TRI_3v3", counter: "REBELS_CLS", hard: false, description: "", video: #""#),

Counter(team: "SITH_TRI_3v3", counter: "VADER_3v3", hard: false, description: "", video: #""#),

Counter(team: "VADER_3v3", counter: "EMPIRE_THRAWN_3v3", hard: false, description: 
"""
Add two other Empire characters!  The Darth Vader L is contingent on dealing out damage over time, so this counter works because Grand Admiral Thrawn's leadership ability provides turn meter when resisting or suffering a debuff, protection when losing a status effect, and an ability to debuff characters.  Vader's leadership will be feeding your team turn meter.
""", video: #""#),

Counter(team: "VADER_3v3", counter: "FO_KRU_3v3", hard: false, description: "", video: #""#),

Counter(team: "VADER_3v3", counter: "SEP_DROIDS_NONGRIEVIOUS_3v3", hard: false, description: "", video: #""#),

]
}
