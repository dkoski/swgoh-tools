//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation

/// Represents the json file built by https://github.com/Crinolo/swgoh-stat-calc-dataBuilder.git as downloaded from https://swgoh-stat-calc.glitch.me/gameData.json (see download_reference_data.sh)
private struct GameStatistics : Codable {
    
    let modSetData: [Int:ModSetData]
    let crTables: CRTables
    let gpTables: GalacticPowerTable
    let gearData: [String:GearData]
    let unitData: [String:UnitData]
    let relicData: [String:RelicData]

    struct ModSetData : Codable {
        /// which stat
        let id: Int
        
        /// how many in the set
        let count: Int
        
        /// how much it is worth
        let value: Int
    }

    struct CRTables : Codable {
        let abilityLevel: [Int:Int]
        let unitLevel: [Int:Int]
        let gearLevel: [Int:Int]
        let relicTier: [Int:Int]
        let relicTierLevel: [Int:Double]
        let crewRarity: [Int:Int]
        let gearPiece: [Int:Int]
        let crewlessAbility: [Int:Double]
        let shipRarity: [Int:Double]
        
        let modRarityLevel: [Int:[Int:Int]]

        let agility_role_support_mastery: [Int:Double]
        let strength_role_tank_mastery: [Int:Double]
        let intelligence_role_tank_mastery: [Int:Double]
        let agility_role_attacker_mastery: [Int:Double]
        let strength_role_healer_mastery: [Int:Double]
        let intelligence_role_healer_mastery: [Int:Double]
        let strength_role_attacker_mastery: [Int:Double]
        let intelligence_role_support_mastery: [Int:Double]
        let intelligence_role_attacker_mastery: [Int:Double]
        let agility_role_tank_mastery: [Int:Double]
        let agility_role_healer_mastery: [Int:Double]
        let strength_role_support_mastery: [Int:Double]

        func masteryTable(_ mastery: String) -> [Int:Double] {
            switch mastery {
            case "agility_role_support_mastery": return agility_role_support_mastery
            case "strength_role_tank_mastery": return strength_role_tank_mastery
            case "intelligence_role_tank_mastery": return intelligence_role_tank_mastery
            case "agility_role_attacker_mastery": return agility_role_attacker_mastery
            case "strength_role_healer_mastery": return strength_role_healer_mastery
            case "intelligence_role_healer_mastery": return intelligence_role_healer_mastery
            case "strength_role_attacker_mastery": return strength_role_attacker_mastery
            case "intelligence_role_support_mastery": return intelligence_role_support_mastery
            case "intelligence_role_attacker_mastery": return intelligence_role_attacker_mastery
            case "agility_role_tank_mastery": return agility_role_tank_mastery
            case "agility_role_healer_mastery": return agility_role_healer_mastery
            case "strength_role_support_mastery": return strength_role_support_mastery
            default: fatalError("unknown mastery: \(mastery)")
            }
        }

        enum CodingKeys: String, CodingKey {
            case abilityLevel = "abilityLevelCR"
            case unitLevel = "unitLevelCR"
            case relicTier = "relicTierCR"
            case gearLevel = "gearLevelCR"
            case crewRarity = "crewRarityCR"
            case relicTierLevel = "relicTierLevelFactor"
            case gearPiece = "gearPieceCR"
            case crewlessAbility = "crewlessAbilityFactor"
            case shipRarity = "shipRarityFactor"
            case modRarityLevel = "modRarityLevelCR"

            case agility_role_support_mastery = "agility_role_support_mastery"
            case strength_role_tank_mastery = "strength_role_tank_mastery"
            case intelligence_role_tank_mastery = "intelligence_role_tank_mastery"
            case agility_role_attacker_mastery = "agility_role_attacker_mastery"
            case strength_role_healer_mastery = "strength_role_healer_mastery"
            case intelligence_role_healer_mastery = "intelligence_role_healer_mastery"
            case strength_role_attacker_mastery = "strength_role_attacker_mastery"
            case intelligence_role_support_mastery = "intelligence_role_support_mastery"
            case intelligence_role_attacker_mastery = "intelligence_role_attacker_mastery"
            case agility_role_tank_mastery = "agility_role_tank_mastery"
            case agility_role_healer_mastery = "agility_role_healer_mastery"
            case strength_role_support_mastery = "strength_role_support_mastery"
        }
    }
    
    struct GalacticPowerTable : Codable {
        let shipLevel: [Int:Int]
        let abilityLevelGP: [Int:Int]
        let shipAbilityLevelGP: [Int:Int]
        let unitLevelGP: [Int:Int]
        let crewSizeFactor: [Int:Double]
        let relicTierLevelFactor: [Int:Double]
        let gearLevelGP: [Int:Int]
        let relicTierGP: [Int:Int]
        let unitRarityGP: [Int:Int]
        let shipRarityFactor: [Int:Double]
        let abilitySpecialGP: [String:Int]
        let modRarityLevelTierGP: [Int:[Int:[Int:Int]]]
        let gearPieceGP: [Int:[Int:Int]]
        let crewlessAbilityFactor: [Int:Double]

        enum CodingKeys: String, CodingKey {
            case shipLevel = "shipLevelGP"
            case abilityLevelGP = "abilityLevelGP"
            case shipAbilityLevelGP = "shipAbilityLevelGP"
            case unitLevelGP = "unitLevelGP"
            case crewSizeFactor = "crewSizeFactor"
            case relicTierLevelFactor = "relicTierLevelFactor"
            case gearLevelGP = "gearLevelGP"
            case relicTierGP = "relicTierGP"
            case unitRarityGP = "unitRarityGP"
            case shipRarityFactor = "shipRarityFactor"
            case abilitySpecialGP = "abilitySpecialGP"
            case modRarityLevelTierGP = "modRarityLevelTierGP"
            case gearPieceGP = "gearPieceGP"
            case crewlessAbilityFactor = "crewlessAbilityFactor"
        }
    }
    
    struct GearData : Codable {
        let stats: [Int:Int]
    }

    struct RelicData : Codable {
        let stats: [Int:Int]
        let gms: [Int:Int]
    }
    
    struct UnitData : Codable {
        let combatType: Int
        let primaryStat: Int
        let gearLevel: [Int:GearLevel]?
        let growthModifiers: [Int:[Int:Int]]
        
        let skills: [Skill]?
        let relic: [Int:String]?
        let masteryModifierID: String?
        
        let crewStats: [Int:Int]?
        let crew: [String]?
        let shipStats: [Int:Int]?
        
        func statsBuilder(gearLevel: Int, stars: Int) -> StatsBuilder {
            let base: [Int:Int]
            if let gear = self.gearLevel {
                base = gear[gearLevel]!.stats
            } else {
                base = self.shipStats!
            }
            return StatsBuilder(base: base,
                         growthModifiers: self.growthModifiers[stars]!,
                         masteryModifierID: self.masteryModifierID)
        }
        
        enum CodingKeys: String, CodingKey {
            case combatType
            case primaryStat
            case gearLevel = "gearLvl"
            case growthModifiers
            case skills
            case relic
            case masteryModifierID
            case crewStats
            case crew
            case shipStats = "stats"
        }
        
        struct GearLevel : Codable {
            let gear: [String]
            let stats: [Int:Int]
        }
        
        struct Skill : Codable {
            let id: String
            let maxTier: Int
            let powerOverrideTags: [Int:String]
            let isZeta: Bool
        }
    }
}

/// mapping from the statID to a name.  based on https://swgoh-stat-calc.glitch.me/lang/statEnum.json  Note that some of these are misleading, e.g. Accuracy = Potency.  See also https://swgoh-stat-calc.glitch.me/lang/eng_us.json
fileprivate let statNames = [ "none", "health", "strength", "agility", "intelligence", "speed", "attackDamage", "abilityPower", "armor", "suppression", "armorPenetration", "suppressionPenetration", "dodgeRating", "deflectionRating", "attackCriticalRating", "abilityCriticalRating", "criticalDamage", "accuracy", "resistance", "dodgePercentAdditive", "deflectionPercentAdditive", "attackCriticalPercentAdditive", "abilityCriticalPercentAdditive", "armorPercentAdditive", "suppressionPercentAdditive", "armorPenetrationPercentAdditive", "suppressionPenetrationPercentAdditive", "healthSteal", "maxShield", "shieldPenetration", "healthRegen", "attackDamagePercentAdditive", "abilityPowerPercentAdditive", "dodgeNegatePercentAdditive", "deflectionNegatePercentAdditive", "attackCriticalNegatePercentAdditive", "abilityCriticalNegatePercentAdditive", "dodgeNegateRating", "deflectionNegateRating", "attackCriticalNegateRating", "abilityCriticalNegateRating", "offense", "defense", "defensePenetration", "evasionRating", "criticalRating", "evasionNegateRating", "criticalNegateRating", "offensePercentAdditive", "defensePercentAdditive", "defensePenetrationPercentAdditive", "evasionPercentAdditive", "evasionNegatePercentAdditive", "criticalChancePercentAdditive", "criticalNegateChancePercentAdditive", "maxHealthPercentAdditive", "maxShieldPercentAdditive", "speedPercentAdditive", "counterAttackRating", "taunt", "", "mastery" ]

// based on https://github.com/Crinolo/swgoh-stat-calc revision c7ab20b2d7484febb1d81f1584f95659070361ce

private let gameData: GameStatistics = {
    let data = try! Data(contentsOf: Bundle(for: ModelClass.self).url(forResource: "gameData", withExtension: "json")!)
    let gameStatistics = try! JSONDecoder().decode(GameStatistics.self, from: data)
    return gameStatistics
}()

private let skillData: [String:[String:GameStatistics.UnitData.Skill]] = {
    Dictionary(uniqueKeysWithValues: gameData.unitData.map { (id, unitData) -> (String, [String:GameStatistics.UnitData.Skill]) in
        if let skillData = unitData.skills {
            return (id, Dictionary(uniqueKeysWithValues: skillData.map { ($0.id, $0) }))
        } else {
            return (id, [:])
        }
    })
}()

/// computations are done in fixed point math using this as scale
private let gameScale = 100_000_000

private func floorScale(_ value: Int) -> Int {
    // a lot of the math is done on scaled numbers
    return (value / gameScale) * gameScale
}

private struct StatsBuilder {
    var base: [Int:Int]
    var growthModifiers: [Int:Int]
    var gear = [Int:Int]()
    var modStats = [Int:Int]()
    
    var crew = [Int:Int]()

    let masteryModifierID: String?
    
    func describe(table: [Int:Int]) -> String {
        var result = ""
        for (statID, value) in table {
            result += "  \(statNames[statID]): \(value)\n"
        }
        return result
    }
    
    var description: String {
        var result = "StatsBuilder:\n"
        
        result += "\nBase:\n"
        result += describe(table: base)

        result += "\nGear:\n"
        result += describe(table: gear)

        result += "\nMods:\n"
        result += describe(table: modStats)
        
        return result
    }
        
    fileprivate mutating func add(gear: Model.Unit.Equipment) {
        let gearData = gameData.gearData[gear.equipmentId]!
        for (statID, value) in gearData.stats {
            switch statID {
            case 2, 3, 4:
                // primary stat, applies before mods
                self.base[statID, default: 0] += value
                
            default:
                // secondary stat, applies after mods
                self.gear[statID, default: 0] += value
            }
        }
    }
    
    fileprivate mutating func add(relic: GameStatistics.RelicData) {
        for (statID, value) in relic.stats {
            base[statID, default: 0] += value
        }
        for (statID, value) in relic.gms {
            growthModifiers[statID, default: 0] += value
        }
    }
        
    fileprivate mutating func finalizeBaseStats(level: Int, unitID: String) {
        // from calculateBaseStats in the js
        
        for statId in 2 ... 4 {
            // str, agi, tac
            base[statId, default: 0] += floorScale(growthModifiers[statId, default: 0] * level)
        }
        
        if let mastery = base[61], mastery > 2 {
            // relic (mastery level)
            let mms = gameData.crTables.masteryTable(masteryModifierID!)
            for (statID, value) in mms {
                base[statID, default: 0] += Int(Double(mastery) * value)
            }
        }
        
        func scale(statID: Int, _ factor: Double) -> Int {
            Int(Double(base[statID, default: 0]) * factor)
        }
                
        // health = str * 18
        base[1, default: 0] += base[2, default: 0] * 18
        
        // Ph. Damage += MainStat * 1.4
        base[6] = floorScale(base[6, default: 0] + scale(statID: gameData.unitData[unitID]!.primaryStat, 1.4))

        // Sp. Damage += TAC * 2.4
        base[7] = floorScale(base[7, default: 0] + scale(statID: 4, 2.4))

        // Armor += STR*0.14 + AGI*0.07
        base[8] = floorScale(base[8, default: 0] + scale(statID: 2, 0.14) + scale(statID: 3, 0.07))

        // Resistance += TAC * 0.1
        base[9] = floorScale(base[9, default: 0] + scale(statID: 4, 0.1))
        
        // Ph. Crit += AGI * 0.4
        base[14] = floorScale(base[14, default: 0] + scale(statID: 3, 0.4))

        // add hard-coded minimums or potentially missing stats
        base[12, default: 0] += 24 * 100_000_000
        base[13, default: 0] += 24 * 100_000_000
        base[15, default: 0] += 0
        base[16, default: 0] += 150 * 1_000_000
        base[18, default: 0] += 15 * 1_000_000
    }
    
    enum ModInstruction {
        case add(Int)
        case addBase(Int)
        
        func apply(table: inout [Int:Int], base: [Int:Int], value: Int) {
            func scale(_ statID: Int, _ factor: Double) -> Int {
                Int(Double(base[statID, default: 0]) * factor)
            }

            switch self {
            case .add(let newStatID):
                // modStats += value
                table[newStatID, default: 0] += value
            case .addBase(let newStatID):
                // modStats += base * value / gameScale (in float)
                table[newStatID] = Int(table[newStatID, default: 0] + scale(newStatID, Double(value) / 100_000_000))
            }
        }
    }
    
    // table from the end of calculateModStats
    static let modInstruction: [Int:[ModInstruction]] = [
        41: [ .add(6), .add(7) ], // offense
        42: [ .add(8), .add(9) ], // defense
        53: [ .add(21), .add(22) ], // CC
        54: [ .add(35), .add(36) ], // crit avoid

        48: [ .addBase(6), .addBase(7) ], // offense %
        49: [ .addBase(8), .addBase(9) ], // defense %
        55: [ .addBase(1) ], // health %
        56: [ .addBase(28) ], // prot %
        57: [ .addBase(5) ], // speed %
    ]
    
    fileprivate mutating func addMods(mods: [Model.Mod]) {
        struct SetCounts {
            var count = 0
            var maxLevel = 0
        }
        
        // figure out what sets are present
        var setCounts = [Int:SetCounts]()
        
        for mod in mods {
            setCounts[mod.set, default: SetCounts()].count += 1
            if mod.level == 15 {
                setCounts[mod.set, default: SetCounts()].maxLevel += 1
            }
        }
        
        // now build up the model of the secondaries
        var rawModStats = [Int:Int]()
        
        @inline(__always)
        func addStat(_ stat: Model.Mod.Stat) {
            let scaledValue: Int
            switch stat.unitStat {
            case 1, 5, 28, 41, 42:
                // flat stats: health, speed, protection, offense, defense
                scaledValue = Int(stat.value * 1e8)
                
            default:
                // percent stats
                scaledValue = Int(stat.value * 1e6)
            }

            rawModStats[stat.unitStat, default: 0] += scaledValue
        }
        
        // accumulate the stats into rawModStats
        for mod in mods {
            addStat(mod.primaryStat)
            for secondary in mod.secondaryStat {
                addStat(secondary)
            }
        }
        
        // add set bonuses
        for (set, counts) in setCounts {
            let setConfiguration = gameData.modSetData[set]!
            let multiplier = counts.count / setConfiguration.count + counts.maxLevel / setConfiguration.count
            rawModStats[setConfiguration.id, default: 0] += setConfiguration.value * multiplier
        }
        
        // compute the final values -> modStats
        for (statID, value) in rawModStats {
            let instructions = StatsBuilder.modInstruction[statID] ?? [ .add(statID) ]
            for instruction in instructions {
                instruction.apply(table: &modStats, base: base, value: value)
            }
        }
    }
    
    enum FormatInstruction {
        case percent(scale: Double, offset: Double)
        case levelBased(scale: Double, offset: Double, shipScale: Double, shipOffset: Double)
        
        func compute(value: Double, level: Int, isShip: Bool) -> Double {
            switch self {
            case let .percent(scale: scale, offset: offset):
                return value / scale + offset
                
            case let .levelBased(scale: scale, offset: offset, shipScale: shipScale, shipOffset: shipOffset):
                let levelEffect: Double
                if !isShip {
                    levelEffect = offset + Double(level) * scale
                } else {
                    levelEffect = shipOffset + Double(level) * shipScale
                }
                return value / (levelEffect + value)
            }
        }
    }
    
    static let formatInstructions: [(Int, FormatInstruction)] = [
        // armor, resistance
        (8, .levelBased(scale: 7.5, offset: 0, shipScale: 5, shipOffset: 300)),
        (9, .levelBased(scale: 7.5, offset: 0, shipScale: 5, shipOffset: 300)),
        // evasion
        (12, .percent(scale: 1200, offset: 0)),
        (13, .percent(scale: 1200, offset: 0)),
        // crit ratings
        (14, .percent(scale: 2400, offset: 0.1)),
        (15, .percent(scale: 2400, offset: 0.1)),
        // acc
        (37, .percent(scale: 1200, offset: 0)),
        (38, .percent(scale: 1200, offset: 0)),
        // crit avoidance
        (39, .percent(scale: 2400, offset: 0)),
        (40, .percent(scale: 2400, offset: 0)),
    ]
    
    func format<S>(level: Int, isShip: Bool, statsFactory: ([Double]) -> S) -> S {
        func scale(_ value: Int) -> Double {
            Double(value) / Double(gameScale)
        }
        
        // compute the final stats, but we don't need all of them -- some are internal
        let count = 41
        var final = [Double](repeating: 0, count: count)
        
        if isShip {
            for statID in 0 ..< count {
                final[statID] = scale(base[statID, default: 0] + crew[statID, default: 0])
            }
        } else {
            for statID in 0 ..< count {
                final[statID] = scale(base[statID, default: 0] + gear[statID, default: 0] + modStats[statID, default: 0])
            }
        }
        
        // a percent value that needs to be converted
        for (statID, instruction) in StatsBuilder.formatInstructions {
            final[statID] = instruction.compute(value: final[statID], level: level, isShip: isShip)
        }
                        
        // crit chance and avoidance need some values combined
        final[14] += final[21]
        final[15] += final[22]
        final[39] += final[35]
        final[40] += final[36]
        
        return statsFactory(final)
    }

}

/// statis information about a unit including all gear, mods, etc.
public struct UnitStats : Codable {
    
    let health: Int
    let protection: Int
    let speed: Int
    
    let criticalDamage: Int
    let potency: Double
    let tenacity: Double
    let healthSteal: Double
    
    let physicalDamage: Int
    let physicalCritChance: Double
    let armorPenetration: Int
    let physicalAccuracy: Double
    
    let armor: Double
    let dodge: Double
    let physicalCritAvoidance: Double
    
    let specialDamage: Int
    let specialCritChance: Double
    let resistancePenetration: Int
    let specialAccuracy: Double
    
    let resistance: Double
    let deflectionChance: Double
    let specialCritAvoidance: Double
    
    // https://gaming-fans.com/2018/11/swgoh-effective-health-pool-its-impact-on-galaxy-of-heroes/
    var effectiveHealth: Int { Int(Double(health+protection) / (1-armor)) }
    
    var effectiveDamage: Int { Int(Double(physicalDamage) * (1.0 + physicalCritChance * (Double(criticalDamage)/100.0 - 1.0))) }
    
    public enum CodingKeys: String, CodingKey {
        case health
        case protection = "prot"
        case speed = "spd"
        case criticalDamage = "cd"
        case potency = "pot"
        case tenacity = "ten"
        case healthSteal = "hs"
        case physicalDamage = "dmg"
        case physicalCritChance = "cc"
        case armorPenetration = "ap"
        case physicalAccuracy = "acc"
        case armor = "arm"
        case dodge
        case physicalCritAvoidance = "ca"
        case specialDamage = "sdmg"
        case specialCritChance = "scc"
        case resistancePenetration = "rp"
        case specialAccuracy = "sacc"
        case resistance = "res"
        case deflectionChance = "dc"
        case specialCritAvoidance = "sca"
    }
    
    init(values: [Double]) {
        // see statNames for mapping
        func r(_ value: Double) -> Double {
            Double(Int(value * 1000)) / 1000
        }
        health = Int(values[1])
        protection = Int(values[28])
        speed = Int(values[5])
        
        criticalDamage = Int(values[16] * 100)
        potency = r(values[17])
        tenacity = r(values[18])
        healthSteal = r(values[27])
        
        physicalDamage = Int(values[6])
        physicalCritChance = r(values[14])
        armorPenetration = Int(values[10])
        physicalAccuracy = r(values[37])
        
        armor = r(values[8])
        dodge = r(values[12])
        physicalCritAvoidance = r(values[39])
        
        specialDamage = Int(values[7])
        specialCritChance = r(values[15])
        resistancePenetration = Int(values[11])
        specialAccuracy = r(values[38])
        
        resistance = r(values[9])
        deflectionChance = r(values[13])
        specialCritAvoidance = r(values[40])
    }
    
    public init(health: Int, protection: Int, speed: Int, criticalDamage: Int, potency: Double, tenacity: Double, healthSteal: Double, physicalDamage: Int, physicalCritChance: Double, armorPenetration: Int, physicalAccuracy: Double, armor: Double, dodge: Double, physicalCritAvoidance: Double, specialDamage: Int, specialCritChance: Double, resistancePenetration: Int, specialAccuracy: Double, resistance: Double, deflectionChance: Double, specialCritAvoidance: Double) {
        self.health = health
        self.protection = protection
        self.speed = speed
        self.criticalDamage = criticalDamage
        self.potency = potency
        self.tenacity = tenacity
        self.healthSteal = healthSteal
        self.physicalDamage = physicalDamage
        self.physicalCritChance = physicalCritChance
        self.armorPenetration = armorPenetration
        self.physicalAccuracy = physicalAccuracy
        self.armor = armor
        self.dodge = dodge
        self.physicalCritAvoidance = physicalCritAvoidance
        self.specialDamage = specialDamage
        self.specialCritChance = specialCritChance
        self.resistancePenetration = resistancePenetration
        self.specialAccuracy = specialAccuracy
        self.resistance = resistance
        self.deflectionChance = deflectionChance
        self.specialCritAvoidance = specialCritAvoidance
    }
    
    public init() {
        self.health = 0
        self.protection = 0
        self.speed = 0
        self.criticalDamage = 0
        self.potency = 0
        self.tenacity = 0
        self.healthSteal = 0
        self.physicalDamage = 0
        self.physicalCritChance = 0
        self.armorPenetration = 0
        self.physicalAccuracy = 0
        self.armor = 0
        self.dodge = 0
        self.physicalCritAvoidance = 0
        self.specialDamage = 0
        self.specialCritChance = 0
        self.resistancePenetration = 0
        self.specialAccuracy = 0
        self.resistance = 0
        self.deflectionChance = 0
        self.specialCritAvoidance = 0
    }

    public static func -(lhs: UnitStats, rhs: UnitStats) -> UnitStats {
        UnitStats(health: lhs.health - rhs.health, protection: lhs.protection - rhs.protection, speed: lhs.speed - rhs.speed, criticalDamage: lhs.criticalDamage - rhs.criticalDamage, potency: lhs.potency - rhs.potency, tenacity: lhs.tenacity - rhs.tenacity, healthSteal: lhs.healthSteal - rhs.healthSteal, physicalDamage: lhs.physicalDamage - rhs.physicalDamage, physicalCritChance: lhs.physicalCritChance - rhs.physicalCritChance, armorPenetration: lhs.armorPenetration - rhs.armorPenetration, physicalAccuracy: lhs.physicalAccuracy - rhs.physicalAccuracy, armor: lhs.armor - rhs.armor, dodge: lhs.dodge - rhs.dodge, physicalCritAvoidance: lhs.specialCritAvoidance - rhs.physicalCritAvoidance, specialDamage: lhs.specialDamage - rhs.specialDamage, specialCritChance: lhs.specialCritChance - rhs.specialCritChance, resistancePenetration: lhs.resistancePenetration - rhs.resistancePenetration, specialAccuracy: lhs.specialAccuracy - rhs.specialAccuracy, resistance: lhs.resistance - rhs.resistance, deflectionChance: lhs.deflectionChance - rhs.deflectionChance, specialCritAvoidance: lhs.specialCritAvoidance - rhs.specialCritAvoidance)
    }
    
    public static func +(lhs: UnitStats, rhs: UnitStats) -> UnitStats {
        UnitStats(health: lhs.health + rhs.health, protection: lhs.protection + rhs.protection, speed: lhs.speed + rhs.speed, criticalDamage: lhs.criticalDamage + rhs.criticalDamage, potency: lhs.potency + rhs.potency, tenacity: lhs.tenacity + rhs.tenacity, healthSteal: lhs.healthSteal + rhs.healthSteal, physicalDamage: lhs.physicalDamage + rhs.physicalDamage, physicalCritChance: lhs.physicalCritChance + rhs.physicalCritChance, armorPenetration: lhs.armorPenetration + rhs.armorPenetration, physicalAccuracy: lhs.physicalAccuracy + rhs.physicalAccuracy, armor: lhs.armor + rhs.armor, dodge: lhs.dodge + rhs.dodge, physicalCritAvoidance: lhs.specialCritAvoidance + rhs.physicalCritAvoidance, specialDamage: lhs.specialDamage + rhs.specialDamage, specialCritChance: lhs.specialCritChance + rhs.specialCritChance, resistancePenetration: lhs.resistancePenetration + rhs.resistancePenetration, specialAccuracy: lhs.specialAccuracy + rhs.specialAccuracy, resistance: lhs.resistance + rhs.resistance, deflectionChance: lhs.deflectionChance + rhs.deflectionChance, specialCritAvoidance: lhs.specialCritAvoidance + rhs.specialCritAvoidance)
    }

    public subscript(_ stat: Model.Mod.Characteristic) -> Double {
        switch stat {
        case .health: return Double(health)
        case .protection: return Double(protection)
        case .defense: return armor
        case .offense: return Double(physicalDamage > specialDamage ? physicalDamage : specialDamage)
        case .speed: return Double(speed)
        case .cd: return Double(criticalDamage)
        case .cc: return physicalDamage > specialDamage ? physicalCritChance : specialCritChance
        case .ca: return physicalCritAvoidance
        case .acc: return physicalAccuracy
        case .potency: return potency
        case .tenacity: return tenacity
        }
    }
    
    public func modStats(_ stats: [Model.Mod.Characteristic] = Model.Mod.Characteristic.allCases) -> [(Model.Mod.Characteristic, Double)] {
        stats.map { ($0, self[$0]) }
    }
}

/// returns the character ids for ships crew or nil if no crew
public func crewIds(unitId: String) -> [String]? {
    gameData.unitData[unitId]?.crew ?? nil
}

public protocol StatsUnit {
    
    var id: String { get }
    
    var stars: Int { get }
    var level: Int { get }
    var gear: Int { get }
    var relic: Int { get }
    var isShip: Bool { get }

    var skills: [Model.Unit.Skill] { get }
    var mods: [Model.Mod] { get }
    var equipment: [Model.Unit.Equipment] { get }
}

private func computeCrewRating(_ crew: [StatsUnit], _ unit: StatsUnit) -> Int {
    let crTables = gameData.crTables
    let crewRating: Int
    if crew.count == 0 {
        // crewless
        let skillCrewRating = unit.skills.reduce(0) {
            $0 + ($1.id.hasPrefix("hardware") ? 0.696 : 2.46) * Double(crTables.abilityLevel[$1.tier]!)
        }
        
        crewRating = crTables.crewRarity[unit.stars]! +
            Int(3.5 * Double(crTables.unitLevel[unit.level]!) + skillCrewRating)
        
    } else {
        crewRating = crew.reduce(0) { current, c in
            var cr = crTables.unitLevel[c.level]! + crTables.crewRarity[c.stars]!
            cr += crTables.gearLevel[c.gear]!
            cr += (crTables.gearPiece[c.gear] ?? 0) * (c.equipment.count)
            cr += c.skills.reduce(0) { $0 + crTables.abilityLevel[$1.tier]! }
            cr += c.mods.reduce(0) { $0 + crTables.modRarityLevel[$1.pips]![$1.level]! }
            
            if c.relic > 2 {
                cr += crTables.relicTier[c.relic]!
                cr += Int(Double(c.level) * crTables.relicTierLevel[c.relic]!)
            }
            
            return current + cr
        }
    }
    return crewRating
}

public func calculateStats(unit: StatsUnit, mods: [Model.Mod]? = nil, crew: [StatsUnit] = []) -> UnitStats {
    _calculateStats(unit: unit, mods: mods, crew: crew, statsFactory: UnitStats.init(values:))
}

public func canCalculateStats(unit: StatsUnit) -> Bool {
    gameData.unitData[unit.id] != nil
}

func _calculateStats<S>(unit: StatsUnit, mods: [Model.Mod]? = nil, crew: [StatsUnit] = [], statsFactory: ([Double]) -> S) -> S {
    // start with stats based on gear level
    var stats = gameData.unitData[unit.id]!.statsBuilder(gearLevel: unit.gear, stars: unit.stars)
    
    if !unit.isShip {
        // then add additional gear, e.g. 12.5
        for gear in unit.equipment {
            stats.add(gear: gear)
        }

        // set up relic data: mastery level + growth modifiers (from getCharRawStats)
        if unit.relic > 2 {
            let relicId = gameData.unitData[unit.id]!.relic![unit.relic]!
            let relicData = gameData.relicData[relicId]!
            stats.add(relic: relicData)
        }
        
        stats.finalizeBaseStats(level: unit.level, unitID: unit.id)
        stats.addMods(mods: mods ?? unit.mods)

        return stats.format(level: unit.level, isShip: unit.isShip, statsFactory: statsFactory)
        
    } else {
        // a ship
        
        if let expectedCrew = gameData.unitData[unit.id]?.crew {
            assert(crew.count == expectedCrew.count)
        }
        
        let crewRating = computeCrewRating(crew, unit)
        
        let statMultiplier = Int(gameData.crTables.shipRarity[unit.stars]! * Double(crewRating))
        
        for (statId, value) in gameData.unitData[unit.id]!.crewStats! {
            switch statId {
            case 0 ..< 16, 28:
                stats.crew[statId] = floorScale(value * statMultiplier)
            default:
                stats.crew[statId] = value * statMultiplier
            }
        }
        
        stats.finalizeBaseStats(level: unit.level, unitID: unit.id)
        
        return stats.format(level: unit.level, isShip: unit.isShip, statsFactory: statsFactory)
    }

}

/// compute the gp for a given unit + crew (for ships).  note: this does not compute the proper gp for characters with the ultimate ability
public func calculateGP(unit: StatsUnit, crew: [StatsUnit] = []) -> Int {
    let gpTables = gameData.gpTables
    
    func skillGP(_ skill: Model.Unit.Skill) -> Int {
        if let override = skillData[unit.id]![skill.id]?.powerOverrideTags[skill.tier] {
            return gpTables.abilitySpecialGP[override]!
        } else {
            return gpTables.abilityLevelGP[skill.tier] ?? 0
        }
    }
    
    if !unit.isShip {
        var gp = gpTables.unitLevelGP[unit.level]!
        gp += gpTables.unitRarityGP[unit.stars]!
        gp += gpTables.gearLevelGP[unit.gear]!
        
        gp += unit.equipment.reduce(0) { $0 + gpTables.gearPieceGP[unit.gear]![$1.slot]! }
        
        gp += unit.skills.reduce(0) { $0 + skillGP($1) }
        
        // TODO the swgoh.help doesn't seem to have the ultimate ability score.  here is the line from crinolo:
        // if (char.purchacedAbilityId) gp += char.purchacedAbilityId.length * gpTables.abilitySpecialGP.ultimate;
        
        gp += unit.mods.reduce(0) { $0 + gpTables.modRarityLevelTierGP[$1.pips]![$1.level]![$1.tier]! }
        
        if unit.relic > 2 {
            gp += gpTables.relicTierGP[unit.relic]!
            gp += Int(Double(unit.level) * gpTables.relicTierLevelFactor[unit.relic]!)
        }
        
        return Int(Double(gp) * 1.5)
        
    } else {
        // a ship
        
        if let expectedCrew = gameData.unitData[unit.id]?.crew {
            assert(crew.count == expectedCrew.count)
        }

        if crew.count == 0 {
            // crewless ship
            var ability = 0
            var reinforcement = 0
            for skill in unit.skills {
                if let override = skillData[unit.id]![skill.id]?.powerOverrideTags[skill.tier] {
                    if override.hasPrefix("reinforcement") {
                        reinforcement += gpTables.abilitySpecialGP[override]!
                    } else {
                        ability += gpTables.abilitySpecialGP[override]!
                    }
                } else {
                    ability += gpTables.abilityLevelGP[skill.tier]!
                }
            }
            
            let level = gpTables.unitLevelGP[unit.level]!
            var gp = Int((Double(level) * 3.5 + Double(ability) * 5.74 + Double(reinforcement) * 1.61) * gpTables.shipRarityFactor[unit.stars]!)
            gp += level + ability + reinforcement
            return Int(Double(gp) * 1.5)
            
        } else {
            // crewed
            var gp = crew.reduce(0) { $0 + calculateGP(unit: $1) }
            gp = Int(Double(gp) * gpTables.shipRarityFactor[unit.stars]! * gpTables.crewSizeFactor[crew.count]!)
            gp += gpTables.unitLevelGP[unit.level]!
            gp += unit.skills.reduce(0) { $0 + skillGP($1) }
            return Int(Double(gp) * 1.5)
        }
    }
}
