//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

/// placeholder class for looking up bundle info
public class ModelClass {
    
}

public struct Model {
    
    /// top level structure of the guild file, e.g. output of https://swgoh.gg/api/guild/46657/ (note that we no longer track this format exactly because we load from swgoh.help)
    public struct Guild : Codable, Comparable {
        
        public let info: Info
        public var players: [Player]
        
        enum CodingKeys: String, CodingKey {
            case info = "data"
            case players
        }
        
        public struct Info : Codable {
            public let gp: Int
            let id: Int
            let memberCount: Int
            public let name: String
            public let shortName: String?
            let profileCount: Int
            let rank: Int
            
            enum CodingKeys: String, CodingKey {
                case gp = "galactic_power"
                case id
                case memberCount = "member_count"
                case name
                case shortName
                case profileCount = "profile_count"
                case rank
            }
        }
            
        public func playersSortedByGP() -> [Player] {
            return players.sorted(by: { (p1, p2) -> Bool in
                return p1.stats.gp < p2.stats.gp
            })
        }
            
        public func allUnits() -> [String:Unit] {
            var units = [String:Unit]()
            for player in players {
                units.merge(player.units, uniquingKeysWith: { (u1, u2) -> Unit in
                    return u1.gp > u2.gp ? u1 : u2
                })
            }
            return units
        }
        
        public var name: String {
            return info.name
        }
        
        public var shortName: String {
            return info.shortName ?? "guild"
        }
        
        public var characterGP: Int {
            return players.reduce(0) { $0 + $1.stats.characterGP }
        }
        
        public var shipGP: Int {
            return players.reduce(0) { $0 + $1.stats.shipGP }
        }
            
        public static func < (lhs: Model.Guild, rhs: Model.Guild) -> Bool {
            lhs.name < rhs.name
        }
        
        public static func == (lhs: Model.Guild, rhs: Model.Guild) -> Bool {
            lhs.name == rhs.name
        }
    }
    
    public struct Mod : Hashable, Codable, CustomStringConvertible {
        public var pips: Int
        public let set: Int
        public var tier: Int
        public let slot: Int
        public var level: Int
        
        public var characteristic: Characteristic { Characteristic(set: set) }
        
        /// describes *which* characteristic it refers to
        public enum Characteristic : Int, Codable, Hashable, CustomStringConvertible, CaseIterable {
            case health, protection, defense, offense, speed
            case cd, cc, ca, acc
            case potency, tenacity
            
            init(set: Int) {
                switch set {
                case 1: self = .health
                case 2: self = .offense
                case 3: self = .defense
                case 4: self = .speed
                case 5: self = .cc
                case 6: self = .cd
                case 7: self = .potency
                case 8: self = .tenacity
                    
                default: fatalError("unkown mod set: \(set)")
                }
            }
            
            init(stat: Int) {
                switch stat {
                case 1, 55:     self = .health
                case 5:         self = .speed
                case 16:        self = .cd
                case 17:        self = .potency
                case 18:        self = .tenacity
                case 28, 56:    self = .protection
                case 41, 48:    self = .offense
                case 42, 49:    self = .defense
                case 52:        self = .acc
                case 53:        self = .cc
                case 54:        self = .ca

                default: fatalError("unsupported stat: \(stat)")
                }
            }
            
            public var description: String {
                switch self {
                case .health: return "health"
                case .protection: return "protection"
                case .defense: return "defense"
                case .offense: return "offense"
                case .speed: return "speed"
                case .cd: return "crit damage"
                case .cc: return "crit chance"
                case .ca: return "crit avoidance"
                case .acc: return "accuracy"
                case .potency: return "potency"
                case .tenacity: return "tenacity"
                }
            }
            
            public var isPercent: Bool {
                switch self {
                case .potency, .tenacity, .defense, .acc, .cc, .ca: return true
                default: return false
                }
            }
        }
        
        public struct Stat : Hashable, Codable, CustomStringConvertible {
            public var unitStat: Int
            public var value: Double
            public var roll: Int?
            
            public var characteristic: Characteristic { Characteristic(stat: unitStat) }
            
            public var description: String {
                "\(Mod.formatters[unitStat]!.string(from: value)) \(characteristic)"
            }
        }
                    
        public var primaryStat: Stat
        public var secondaryStat: [Stat]
                    
        var speed: Int {
            Int(secondaryStat.first(where: { $0.unitStat == 5})?.value ?? 0)
        }
        
        static public let slotNames = [
            "",
            "square",
            "arrow",
            "diamond",
            "triangle",
            "circle",
            "cross"
        ]
        
        static public let setNames = [
            "",
            "health",
            "offense",
            "defense",
            "speed",
            "critical chance",
            "critical damage",
            "potency",
            "tenacity",
        ]
                    
        static let percent: NumberFormatter = {
            let nf = NumberFormatter()
            nf.format = "+#.##'%'"
            return nf
        }()

        static let decimal: NumberFormatter = {
            let nf = NumberFormatter()
            nf.format = "+#"
            return nf
        }()

        static public let formatters = [
            1:  decimal,
            5:  decimal,
            16: percent,
            17: percent,
            18: percent,
            28: decimal,
            41: decimal,
            42: decimal,
            48: percent,
            49: percent,
            52: percent,
            53: percent,
            54: percent,
            55: percent,
            56: percent,
        ]
        
        static public let tiers = [ "", "A", "B", "C", "D", "E" ]
        
        /// return the active mod sets in a list of mods
        static func sets(mods: [Mod]) -> [Characteristic] {
            var counts = [Characteristic:Int]()
            for mod in mods {
                counts[mod.characteristic, default: 0] += 1
            }
                            
            var result = [Characteristic]()
            func append(_ characteristic: Characteristic, _ count: Int) {
                for _ in 0 ..< count {
                    result.append(characteristic)
                }
            }
            
            for (characteristic, count) in counts {
                switch characteristic {
                case .health, .defense, .cc, .potency, .tenacity:
                    append(characteristic, count / 2)
                case .offense, .cd, .speed:
                    append(characteristic, count / 4)
                default: fatalError("unexpected mod set: \(characteristic)")
                }
            }
            
            return result
        }
        
        public var description: String {
            var result = "\(Mod.setNames[set]) set: \(Mod.slotNames[slot]) \(primaryStat) \(level)\(Mod.tiers[tier]) \(pips) dot\n"
            
            for secondary in secondaryStat {
                result += "\(secondary) (\(secondary.roll!))\n"
            }
            
            return result
        }
    }
    
    public struct GearLevel : CustomStringConvertible, Comparable, Equatable {
        
        public let value: Int
        
        public init(gear: Int, relic: Int) {
            if relic > 2 {
                value = 20 + relic - 2
            } else {
                value = gear
            }
        }
        
        public init(_ value: Int) {
            self.value = value
        }
        
        /// distance (number of gear levels) to the other.  if self is > other this returns 0
        public func distance(_ other: GearLevel) -> Int {
            if self > other {
                return 0
            }
            
            if other.value > 20 {
                if self.value > 20 {
                    return other.value - self.value
                } else {
                    return (13 - self.value) + (other.value - 20)
                }
            } else {
                // other is 13 or less and so is self
                return other.value - self.value
            }
        }
        
        public var representedGearLevel: Int {
            value > 20 ? 13 : value
        }
        
        public var representedRelicLevel: Int {
            value > 20 ? value - 20 + 2 : 1
        }
        
        public static func < (lhs: Model.GearLevel, rhs: Model.GearLevel) -> Bool {
            lhs.value < rhs.value
        }
        
        public var description: String {
            value > 20 ? "R\(value - 20)" : value.description
        }
        
        public static let levels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 21, 22, 23, 24, 25, 26, 27, 28]
        
        public static let gearLevels: [GearLevel] = {
            levels.map { GearLevel($0) }
        }()
    }

    /// An instance of a Character -- these make up a Player's roster
    public struct Unit : StatsUnit, Codable, Hashable {
                                
        /// Info about a specific unit ability (e.g. a skill) as opposed to info about the skill itself (Model.Ability)
        public struct Skill : Codable {
            public let tier: Int
            public let id: String
            let ability: Model.Ability
            
            enum CodingKeys: String, CodingKey {
                case tier
                case id
            }
            
            public init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                
                self.tier = try values.decode(Int.self, forKey: .tier)
                let id = try values.decode(String.self, forKey: .id)
                self.id = id
                self.ability = Model.Ability.abilities[self.id] ?? Model.Ability.empty(unitId: "stand-in", id: id)
            }
            
            init(id: String, tier: Int = 0) {
                self.tier = tier
                self.id = id
                self.ability = Model.Ability.abilities[self.id]!
            }
            
            func structure() -> String {
                return "\(id): \(ability.name) = \(tier)"
            }
            
            public var maxTier: Int { ability.maxTier }
            public var isOmega: Bool { ability.isOmega }
            public var isZeta: Bool { ability.isZeta }
            public var name: String { ability.name }
            public var hasOmegaZeta: Bool { (isZeta || isOmega) && tier == ability.maxTier }
            
            public var abilityType: Model.Ability.AbilityType { ability.abilityType }
        }
        
        /// beyond the gear level, what equipment is in place
        public struct Equipment : Codable {
            let nameKey: String
            let equipmentId: String
            let slot: Int
        }
        
        public let gear: Int
        public let relic: Int
        public let level: Int
        public let name: String
        public let id: String
        public let gp: Int
        public let zetas: [String]
        public let zetaIds: [String]
        public let stars: Int
        public let stats: UnitStats
        
        public var skills: [Skill]
        public let mods: [Mod]
        public let equipment: [Equipment]
                
        // TODO need to get rid of the evaluation rules that use this
        public enum CodingKeys: String, CodingKey {
            case skills
            case mods
            case gear
            case relic
            case level
            case name
            case id
            case gp
            case zetas
            case zetaIds
            case stars
            case stats
            case equipment
            
            var name: String {
                switch self {
                case .skills: return "skills"
                case .mods: return "mods"
                case .gear: return "gear level"
                case .relic: return "relic"
                case .level: return "level"
                case .name: return "name"
                case .id: return "id"
                case .gp: return "power"
                case .zetas: return "zetas"
                case .zetaIds: return "zetaIds"
                case .stars: return "stars"
                case .stats: return "stats"
                case .equipment: return "equipment"
                }
            }
        }
               
        /// create an empty Unit
        public init(id: String, name: String) {
            self.id = id
            self.skills = []
            mods = []
            gear = 0
            level = 0
            self.name = name
            gp = 0
            zetas = []
            zetaIds = []
            stars = 0
            stats = UnitStats()
            relic = 0
            equipment = []
        }
        
        init(id: String, gearLevel: Int, relic: Int, level: Int, gp: Int, stars: Int, skills: [Skill], mods: [Mod], equipment: [Equipment], stats: UnitStats) {
            self.id = id
            self.skills = skills
            self.mods = mods
            self.gear = gearLevel
            self.relic = relic
            self.level = level
            self.name = Character.find(id).name
            self.gp = gp
            zetas = skills.filter { $0.isZeta && $0.tier == $0.maxTier }.map { $0.name }
            zetaIds = skills.filter { $0.isZeta && $0.tier == $0.maxTier }.map { $0.id }
            self.stars = stars
            self.stats = stats
            self.equipment = equipment
        }
                
        func value(_ key: CodingKeys) -> Int {
            switch key {
            case .gear: return gear
            case .level: return level
            case .gp: return gp
            case .stars: return stars
            default: fatalError("not an Int property")
            }
        }
        
        public var isShip: Bool { return character.ship }
        
        public var commonName: String {
            return commonNames[name] ?? name
        }
        
        public static func == (lhs: Unit, rhs: Unit) -> Bool {
            return lhs.id == rhs.id
        }
        
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
        
        public func structure() -> String {
            var result = "\(id): \(commonName)\n"
            for ability in skills {
                result.append("\t")
                result.append(ability.structure())
                result.append("\n")
            }
            return result
        }
        
        public var character: Character {
            return Character.find(id)
        }
        
        public var combinedGearLevel: GearLevel {
            GearLevel(gear: gear, relic: relic)
        }
        
        public var gearLevelDescription: String {
            GearLevel(gear: gear, relic: relic).description
        }
        
        public var zetasDescription: String {
            zetas.map { _ in "z" }.joined(separator: "")
        }
        
        public func description(images: Bool = false, zetas: Bool = true, gearBelow: Int = 0, starsBelow: Int = 0) -> String {
            var z = ""
            if zetas {
                for _ in self.zetas {
                    z += "z"
                }
            }
            let g = !isShip && gear < gearBelow ? " \(gearLevelDescription)" : ""
            let c = commonName.contains(" ") && !images ? "," : ""
            let stars = self.stars < starsBelow ? " \(self.stars)*" : ""
            
            let text = "\(z)\(commonName)\(stars)\(g)\(c)"
            
            if images {
                return "<div class=\"tot\"><img class=\"toI squad \(character.id)-i\"/><br/>\(text)</div>"
            } else {
                return text
            }
        }
    }
    
    public struct Character : Codable, Hashable, Comparable {
        
        public let abilityClasses: [String]
        public let alignment: Alignment
        public let id: String
        public let categories: [String]
        public let name: String
        public let role: String
        public var ship: Bool = false
        public let characterDescription: String
        public let gearLevels: [GearLevel]?
                
        /// see `imageURL` below
        private let _imageURL: String
        
        public struct GearLevel : Codable, Equatable {
            let tier: Int
            let gear: [String]
        }
        
        enum CodingKeys: String, CodingKey {
            case abilityClasses = "ability_classes"
            case alignment
            case id = "base_id"
            case categories
            case name
            case role
            case _imageURL = "image"
            case characterDescription = "description"
            case gearLevels = "gear_levels"
        }
                        
        public var imageURL: URL {
            // the value we get in the JSON is e.g. "/game-asset/u/AAYLASECURA/"
            _imageURL.starts(with: "http") ? URL(string: _imageURL)! : URL(string: "https://swgoh.gg" + _imageURL)!
        }
                
        public func matches<T: StringProtocol>(_ words: [T]) -> Bool {
            for match in words {
                if name.matchesWord(match) ||
                    role.matchesWord(match) ||
                    alignment.rawValue.matchesWord(match) ||
                    commonNames[name]?.matchesWord(match) ?? false ||
                    abilityClasses.contains(where: { $0.matchesWord(match) }) ||
                    categories.contains(where: { $0.matchesWord(match) }) {
                    // matching so far, continue
                } else {
                    return false
                }
            }
            
            // matched all words
            return true
        }
        
        public var commonName: String {
            return commonNames[name] ?? name
        }
        
        public var abilities: [Ability] {
            Ability.abilitiesByUnitId[self.id] ?? []
        }
                
        public static func find(_ name: String) -> Character {
            
            // id match
            if let character = characters[name] {
                return character
            }
            
            // name -> id mapping
            if let id = NameCache.get(name) {
                return characters[id]!
            }
            
            var resolvedId: String? = nil
            let lowercaseName = name.lowercased()

            // exact match first
            for character in characters.values {
                if character.name.lowercased() == lowercaseName || character.commonName.lowercased() == lowercaseName {
                    if let resolvedId = resolvedId { fatalError("duplicate unit for \(name): \(resolvedId) and \(character.id)")}
                    resolvedId = character.id
                }
            }
            
            if resolvedId == nil {
                // fall back to substring match
                for character in characters.values {
                    if character.name.lowercased().contains(lowercaseName) || character.commonName.lowercased().contains(lowercaseName) {
                        if let resolvedId = resolvedId { fatalError("duplicate unit for \(name): \(resolvedId) and \(character.id)")}
                        resolvedId = character.id
                    }
                }
            }

            if let resolvedId = resolvedId {
                NameCache.set(name, resolvedId)
                return characters[resolvedId]!
            }
                
            fatalError("unable to look up \(name)")
        }
        
        public var urlName: String {
            // convert Padmé Amidala -> padme-amidala
            name
                .lowercased()
                .replacingOccurrences(of: " ", with: "-")
                .replacingOccurrences(of: "\"", with: "")
                .replacingOccurrences(of: "(", with: "")
                .replacingOccurrences(of: ")", with: "")
                .folding(options: .diacriticInsensitive, locale: .current)
        }
        
        public static var all: [Character] { Array(characters.values) }
        
        /// characters that are not yet available but I want to reference them
        public static let extraCharacters: [(String, String, String)] = [
            //            ("Moff Gideon", "MOFFGIDEONS1", "/game-asset/u/EMPERORPALPATINE/"),
        ]
        
        public static let characters: [String:Model.Character] = {
            let characterData = try! Data(contentsOf: Bundle(for: ModelClass.self).url(forResource: "characters", withExtension: "json")!)
            var characters = try! JSONDecoder().decode([Character].self, from: characterData)

            let shipData = try! Data(contentsOf: Bundle(for: ModelClass.self).url(forResource: "ships", withExtension: "json")!)
            var ships = try! JSONDecoder().decode([Character].self, from: shipData).map { s -> Character in var s = s; s.ship = true; return s }
            
            characters.append(contentsOf: ships)
            
            for (name, id, url) in extraCharacters {
                let c = Character(abilityClasses: [], alignment: .dark, id: id, categories: [], name: name, role: "", characterDescription: "", gearLevels: nil, _imageURL: url)
                characters.append(c)
            }
            
            return Dictionary(uniqueKeysWithValues: characters.map { ($0.id, $0) })
        }()
        
        public static func matchesCategory(_ category: String, ship: Bool = false) -> [Character] {
            let result = characters.values.filter { $0.ship == ship && $0.categories.contains(category) }
            assert(!result.isEmpty)
            return result
        }

        public static var counts: [Alignment: Int] = {
            var result = [Alignment: Int]()
            for c in all {
                result[c.alignment, default: 0] += 1
            }
            return result
        }()
        
        public static func < (lhs: Character, rhs: Character) -> Bool {
            return lhs.name < rhs.name
        }
        
        public static func == (lhs: Character, rhs: Character) -> Bool {
            return lhs.id == rhs.id
        }
        
        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }
    }

    public struct Player : CustomStringConvertible, Codable, Hashable, Comparable, UnitProvider, Identifiable {

        public struct Stats : Codable {
            public let allyCode: Int
            public let name: String
            public let gp: Int
            public let characterGP: Int
            public let shipGP: Int
            
            enum CodingKeys: String, CodingKey {
                case allyCode
                case name
                case gp
                case characterGP
                case shipGP
            }
        }
        
        public var id: Int { stats.allyCode }

        public let stats: Stats
        public let units: [String:Unit]
        
        enum CodingKeys: String, CodingKey {
            case stats = "data"
            case units
        }
        
        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            stats = try values.decode(Stats.self, forKey: .stats)
            
            let units = try values.decode([Unit].self, forKey: .units)
            self.units = Dictionary(uniqueKeysWithValues: units.map({ ($0.id, $0 )}))
        }
        
        init(from lightPlayer: LightPlayer) {
            stats = Stats(allyCode: lightPlayer.allyCode, name: lightPlayer.name, gp: 0, characterGP: 0, shipGP: 0)
            units = [:]
        }
        
        public init(name: String, allyCode: Int, units: [String:Unit]) {
            self.units = units
            
            var charsGP = 0
            var shipsGP = 0
            
            for unit in units.values {
                if unit.isShip {
                    shipsGP += unit.gp
                } else {
                    charsGP += unit.gp
                }
            }
            
            self.stats = Stats(allyCode: allyCode, name: name, gp: charsGP + shipsGP, characterGP: charsGP, shipGP: shipsGP)
        }
        
        // TODO rename to ally code? use ally code formatting?
        public var code: String { return stats.allyCode.description }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(stats, forKey: .stats)
            try container.encode(Array(units.values), forKey: .units)
        }
        
        public subscript(_ id: String) -> Unit {
            // id match
            if let unit = units[id] {
                return unit
            }
            
            // name fallback
            let character = Character.find(id)
                    
            if let unit = units[character.id] {
                return unit
            }
            
            // not found, return empty unit
            return Unit(id: character.id, name: character.name)
        }
        
        public func crew(unit: Unit) -> [Unit] {
            if let crewIds = crewIds(unitId: unit.id) {
                return crewIds.map { self[$0] }
            } else {
                return []
            }
        }
        
        public var name: String { stats.name }
            
        public var description: String {
            return self.name
        }
        
        public var rawBalance: Int {
            func sum(_ alignment: Alignment) -> Int {
                units.values.filter {
                    let character = $0.character
                    return !character.ship && character.alignment == alignment
                }.reduce(0) { $0 + $1.gp }
            }
            let ls = sum(.light)
            let ds = sum(.dark)
            
            let lw = Model.Character.counts[.light]!
            let dw = Model.Character.counts[.dark]!
            
            let weightedResult = (ls / lw - ds / dw) * (lw + dw)
            return weightedResult
        }
        
        public var balance: Int {
            rawBalance * 1000 / stats.characterGP
        }
        
        private static let balanceDescription = [
            (-700, "Sith Master"),
            (-400, "Sith Council"),
            (-300, "Darth"),
            (-200, "Sith Lord"),
            (-100, "Sith"),
            (-10, "Sith Sympathizer"),
            (5, "Neutral"),
            (30, "Youngling"),
            (60, "Padawan"),
            (100, "Jedi Knight"),
            (150, "Jedi Consular"),
            (225, "Jedi Guardian"),
            (300, "Jedi Sentinel"),
            (375, "Jedi Master"),
            (450, "Jedi Council"),
            (500, "Master of the Order"),
        ]
        
        public static func describe(balance: Int) -> String {
            for (threshold, value) in balanceDescription {
                if balance <= threshold {
                    return value
                }
            }
            return balanceDescription.last!.1
        }
        
        public var balanceDescription: String {
            let b = self.balance
            switch b {
            case .min ..< -5: return "DS"
            case -5 ... 5: return "N"
            case 6 ..< .max: return "LS"
            default: fatalError("mystery balance: \(b)")
            }
        }
        
        public func countSpeedModsGreaterEqual(than value: Int) -> Int {
            units.values.reduce(0, { $0 + ($1.mods.filter { $0.speed >= value }.count) })
        }
        
        public var modScore: Double {
            // from DSR Bot
            // Mod Quality: Number of +15 Speeds / (squad GP / 100000)
            //
            // specifically count of mods with speed secondaries >= 15 / character gp / 100_000
            
            Double(countSpeedModsGreaterEqual(than: 15)) / (Double(stats.characterGP) / 100_000.0)
        }
        
        public var relicTierCount: Int {
            units.values.filter { $0.gear == 13 }.reduce(0) { $0 + $1.relic - 2 }
        }
        
        public var gearScore: Double {
            // from DSR Bot
            // Gear Quality: (Number of G12+ + G13 Bonus Score) / (Total GP / 100000)
            // G13 Bonus score: 1 + (0.2 bonus per relic tier) (ex: r0 = 1, r1 = 1.2, ..., r7 = 2.4)
            
            let countG12 = Double(units.values.filter { $0.gear >= 12 }.count)
            let bonusG13 = Double(units.values.filter { $0.gear == 13 }.count) + Double(self.relicTierCount) * 0.2

            
            return (countG12 + bonusG13) / (Double(stats.gp) / 100_000.0)
        }
        
        public static func == (lhs: Player, rhs: Player) -> Bool {
            return lhs.stats.allyCode == rhs.stats.allyCode
        }
        
        public func hash(into hasher: inout Hasher) {
            hasher.combine(stats.name)
        }

        public static func < (lhs: Player, rhs: Player) -> Bool {
            return lhs.name.lowercased() < rhs.name.lowercased()
        }

        public var asLightPlayer : LightPlayer { LightPlayer(allyCode: stats.allyCode, name: stats.name) }
        
        /// useful for serializing a light version e.g. players for TW order preferences
        public struct LightPlayer : Codable, Hashable {
            let allyCode: Int
            let name: String
            
            enum CodingKeys: String, CodingKey {
                case stats = "data"
                case allyCode
                case name
            }
            init(allyCode: Int, name: String) {
                self.allyCode = allyCode
                self.name = name
            }

            public init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                
                if values.contains(.stats) {
                    // handle reading a Player as a LightPlayer
                    let stats = try values.decode(Stats.self, forKey: .stats)
                    self.allyCode = stats.allyCode
                    self.name = stats.name
                } else {
                    self.allyCode = try values.decode(Int.self, forKey: .allyCode)
                    self.name = try values.decode(String.self, forKey: .name)
                }
            }
            
            public func encode(to encoder: Encoder) throws {
                var container = encoder.container(keyedBy: CodingKeys.self)
                try container.encode(allyCode, forKey: .allyCode)
                try container.encode(name, forKey: .name)
            }
        }

    }
    
    /// Ability reference data.
    ///
    /// curl https://swgoh.gg/api/abilities/ > SWGOH/Reference\ Data/abilities.json
    public struct Ability : Codable {
        
        public let unitId: String
        public let maxTier: Int
        public let id: String
        public let name: String
        public let isOmega: Bool
        public let isZeta: Bool
        public let image: String
        public let url: String
        public let abilityType: AbilityType
        
        public enum AbilityType : Int, Codable {
            case basic = 1
            case special = 2
            case leader = 3
            case unique = 4
            case crew = 5
            case ship = 0
        }
        
        enum CodingKeys: String, CodingKey {
            case characterId = "character_base_id"
            case shipId = "ship_base_id"
            case maxTier = "tier_max"
            case id = "base_id"
            case name
            case isOmega = "is_omega"
            case isZeta = "is_zeta"
            case image
            case url
            case abilityType = "type"
        }
        
        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            self.unitId = try values.decode(String?.self, forKey: .characterId) ?? values.decode(String.self, forKey: .shipId)

            self.maxTier = try values.decode(Int.self, forKey: .maxTier)
            self.id = try values.decode(String.self, forKey: .id)
            self.name = try values.decode(String.self, forKey: .name)
            self.isZeta = try values.decode(Bool.self, forKey: .isZeta)
            self.isOmega = try values.decode(Bool.self, forKey: .isOmega)
            self.image = try values.decode(String.self, forKey: .image)
            self.url = try values.decode(String.self, forKey: .url)
            if values.contains(.abilityType) {
                self.abilityType = try values.decode(AbilityType?.self, forKey: .abilityType) ?? .ship
            } else {
                self.abilityType = .ship
            }
        }
        
        private init(unitId: String, maxTier: Int, id: String, name: String, isOmega: Bool, isZeta: Bool, image: String, url: String, abilityType: Model.Ability.AbilityType) {
            self.unitId = unitId
            self.maxTier = maxTier
            self.id = id
            self.name = name
            self.isOmega = isOmega
            self.isZeta = isZeta
            self.image = image
            self.url = url
            self.abilityType = abilityType
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(unitId, forKey: .characterId)
            try container.encode(maxTier, forKey: .maxTier)
            try container.encode(id, forKey: .id)
            try container.encode(name, forKey: .name)
            try container.encode(isZeta, forKey: .isZeta)
            try container.encode(isOmega, forKey: .isOmega)
            try container.encode(image, forKey: .image)
            try container.encode(url, forKey: .url)
            try container.encode(abilityType, forKey: .abilityType)
        }
        
        public var zetaDescription: String? {
            // see Generated/ZetaPriorities.swift
            Model.Ability.zetaDescriptions[self.name]
        }
        
        private static var rawAbilities: [Ability] = {
            let data = try! Data(contentsOf: Bundle(for: ModelClass.self).url(forResource: "abilities", withExtension: "json")!)
            return try! JSONDecoder().decode([Ability].self, from: data)
        }()
        
        public static var abilitiesByUnitId: [String:[Ability]] = {
            return Dictionary(grouping: rawAbilities, by: \.unitId)
        }()
        
        public static var abilities: [String:Ability] = {
            return Dictionary(rawAbilities.map { ($0.id, $0) }, uniquingKeysWith: { v1, v2 in v1 })
        }()
        
        public static func empty(unitId: String, id: String) -> Ability {
            Ability(unitId: unitId, maxTier: 8, id: id, name: "stand-in", isOmega: false, isZeta: false, image: "none", url: "none", abilityType: .basic)
        }
    }


    // MARK: - Characters File -- https://swgoh.gg/api/characters/

    public enum Alignment: String, Codable {
        case light = "Light Side"
        case dark = "Dark Side"
    }

}

