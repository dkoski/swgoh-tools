//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
import Combine

public enum DataFileError : Error {
    case credentialsNotSet
    case unknownType
}

private let dateFormatter: DateFormatter = {
    let df = DateFormatter()
    df.dateStyle = .short
    df.timeStyle = .short
    return df
}()

public protocol TopLevelModel : Codable {
    
    static var serializedPrefix: String { get }
    
}

/// marker to indicate that the TopLevelModel is local (not downloaded)
public protocol LocalModel {
    
    static var sharingFileExtension: String { get }
    
}

extension Model.Guild : TopLevelModel {

    public static let serializedPrefix = "guild-"
    
}

extension Model.Player : TopLevelModel {
    
    public static let serializedPrefix = "player-"

}

fileprivate var cacheDirectoryURL: URL = {
    let fm = FileManager()
    if let identifier = Bundle.main.bundleIdentifier,
        let url = fm.urls(for: .applicationSupportDirectory, in: .userDomainMask).first {
        let appCacheDirectory = url.appendingPathComponent(identifier)
        try? fm.createDirectory(at: appCacheDirectory, withIntermediateDirectories: true, attributes: nil)
        return appCacheDirectory
    } else {
        return URL(fileURLWithPath: "/tmp")
    }
}()

public class DataFileManager {
    
    public static let guilds = Storage<Model.Guild>(downloadProvider: GuildDownloadProvider())
    public static let players = Storage<Model.Player>(downloadProvider: PlayerDownloadProvider())
    public static let twConfigs = Storage<EditableTWConfiguration>(downloadProvider: DownloadProvider<EditableTWConfiguration>())
    public static let discord = Storage<DiscordModel>(downloadProvider: DownloadProvider<DiscordModel>())
    public static let squadRules = Storage<SquadRuleSet>(downloadProvider: DownloadProvider<SquadRuleSet>())

    public static var isOffline = false {
        didSet {
            if isOffline {
                cacheDirectoryURL = Bundle(for: DataFileManager.self).url(forResource: "Offline", withExtension: nil)!
            }
        }
    }

    public static var defaultGuild: DataFile<Model.Guild>? {
        guilds.find(allyCode: Defaults.shared.allyCode)
    }
    
    public static var defaultPlayer: DataFile<Model.Player>? {
        players.find(allyCode: Defaults.shared.allyCode)
    }
    
    public static var discordMentions: DataFile<DiscordModel>? {
        discord.newest
    }
    
    class DownloadProvider <T : TopLevelModel> : NSObject {
        func download(downloader: Downloader, allyCode: Int, directory: URL, ioQueue: DispatchQueue) -> AnyPublisher<DataFile<T>, Error> {
            fatalError("abstract")
        }
    }

    class GuildDownloadProvider : DownloadProvider<Model.Guild> {
        override func download(downloader: Downloader, allyCode: Int, directory: URL, ioQueue: DispatchQueue) -> AnyPublisher<DataFile<Model.Guild>, Error> {
            return downloader.fetchGuild(shortName: "guild", playerId: allyCode)
                .map { DataFile(allyCode: allyCode, guild: $0, directory: directory) }
                .receive(on: ioQueue)
                .tryMap { df -> DataFile<Model.Guild> in
                    try df.save()
                    return df
                }
                .eraseToAnyPublisher()
        }
    }

    class PlayerDownloadProvider : DownloadProvider<Model.Player> {
        override func download(downloader: Downloader, allyCode: Int, directory: URL, ioQueue: DispatchQueue) -> AnyPublisher<DataFile<Model.Player>, Error> {
            return downloader.fetchPlayer(allyCode: allyCode)
                .map { DataFile(player: $0, directory: directory) }
                .receive(on: ioQueue)
                .tryMap { df -> DataFile<Model.Player> in
                    try df.save()
                    return df
                }
                .eraseToAnyPublisher()
        }
    }
        
    public class Storage<T : TopLevelModel> : NSObject, ObservableObject {
        
        private let queue = DispatchQueue(label: "DataFileManager")
        private let ioQueue = DispatchQueue(label: "DataFileManager.io")
        
        private var allFiles = [DataFile<T>]()
        
        private let downloadProvider: DownloadProvider<T>

        fileprivate init(downloadProvider: DownloadProvider<T>) {
            self.downloadProvider = downloadProvider
            super.init()
            let directory = cacheDirectoryURL
            let urls = (try? FileManager.default.contentsOfDirectory(at: directory, includingPropertiesForKeys: nil)) ?? []
            for url in urls {
                if url.lastPathComponent.starts(with: T.serializedPrefix) {
                    if let dataFile = DataFile<T>(url: url) {
                        allFiles.append(dataFile)
                    }
                }
            }
            allFiles.sort()
        }
        
        public var newest: DataFile<T>? {
            queue.sync {
                allFiles.sorted { $0.date > $1.date }.first { $0.allyCode != Defaults.shared.allyCode }
            }
        }
            
        public var values: [DataFile<T>] {
            queue.sync {
                allFiles
            }
        }
                
        public func find(allyCode: Int) -> DataFile<T>? {
            queue.sync {
                allFiles.first(where: { $0.allyCode == allyCode })
            }
        }
        
        public func find(name: String) -> DataFile<T>? {
            queue.sync {
                allFiles.first(where: { $0.name == name })
            }
        }
                        
        private func didUpdate(dataFile: DataFile<T>) {
            // remove any stale cached data
            dataFile.invalidateCachedData()
            
            DispatchQueue.main.async {
                // we need to be on the main queue to publish the change
                self.objectWillChange.send()
                self.queue.sync {
                    if let index = self.allFiles.firstIndex(where: { $0 == dataFile }) {
                        self.allFiles[index] = dataFile
                    } else {
                        self.allFiles.append(dataFile)
                        self.allFiles.sort()
                    }
                }
            }
        }
    
        
        private let cachedDownloader = UpdatableBox<Downloader>()
        
        private func downloader() -> AnyPublisher<Downloader, Error> {
            var downloader: Downloader
            if let v = self.cachedDownloader.value {
                downloader = v
            } else {
                let credentials = Defaults.shared.credentials
                if !credentials.isComplete {
                    return Fail(error: DataFileError.credentialsNotSet).eraseToAnyPublisher()
                }
                    
                downloader = Downloader(user: credentials.username, password: credentials.password)
                cachedDownloader.value = downloader
            }
            
            return Just(downloader).setFailureType(to: Error.self).eraseToAnyPublisher()
        }
                        
        public func download(allyCode: Int) -> AnyPublisher<DataFile<T>, Error> {
            return downloader()
                .flatMap { downloader -> AnyPublisher<DataFile<T>, Error> in
                    self.downloadProvider.download(downloader: downloader, allyCode: allyCode, directory: cacheDirectoryURL, ioQueue: self.ioQueue)
                }
                .map { value -> DataFile<T> in
                    self.didUpdate(dataFile: value)
                    return value
                }
                .eraseToAnyPublisher()
        }

        public func update(_ dataFile: DataFile<T>) -> AnyPublisher<DataFile<T>, Error> {
            let allyCode = dataFile.allyCode
            
            return downloader()
                .flatMap { downloader -> AnyPublisher<DataFile<T>, Error> in
                    self.downloadProvider.download(downloader: downloader, allyCode: allyCode, directory: cacheDirectoryURL, ioQueue: self.ioQueue)
                }
                .map { value -> DataFile<T> in
                    self.didUpdate(dataFile: value)
                    return value
                }
                .eraseToAnyPublisher()
        }
        
        public func delete(_ dataFile: DataFile<T>) {
            for url in dataFile.relatedCacheURLS() {
                try? FileManager.default.removeItem(at: url)
            }
            try? FileManager.default.removeItem(at: dataFile.url)
            
            DispatchQueue.main.async {
                // we need to be on the main queue to publish the change
                self.objectWillChange.send()
                self.queue.sync {
                    self.allFiles.removeAll(where: { $0 == dataFile })
                }
            }
        }
    }

}

public extension DataFileManager.Storage where T: LocalModel {
    
    func save(dataFile: DataFile<T>) throws {
        try dataFile.save()
        
        // remove any stale cached data
        dataFile.invalidateCachedData()
        
        DispatchQueue.main.async {
            // we need to be on the main queue to publish the change
            self.objectWillChange.send()
            self.queue.sync {
                if let index = self.allFiles.firstIndex(where: { $0.name == dataFile.name }) {
                    self.allFiles[index] = dataFile
                } else {
                    self.allFiles.append(dataFile)
                    self.allFiles.sort()
                }
            }
        }
    }
    
}

public class DataFile<T: TopLevelModel> : ObservableObject, Comparable, Hashable, CustomStringConvertible, NSCopying, Identifiable {

    class Promise<T> {
        private let loader: () throws -> T
        private var value: T? = nil
        
        init(loader: @escaping () throws -> T) {
            self.loader = loader
        }
        
        init(value: T) {
            loader = { fatalError() }
            self.value = value
        }
        
        func load() throws -> T {
            if let value = value {
                return value
            }
            
            let value = try loader()
            self.value = value
            return value
        }
    }
    
    private let promise: Promise<T>
    
    public let allyCode: Int
    
    @Published
    public var name: String
    
    public let url: URL
    
    @Published
    public var date: Date
    
    internal init(promise: DataFile<T>.Promise<T>, allyCode: Int, name: String, url: URL, date: Date) {
        self.promise = promise
        self.allyCode = allyCode
        self.name = name
        self.url = url
        self.date = date
    }

    init?(url: URL) {
        let filename = url.deletingPathExtension().deletingPathExtension().lastPathComponent
        
        var parts = filename.split(separator: "-")
        if parts.count < 3 {
            return nil
        }
        
        // toss the type
        parts.removeFirst()
        
        self.promise = Promise(loader: {
            let data = try Data(contentsOf: url)
            let value = try JSONDecoder().decode(T.self, from: data)
            return value
        })

        guard let allyCode = Int(String(parts.removeFirst())) else {
            return nil
        }
        
        self.allyCode = allyCode
        self.name = parts.joined(separator: "-")
        
        self.url = url
        
        do {
            let attributes = try FileManager.default.attributesOfItem(atPath: url.path)
            self.date = attributes[FileAttributeKey.modificationDate] as! Date
        } catch {
            self.date = Date.distantPast
        }
    }
    
    public func copy(with zone: NSZone? = nil) -> Any {
        return self
    }
    
    public func value() throws -> T {
        try promise.load()
    }
        
    fileprivate func save() throws {
        let data: Data
        data = try JSONEncoder().encode(try promise.load())
        try data.write(to: self.url)
    }
    
    public var isOutOfDate: Bool {
        date < Date() - 3600
    }
    
    public var dateDescription: String {
        let now = Date()
        if date < now - (3600 * 24) {
            return dateFormatter.string(from: date)
        } else if date > now - 10 * 60 {
            return "now"
        } else {
            let difference = date.distance(to: Date()) / 3600.0
            let hours = Double(Int(difference * 10)) / 10.0
            return "\(hours) hours ago"
        }
    }
    
    public static func < (lhs: DataFile, rhs: DataFile) -> Bool {
        lhs.name < rhs.name
    }
    
    public static func == (lhs: DataFile, rhs: DataFile) -> Bool {
        lhs.allyCode == rhs.allyCode && lhs.name == rhs.name
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(allyCode)
    }
    
    public var id: Int { allyCode }
    
    public var description: String {
        "\(allyCode) - \(name) - \(dateFormatter.string(from: date))"
    }

}

extension DataFile where T == Model.Player {
    
    public convenience init(player: Model.Player, directory: URL) {
        let url = directory.appendingPathComponent(DataFile.makeFilename(player: player)).appendingPathExtension("json")
        self.init(promise: Promise(value: player), allyCode: player.stats.allyCode, name: player.name, url: url, date: Date())
    }

    static func makeFilename(player: Model.Player) -> String {
        T.serializedPrefix + "\(player.stats.allyCode)-\(player.name)"
    }

}

extension DataFile where T == Model.Guild {
    
    public convenience init(allyCode: Int? = nil, guild: Model.Guild, directory: URL) {
        let allyCode = allyCode ?? guild.playersSortedByGP().first?.stats.allyCode ?? 555_555_555
        let url = directory.appendingPathComponent(DataFile.makeFilename(allyCode: allyCode, guild: guild)).appendingPathExtension("json")
        self.init(promise: Promise(value: guild), allyCode: allyCode, name: guild.info.name, url: url, date: Date())
    }

    static func makeFilename(allyCode: Int, guild: Model.Guild) -> String {
        T.serializedPrefix + "\(allyCode)-\(guild.info.name.replacingOccurrences(of: "/", with: "-"))"
    }
    
}

extension DataFile where T == EditableTWConfiguration {
    
    public convenience init(config: EditableTWConfiguration) {
        let url = cacheDirectoryURL.appendingPathComponent(T.serializedPrefix + "0-" + config.name).appendingPathExtension("json")
        self.init(promise: Promise(value: config), allyCode: 0, name: config.name, url: url, date: Date())
    }
    
}

extension DataFile where T == DiscordModel {
    
    public convenience init(discord: DiscordModel) {
        let url = cacheDirectoryURL.appendingPathComponent(T.serializedPrefix + "0-" + "mentions").appendingPathExtension("json")
        self.init(promise: Promise(value: discord), allyCode: 0, name: "mentions", url: url, date: Date())
    }
    
}

extension DataFile where T == SquadRuleSet {
    
    public convenience init(squadRules: SquadRuleSet) {
        let url = cacheDirectoryURL.appendingPathComponent(T.serializedPrefix + "0-" + squadRules.name).appendingPathExtension("json")
        self.init(promise: Promise(value: squadRules), allyCode: 0, name: squadRules.name, url: url, date: Date())
    }
    
}

extension DataFile {
    
    private func cacheFilePrefix() -> String {
        String(describing: T.self) + "." + allyCode.description + "."
    }
    
    func relatedCacheURLS() -> [URL] {
        var result = [URL]()
        let prefix = cacheFilePrefix()
        let directory = self.url.deletingLastPathComponent()
        let urls = (try? FileManager.default.contentsOfDirectory(at: directory, includingPropertiesForKeys: nil)) ?? []
        for url in urls {
            if url.lastPathComponent.starts(with: prefix) {
                result.append(url)
            }
        }
        return result
    }
    
    func invalidateCachedData() {
        for url in relatedCacheURLS() {
            try? FileManager.default.removeItem(at: url)
        }
    }
    
    private func cacheURLForData(name: String) -> URL {
        self.url.deletingLastPathComponent().appendingPathComponent(cacheFilePrefix() + name + ".json")
    }
    
    public func squadEvaluations(name: String = TW.defaultSquadRuleName) -> AnyPublisher<[Model.Player:[SquadEvaluation]], Error> {
        let players: [Model.Player]
        
        do {
            let data = try self.promise.load()
            if let player = data as? Model.Player {
                players = [ player ]
            } else if let guild = data as? Model.Guild {
                players = guild.players
            } else {
                return Fail(outputType: [Model.Player:[SquadEvaluation]].self, failure: DataFileError.unknownType).eraseToAnyPublisher()
            }
        } catch {
            return Fail(outputType: [Model.Player:[SquadEvaluation]].self, failure: error).eraseToAnyPublisher()
        }
        
        let url = cacheURLForData(name: "tw-squads")
        
        func load(data: Data) -> [Model.Player:[SquadEvaluation]]? {
            do {
                let lightPlayerResults = try JSONDecoder().decode([Model.Player.LightPlayer:[SquadEvaluation]].self, from: data)
                let playerMap = Dictionary(uniqueKeysWithValues: players.map { ( $0.stats.allyCode, $0 )})
                return Dictionary(uniqueKeysWithValues: lightPlayerResults.map { (playerMap[$0.key.allyCode]!, $0.value )})
            } catch {
                print("Unable to decode: \(error)")
                return nil
            }
        }
        
        return Future<[Model.Player:[SquadEvaluation]], Error> { promise in
            DispatchQueue.global().async {
                // try loading it
                if name == TW.defaultSquadRuleName, let data = try? Data(contentsOf: url),
                    let result = load(data: data) {
                    promise(.success(result))
                    
                } else {
                    // if not, prepare it
                    let evaluationFilter = PreferredViabilitySortFilter().compose(ViableSquadFilter()).compose(SquadListFilter())
                    let squadEvaluator = SquadEvaluator(rules: [:])
                    let squadBuilder = MultipleSquadBuilder(TW.configurations[name]!)
                    
                    let result = DispatchQueue.concurrentPerform(iterations: players.count) { index -> (Model.Player, [SquadEvaluation]) in
                        let player = players[index]
                        
                        let squads = squadBuilder.generateSquads(units: player)
                        
                        let evaluations = squadEvaluator.evaluate(squads: squads)
                        let finalEvaluations = evaluationFilter.filter(evaluations)
                        
                        return (player, finalEvaluations)
                    }
                    
                    // cache it -- write it out as LightPlayer (we don't need the full player info as a key)
                    if name == TW.defaultSquadRuleName {
                        let transformedResult = Dictionary(uniqueKeysWithValues: result.map { ($0.key.asLightPlayer, $0.value) })
                        if let data = try? JSONEncoder().encode(transformedResult) {
                            try? data.write(to: url)
                        }
                    }
                    
                    promise(.success(result))
                }
            }
        }
        .eraseToAnyPublisher()
    }
    
}
