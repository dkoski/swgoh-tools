//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation

public struct SquadRuleSet : Codable, TopLevelModel, LocalModel {
    
    public static let sharingFileExtension = "squadconfig"
    public static let serializedPrefix = "squadRules-"
    
    public var name: String
    public var notes = ""
    
    public var squadRules = [SquadRule]()
    
    public init(name: String) {
        self.name = name
    }
    
    public mutating func add(rule: SquadRule) {
        squadRules.append(rule)
        squadRules.sort()
    }
}

public struct SquadRule : Codable, Hashable, Comparable {
    
    var uuid: String
    
    // TODO need a version number or timestamp.  might need to do that in serialization to make sure we only bump it if it changes.  need this for merging (future)
    
    public enum Kind : String, Codable {
        case fleet
        case squad
    }

    public let kind: Kind
    
    public var disabled: Bool
    
    /// the canonical name of the team, e.g. NS.  used for grouping
    public var canonicalName: String
    
    /// the variant name of the team, e.g. NestSisters
    public var name: String
    
    public var displayName: String { name.isEmpty ? canonicalName : name }
    
    public var notes: String
    
    public var tier: Int
    public var defense: Bool
    
    public var attackerId: String
    public var defenderId: String
    
    /// set of character id's per slot
    public var possibleCharacterIds: [Set<String>]
    
    public var possibleCharacters: [Set<Model.Character>] {
        get {
            possibleCharacterIds.map { set in
                Set(set.map { cid in Model.Character.find(cid) })
            }
        }
        set {
            possibleCharacterIds = newValue.map { set in
                Set(set.map { c in c.id })
            }
        }
    }

    public var preference: Int
    
    public var rules: [String:UnitRule]
    
    public struct UnitRule : Codable, Hashable {
        public let id: String
        
        public var requiredZetas = Set<String>()
        public var minimumGearLevel = 10
        public var minimumStars = 7
        public var preference = 0
        
        public var unitMinimumGP = 6000
    }
    
    public init(kind: Kind = .squad, squadName: String) {
        self.uuid = UUID().uuidString
        self.kind = kind
        self.disabled = false
        self.canonicalName = squadName
        self.name = squadName
        self.notes = ""
        self.tier = 3
        self.defense = true
        self.attackerId = ""
        self.defenderId = ""
        let count: Int
        switch kind {
        case .fleet: count = 9
        case .squad: count = 5
        }
        self.possibleCharacterIds = Array<Set<String>>(repeating: [], count: count)
        self.preference = 0
        self.rules = [:]
    }
    
    public var copy: SquadRule {
        var copy = self
        copy.uuid = UUID().uuidString
        return copy
    }
    
    public var allSquadCharacters: Set<Model.Character> {
        var result = Set<Model.Character>()
        for list in possibleCharacters {
            result.formUnion(list)
        }
        return result
    }
    
    public var sortedCharacters: [Model.Character] {
        allSquadCharacters
            .sorted { (c1, c2) -> Bool in
                c1.commonName < c2.commonName
            }
    }
    
    /// return the representative characters for each slot in the squad
    public var representativeCharacters: [Model.Character] {
        var used = Set<Model.Character>()
        
        // so we can show the characters in preference order
        // (hopefully the most common and recognizable view)
        func prioritySort(_ c1: Model.Character, _ c2: Model.Character) -> Bool {
            if let r1 = rules[c1.id], let r2 = rules[c2.id] {
                if r1.preference == r2.preference {
                    return c1 < c2
                } else {
                    // reverse order of preference (highest first)
                    return r1.preference > r2.preference
                }
            }
            return c1 < c2
        }
        
        return possibleCharacters.compactMap {
            // try and show different characters for each spot
            let remainingCharacters = $0.subtracting(used)
            if remainingCharacters.isEmpty {
                return $0.sorted(by: prioritySort(_:_:)).first
            } else {
                let character = remainingCharacters.sorted(by: prioritySort(_:_:)).first!
                used.insert(character)
                return character
            }
        }
    }
    
    public func matches<T: StringProtocol>(_ words: [T]) -> Bool {
        var matchedAllWords = true
        
        // we want to find squads that match all the words or have characters that match all the words
        for word in words {
            if canonicalName.matchesWord(word) || name.matchesWord(word) || notes.matchesWord(word) {
                // matched this word, continue
                
            } else {
                matchedAllWords = false
                break
            }
        }
                
        if !matchedAllWords {
            // try and find a character then
            return allSquadCharacters.contains { $0.matches(words) }
        }
        
        return true
    }
        
    public subscript(unitId: String) -> UnitRule {
        get { rules[unitId] ?? UnitRule(id: unitId, requiredZetas: [], minimumGearLevel: 10, minimumStars: 7, preference: 0) }
        set { rules[unitId] = newValue }
    }
    
    public static func < (lhs: SquadRule, rhs: SquadRule) -> Bool {
        lhs.canonicalName == rhs.canonicalName ? lhs.uuid < rhs.uuid : lhs.canonicalName < rhs.canonicalName
    }
    
}

struct SquadRuleUnitSelector : UnitSelect {
    
    let unitIds: Set<String>
    let rules: [String:SquadRule.UnitRule]

    // Note: for conversion from old style to new style.  not used
    var ids: [String] { Array(unitIds) }
    
    func select(units: UnitProvider) -> [Model.Unit] {
        var result = [Model.Unit]()
        for id in ids {
            let unit = units[id]
            if unit.stars > 0 {
                let rule = rules[id]!
                if !rule.evaluate(unit: unit).status.isViable {
                    continue
                }
                result.append(unit)
            }
        }
        return result
    }

}

extension SquadRule {
    
    /// if allVariants is true then do not filter the units at select time
    public func squadBuilder(allVariants: Bool = false) -> SquadBuilder {
        let rules = Dictionary(uniqueKeysWithValues: self.rules.map { ($0.key, UnitEvaluator($0.value)) })
        
        // for ease of editing we store nil id's as "" -- convert them back to nil
        func nullify(_ value: String) -> String? {
            value.isEmpty ? nil : value
        }
        
        let properties = Squad.Properties(name: self.name, canonicalName: self.canonicalName, attackerId: nullify(self.attackerId), defenderId: nullify(self.defenderId), preference: self.preference, rules: rules)
        
        if allVariants {
            let unitSelectors = possibleCharacterIds.map { OneOfUnitSelect(Array($0)) }
            return SelectUnitSquadBuilder(properties: properties, unitSelectors: unitSelectors)
        } else {
            let unitSelectors = possibleCharacterIds.map { SquadRuleUnitSelector(unitIds: $0, rules: self.rules) }
            return SelectUnitSquadBuilder(properties: properties, unitSelectors: unitSelectors)
        }
    }
    
}

extension SquadRule.UnitRule : UnitEvaluationRule {
    
    public func update(id: String, unitRule: inout SquadRule.UnitRule) {
        // this is for converting old style rules to SquadRule format -- NOP
    }
    
    func lerp(_ value: Int, _ min: Int, _ max: Int, _ scale: Int) -> Int {
        if min == max {
            return scale
            
        } else {
            let numerator = Double(value - min)
            let denominator = Double(max - min)
            
            return Int((numerator / denominator) * Double(scale))
        }
    }
    
    public func evaluate(unit: Model.Unit) -> Message {
        var max = 0
        var score = 0
        var messages = [String]()
        var status = Status.ok
        
        if unit.gp < unitMinimumGP {
            return Message(message: "below gp", status: .needsRequired, score: 0, max: 1000, preference: 0)
        }
        
        let requiredZetaCount = requiredZetas.count
        if requiredZetaCount > 0 {
            max += 200 * requiredZetaCount
            let zetaCount = unit.zetaIds.reduce(0) { $0 + (requiredZetas.contains($1) ? 1 : 0) }
            score += 200 * zetaCount
            if zetaCount < requiredZetaCount {
                messages.append("missing zetas")
                status = min(status, .needsRequired)
            }
        }
                
        if unit.level < 85 {
            messages.append("low level")
            status = min(status, .needsRequired)
        }
        
        if unit.stars < minimumStars {
            messages.append("low stars")
            status = min(status, .needsFarming)
        }
        
        if minimumGearLevel > 20 {
            // relics
            if unit.gear < 13 {
                messages.append("not relic")
                status = min(status, .needsRequired)
                
            } else if unit.relic - 2 < minimumGearLevel - 20 {
                messages.append("low relic")
                status = min(status, .needsGear)
            }
        
            max += 700
            score += lerp(unit.relic - 2, minimumGearLevel - 20, 7, 700)

        } else {
            if unit.gear < minimumGearLevel - 1 {
                messages.append("below minimum gear")
                status = min(status, .needsRequired)
                
            } else if unit.gear < minimumGearLevel {
                messages.append("low gear")
                status = min(status, .needsGear)

            }
            max += 700
            score += lerp(unit.gear + unit.relic - 2, minimumGearLevel, 13, 700)
        }
        
        max += 260
        score += lerp(unit.stars, minimumStars, 7, 260)
                
        max += 40
        score += (unit.gp + 500) / 1000

        if messages.isEmpty {
            return Message(message: "ok", status: status, score: score, max: max, preference: preference)
        } else {
            return Message(message: messages.joined(separator: ", "), status: status, score: score, max: max, preference: preference)
        }
    }

}
