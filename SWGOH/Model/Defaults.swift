//
//  UserDefaults.swift
//  BRGTool
//
//  Created by David Koski on 3/17/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
import CryptoKit

// from https://www.avanderlee.com/swift/property-wrappers/
@propertyWrapper
public struct UserDefault<T> {
    let key: String
    let defaultValue: T

    init(_ key: String, defaultValue: T) {
        self.key = key
        self.defaultValue = defaultValue
    }

    public var wrappedValue: T {
        get {
            return Foundation.UserDefaults.standard.object(forKey: key) as? T ?? defaultValue
        }
        set {
            Foundation.UserDefaults.standard.set(newValue, forKey: key)
        }
    }
}

// TODO perhaps break this into extensions
public class Defaults : NSObject, ObservableObject {
    
    public static let shared = Defaults()
    
    public class Credentials {
        @UserDefault("swgoh.username", defaultValue: "")
        public var username: String
        
        @UserDefault("swgoh.password", defaultValue: Data())
        var passwordData: Data
        
        private var k: Data = {
            var r = MersenneTwister(seed: 7865)
            var a = [UInt64]()
            a.append(r.next())
            a.append(r.next())
            a.append(r.next())
            a.append(r.next())
            return a.withUnsafeBufferPointer { Data(buffer: $0) }
        }()
        
        public var password: String {
            get {
                let key = SymmetricKey(data: k)
                if let box = try? AES.GCM.SealedBox(combined: passwordData),
                    let v = try? AES.GCM.open(box, using: key) {
                    return String(data: v, encoding: .utf8) ?? ""
                }
                return ""
            }
            set {
                let key = SymmetricKey(data: k)
                if let box = try? AES.GCM.seal(newValue.data(using: .utf8)!, using: key),
                    let data = box.combined {
                    passwordData = data
                }
            }
        }
        
        private func nilOrEmpty(_ value: String?) -> Bool {
            value == nil || value!.isEmpty
        }
        
        public var isComplete: Bool {
            !nilOrEmpty(username) && !nilOrEmpty(password)
        }
                
        public func store() {
            // NOP for the UD backed version
        }
    }
    
    private override init() {
        credentials = Credentials()
    }
    
    public var credentials: Credentials

    @UserDefault("allyCode", defaultValue: 0)
    public var allyCode: Int
    
    @UserDefault("tw.notPlaying", defaultValue: [])
    public var twNotPlaying: [Int]
    
    public var isMissingData: Bool {
        !credentials.isComplete ||
        allyCode == 0
    }
    
}

// NOTE: I would prefer to use this, but the username/password for this is not all that
// sensitive and without code signing, I can't rally use keychain
struct CredentialsKeychain {
    static let server = "api.swgoh.help"
    
    var username: String? {
        willSet {
            if username != newValue {
                isDirty = true
            }
        }
    }
    
    var password: String? {
        willSet {
            if password != newValue {
                isDirty = true
            }
        }
    }
    
    private var isFromKeychain = false
    private var isDirty = false
    
    private func nilOrEmpty(_ value: String?) -> Bool {
        value == nil || value!.isEmpty
    }
    
    var isComplete: Bool {
        !nilOrEmpty(username) && !nilOrEmpty(password)
    }
    
    static func load() -> CredentialsKeychain {
        var item: CFTypeRef?
        let query = [
                kSecClass as String: kSecClassInternetPassword,
                kSecAttrServer as String: server,
                kSecMatchLimit as String: kSecMatchLimitOne,
                kSecReturnAttributes as String: true,
                kSecReturnData as String: true
            ] as CFDictionary
        let status = SecItemCopyMatching(query, &item)
        if status != errSecSuccess {
            return CredentialsKeychain(username: "", password: "")
        }
        
        guard let existingItem = item as? [String : Any],
            let passwordData = existingItem[kSecValueData as String] as? Data,
            let password = String(data: passwordData, encoding: String.Encoding.utf8),
            let account = existingItem[kSecAttrAccount as String] as? String
        else {
            return CredentialsKeychain(username: "", password: "")
        }
        return CredentialsKeychain(username: account, password: password, isFromKeychain: true)
    }
    
    mutating func store() {
        if isDirty {
            if let account = username, let password = self.password?.data(using: String.Encoding.utf8) {
                let attributes: [String: Any] = [kSecClass as String: kSecClassInternetPassword,
                                        kSecAttrAccount as String: account,
                                        kSecAttrServer as String: CredentialsKeychain.server,
                                        kSecValueData as String: password]
            
                if isFromKeychain {
                    let query = [
                            kSecClass as String: kSecClassInternetPassword,
                            kSecAttrServer as String: CredentialsKeychain.server,
                            kSecMatchLimit as String: kSecMatchLimitOne,
                        ] as CFDictionary

                    let v = SecItemUpdate(query, attributes as CFDictionary)
                    print(v)
                } else {
                    _ = SecItemAdd(attributes as CFDictionary, nil)
                }
                isDirty = false
            }
        }
    }
}
