//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation

public struct DiscordModel : TopLevelModel, LocalModel, Codable {
    
    public static let sharingFileExtension = "swgoh-discord"
    public static let serializedPrefix = "discord-"
    

    public var mentions = [Int:String]()
    
    public init() {        
    }
    
    public subscript(verifiedAllyCode: Int) -> String {
        get { mentions[verifiedAllyCode] ?? "" }
        set { mentions[verifiedAllyCode] = newValue }
    }
    
    public mutating func importMentions(_ string: String) {
        let lines = string.split(separator: "\n")
        let sep: Character = string.contains("\t") ? "\t" : ","
        
        let middle = lines.count / 2
        if lines[middle].split(separator: sep).count >= 2 {
            // it looks like a csv or tsv data
            for line in string.split(separator: "\n") {
                var allyCode: Int? = nil
                var mention: String? = nil
                for field in line.split(separator: sep).map ({ String($0) }) {
                    if field.isValidAllyCode {
                        allyCode = Int(allyCode: field)
                    } else if field.contains("@") {
                        mention = field
                    }
                }
                if let allyCode = allyCode, let mention = mention {
                    mentions[allyCode] = mention
                }
            }
            
        } else {
            // it is probably \n separated
            var allyCode: Int? = nil
            var mention: String? = nil
            
            for line in lines.map({ String($0) }) {
                if line.isValidAllyCode {
                    allyCode = Int(allyCode: line)
                } else if line.contains("@") {
                    mention = line
                }
                
                if let a = allyCode, let m = mention {
                    mentions[a] = m
                    mention = nil
                    allyCode = nil
                }
            }
        }
    }
}

