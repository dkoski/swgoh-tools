//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
    
private let defaultTWSquads: Set<String> = [ "BH", "FO", "GK", "BS", "zEP", "Qira", "Grievous", "Geonosians", "Carth", "CLS", "NS", "NestSisters", "NS_Alt", "Revan", "Revan Offense", "DR", "DRMalak", "Clones", "ShaakClones", "501st", "GAS" ]

public struct EditableTWConfiguration : Codable, TopLevelModel, LocalModel {
    
    public static let sharingFileExtension = "twconfig"
    public static let serializedPrefix = "tworders-"
    
    public var name: String = "New Config"

    public var anyPlacedFleet: String = "Fleet: **BEST** defensive fleet"

    /// defensive player setup
    public var defensivePlayerTeamCount = 10
    public var defensivePlayers = Set<Model.Player>()
    public var defensivePlayerTeams = defaultTWSquads

    /// offsensive players and their counts
    public var offensivePlayerTeamCount = 3
    public var offensivePlayers = Set<Model.Player>()
    
    /// overrides for number of named squads per-player
    public var squadCountOverrides = [Model.Player:Int]()
    
    /// held back squads
    public var holdbacks = [Model.Player:Set<String>]()
        
    public struct SquadCounts : Codable {
        public var threshold: Int
        public var counts: TWSquadCount
        
        init(_ threshold: Int, _ counts: TWSquadCount) {
            self.threshold = threshold
            self.counts = counts
        }
    }
    
    public var squadCounts: [SquadCounts] = [
        SquadCounts( 1_500_000, TWSquadCount(named: 1, fluff: 1) ),
        SquadCounts( 2_200_000, TWSquadCount(named: 3, fluff: 1) ),
        SquadCounts( 3_000_000, TWSquadCount(named: 4, fluff: 0) ),
        SquadCounts( 4_000_000, TWSquadCount(named: 5, fluff: 0) ),
        SquadCounts( 5_000_000, TWSquadCount(named: 5, fluff: 0) ),
        SquadCounts( 10_000_000, TWSquadCount(named: 6, fluff: 0) ),
    ]
    
    public struct Instruction : Hashable, Codable {
        
        public let squadName: String
        public let isFleet: Bool

        public var isEnabled = false

        public var minimumGP: Int
        public var maximumGP: Int

        public var minimumGPF: Double {
            get { Double(minimumGP) }
            set { minimumGP = Int(newValue) }
        }
        
        public var count = 50
        
        /// allow backfill once placed squads are done
        public var allowRandomFill = false
        
        /// place in squad order (vs gp order)
        public var placeInOrder = false

        public var weightT1 = 0
        public var weightT2 = 0
        public var weightT3 = 0
        public var weightT4 = 0

        public var weightB1 = 0
        public var weightB2 = 0
        public var weightB3 = 0
        public var weightB4 = 0

        public var weightF1 = 0
        public var weightF2 = 0

        internal init(squadName: String, isFleet: Bool, isEnabled: Bool = false) {
            self.squadName = squadName
            self.isFleet = isFleet
            self.isEnabled = isEnabled
        
            if isFleet {
                self.minimumGP = 250_000
                self.maximumGP = 650_000
            } else {
                self.minimumGP = 60_000
                self.maximumGP = 250_000
            }
        }
        
        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            self.squadName = try values.decode(String.self, forKey: .squadName)
            self.isFleet = try values.decode(Bool.self, forKey: .isFleet)
            self.isEnabled = try values.decode(Bool.self, forKey: .isEnabled)
            self.minimumGP = try values.decode(Int.self, forKey: .minimumGP)
            self.maximumGP = try values.decode(Int.self, forKey: .maximumGP)
            self.count = try values.decode(Int.self, forKey: .count)
            self.allowRandomFill = try values.decode(Bool.self, forKey: .allowRandomFill)
            self.placeInOrder = (try? values.decode(Bool.self, forKey: .placeInOrder)) ?? false

            self.weightT1 = try values.decode(Int.self, forKey: .weightT1)
            self.weightT2 = try values.decode(Int.self, forKey: .weightT2)
            self.weightT3 = try values.decode(Int.self, forKey: .weightT3)
            self.weightT4 = try values.decode(Int.self, forKey: .weightT4)

            self.weightB1 = try values.decode(Int.self, forKey: .weightB1)
            self.weightB2 = try values.decode(Int.self, forKey: .weightB2)
            self.weightB3 = try values.decode(Int.self, forKey: .weightB3)
            self.weightB4 = try values.decode(Int.self, forKey: .weightB4)

            self.weightF1 = try values.decode(Int.self, forKey: .weightF1)
            self.weightF2 = try values.decode(Int.self, forKey: .weightF2)
        }

        public var sections: [String] {
            var result = [String]()
            if weightT1 > 0 { result.append("T1") }
            if weightT2 > 0 { result.append("T2") }
            if weightT3 > 0 { result.append("T3") }
            if weightT4 > 0 { result.append("T4") }
            if weightB1 > 0 { result.append("B1") }
            if weightB2 > 0 { result.append("B2") }
            if weightB3 > 0 { result.append("B3") }
            if weightB4 > 0 { result.append("B4") }
            if weightF1 > 0 { result.append("F1") }
            if weightF2 > 0 { result.append("F2") }
            return result
        }
        
        public var isT1 : Bool { get { weightT1 > 0 } set { weightT1 = newValue ? 1 : 0 }}
        public var isT2 : Bool { get { weightT2 > 0 } set { weightT2 = newValue ? 1 : 0 }}
        public var isT3 : Bool { get { weightT3 > 0 } set { weightT3 = newValue ? 1 : 0 }}
        public var isT4 : Bool { get { weightT4 > 0 } set { weightT4 = newValue ? 1 : 0 }}
        public var isB1 : Bool { get { weightB1 > 0 } set { weightB1 = newValue ? 1 : 0 }}
        public var isB2 : Bool { get { weightB2 > 0 } set { weightB2 = newValue ? 1 : 0 }}
        public var isB3 : Bool { get { weightB3 > 0 } set { weightB3 = newValue ? 1 : 0 }}
        public var isB4 : Bool { get { weightB4 > 0 } set { weightB4 = newValue ? 1 : 0 }}
        public var isF1 : Bool { get { weightF1 > 0 } set { weightF1 = newValue ? 1 : 0 }}
        public var isF2 : Bool { get { weightF2 > 0 } set { weightF2 = newValue ? 1 : 0 }}

        public var availableSections: [String] {
            isFleet ? TW.allFleetSections : TW.allGroundSections
        }
        
        /// reasonable values to put in a slider controlling the gp range
        public var minumimGPRange: ClosedRange<Int> {
            isFleet ? 250_000 ... 500_000 : 60_000 ... 140_000
        }
        
        public var maximumGPRange: ClosedRange<Int> {
            isFleet ? 250_000 ... 650_000 : 60_000 ... 250_000
        }

        public func hash(into hasher: inout Hasher) {
            hasher.combine(squadName)
        }
    }
    
    public var hasInventory = false
    public var instructions = [Instruction]()
    
    enum CodingKeys : String, CodingKey {
        case name
        
        case anyPlaceFleet

        case defensivePlayerTeamCount
        case defensivePlayers
        case defensivePlayerTeams
        
        case offensivePlayerTeamCount
        case offensivePlayers
        
        case squadCounts
        
        case squadCountOverrides
        case holdbacks
        
        case instructions
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(name, forKey: .name)
        
        try container.encode(anyPlacedFleet, forKey: .anyPlaceFleet)

        try container.encode(defensivePlayerTeamCount, forKey: .defensivePlayerTeamCount)
        try container.encode(defensivePlayers.map { $0.asLightPlayer }, forKey: .defensivePlayers)
        try container.encode(defensivePlayerTeams, forKey: .defensivePlayerTeams)
        
        try container.encode(offensivePlayerTeamCount, forKey: .offensivePlayerTeamCount)
        try container.encode(offensivePlayers.map { $0.asLightPlayer }, forKey: .offensivePlayers)
        
        try container.encode(squadCounts, forKey: .squadCounts)
        
        try container.encode(squadCountOverrides, forKey: .squadCountOverrides)
        try container.encode(holdbacks, forKey: .holdbacks)

        try container.encode(instructions, forKey: .instructions)
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        name = try values.decode(String.self, forKey: .name)
        
        if values.contains(.anyPlaceFleet) {
            anyPlacedFleet = try values.decode(String.self, forKey: .anyPlaceFleet)
        } else {
            anyPlacedFleet =  "Fleet: **BEST** defensive fleet"
        }

        defensivePlayerTeamCount = try values.decode(Int.self, forKey: .defensivePlayerTeamCount)
        defensivePlayers = Set(try values.decode([Model.Player.LightPlayer].self, forKey: .defensivePlayers).map { Model.Player(from: $0) })
        defensivePlayerTeams = Set(try values.decode([String].self, forKey: .defensivePlayerTeams))

        offensivePlayerTeamCount = try values.decode(Int.self, forKey: .offensivePlayerTeamCount)
        offensivePlayers = Set(try values.decode([Model.Player.LightPlayer].self, forKey: .offensivePlayers).map { Model.Player(from: $0) })
        
        if values.contains(.squadCounts) {
            squadCounts = try values.decode([SquadCounts].self, forKey: .squadCounts)
        }
        
        if values.contains(.squadCountOverrides) {
            squadCountOverrides = try values.decode([Model.Player:Int].self, forKey: .squadCountOverrides)
        }
        if values.contains(.holdbacks) {
            holdbacks = try values.decode([Model.Player:Set<String>].self, forKey: .holdbacks)
        }


        instructions = try values.decode([Instruction].self, forKey: .instructions)
    }
    
    public init(name: String? = nil) {
        if let name = name {
            self.name = name
        }
    }
    
    mutating public func evaluationsDidChange(inventory: [String:[Squad]]) {
        // make sure we have the right instructions
        let squads = Set(inventory.keys)
        var missing = squads
        for i in instructions {
            missing.remove(i.squadName)
        }
                    
        let sortedByCount = inventory.map { ($0, $1.count) }.sorted { $0.1 > $1.1 }.map { $0.0 }
        
        var enabledInstructions = [Instruction]()
        var disabledInstructions = [Instruction]()
        for squadName in sortedByCount where missing.contains(squadName) {
            let isFleet = inventory[squadName]?.first?.isFleet ?? false
            var instruction = Instruction(squadName: squadName, isFleet: isFleet, isEnabled: false)
            if defaultTWSquads.contains(squadName) {
                instruction.isEnabled = true
                enabledInstructions.append(instruction)
            } else {
                disabledInstructions.append(instruction)
            }
        }
        instructions.append(contentsOf: enabledInstructions)
        instructions.append(contentsOf: disabledInstructions)
        
        hasInventory = true
    }
                    
    public func compile() -> TWConfig {
        let instructionTeams = Set(instructions.compactMap { $0.isEnabled ? $0.squadName : nil })
        
        var randomPlacementDefenseTeams = instructionTeams
        randomPlacementDefenseTeams.formUnion(defensivePlayerTeams)

        // these are the teams to remove after we run the defensive players
        var teamsToRemove = defensivePlayerTeams
        teamsToRemove.subtract(instructions.filter { $0.isEnabled }.map { $0.squadName })
                
        // preferred sections
        var preferredSections = [String : [String]]()
        for instruction in instructions {
            guard instruction.isEnabled || defensivePlayerTeams.contains(instruction.squadName) else {
                continue
            }
            var sections = [String]()
            func append(_ name: String, _ weight: Int) {
                for _ in 0 ..< weight {
                    sections.append(name)
                }
            }
            append("T1", instruction.weightT1)
            append("T2", instruction.weightT2)
            append("T3", instruction.weightT3)
            append("T4", instruction.weightT4)

            append("B1", instruction.weightB1)
            append("B2", instruction.weightB2)
            append("B3", instruction.weightB3)
            append("B4", instruction.weightB4)

            append("F1", instruction.weightF1)
            append("F2", instruction.weightF2)
            
            if !sections.isEmpty {
                preferredSections[instruction.squadName] = sections
            }
        }

        // placement instructions
        var randomPlacementInstructions = [PlacementInstruction]()
        
        // holdbacks
        for (player, squads) in holdbacks {
            randomPlacementInstructions.append(.removeSquads(squads: squads, player: player))
        }
        
        // defensive players
        let defensivePlayerIds = Set(defensivePlayers.map { $0.stats.allyCode })
        for player in defensivePlayers {
            randomPlacementInstructions.append(.placePlayerSquads(player: player.stats.allyCode.description, count: defensivePlayerTeamCount))
        }
        for team in teamsToRemove {
            randomPlacementInstructions.append(.removeAll(squad: team, status: .skippedDefenseOnly))
        }
        
        // first trim out any squads based on thresholds
        for instruction in instructions where instruction.isEnabled {
            let squad = instruction.squadName
            if instruction.minimumGP > instruction.minumimGPRange.lowerBound {
                randomPlacementInstructions.append(.removeBelow(squad: squad, gp: instruction.minimumGP))
            }
            if instruction.maximumGP < instruction.maximumGPRange.upperBound {
                // remove any squads above the max but allow defensive players to place them
                randomPlacementInstructions.append(.removeAbove(squad: squad, gp: instruction.maximumGP, skip: defensivePlayerIds))
            }
        }
        
        // then place the squads in the preferred sections

        // this places them in the order listed
        for instruction in instructions where instruction.isEnabled && instruction.placeInOrder {
            let squad = instruction.squadName
            if instruction.count > 0 && preferredSections[squad] != nil {
                randomPlacementInstructions.append(.placePreferred(squad: squad, count: instruction.count))
            }
        }

        // this places the remainder in gp order
        var placedSquadCounts = [String:Int]()
        for instruction in instructions where instruction.isEnabled && !instruction.placeInOrder {
            let squad = instruction.squadName
            if instruction.count > 0 && preferredSections[squad] != nil {
                placedSquadCounts[squad] = instruction.count
            }
        }
        if !placedSquadCounts.isEmpty {
            randomPlacementInstructions.append(.placeAllPreferred(squadCounts: placedSquadCounts))
        }
        
        // finally, for any that do not allow backfill, remove the rest
        for instruction in instructions where instruction.isEnabled {
            let squad = instruction.squadName
            if !instruction.allowRandomFill {
                randomPlacementInstructions.append(.removeAll(squad: squad, status: .skippedNoBackfill))
            }
        }
        
        var overrideMaximumSquads = [String : TWSquadCount]()
        for player in defensivePlayers {
            overrideMaximumSquads[player.stats.allyCode.description] = TWSquadCount(named: defensivePlayerTeamCount, fluff: 0, fleet: 2)
        }
        for player in offensivePlayers {
            overrideMaximumSquads[player.stats.allyCode.description] = TWSquadCount(named: offensivePlayerTeamCount, fluff: 0)
        }
        
        for (player, count) in squadCountOverrides {
            overrideMaximumSquads[player.stats.allyCode.description] = TWSquadCount(named: count, fluff: 0)
        }
                
        return CompiledConfig(fleetInstructions: anyPlacedFleet,
                              squadCountThresholds: squadCounts.map { ($0.threshold, $0.counts) },
                              defenseTeams: randomPlacementDefenseTeams,
                              preferredSections: preferredSections,
                              placementInstructions: randomPlacementInstructions,
                              playerSquadCountOverrides: overrideMaximumSquads)
    }
}
    
/// the config we build from the rules above
private struct CompiledConfig : TWConfig {
    
    public let minimumGP = 60_000
    public var fullSectionCount: Int = 25
 
    public let fleetInstructions: String

    public let placementSections: [String] = TW.allSections
    
    public let squadCountThresholds: [(Int, TWSquadCount)]

    public let defenseTeams: Set<String>
    public let preferredSections: [String : [String]]
    public let placementInstructions: [PlacementInstruction]
    public let playerSquadCountOverrides: [String : TWSquadCount]
    
}
