//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
import Combine
import os

private let logger = OSLog(subsystem: "com.koski.swgoh", category: "Downloader")

public enum DownloadError : Error {
    case httpError(String)
    case responseCode(Int)
    case rateLimit
    case generic(String)
    case tooManyRetries
}

public struct Downloader {
    
    let user: String
    let password: String
    
    public init(user: String, password: String) {
        self.user = user
        self.password = password
    }
    
    var token = UpdatableBox<Token>()
    
    let url = URL(string: "https://api.swgoh.help")!
    
    let session = URLSession.shared
    
    struct Token : Decodable {
        let access_token: String
        
        var token: String { "Bearer " + access_token }
    }
    
    func newRequest<T: Encodable>(_ path: String, token: Token, _ body: T) -> URLRequest {
        var request = URLRequest(url: url.appendingPathComponent(path))
        request.setValue(token.token, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.httpBody = try! JSONEncoder().encode(body)
        request.timeoutInterval = 120
        return request
    }
    
    func login() -> AnyPublisher<Token, Error> {
        if let token = token.value {
            return Just(token).setFailureType(to: Error.self).eraseToAnyPublisher()
        }
        
        var request = URLRequest(url: url.appendingPathComponent("auth/signin"))

        let body = "username=\(user)&password=\(password)&grant_type=password&client_id=123&client_secret=abc".data(using: .utf8)!
        
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
        request.addValue(body.count.description, forHTTPHeaderField: "Content-Length")
        request.httpMethod = "POST"
        request.httpBody = body
        
        return session.dataTaskPublisher(for: request)
            .checkHTTPResponse()
            .decode(type: Token.self, decoder: JSONDecoder())
            .memoize(in: self.token)
            .observe { _ in os_log(.debug, log: logger, "swgoh.help login ok") }
            .eraseToAnyPublisher()
    }

    struct GuildResponse : Decodable {
        let name: String
        let roster: [GuildRosterEntry]
        
        struct GuildRosterEntry : Decodable {
            let name: String
            let allyCode: Int
            let gp: Int
        }
    }
    
    private func fetchGuildPlayerIds(playerId: Int, token: Token) -> AnyPublisher<(token: Token, guild: GuildResponse), Error> {
        let request = newRequest("swgoh/guild/", token: token, ["allycode" : playerId])
        return session.dataTaskPublisher(for: request)
            .checkHTTPResponse()
            .decode(type: [GuildResponse].self, decoder: JSONDecoder())
            .map { guilds in
                (token: token, guild: guilds[0])
            }
            .observe { result in os_log(.debug, log: logger, "fetchPlayerIds %d -> [%d]", playerId, result.guild.roster.count) }
            .eraseToAnyPublisher()
    }

    private func fetch(playerIds: [Int], token: Token) -> AnyPublisher<[SWGOHHelpPlayer.Player], Error> {
        let request = newRequest("swgoh/player/", token: token, [ "allycode" : playerIds])
        os_log(.debug, log: logger, "request %d players", playerIds.count)
        return session.dataTaskPublisher(for: request)
            .checkHTTPResponse()
            .decode(type: [SWGOHHelpPlayer.Player].self, decoder: JSONDecoder())
            .observe { players in os_log(.debug, log: logger, "received %d players", players.count) }
            .eraseToAnyPublisher()
    }
    
    private func batchFetch(shortName: String, guildResponse: GuildResponse, token: Token) -> Future<SWGOHHelpPlayer.Guild, Error> {
        return Future<SWGOHHelpPlayer.Guild, Error> { promise in
            let playerIdsNeeded = Set(guildResponse.roster.map { $0.allyCode })
            var receivedPlayers = [Int:SWGOHHelpPlayer.Player]()
            
            let maxRetries = 10
            var retryCount = 0
                
            while true {
                let remainingPlayerIds = Array(playerIdsNeeded.subtracting(receivedPlayers.keys))
                if remainingPlayerIds.isEmpty {
                    break
                }
                
                if retryCount > maxRetries {
                    promise(.failure(DownloadError.tooManyRetries))
                    return
                }
                
                let players = self.fetch(playerIds: remainingPlayerIds, token: token)
                    .mapError { error -> Error in print(error); return error }
                    .replaceError(with: [])
                    .await()
                
                for player in players {
                    receivedPlayers[player.allyCode] = player
                }
                
                retryCount += 1
            }
            
            promise(.success(SWGOHHelpPlayer.Guild(name: guildResponse.name, shortName: shortName, players: Array(receivedPlayers.values))))
        }
    }
    
    public struct PlayerInfo {
        public let allyCode: Int
        public let guild: String
        public let name: String
        public let gp: Int
    }
    
    /// return the current guild name of a list of allyCodes.
    public func getGuildNames(allyCodes: [Int]) -> [Int:PlayerInfo] {
        var result = [Int:PlayerInfo]()
        
        for code in allyCodes {
            if result[code] == nil {
                do {
                    let response = try login()
//                        .observe { _ in os_log(.debug, log: logger, "fetching guild: \(code)") }
                        .flatMap { self.fetchGuildPlayerIds(playerId: code, token: $0) }
                        .await()
                    
                    let g = response.guild
                    for player in g.roster {
                        result[player.allyCode] = PlayerInfo(allyCode: player.allyCode, guild: g.name, name: player.name, gp: player.gp)
                    }
                } catch {
                    // by default we just can't provide the guild mapping
                    if let error = error as? DownloadError {
                        switch error {
                        case .responseCode(let statusCode):
                            if statusCode == 404 {
                                result[code] = PlayerInfo(allyCode: code, guild: "No Guild", name: "Unknown", gp: 0)
                            }
                        default: break
                        }
                    }
                }
            }
        }
        
        return result
    }
    
    public func fetchGuild(shortName: String, playerId: Int) -> AnyPublisher<Model.Guild, Error> {
        login()
            .observe { _ in os_log(.debug, log: logger, "fetching guild player ids") }
            .flatMap { self.fetchGuildPlayerIds(playerId: playerId, token: $0) }
            
            // need to receive this on another queue -- we don't want to tie up the URLSession queue
            .receive(on: DispatchQueue.global())
            .flatMap { self.batchFetch(shortName: shortName, guildResponse: $1, token: $0) }
            .tryMap { try SWGOHHelpPlayer.load($0) }
            .observe { _ in os_log(.debug, log: logger, "fetchGuild done") }
            .eraseToAnyPublisher()
    }
    
    public func fetchPlayer(allyCode: Int) -> AnyPublisher<Model.Player, Error> {
        login()
            .flatMap { self.fetch(playerIds: [ allyCode ], token: $0) }
            .map { $0.first!.modelPlayer }
            .receive(on: DispatchQueue.global())
            .observe { _ in os_log(.debug, log: logger, "fetchPlayer done") }
            .eraseToAnyPublisher()
    }
}
