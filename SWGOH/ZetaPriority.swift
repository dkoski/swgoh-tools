//
//  Abilities.swift
//  swgoh
//
//  Created by David Koski on 9/11/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

public struct ZetaPriority {
    
    static let missing = ZetaPriority(character: "missing", type: "missing", name: "missing", priorities: [20, 20, 20, 20, 20])
    
    let character: String
    let type: String
    let name: String
    let priorities: [Double]
    
    public func priority(_ type: String) -> Int {
        var value: Double
        switch type {
        case "HSR":
            value = priorities[0]
        case "PVP":
            value = priorities[1]
        case "Versa":
            value = priorities[2]
        case "TW":
            value = priorities[3]
        case "TB":
            value = priorities[4]
        default:
            fatalError("unknown type: \(type)")
        }
        
        return Int(5 * (20 - value))
    }
    
    static let priorityNames = [ "HSR", "PVP", "Versa", "TW", "TB"]
    
    private static let zetaPriorities: [String:ZetaPriority] = {
        var result = [String:ZetaPriority]()
        
        for zeta in data {
            result[zeta.name.lowercased()] = zeta
        }
        
        return result
    }()
    
    /// look up a zeta priority by name
    public static func find(_ name:String) -> ZetaPriority {
        return zetaPriorities[name.lowercased()] ?? ZetaPriority.missing
    }
    
    static let popularityScoreRules = [
        "leaderskill_BASTILASHAN" : [
            // if you have Revan, you don't really need BS's lead skill
            "JKR" : -60,
        ]
    ]
    
    public static func modifyPopularityScore(player: Model.Player, abilityId: String, score: Int) -> Int {
        var newScore = score
        if let rule = popularityScoreRules[abilityId] {
            for (otherCharacterName, bonus) in rule {
                let otherCharacter = Model.Character.find(otherCharacterName)
                if let unit = player.units[otherCharacter.id] {
                    if unit.gear >= 10 {
                        newScore += bonus
                    }
                }
            }
        }
        return max(newScore, 0)
    }
}
