//
//  Copyright © 2019 David Koski. All rights reserved.
//

public struct Squad : CustomStringConvertible, Hashable, Codable, Comparable {
    public let units: [Model.Unit]
    public let gp: Int
    public var properties: Properties
    
    public var isFleet: Bool

    // memoize this -- it is used for hashing and comparison
    private let unitIdsMinusLeader: [String]

    /// Properties of the squad -- these can be built before the units of the squad are assembled
    public struct Properties : Codable {
        /// human readable name.  in particular this is the variant name if it has one (e.g. NestSisters)
        public let name: String
        
        /// the canonical name of the team.  this is often the same as the name, but in some cases it will be a more generic form, e.g. NestSisters / NS
        public let canonicalName: String
        
        /// id to match Counters for attacks
        public let attackerId: String?
        
        /// id to match Counters for defense
        public let defenderId: String?
        public let preference: Int
        var unitMinimumGP: Int
        let rules: [String:UnitEvaluator]

        public init(name: String, canonicalName: String? = nil, attackerId: String? = nil, defenderId: String? = nil, preference: Int = 0, unitMinimumGP: Int = 0, rules: [String:UnitEvaluator] = [:]) {
            self.name = name
            self.canonicalName = canonicalName ?? name
            self.attackerId = attackerId
            self.defenderId = defenderId ?? attackerId
            self.preference = preference
            self.unitMinimumGP = unitMinimumGP
            self.rules = UnitEvaluator.canonicalize(rules)
        }
        
        enum CodingKeys: String, CodingKey {
            case name
            case canonicalName
            case preference
            case attackerId
            case defenderId
        }

        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            
            self.name = try values.decode(String.self, forKey: .name)
            if values.contains(.canonicalName) {
                self.canonicalName = try values.decode(String.self, forKey: .canonicalName)
            } else {
                self.canonicalName = self.name
            }
            if values.contains(.attackerId) {
                self.attackerId = try? values.decode(String?.self, forKey: .attackerId)
            } else {
                self.attackerId = nil
            }
            if values.contains(.defenderId) {
                self.defenderId = try? values.decode(String?.self, forKey: .defenderId)
            } else {
                self.defenderId = nil
            }
            self.preference = try values.decode(Int.self, forKey: .preference)
            self.unitMinimumGP = 0
            self.rules = [:]
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case units
        case gp
        case properties
    }
    
    init(_ properties: Properties, units: [Model.Unit], isFleet: Bool? = nil) {
        self.properties = properties
        self.units = units
        self.gp = units.reduce(0) { $0 + $1.gp }
        self.unitIdsMinusLeader = units.dropFirst().map { $0.id }.sorted()
        self.isFleet = isFleet ?? units[0].isShip
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.units = try values.decode([Model.Unit].self, forKey: .units)
        self.gp = try values.decode(Int.self, forKey: .gp)
        self.properties = try values.decode(Properties.self, forKey: .properties)
        
        self.unitIdsMinusLeader = units.dropFirst().map { $0.id }.sorted()
        self.isFleet = units[0].isShip
    }
        
    public var name: String {
        return properties.name
    }
    
    public var canonicalName: String {
        return properties.canonicalName
    }
        
    public var counterTeam: CounterTeam? {
        if let id = properties.defenderId {
            return CounterTeam.find(id)
        } else {
            return nil
        }
    }
    
    public var attackTeam: CounterTeam? {
        if let id = properties.attackerId {
            return CounterTeam.find(id)
        } else {
            return nil
        }
    }
    
    public var description: String {
        return units.map { $0.commonName }.joined(separator: ", ")
    }
    
    public func description(images: Bool = false, zetas: Bool = true, gearBelow: Int = 0, starsBelow: Int = 0) -> String {
        return units.enumerated().map { index, unit in
            var result = ""
            if zetas {
                for _ in unit.zetas {
                    result += "z"
                }
            }
            result += unit.commonName
            let stars = unit.stars < starsBelow ? " \(unit.stars)*" : ""
            result += stars

            let g = !unit.isShip && unit.gear < gearBelow ? " \(unit.gearLevelDescription)" : ""
            result += g
            
            let needsComma = unit.commonName.contains(" ") || (!unit.isShip && unit.gear < gearBelow)
            if needsComma && index < units.count - 1 && !images {
                result += ","
            }

            if images {
                return "<div class=\"toT\"><img class=\"toI squad \(unit.character.id)-i\"><br/>\(result)</div>"
            } else {
                return result
            }
            }.joined(separator: " ")
    }
        
    public func hash(into hasher: inout Hasher) {
        // the lead is considered separate
        hasher.combine(units[0].id)
        
        // combine the rest in a canonical order
        hasher.combine(unitIdsMinusLeader)
    }

    public static func == (lhs: Squad, rhs: Squad) -> Bool {
        if lhs.units.count == 0 || rhs.units.count == 0 {
            // e.g. an Any Squad from TW
            return false
        }
        if lhs.units[0] == rhs.units[0] {
            return lhs.unitIdsMinusLeader == rhs.unitIdsMinusLeader
        }
        return false
    }
    
    public static func < (lhs: Squad, rhs: Squad) -> Bool {
        if lhs.name != rhs.name {
            return lhs.name < rhs.name
        }
        return lhs.gp < rhs.gp
    }

}

public enum Status : Int, Comparable, Hashable, Codable {
    
    case ok = 100
    case needsTuning = 90
    case needsGear = 75
    case needsRequiredGear = 60
    case needsRequired = 50
    case needsFarming = 25
    case needsUnlock = 0
    case unknown = -1
    
    public var isViable: Bool {
        switch self {
        case .ok, .needsTuning, .needsGear, .needsRequiredGear:
            return true
        default:
            return false
        }
    }
    
    public var viability: String {
        switch self {
        case .ok: return ":white_check_mark:"
        case .needsTuning: return ""
        case .needsGear: return ":small_orange_diamond:"
        case .needsRequiredGear: return ":gear:"
        case .needsRequired: return ":point_left:"
        case .needsFarming: return ":octagonal_sign:"
        case .needsUnlock: return ":octagonal_sign::closed_lock_with_key:"
        case .unknown: return "???"
        }
    }
    
    public var marker: String {
        switch self {
        case .ok: return ""
        case .needsTuning: return ""
        case .needsGear: return ":small_orange_diamond:"
        case .needsRequiredGear: return ":gear:"
        case .needsRequired: return ":point_left:"
        case .needsFarming: return ":octagonal_sign:"
        case .needsUnlock: return ":octagonal_sign::closed_lock_with_key:"
        case .unknown: return "???"
        }
    }
    
    public var unicodeMarker: String {
        switch self {
        case .ok: return ""
        case .needsTuning: return ""
        case .needsGear: return "🔸"
        case .needsRequiredGear: return "⚙️"
        case .needsRequired: return "👈"
        case .needsFarming: return "🛑"
        case .needsUnlock: return "🛑"
        case .unknown: return "???"
        }
    }

    public static func < (lhs: Status, rhs: Status) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }

}

/// The result of a SquadEvaluator.  This answers questions
/// like: is this squad workable?  What problems does it have?
public struct SquadEvaluation : CustomStringConvertible, Hashable, Codable, Comparable {
    public let squad: Squad
    let evaluations: [String:EvaluationSummary]
    
    init(squad: Squad, evaluations: [String:EvaluationSummary]) {
        self.squad = squad
        self.evaluations = evaluations
    }
    
    public var scorePercent: Double {
        return Double(score) / Double(max)
    }

    public var score: Int {
        return evaluations.values.reduce(0) { $0 + $1.score }
    }
    
    var max: Int {
        return evaluations.values.reduce(0) { $0 + $1.max }
    }
    
    public var preference: Int {
        return evaluations.values.reduce(0) { $0 + $1.preference } + squad.properties.preference
    }
    
    public var status: Status {
        return evaluations.values.reduce(Status.ok) { min($0, $1.status) }
    }
    
    public var viabilityScore: Int {
        return evaluations.values.reduce(0) { $0 + $1.status.rawValue }
    }
    
    public var unitEvaluations: [(Model.Unit, EvaluationSummary)] {
        return squad.units.map { ($0, evaluations[$0.id]!) }
    }
    
    public func gearNeeded() -> String? {
        if self.status <= .needsRequiredGear {
            var result = ""
            for (unit, summary) in self.unitEvaluations {
                let messages = summary.messages.filter { $0.status <= .needsRequiredGear }
                if !messages.isEmpty {
                    result += "\(unit.commonName): \(messages.map { $0.message }.joined(separator: ", ")).  "
                }
            }
            return result
        }
        return nil
    }
    
    public var description: String {
        let unitResult = unitEvaluations.map { (unit, e) -> String in
            return "\(unit.commonName) \(e.status.viability)"
        }.joined(separator: ", ")
        return "\(unitResult): status: \(status) viabilityScore: \(viabilityScore) preference: \(preference) score: \(scorePercent)"
    }
    
    public static func == (lhs: SquadEvaluation, rhs: SquadEvaluation) -> Bool {
        return lhs.squad == rhs.squad
    }

    public static func < (lhs: SquadEvaluation, rhs: SquadEvaluation) -> Bool {
        lhs.squad < rhs.squad
    }
    
    public func hash(into hasher: inout Hasher) {
        squad.hash(into: &hasher)
    }

}

/// The result of a UnitEvaluator (typically part of a SquadEvaluation).
public struct EvaluationSummary : Hashable, Codable {
    
    public var status = Status.ok
    var score = 0
    var max = 0
    var preference = 0
    
    public var messages = [Message]()
    
    mutating func add(message: Message) {
        score += message.score
        max += message.max
        preference += message.preference
        
        if message.status < status {
            status = message.status
        }
        
        messages.append(message)
    }
}

/// A single message with information about the Unit.  Messages that are Status.ok typically don't need to be printed.  The rest contain some information about why the Unit is not perfect.
public struct Message : Comparable, Hashable, Codable {
    public let message: String
    public let status: Status
    public let score: Int
    public let max: Int
    
    /// a way to mark a unit as preferred (positive) or not recommended (negative)
    let preference: Int
    
    public init(message: String, status: Status, score: Int, max: Int, preference: Int = 0) {
        self.message = message
        self.status = status
        self.score = score
        self.max = max
        self.preference = preference
    }
    
    public static func < (lhs: Message, rhs: Message) -> Bool {
        if lhs.status == rhs.status {
            let ls = Double(lhs.score) / Double(lhs.max == 0 ? 1 : lhs.max)
            let rs = Double(rhs.score) / Double(rhs.max == 0 ? 1 : lhs.max)
            return ls < rs
        } else {
            return lhs.status < rhs.status
        }
    }
    

}

public struct UnitEvaluator {
    
    var rules: [UnitEvaluationRule]
    
    public init(_ rules: UnitEvaluationRule...) {
        self.rules = rules
    }
    
    mutating func add(_ rule: UnitEvaluationRule) {
        rules.append(rule)
    }
    
    func evaluate(unit: Model.Unit) -> EvaluationSummary {
        var evaluation = EvaluationSummary()
        
        for rule in rules {
            evaluation.add(message: rule.evaluate(unit: unit))
        }

        // special case for not unlocked -- we collect all the messages above for the scoring
        if unit.stars == 0 {
            evaluation.messages.removeAll()
            evaluation.add(message: Message(message: "not unlocked", status: .needsUnlock, score: 0, max: 0))
        }
        
        return evaluation
    }
    
    /// converts rules from [ name : evaluator ] -> [ character_id : evaluator ]
    static func canonicalize(_ rules: [String:UnitEvaluator]) -> [String:UnitEvaluator] {
        var result = [String:UnitEvaluator]()
        
        for (id, evaluator) in rules {
            if id == "DEFAULT" {
                result[id] = evaluator
            } else {
                let character = Model.Character.find(id)
                result[character.id] = evaluator
            }
        }
        
        return result
    }
}

extension UnitEvaluator {
    
    func asUnitRule(id: String) -> SquadRule.UnitRule {
        var result = SquadRule.UnitRule(id: id, requiredZetas: Set<String>(), minimumGearLevel: 10, minimumStars: 7, preference: 0)
        
        for rule in rules {
            rule.update(id: id, unitRule: &result)
        }
        
        return result
    }
    
}

public struct SquadEvaluator {
    
    let rules: [String:UnitEvaluator]
    
    public init(rules: [String:UnitEvaluator]) {
        self.rules = UnitEvaluator.canonicalize(rules)
    }
    
    func evaluate(squad: Squad) -> SquadEvaluation {
        var evaluations = [String:EvaluationSummary]()
        for unit in squad.units {
            let rules = squad.properties.rules.isEmpty ? self.rules : squad.properties.rules
            guard let evaluator = rules[unit.id] ?? rules["DEFAULT"] else {
                fatalError("no evaluation rule for \(unit.id)")
            }
            
            let unitEvaluation = evaluator.evaluate(unit: unit)
            evaluations[unit.id] = unitEvaluation
        }
        
        let squadEvaluation = SquadEvaluation(squad: squad, evaluations: evaluations)
        return squadEvaluation
    }
    
    public func evaluate(squads: [Squad]) -> [SquadEvaluation] {
        var squadEvaluations = [SquadEvaluation]()
        
        for squad in squads {
            squadEvaluations.append(evaluate(squad: squad))
        }
        
        return squadEvaluations
    }
        
}

// MARK: - Rules

public protocol UnitEvaluationRule {
    
    func evaluate(unit: Model.Unit) -> Message
    
    func update(id: String, unitRule: inout SquadRule.UnitRule)
}

public protocol AtLeastEvaluationRule : UnitEvaluationRule {

    var atLeast: Int { get }
    var target: Int { get }
    var weight: Int { get }
    var status: Status { get }

}

public extension AtLeastEvaluationRule {
    
    func evaluate(value: Int, name: String) -> Message {
        if value < atLeast {
            let text = "\(name) is \(value)/\(target)"
            return Message(message: text, status: status, score: 0, max: weight)
        } else if value < target {
            let text = "\(name) is \(value)/\(target)"
            let score = Int(Double(value - atLeast) / Double(target - atLeast) * Double(weight))
            return Message(message: text, status: .needsTuning, score: score, max: weight)
        } else {
            return Message(message: "\(name) OK", status: .ok, score: weight, max: weight)
        }
    }

}

public struct UnitRule : AtLeastEvaluationRule {
    
    let key: Model.Unit.CodingKeys
    public let atLeast: Int
    public let target: Int
    public let weight: Int
    public let status: Status
    
    public init(_ key: Model.Unit.CodingKeys, atLeast: Int, target: Int, weight: Int = 100, status: Status = .needsFarming) {
        self.key = key
        self.atLeast = atLeast
        self.target = target
        self.weight = weight
        self.status = status
    }
    
    public func evaluate(unit: Model.Unit) -> Message {
        let value = unit.value(key)
        return evaluate(value: value, name: key.name)
    }

    /// custom rule for this -- hard stop at G7 and below.
    public static let gear10PlusRule = CustomRule { (unit) -> Message in
        switch unit.gear {
        case 0 ... 7:
            return Message(message: "gear level must be at least 8", status: .needsRequired, score: 0, max: 400)
        case 8 ... 9:
            return Message(message: "should be G10 (\(unit.gearLevelDescription))", status: .needsRequiredGear, score: 100, max: 400)
        case 10:
            return Message(message: "gear G10/G12", status: .needsTuning, score: 200, max: 400)
        case 11:
            return Message(message: "gear G11/G12", status: .needsTuning, score: 300, max: 400)
        case 12 ..< Int.max:
            return Message(message: "G12", status: .ok, score: 400, max: 400)
        default:
            return Message(message: "unknown gear level", status: .needsRequired, score: 0, max: 400)
        }
    }
    
    public static let gear9PlusRule = CustomRule { (unit) -> Message in
        switch unit.gear {
        case 0 ... 6:
            return Message(message: "gear level must be at least 7", status: .needsRequired, score: 0, max: 400)
        case 7 ... 8:
            return Message(message: "should be G9 (\(unit.gearLevelDescription))", status: .needsRequiredGear, score: 100, max: 400)
        case 9:
            return Message(message: "gear G10/G12", status: .needsTuning, score: 150, max: 400)
        case 10:
            return Message(message: "gear G10/G12", status: .needsTuning, score: 200, max: 400)
        case 11:
            return Message(message: "gear G11/G12", status: .needsTuning, score: 300, max: 400)
        case 12 ..< Int.max:
            return Message(message: "G12", status: .ok, score: 400, max: 400)
        default:
            return Message(message: "unknown gear level", status: .needsRequired, score: 0, max: 400)
        }
    }
    
    public static let level85Rule = UnitRule(.level, atLeast: 85, target: 85, weight: 200, status: .needsTuning)
    public static let sevenStarRule = UnitRule(.stars, atLeast: 7, target: 7, weight: 200, status: .needsFarming)
    
    public func update(id: String, unitRule: inout SquadRule.UnitRule) {
        if key == .gear {
            unitRule.minimumGearLevel = atLeast
        }
        if key == .stars {
            unitRule.minimumStars = atLeast
        }
    }

}

public struct AllOmegasRule : UnitEvaluationRule {
    
    let weight: Int
    let status: Status
    
    public init(weight: Int = 100, status: Status = .needsGear) {
        self.weight = weight
        self.status = status
    }
    
    public func evaluate(unit: Model.Unit) -> Message {
        var missingAbilities = [Model.Unit.Skill]()
        var totalOmegas = 0
        for ability in unit.skills {
            if ability.isOmega {
                totalOmegas += 1
                if !ability.hasOmegaZeta {
                    missingAbilities.append(ability)
                }
            }
        }
        if missingAbilities.isEmpty {
            return Message(message: "all omegas present", status: .ok, score: totalOmegas * weight, max: totalOmegas * weight)
        } else {
            return Message(message: "missing omegas: \(missingAbilities.map { $0.name }.joined(separator: ", "))", status: status, score: (totalOmegas - missingAbilities.count) * weight, max: totalOmegas * weight)
        }
    }
    
    public func update(id: String, unitRule: inout SquadRule.UnitRule) {
    }

}

public struct OmegaZetaAbilityRule : UnitEvaluationRule {
    
    let id: String
    let weight: Int
    let status: Status
    
    public init(_ id: String, weight: Int = 100, status: Status = .needsRequired) {
        self.id = id
        self.weight = weight
        self.status = status
    }
    
    public func evaluate(unit: Model.Unit) -> Message {
        var name: String? = nil
        for ability in unit.skills {
            if ability.id == id {
                if ability.hasOmegaZeta {
                    return Message(message: "\(ability.name) has \(ability.isZeta ? "zeta" : "omega")", status: .ok, score: weight, max: weight)
                }
                name = ability.name
            }
        }
        return Message(message: "\(name ?? "zeta") missing", status: status, score: 0, max: weight)
    }
    
    public func update(id: String, unitRule: inout SquadRule.UnitRule) {
        if let ability = Model.Ability.abilities[self.id] {
            if ability.isZeta {
                unitRule.requiredZetas.insert(self.id)
            }
        }
    }

}

public struct PreferenceRule : UnitEvaluationRule {
    
    let preference: Int
    
    /// only apply the preference if the unit is 7 stars, e.g. it is a very difficult unlock (legendaries)
    let ifSevenStar: Bool
    
    public init(preference: Int = 1, ifSevenStar: Bool = false) {
        self.preference = preference
        self.ifSevenStar = ifSevenStar
    }
    
    public func evaluate(unit: Model.Unit) -> Message {
        if ifSevenStar && unit.stars != 7 {
            return Message(message: "ok", status: .ok, score: 0, max: 0)
        }
        return Message(message: "unit boost", status: .ok, score: 0, max: 0, preference: preference)
    }
    
    public func update(id: String, unitRule: inout SquadRule.UnitRule) {
        unitRule.preference = preference
    }

}

public struct CustomRule : UnitEvaluationRule {
    
    let rule: (Model.Unit) -> Message
    
    public func evaluate(unit: Model.Unit) -> Message {
        return rule(unit)
    }
    
    public init(rule: @escaping (Model.Unit) -> Message) {
        self.rule = rule
    }
    
    public func update(id: String, unitRule: inout SquadRule.UnitRule) {
    }

}

// MARK: - TW

// TODO eventually I want to 1) rename this, 2) make this be the rule representation (directly), 3) remove the .e, and UnitEvaluator, 4) make it Hashable, 5) cache the values, 6) probably replace the PlayerSelector/UnitSelector
public struct TWRule : UnitEvaluationRule {
    
    let requiresLeaderZeta: Bool
    let requiresUniqueZetas: Bool
    let minimumStars: Int
    let minimumGear: Int
    let maximumGear: Int
    let preference: Int
    let minimumPower: Int
    
    public init(leaderZeta: Bool = false, uniqueZetas: Bool = false, stars: Int = 7, gear: Int = 8, maximumGear: Int = 0, preference: Int = 0, gp: Int = 0) {
        self.requiresLeaderZeta = leaderZeta
        self.requiresUniqueZetas = uniqueZetas
        self.minimumStars = stars
        self.minimumGear = gear
        self.maximumGear = maximumGear
        self.preference = preference
        self.minimumPower = gp
    }
    
    func score(_ value: Int, _ min: Int, _ max: Int, _ scale: Int) -> Int {
        if min == max {
            return scale
            
        } else {
            let numerator = Double(value - min)
            let denominator = Double(max - min)
            
            return Int((numerator / denominator) * Double(scale))
        }
    }
    
    public func evaluate(unit: Model.Unit) -> Message {
        let hasLeaderZeta = unit.zetaIds.contains { $0.starts(with: "leaderskill") }
        let hasUniqueZeta = unit.zetaIds.contains { $0.starts(with: "uniqueskill") || $0.starts(with: "specialskill") }
        
        if (requiresLeaderZeta && !hasLeaderZeta) || (requiresUniqueZetas && !hasUniqueZeta) {
            return Message(message: "missing zetas", status: .needsRequired, score: 0, max: 1000)
        }
        
        if unit.level < 70 {
            return Message(message: "low level", status: .needsRequired, score: 0, max: 1000)
        }
        
        if unit.stars < minimumStars {
            return Message(message: "low stars", status: .needsRequired, score: 0, max: 1000)
        }
        
        if unit.gp < self.minimumPower {
            return Message(message: "low gp", status: .needsRequired, score: 0, max: 1000)
        }
        
        let gearScore: Int
        if minimumGear > 20 {
            // relics
            if unit.gear < 13 {
                return Message(message: "low gear", status: .needsRequired, score: 0, max: 1000)
            }
            if unit.relic - 2 < minimumGear - 20 {
                return Message(message: "low relic", status: .needsRequired, score: 400, max: 1000)
            }
            gearScore = score(unit.relic - 2, minimumGear - 20, 7, 700)
        } else {
            if unit.gear < minimumGear {
                return Message(message: "low gear", status: .needsRequired, score: 0, max: 1000)
            }
            gearScore = score(unit.gear + (unit.relic - 2), minimumGear, 12, 700)
        }
        
        if maximumGear > 0 {
            if maximumGear > 20 {
                if unit.relic - 2 > maximumGear - 20 {
                    return Message(message: "gear level exceeded", status: .needsRequired, score: 0, max: 1000)
                }
            } else {
                if unit.gear > maximumGear {
                    return Message(message: "gear level exceeded", status: .needsRequired, score: 0, max: 1000)
                }
            }
        }
        
        let s = score(unit.stars, minimumStars, 7, 300) + gearScore
        
        return Message(message: "ok", status: .ok, score: s, max: 1000, preference: self.preference)
    }
    
    static func table(_ namesAndPreference: (String, Int)...) -> [String:UnitEvaluator] {
        var result = [String:UnitEvaluator]()
        
        for (name, preference) in namesAndPreference {
            result[name] = TWRule(preference: preference).e
        }
        
        return result
    }
    
    public var e: UnitEvaluator {
        return UnitEvaluator(self)
    }
    
    public func update(id: String, unitRule: inout SquadRule.UnitRule) {
        if requiresLeaderZeta || requiresUniqueZetas {
            if let abilities = Model.Ability.abilitiesByUnitId[id] {
                for ability in abilities where ability.isZeta {
                    if ability.abilityType == .leader && requiresLeaderZeta {
                        unitRule.requiredZetas.insert(ability.id)
                    }
                    if ability.abilityType == .unique && requiresUniqueZetas {
                        unitRule.requiredZetas.insert(ability.id)
                    }
                }
            }
        }
        
        unitRule.minimumStars = minimumStars
        unitRule.minimumGearLevel = minimumGear
        unitRule.preference = preference
    }

}
