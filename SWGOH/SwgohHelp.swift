//
//  SwgohHelp.swift
//  swgoh
//
//  Created by David Koski on 5/13/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation
import Combine

// Serialization for https://api.swgoh.help with a mappping to the structures in Serialization (Player, etc.)

public struct SWGOHHelp {
    
    public static func load(_ data:Data) throws -> Model.Guild {
        return try SWGOHHelpPlayer.load(data)
    }
}

/// structure describing the format of swgoh.help's /player/ API
struct SWGOHHelpPlayer {
    
    static func load(_ data:Data) throws -> Model.Guild {
        let jsonDecoder = JSONDecoder()

        let guild = try jsonDecoder.decode(Guild.self, from: data)
        return try load(guild)
    }
        
    static func load(_ guild:Guild) throws -> Model.Guild {

        var players = [Model.Player]()
        for player in guild.players {
            players.append(player.modelPlayer)
        }
                
        let guildGp = players.reduce(0) { $0 + $1.stats.gp }
        let guildInfo = Model.Guild.Info(gp: guildGp, id: 0, memberCount: players.count, name: guild.name, shortName: guild.shortName, profileCount: 0, rank: 0)
        let guildFile = Model.Guild(info: guildInfo, players: players)
        
        return guildFile
    }
    
    struct Player : Codable {
        let name: String
        let allyCode: Int
        let roster: [Unit]

        var modelPlayer: Model.Player {
            let roster = Dictionary(uniqueKeysWithValues: self.roster.map { ($0.id, $0) })
            
            func crew(unit: Unit) -> [Unit] {
                if let crewIds = crewIds(unitId: unit.id) {
                    return crewIds.map { roster[$0]! }
                } else {
                    return []
                }
            }
            
            let units = Dictionary(uniqueKeysWithValues: self.roster.map { ($0.id, $0.asModelUnit(crew: crew(unit: $0))) })
                        
            return Model.Player(name: self.name, allyCode: self.allyCode, units: units)
        }
    }
    
    struct Unit : Codable, StatsUnit {
        let id: String
        let gp: Int
        let gear: Int
        let stars: Int
        let level: Int
        let relicData: Relic?
        let skills: [Model.Unit.Skill]
        let mods: [Model.Mod]
        let equipment: [Model.Unit.Equipment]
        
        var relic: Int { relicData?.currentTier ?? 0 }
        
        var isShip: Bool { Model.Character.find(id).ship }
        
        enum CodingKeys: String, CodingKey {
            case id = "defId"
            case gp
            case gear
            case stars = "rarity"
            case level
            case relicData = "relic"
            case skills
            case mods
            case equipment = "equipped"
        }

        func asModelUnit(crew: [Unit]) -> Model.Unit {
            let hasStats = canCalculateStats(unit: self)
            let gp = hasStats ? calculateGP(unit: self, crew: crew) : 0
            let stats = hasStats ? calculateStats(unit: self, crew: crew) : UnitStats()
            
            return Model.Unit(id: self.id, gearLevel: self.gear, relic: self.relic, level: self.level, gp: gp, stars: self.stars, skills: self.skills, mods: self.mods, equipment: self.equipment, stats: stats)
        }
    }
    
    struct Relic : Codable {
        let currentTier: Int
    }
    
    struct Guild : Codable {
        let name: String
        let shortName: String
        let players: [Player]
    }
    
}
