//
//  Copyright © 2019 David Koski. All rights reserved.
//

public protocol Report {

    func report(data: [Model.Player:[SquadEvaluation]]) -> String
    
    func report(player: Model.Player, evaluations: [SquadEvaluation]) -> String
    
}

public extension Report {
    
    func report(data: [Model.Player:[SquadEvaluation]]) -> String {
        var result = ""
        for player in data.keys.sorted() {
            result += report(player: player, evaluations: data[player]!)
            result += "\n"
        }
        return result
    }

}

/// produces detailed dump of the evaluations
public struct DebugReport : Report {
    
    public init() {
    }
    
    public func report(player: Model.Player, evaluations: [SquadEvaluation]) -> String {
        var result = ""
        
        result += "\(player.name):\n"
        
        var index = 1
        for squadEv in evaluations {
            result += "Variation \(index) \(squadEv.squad.name.appending(" "))status: \(squadEv.status) viabilityScore: \(squadEv.viabilityScore) \(squadEv.status.isViable ? "viable " : " ")preference: \(squadEv.preference) quality: \(Int(squadEv.scorePercent * 100))\n"
            
            for (unit, unitEv) in squadEv.unitEvaluations {
                result += "\(unit.commonName): \(unitEv.status) viabilityScore: \(unitEv.status.rawValue) preference: \(unitEv.preference) quality: \(Int(Double(unitEv.score) / Double(unitEv.max)  * 100)) \(unitEv.score)/\(unitEv.max)\n"
                for message in unitEv.messages {
                    result += "\t\(message.message) \(message.status) \(message.score)/\(message.max)\n"
                }
            }
            result += "\n"

            index += 1
        }
        
        return result
    }
    
}
