//
//  ModsPattern.swift
//  SWGOH
//
//  Created by David Koski on 12/23/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

private let setFactor = 5
private let primaryFactor = 5
private let secondaryFactor = 1

/// require at least 5% to keep the pattern
private let patternThreshold = 0.05

/// A Histogram of mods and stats.
public struct ModHistogram: Hashable, Codable {
        
    public private(set) var health = 0
    public private(set) var protection = 0
    public private(set) var defense = 0
    public private(set) var offense = 0
    public private(set) var speed = 0
    public private(set) var cd = 0
    public private(set) var cc = 0
    public private(set) var ca = 0
    public private(set) var acc = 0
    public private(set) var potency = 0
    public private(set) var tenacity = 0
    
    public private(set) var count = 1
        
    public init(mods: [Model.Mod]) {
        self.add(mods: mods)
    }
    
    init(health: Int = 0, protection: Int = 0, defense: Int = 0, offense: Int = 0, speed: Int = 0, cd: Int = 0, cc: Int = 0, ca: Int = 0, acc: Int = 0, potency: Int = 0, tenacity: Int = 0, count: Int = 1) {
        self.health = health
        self.protection = protection
        self.defense = defense
        self.offense = offense
        self.speed = speed
        self.cd = cd
        self.cc = cc
        self.ca = ca
        self.acc = acc
        self.potency = potency
        self.tenacity = tenacity
        self.count = count
    }
    
    /// return the average value of the accumulated values
    public var average: ModHistogram {
        if count == 1 {
            return self
        } else {
            return ModHistogram(
                health: health / count,
                protection: protection / count,
                defense: defense / count,
                offense: offense / count,
                speed: speed / count,
                cd: cd / count,
                cc: cc / count,
                ca: ca / count,
                acc: acc / count,
                potency: potency / count,
                tenacity: tenacity / count
            )
        }
    }
    
    /// return a histogram normalized to 0 ... 100
    public var normalized: ModHistogram {
        // this is expected to run on a average histogram
        assert(count == 1)
        
        let scale = 100
        let maxValue = max(health, protection, defense, offense, speed, cd, cc, ca, acc, potency, tenacity)
        
        return ModHistogram(
            health: health * scale / maxValue,
            protection: protection * scale / maxValue,
            defense: defense * scale / maxValue,
            offense: offense * scale / maxValue,
            speed: speed * scale / maxValue,
            cd: cd * scale / maxValue,
            cc: cc * scale / maxValue,
            ca: ca * scale / maxValue,
            acc: acc * scale / maxValue,
            potency: potency * scale / maxValue,
            tenacity: tenacity * scale / maxValue
        )
    }
    
    public func dimensions() -> [Int] {
        return [health, protection, defense, offense, speed, cd, cc, ca, acc, potency, tenacity].map { $0 < 25 ? 0 : $0 }
    }
    
    public struct Priority : Codable, Comparable {
        public let c: Model.Mod.Characteristic
        public let v: Int
        
        public static func < (lhs: Priority, rhs: Priority) -> Bool {
            lhs.v > rhs.v
        }
    }
    
    public struct Value : Codable {
        public let c: Model.Mod.Characteristic
        public let v: Int
        
        init(c: Model.Mod.Characteristic, v: Double) {
            self.c = c
            self.v = c.isPercent ? Int(v * 100) : Int(v)
        }
    }
    
    public func priorities(threshold: Int = 25) -> [Priority] {
        // this is expected to run on a average histogram
        assert(count == 1)
        
        var result = [Priority]()
        func append(_ characteristic: Model.Mod.Characteristic, _ value: Int) {
            if value > threshold {
                result.append(Priority(c: characteristic, v: value))
            }
        }

        append(.health, health)
        append(.protection, protection)
        append(.defense, defense)
        append(.offense, offense)
        append(.speed, speed)
        append(.cd, cd)
        append(.cc, cc)
        append(.ca, ca)
        append(.acc, acc)
        append(.potency, potency)
        append(.tenacity, tenacity)
        
        result.sort()
        
        return result
    }
        
    mutating func add(mods: [Model.Mod]) {
        for mod in mods {
            add(mod: mod)
        }
    }
    
    mutating func add(mod: Model.Mod) {
        // only add set stats for pieces that vary
        add(set: mod.set)
        add(primary: mod.primaryStat, slot: mod.slot, pips: mod.pips)
        add(secondaries: mod.secondaryStat)
    }
    
    mutating func add(set: Int, factor: Int = setFactor) {
        switch set {
        case 1:         health += factor
        case 2:         offense += factor
        case 3:         defense += factor
        case 4:         speed += factor
        case 5:         cc += factor
        case 6:         cd += factor
        case 7:         potency += factor
        case 8:         tenacity += factor
        default: break
        }
    }
    
    private mutating func add(stat: Int, value: Int) {
        switch stat {
        case 1, 55:     health += value
        case 5:         speed += value
        case 16:        cd += value
        case 17:        potency += value
        case 18:        tenacity += value
        case 28, 56:    protection += value
        case 41, 48:    offense += value
        case 42, 49:    defense += value
        case 53:        cc += value
            
        // these two get a boost because there is only a single spot you can get them (arrow primary)
        case 52:        acc += 2 * value
        case 54:        ca += 2 * value
        default: break
        }
    }
    
    mutating func add(primary: Model.Mod.Stat, slot: Int, pips: Int, factor: Int = primaryFactor) {
        let factor = { () -> Int in
            switch slot {
            case 2:
                if primary.characteristic == .speed {
                    return factor
                } else {
                    // if we pick something other than speed for the arrow, that is important
                    return factor * 3
                }
                
            case 4:
                // the choice on the triangle is pretty important, boost it a little
                return factor * 2
                
            case 6:
                if primary.characteristic == .potency || primary.characteristic == .tenacity {
                    // these are big boosts and are considered important
                    return factor * 2
                } else {
                    return factor
                }
                
            default: return factor
            }
        }()
        
        if pips >= 6 {
            // if they upgraded it to 6 pips they get a big bonus for *some* stats
            if primary.characteristic != .protection {
                add(stat: primary.unitStat, value: factor * 3/2)
            } else {
                add(stat: primary.unitStat, value: factor)
            }
            
        } else {
            // otherwise only add the primary stat if they have a choice
            switch slot {
            case 2, 4, 5, 6: add(stat: primary.unitStat, value: factor)
            default: break
            }
        }
    }
    
    mutating func add(secondaries: [Model.Mod.Stat], factor: Int = secondaryFactor) {
        for stat in secondaries {
            // add stats with rolls above 1
            add(stat: stat.unitStat, value: factor * (stat.roll ?? 0))
        }
    }
    
    mutating func accumulate(histogram other: ModHistogram) {
        health += other.health
        protection += other.protection
        defense += other.defense
        offense += other.offense
        speed += other.speed
        cd += other.cd
        cc += other.cc
        ca += other.ca
        acc += other.acc
        potency += other.potency
        tenacity += other.tenacity
        
        count += other.count
    }
    
    public func contains(other: ModHistogram, threshold: Int = 25, clusterSize: Int) -> Bool {
        // manhattan distance ignoring uninteresting properties, bonus for large clusters
        assert(other.count == 1)

        let distance = self.average.distance(from: other, threshold: threshold)
        return distance <= clusterSize * (300 + self.count) / 300
    }
    
    public func distance(from other: ModHistogram, threshold: Int = 25) -> Int {
        // manhattan distance ignoring uninteresting properties
        assert(count == 1)
        assert(other.count == 1)
                
        func d(_ v1: Int, _ v2: Int, _ c: Model.Mod.Characteristic) -> Int {
            // if the stat isn' that important, ignore it
            if v1 < threshold && v2 < threshold {
                return 0
            }
            
            let v1 = v1 >= threshold ? v1 : 0
            let v2 = v2 >= threshold ? v2 : 0

            let d = abs(v1 - v2)
            if d < 20 {
                // if they are pretty close, consider it equal
                return 0
            }
            
            // otherwise return the scale (max of the values) times the distance
            return max(v1, v2) * d / 40
        }

        // variant of the distance -- this is for stats where you either have
        // it or you don't
        func mustHaveD(_ v1: Int, _ v2: Int) -> Int {
            if v1 != v2 {
                return 7500
            } else {
                return 0
            }
        }

        return
            d(health, other.health, .health) +
            d(protection, other.protection, .protection) +
            d(defense, other.defense, .defense) +
            d(offense, other.offense, .offense) +
            d(speed, other.speed, .speed) +
            d(cd, other.cd, .cd) +
            d(cc, other.cc, .cc) +
            mustHaveD(ca, other.ca) +
            mustHaveD(acc, other.acc) +
            d(potency, other.potency, .potency) +
            d(tenacity, other.tenacity, .tenacity)
    }
}

/// return the bonus from the given mods for the priorities (which stats)
private func modStats(unit: StatsUnit, priorities: [ModHistogram.Priority], mods: [Model.Mod]) -> [ModHistogram.Value] {
    let baseStats = calculateStats(unit: unit, mods: [], crew: [])
    let modsStats = (calculateStats(unit: unit, mods: mods, crew: []) - baseStats).modStats(priorities.map { $0.c }).map { ModHistogram.Value(c: $0.0, v: $0.1)}
    return modsStats
}

private func modStats(unit: StatsUnit, characteristics: [Model.Mod.Characteristic], mods: [Model.Mod]) -> [ModHistogram.Value] {
    let baseStats = calculateStats(unit: unit, mods: [], crew: [])
    let modsStats = (calculateStats(unit: unit, mods: mods, crew: []) - baseStats).modStats(characteristics).map { ModHistogram.Value(c: $0.0, v: $0.1)}
    return modsStats
}

public func modStats(character: Model.Character, gearLevel: Int, priorities: [ModHistogram.Priority], mods: [Model.Mod]) -> [ModHistogram.Value] {
    let unit = IdealUnit(id: character.id, gearLevel: gearLevel)
    return modStats(unit: unit, priorities: priorities, mods: mods)
}

/// return a score for the given stats and priorities -- higher is better.  this is an absolute value and can be used for ordering things.
/// it is transitive, so that a > b, b > c, thus a > c -- this is not true of the modScore() that has a target (below)
public func modScore(priorities: [ModHistogram.Priority], stats: [ModHistogram.Value], primaryBoost: Int = 150) -> Int {
    var score = 0
    
    for (priority, stat) in zip(priorities, stats) {
        let scale: Int
        let multiplier: Int
        
        // TODO combine scale
        switch priority.c {
        case .offense: scale = 10; multiplier = 1
        case .health: scale = 100; multiplier = 1
        case .protection: scale = 500; multiplier = 1
        case .defense: scale = 1; multiplier = 5
        default: scale = 1; multiplier = 1
        }
        
        let boost = priority.v == 100 ? (primaryBoost * primaryBoost) : priority.v * priority.v
        
//        print("\(priority.c)/\(priority.v): \(stat.v * multiplier / scale) = \(stat.v * multiplier / scale * boost)")
        
        score += stat.v * multiplier / scale * boost
    }
    
    return score
}

/// return a score for the given stats compared to a target (should be same gear level and config).  this should be more explainable
/// than the other mod score, but it isn't necessarily transitive, so not suitable for comparisons/sorting, though using a static score
/// should be fine.
public func modScore(priorities: [ModHistogram.Priority], stats: [ModHistogram.Value], targets: [ModHistogram.Value]) -> Int {
    var score = 0
    var boostSum = 0
    
    for (priority, stat, target) in zip3(priorities, stats, targets) {
        if target.v != 0 {
            if stat.c == .defense {
                // defense is a very small range and is not linear
                let boost = priority.v
                boostSum += priority.v
                score += boost * (stat.v + 50) / (target.v + 50)
            } else {
                let boost = Int(pow(Double(priority.v), 1.5))
                boostSum += boost
                score += boost * stat.v / target.v
            }
        }
    }
    
    score = score - boostSum
    
    return score
}

/// return stat adds if proposed is better than mods or nil if proposed is not as good
private func isBetter(unit: StatsUnit, priorities: [ModHistogram.Priority]? = nil, mods: [Model.Mod], proposed: [Model.Mod]) -> [ModHistogram.Value]? {
    let priorities = priorities ?? ModHistogram(mods: mods).normalized.priorities()
    
    let baseStats = calculateStats(unit: unit, mods: [], crew: [])
    let modsStats = (calculateStats(unit: unit, mods: mods, crew: []) - baseStats).modStats(priorities.map { $0.c }).map { ModHistogram.Value(c: $0.0, v: $0.1)}
    let proposedStats = (calculateStats(unit: unit, mods: proposed, crew: []) - baseStats).modStats(priorities.map { $0.c }).map { ModHistogram.Value(c: $0.0, v: $0.1)}
    
    let modsScore = modScore(priorities: priorities, stats: modsStats)
    let proposedScore = modScore(priorities: priorities, stats: proposedStats)
    
    if proposedScore > modsScore {
        return proposedStats
    } else {
        return nil
    }
}

private func partition(normalizedCenters: [ModHistogram], samples: [ModHistogram], normalizedSamples: [ModHistogram]) -> [[ModHistogram]] {
    var result = [[ModHistogram]](repeating: [], count: normalizedCenters.count)
    
    for (sample, normalizedSample) in zip(samples, normalizedSamples) {
        let (closestCenterIndex, _) = normalizedCenters
            .map { normalizedSample.distance(from: $0) }
            .enumerated()
            .min(by: { $0.1 < $1.1 })!
        
        result[closestCenterIndex].append(sample)
    }
    
    return result
}

private func centroid(samples: [ModHistogram]) -> ModHistogram {
    var sum = ModHistogram(count: 0)
    
    for sample in samples {
        sum.accumulate(histogram: sample)
    }
    
    return sum
}

func kMeans(seeds: [ModHistogram], samples: [ModHistogram], clusterSize: Int, maxIterations: Int = 10) -> [ModHistogram] {
    var centers = seeds
    
    let normalizedSamples = samples.map { $0.normalized }
    
    // the fewer samples we have, the larger the result clusters are -- I find
    // this works better than a lot of small clusters, even though we merge them
    let targetSamples = 6
    while centers.count <= targetSamples {
        centers.append(samples.randomElement()!)
    }
    
    for _ in 0 ..< maxIterations {
        // perturb it with more samples (they get trimmed by size at the end)
        var tryCenters = centers
        while tryCenters.count <= targetSamples {
            tryCenters.append(samples.randomElement()!)
        }

        if tryCenters.isEmpty {
            return []
        }

        var partitions = partition(normalizedCenters: tryCenters.map { $0.average.normalized }, samples: samples, normalizedSamples: normalizedSamples).filter { !$0.isEmpty }
        let proposedCenters = partitions.map { centroid(samples: $0).average }
        
        // merge any clusters that are too close
        var merge = [Int:[Int]]()
        var remove = Set<Int>()
        for (i1, c1) in proposedCenters.enumerated() {
            for (i2, c2) in proposedCenters.enumerated() {
                // same cluster, canonical order, already merged
                if i1 == i2 || i1 > i2 || remove.contains(i2) {
                    continue
                }
                
                if c1.distance(from: c2) <= clusterSize {
                    merge[i1, default: []].append(i2)
                    remove.insert(i2)
                }
            }
        }
                
        if !merge.isEmpty {
            for (target, others) in merge {
                for other in others {
                    partitions[target].append(contentsOf: partitions[other])
                }
            }
            for removeIndex in remove.sorted().reversed() {
                partitions.remove(at: removeIndex)
            }
        }
        
        // remove any that look too small
        let cutThreshold = max(samples.count / 100, 3)
        for (i, c) in partitions.enumerated().reversed() {
            if c.count < cutThreshold {
                partitions.remove(at: i)
            }
        }
        
        // if the largest partition is *too* big, it won't actually have anything near
        // its center
        var splitPartitions = [Int]()
        for (index, partition) in partitions.enumerated() where partition.count > samples.count / 8 {
            let center = centroid(samples: partition)
            let count = partition.filter { center.contains(other: $0, clusterSize: clusterSize)}.count

            if count == 0 {
                splitPartitions.append(index)
            }
        }
        
        for index in splitPartitions.reversed() {
            let partition = partitions.remove(at: index)
            
            // try and find widely separated points
            for _ in 0 ..< 100 {
                let p1 = partition.randomElement()!
                let p2 = partition.randomElement()!
                
                if p1.distance(from: p2) >= clusterSize * 2 {
                    partitions.append([p1])
                    partitions.append([p2])
                    break
                }
            }
            
            // if we fall through we will just drop the cluster and try a new random one
        }
        
        // we have the patterns partitioned -- accumulate all of the samples info
        // a single histogram which can act as our new center
        centers = partitions.map { centroid(samples: $0) }
    }
    
    return centers
}

/// Record for a unit that could take mods from another unit and benefit
public struct InterchangeEntry {
    /// source character
    let id: String
    
    /// current priorities
    let priorities: [ModHistogram.Priority]
    
    /// stats from the current mods
    let stats: [ModHistogram.Value]
    
    let swaps: [InterchangeUnit]
}

/// A unit whos mods we could use
public struct InterchangeUnit {
    /// other character id
    let id: String
    
    /// the priorities that make this attractive
    let priorities: [ModHistogram.Priority]
    
    /// stats from using these
    let stats: [ModHistogram.Value]
}

/// produce a set of interchangeable mods for a given player.  this will find all Units that
/// could have their mods replaced by the mods from another unit, e.g. on a temporary basis
/// where the stats will improve by some measure.  e.g. borring Padme's super-mods to run GBA
/// during a TB.
public func interchangeableMods(player: Model.Player, recommendations: [String:ModRecommendation]) -> [InterchangeEntry] {
    
    let viableMods = Dictionary(uniqueKeysWithValues: player.units
        .filter { $0.value.gear > 10 && $0.value.mods.count == 6 && !$0.value.isShip }
        .map { ($0.key, $0.value.mods) })
    
    if viableMods.isEmpty {
        return []
    }
    
    let unitIds = Array(player.units.keys)
    let result = DispatchQueue.concurrentPerform(iterations: unitIds.count) { index -> (String, InterchangeEntry?) in
        let id = unitIds[index]
        let unit = player.units[id]!
        
        if unit.gear < 10 || unit.isShip {
            return (id, nil)
        }
                
        // build a list of priorities we are trying to hit
        var priorities = [[ModHistogram.Priority]]()
        if let rec = recommendations[unit.id] {
            let threshold = rec.totalCount / 3
            for cluster in rec.clusters where cluster.count >= threshold {
                priorities.append(cluster.priorities)
            }
        }
        if let mods = viableMods[unit.id] {
            priorities.append(ModHistogram(mods: mods).normalized.priorities())
        }

        // now build a map of ids that are better than what is currently equipped
        // following these priorities
        var swapping = Set<String>()
        var swaps = [InterchangeUnit]()
        
        for priority in priorities {
            let bestSwap = viableMods
                .filter {
                    !swapping.contains($0.key) &&
                    isBetter(unit: unit, priorities: priority, mods: unit.mods, proposed: $0.value) != nil
                }
                .max { lhs, rhs in
                    isBetter(unit: unit, priorities: priority, mods: lhs.value, proposed: rhs.value) != nil
                }
            if let bestSwap = bestSwap, bestSwap.key != unit.id {
                // see if the best swap is actually better than what we have
                if let stats = isBetter(unit: unit, priorities: priority, mods: unit.mods, proposed: bestSwap.value) {
                    swaps.append(InterchangeUnit(id: bestSwap.key, priorities: priority, stats: stats))
                    swapping.insert(bestSwap.key)
                }
            }
        }
        
        if !swaps.isEmpty {
            let priorities: [ModHistogram.Priority]
            if let mods = viableMods[unit.id] {
                priorities = ModHistogram(mods: mods).normalized.priorities()
            } else {
                priorities = []
            }
            let baseStats = calculateStats(unit: unit, mods: [], crew: [])
            let modsStats = (calculateStats(unit: unit, mods: unit.mods, crew: []) - baseStats).modStats(priorities.map { $0.c }).map { ModHistogram.Value(c: $0.0, v: $0.1)}
            
            let entry = InterchangeEntry(id: unit.id, priorities: priorities, stats: modsStats, swaps: swaps)
            return (id, entry)
        } else {
            return (id, nil)
        }
    }
    
    return result.values.compactMap { $0 }
}

/// holder for a Unit that we can use to generate stats
private struct IdealUnit : StatsUnit {
    let id: String
    let stars = 7
    let level = 85
    var gear = 13
    var relic = 5 + 2
    let isShip = false
    let skills: [Model.Unit.Skill] = []
    let mods: [Model.Mod] = []
    let equipment: [Model.Unit.Equipment] = []
    
    init(id: String) {
        self.id = id
    }
    
    init(id: String, gearLevel: Int) {
        self.id = id
        let g = Model.GearLevel(gearLevel)
        self.gear = g.representedGearLevel
        self.relic = g.representedRelicLevel
    }
}

/// Collection of clusters for modding a single character, with examples
public struct ModRecommendation : Codable {
    
    public struct AttributeHistogram : Codable {
        public let characteristic: Model.Mod.Characteristic
        public let binSize: Int
        public let histogram: CountedSet<Int>
    }
    
    public struct Cluster : Codable {
        /// number of samples that fall into this cluster
        public let count: Int
        
        public let cluster: ModHistogram
        public let centroid: ModHistogram
        
        /// the normalized center of the cluster (compare distance to centroidNormalized to test membership)
        public let centroidNormalized: ModHistogram
        public let priorities: [ModHistogram.Priority]
        
        public let attributeHistograms: [AttributeHistogram]
        
        public struct Example : Codable {
            public let allyCode: Int
            public let histogram: ModHistogram
            public let mods: [Model.Mod]
            public let stats: [ModHistogram.Value]
            
            init(allyCode: Int, mods: [Model.Mod], priorities: [ModHistogram.Priority], ideal: StatsUnit) {
                self.allyCode = allyCode
                self.histogram = ModHistogram(mods: mods)
                self.mods = mods
                self.stats = modStats(unit: ideal, priorities: priorities, mods: mods)
            }
        }
        
        public let center: Example
        public let maximal: Example
    }
    
    public let clusters: [Cluster]
    public let totalCount: Int
    
    public let attributeHistograms: [AttributeHistogram]
    
    public static func build(players: [Model.Player], seedClusters: [String:ModRecommendation]? = nil, clusterSize: Int = 200, threshold: Double = 0.0) -> [String:ModRecommendation] {
        DispatchQueue.concurrentPerform(iterations: Model.Character.all.count) { index in
            let id = Model.Character.all[index].id
            return (id, build(players: players, id: id, clusterSize: clusterSize, threshold: threshold, seedClusters: seedClusters?[id]))
        }
    }
    
    private static func buildAttributeHistograms(unit: IdealUnit, characteristics: [Model.Mod.Characteristic], mods: [[Model.Mod]]) -> [AttributeHistogram] {
        var histograms = Array(repeating: CountedSet<Int>(), count: characteristics.count)
        var bins = [Int]()
        for m in mods {
            let stats = modStats(unit: unit, characteristics: characteristics, mods: m)
            
            for (index, stat) in stats.enumerated() {
                
                // TODO combine -- scale
                // bin some of the stats to the nearest x
                let bin: Int
                switch stat.c {
                case .offense: bin = 100
                case .protection, .health: bin = 2500
                case .speed: bin = 10
                case .potency: bin = 5
                default: bin = 1
                }
                
                let v = (stat.v + bin / 2) / bin * bin
                
                histograms[index].add(v)
                bins.append(bin)
            }
        }
        
        return zip3(characteristics, bins, histograms)
            .map { AttributeHistogram(characteristic: $0.0, binSize: $0.1, histogram: $0.2) }
    }
    
    private static func buildClusters(modHistograms: [ModHistogram], clusterSize: Int = 150, seedClusters: ModRecommendation? = nil) -> [ModHistogram] {
        var clusters = [ModHistogram]()
        var clustersScore = 0
        
        // if we have old clusters we can use those as seed values
        var seeds = seedClusters?.clusters.map { $0.cluster } ?? []
        
        // prefer large clusters over small, move coverage over less
        func scoreClusters(_ clusters: [ModHistogram]) -> Int {
            let totalCount = clusters.reduce(0) { $0 + $1.count }
            let threshold = Int(Double(totalCount) * 0.10)
            
            return clusters.filter { $0.count > threshold }.enumerated().reduce(0) { $0 + $1.1.count * $1.1.count / (1 + $1.0) }
        }
        
        // try a few times (we may end early) to find some good large clusters
        for i in 0 ..< 100 {
            
            // keep a few seeds around
            seeds.sort(by: { $0.count > $1.count })
            while seeds.count > 3 {
                seeds.removeLast()
            }
                        
            let candidate = kMeans(seeds: seeds, samples: modHistograms, clusterSize: clusterSize, maxIterations: 10).sorted(by: { $0.count > $1.count })
            
            let score = scoreClusters(candidate)
            if clusters.isEmpty && !candidate.isEmpty {
                // first cluster, take it
                clusters = candidate
                clustersScore = score
                seeds.append(clusters[0])
                
            } else if score > clustersScore {
                // looks like a better cluster
                
                let actualCount = modHistograms.filter { candidate[0].contains(other: $0, clusterSize: clusterSize)}.count
                if actualCount == 0 {
                    // it turns out it didn't have anything in it -- kMeans has methods to
                    // prevent this, but if it sneaks through, ignore it.  reset the seeds
                    // to try and kick things loose
                    seeds.removeAll()
                    
                } else {
                    // a better cluster, take it and take the top entry for the seeds
                    
                    // print("improve: \(i) - \(candidate[0].count) score: \(score) \(candidate.map { $0.count.description }.joined(separator: ","))")
                    // print(candidate[0].average.normalized.priorities(threshold: 35).map { $0.c.description }.joined(separator: ", "))
                    
                    clusters = candidate
                    clustersScore = score
                    seeds.append(clusters[0])
                }
                
            } else {
                // didn't produce a better set of clusters, keep looking
                // print("fail: \(i) - \(candidate[0].count) score: \(score)")
            }
            
            // if we have good looking clusters, try to stop early
            var wantsToStop = false
            if !clusters.isEmpty {
                if i > 10 && clusters[0].count > modHistograms.count / 2 {
                    wantsToStop = true
                }
                if i > 20 && clusters[0].count > modHistograms.count / 3 {
                    wantsToStop = true
                }
            }

            if wantsToStop {
                // check to see the real quality of the cluster
                let actualCount = modHistograms.filter { clusters[0].contains(other: $0, clusterSize: clusterSize)}.count
                
                if actualCount > clusters[0].count / 3 {
                    // found enough, go for it
                    break
                } else {
                    seeds.removeAll()
                }
            }
        }
        
        return clusters
    }
    
    public static func build(players: [Model.Player], id: String, clusterSize: Int = 150, threshold: Double = 0.0, seedClusters: ModRecommendation? = nil) -> ModRecommendation {
        
        struct PlayerUnit : Hashable {
            let allyCode: Int
            let mods: [Model.Mod]
            let histogram: ModHistogram
            
            init?(player: Model.Player, id: String) {
                if let u = player.units[id], u.gear > 10 && u.mods.count == 6 {
                    self.allyCode = player.stats.allyCode
                    self.mods = u.mods
                    self.histogram = ModHistogram(mods: self.mods).normalized
                } else {
                    return nil
                }
            }
            
            func example(priorities: [ModHistogram.Priority], ideal: StatsUnit) -> Cluster.Example {
                Cluster.Example(allyCode: allyCode, mods: mods, priorities: priorities, ideal: ideal)
            }
        }

        let playerUnits = players.compactMap { PlayerUnit(player: $0, id: id) }
        
        if playerUnits.isEmpty {
            return ModRecommendation(clusters: [], totalCount: 0, attributeHistograms: [])
        }
        
        var result = [Cluster]()
        let idealUnit = IdealUnit(id: id)

        // build the clusters based on normalized mods
        let modHistograms = playerUnits.map { $0.histogram }
        let clusters = buildClusters(modHistograms: modHistograms, clusterSize: clusterSize, seedClusters: seedClusters)
        
        let totalCount = clusters.reduce(0, { $0 + $1.count })
        let cutThreshold = Int(round(Double(totalCount) * threshold))
                
        for cluster in clusters.filter({ $0.count >= cutThreshold }).sorted(by: { $0.count > $1.count }) {
            let center = cluster.average
                        
            // find the set of entries that are in the cluster
            var entriesInCluster = Set(playerUnits.filter { cluster.contains(other: $0.histogram, clusterSize: clusterSize) })
            
            if entriesInCluster.isEmpty {
                // try again with a relaxed fit
                entriesInCluster = Set(playerUnits.filter { cluster.contains(other: $0.histogram, clusterSize: 2 * clusterSize) })
                
                if entriesInCluster.isEmpty {
                    // still empty, bail
                    continue
                }
            }
            
            // compute the histograms for the different stats
            let priorities = center.normalized.priorities(threshold: 35)
            let attributeHistograms = buildAttributeHistograms(unit: idealUnit, characteristics: priorities.map { $0.c }, mods: entriesInCluster.map { $0.mods })
            
            // find the center entry
            let centerEntry = entriesInCluster
                // distance from center
                .map { ($0.histogram.distance(from: center), $0) }
                // smallest distance
                .min { $0.0 < $1.0 }
                // extract the entry
                .map { $0.1 }!
                        
            // find the maximal set of mods
            let maximalEntry = entriesInCluster.max { lhs, rhs in
                // should return true if lhs < rhs
                isBetter(unit: idealUnit, priorities: priorities, mods: lhs.mods, proposed: rhs.mods) != nil
            }!

            let value = Cluster(count: cluster.count, cluster: cluster, centroid: center, centroidNormalized: center.normalized, priorities: priorities, attributeHistograms: attributeHistograms, center: centerEntry.example(priorities: priorities, ideal: idealUnit), maximal: maximalEntry.example(priorities: priorities, ideal: idealUnit))
            result.append(value)
        }
        
        // the oveall histogram
        let characteristics: [Model.Mod.Characteristic] = [ .speed, .health, .protection, .offense, .defense, .cc]
        let attributeHistograms = buildAttributeHistograms(unit: idealUnit, characteristics: characteristics, mods: playerUnits.map { $0.mods })
        
        return ModRecommendation(clusters: result, totalCount: totalCount, attributeHistograms: attributeHistograms)
    }
}

public var referenceRecommendations: [String:ModRecommendation] = {
    let url = Bundle(for: ModelClass.self).url(forResource: "mods", withExtension: "json")!
    let data = try! Data(contentsOf: url)
    return try! JSONDecoder().decode([String:ModRecommendation].self, from: data)
}()
