//
//  Copyright © 2020 David Koski. All rights reserved.
//

public struct Histogram : Hashable {
    public static let TARGET_BUCKETS = 10
    
    public let name: String?
    
    public let buckets: [Int]
    
    /// number of data points represented by the histogram
    public let dataCount: Int
    
    /// number of buckets
    public let count: Int
    
    /// start value of the 0th bucket
    public let start: Int
    
    /// width of each of the buckets
    public let bucketSize: Int
    
    /// maximum count in any of the buckets
    public let maxCount: Int
    
    public let markers: [String:Int]?
    
    static func niceNumber(_ value: Int, up: Bool = false) -> Int {
        let decimator: Int
        switch value {
        case 0 ..< 800:
            decimator = 1
        case 800 ..< 8000:
            decimator = 10
        case 8000 ..< 80_000:
            decimator = 100
        case 80_000 ..< 800_000:
            decimator = 1000
        case 8_000_000 ..< 80_000_000:
            decimator = 10_000
        case 80_000_000 ..< 800_000_000:
            decimator = 100_000
        case 800_000_00 ..< 8_000_000_000:
            decimator = 1_000_000
        default:
            decimator = 1_000_000
        }
        if up {
            if value % decimator == 0 {
                return value / decimator * decimator
            } else {
                return (value / decimator + 1) * decimator
            }
        } else {
            return value / decimator * decimator
        }
    }
    
    public static func analyze(data: [[Int]], targetBuckets: Int = TARGET_BUCKETS) -> (start: Int, bucketSize: Int, count: Int) {
        if data.reduce(0, { $0 + $1.count }) == 0 {
            return (0, 100, 10)
        }
        
        let min = data.reduce(Int.max, { Swift.min($0, $1.min() ?? Int.max) })
        let max = data.reduce(Int.min, { Swift.max($0, $1.max() ?? Int.min) })
        
        let niceMin = niceNumber(min)
        let niceMax = niceNumber(max, up: true)
        
        let range = niceMax - niceMin
        let bucketSize = niceNumber(range / targetBuckets)
        let count = Int(ceil(Double(range) / Double(bucketSize)))
                
        return (niceMin, bucketSize, count)
    }
    
    public init(name: String? = nil, data: [Int], start: Int, bucketSize: Int, count: Int, markers: [String:Int]? = nil) {
        self.name = name
        
        var buckets = [Int](repeating: 0, count: count)
        for i in data {
            let index = (i - start) / bucketSize
            if index < buckets.count {
                buckets[index] += 1
            } else {
                buckets[buckets.count - 1] += 1
            }
        }
        
        self.buckets = buckets
        self.count = count
        self.maxCount = buckets.reduce(0, max)
        self.dataCount = data.count
        self.start = start
        self.bucketSize = bucketSize
        self.markers = markers
    }
    
    public init(cumulative other: Histogram) {
        self.name = other.name
        var buckets = [Int](repeating: 0, count: other.count)
        var sum = 0
        for (index, value) in other.buckets.enumerated() {
            sum = sum + value
            buckets[index] = sum
        }
        
        self.buckets = buckets
        self.count = other.count
        self.maxCount = sum
        self.dataCount = other.dataCount
        self.start = other.start
        self.bucketSize = other.bucketSize
        self.markers = other.markers
    }
}
