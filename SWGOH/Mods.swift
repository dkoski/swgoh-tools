//
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation

// see https://wiki.swgoh.help/wiki/Mods and https://swgoh-stat-calc.glitch.me/lang/eng_us.json
private let primaryStats = Set([5, 16, 17, 18, 48, 49, 52, 53, 54, 55, 56])
private let secondaryStats = Set([1, 5, 17, 18, 28, 41, 42, 48, 49, 53, 55, 56])
private let primaryStatsBySlot: [Int:Set<Int>] = [
    1: [48],
    2: [5, 48, 49, 52, 54, 55, 56],
    3: [49],
    4: [16, 48, 49, 53, 55, 56],
    5: [55, 56],
    6: [17, 18, 48, 49, 55, 56],
]

/// stat value of the primary at level 15 for a 5 and 6 pip mod
private let primaryValue: [Int:(Double, Double)] = [
    5:  (30,    32),  // speed
    16: (36,    42),  // cd
    17: (24,    30),  // potency
    18: (24,    35),  // tenacity
    48: (5.88,  8),   // offense
    49: (11.75, 20),   // defense
    52: (12,    30),   // accuracy
    53: (12,    20),  // cc
    54: (24,    35),  // ca
    55: (5.88,  16),  // health %
    56: (23.5,  24),  // prot %
]

/// the range of values per roll
private let upgradeSecondary: [Int:(Double, Double)] = [
    1:  (214    , 428    ), // health flat
    5:  (3      , 6      ), // speed
    17: (1.125  , 2.250  ), // potency
    18: (1.125  , 2.250  ), // tenacity
    28: (415    , 830    ), // protection flat
    41: (23     , 46     ), // offense flat
    42: (4      , 10     ), // defense flat
    48: (0.281  , 0.563  ), // offense %
    49: (0.850  , 1.700  ), // defense %
    53: (1.125  , 2.250  ), // cc
    55: (0.563  , 1.250  ), // health %
    56: (1.250  , 2.250  ), // prot %
]

/// multiplier for secondary value going from 5 -> 6
private let upgradeSecondary6: [Int:Double] = [
    1:  1.26, // health flat
    5:  1.03, // speed
    17: 1.33, // potency
    18: 1.33, // tenacity
    28: 1.11, // protection flat
    41: 1.10, // offense flat
    42: 1.63, // defense flat
    48: 3.02, // offense %
    49: 2.34, // defense %
    53: 1.04, // cc
    55: 1.86, // health %
    56: 1.33, // prot %
]

private let extraRolls: [Int:Int] = [
    1: 4,   // tier A, 4 extra rolls
    2: 3,
    3: 2,
    4: 1,
    5: 0,
]

private func rollSecondary(_ stat: Int) -> Double {
    let range = upgradeSecondary[stat]!
    switch stat {
    case 5, 28, 41, 42:
        return Double(Int.random(in: Int(range.0) ... Int(range.1)))
    default:
        return Double.random(in: range.0 ... range.1)
    }
}

/// add secondary stats up to the max
private func addSecondaries(primaryStat: Int, secondaries: inout [Model.Mod.Stat]) {
    // secondaries will not have the same stat as the primary, e.g. a health % primary won't have a health % secondary
    var used = Set(secondaries.map { $0.unitStat })
    used.insert(primaryStat)
    
    for _ in 0 ..< 4 - secondaries.count {
        var secondary = secondaryStats.randomElement()!
        
        while used.contains(secondary) {
            secondary = secondaryStats.randomElement()!

        }
        used.insert(secondary)
        
        secondaries.append(Model.Mod.Stat(unitStat: secondary, value: rollSecondary(secondary), roll: 1))
    }
}

private func upgradeSecondaries(tier: Int, secondaries: inout [Model.Mod.Stat]) {
    // figure out how many extra rolls we already have
    let currentRolls = secondaries.reduce(0) { $0 + ($1.roll! - 1) }

    for _ in 0 ..< extraRolls[tier]! - currentRolls {
        let index = Int.random(in: 0 ..< 4)
        secondaries[index].roll! += 1
        secondaries[index].value += rollSecondary(secondaries[index].unitStat)
    }
}


private let sets = 1 ... 8
private let tiers = 1 ... 5
private let slots = 1 ... 6

/// generate a random mod
public func generateMod(set: Int? = nil, tier: Int = 1, slot: Int? = nil, primary: Int? = nil) -> Model.Mod {
    let set = set ?? sets.randomElement()!
    let slot = slot ?? slots.randomElement()!
    let primary = primary ?? primaryStatsBySlot[slot]!.randomElement()!
    
    let primaryStat = Model.Mod.Stat(unitStat: primary, value: primaryValue[primary]!.0, roll: nil)
    var secondaries = [Model.Mod.Stat]()
    addSecondaries(primaryStat: primaryStat.unitStat, secondaries: &secondaries)
    upgradeSecondaries(tier: tier, secondaries: &secondaries)
    
    return Model.Mod(pips: 5, set: set, tier: tier, slot: slot, level: 15, primaryStat: primaryStat, secondaryStat: secondaries)
}

/// upgrade a mod to level 15
public func upgradeMod(mod: Model.Mod) -> Model.Mod {
    var newMod = mod

    newMod.level = 15

    addSecondaries(primaryStat: mod.primaryStat.unitStat, secondaries: &newMod.secondaryStat)
    upgradeSecondaries(tier: mod.tier, secondaries: &newMod.secondaryStat)
    
    return newMod
}

/// promote a mod to the next tier
public func promoteMod(mod: Model.Mod) -> Model.Mod {
    if mod.pips == 5 {
        var newMod = mod
        if mod.tier == 1 {
            // 5a -> 6e promotion
            newMod.pips = 6
            newMod.tier = 5
            newMod.primaryStat.value = primaryValue[newMod.primaryStat.unitStat]!.1
            
            for index in 0 ..< newMod.secondaryStat.count {
                newMod.secondaryStat[index].value *= upgradeSecondary6[newMod.secondaryStat[index].unitStat]!
            }
        } else {
            newMod.tier -= 1
            upgradeSecondaries(tier: newMod.tier, secondaries: &newMod.secondaryStat)
        }
        
        return newMod
    } else {
        // not promotable
        return mod
    }
}
