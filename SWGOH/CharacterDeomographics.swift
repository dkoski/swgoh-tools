//
//  CharacterDeomographics.swift
//  swgoh
//
//  Created by David Koski on 9/10/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

public struct CharacterDeomographics {
    public let id: String
    let viable: Double
    public let zetas: [String:Double]
    let g13: Double
    let r5: Double
    
    public var importance: Int {
        let value = Int(r5 * 100 + g13 * 10 + viable)
        if let override = CharacterDeomographics.importantOverrides[id], override > value {
            return override
        }
        return value
    }
    
    public var important: Bool {
        return r5 > 5 || g13 > 35 || viable > 50 || (CharacterDeomographics.importantOverrides[id, default: 0] > 500)
    }
    
    public var unimportant: Bool {
        return (viable < 10 || g13 < 10) && (CharacterDeomographics.importantOverrides[id, default: 0] < 500)
    }

    public var character: Model.Character {
        return Model.Character.find(id)
    }
    
    static let importantOverrides = { () -> [String:Int] in
        var result = [String:Int]()
        
        for (name, score) in CharacterDeomographics.importantOverridesData {
            let character = Model.Character.find(name)
            result[character.id] = score
        }
        
        return result
    }()
    
    // characters that we want to mark as important despite the numbers
    static let importantOverridesData = [
        "Nute" : 3500,
        "Shaak Ti" : 3500,
        "GBA" : 3500,
        
        "Sun Fac" : 500,
        "Geo Soldier" : 500,
        "Geo Spy" : 500,
        "Poggle" : 500,
    ]
}
