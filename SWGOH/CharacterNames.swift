//
//  Data.swift
//  swgoh
//
//  Created by David Koski on 5/13/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

// MARK: - Various Mappings

let commonNames: [String:String] = [
    "Aayla Secura": "Aayla",
    "Admiral Ackbar": "Ackbar",
    "Ahsoka Tano": "Ahsoka",
    "Ahsoka Tano (Fulcrum)": "Fulcrum",
    "Amilyn Holdo": "Holdo",
    "Asajj Ventress": "AV",
    "Aurra Sing": "Aurra",
    "B1 Battle Droid": "B1",
    "B2 Super Battle Droid": "B2",
    "Barriss Offee": "Barriss",
    "Bastila Shan": "BS",
    "Bastila Shan (Fallen)": "BSF",
    "Baze Malbus": "Baze",
    "Biggs Darklighter": "Biggs",
    "Boba Fett": "Boba",
    "CC-2224 \"Cody\"": "Cody",
    "CT-21-0408 \"Echo\"": "Echo (501st)",
    "CT-5555 \"Fives\"": "Fives",
    "CT-7567 \"Rex\"": "Rex",
    "Cad Bane": "Cad",
    "Canderous Ordo": "Canderous",
    "Captain Han Solo": "Cpt Solo",
    "Captain Phasma": "Phasma",
    "Cassian Andor": "Cassian",
    "Chewbacca": "Chewbacca",
    "Chief Chirpa": "Chirpa",
    "Chief Nebit": "Nebit",
    "Chirrut Îmwe": "Chirrut",
    "Clone Sergeant - Phase I": "Clone Sergeant",
    "Clone Wars Chewbacca": "CW Chewbacca",
    "Colonel Starck": "Starck",
    "Commander Luke Skywalker": "CLS",
    "Coruscant Underworld Police": "CUP",
    "Count Dooku": "Dooku",
    "Darth Malak" : "Malak",
    "Darth Maul": "Maul",
    "Darth Nihilus": "DN",
    "Darth Revan" : "DR",
    "Darth Sidious": "Sidious",
    "Darth Sion": "Sion",
    "Darth Traya": "Traya",
    "Darth Vader": "Vader",
    "Death Trooper": "DT",
    "Director Krennic": "Krennic",
    "Emperor Palpatine": "EP",
    "Enfys Nest": "Nest",
    "Ewok Elder": "Elder",
    "Ewok Scout": "Scout",
    "Ezra Bridger": "Ezra",
    "First Order Executioner": "FOE",
    "First Order Officer": "FOO",
    "First Order SF TIE Pilot": "FOSFTP",
    "First Order Stormtrooper": "FOST",
    "First Order TIE Pilot": "FOTP",
    "Garazeb \"Zeb\" Orrelios": "Zeb",
    "General Grievous": "GG",
    "General Kenobi": "GK",
    "General Skywalker": "GAS",
    "General Veers": "Veers",
    "Geonosian Brood Alpha" : "GBA",
    "Geonosian Soldier" : "Geo Soldier",
    "Geonosian Spy" : "Geo Spy",
    "Grand Admiral Thrawn": "Thrawn",
    "Grand Master Yoda": "GMY",
    "Grand Moff Tarkin": "Tarkin",
    "Han Solo": "Han",
    "Hera Syndulla": "Hera",
    "Hermit Yoda": "HYoda",
    "IG-100 MagnaGuard": "MagnaGuard",
    "IG-86 Sentinel Droid": "IG-86",
    "Imperial Probe Droid": "IPD",
    "Jango Fett": "Jango",
    "Jedi Knight Anakin": "JKA",
    "Jedi Knight Revan": "JKR",
    "Jolee Bindo": "Jolee",
    "Jyn Erso": "Jyn",
    "Kanan Jarrus": "Kanan",
    "Kylo Ren": "Kylo",
    "Kylo Ren (Unmasked)": "KRU",
    "Lando Calrissian": "Lando",
    "Luke Skywalker (Farmboy)": "Farmboy",
    "Luminara Unduli": "Lumi",
    "Mace Windu": "Mace",
    "Magmatrooper": "Magma",
    "Mission Vao": "Mission",
    "Mother Talzin": "MT",
    "The Negotiator" : "Negotiator",
    "Nightsister Acolyte": "Acolyte",
    "Nightsister Initiate": "Initiate",
    "Nightsister Spirit": "Spirit",
    "Nightsister Zombie": "Zombie",
    "Nute Gunray": "Nute",
    "Obi-Wan Kenobi (Old Ben)": "Old Ben",
    "Old Daka": "Daka",
    "Padmé Amidala": "Padme",
    "Plo Koon": "Plo",
    "Poe Dameron": "Poe",
    "Poggle the Lesser": "Poggle",
    "Princess Leia": "Leia",
    "Qi'ra": "Qira",
    "Qui-Gon Jinn": "QGJ",
    "Rebel Officer Leia Organa": "ROLO",
    "Resistance Trooper": "RT",
    "Rey (Jedi Training)": "JTR",
    "Rey (Scavenger)": "Scav Rey",
    "Rose Tico": "Rose",
    "Royal Guard": "RG",
    "Sabine Wren": "Sabine",
    "Savage Opress": "Savage",
    "Scarif Rebel Pathfinder": "Scarif",
    "TIE Fighter Pilot": "TFP",
    "Vandor Chewbacca": "Vandor Chewbacca",
    "Veteran Smuggler Chewbacca": "Vet Chewbacca",
    "Veteran Smuggler Han Solo": "Vet Han",
    "Visas Marr": "Visas",
    "Wedge Antilles": "Wedge",
    "Young Han Solo": "Young Han",
    "Young Lando Calrissian": "Young Lando",
    "Zam Wesell": "Zam",
    "Supreme Leader Kylo Ren" : "SL Kylo",
    "Jedi Master Kenobi" : "JMK",
    
    "Han's Millennium Falcon": "Han's MF",
    "Hound's Tooth": "HT",
    "Phantom II": "Phantom",
    "TIE Advanced x1": "Tie Adv",
    "Wedge Antilles's X-wing": "Wedge X-wing",
    "Biggs Darklighter's X-wing": "Biggs X-wing",
    "Geonosian Soldier's Starfighter": "Soldier SF",
    "Geonosian Spy's Starfighter": "Spy SF",
    "Sun Fac's Geonosian Starfighter": "Sun Fac SF",
    "General Hux" : "Hux",
    
    "Jedi Knight Luke Skywalker" : "JKL",
    "Sith Eternal Emperor": "SEE",
    "Jedi Master Luke Skywalker": "JMLS",
    "Threepio & Chewie": "Chewpio",
]

// MARK: - Utilities

/// Cache of names, e.g. u("CLS") -> Identifier
struct NameCache {
    
    static var nameCache = MemoizeCache<String, String>()
    
    @inlinable
    static func get(_ string: String) -> String? {
        nameCache[string]
    }
    
    static func set(_ string: String, _ value: String) {
        nameCache[string] = value
    }
    
}


