//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

public protocol UnitProvider {

    /// Provide the Player's Unit or an empty Unit if not owned
    subscript(_ id: String) -> Model.Unit { get }
    
}

public protocol UnitSelect {
    
    /// Select one or more Units from the UnitProvider
    func select(units: UnitProvider) -> [Model.Unit]
    
    /// Note: these are not necessarily canonical character ids -- this is just used for rule conversion
    var ids: [String] { get }
    
}

public func u(_ id: String) -> UnitSelect {
    return SingleUnitSelect(id)
}

public func u(_ ids: [String]) -> UnitSelect {
    return OneOfUnitSelect(ids)
}

public func u(_ ids: String...) -> UnitSelect {
    return OneOfUnitSelect(ids)
}

struct SingleUnitSelect : UnitSelect {
    let id: String
    
    init(_ id: String) {
        self.id = id
    }
    
    var ids: [String] { return [id] }
    
    func select(units: UnitProvider) -> [Model.Unit] {
        let unit = units[id]
        return unit.stars == 0 ? [] : [unit]
    }
    
    static func build(_ ids: String...) -> [UnitSelect] {
        var result = [UnitSelect]()
        for id in ids {
            result.append(u(id))
        }
        return result
    }
}

struct OneOfUnitSelect : UnitSelect {
    
    let ids: [String]
    
    init(_ ids: String...) {
        self.ids = ids
    }
    
    init(_ ids: [String]) {
        self.ids = ids
    }
    
    func select(units: UnitProvider) -> [Model.Unit] {
        var result = [Model.Unit]()
        for id in ids {
            let unit = units[id]
            if unit.stars != 0 {
                result.append(units[id])
            }
        }
        return result
    }
}

/// provides some low end filtering of units before presenting them
struct FilteredOneOfUnitSelect : UnitSelect {
    
    let ids: [String]
    
    init(_ ids: String...) {
        self.ids = ids
    }
    
    init(_ ids: [String]) {
        self.ids = ids
    }
    
    func select(units: UnitProvider) -> [Model.Unit] {
        var result = [Model.Unit]()
        for id in ids {
            let unit = units[id]
            if unit.stars < 7 || unit.gear < 10 {
                continue
            }
            result.append(unit)
        }
        return result
    }
}

public protocol SquadBuilder {
    
    var name: String { get }
    func generateSquads(units: UnitProvider) -> [Squad]
    
    func asSquadRules() -> [SquadRule]
}

public struct MultipleSquadBuilder : SquadBuilder {
    
    public let squadBuilders: [SquadBuilder]
    
    public init(_ squadBuilders: SquadBuilder...) {
        self.squadBuilders = squadBuilders
    }
    
    public init(minimumGP: Int, _ squadBuilders: SelectUnitSquadBuilder...) {
        self.squadBuilders = squadBuilders.map { $0.with(minimumGP: minimumGP) }
    }

    
    public init(_ squadBuilders: [SquadBuilder]) {
        self.squadBuilders = squadBuilders
    }
    
    public var name: String {
        Set(squadBuilders.map { $0.name }).sorted().joined(separator: ", ")
    }

    public func generateSquads(units: UnitProvider) -> [Squad] {
        var result = Set<Squad>()
        
        for squadBuilder in squadBuilders {
            let squads = squadBuilder.generateSquads(units: units)
            result.formUnion(squads)
        }
        
        return Array(result)
    }
    
    public func asSquadRules() -> [SquadRule] {
        squadBuilders.flatMap { $0.asSquadRules() }
    }

}

public struct SelectUnitSquadBuilder : SquadBuilder {

    let properties: Squad.Properties
    let unitSelectors: [UnitSelect]
    
    public init(_ name: String, _ preference: Int, _ rules: [String : UnitEvaluator], _ unitSelectors: UnitSelect...) {
        self.properties = Squad.Properties(name: name, preference: preference, rules: rules)
        self.unitSelectors = unitSelectors
    }
    
    public init(properties: Squad.Properties, _ unitSelectors: UnitSelect...) {
        self.properties = properties
        self.unitSelectors = unitSelectors
    }
    
    public init(properties: Squad.Properties, unitSelectors: [UnitSelect]) {
        self.properties = properties
        self.unitSelectors = unitSelectors
    }
    
    public func with(minimumGP: Int) -> SelectUnitSquadBuilder {
        var properties = self.properties
        properties.unitMinimumGP = minimumGP
        return SelectUnitSquadBuilder(properties: properties, unitSelectors: self.unitSelectors)
    }
    
    public var name: String { properties.name }

    public func generateSquads(units: UnitProvider) -> [Squad] {
        var result = Set<Squad>()
        enumerateUnits(availableUnits: units, prefixSquad: [], remainingUnitSelectors: unitSelectors, result: &result)
        return Array(result)
    }
    
    private func enumerateUnits(availableUnits: UnitProvider, prefixSquad: [Model.Unit], remainingUnitSelectors: [UnitSelect], result: inout Set<Squad>) {
        var unitSelectors = remainingUnitSelectors
        let currentUnitSelector = unitSelectors.removeFirst()
        
        for unit in currentUnitSelector.select(units: availableUnits) {
            if unit.gp < properties.unitMinimumGP {
                continue
            }
            
            if prefixSquad.contains(unit) {
                // no repeats
                continue
            }
            
            var currentSquad = prefixSquad
            currentSquad.append(unit)
            
            if unitSelectors.isEmpty {
                result.insert(Squad(properties, units: currentSquad))
            } else {
                enumerateUnits(availableUnits: availableUnits, prefixSquad: currentSquad, remainingUnitSelectors: unitSelectors, result: &result)
            }
        }
    }
    
    func asSquadRule() -> SquadRule {
        let isFleet = Model.Character.find(unitSelectors[0].ids[0]).ship
        let name = TW.canonicalName(self.name)
        var rule = SquadRule(kind: isFleet ? .fleet : .squad, squadName: name)
        
        if name != self.name {
            // e.g. name is NS, variant is NestSisters
            rule.name = self.name
        }
        
        if TW.keySquads.contains(name) {
            rule.tier = 1
        }

        var allCharacterIds = Set<String>()
        for (index, s) in unitSelectors.enumerated() {
            let unitIds = s.ids.map { Model.Character.find($0).id }
            rule.possibleCharacterIds[index] = Set(unitIds)
            allCharacterIds.formUnion(unitIds)
        }
        
        rule.preference = properties.preference
        rule.attackerId = properties.attackerId ?? ""
        rule.defenderId = properties.defenderId ?? ""
        
        if let id = properties.defenderId {
            let counter = CounterTeam.find(id)
            rule.defense = counter.type != .offense
        }

        for characterId in allCharacterIds {
            let ev = properties.rules[characterId] ?? properties.rules["DEFAULT"]!
            var unitRule = ev.asUnitRule(id: characterId)
            unitRule.unitMinimumGP = properties.unitMinimumGP
            rule.rules[characterId] = unitRule
        }

        return rule
    }
    
    public func asSquadRules() -> [SquadRule] {
        return [asSquadRule()]
    }

}

public func evaluatePlayers(players: [Model.Player], squadBuilder: SquadBuilder, squadEvaluator: SquadEvaluator = SquadEvaluator(rules: [:])) -> [Model.Player:[SquadEvaluation]] {
    var result = [Model.Player:[SquadEvaluation]]()
    
    var evaluationFilter: Filter = PreferredViabilitySortFilter()
    evaluationFilter = evaluationFilter.compose(SquadListFilter())
    
    let queue = DispatchQueue(label: "build")
    func update(player: Model.Player, evaluations: [SquadEvaluation]) {
        queue.sync {
            result[player] = evaluations
        }
    }
    
    DispatchQueue.concurrentPerform(iterations: players.count) {
        let player = players[$0]
        
        let squads = squadBuilder.generateSquads(units: player)
        
        let evaluations = squadEvaluator.evaluate(squads: squads)
        let finalEvaluations = evaluationFilter.filter(evaluations)
        
        update(player: player, evaluations: finalEvaluations)
    }
    
    return result
}
