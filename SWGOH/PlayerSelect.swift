//
//  Copyright © 2019 David Koski. All rights reserved.
//

// TODO delete?

public protocol PlayerSelect {
    
    func matches(player: Model.Player) -> Bool
    
}

public struct PlayerSelectAll : PlayerSelect {
    
    public init() {        
    }
    
    public func matches(player: Model.Player) -> Bool {
        return true
    }
    
}

public struct PlayersSelectMatch : PlayerSelect {
    
    let names: [String]
    let invert: Bool
    
    public init(_ names: [String], invert: Bool = false) {
        self.names = names.map { $0.lowercased() }
        self.invert = invert
    }
    
    public func matches(player: Model.Player) -> Bool {
        let match = player.name.lowercased()
        
        return names.contains { match.contains($0) } != invert
    }
    
}

public struct PlayerSelectCharacter : PlayerSelect {
    
    let match: String
    
    public init(_ match: String) {
        self.match = match
    }

    public func matches(player: Model.Player) -> Bool {
        return player.units[match] != nil
    }
    
}
