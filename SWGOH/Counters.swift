//
//  Counters.swift
//  swgoh
//
//  Created by David Koski on 8/18/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

public enum CounterType {
    case defense
    case offense
    case both
    
    public var symbol: String {
        switch self {
        case .defense: return "<img src=\"../../i/shield24.png\"/>"
        case .offense: return "<img src=\"../../i/ls24.png\"/>"
        case .both: return "<img src=\"../../i/ls24.png\"/><img src=\"../../i/shield24.png\"/>"
        }
    }
    
    public var isDefense: Bool {
        switch self {
        case .defense, .both:
            return true
        case .offense:
            return false
        }
    }
    
    public var isOffense: Bool {
        switch self {
        case .offense, .both:
            return true
        case .defense:
            return false
        }
    }
}

/// more counters that I find effective
private let additionalCounters = [
    Counter(team: "GALACTIC_REPUBLIC", counter: "GALACTIC_REPUBLIC", hard: true, description: #""#, video: #""#),
    Counter(team: "GALACTIC_REPUBLIC", counter: "GALACTIC_REPUBLIC_C3PO", hard: true, description: #""#, video: #""#),
    Counter(team: "NS_MT", counter: "JEDI_REVAN", hard: true, description: #""#, video: #""#),
    Counter(team: "NS_ASAJJ", counter: "JEDI_REVAN", hard: true, description: #""#, video: #""#),
    Counter(team: "NS_MT_NA", counter: "JEDI_REVAN", hard: true, description: #""#, video: #""#),
]

// counters based on
//
// - https://bobbybaxter.github.io/swgoh-counters/
// - https://github.com/bobbybaxter/swgoh-counters
// - https://docs.google.com/spreadsheets/d/1RVo7ej1PE06FKkwS1q5_slB9YLLQX3EF-dN98MkFmOM/edit#gid=1364839479
//
// See counters.py
//
public struct CounterTeam : Equatable {
    
    public let id: String
    public let name: String
    public let type: CounterType
    public let squad: [Model.Character]
    public let zetas: [Bool]
    public let subs: String
    public let description: String
    public let strategy: String
    
    init(id: String, name: String, type: CounterType, squad: [String], zetas: [Bool], subs: String, description: String, strategy: String) {
        self.id = id
        self.name = name
        self.type = type
        self.squad = squad.compactMap { $0.isEmpty ? nil : Model.Character.find($0) }
        self.zetas = zetas
        self.subs = subs
        self.description = description
        self.strategy = strategy
    }
    
    public static func find(_ id: String) -> CounterTeam {
        return counterTeams[id]!
    }
    
    public func counters() -> [Counter] {
        return Counter.counters[id, default: []]
    }

    public func reverseCounters() -> [Counter] {
        return Counter.reverseCounters[id, default: []]
    }
}

public struct Counter : Comparable {
    
    /// the defending team
    public let team: CounterTeam
    
    /// the attacking team
    public let counter: CounterTeam
    
    /// hard counters are better at beating the team than a soft counter
    public let hard: Bool
    
    /// how the `counter` can beat the `team`
    public let description: String
    
    public let video: String?
    
    public var id: String { "\(team.id)-\(counter.id)" }
    
    init(team: String, counter: String, hard: Bool, description: String, video: String) {
        self.team = CounterTeam.find(team)
        self.counter = CounterTeam.find(counter)
        self.hard = hard
        self.description = description
        self.video = video.isEmpty ? nil : video
    }
    
    public static let combinedCounters: [Counter] = {
        squad5v5
        + additionalCounters
        // for now we don't want them mixed in
        // + squad3v3
    }()
    
    /// a list of Counters (teams that can beat a team) for a given CounterTeam.id
    public static let counters: [String:[Counter]] = {
        var result = [String:[Counter]]()
        for counter in combinedCounters {
            result[counter.team.id, default:[]].append(counter)
        }
        
        // per https://bobbybaxter.github.io/swgoh-counters/
        // Note: Darth Revan (with or without Malak) is a hard counter unless it is listed as a soft counter
        let dr1 = "SITH_EMPIRE_W_MALAK"
        let dr2 = "SITH_EMPIRE_WO_MALAK"
        for (id, list) in result {
            if !list.contains(where: { $0.counter.id == dr1 || $0.counter.id == dr2 }) {
                result[id, default: []].append(Counter(team: id, counter: "SITH_EMPIRE_WO_MALAK", hard: true, description: "", video: ""))
            }
        }
        
        result = result.mapValues({ $0.sorted() })

        return result
    }()
    
    /// a list of teams that a given CounterTeam.id counters (can beat).  look at the team
    public static let reverseCounters: [String:[Counter]] = {
        var result = [String:[Counter]]()
        for counters in Counter.counters.values {
            for counter in counters {
                result[counter.counter.id, default:[]].append(counter)
            }
        }
                
        result = result.mapValues({ $0.sorted() })

        return result
    }()
    
    public static let counterableIds: [String] = {
        return Set(combinedCounters.map { $0.team.id }).sorted()
    }()

    public static func < (lhs: Counter, rhs: Counter) -> Bool {
        if !lhs.hard && rhs.hard {
            return false
        }
        if lhs.hard && !rhs.hard {
            return true
        }

        return lhs.team.name > rhs.team.name
    }

}
