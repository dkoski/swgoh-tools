//
//  TWScheduler.swift
//  swgoh
//
//  Created by David Koski on 5/9/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

public func defaultTWScheduler() -> TWSectionScheduler {
    return RandomScheduler3()
}

public struct TWSectionSchedulerResult {
    
    public let placedSquads: [(Model.Player, SquadEvaluation, String)]
    
    public let remainder: [(Model.Player, SquadEvaluation)]
    
    private static func computeRemainder(placedSquads: [(Model.Player, SquadEvaluation, String)], all: [Model.Player : [SquadEvaluation]]) -> [(Model.Player, SquadEvaluation)] {
        struct Tuple : Hashable {
            let player: Model.Player
            let ev: SquadEvaluation
            
            func hash(into hasher: inout Hasher) {
                hasher.combine(player)
                hasher.combine(ev.squad)
            }
        }
        
        let placed = Set<Tuple>(placedSquads.filter { $0.1.squad.units.count > 0 }.map { Tuple(player: $0.0, ev: $0.1) })
        var remainder = [(Model.Player, SquadEvaluation)]()
        for (player, evs) in all {
            for ev in evs where ev.status.isViable {
                let probe = Tuple(player: player, ev: ev)
                if !placed.contains(probe) {
                    remainder.append((player, ev))
                }
            }
        }
        return remainder
    }
    
    private static func computeCounters(placedSquads: [(Model.Player, SquadEvaluation, String)]) -> [(Model.Player, SquadEvaluation)] {
        // build [Player:[SquadEval]]
        let placed = Dictionary(uniqueKeysWithValues: Dictionary(grouping: placedSquads) { $0.0 }.map { ($0.key, $0.value.map {$0.1})})

        var result = [(Model.Player, SquadEvaluation)]()
        
        let squadBuilder = MultipleSquadBuilder(TW.configurations["counters"]!)
        let available = SquadListFilter()
        let viable = ViableSquadFilter()
        let duplicates = UniqueSquadFilter()
        
        for (player, placed) in placed {
            let squads = squadBuilder.generateSquads(units: player)
            let evaluations = duplicates.filter(viable.filter(
                SquadEvaluator(rules: [:])
                    .evaluate(squads: squads)
                    .sorted(by: preferenceSort)))
            let counters = available.filter(placed: placed, counters: evaluations)
            
            for ev in counters {
                result.append((player, ev))
            }
        }
        
        return result
    }
    
    public init(placedSquads: [(Model.Player, SquadEvaluation, String)], all: [Model.Player : [SquadEvaluation]]) {
        self.placedSquads = placedSquads
        
        var remainder = TWSectionSchedulerResult.computeRemainder(placedSquads: placedSquads, all: all)
        remainder.append(contentsOf: TWSectionSchedulerResult.computeCounters(placedSquads: placedSquads))
        self.remainder = remainder
    }
}


public protocol TWSectionScheduler {
    
    func schedule(data: [Model.Player : [SquadEvaluation]], config: TWConfig, squadsPerSection: Int, trace: TWTrace) -> TWSectionSchedulerResult
    
}

public class TWTrace {
    
    public enum Status : Hashable {
        case unknown
        
        /// placed in its preferred spot (controlled by the count)
        case placedPreferred(String)
        
        /// placed a defensive player squad (via .placePlayerSquads)
        case placedDefense(String)

        /// random placement
        case placedRandom(String)
        
        /// An any squad is placed
        case placedAny(String)

        /// not placed because it is above the player's limit
        case skippedAboveLimit
        
        /// skipped (for now) because the preferred sections are full
        case skippedPreferredSectionsFull
        
        /// random sections are full
        case skippedRandomSectionsFull
        
        /// defensive player (via .placePlayerSquads) hit their limit
        case skippedAboveLimitDefense
        
        /// team is only considered for defense players
        case skippedDefenseOnly
        
        /// only preferred placement (no backfill)
        case skippedNoBackfill
        
        case skippedAboveGPThreshold
        case skippedBelowGPThreshold
        
        /// held back (configuration)
        case skippedHeldBack
        
        /// record the maxSquads setting
        case maxSquads(Int)
        case availableSquads(Int)
        case availableFleet(Int)
        
        public var verb: String {
            switch self {
            case .unknown: fatalError()
            case .placedPreferred(_), .placedAny(_), .placedRandom(_), .placedDefense(_): return "PLACED"
            case .availableFleet(_), .availableSquads(_), .maxSquads(_): return "INFO"
            case .skippedAboveLimit, .skippedNoBackfill, .skippedDefenseOnly, .skippedAboveGPThreshold, .skippedBelowGPThreshold, .skippedAboveLimitDefense, .skippedRandomSectionsFull, .skippedPreferredSectionsFull, .skippedHeldBack: return "SKIPPED"
            }
        }
        
        public var description : String {
            switch self {
            case .unknown: fatalError()
            case .placedPreferred(let section): return "Preferred section: \(section)"
            case .placedDefense(let section): return "Defensive player: \(section)"
            case .placedRandom(let section): return "Backfill section: \(section)"
            case .placedAny(let section): return "Fluff squad: \(section)"
            case .skippedAboveLimit: return "Hit player squad limit"
            case .skippedPreferredSectionsFull: return "Preferred section(s) full"
            case .skippedRandomSectionsFull: return "All sections full"
            case .skippedAboveLimitDefense: return "Defensive player hit squad limit."
            case .skippedDefenseOnly: return "Defensive players only."
            case .skippedNoBackfill: return "No backfill -- placed only."
            case .skippedAboveGPThreshold: return "Above GP limit."
            case .skippedBelowGPThreshold: return "Below GP limit."
            case .skippedHeldBack: return "Held back."
            case .maxSquads(let v): return "Place up to \(v) squads."
            case .availableFleet(let v): return "\(v) fleets available."
            case .availableSquads(let v): return "\(v) squads available."
            }
        }
        
        public var help: String {
            switch self {
            case .unknown:
                fatalError()
                
            case .placedPreferred(_):
                return "Enabled squad is placed in one of the preferred sections (Squads/Sections tab).  You can control weighting of sections in the Squads/Weights tab and how many are placed in the Squads/Counts tab."
            case .placedDefense(_):
                return "Defensive player squad is placed.  You can control which squads are considered for defensive players (in addition to any that are enabled) in Players/Defensive Players.  These squads will honor the preferred sections in the Squads/Sections tab, even if the squad is not enabled."
            case .placedRandom(_):
                return "Squad placed to fill remaining sections.  This is controlled via Squad/Counts, 'Place remaining squads to fill gaps'"
            case .placedAny(_):
                return "An unnamed squad is placed -- it may be helpful to select the player if looking at a section.  There were either no backfill squads (Squad/Counts, 'Place remaining squads to fill gaps') or this player didn't have any squads.  Look for squads that were SKIPPED or show Remaining in the Squads / Sections tab."
            case .skippedAboveLimit:
                return "The squad is not placed because the player has hit their placement limit.  This limit is based on their GP but you can override it in the Players section by designating them either a Defensive (place more on defense) or Offensive (don't place as many on defense) player.  You can also control the order that squads are placed by changing the order of the squads in the Squads tab."
            case .skippedPreferredSectionsFull:
                return "More squads are available, but the preferred sections are full.  You can add more sections in the Squads/Sections tab or you can allow them to back fill gaps with Squad/Counts, 'Place remaining squads to fill gaps'."
            case .skippedRandomSectionsFull:
                return "All sections are full -- no more squads placed."
            case .skippedAboveLimitDefense:
                return "Skipped: the defensive player has hit their limit.  Control this in the Players tab for Defensive Players."
            case .skippedDefenseOnly:
                return "Skipped: this squad is not enabled (Squads tab) and is only allowed for Defensive Players (Players tab)."
            case .skippedNoBackfill:
                return "Skipped: there are more squads available but they are not allowed for filling in gaps and the preferred sections are full.  Add preferred sections in Squads/Sections or allow 'Place remaining squads to fill gaps' in Squad/Counts."
            case .skippedAboveGPThreshold:
                return "Squad not considered, it is above the gp limit.  See the Squads/Ranges tab."
            case .skippedBelowGPThreshold:
                return "Squad not considered, it is below the gp limit.  See the Squads/Ranges tab."
            case .skippedHeldBack:
                return "Squad held back via configuration."
            case .maxSquads(_):
                return "The player is allowed to place up to this many squads."
            case .availableSquads(_):
                return "The player has this many squads available (includes squads that are not enabled)"
            case .availableFleet(_):
                return "The player has this many fleets available (includes fleets that are not enabled)"
            }
        }
        
        public var section: String? {
            switch self {
            case .unknown:
                fatalError()
            case .availableFleet(_), .availableSquads(_), .maxSquads(_), .skippedAboveGPThreshold, .skippedAboveLimit, .skippedAboveLimitDefense, .skippedBelowGPThreshold, .skippedDefenseOnly, .skippedNoBackfill, .skippedPreferredSectionsFull, .skippedRandomSectionsFull, .skippedHeldBack:
                return nil
            case .placedAny(let section), .placedRandom(let section), .placedDefense(let section), .placedPreferred(let section):
                return section
            }
        }
    }
    
    public struct Detail : Hashable {
        public let player: Model.Player
        public let squad: Squad?
        public let status: Status
    }
    
    public var enabled = false
    public var messages = [String]()
    
    public var details = [Detail]()
    
    public init() {
    }
    
    func append(_ string: String) {
        messages.append(string)
    }
    
    func record(_ player: Model.Player, _ squad: Squad?, status: Status) {
        details.append(Detail(player: player, squad: squad, status: status))
    }
}

public struct TeamNameScheduler : TWSectionScheduler {
    
    public init() {        
    }
    
    public func schedule(data: [Model.Player : [SquadEvaluation]], config: TWConfig, squadsPerSection: Int, trace: TWTrace = TWTrace()) -> TWSectionSchedulerResult {
        var result = [(Model.Player, SquadEvaluation, String)]()
        
        for (player, evaluations) in data {
            for evaluation in evaluations {
                let name = evaluation.squad.name
                result.append((player, evaluation, name))
            }
        }
        
        return TWSectionSchedulerResult(placedSquads: result, all: [:])
    }
    
}

/// Follow the gp thresholds, but randomly place squads for the most part.  This actually has a couple phases:
///
/// - follow the randomPlacementInstructions for specific placements
/// - randomly place the top N squads from each player where N is dependent on gp
/// - fill the remainder with generic "fluff" squads
///
/// This is done in a repeatable way by using a RandomNumberGenerator with a fixed seed and canonical
/// ordering of all the data (via sorts).
public struct RandomScheduler3 : TWSectionScheduler {
    
    public init() {
    }
    
    static var r = MersenneTwister(seed: 8)
    
    public static func resetSeed() {
        r = MersenneTwister(seed: 8)
    }
    
    // MARK: - comparisons for canonical ordering
    
    func compareSquads(_ t1: (Model.Player, SquadEvaluation), _ t2: (Model.Player, SquadEvaluation), config: TWConfig) -> Bool {
        return compareSquads(t1.1, t2.1, config: config)
    }
    
    func compareSquads(_ s1: SquadEvaluation, _ s2: SquadEvaluation, config: TWConfig) -> Bool {
        let df1 = s1.squad.gp
        let df2 = s2.squad.gp
        
        return df1 > df2
    }
    
    // MARK: - result tracking and counters
    
    struct Result {
        /// this is what we are producing
        var placements = [(Model.Player, SquadEvaluation, String)]()
        
        var placedSquadCountByPlayer = [Model.Player:Int]()
        var placedFleetCountByPlayer = [Model.Player:Int]()
        var placedSquadCountBySection = [String: Int]()
        var squadsBySection = [String: CountedSet<String>]()
        
        let config: TWConfig
        let squadsPerSection: Int
        
        init(config: TWConfig, squadsPerSection: Int) {
            self.config = config
            self.squadsPerSection = squadsPerSection
        }
        
        func maximumSquadsFor(player: Model.Player) -> TWSquadCount {
            if let override = config.playerSquadCountOverrides[player.name] ?? config.playerSquadCountOverrides[player.stats.allyCode.description] {
                return override
            }
            
            for entry in config.squadCountThresholds {
                if player.stats.gp <= entry.0 {
                    return entry.1
                }
            }
            return config.squadCountThresholds.last!.1
        }
        
        func placedSquadsFor(player: Model.Player) -> TWSquadCount {
            TWSquadCount(named: placedSquadCountByPlayer[player, default: 0], fluff: 0, fleet: placedFleetCountByPlayer[player, default: 0])
        }
        
        mutating func add(player: Model.Player, evalutation: SquadEvaluation, section: String) {
            placements.append((player, evalutation, section))
            squadsBySection[section, default: CountedSet<String>()].add(evalutation.squad.name)
            if TW.allFleetSectionsSet.contains(section) {
                placedFleetCountByPlayer[player, default: 0] += 1
            } else {
                placedSquadCountByPlayer[player, default: 0] += 1
            }
            placedSquadCountBySection[section, default: 0] += 1
        }
        
        func allSectionsExceptFleetFull() -> Bool {
            return !config.placementSections.contains { !TW.allFleetSectionsSet.contains($0) && placedSquadCountBySection[$0, default: 0] < squadsPerSection }
        }
        
        func allFleetSectionsFull() -> Bool {
            return !config.placementSections.contains { TW.allFleetSectionsSet.contains($0) && placedSquadCountBySection[$0, default: 0] < squadsPerSection }
        }
        
        func isPlayerDone(_ player: Model.Player, fleet: Bool?) -> Bool {
            let allowed = maximumSquadsFor(player: player)
            if let fleet = fleet {
                return fleet ?
                    placedFleetCountByPlayer[player, default: 0] >= allowed.fleet :
                    placedSquadCountByPlayer[player, default: 0] >= allowed.totalSquads
            } else {
                return (placedFleetCountByPlayer[player, default: 0] >= allowed.fleet) &&
                    (placedSquadCountByPlayer[player, default: 0] >= allowed.totalSquads)
            }
        }
        
        func allDone(players: [Model.Player], fleet: Bool) -> Bool {
            players.allSatisfy { isPlayerDone($0, fleet: fleet) }
        }
    }
    
    // MARK: - find a random section for a given squad
    
    func randomSection(gp: Int, isFleet: Bool, counts: [String: Int], shuffledSections: [String], squadsPerSection: Int) -> String? {
        for section in shuffledSections {
            // do not try and place a fleet in a squad area and vice versa
            if TW.allFleetSectionsSet.contains(section) != isFleet {
                continue
            }
            
            if counts[section, default: 0] >= squadsPerSection {
                continue
            }
            
            return section
        }
        
        return nil
    }
    
    func randomSection(squadName: String, gp: Int, isFleet: Bool, counts: [String: Int], potentialSections: [String], alreadyPlacedSquads: [String:CountedSet<String>], preferredSections: [String: [String]], squadsPerSection: Int) -> String? {
        // pick a preferred section first
        if let squadSections = preferredSections[squadName] {
            if let section = randomSection(gp: gp, isFleet: isFleet, counts: counts, shuffledSections: squadSections.shuffled(using: &RandomScheduler3.r), squadsPerSection: squadsPerSection) {
                return section
            }
        }

        let shuffled = potentialSections.shuffled(using: &RandomScheduler3.r)

        // then prefer a section that already has these squads
        for section in shuffled {
            if let squadCounts = alreadyPlacedSquads[section] {
                if squadCounts[squadName] > 0 {
                    if let section = randomSection(gp: gp, isFleet: isFleet, counts: counts, shuffledSections: [ section ], squadsPerSection: squadsPerSection) {
                        return section
                    }
                }
            }
        }

        // then any section
        if let section = randomSection(gp: gp, isFleet: isFleet, counts: counts, shuffledSections: shuffled, squadsPerSection: squadsPerSection) {
            return section
        }
                
        return nil
    }
    
    // MARK: - Scheduling phases
    
    func buildSquadsByPlayer(data: [Model.Player : [SquadEvaluation]], config: TWConfig) -> [Model.Player:[(Model.Player, SquadEvaluation)]] {
        var result = [Model.Player:[(Model.Player, SquadEvaluation)]]()
        for (player, evaluations) in data {
            // collect all the squads to consider.  we want a full list of them for running through randomPlacementInstructions and lists by player for the remainder of the scheduling
            let playerSquads = evaluations.filter { config.defenseTeams.contains($0.squad.name) }.sorted(by: { compareSquads($0, $1, config: config) }).map { (player, $0) }
            result[player] = playerSquads
        }
        return result
    }
    
    /// for defense players, invert the weights of the preferred sections, e.g. if T1: 4 and T2: 1, produce a new weighting of T2: 4, T1, 1
    func invertedPreferredSections(_ preferredSections: [String: [String]]) -> [String: [String]] {
        var result = [String: [String]]()
        for (squadName, sections) in preferredSections {
            let counts = CountedSet(items: sections)
            let max = counts.keys.map { counts[$0] }.max() ?? 0
            if max <= 1 {
                result[squadName] = sections
                
            } else {
                var newCounts = CountedSet<String>()
                for section in counts.keys {
                    newCounts.add(section, count: (max + 1) - counts[section])
                }
                result[squadName] = newCounts.asArray
            }
        }
        
        return result
    }
    
    func runPlacementInstructions(squadsByPlayer: inout [Model.Player:[(Model.Player, SquadEvaluation)]], into result: inout Result, config: TWConfig, squadsPerSection: Int, trace: TWTrace) {
        // we want to look at all of the available squads in a canonical sorted order
        var consideredSquads = [(Model.Player, SquadEvaluation)]()
        for player in squadsByPlayer.keys.sorted() {
            consideredSquads.append(contentsOf: squadsByPlayer[player, default: []])
        }
        consideredSquads.sort(by: { compareSquads($0, $1, config: config) })
        
        // prepare this for defense placement
        let invertedPreferredSections = self.invertedPreferredSections(config.preferredSections)
        
        // run the instructions
        for instruction in config.placementInstructions {
            trace.append("RUN \(instruction) {")
            switch instruction {
            case .placeSquad(squad: let squad, section: let section):
                if let index = consideredSquads.firstIndex(where: { item in
                    if item.1.squad.name == squad {
                        let player = item.0
                        let alreadyPlaced = result.placedSquadsFor(player: player)
                        let allowed = result.maximumSquadsFor(player: player)
                        
                        return allowed.canPlace(squad: item.1.squad, placed: alreadyPlaced)
                    }
                    return false
                }) {
                    // remove it from the big list so we don't use it again
                    let item = consideredSquads.remove(at: index)
                    
                    result.add(player: item.0, evalutation: item.1, section: section)
                    
                    // remove it from the player list so it doesn't get scheduled again later
                    squadsByPlayer[item.0, default:[]].removeAll() { return $0.1.squad.name == squad }
                }
                
            case .placeManySquads(squad: let squad, section: let section, count: let count):
                var placed = 0
                while placed < count, let index = consideredSquads.firstIndex(where: { item in
                    if item.1.squad.name == squad {
                        let player = item.0
                        let alreadyPlaced = result.placedSquadsFor(player: player)
                        let allowed = result.maximumSquadsFor(player: player)
                        
                        return allowed.canPlace(squad: item.1.squad, placed: alreadyPlaced)
                    }
                    return false
                }) {
                    placed += 1
                    
                    // remove it from the big list so we don't use it again
                    let item = consideredSquads.remove(at: index)
                    
                    result.add(player: item.0, evalutation: item.1, section: section)
                    
                    // remove it from the player list so it doesn't get scheduled again later
                    squadsByPlayer[item.0, default:[]].removeAll() { return $0.1.squad.name == squad }
                }

            // place high value squads in preferred sections, if possible
            case .placePreferred(let squadName, let count):
                var placed = 0
                var reported = Set<Model.Player>()
                while placed < count, let index = consideredSquads.firstIndex(where: { item in
                    if item.1.squad.name == squadName {
                        let player = item.0
                        let alreadyPlaced = result.placedSquadsFor(player: player)
                        let allowed = result.maximumSquadsFor(player: player)
                        
                        if allowed.canPlace(squad: item.1.squad, placed: alreadyPlaced) {
                            return true
                        } else {
                            if trace.enabled && !reported.contains(item.0) {
                                let squad = item.1.squad
                                trace.append("skipping (above placement limit): \(player.name) - \(squad.name) - \(squad.description()) \(squad.gp) gp")
                                trace.record(player, squad, status: .skippedAboveLimit)
                            }
                            reported.insert(player)
                        }
                    }
                    return false
                }) {
                    let item = consideredSquads[index]
                    
                    if let preferredSections = config.preferredSections[squadName] {
                        let player = item.0
                        let squad = item.1.squad
                        if let section = randomSection(gp: squad.gp, isFleet: squad.isFleet, counts: result.placedSquadCountBySection, shuffledSections: preferredSections.shuffled(using: &RandomScheduler3.r), squadsPerSection:squadsPerSection) {
                            
                            placed += 1
                            consideredSquads.remove(at: index)
                            
                            if trace.enabled {
                                trace.append("placing in \(section): \(player.name) - \(squad.name) - \(squad.description()) \(squad.gp) gp")
                                trace.record(player, squad, status: .placedPreferred(section))
                            }
                            
                            result.add(player: player, evalutation: item.1, section: section)
                            
                            // remove it from the player list so it doesn't get scheduled again later
                            squadsByPlayer[player, default:[]].removeAll() { return $0.1.squad.name == squadName }
                        } else {
                            // failed to place, all others will be less than this
                            if trace.enabled {
                                trace.append("failed to place in \(preferredSections): \(player.name) - \(squad.name) - \(squad.description()) \(squad.gp) gp")
                                trace.record(player, squad, status: .skippedPreferredSectionsFull)
                            }
                            break
                        }
                    }
                }
                trace.append("placed \(placed)")
                
            case .placeAllPreferred(var squadCounts):
                var placed = 0
                var reported = Set<Model.Player>()
                while !squadCounts.isEmpty, let index = consideredSquads.firstIndex(where: { item in
                    if squadCounts[item.1.squad.name] != nil {
                        let player = item.0
                        let alreadyPlaced = result.placedSquadsFor(player: player)
                        let allowed = result.maximumSquadsFor(player: player)
                        
                        if allowed.canPlace(squad: item.1.squad, placed: alreadyPlaced) {
                            return true
                        } else {
                            if trace.enabled && !reported.contains(item.0) {
                                let squad = item.1.squad
                                trace.append("skipping (above placement limit): \(player.name) - \(squad.name) - \(squad.description()) \(squad.gp) gp")
                                trace.record(player, squad, status: .skippedAboveLimit)
                            }
                            reported.insert(player)
                        }
                    }
                    return false
                }) {
                    let item = consideredSquads[index]
                    let squadName = item.1.squad.name
                    
                    if let preferredSections = config.preferredSections[squadName] {
                        let player = item.0
                        let squad = item.1.squad
                        if let section = randomSection(gp: squad.gp, isFleet: squad.isFleet, counts: result.placedSquadCountBySection, shuffledSections: preferredSections.shuffled(using: &RandomScheduler3.r), squadsPerSection:squadsPerSection) {
                            
                            squadCounts[squadName, default: 1] -= 1
                            if squadCounts[squadName] == 0 {
                                squadCounts[squadName] = nil
                            }
                            placed += 1
                            consideredSquads.remove(at: index)
                            
                            if trace.enabled {
                                trace.append("placing in \(section): \(player.name) - \(squad.name) - \(squad.description()) \(squad.gp) gp")
                                trace.record(player, squad, status: .placedPreferred(section))
                            }
                            
                            result.add(player: player, evalutation: item.1, section: section)
                            
                            // remove it from the player list so it doesn't get scheduled again later
                            squadsByPlayer[player, default:[]].removeAll() { return $0.1.squad.name == squadName }
                        } else {
                            // failed to place, all others (of this squad) will be less than this
                            if trace.enabled {
                                trace.append("failed to place in \(preferredSections): \(player.name) - \(squad.name) - \(squad.description()) \(squad.gp) gp")
                                trace.record(player, squad, status: .skippedPreferredSectionsFull)
                            }
                            squadCounts[squadName] = nil
                        }
                    }
                }
                trace.append("placed \(placed)")

            // force place N squads for the player (defensive player)
            case .placePlayerSquads(let playerName, let count):
                let allyCode = Int(playerName)
                if let player = squadsByPlayer.keys.first(where: { $0.name == playerName || $0.stats.allyCode == allyCode }) {
                    let allowed = result.maximumSquadsFor(player: player)
                    for _ in 0 ..< count {
                        let alreadyPlaced = result.placedSquadsFor(player: player)
                        
                        let canPlaceSquad = alreadyPlaced.named < allowed.named
                        let canPlaceFleet = alreadyPlaced.fleet < allowed.fleet
                        
                        if !canPlaceSquad && !canPlaceFleet {
                            if trace.enabled {
                                trace.append("stop (above placement limit): \(player.name)")
                                trace.record(player, nil, status: .skippedAboveLimitDefense)
                            }
                            break
                        }
                        
                        if let index = consideredSquads.firstIndex(where: {
                            $0.0 == player &&
                            ($0.1.squad.isFleet ? canPlaceFleet : canPlaceSquad)
                        }) {
                            let (player, evaluation) = consideredSquads[index]
                            let squad = evaluation.squad
                            let gp = squad.gp
                            if gp > config.minimumGP {
                                
                                if let section = randomSection(squadName:squad.name, gp: gp, isFleet: squad.isFleet, counts: result.placedSquadCountBySection, potentialSections: config.placementSections, alreadyPlacedSquads: result.squadsBySection, preferredSections: invertedPreferredSections, squadsPerSection: squadsPerSection) {
                                    
                                    result.add(player: player, evalutation: evaluation, section: section)
                                    consideredSquads.remove(at: index)
                                    squadsByPlayer[player, default:[]].removeAll() { return $0.1.squad == squad }
                                    
                                    if trace.enabled {
                                        trace.append("placing in \(section): \(player.name) - \(squad.name) - \(squad.description()) \(squad.gp) gp")
                                        trace.record(player, squad, status: .placedDefense(section))
                                    }
                                }
                            }
                        }
                    }
                }
                
            case .removeAll(squad: let squad, let status):
                consideredSquads.removeAll() { item in
                    if item.1.squad.name == squad {
                        trace.record(item.0, item.1.squad, status: status)
                        return true
                    } else {
                        return false
                    }
                }
                
                for player in squadsByPlayer.keys {
                    squadsByPlayer[player, default: []].removeAll () { return $0.1.squad.name == squad }
                }
                
            case .removeTop(squad: let squad, let count):
                var removed = 0
                while removed < count, let index = consideredSquads.firstIndex(where: { item in
                    return item.1.squad.name == squad
                }) {
                    removed += 1
                    
                    // remove it from the big list so we don't use it again
                    let item = consideredSquads.remove(at: index)
                    
                    // remove it from the player list so it doesn't get scheduled again later
                    squadsByPlayer[item.0, default:[]].removeAll() { return $0.1.squad.name == squad }
                }

            case .removeAbove(let squadName, let gp, let skipPlayerIds):
                let filter = { (squad: Squad) in
                    return squad.name == squadName && squad.gp > gp
                }
                if trace.enabled {
                    for (player, evaluation) in consideredSquads {
                        if !skipPlayerIds.contains(player.stats.allyCode) && filter(evaluation.squad) {
                            trace.append("remove (above): \(player.name) - \(evaluation.squad.name) - \(evaluation.squad.description()) \(evaluation.squad.gp) gp")
                            trace.record(player, evaluation.squad, status: .skippedAboveGPThreshold)
                        }
                    }
                }
                consideredSquads.removeAll() { item in
                    filter(item.1.squad)
                }
                for player in squadsByPlayer.keys {
                    squadsByPlayer[player, default: []].removeAll () { return filter($0.1.squad) }
                }
                
            case .removeBelow(let squadName, let gp):
                let filter = { (squad: Squad) in
                    return squad.name == squadName && squad.gp <= gp
                }
                if trace.enabled {
                    for (player, evaluation) in consideredSquads {
                        if filter(evaluation.squad) {
                            trace.append("remove (below): \(player.name) - \(evaluation.squad.name) - \(evaluation.squad.description()) \(evaluation.squad.gp) gp")
                            trace.record(player, evaluation.squad, status: .skippedBelowGPThreshold)
                        }
                    }
                }
                consideredSquads.removeAll() { item in
                    filter(item.1.squad)
                }
                for player in squadsByPlayer.keys {
                    squadsByPlayer[player, default: []].removeAll () { return filter($0.1.squad) }
                }

            case .removeWhere(filter: let filter):
                consideredSquads.removeAll() { item in
                    return filter(item.1)
                }
                
                for player in squadsByPlayer.keys {
                    squadsByPlayer[player, default: []].removeAll () { return filter($0.1) }
                }
                
            case .removeSquads(squads: let squads, player: let targetPlayer):
                
                consideredSquads.removeAll() { (player, ev) in
                    if player == targetPlayer {
                        if squads.contains(ev.squad.name) {
                            trace.append("skipping (held back): \(player.name) - \(ev.squad.name) - \(ev.squad.description()) \(ev.squad.gp) gp")
                            trace.record(player, ev.squad, status: .skippedHeldBack)

                            return true
                        }
                    }
                    return false
                }
                
                squadsByPlayer[targetPlayer, default: []].removeAll () {
                    squads.contains($0.1.squad.name)
                }
                
            }
            trace.append("} RUN \(instruction)")
        }
    }
    
    public func schedule(data: [Model.Player : [SquadEvaluation]], config: TWConfig, squadsPerSection: Int, trace: TWTrace = TWTrace()) -> TWSectionSchedulerResult {
        let potentialSections = config.placementSections
        
        var result = Result(config: config, squadsPerSection: squadsPerSection)
        var squadsByPlayer = buildSquadsByPlayer(data: data, config:config)
                
        if trace.enabled {
            for player in squadsByPlayer.keys.sorted() {
                trace.append("max squads \(player.name): \(result.maximumSquadsFor(player: player)), available = \(squadsByPlayer[player, default:[]].count)")
                trace.record(player, nil, status: .maxSquads(result.maximumSquadsFor(player: player).named))
            }
            
            trace.append("START { inventory")
            for player in data.keys.sorted() {
                for evaluation in data[player]! {
                    trace.append("\(player.name) - \(evaluation.squad.name) - \(evaluation.squad.description()) \(evaluation.squad.gp) gp")
                }
                let squadCount = data[player]!.reduce(0, { $0 + ($1.squad.isFleet ? 0 : 1) })
                let fleetCount = data[player]!.reduce(0, { $0 + ($1.squad.isFleet ? 1 : 0) })
                trace.record(player, nil, status: .availableSquads(squadCount))
                trace.record(player, nil, status: .availableFleet(fleetCount))
            }
            trace.append("END } inventory")
        }
        
        trace.append("PREFERRED SECTIONS: \(config.preferredSections)")
        
        trace.append("START { runPlacementInstructions")
        runPlacementInstructions(squadsByPlayer: &squadsByPlayer, into: &result, config:config, squadsPerSection: squadsPerSection, trace: trace)
        trace.append("END } runPlacementInstructions")

        // now build the squad list to contain only the squads we intend to place -- honor the player limits
        var consideredSquads = [(Model.Player, SquadEvaluation)]()
        for player in squadsByPlayer.keys.sorted() {
            
            let alreadyPlaced = result.placedSquadsFor(player: player)
            let allowed = result.maximumSquadsFor(player: player)
            
            let keepSquad = allowed.named - alreadyPlaced.named
            let keepFleet = allowed.fleet - alreadyPlaced.fleet
            
            let squads = squadsByPlayer[player, default: []].filter { !$0.1.squad.isFleet }
            let fleets = squadsByPlayer[player, default: []].filter { $0.1.squad.isFleet }
            let playerSquads = squads.prefix(keepSquad)
            let playerFleets = fleets.prefix(keepFleet)
            
            consideredSquads.append(contentsOf: playerSquads)
            consideredSquads.append(contentsOf: playerFleets)

            trace.append("leftover squads for \(player.name): \(playerSquads.count) / \(playerFleets.count)")
        }
        
        // again, do a global sort to get the top squads we want to schedule in order.  this time we only have the top N squads from each player
        consideredSquads.sort(by: { compareSquads($0, $1, config: config) })
        
        if trace.enabled {
            trace.append("START { consideredSquads")
            for squad in consideredSquads {
                trace.append("\(squad.0.name) - \(squad.1.squad.name) - \(squad.1.squad.description()) \(squad.1.squad.gp) gp")
            }
            trace.append("END } consideredSquads")
        }
        
        trace.append("START { random placement")

        // then schedule the rest randomly
        for (player, evaluation) in consideredSquads {
            let gp = evaluation.squad.gp
            if gp > config.minimumGP {
                if let section = randomSection(squadName:evaluation.squad.name, gp: gp, isFleet: evaluation.squad.isFleet, counts: result.placedSquadCountBySection, potentialSections: potentialSections, alreadyPlacedSquads: result.squadsBySection, preferredSections: config.preferredSections, squadsPerSection: squadsPerSection) {
                    
                    trace.append("Placed \(player.name) - \(evaluation.squad.name) - \(section)")
                    trace.record(player, evaluation.squad, status: .placedRandom(section))
                    result.add(player: player, evalutation: evaluation, section: section)
                } else {
                    trace.append("Unable to place \(player.name) - \(evaluation.squad.name)")
                    trace.record(player, evaluation.squad, status: .skippedRandomSectionsFull)
                }
            }
        }
        
        trace.append("END } random placement")

        // finally, fill in the remaining spots with fluff squads
        let remainingPlayers = data.keys.filter { player in !result.isPlayerDone(player, fleet: nil) }.sorted { p1, p2 in
            let pc1 = result.maximumSquadsFor(player: p1).totalSquads - result.placedSquadCountByPlayer[p1, default: 0]
            let pc2 = result.maximumSquadsFor(player: p2).totalSquads - result.placedSquadCountByPlayer[p2, default: 0]
            if pc1 == pc2 {
                return p1 > p2
            } else {
                return pc1 > pc2
            }
        }
        
        trace.append("START { place fluff squads")

        let sectionsMinusFleet = potentialSections.removing(TW.allFleetSections)
        while !result.allSectionsExceptFleetFull() && !result.allDone(players: remainingPlayers, fleet: false) {
            for player in remainingPlayers {
                if result.allSectionsExceptFleetFull() {
                    break
                }
                if !result.isPlayerDone(player, fleet: false) {
                    // find an section that is not full
                    while let section = sectionsMinusFleet.randomElement(using: &RandomScheduler3.r) {
                        if result.placedSquadCountBySection[section, default: 0] < squadsPerSection {
                            trace.append("fluff squad for \(player.name) -> \(section)")
                            trace.record(player, nil, status: .placedAny(section))
                            
                            let squad = Squad(Squad.Properties(name: "Any Squad"), units: [], isFleet: false)
                            let evaluation = SquadEvaluation(squad: squad, evaluations: [:])
                            result.add(player: player, evalutation: evaluation, section: section)
                            break
                        }
                    }
                }
            }
        }
        
        if config.fleetInstructions.isEmpty || config.fleetInstructions == "NONE" {
            // don't place fluff fleets
            
        } else {
            let fleetSections = potentialSections.removing(TW.allGroundSections)
            while !result.allFleetSectionsFull() && !result.allDone(players: remainingPlayers, fleet: true) {
                for player in remainingPlayers {
                    if result.allFleetSectionsFull() {
                        break
                    }
                    if !result.isPlayerDone(player, fleet: true) {
                        // find an section that is not full
                        while let section = fleetSections.randomElement(using: &RandomScheduler3.r) {
                            if result.placedSquadCountBySection[section, default: 0] < squadsPerSection {
                                trace.append("fluff fleet for \(player.name) -> \(section)")
                                trace.record(player, nil, status: .placedAny(section))
                                
                                let squad = Squad(Squad.Properties(name: config.fleetInstructions), units: [], isFleet: true)
                                let evaluation = SquadEvaluation(squad: squad, evaluations: [:])
                                result.add(player: player, evalutation: evaluation, section: section)
                                break
                            }
                        }
                    }
                }
            }

        }
        
        trace.append("END } place fluff squads")
        
        return TWSectionSchedulerResult(placedSquads: result.placements, all: data)
    }
    
}

