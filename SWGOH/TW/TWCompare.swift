//
//  TWCompare.swift
//  swgoh
//
//  Created by David Koski on 5/14/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

public struct TWCompare {
    
    /// array of player -> SquadEvaluation maps, one per team (we support two teams)
    let sides: [[Model.Player:[SquadEvaluation]]]
    
    let allSquadNames: [String]
    
    private let squadsByName: [[String:[Model.Player:[Squad]]]]
        
    public init(side1: [Model.Player:[SquadEvaluation]], side2: [Model.Player:[SquadEvaluation]]) {
        func filterSide(_ values: [Model.Player:[SquadEvaluation]]) -> [Model.Player:[SquadEvaluation]] {
            Dictionary(uniqueKeysWithValues: values.map { ($0, $1.filter { $0.status.isViable }) })
        }
        sides = [filterSide(side1), filterSide(side2)]
        
        let allEvaluations = sides.flatMap { $0.values.flatMap { $0 } }
        allSquadNames = Set(allEvaluations.map(TW.canonicalName)).sorted()
        
        var squadsByName = [[String:[Model.Player:[Squad]]]]()
        for side in sides {
            var teamSquads = [String:[Model.Player:[Squad]]]()
            for (player, evaluations) in side {
                for evaluation in evaluations {
                    let name = TW.canonicalName(evaluation)
                    teamSquads[name, default: [:]][player, default:[]].append(evaluation.squad)
                }
            }
            squadsByName.append(teamSquads)
        }
        self.squadsByName = squadsByName
    }
        
    /// represents a single row in the result
    public class Row : Equatable, Identifiable {
        
        public let group: Int
        public let label: String
        public let teams: [Team]
        
        public var team: Team { teams[0] }
        public var opponent: Team { teams[1] }
        
        public var percentCount: Double { percent(keyPath: \.count) }
        public var percentScore: Double { percent(keyPath: \.score) }

        public lazy var histograms: [Histogram] = {
            let scores = teams.map { team in team.scores }
            let targetBuckets = scores[0].count > 100 ? 50 : Histogram.TARGET_BUCKETS
            let params = Histogram.analyze(data: scores, targetBuckets: targetBuckets)
            let histograms = teams.map { team -> Histogram in
                let markers = [ "50th" : team.p50, "75th" : team.p75, "90th" : team.p90 ]
                return Histogram(name: label, data: team.scores, start: params.start, bucketSize: params.bucketSize, count: params.count, markers: markers)
            }
            return histograms
        }()
        
        public lazy var cumulativeHistograms: [Histogram] = {
            self.histograms.map { Histogram(cumulative: $0) }
        }()
        
        lazy var unitHistograms: [[Model.Character:Histogram]] = {
            // collect all the unit gps
            let units = [[Model.Character:[Int]]]()
            for side in teams {
                var teamUnits = [Model.Character:[Int]]()
                for squads in side.data.values {
                    for squad in squads {
                        for unit in squad.units {
                            teamUnits[unit.character, default: []].append(unit.gp)
                        }
                    }
                }
            }
            
            // turn those into histograms
            var unitHistograms = [[Model.Character:Histogram]](repeating: [:], count: 2)
            let allCharacters = Set(units.flatMap { teamUnits in teamUnits.keys })
            for character in allCharacters {
                let units0 = units[0][character, default: []]
                let units1 = units[1][character, default: []]

                let params = Histogram.analyze(data: [ units0, units1 ])
                unitHistograms[0][character] = Histogram(name: character.name, data: units0, start: params.start, bucketSize: params.bucketSize, count: params.count)
                unitHistograms[1][character] = Histogram(name: character.name, data: units0, start: params.start, bucketSize: params.bucketSize, count: params.count)
            }
            
            return unitHistograms
        }()
        
        init(group: Int, label: String, score: KeyPath<Team, Int>, sides: [[Model.Player:[Squad]]]) {
            self.group = group
            self.label = label
            
            let scores = sides.map { team in team.values.flatMap { $0.map { $0.gp }}.sorted() }
            self.teams = zip(sides, scores).map { Team(data: $0, scores: $1, score: score) }
        }
        
        func percent(keyPath: KeyPath<Team, Int>) -> Double {
            let v1 = teams[0][keyPath: keyPath]
            let v2 = teams[1][keyPath: keyPath]
            if v2 == 0 {
                return 0.0
            } else {
                return Double(v1) / Double(v2)
            }
        }
        
        public func rating(scores: [(Int, Int)]) -> Double {
            var percents = [Double]()
            for (s1, s2) in scores {
                if min(s1, s2) > 1 && max(s1, s2) < 5 {
                    // small data, don't worry
                    percents.append(1)
                } else {
                    if s2 == 0 {
                        percents.append(1)
                    } else {
                        percents.append(Double(s1) / Double(s2))
                    }
                }
            }

            // TODO for count + sum we might want the min, but for count + percentile, maybe multiply (because low on both is extra bad)
            let rating = min(percents.reduce(1.0, { $0 * $1 }), 1.0)
//            let rating = min(percents.min() ?? 1.0, 1.0)
            return rating
        }
                
        var rating: Double { rating(scores: [(teams[0].count, teams[1].count), (teams[0].score, teams[1].score)])}
                
        public static func == (lhs: TWCompare.Row, rhs: TWCompare.Row) -> Bool {
            lhs.label == rhs.label && lhs.teams == rhs.teams
        }

        public struct Team : Equatable {
            /// the backing data
            public let data: [Model.Player:[Squad]]
            
            /// sorted array of gp scores for the data
            public let scores: [Int]
            
            public var count: Int { scores.count }
            public var p50: Int { count == 0 ? 0 : scores[count / 2] }
            public var p75: Int { count == 0 ? 0 : scores[count * 3 / 4] }
            public var p90: Int { count == 0 ? 0 : scores[count * 9 / 10] }
            public var p95: Int { count == 0 ? 0 : scores[count * 19 / 20] }
            public var p99: Int { count == 0 ? 0 : scores[count * 99 / 100] }
            
            public let sum: Int
            
            public var average: Int { count > 0 ? sum / count : 0 }
            
            public var score: Int = 0
                        
            public static func == (lhs: TWCompare.Row.Team, rhs: TWCompare.Row.Team) -> Bool {
                lhs.scores == rhs.scores
            }

            internal init(data: [Model.Player : [Squad]], scores: [Int], score: KeyPath<Team, Int>) {
                self.data = data
                self.scores = scores
                self.sum = scores.reduce(0, +)
                self.score = self[keyPath: score]
            }

        }
        
    }
        
    /// produce "squads" containing the units that match the filter
    private func filter(units: (Model.Unit) -> Bool) -> [[Model.Player:[Squad]]] {
        var result = [[Model.Player:[Squad]]]()
        
        for side in sides {
            var sideResult = [Model.Player:[Squad]]()
            for player in side.keys {
                sideResult[player] = player.units.values.filter(units).map { Squad(Squad.Properties(name: "anon"), units:[$0])}
            }
            result.append(sideResult)
        }
        
        return result
    }
    
    public func unitRows() -> [Row] {
        var result = [Row]()
        
        result.append(Row(group: 1, label: "G10+", score: \.sum, sides: filter(units: { $0.gear >= 10 })))
        result.append(Row(group: 1, label: "G11+", score: \.sum, sides: filter(units: { $0.gear >= 11 })))
        result.append(Row(group: 1, label: "G12+", score: \.sum, sides: filter(units: { $0.gear >= 12 })))
        result.append(Row(group: 1, label: "G13+", score: \.sum, sides: filter(units: { $0.gear >= 13 })))
        result.append(Row(group: 1, label: "R4+", score: \.sum, sides: filter(units: { $0.relic >= (4 + 2) })))
        result.append(Row(group: 1, label: "R7", score: \.sum, sides: filter(units: { $0.relic >= (7 + 2) })))
        
        // by squad gp
        let allSquads = sides.map { $0.mapValues { evs in evs.map { ev in ev.squad } }}
        func appendRow(gp: Int) {
            let sides = allSquads.map { $0.mapValues { squads in squads.filter { !$0.isFleet && $0.gp > gp * 1_000 } } }
            
            if sides.map({ $0.contains { !$0.value.isEmpty } }).contains(true) {
                result.append(Row(group: 10, label: "\(gp)k+ Squads", score: \.sum, sides: sides))
            }
        }
        
        appendRow(gp: 100)
        appendRow(gp: 110)
        appendRow(gp: 120)
        appendRow(gp: 130)
        appendRow(gp: 140)
        appendRow(gp: 150)
        
        let fleets = allSquads.map { $0.mapValues { squads in squads.filter { $0.isFleet } } }
        result.append(Row(group: 11, label: "Fleets", score: \.sum, sides: fleets))
        
        for importantToon in [ "Rey", "SL Kylo", "GAS", "JKL", "SEE", "JMLS", "GMK" ] {
            let sides = filter(units: { $0.commonName == importantToon })
            let count = sides.reduce(0, { $0 + $1.values.reduce(0, { $0 + $1.count })})
            if count > 0 {
                result.append(Row(group: 12, label: "Unit: \(importantToon)", score: \.average, sides: sides))
            }
        }

        return result
    }
    
    public func squadRows(isKey: Bool) -> [Row] {
        var result = [Row]()

        // by squad name
        for squadName in allSquadNames where TW.keySquads.contains(squadName) == isKey {
            let s0 = squadsByName[0][squadName, default: [:]]
            let s1 = squadsByName[1][squadName, default: [:]]

            result.append(Row(group: isKey ? 11 : 12, label: squadName, score: \.p75, sides: [s0, s1]))
        }
        
        return result
    }
}
