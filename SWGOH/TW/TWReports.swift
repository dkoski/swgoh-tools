//
//  TWReports.swift
//  swgoh
//
//  Created by David Koski on 5/9/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

public struct TWPlanningReport : Report {
    
    let scheduler: TWSectionScheduler
    let config: TWConfig
    let squadsPerSection: Int
    
    public init(scheduler: TWSectionScheduler = defaultTWScheduler(), config: TWConfig, squadsPerSection: Int = 25) {
        self.scheduler = scheduler
        self.config = config
        self.squadsPerSection = squadsPerSection
    }
    
    public func report(data: [Model.Player : [SquadEvaluation]]) -> String {
        
        let schedule = scheduler.schedule(data: data, config: config, squadsPerSection: squadsPerSection, trace: TWTrace()).placedSquads
        var groups = [String:[(Model.Player,SquadEvaluation)]]()
        
        for (player, evaluation, section) in schedule {
            groups[section, default: []].append( (player, evaluation) )
        }
        
        var result = ""
        for group in groups.keys.sorted() {
            result += "**\(group)** count = \(groups[group]!.count)\n"
            
            let squads = groups[group]!.sorted { (s1, s2) -> Bool in
                let gp1 = s1.1.squad.gp
                let gp2 = s2.1.squad.gp
                return gp1 > gp2
            }
            for (player, evaluation) in squads {
                result += "\(player.name): "
                result += evaluation.squad.description(zetas: true, gearBelow: 30, starsBelow: 7)
                result += " - \(evaluation.squad.gp)"
                
                result += "\n"
            }
            
            result += "\n\n"
        }
        
        return result
    }
    
    public func report(player: Model.Player, evaluations: [SquadEvaluation]) -> String {
        // not used
        return ""
    }
}

public struct TWPlanningSummaryReport : Report {
    
    let scheduler: TWSectionScheduler
    let config: TWConfig
    let squadsPerSection: Int
    
    public init(scheduler: TWSectionScheduler = defaultTWScheduler(), config: TWConfig, squadsPerSection: Int = 25) {
        self.scheduler = scheduler
        self.config = config
        self.squadsPerSection = squadsPerSection
    }
    
    public func report(data: [Model.Player : [SquadEvaluation]]) -> String {
        
        let schedule = scheduler.schedule(data: data, config: config, squadsPerSection: squadsPerSection, trace: TWTrace()).placedSquads
        var groups = [String:[String:Int]]()
        
        for (_, evaluation, section) in schedule {
            groups[section, default: [:]][evaluation.squad.name, default:0] += 1
        }
        
        var result = ""
        for groupName in groups.keys.sorted() {
            let group = groups[groupName, default:[:]]
            result += "**\(groupName)** count = \(group.values.reduce(0, +))\n"

            for squadName in group.keys.sorted() {
                result += "\(squadName): \(group[squadName]!)\n"
            }
            
            result += "\n\n"
        }
        
        return result
    }
    
    public func report(player: Model.Player, evaluations: [SquadEvaluation]) -> String {
        // not used
        return ""
    }
}

public struct TWPlanningReportHTML : Report {
    
    let scheduler: TWSectionScheduler
    let config: TWConfig
    let squadsPerSection: Int
    
    public init(scheduler: TWSectionScheduler = defaultTWScheduler(), config: TWConfig, squadsPerSection: Int = 25) {
        self.scheduler = scheduler
        self.config = config
        self.squadsPerSection = squadsPerSection
    }
    
    public func report(data: [Model.Player : [SquadEvaluation]]) -> String {
        
        let schedule = scheduler.schedule(data: data, config: config, squadsPerSection: squadsPerSection, trace: TWTrace()).placedSquads
        var groups = [String:[(Model.Player,SquadEvaluation)]]()
        
        for (player, evaluation, section) in schedule {
            groups[section, default: []].append( (player, evaluation) )
        }
        
        var result = ""
        for group in groups.keys.sorted() {
            result += "**\(group)** count = \(groups[group]!.count)\n"
            
            let squads = groups[group]!.sorted { (s1, s2) -> Bool in
                let gp1 = s1.1.squad.gp
                let gp2 = s2.1.squad.gp
                return gp1 > gp2
            }
            for (player, evaluation) in squads {
                result += "\(player.name): "
                result += evaluation.squad.name
                if !evaluation.squad.units.isEmpty {
                    result += " - "
                    result += evaluation.squad.description(gearBelow: 12, starsBelow: 7)
                    
                    let gp = evaluation.squad.gp
                    result += " - \(gp)"
                }
                
                result += "\n"
            }
            
            result += "\n\n"
        }
        
        return result
    }
    
    public func report(player: Model.Player, evaluations: [SquadEvaluation]) -> String {
        // not used
        return ""
    }
}

/// description of the TWSectionSchedulerResult.remainder (e.g. counters in a TW) by player
public func twRemainderByPlayer(remainder: [(Model.Player, SquadEvaluation)], mentions: [Int : String]
) -> String {
    var teams = [Model.Player:[SquadEvaluation]]()
    
    for (player, squadEvaluation) in remainder where squadEvaluation.status.isViable {
        teams[player, default: []].append(squadEvaluation)
    }
    
    for player in teams.keys {
        teams[player]?.sort()
    }
    
    var result = ""
    for player in teams.keys.sorted() {
        result += "**\(player.name)**"
        if let mention = mentions[player.stats.allyCode] {
            if !mention.contains("@") {
                result += " @" + mention
            } else {
                result += " " + mention
            }
        }
        result += "\n"
        for ev in teams[player, default: []].sorted() {
            result += "\t\(ev.squad.name): "
            result += TWPlayerReport.detailSquadFormatter(squad: ev.squad)
            result += "\n"
        }
        result += "\n\n"
    }
    
    return result
}

/// description of the TWSectionSchedulerResult.remainder (e.g. counters in a TW) by team
public func twRemainderByTeam(remainder: [(Model.Player, SquadEvaluation)], mentions: [Int : String]
) -> String {
    var teams = [String:[(Model.Player, SquadEvaluation)]]()
    
    for (player, squadEvaluation) in remainder {
        teams[squadEvaluation.squad.name, default: []].append((player, squadEvaluation))
    }
    
    for name in teams.keys {
        teams[name]?.sort { lhs, rhs in lhs.1 > rhs.1 }
    }
    
    var result = ""
    for name in teams.keys.sorted() {
        result += "**\(name)**: "
        
        for (player, _) in teams[name, default: []] {
            if let mention = mentions[player.stats.allyCode] {
                if !mention.contains("@") {
                    result += " @" + mention
                } else {
                    result += " " + mention
                }
            } else {
                result += " "
                result += player.name
            }
        }
        result += "\n*e.g.:* "
        result += TWPlayerReport.standardSquadFormatter(squad: teams[name]!.first!.1.squad)
        result += "\n\n"
        
        for (player, ev) in teams[name, default: []] {
            result += "\(player.name): "
            result += TWPlayerReport.detailSquadFormatter(squad: ev.squad)
            result += "\n"
        }
        result += "\n\n"
    }
    
    return result
}


public struct TWPlayerReport : Report {
    
    let scheduler: TWSectionScheduler
    let config: TWConfig
    let squadsPerSection: Int
        
    public init(scheduler: TWSectionScheduler = defaultTWScheduler(), config: TWConfig, squadsPerSection: Int = 25) {
        self.scheduler = scheduler
        self.config = config
        self.squadsPerSection = squadsPerSection
    }
    
    // TODO: turn this into a struct or enum?  shared across many classes?
    public static func standardSquadFormatter(squad: Squad) -> String {
        squad.description(zetas: true, gearBelow: 0, starsBelow: 0)
    }
    
    public static func detailSquadFormatter(squad: Squad) -> String {
        let gp = squad.gp
        return squad.description(zetas: true, gearBelow: 30, starsBelow: 7) +
            (gp > 0 ? " \(squad.gp) gp" : "")
    }
    
    public static func squadPlusGPFormatter(squad: Squad) -> String {
        let gp = squad.gp
        return squad.description(zetas: true, gearBelow: 0, starsBelow: 0) +
            (gp > 0 ? " \(squad.gp)" : "")
    }

    public static func report(schedule: [(Model.Player, SquadEvaluation, String)], mentions: [Int:String]?, squadFormatter: (Squad) -> String = standardSquadFormatter) -> String {
        var result = ""
        
        var byPlayer = [Model.Player: [(String, SquadEvaluation)]]()
        
        for (player, evaluation, section) in schedule {
            byPlayer[player, default: []].append( (section, evaluation) )
        }
        
        for player in byPlayer.keys.sorted() {
            let sectionEvaluations = byPlayer[player]!
            
            result += "**\(player.name)**"
            if let mention = mentions?[player.stats.allyCode] {
                if !mention.contains("@") {
                    result += " @" + mention
                } else {
                    result += " " + mention
                }
            }
            
            let placementScore = 34 * sectionEvaluations.filter { $0.1.squad.isFleet }.count +
                30 * sectionEvaluations.filter { !$0.1.squad.isFleet }.count
            result += " score: \(placementScore)"
            
            result += "\n"
            
            for (section, evaluation) in sectionEvaluations.sorted(by: { return $0.0 < $1.0 }) {
                result += "\(section): "
                result += evaluation.squad.name
                let description = squadFormatter(evaluation.squad)
                if !description.isEmpty {
                    result += " - " + description
                }
                result += "\n"
            }
            result += "\n\n"
        }
        
        return result
    }

    public func report(data: [Model.Player : [SquadEvaluation]], mentions: [Int:String]? = nil) -> String {
        let schedule = scheduler.schedule(data: data, config: config, squadsPerSection: squadsPerSection, trace: TWTrace()).placedSquads
        return TWPlayerReport.report(schedule: schedule, mentions: mentions)
    }
    
    public func report(player: Model.Player, evaluations: [SquadEvaluation]) -> String {
        // not used
        return ""
    }
}

