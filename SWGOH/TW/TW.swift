//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

public func TWProperties(name: String, id: String? = nil, attackerId: String? = nil, defenderId: String? = nil, preference: Int = 0, unitMinimumGP: Int = 0, rules: [String:UnitEvaluator]) -> Squad.Properties {
    return Squad.Properties(name: name, attackerId: id ?? attackerId, defenderId: id ?? defenderId, preference: preference, unitMinimumGP: 6000, rules: rules)
}

public struct TW {
    
    // MARK: - Squads
    
    // per hewbris, s-class
    static let trayaAdds = u("BSF", "Count Dooku", "Palpatine", "Sith Assassin", "Sith Empire Trooper", "Nest", "Thrawn")
    static let trayaSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Traya", id: "SITH_TRI", preference: 20, rules: trayaRules), u("Traya"), u("Nihilus"), u("Sion"), trayaAdds, trayaAdds)
    
    static let trayaRules = [
        "DEFAULT" : TWRule().e,
        "Traya" : TWRule(leaderZeta: true, uniqueZetas: true, stars: 7, gear: 11, preference: 2).e,
        "Sith Empire Trooper" : TWRule(preference: 2).e,
        "Thrawn" : TWRule(preference: 2).e,
    ]
    
    static let drAdds = u("Sith Marauder", "Sith Empire Trooper", "Sith Assassin", "Thrawn")
    static let drAddsNoZetaMalak = u("Sith Marauder", "Sith Empire Trooper", "Sith Assassin", "Thrawn", "Malak")
    public static let darthRevanSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "DRMalak", id: "SITH_EMPIRE_W_MALAK", preference: 25, rules: darthRevanRules), u("Darth Revan"), u("HK"), u("Bastila Shan (Fallen)"), u("Malak"), drAdds)
    static let darthRevanSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "DR", id: "SITH_EMPIRE_WO_MALAK", preference: 25, rules: darthRevanNoMalakRules), u("Darth Revan"), u("HK"), u("Bastila Shan (Fallen)"), drAdds, drAdds)
        
    static let darthRevanRules = [
        "DEFAULT" : TWRule().e,
        "Darth Revan" : TWRule(leaderZeta: true, uniqueZetas: true, stars: 7, gear: 10, preference: 2).e,
        "Sith Empire Trooper" : TWRule(preference: 2).e,
        "Sith Assassin" : TWRule(preference: 2).e,
        "Malak" : TWRule(uniqueZetas: true, stars: 6, gear: 11, preference: 5).e,
    ]
    
    
    static let malakNoZetaRules = UnitEvaluator(
        UnitRule.gear10PlusRule,
        UnitRule.level85Rule,
        
        // if the malak has no zetas, he is fine in the SITH_EMPIRE_WO_MALAK squad
        CustomRule() { unit in
            if unit.zetas.count > 0 {
                return Message(message: "no zetas", status: .needsFarming, score: 0, max: 0)
            }
            return Message(message: "unit boost", status: .ok, score: 0, max: 0, preference: 1)
        }
    )

    static let darthRevanNoMalakRules = [
        "DEFAULT" : TWRule().e,
        "Darth Revan" : TWRule(leaderZeta: true, uniqueZetas: true, stars: 7, gear: 10, preference: 2).e,
        "Sith Empire Trooper" : TWRule(preference: 2).e,
        "Sith Assassin" : TWRule(preference: 2).e,
        "Malak" : malakNoZetaRules,
    ]

    static let foRemainder = u(Model.Character.matchesCategory("First Order").map(\.id))
    public static let foSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "FO", id: "FO_KRU", rules: foRules), u("KYLORENUNMASKED"), u("KYLOREN"), foRemainder, foRemainder, foRemainder)

    static let foSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "FO", id: "FO_KRU", rules: foRules), u("KYLORENUNMASKED"), u("KYLOREN"), u("FOE"), u("Sith Trooper"), u("Hux"))

    static let foRules = [
        "DEFAULT" : TWRule().e,
        "KRU" : TWRule(leaderZeta: true, stars: 7, gear: 10, preference: 2).e,
        "FOE" : TWRule(preference: 2).e,
        "FOO" : TWRule(preference: 1).e,
        "FOST" : TWRule(preference: 1).e,
        "Hux" : TWRule(uniqueZetas: true, stars: 5, gear: 10, preference: 1).e,
    ]
    
    static let gkSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "GK", id: "GK", preference:8, rules: gkDefenseRules), u("GENERALKENOBI"), u("BARRISSOFFEE"), u("Ahsoka"), u("ANAKINKNIGHT"), u("GMY"))
    static let gkSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "GK", id: "GK", rules: gkDefenseRules), u("GENERALKENOBI"), u("BARRISSOFFEE"), u("WAMPA"), u("DN"), u("Talzin"))
    
    static let gkDefenseRules = [
        "DEFAULT" : TWRule().e,
        "GK" : TWRule(preference: 1).e,
        "BARRISSOFFEE" : TWRule(uniqueZetas: true, preference: 1).e,
        "JOLEEBINDO" : TWRule(preference: 2).e,

        "QUIGONJINN" : TWRule(preference: 1).e,
        "AHSOKATANO" : TWRule(preference: 1).e,

    ]
    
    static let revanSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Revan", id: "JEDI_REVAN", preference: 17, rules: revanRules), u("JEDIKNIGHTREVAN"), u("BASTILASHAN"), u("GMY"), u("Jolee"), u("GK"))
    static let revanSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Revan Offense", attackerId: "JEDI_REVAN", defenderId: "JEDI_REVAN", preference: 15, rules: revanRules), u("JEDIKNIGHTREVAN"), u("GMY"), u("Bastila Shan", "Hermit Yoda"), u("Jolee"), u("Hermit Yoda", "JKA", "Ezra", "BASTILASHAN"))
    
    static let revanRules = [
        "DEFAULT" : TWRule().e,
        
        "JKR" : TWRule(leaderZeta: true, uniqueZetas: true).e,
        "Hermit Yoda" : TWRule(preference: 1).e,
    ]
    
    static let bastilaSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "BS", id: "JEDI_BASTILA", rules: bastilaShanRules), u("BASTILASHAN"), u("GRANDMASTERYODA"), u("HERMITYODA", "JOLEE", "Aayla", "Old Ben", "JKA", "Ahsoka"), u("GENERALKENOBI", "Old Ben"), u("EZRABRIDGERS3", "HERMITYODA", "JKA", "Ahsoka"))
    static let bastilaRevanBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "BS+Revan", id: "JEDI_BASTILA_REVAN", preference: -10, rules: bastilaShanRules), u("BASTILASHAN"), u("GRANDMASTERYODA"), u("HERMITYODA", "JOLEE", "Aayla", "Old Ben", "JKA", "Ezra"), u("GENERALKENOBI"), u("Jedi Knight Revan"))

    static let bastilaShanRules = [
        "DEFAULT" : TWRule().e,
        
        "Jolee" : TWRule(preference: 1).e,
        "Hermit Yoda" : TWRule(preference: 1).e,
        
        "BASTILASHAN" : TWRule(leaderZeta: true).e,
        "GRANDMASTERYODA" : TWRule().e,
        
        "Old Ben" : TWRule(preference: -1).e,
    ]
    
    static let phoenixRemainder = u(Model.Character.matchesCategory("Phoenix").map(\.id))

    static let phoenixSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Phoenix", id: "PHOENIX", rules: phoenixRules), u("Hera"), phoenixRemainder, phoenixRemainder, phoenixRemainder, phoenixRemainder)
    
    static let phoenixRules = [
        "Hera" : TWRule(preference: 1).e,
        "Ezra" : TWRule(preference: 1).e,
        "Kanan" : TWRule(preference: 1).e,
        "Zeb" : TWRule(preference: 1).e,
        "Sabine" : TWRule(preference: 1).e,
        "Chopper" : TWRule(preference: 0).e,
    ]
    
    static let bhAddsIds = Model.Character.matchesCategory("Bounty Hunter").map(\.id)

    static let bhAdds = u(bhAddsIds)
    static let bhAddsPlusNest = u(bhAddsIds.adding("Nest"))
    static let bhSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "BH", id: "BH_BOSSK", preference: 6, rules: bosskRules), u("Bossk"), bhAdds, bhAdds, bhAdds, bhAdds)
    static let bhSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "BH", id: "BH_JANGO", preference: 3, rules: jangoRules), u("Jango"), bhAdds, bhAdds, bhAdds, bhAddsPlusNest)
    static let bhSquadBuilder3 = SelectUnitSquadBuilder(properties: TWProperties(name: "Disintegration", preference: 20, rules: disintegrationRules), u("Aurra"), u("Greef Karga"), u("Boba Fett"), u("Bossk"), u("The Mandalorian"))

    static let bosskRules = [
        "BOSSK" : TWRule(leaderZeta: true, preference: 5).e,
        "DENGAR" : TWRule(preference: 1).e,
        "BOBAFETT" : TWRule(preference: 1).e,
        "DEFAULT" : TWRule().e,
    ]
    static let jangoRules = [
        "JANGOFETT" : TWRule(leaderZeta: true, preference: 5).e,
        "BOSSK" : TWRule(preference: 1).e,
        "DENGAR" : TWRule(preference: 1).e,
        "BOBAFETT" : TWRule(preference: 1).e,
        "Nest" : TWRule(uniqueZetas: true, stars: 7, gear: 11, preference: 0).e,
        "DEFAULT" : TWRule().e,
    ]
    static let disintegrationRules = [
        "The Mandalorian" : TWRule(stars: 6).e,
        "Aurra" : TWRule(gear: 9).e,
        "DEFAULT" : TWRule().e,
    ]

    // per hewbris, this is S class
    static let clsSquadBuilder1 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", id: "REBELS_CLS", preference: 20, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("CHEWBACCALEGENDARY"), u("R2D2_LEGENDARY"), u("C3POLEGENDARY"))
    static let clsSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", id: "REBELS_CLS_CHAZE", preference: 1, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("Baze"), u("Chirrut"), u("CHEWBACCALEGENDARY"))
    static let clsSquadBuilder3 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", id: "REBELS_CLS", preference: 4, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("Leia"), u("Fulcrum"), u("CHEWBACCALEGENDARY"))
    static let clsSquadBuilder4 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", id: "REBELS_CLS", preference: 4, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("Leia"), u("Old Ben"), u("CHEWBACCALEGENDARY"))
    static let clsSquadBuilder5 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", id: "REBELS_CLS", preference: 4, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("Wedge"), u("Biggs"), u("CHEWBACCALEGENDARY"))
    static let clsSquadBuilder6 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", id: "REBELS_CLS", preference: 14, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("CHEWBACCALEGENDARY"), u("Leia"), u("C3POLEGENDARY"))
    static let clsSquadBuilder8 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", id: "REBELS_CLS_THRAWN", preference: 14, rules: defenseRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("CHEWBACCALEGENDARY"), u("Thrawn"), u("C3POLEGENDARY"))

    static let clsSquadBuilder = MultipleSquadBuilder(clsSquadBuilder1, clsSquadBuilder2, clsSquadBuilder3, clsSquadBuilder4, clsSquadBuilder5, clsSquadBuilder6, clsSquadBuilder8)

    // this one is supposed to be over the top
    static let qiraSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Qira", id: "QIRA", preference: 40, rules: defenseRules), u("QIRA"), u("L3_37"), u("VANDOR"), u("MISSION"), u("ZAALBAR"))
    static let qiraSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Qira", id: "QIRA", preference: 30, rules: defenseRules), u("QIRA"), u("L3_37"), u("VANDOR"), u("NEST"), u("ZAALBAR"))

    static let qiraRules = [
        "NEST" : TWRule(stars: 5, gear: 9, preference: 2).e,
        "VANDOR" : TWRule(uniqueZetas:true, stars: 6, gear: 9).e,
        "QIRA" : TWRule(stars: 7, gear: 10, preference: 0).e,
        "L3_37" : TWRule(stars: 7, gear: 10, preference: 2).e,
        "DEFAULT" : TWRule(stars: 7, gear: 10).e,
    ]
        
    static let epAdds = u("Dooku", "DT", "Krennic", "IPD", "TFP", "Sith Trooper", "BSF", "Traya", "Sion", "DN")
    static let defenseSithSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "zEP", id: "EP", preference: 6, rules: epRules), u("Palpatine"), u("Darth Vader"), u("Tarkin"), epAdds, epAdds)
    
    static let epRules = [
        "Palpatine" : TWRule(leaderZeta: true).e,
        "Vader" : TWRule(preference: 1).e,
        "Tarkin" : TWRule(preference: 1).e,
        "TFP" : TWRule(preference: 1).e,
        "IPD" : TWRule(preference: 1).e,
        
        // prefer dooku for separatists
        "Dooku" : TWRule(preference: -5).e,
        "DEFAULT" : TWRule().e,
    ]

    static let wiggsDefenseBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Wiggs", id: "REBELS_WIGGS", preference: 2, rules: defenseRules), u("Wedge"), u("Biggs"), u("Leia", "Fulcrum"), u("Chirrut"), u("Baze"))

    static let standardPreference1Rules = UnitEvaluator(
        UnitRule.gear10PlusRule,
        UnitRule.level85Rule,
        UnitRule.sevenStarRule,
        
        CustomRule() { unit in
            var preference = 0
            if unit.gear >= 10 {
                preference = 1
            }
            return Message(message: "unit boost", status: .ok, score: 0, max: 0, preference: preference)
        }
    )

    static let defenseRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        
        "COMMANDERLUKESKYWALKER" : UnitEvaluator(
            OmegaZetaAbilityRule("leaderskill_COMMANDERLUKESKYWALKER", status: .needsRequired),
//          OmegaZetaAbilityRule("uniqueskill_COMMANDERLUKESKYWALKER02", status: .needsRequired),

            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        
        "JEDIKNIGHTREVAN" : UnitEvaluator(
            OmegaZetaAbilityRule("leaderskill_JEDIKNIGHTREVAN", status: .needsRequired),

            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            
            PreferenceRule(preference: 5)
        ),
        "BASTILASHAN" : standardPreference1Rules,
        "GRANDMASTERYODA" : standardPreference1Rules,
        "HERMITYODA" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            
            PreferenceRule(preference: 3)
        ),
        "GENERALKENOBI" : standardPreference1Rules,
        "BARRISSOFFEE" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            
            OmegaZetaAbilityRule("uniqueskill_BARRISSOFFEE01", status: .needsRequired),
            
            PreferenceRule(preference: 1)
        ),
        "BOSSK" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            
            OmegaZetaAbilityRule("leaderskill_BOSSK", status: .needsRequired),
            
            PreferenceRule(preference: 2)
        ),
        "EMBO" : TWRule(leaderZeta: true, preference: 2).e,
        "DENGAR" : standardPreference1Rules,
        "JANGOFETT" : standardPreference1Rules,
        "BOBAFETT" : standardPreference1Rules,
        "EMPERORPALPATINE" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            
            OmegaZetaAbilityRule("leaderskill_EMPERORPALPATINE", status: .needsRequired),
            
            PreferenceRule(preference: 1)
        ),
        "CHEWBACCALEGENDARY" : standardPreference1Rules,
        "C3POLEGENDARY" : UnitEvaluator(
            // c3po is a bit of a glass, well, not cannon, but glass -- require him to be tough
            UnitRule(.gear, atLeast: 9, target: 12),
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            
            PreferenceRule(preference: 1)
        ),
        "KYLORENUNMASKED" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,

            OmegaZetaAbilityRule("leaderskill_KYLORENUNMASKED", status: .needsRequired),

            PreferenceRule(preference: 1)
        ),
        "FIRSTORDEREXECUTIONER" : standardPreference1Rules,
        "CT5555" : standardPreference1Rules,
        
        // IG is a little squishy, so de-emphasize him
        "IG-88" : TWRule(preference: -1).e,
        
        "ENFYSNEST" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule(.stars, atLeast: 5, target: 7)
        ),

    ]
    
    static let defenseSithRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        
        "Malak" : TWRule(stars: 5, gear: 10, preference: 5).e,
        
        "EMPERORPALPATINE" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            
            OmegaZetaAbilityRule("leaderskill_EMPERORPALPATINE", status: .needsRequired),
            
            PreferenceRule(preference: 1)
        ),
        "BASTILASHANDARK" : UnitEvaluator(
            UnitRule.gear9PlusRule,
            UnitRule.level85Rule,
            
            CustomRule() { unit in
                var preference = 0
                if unit.stars > 6 && unit.gear >= 9 {
                    preference = 2
                }
                return Message(message: "unit boost", status: .ok, score: 0, max: 0, preference: preference)
            }
        ),
        "DARTHTRAYA" : standardPreference1Rules,
        "DARTHSION" : standardPreference1Rules,
        "DARTHNIHILUS" : standardPreference1Rules,
        "SITHTROOPER" : standardPreference1Rules,

    ]
    
    // this is meant for offense and is quite broad -- low priority
    static let zmaulAdds = FilteredOneOfUnitSelect("BSF", "Dooku", "DN", "DR", "Darth Sidious", "Sith Assassin", "Savage", "Sith Empire Trooper", "Sith Marauder")
    static let zetaMaulBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "zMaul", id: "SITH_MAUL", preference: 0, rules: zetaMaulRules), u("MAUL"), zmaulAdds, zmaulAdds, zmaulAdds, zmaulAdds)

    static let zetaMaulRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "MAUL" : TWRule(leaderZeta: true).e,
        "Savage Opress" : TWRule(preference: 1).e,
        "Sith Empire Trooper" : TWRule(preference: 1).e,
        "Dooku" : TWRule(preference: 1).e,
        "Sith Assassin" : TWRule(preference: 1).e,

        ]

    static let ewokAdds = u("Teebo", "Ewok Scout", "Wicket", "Logray")
    static let ewokBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Ewoks", id: "EWOKS", preference: 10, rules: ewokRules), u("CHIEFCHIRPA"), u("Elder"), u("Paploo"), ewokAdds, ewokAdds)
    static let ewokC3poBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Ewoks+C3PO", id: "EWOKS_C3PO", preference: -10, rules: ewokRules), u("CHIEFCHIRPA"), u("Elder"), u("Paploo"), u("Wicket"), u("C-3PO"))

    static let ewokRules = [
        "DEFAULT" : TWRule().e,
        "Chirpa" : TWRule(leaderZeta: true, preference: 1).e,
        "Wicket" : TWRule(preference: 1).e,
        "Elder" : TWRule(preference: 1).e,
        "Paploo" : TWRule(preference: 1).e,
        "Logray" : TWRule(preference: 1).e,
    ]
    
    static let nsAdds = Model.Character.matchesCategory("Nightsister").map(\.id).removing("NIGHTSISTERACOLYTE", "NIGHTSISTERINITIATE", "ASAJVENTRESS", "MOTHERTALZIN")

    static let nsBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "NS_Alt", id: "NS_ASAJJ", preference: 14, rules: nsRulesAVLead), u("ASAJVENTRESS"), u(nsAdds), u(nsAdds), u(nsAdds), u(nsAdds))
    static let ns2Builder = SelectUnitSquadBuilder(properties: TWProperties(name: "NS_Alt", id: "NS_MT", preference: 15, rules: nsRulesMTLead), u("MOTHERTALZIN"), u("ASAJVENTRESS"), u(nsAdds), u(nsAdds), u(nsAdds))
    static let ns3Builder = SelectUnitSquadBuilder(properties: TWProperties(name: "NS_Alt", id: "NS_MT_NA", preference: 10, rules: nsRulesMTLead), u("MOTHERTALZIN"), u("Acolyte"), u("Daka"), u("Asajj"), u("Zombie"))
    static let nsIdealBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "NS", id: "NS_MT", preference: 18, rules: nsRulesMTLead), u("MOTHERTALZIN"), u("ASAJVENTRESS"), u("Daka"), u("Zombie"), u("Spirit"))

    static let nestSistersBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "NestSisters", id: "NS_NEST", preference: 15, rules: nsRulesAVLead), u("ASAJJ"), u("MOTHERTALZIN"), u("DAKA"), u("Zombie"), u("ENFYSNEST"))
    
    static let nsRulesAVLead = [
        // MT doesn't need her lead
        "Talzin" : TWRule(gear: 11, preference: 5).e,
        "Asajj" : TWRule(leaderZeta: true, uniqueZetas: true, gear: 11, preference: 5).e,
        "Daka" : TWRule(gear: 10, preference: 3).e,
        "Zombie" : TWRule(gear: 10, preference: 3).e,
        "Spirit" : TWRule(gear: 11, preference: 3).e,
        "Acolyte" : TWRule(gear: 10, preference: 1).e,
        "Nest" : TWRule(uniqueZetas: true, stars: 7, gear: 11, preference: 3).e,
        "Talia" : TWRule(gear: 10, preference: -1).e,
        "NIGHTSISTERINITIATE" : TWRule(gear: 10, preference: -2).e,
    ]
    
    static let nsRulesMTLead = [
        "Talzin" : TWRule(leaderZeta: true, gear: 11, preference: 5).e,
        // Asajj doesn't need her lead
        "Asajj" : TWRule(uniqueZetas: true, gear: 11, preference: 5).e,
        "Daka" : TWRule(gear: 10, preference: 3).e,
        "Zombie" : TWRule(gear: 10, preference: 3).e,
        "Spirit" : TWRule(gear: 11, preference: 3).e,
        "Acolyte" : TWRule(gear: 10, preference: 1).e,
        "Nest" : TWRule(uniqueZetas: true, stars: 7, gear: 11, preference: 3).e,
        "Talia" : TWRule(gear: 10, preference: -1).e,
        "NIGHTSISTERINITIATE" : TWRule(gear: 10, preference: -2).e,
    ]

    
    static let grievousAdds = u("B1BATTLEDROIDV2", "DROIDEKA", "MAGNAGUARD", "Dooku", "IG-88", "IPD", "T3-M4", "L3-37")
    static let grievousBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Grievous", id: "SEP_DROIDS", preference: 20, rules: grievousRules), u("GRIEVOUS"), u("B2SUPERBATTLEDROID"), grievousAdds, grievousAdds, grievousAdds)
    static let grievousBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Grievous-Wat", id: "SEP_DROIDS", preference: 21, rules: grievousRules), u("GRIEVOUS"), u("B2SUPERBATTLEDROID"), u("B1"), u("Magna"), u("Wat Tambor"))
    
    /// the preferred form
    static let grievousBuilder3 = SelectUnitSquadBuilder(properties: TWProperties(name: "Grievous", id: "SEP_DROIDS", preference: 22, rules: grievousRules2), u("GRIEVOUS"), u("B2SUPERBATTLEDROID"), u("B1BATTLEDROIDV2"), u("MAGNAGUARD"), u("DROIDEKA", "Nute"))

    static let grievousRules = [
        "DEFAULT" : TWRule().e,
        "GRIEVOUS" : TWRule(leaderZeta: true).e,
        "B1BATTLEDROIDV2" : TWRule(preference: 1).e,
        "B2SUPERBATTLEDROID" : TWRule(preference: 1).e,
        "DROIDEKA" : TWRule(preference: 1).e,
        "MAGNAGUARD" : TWRule(preference: 1).e,
        "Wat Tambor" : TWRule(uniqueZetas: true, stars:5, gear: 10).e,
        "BB8" : TWRule().e,
        ]
    
    static let grievousRules2 = [
        "DEFAULT" : TWRule(gear: 12).e,
        "GRIEVOUS" : TWRule(leaderZeta: true, gear: 12).e,
        "B1BATTLEDROIDV2" : TWRule(gear: 11, preference: 1).e,
        "B2SUPERBATTLEDROID" : TWRule(gear: 11, preference: 1).e,
        "MAGNAGUARD" : TWRule(gear: 11, preference: 1).e,
        "Nute" : TWRule(uniqueZetas:true, gear: 11, preference: 1).e,
        ]

    
    // TODO: NO COUNTER
    // from zynix: Nute, B2, B1, Magna, and Wat
    static let nuteBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Nute-Wat", preference: 20, rules: nuteRules), u("Nute"), u("B1"), u("B2"), u("Magnaguard"), u("Wat Tambor"))
    
    static let nuteBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Nute-Wat", preference: 20, rules: nuteRules), u("Nute"), u("Dooku"), u("Wat Tambor"), u("Jango"), u("Nest"))

    static let nuteBuilder3 = SelectUnitSquadBuilder(properties: TWProperties(name: "Nute-OR", preference: 19, rules: nuteRules), u("Nute"), u("Dooku"), u("Jango"), u("Zaalbar"), u("Mission"))
    

    static let nuteRules = [
        "DEFAULT" : TWRule().e,
        "Nute" : TWRule(uniqueZetas: true).e,
        "Wat Tambor" : TWRule(uniqueZetas: true, stars:4, gear: 9).e,
        "Jango Fett" : TWRule(stars:7, gear: 10).e,
        "Zaalbar" : TWRule(uniqueZetas: true).e,
        ]

    
    static let carthBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Carth", id: "OR_CARTH", preference: 10, rules: carthRules), u("Carth"), u("Mission"), u("Zaalbar"), u("Juhani", "Vandor Chewbacca"), u("Canderous"))
    
    static let carthRules = [
        "DEFAULT" : TWRule().e,
        "L3-37" : TWRule(preference:1).e,
        "T3-M4" : TWRule(preference:1).e,
        "Canderous" : TWRule(preference:1).e,
        "Mission" : TWRule(preference:1).e,
        "Zaalbar" : TWRule(preference:1).e,
        "Wampa" : TWRule(preference:1).e,
        "Vandor Chewbacca" : TWRule(uniqueZetas: true, preference: 1).e,
    ]

    // padme
    static let padmeBuilder1 = SelectUnitSquadBuilder(properties: TWProperties(name: "padme", id: "GALACTIC_REPUBLIC", preference: 30, rules: padmeRules), u("PADMEAMIDALA"), u("GENERALKENOBI"), u("ANAKINKNIGHT"), u("AHSOKATANO"), u("BARRISSOFFEE", "Clone Wars Chewbacca", "CT-5555"))
    static let padmeBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "padme", attackerId: "GALACTIC_REPUBLIC_C3PO", defenderId: "GALACTIC_REPUBLIC",  preference: 30, rules: padmeRules), u("PADMEAMIDALA"), u("GENERALKENOBI"), u("ANAKINKNIGHT"), u("AHSOKATANO"), u("C3POLEGENDARY"))
    
    public static let padmeBuilder = MultipleSquadBuilder(padmeBuilder1, padmeBuilder2)

    static let padmeRules = [
        "PADMEAMIDALA" : TWRule(leaderZeta: true, stars: 5, gear: 8).e,

        "ANAKINKNIGHT" : TWRule(preference: 1).e,
        "AHSOKATANO" : TWRule(preference: 1).e,
        "GK" : TWRule(preference: 1).e,
        "BARRISSOFFEE" : TWRule(uniqueZetas: true, preference: 1).e,
        "C3POLEGENDARY" : TWRule(preference: 1).e,
        
        // we prefer him to be part of the clones
        "CT-5555" : TWRule(preference: -30).e,

        "DEFAULT" : TWRule().e,
        
        ]
    
    static let rogue1Extras = Model.Character.matchesCategory("Rogue One").map(\.id).removing("JYNERSO", "CHIRRUTIMWE", "BAZEMALBUS")

    static let rogue1Builder = SelectUnitSquadBuilder(properties: TWProperties(name: "Rogue1", id: "ROGUE_ONE", preference: 1, rules: rogue1Rules), u("Jyn Erso"), u("Chirrut"), u("Baze"), u(rogue1Extras), u(rogue1Extras))
    
    static let rogue1Rules = [
        "DEFAULT" : TWRule(gear: 8).e,
        "Cassian" : TWRule(gear: 8, preference: 1).e,
        "K-2SO" : TWRule(gear: 8, preference: 1).e,
        ]
    
    static let geonosianBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Geonosians", id: "GEONOSIANS", preference: 25, rules: geonosianRules), u("GEONOSIANBROODALPHA"), u("SUNFAC"), u("GEONOSIANSOLDIER"), u("GEONOSIANSPY"), u("Poggle"))
    
    static let geonosianRules = [
        "GEONOSIANBROODALPHA" : TWRule(leaderZeta: true, uniqueZetas: false, stars: 5, gear: 9, preference: 2).e,

        "DEFAULT" : UnitEvaluator(
            UnitRule.gear9PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        
        ]
    
    static let clones5 = u("CLONESERGEANTPHASEI", "ARCTROOPER501ST")
    static let clonesBuilder1 = SelectUnitSquadBuilder(properties: TWProperties(name: "Clones", id: "CLONES_CODY", preference: 5, rules: clonesRules), u("CC2224"), u("CT7567"), u("CT210408"), u("CT5555"), clones5)
    static let clonesBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "ShaakClones", id: "CLONES_SHAAKTI", preference: 12, rules: clonesRules), u("SHAAKTI"), u("CT7567"), u("CT210408"), u("CT5555"), clones5)
    static let clonesBuilder3 = SelectUnitSquadBuilder(properties: TWProperties(name: "501st", id: "REX_501", preference: 6, rules: clonesRules), u("CT7567"), clones5, u("CT210408"), u("CT5555"), u("Ahsoka Tano"))
    public static let gasBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "GAS", id: "501ST", preference: 15, rules: clonesRules), u("GAS"), u("CT7567"), u("CT210408"), u("CT5555"), u("Ahsoka Tano", "ARCTROOPER501ST"))

    static let clonesRules = [
        "SHAAKTI" : TWRule(leaderZeta: true, stars: 6, gear: 9, preference: 1).e,
        "GAS" : TWRule(leaderZeta: true, uniqueZetas: true, stars: 7, gear: 12, preference: 2).e,
        "ARCTROOPER501ST" : TWRule(preference: 1).e,
        "DEFAULT" : TWRule(gear: 10).e
    ]

    
    // MARK: - Offense
    
    static let imperialTroopers = Model.Character.matchesCategory("Imperial Trooper").map(\.id)

    static let troopersBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Troopers", id: "IT", preference: 5, rules: offenseRules), u("Veers"), u("Starck"), u(imperialTroopers), u(imperialTroopers), u(imperialTroopers))
    static let troopersBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "ThrawnTroopers", id: "IT_THRAWN", preference: 0, rules: offenseRules), u("Thrawn"), u("Range Trooper"), u("Death Trooper"), u("Shoretrooper"), u(imperialTroopers))

    static let wiggsAdds = u("CLS", "Han Solo", "Chewbacca", "Fulcrum", "Ezra", "C-3PO", "Princess Leia")
    static let wiggsBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Wiggs", id: "REBELS_DANGER_ZONE", preference: 5, rules: offenseRules), u("Wedge"), u("Biggs"), wiggsAdds, wiggsAdds, wiggsAdds)

    static let jtrBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "JTR", id: "JTR", preference: 22, rules: jtrRules), u("REYJEDITRAINING"), u("REY", "C-3PO", "Poe"), u("BB8"), u("R2D2_LEGENDARY"), u("RESISTANCETROOPER", "Finn"))
    static let jtrBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "JTR+droids", id: "JTR_DROIDS", preference: -5, rules: jtrRules), u("REYJEDITRAINING"), u("C-3PO"), u("BB8"), u("R2D2_LEGENDARY"), u("RESISTANCETROOPER", "Chopper", "L3-37", "T3-M4", "IG-88"))
    static let jtrBuilder3 = SelectUnitSquadBuilder(properties: TWProperties(name: "JTR", id: "JTR", preference: 27, rules: jtrRules), u("REYJEDITRAINING"), u("R2D2_LEGENDARY"), u("BB8"), u("Resistance Hero Finn"), u("Resistance Hero Poe"))

    static let jtrRules = [
        "REYJEDITRAINING" : TWRule(leaderZeta: true).e,
        "REY" : TWRule(preference: 1).e,

        "DEFAULT" : TWRule().e,
    ]

    static let magmaAdds = u("Death trooper", "Director Krennic", "Tarkin", "Stormtrooper", "Shoretrooper")
    static let magmaBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Thragma (Counter)", id: "MAGMA", preference: 5, rules: offenseRules), u("Thrawn"), u("MAGMATROOPER"), magmaAdds, magmaAdds, magmaAdds)

    static let rexBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Rex (Counter)", id: "REX", preference: 5, rules: offenseRules), u("Rex"), u("Wampa", "Nest", "Nihilus"), u("Chirrut"), u("Baze"), u("R2-D2"))

    static let wampaAdds = u("Death trooper", "Director Krennic", "Visas Marr", "Hermit Yoda", "Jyn", "FOO")
    static let wampaBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Wampa (Counter)", id: "REX_WAMPA", preference: 5, rules: offenseRules), u("Rex"), u("Wampa"), wampaAdds, wampaAdds, wampaAdds)

    static let qgjAdds = u("General Kenobi", "Old Ben", "Kanan", "Hermit Yoda")
    static let qgjBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "QGJ", id: "JEDI_QGJ", preference: 1, rules: offenseRules), u("QGJ"), u("Aayla"), u("Jedi Knight Anakin"), qgjAdds, u("Ezra Bridger", "Ahsoka Tano"))

    static let offenseRules = [
        // JTR
        "REYJEDITRAINING" : UnitEvaluator(
            OmegaZetaAbilityRule("leaderskill_REYJEDITRAINING", status: .needsRequiredGear),
            OmegaZetaAbilityRule("uniqueskill_REYJEDITRAINING02", status: .needsTuning),
            
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        
        // Imperial Troopers
        "VEERS" : UnitEvaluator(
            UnitRule.gear9PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,

            OmegaZetaAbilityRule("uniqueskill_VEERS01", status: .needsRequired)
        ),
        "RANGETROOPER" : UnitEvaluator(
            UnitRule.gear9PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            PreferenceRule(preference: 1)
        ),
        "MAGMATROOPER" : UnitEvaluator(
            UnitRule.gear9PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            PreferenceRule(preference: -1)
        ),
        
        "DEFAULT" : UnitEvaluator(
            UnitRule.gear9PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
    ]
    
    // MARK: - Counters
    // for https://github.com/bobbybaxter/swgoh-counters
    
    static let sionSoloBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "SionSolo (Counter)", id: "SION_SOLO", preference: -10, rules: counterRules), u("Sion"))
    static let nestSoloBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "NestSolo (Counter)", id: "NEST_SOLO", preference: -10, rules: counterRules), u("Nest"))
    static let jawaBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Jawas", id: "JAWAS", preference: -10, rules: counterRules), u("Nebit"), u("Jawa Engineer"), u("Jawa Scavenger"), u("Jawa"), u("DATHCHA"))
    static let jtrHoldoBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "JTR Holdo (Counter)", id: "JTR_HOLDO", preference: -10, rules: counterRules), u("JTR"), u("BB-8"), u("R2-D2"), u("C-3PO"), u("Holdo"))
    
    static let qiraNestHodoBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "QiraNest", id: "QIRA_NEST_HODA", preference: -10, rules: counterRules), u("QIRA"), u("Nest"), u("Hermit Yoda"))
    static let qiraNestHodoBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "QiraNest", id: "QIRA_NEST_HODA_FULL", preference: -10, rules: counterRules), u("QIRA"), u("Nest"), u("Vandor Chewbacca"), u("L3-37"), u("Hermit Yoda"))
    
    static let ig88Builder = SelectUnitSquadBuilder(properties: TWProperties(name: "IG88", id: "IG-88_HI", preference: -10, rules: counterRules), u("IG-88"), u("Wampa"), u("Cassian Andor"), u("Boba Fett"), u("Kylo Ren"))
    static let droidsAntiMalakBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "IG88", id: "DROIDS_ANTI_MALAK", preference: -10, rules: counterRules), u("IG-88"), u("Grievous"), u("BB-8"), u("T3-M4"), u("L3-37"))


    static let counterRules = [
        "DEFAULT" : TWRule().e,
    ]
    
    static let zaderRules = [
        "DEFAULT" : TWRule().e,
        "Darth Vader" : TWRule(leaderZeta: true).e,
        "Wampa" : TWRule(preference: 1).e,
        "Tarkin" : TWRule(preference: 1).e,
        "Darth Sidious" : TWRule(preference: 1).e,
        "Boba Fett" : TWRule(preference: 1).e,
    ]

    static let epAntiRebelAdds = u("Shoretrooper", "Krennic", "Death Trooper", "Wampa", "Nest", "Range Trooper", "Stormtrooper", "Starck")
    static let epAntiRebelBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "EP (Counter, Rebel)", id: "EP_ANTI_REBEL", preference: -10, rules: epAntiRebelRules), u("Palpatine"), epAntiRebelAdds, epAntiRebelAdds, epAntiRebelAdds, epAntiRebelAdds)
    
    static let epAntiRebelRules = [
        "DEFAULT" : TWRule().e,
        "Shoretrooper" : TWRule(preference: 1).e,
        "Krennic" : TWRule(preference: 1).e,
        "Death Trooper" : TWRule(preference: 1).e,
        "Wampa" : TWRule(preference: 1).e,
    ]
    
    static let epTrioBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "EP (trio)", id: "EP_TRIO", preference: -10, rules: counterRules), u("Palpatine"), u("Traya"), u("Nihilus"), u("Sion"), u("Thrawn"))

    static let epTrayaAdds = u("Vader", "BSF")
    static let epTrayaBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "EP (Traya)", id: "EP_TRAYA", preference: -10, rules: counterRules), u("Palpatine"), u("Traya"), u("Nihilus"), u("Sith Empire Trooper"), u("Vader", "BSF"))
    
    static let glRey1 = SelectUnitSquadBuilder(properties: TWProperties(name: "GL Rey", id: "REY", preference: 40, rules: glReyRules), u("Rey"), u("Holdo", "GK", "JTR"), u("R2-D2", "BB-8", "JTR"), u("Resistance Hero Poe", "Holdo", "L3"), u("Resistance Hero Finn"))
    
    static let glReyRules = [
        "DEFAULT" : TWRule(uniqueZetas: true, gear: 22).e,
        "Rey" : TWRule(leaderZeta: true, uniqueZetas: true, gear: 25).e,
    ]
    
    static let slKylo1 = SelectUnitSquadBuilder(properties: TWProperties(name: "SL Kylo", id: "FO_SLKR", preference: 40, rules: slKyloRules), u("SL Kylo"), u("FOSITHTROOPER"), u("Hux"), u("KRU"), u("Thrawn"))
    
    static let slKyloRules = [
        "DEFAULT" : TWRule().e,
        "SL Kylo" : TWRule(leaderZeta: true, uniqueZetas: true, gear: 10).e,
        "KRU" : TWRule(uniqueZetas: true, gear: 10).e,
    ]
    
    static let see1 = SelectUnitSquadBuilder(properties: TWProperties(name: "SEE", preference: 40, rules: seeRules), u("Sith Eternal Emperor"), u("BSF"), u("Malak"), u("DR"), u("Vader", "Dooku"))
    
    static let seeRules = [
        "DEFAULT" : TWRule().e,
        "Sith Eternal Emperor" : TWRule(leaderZeta: true, uniqueZetas: true, gear: 10).e,
    ]




    // MARK: - fleets
    
    // From zynix
    // So, the best fleet is Ackbar Capital, OGMF 5+, Houndstooth 7, Tie Silencer 7. Then, Phantom II + Ghost both 7, Cassian’s U-wing 7, Bigg’s or Wedge’s X-wing, in that order. It’s better if the toons associated with the ships are 7 + G12 and zetaed.
    // Some people put Bigg’s X-wing in place of the Tie Silencer, but, the Silencer goes first if KRU is geared/zetaed and can stun an opponent’s ship right away.
    //  I don’t have Cassian’s U-wing/Rogue One toons up to snuff so I substitute it with DV’s Tie.
    
    static let hansMFBuilder1 = SelectUnitSquadBuilder(properties: TWProperties(name: "HansMF", preference: 25, rules: hansmfRules), u("CAPITALMONCALAMARICRUISER"), u("MILLENNIUMFALCON"), u("HOUNDSTOOTH", "Rebel Y-wing", "Biggs Darklighter's X-wing"), u("Bistan's U-wing", "Ghost"), u("PHANTOM2"), u("GHOST", "Biggs Darklighter's X-wing"), u("UWINGROGUEONE"), u("XWINGRED3", "XWINGRED2"))


    // from zynix: Negotiator, HT, Anakin’s, Umbaran, Ahsoka’s + strongest 3 GR ships?
    static let grFleetAdds = u("BTL-B Y-wing Starfighter", "Clone Sergeant's ARC-170", "Jedi Consular's Starfighter", "Plo Koon's Jedi Starfighter", "Rex's ARC-170")
    static let negotiator1 = SelectUnitSquadBuilder(properties: TWProperties(name: "Negotiator", preference: 30, rules: negotiatorRules), u("Negotiator"), u("HOUNDSTOOTH"), u("JEDISTARFIGHTERANAKIN"), u("UMBARANSTARFIGHTER"), u("JEDISTARFIGHTERAHSOKATANO"), grFleetAdds, grFleetAdds, grFleetAdds)
    static let negotiator2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Negotiator", preference: 31, rules: negotiatorRules), u("Negotiator"), u("HOUNDSTOOTH"), u("JEDISTARFIGHTERANAKIN"), u("UMBARANSTARFIGHTER"), u("JEDISTARFIGHTERAHSOKATANO"), u("BTL-B Y-wing Starfighter"), u("Plo Koon's Jedi Starfighter"), u("Clone Sergeant's ARC-170"))
    
    public static let negotiatorBuilder = MultipleSquadBuilder(negotiator1, negotiator2)

    public static let malevolanceBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Malevolence", preference: 30, rules: malevolenceRules), u("Malevolence"), u("GEONOSIANSTARFIGHTER1"), u("GEONOSIANSTARFIGHTER2"), u("GEONOSIANSTARFIGHTER3"), u("HYENABOMBER"), u("VULTUREDROID"), u("IG2000"))

    static let fleet5Builder = SelectUnitSquadBuilder(properties: TWProperties(name: "Raddus", preference: 20, rules: raddusRules), u("Raddus"), u("GEONOSIANSTARFIGHTER1"), u("Rey's Millennium Falcon"), u("Poe Dameron's X-wing"), u("Resistance X-wing"), u("Biggs Darklighter's X-wing"), u("Wedge Antilles's X-wing"), u("PHANTOM2"), u("GHOST"))

    static let fleet6Builder = SelectUnitSquadBuilder(properties: TWProperties(name: "Executrix", preference: 20, rules: empireFleetRules), u("Executrix"), u("TIEADVANCED"), u("EMPERORSSHUTTLE"), u("TIEREAPER"), u("TIEFIGHTERIMPERIAL"), u("GAUNTLETSTARFIGHTER"), u("TIEFIGHTERFOSF"), u("TIEFIGHTERFIRSTORDER"))
    
    static let hansmfRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "Rebel Y-wing": UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule(.stars, atLeast: 6, target: 7),
            PreferenceRule(preference: 1)
        ),
        "TIESILENCER" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            PreferenceRule(preference: -1)
        ),
        "TIEADVANCED" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            PreferenceRule(preference: -1)
        ),
        "UWINGROGUEONE" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            PreferenceRule(preference: 1)
        ),
        "UWINGSCARIF" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            PreferenceRule(preference: 1)
        ),
        "MILLENNIUMFALCON" : UnitEvaluator(
            UnitRule(.stars, atLeast: 5, target: 7),
            UnitRule.level85Rule
        ),
        "HOUNDSTOOTH" : UnitEvaluator(
            UnitRule(.stars, atLeast: 6, target: 7),
            UnitRule.level85Rule
        ),
    ]
    
    static let negotiatorRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "Rex's ARC-170" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            PreferenceRule(preference: -2)
        ),
        "Plo Koon's Jedi Starfighter" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            PreferenceRule(preference: -1)
        ),
        "Jedi Consular's Starfighter" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule,
            PreferenceRule(preference: -2)
        ),
        "BTL-B Y-wing Starfighter" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule(.stars, atLeast: 6, target: 7),
            PreferenceRule(preference: 2)
        ),
        "Negotiator" : UnitEvaluator(
            UnitRule(.stars, atLeast: 5, target: 7),
            UnitRule.level85Rule
        ),
    ]

    static let malevolenceRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "Malevolence" : UnitEvaluator(
            UnitRule(.stars, atLeast: 5, target: 7),
            UnitRule.level85Rule
        ),
    ]

    static let raddusRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "Raddus" : UnitEvaluator(
            UnitRule(.stars, atLeast: 5, target: 7),
            UnitRule.level85Rule
        ),
    ]

    static let empireFleetRules = [
        "DEFAULT" : UnitEvaluator(
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
    ]
    
    static let cls = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", id: "REBELS_CLS", preference: 31, rules: clsRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("CHEWBACCALEGENDARY"), u("R2D2_LEGENDARY"), u("C3POLEGENDARY", "C3POCHEWBACCA", "Old Ben", "STORMTROOPERHAN"))
    
    static let cls_kotd = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", id: "REBELS_CLS", preference: 31, rules: kotd_clsRules), u("COMMANDERLUKESKYWALKER"), u("HANSOLO"), u("CHEWBACCALEGENDARY"), u("C3POCHEWBACCA"), u("Old Ben", "Cara Dune", "Princess Leia"))

    
    static let clsRules = [
        "DEFAULT" : TWRule().e,
        "COMMANDERLUKESKYWALKER" : TWRule(leaderZeta: true).e,
        "C3POCHEWBACCA" : TWRule(stars: 7, gear: 11, preference: 2).e,
        "Old Ben" : TWRule(gear: 12).e,
        "STORMTROOPERHAN" : TWRule(gear: 12).e,
        ]
    
    static let kotd_clsRules = [
        "DEFAULT" : TWRule().e,
        "COMMANDERLUKESKYWALKER" : TWRule(leaderZeta: true).e,
        "C3POCHEWBACCA" : TWRule(stars: 6, gear: 9, preference: 2).e,
        "Old Ben" : TWRule(gear: 21, preference: 4).e,
        "STORMTROOPERHAN" : TWRule(gear: 21, preference: 3).e,
        "Cara Dune" : TWRule(gear: 21, preference: 2).e,
        "Princess Leia" : TWRule(gear: 21, preference: 1).e,
        ]

    
    
    // MARK: - KOTD from Antares
    
    static let cloneTrooperIds = Model.Character.matchesCategory("Clone Trooper").map(\.id)

    static let grAddIds = Model.Character.matchesCategory("Galactic Republic").map(\.id).removing("GENERALSKYWALKER").removing(cloneTrooperIds)
    static let grAdds = FilteredOneOfUnitSelect(grAddIds)

    static let kotd_padme = SelectUnitSquadBuilder(properties: TWProperties(name: "padme-gr", id: "GALACTIC_REPUBLIC", preference: 20, rules: kotd_padmeRules), u("PADMEAMIDALA"), grAdds, grAdds, grAdds, grAdds)
    
    static let kotd_padmeRules = [
        "DEFAULT" : TWRule(gear: 12).e,
        "Padme": TWRule(leaderZeta: true, uniqueZetas: true, gear: 12).e,
        ]

    

    
    // T1 - Grevious + BB8 (GG, B1, B2, bb8, Magma)
    // B1 - Grevious + WAT (GG, B1, B2, Wat, Magma)
    static let kotd_gg = SelectUnitSquadBuilder(properties: TWProperties(name: "Grievous", id: "SEP_DROIDS", preference: 30, rules: kotd_grievousRules), u("GRIEVOUS"), u("B1"), u("B2"), u("Wat", "Nute", "Droideka"), u("Magna"))
    
    static let kotd_grievousRules = [
        "DEFAULT" : TWRule().e,
        "GRIEVOUS" : TWRule(leaderZeta: true).e,
        "B1BATTLEDROIDV2" : TWRule(preference: 1).e,
        "B2SUPERBATTLEDROID" : TWRule(preference: 1).e,
        "MAGNAGUARD" : TWRule(preference: 1).e,
        "Wat Tambor" : TWRule(uniqueZetas: true, stars:6, gear: 11, preference: -1).e,
        "BB8" : TWRule(uniqueZetas: false, gear: 12).e,
        ]

    static let jkl = SelectUnitSquadBuilder(properties: TWProperties(name: "JKL", preference: 30, rules: jklRules), u("JKL"), u("JKR"), u("Hermit Yoda"), u("Old Ben"), u("Barriss", "Ezra", "Aayla Secura"))
    
    static let jmls1 = SelectUnitSquadBuilder(properties: TWProperties(name: "JMLS", preference: 34, rules: jklRules), u("JMLS"), u("JKL"), u("Hermit Yoda"), u("Old Ben"), u("Ezra"))
    static let jmls2 = SelectUnitSquadBuilder(properties: TWProperties(name: "JMLS", preference: 35, rules: jklRules), u("JKR"), u("JKL"), u("JMLS"), u("Hyoda", "WAT"), u("GAS"))
    static let jml3s = SelectUnitSquadBuilder(properties: TWProperties(name: "JMLS", preference: 35, rules: jklRules), u("Bastila Shan"), u("JMLS"), u("Hyoda", "WAT"), u("JKL", "Hermit Yoda"), u("Barriss", "Jolee"))
    static let jmls = MultipleSquadBuilder(jmls1, jmls2, jml3s)
    
    static let jklRules = [
        "DEFAULT" : TWRule(gear: 21).e,
        "Old Ben" : TWRule(gear: 21).e,
        "Barriss" : TWRule(uniqueZetas: true, gear: 21).e,
        "Jolee" : TWRule(uniqueZetas: true, gear: 21).e,
        "Ezra" : TWRule(gear: 21).e,
        "Bastila Shan" : TWRule(leaderZeta: true, gear: 21).e,
        "Wat" : TWRule(uniqueZetas: true, stars: 5, gear: 8).e,
        "JMLS" : TWRule(leaderZeta: true, uniqueZetas: true, gear: 24).e,
        ]

    static let rebelFighters = Model.Character.matchesCategory("Rebel Fighter").map(\.id)

    static let monMothma = SelectUnitSquadBuilder(properties: TWProperties(name: "Mon Mothma", preference: 15, rules: monMothmaRules), u("Mon Mothma"), u(rebelFighters), u(rebelFighters), u(rebelFighters), u(rebelFighters))

    static let monMothmaRules = [
        "DEFAULT" : TWRule(gear: 10).e,
        "Mon Mothma" : TWRule(stars: 7, gear: 12).e,
        "Chirrut" : TWRule(gear: 10, preference: 1).e,
        "Baze" : TWRule(gear: 10, preference: 1).e,
        "Wedge" : TWRule(gear: 10, preference: 1).e,
        "Biggs" : TWRule(gear: 10, preference: 1).e,
        "Pao" : TWRule(gear: 10, preference: 2).e,
        "Bistan" : TWRule(gear: 10, preference: 1).e,
        "Hoth Rebel Scout" : TWRule(gear: 10, preference: 2).e,
        "Cara Dune" : TWRule(gear: 10, preference: 2).e,
        "Scarif Rebel Pathfinder" : TWRule(gear: 10, preference: 2).e,
        ]

    static let padmeShaak = SelectUnitSquadBuilder(properties: TWProperties(name: "padme", id: "GALACTIC_REPUBLIC", preference: 30, rules: padmeRules), u("PADMEAMIDALA"), u("GENERALKENOBI"), u("ANAKINKNIGHT"), u("AHSOKATANO"), u("SHAAKTI", "BARRISSOFFEE", "C3POLEGENDARY"))
    
    static let nuteSeps = SelectUnitSquadBuilder(properties: TWProperties(name: "Nute-Sep", preference: 10, rules: nuteRules), u("Nute"), u("Dooku"), u("Jango"), u("Nest"), u("Droideka"))

    static let kotd_nsAVLead = SelectUnitSquadBuilder(properties: TWProperties(name: "NS", id: "NS_MT", preference: 30, rules: kotd_nsRulesAVLead), u("Asajj"), u("MT"), u("Daka"), u("Zombie"), u("Spirit", "Talia"))
    
    static let kotd_nsRulesAVLead = [
        // MT doesn't need her lead
        "Talzin" : TWRule(gear: 11, preference: 5).e,
        "Asajj" : TWRule(leaderZeta: true, uniqueZetas: true, gear: 11, preference: 5).e,
        "Daka" : TWRule(gear: 10, maximumGear: 24, preference: 3).e,
        "Zombie" : TWRule(gear: 10, preference: 3).e,
        "Spirit" : TWRule(gear: 11, preference: 3).e,
        "Acolyte" : TWRule(gear: 10, preference: 1).e,
        "Nest" : TWRule(uniqueZetas: true, stars: 7, gear: 11, preference: 3).e,
        "Talia" : TWRule(gear: 10, preference: -1).e,
        "NIGHTSISTERINITIATE" : TWRule(gear: 10, preference: -2).e,
    ]
    
    static let kotd_rey = SelectUnitSquadBuilder(properties: TWProperties(name: "GL Rey", id: "REY", preference: 40, rules: glReyRules), u("Rey"), u("Poe"), u("Holdo"), u("Veteran Smuggler Han Solo"), u("Veteran Smuggler Chewbacca"))
    
    static let kotd_fo = SelectUnitSquadBuilder(properties: TWProperties(name: "FO", id: "FO_KRU", rules: foRules), u("KYLORENUNMASKED"), u("KYLOREN"), u("FOE"), u("FOO"), u("FOST"))

    static let kotd_slKylo1 = SelectUnitSquadBuilder(properties: TWProperties(name: "SL Kylo", id: "FO_SLKR", preference: 40, rules: slKyloRules), u("SL Kylo"), u("FOSITHTROOPER"), u("Hux"), u("Phasma"), u("SF Tie Pilot"))
    
    static let jmk = SelectUnitSquadBuilder(properties: TWProperties(name: "JMK", preference: 60, rules: jmkRules), u("JMK"), u("JKA"), u("GMY", "KIADIMUNDI", "C-3PO"), u("Ahsoka", "Shaak Ti"), u("GK"))
    
    static let jmkRules = [
        "DEFAULT" : TWRule(gear: 25).e,
    ]
    
    // MARK: - Counters
    
    static let jkl_vs_slkr = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR Counter: JKL", preference: 30, rules: vs_slkr_rules), u("JKR"), u("JKL"), u("Old Ben"), u("Bastila Shan"), u("Hermit Yoda"))
    
    static let gas_vs_slkr = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR Counter: GAS", rules: vs_slkr_rules), u("GAS"), u("Fives"), u("Han Solo"), u("Chewbacca"))
    
    static let slkr_vs_slkr = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR Counter: SLKR", rules: vs_slkr_rules), u("SL Kylo"), u("Hux"), u("KRU"), u("FOST"), u("Sith Trooper"))

    static let slkr_vs_rey = SelectUnitSquadBuilder(properties: TWProperties(name: "Rey Counter: SLKR", rules: vs_slkr_rules), u("SL Kylo"), u("Hux"), u("KRU"), u("FOST"), u("Sith Trooper"))
    static let see_vs_rey = SelectUnitSquadBuilder(properties: TWProperties(name: "Rey Counter: SEE", rules: vs_slkr_rules), u("SEE"), u("Wat"))
    static let dr_vs_rey = SelectUnitSquadBuilder(properties: TWProperties(name: "Rey Counter: DR", rules: vs_slkr_rules), u("Darth Revan"), u("Bastila Shan (Fallen)"), u("Wat"), u("Thrawn"), u("GBA"))
    static let vader_vs_rey = SelectUnitSquadBuilder(properties: TWProperties(name: "Rey Counter: Vader", rules: vs_slkr_rules), u("Darth Vader"), u("Bastila Shan (Fallen)"), u("Wat"), u("Thrawn"), u("Traya", "IPD"))
    static let jkl_vs_rey = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR Counter: JKL", rules: vs_slkr_rules), u("JKR"), u("JKL"), u("GAS"), u("Bastila Shan"), u("Hermit Yoda"))


    static let traya_vs_geos = SelectUnitSquadBuilder(properties: TWProperties(name: "Geos Counter: Traya", preference: 5, rules: vs_geos_rules), u("Traya"), u("Nihilus"), u("Sion"))
    static let traya_jawas_vs_geos = SelectUnitSquadBuilder(properties: TWProperties(name: "Geos Counter: Traya", rules: vs_geos_rules_traya_jawas), u("Traya"), u("Sion"), u("Jawa Scavenger"), u("Chief Nebit"), u("Jawa Engineer"))
    static let jawas_vs_geos = SelectUnitSquadBuilder(properties: TWProperties(name: "Geos Counter: Jawas", rules: vs_geos_rules_jawas), u("Chief Nebit"), u("Jawa Scavenger"), u("Jawa Engineer"), u("Jawa"), u("Dathcha"))
    
    static let jtr_vs_bh_adds = u(Model.Character.matchesCategory("Resistance").map(\.id))
    static let jtr_vs_bh = SelectUnitSquadBuilder(properties: TWProperties(name: "BH Counter: JTR", rules: jtr_vs_bh_rules), u("JTR"), u("BB-8"), jtr_vs_bh_adds, jtr_vs_bh_adds, jtr_vs_bh_adds)
    
    // JML counter - https://www.youtube.com/watch?v=tVDrY1_fvGg
    static let gg_vs_jml = SelectUnitSquadBuilder(properties: TWProperties(name: "JMLS Counter: GG+Nute", preference: 30, rules: vs_slkr_rules), u("GG"), u("B2"), u("B1"), u("Wat"), u("Nute"))

    // Ahnald Non GL counters: https://www.youtube.com/watch?v=r1jtWeJdQrk&feature=youtu.be
    static let rex_vs_slkr = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR Counter: Rex", rules: generic_gl_counters), u("Rex"), u("Fives"), u("CT210408"), u("Arc Trooper"), u("Barriss"))
    static let thrawn_vs_slkr = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR Counter: Thrawn+Stormtrooper", rules: generic_gl_counters), u("Thrawn"), u("Traya"), u("Vader"), u("Stormtrooper"), u("Wat"))
    static let see_vs_slkr = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR Counter: SEE", rules: generic_gl_counters), u("Sith Eternal Emperor"), u("The Armorer"), u("Sith Empire Trooper"))

    static let vader_no_wat_vs_rey = SelectUnitSquadBuilder(properties: TWProperties(name: "Rey Counter: Vader+Gideon", rules: generic_gl_counters), u("Vader"), u("Piett"), u("Moff Gideon"), u("Thrawn"), u("Shoretrooper"))

    static let zep_vs_jmls = SelectUnitSquadBuilder(properties: TWProperties(name: "JMLS Counter: EP+DN", rules: generic_gl_counters), u("EP"), u("DN"), u("Traya"), u("BSF"), u("DR"))
    static let dr_vs_jmls = SelectUnitSquadBuilder(properties: TWProperties(name: "JMLS Counter: DR", rules: generic_gl_counters), u("DR"), u("BSF"), u("Wat"))
    
    static let troopers_vs_see = SelectUnitSquadBuilder(properties: TWProperties(name: "SEE Counter: Troopers", rules: generic_gl_counters), u("Veers"), u("Piett"), u("Starck"), u("Snowtrooper"), u("Death Trooper"))
    static let vader_vs_see = SelectUnitSquadBuilder(properties: TWProperties(name: "SEE Counter: Vader", rules: generic_gl_counters), u("Vader"), u("Wat"), u("Thrawn"), u("Traya"), u("Sith Empire Trooper"))


    static let jtr_vs_bh_rules = [
        "DEFAULT": TWRule().e,
        "JTR": TWRule(leaderZeta: true).e,
        "R2-D2": TWRule(preference: 1).e,
        "Resistance Trooper": TWRule(preference: 1).e,
        "Rey": TWRule(preference: -10).e,
        "C-3PO": TWRule(preference: -1).e,
    ]

    static let vs_slkr_rules = [
        "DEFAULT": TWRule().e,
        
        "GAS": TWRule(leaderZeta: true, stars: 7, gear: 25).e,
        "Han Solo": TWRule(gear: 23).e,
        "Chewbacca": TWRule(gear: 23).e,
        "Fives": TWRule(gear: 25).e,
    ]
    
    static let vs_geos_rules = [
        "Traya": TWRule(leaderZeta: true, stars: 7, gear: 12).e,
        "Sion": TWRule(uniqueZetas: true, stars: 7, gear: 12).e,
        "Nihilus": TWRule(stars: 7, gear: 11).e,
    ]
    
    static let vs_geos_rules_jawas = [
        "DEFAULT": TWRule(stars: 7, gear: 10).e,
    ]
    
    static let vs_geos_rules_traya_jawas = [
        "Traya": TWRule(leaderZeta: true, stars: 7, gear: 11).e,
        "Sion": TWRule(uniqueZetas: true, stars: 7, gear: 10).e,
        "Jawa Scavenger": TWRule(stars: 7, gear: 9).e,
        "DEFAULT": TWRule(stars: 7, gear: 7).e,
    ]

    static let generic_gl_counters = [
        "DEFAULT": TWRule(stars: 7, gear: 22).e,
        "Wat": TWRule(uniqueZetas: true, stars: 6, gear: 10).e,
        "Barriss": TWRule(uniqueZetas: true, stars: 7, gear: 12).e,
        "StormTrooper": TWRule(uniqueZetas: true, stars: 7, gear: 22).e,
    ]


    // MARK: - Configurations
    
    static let all: [SquadBuilder] = [
        foSquadBuilder, foSquadBuilder2, gkSquadBuilder, gkSquadBuilder2, revanSquadBuilder, revanSquadBuilder2, bastilaSquadBuilder, phoenixSquadBuilder, bhSquadBuilder, bhSquadBuilder2, bhSquadBuilder3, clsSquadBuilder, qiraSquadBuilder, qiraSquadBuilder2, defenseSithSquadBuilder, wiggsDefenseBuilder, ewokBuilder, ewokC3poBuilder,  grievousBuilder, grievousBuilder2, grievousBuilder3, nsIdealBuilder, nsBuilder, ns2Builder, ns3Builder, nestSistersBuilder, trayaSquadBuilder, darthRevanSquadBuilder, darthRevanSquadBuilder2, troopersBuilder, wiggsBuilder, jtrBuilder, jtrBuilder2, jtrBuilder3, magmaBuilder, zetaMaulBuilder, rexBuilder, wampaBuilder, qgjBuilder, padmeBuilder1, padmeBuilder2, geonosianBuilder, rogue1Builder, carthBuilder,
        hansMFBuilder1, negotiator1, negotiator2, malevolanceBuilder, fleet5Builder, fleet6Builder, nuteBuilder, nuteBuilder2, nuteBuilder3, glRey1, slKylo1, nuteSeps, see1, jmk,
        
        // migrated from kotd
        cls, jkl, jmls, monMothma,
        
        // specifically for counters
        sionSoloBuilder, nestSoloBuilder, jawaBuilder, jtrHoldoBuilder, qiraNestHodoBuilder, qiraNestHodoBuilder2, clonesBuilder1, clonesBuilder2, clonesBuilder3, gasBuilder, bastilaRevanBuilder, troopersBuilder2, ig88Builder, epAntiRebelBuilder, epTrioBuilder, epTrayaBuilder, droidsAntiMalakBuilder,
    ]
    
    public static let defaultSquadRuleName = "default"
    public static let configurations: [String:[SquadBuilder]] = [
        defaultSquadRuleName: all,
        
        "kotd": replace(
            remove(all, "Grievous", "Grievous-Wat", "JTR", "JTR+droids", "Nute-Wat", "GL Rey", "FO", "SL Kylo", "CLS", "JMLS"),
            more:
            kotd_gg,
            padmeShaak,
            kotd_padme,
            nuteSeps,
            kotd_nsAVLead,
            cls_kotd,
            jkl, jml3s, monMothma, jtrBuilder3,
            kotd_rey, kotd_fo, kotd_slKylo1
        ),
        
        "3v3" : TW3v3.builders,
        
        "test" : [ padmeBuilder1, padmeBuilder2, kotd_padme, padmeShaak, jmk ],
        
        "hope" : boost(all, preference: 30, "Nute-Sep"),
        
        "counters" : [
            jkl_vs_slkr, gas_vs_slkr, slkr_vs_slkr,
            slkr_vs_rey, see_vs_rey, dr_vs_rey, vader_vs_rey, jkl_vs_rey,
            traya_vs_geos, traya_jawas_vs_geos, jawas_vs_geos,
            jtr_vs_bh,
            
            gg_vs_jml,
            rex_vs_slkr, thrawn_vs_slkr, see_vs_slkr,
            vader_no_wat_vs_rey,
            zep_vs_jmls,
            troopers_vs_see, vader_vs_see,
        ],
    ]
    
    private static func boost(_ builders: [SquadBuilder], preference: Int, _ names: String...) -> [SquadBuilder] {
        builders
            .map { builder -> SquadBuilder in
                if names.contains(builder.name) {
                    let rules = builder.asSquadRules()
                    if rules.count == 1 {
                        var rule = rules[0]
                        rule.preference = preference
                        return rule.squadBuilder()
                    } else {
                        return MultipleSquadBuilder(
                            rules
                                .map { rule -> SquadRule in
                                    var rule = rule
                                    rule.preference = preference
                                    return rule
                                }
                                .map { $0.squadBuilder() }
                        )
                    }
                } else {
                    return builder
                }
            }
    }
    
    private static func remove(_ builders: [SquadBuilder], _ names: String...) -> [SquadBuilder] {
        builders.filter { !names.contains($0.name) }
    }
    
    private static func add(_ builders: [SquadBuilder], more: SquadBuilder...) -> [SquadBuilder] {
        var copy = builders
        copy.append(contentsOf: more)
        return copy
    }
    
    private static func replace(_ builders: [SquadBuilder], more: SquadBuilder...) -> [SquadBuilder] {
        var copy = builders
        let replacing = Set(more.map { $0.name })
        copy.removeAll { replacing.contains($0.name) }
        copy.append(contentsOf: more)
        return copy
    }
    
    public static let allSections = [ "T1", "T2", "T3", "T4", "B1", "B2", "B3", "B4", "F1", "F2" ]
    public static let allFleetSections = [ "F1", "F2"  ]
    public static let allGroundSections = [ "T1", "T2", "T3", "T4", "B1", "B2", "B3", "B4" ]

    public static let allFleetSectionsSet = Set([ "F1", "F2"  ])

    public static let topSections = [ "T1", "T2", "T3", "T4" ]
    public static let bottomSections = [ "B1", "B2", "B3", "B4" ]
    public static let frontSections = [ "T1", "B1" ]
    public static let backSections = [ "T4", "B4" ]
    public static let middleSections = [ "T2", "T3",  "B2", "B3" ]

    public static let keySquads = Set([
        "NS", "FO", "GAS", "Revan", "DR", "BH", "Geonosians", "Grievous", "CLS", "Carth", "Clones",
        "JTR", "Negotiator", "HansMF", "Nute-Wat", "Qira", "Traya", "padme", "zEP",
        
        "GL Rey", "SL Kylo", "SEE", "JMLS", "JMK",
    ])

    /// mapping to fold alternate names down to one
    static private let remap: [String:String] = [
        "NS_Alt" : "NS",
        "NestSisters" : "NS",
        "501st" : "Clones",
        "ShaakClones" : "Clones",
        "ThrawnTroopers" : "Troopers",
        "DRMalak" : "DR",
        "Revan Offense" : "Revan",
        "QiraNest" : "Qira",
        "Disintegration" : "BH",
        "Grievous-Wat" : "Grievous",
        
        // custom ones
        "GG-BB8" : "Grievous",
        "GG-Wat" : "Grievous",
    ]

    public static func canonicalName(_ e: SquadEvaluation) -> String {
        let name = e.squad.name
        return remap[name, default: name]
    }

    public static func canonicalName(_ name: String) -> String {
        return remap[name, default: name]
    }

}


