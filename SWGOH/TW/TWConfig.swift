//
//  TWConfig.swift
//  swgoh
//
//  Created by David Koski on 5/9/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

public struct TWSquadCount : Codable {
    public var named: Int
    public var fluff: Int
    public var fleet = 1
    
    var totalSquads: Int {
        return named + fluff
    }
    
    func canPlace(squad: Squad, placed: TWSquadCount) -> Bool {
        if squad.isFleet {
            return placed.fleet + 1 <= fleet
        } else {
            return placed.named + 1 <= named
        }
    }
}

public protocol TWConfig {
    /// for random placement, these are the teams we consider
    var defenseTeams: Set<String> { get }
    
    var fleetInstructions: String { get }
    
    var minimumGP: Int { get }
    var placementSections: [String] { get }
    var preferredSections: [String:[String]] { get }
    
    var placementInstructions: [PlacementInstruction] { get }
    
    var squadCountThresholds: [(Int, TWSquadCount)] { get }
    var playerSquadCountOverrides: [String:TWSquadCount] { get }
}

// strong T3, fleets
public struct OptimizedConfig : TWConfig {
        
    public init() {        
    }
    
    public let fleetInstructions = "Fleet: **BEST** defensive fleet, fill F1 first"
    
    // extra T3 and T4 because these are the areas of concentration.  extra B1 because we want it to look tougher
    public let placementSections = [ "T1", "T2", "T3", "T4", "B1", "B2", "B3", "B4", "T3", "T4", "T3", "T4", "B1" ]

    // these allow over the placement quota
    let backSections = [ "B1", "B2", "B3", "B4" ]

    /// if a squad is preferred to be placed, e.g. in the front or back
    public let preferredSections = [
        "Phoenix" : [ "T1", "T2", "B1", "B2" ],
        "BH" : [ "T3", "T3", "B3" ],
        "Grievous" : [ "T3", "T4", "B3", "B4" ],
        "padme" : [ "T3", "T4", "B3", "B4" ],
        "Geonosians" : [ "T3", "T4", "B3", "B4" ],
        "DR" : [ "T3", "T4", "B3", "B4" ],
        "DRMalak" : [ "T3", "T4", "B3", "B4" ],
        "zEP" : [ "T3", "T4", "B3", "B4", "T4", "T4", "T4" ],
        "Qira" : [ "T3", "T4" ],
        "Carth" : [ "T2" ],
        "Ewoks" : [ "T2" ],
        "FO" : [ "B2" ],
    ]
    
    public let minimumGP = 60000
        
    let fullSectionCount = 25

    public let defenseTeams: Set = [
        "BH", "FO", "GK",
        "BS",
        "zEP",
        "NS", "NestSisters",
        "Phoenix",
        "Ewoks",
        "Qira",
        "Geonosians",
        "Rogue1",
        "Carth",

        // padme back on offense
//        "padme",

        // Revan is on defense but gets removed via the special instructions
        "Revan",
        "DR",
        "DRMalak",
        // "Traya",
        
        // fleets
        "HansMF",
    ]
    
    public let placementInstructions: [PlacementInstruction] = [
        
        // remove high power NS -- want them on offense
        .removeWhere { ev in ev.squad.name == "NS" && ev.squad.gp > 101000 },
        .removeTop(squad: "DR", count: 3),
        .removeTop(squad: "DRMalak", count: 3),

        .placeManySquads(squad: "NS", section: "B4", count: 4),
        .placeManySquads(squad: "Qira", section: "T4", count: 4),

        .placeManySquads(squad: "FO", section: "B2", count: 22),
        .placeManySquads(squad: "Ewoks", section: "T2", count: 22),

//        .placeManySquads(squad: "padme", section: "T4", count: 3),
//        .placeManySquads(squad: "padme", section: "B4", count: 3),

        .placeSquad(squad: "Revan", section: "T3"),
        .placeManySquads(squad: "Revan", section: "T3", count: 3),
        .placeManySquads(squad: "DR", section: "T4", count: 3),
        .placeManySquads(squad: "Geonosians", section: "T4", count: 4),

        .placeManySquads(squad: "BH", section: "T3", count: 10),
        .placeManySquads(squad: "zEP", section: "T4", count: 9),
        
        .placeManySquads(squad: "BH", section: "B3", count: 10),
        .placeManySquads(squad: "DR", section: "B4", count: 2),

        // some stronger fleets
        .placeManySquads(squad: "HansMF", section: "F2", count: 4),
        .placeManySquads(squad: "HansMF", section: "F1", count: 10),
        .placeManySquads(squad: "HansMF", section: "F2", count: 10),

        // don't place any more Revan squads on defense
        .removeAll(squad: "Revan", status: .unknown),
        .removeAll(squad: "NS", status: .unknown),
        .removeAll(squad: "NestSisters", status: .unknown),
        .removeAll(squad: "DR", status: .unknown),
        .removeAll(squad: "DRMalak", status: .unknown),
        .removeAll(squad: "padme", status: .unknown),

    ]
    
    public let squadCountThresholds: [(Int, TWSquadCount)] = [
        ( 1_500_000, TWSquadCount(named: 1, fluff: 1) ),
        ( 2_200_000, TWSquadCount(named: 2, fluff: 1) ),
        ( 3_000_000, TWSquadCount(named: 4, fluff: 0) ),
        ( 4_000_000, TWSquadCount(named: 5, fluff: 0) ),
        ( 5_000_000, TWSquadCount(named: 5, fluff: 0) ),
        ( 10_000_000, TWSquadCount(named: 4, fluff: 2) ),
    ]

    public let playerSquadCountOverrides = [String : TWSquadCount]()
}

struct HopeConfig2 : TWConfig {
    
    let fleetInstructions = "Fleet: **BEST** defensive fleet, fill F1 first"

    let placementSections: [String] = TW.allGroundSections

    /*
    
    From MF:
    
    1N - CLS lead Rebels 80k+
    1S - Kru lead First Order 80k+
    2N - Grievous Seperatists/Droids 80k+
    (Revans if unable to attack)
    2S - Padme GR 80k+
    (Revans if unable to attack)
    3N - Brood Alpha Geos / Talzin lead split 80k+
    3S - Brood Alpha Geos 75k+ / Bounty Hunters split 90k+
    4N - Synergy Squads 80k+
    4S - Bounty Hunters 80k+

    */

    /// if a squad is preferred to be placed, e.g. in the front or back
    let preferredSections = [
        "DR" : [ "T1", "T1", "B2" ],
        "DRMalak" : [ "T1" ],
        "Grievous" : [ "T1", "T1", "B2" ],
        
        "Carth" : [ "T2" ],
        "GK" : [ "T2", ],
        "BS" : [ "T2", ],

        "BH" : [ "T3" ],
        
        "FO" : [ "T4", "T4", "T4", "T4", "B3" ],
        
        "Revan" : [ "B1" ],
        "ShaakClones" : [ "B1" ],
        "padme" : [ "B1" ],
        "Qira" : [ "B1" ],

        "NS" : [ "B3", "B3", "B2" ],
        "NestSisters" : [ "B3", "B3", "B2" ],
        
        "zEP" : [ "B3", ],
        
        "Geonosians" : [ "B4", ],
        
        "Phoenix" : [ "B4" ],
        "CLS" : [ "T2", "T4" ],
    ]
    
    let minimumGP = 60000
        
    let fullSectionCount = 25

    let defenseTeams: Set = [
        "BH", "FO", "GK",
        "BS",
        "zEP",
        "Qira",
        "Grievous",
        "Geonosians",
        "Carth",
        "CLS",
        "NS",
        "NestSisters",
        "ShaakClones",

        "padme",

        "Revan",
        
        "zMaul",
        
        // Clones, 501st, ShaakClones

        // use these for people going defensive
        "DR",
        "DRMalak",
        "Phoenix",
        "Revan Offense",
        "Traya",

    ]
    
    let placementInstructions: [PlacementInstruction] = [
        
        // defensive placements
        .placePlayerSquads(player: "Jasmyne Enyaw", count: 10),
        .placePlayerSquads(player: "Arwkin", count: 10),
        .placePlayerSquads(player: "MattBerg757", count: 10),
        
        .removeAll(squad: "Phoenix", status: .unknown),
        .removeAll(squad: "Revan Offense", status: .unknown),
        .removeAll(squad: "Traya", status: .unknown),
//        .removeAll(squad: "padme", status: .unknown),

        .removeBelow(squad: "BH", gp: 85_000),
        .removeBelow(squad: "CLS", gp: 90_000),
        .removeBelow(squad: "NS", gp: 90_000),
        .removeBelow(squad: "NestSisters", gp: 85_000),
        .removeBelow(squad: "Carth", gp: 80_000),
        .removeBelow(squad: "FO", gp: 75_000),
        .removeBelow(squad: "Qira", gp: 85_000),
        .removeBelow(squad: "Rogue1", gp: 75_000),
        .removeBelow(squad: "zEP", gp: 85_000),
        .removeBelow(squad: "zMaul", gp: 95_000),
        
        
        .placePreferred(squad: "DRMalak", count: 25),
        .placePreferred(squad: "Grievous", count: 25),
        .placePreferred(squad: "DR", count: 25),

        .removeAll(squad: "DRMalak", status: .unknown),
        .removeAll(squad: "Grievous", status: .unknown),
        .removeAll(squad: "DR", status: .unknown),

        .placePreferred(squad: "Revan", count: 25),
        .placePreferred(squad: "Qira", count: 25),
        .placePreferred(squad: "ShaakClones", count: 25),
        .placePreferred(squad: "padme", count: 10),
        
        .removeAll(squad: "Revan", status: .unknown),
        .removeAll(squad: "Qira", status: .unknown),
        .removeAll(squad: "ShaakClones", status: .unknown),
        .removeAll(squad: "padme", status: .unknown),

        .placePreferred(squad: "BH", count: 25),
        .placePreferred(squad: "FO", count: 25),
        .placePreferred(squad: "NS", count: 25),
        .placePreferred(squad: "NestSisters", count: 25),
        .placePreferred(squad: "Geonosians", count: 25),
        
        .removeAll(squad: "BH", status: .unknown),
        .removeAll(squad: "FO", status: .unknown),
        .removeAll(squad: "NS", status: .unknown),
        .removeAll(squad: "NestSisters", status: .unknown),
        .removeAll(squad: "Geonosians", status: .unknown),
        
        .placePreferred(squad: "FO", count: 25),
        .removeAll(squad: "FO", status: .unknown),

        .placePreferred(squad: "CLS", count: 25),
        .removeAll(squad: "CLS", status: .unknown),
    ]
    
    let squadCountThresholds: [(Int, TWSquadCount)] = [
        ( 1_500_000, TWSquadCount(named: 1, fluff: 1) ),
        ( 2_200_000, TWSquadCount(named: 3, fluff: 1) ),
        ( 3_000_000, TWSquadCount(named: 4, fluff: 0) ),
        ( 4_000_000, TWSquadCount(named: 5, fluff: 0) ),
        ( 5_000_000, TWSquadCount(named: 5, fluff: 0) ),
        ( 10_000_000, TWSquadCount(named: 6, fluff: 0) ),
    ]
    
    // jasmyne, bjorn, arwkin, tim, sanderz,
    // longshadow, waldo, mc1631
    
    // temp defense: busankaz, mosvander

    var playerSquadCountOverrides: [String : TWSquadCount] = [
        "Jasmyne Enyaw" : TWSquadCount(named: 10, fluff: 0),
        "Arwkin" : TWSquadCount(named: 10, fluff: 0),
        "MattBerg757" : TWSquadCount(named: 10, fluff: 0),
                
        // limit defense teams
        "BigRed" : TWSquadCount(named: 3, fluff: 0),
        "Gorgatron" : TWSquadCount(named: 3, fluff: 0),
        "Tag1turn2" : TWSquadCount(named: 3, fluff: 0),
        
        "Whitezilla" : TWSquadCount(named: 3, fluff: 0),
        "Delmon Ciiid'r" : TWSquadCount(named: 3, fluff: 0),
        "Ashe Karrde" : TWSquadCount(named: 4, fluff: 0),

    ]

}

struct ExileConfig : TWConfig {
        
    public init() {
    }
    
    public let fleetInstructions = "Fleet: **BEST** defensive fleet, fill F1 first"
    
    // extra T3 and T4 because these are the areas of concentration.  extra B1 because we want it to look tougher
    public let placementSections = [ "T1", "T2", "T3", "T4", "B1", "B2", "B3", "B4", "T3", "T4", "T3", "T4", "B1" ]

    // these allow over the placement quota
    let backSections = [ "B1", "B2", "B3", "B4" ]

    /// if a squad is preferred to be placed, e.g. in the front or back
    public let preferredSections = [
        "Phoenix" : [ "T1", "T2", "B1", "B2" ],
        "BH" : [ "T3", "T3", "B3" ],
        "Grievous" : [ "T3", "T4", "B3", "B4" ],
        "padme" : [ "T3", "T4", "B3", "B4" ],
        "Geonosians" : [ "T3", "T4", "B3", "B4" ],
        "DR" : [ "T3", "T4", "B3", "B4" ],
        "DRMalak" : [ "T3", "T4", "B3", "B4" ],
        "zEP" : [ "T3", "T4", "B3", "B4", "T4", "T4", "T4" ],
        "Qira" : [ "T3", "T4" ],
        "Carth" : [ "T2" ],
        "Ewoks" : [ "T2" ],
        "FO" : [ "B2" ],
    ]
    
    public let minimumGP = 60000
        
    let fullSectionCount = 25

    public let defenseTeams: Set = [
        "BH", "FO", "GK",
        "BS",
        "zEP",
        "NS", "NestSisters",
        "Phoenix",
        "Ewoks",
        "Qira",
        "Geonosians",
        "Rogue1",
        "Carth",

        // padme back on offense
//        "padme",

        // Revan is on defense but gets removed via the special instructions
        "Revan",
        "DR",
        "DRMalak",
        // "Traya",
        
        // fleets
        "HansMF",
    ]
    
    public let placementInstructions: [PlacementInstruction] = [
        
        // remove high power NS -- want them on offense
        .removeWhere { ev in ev.squad.name == "NS" && ev.squad.gp > 101000 },
        .removeTop(squad: "DR", count: 3),
        .removeTop(squad: "DRMalak", count: 3),

        .placeManySquads(squad: "NS", section: "B4", count: 4),
        .placeManySquads(squad: "Qira", section: "T4", count: 4),

        .placeManySquads(squad: "FO", section: "B2", count: 22),
        .placeManySquads(squad: "Ewoks", section: "T2", count: 22),

//        .placeManySquads(squad: "padme", section: "T4", count: 3),
//        .placeManySquads(squad: "padme", section: "B4", count: 3),

        .placeSquad(squad: "Revan", section: "T3"),
        .placeManySquads(squad: "Revan", section: "T3", count: 3),
        .placeManySquads(squad: "DR", section: "T4", count: 3),
        .placeManySquads(squad: "Geonosians", section: "T4", count: 4),

        .placeManySquads(squad: "BH", section: "T3", count: 10),
        .placeManySquads(squad: "zEP", section: "T4", count: 9),
        
        .placeManySquads(squad: "BH", section: "B3", count: 10),
        .placeManySquads(squad: "DR", section: "B4", count: 2),

        // some stronger fleets
        .placeManySquads(squad: "HansMF", section: "F2", count: 4),
        .placeManySquads(squad: "HansMF", section: "F1", count: 10),
        .placeManySquads(squad: "HansMF", section: "F2", count: 10),

        // don't place any more Revan squads on defense
        .removeAll(squad: "Revan", status: .unknown),
        .removeAll(squad: "NS", status: .unknown),
        .removeAll(squad: "NestSisters", status: .unknown),
        .removeAll(squad: "DR", status: .unknown),
        .removeAll(squad: "DRMalak", status: .unknown),
        .removeAll(squad: "padme", status: .unknown),

    ]
    
    public let squadCountThresholds: [(Int, TWSquadCount)] = [
        ( 1_500_000, TWSquadCount(named: 1, fluff: 1) ),
        ( 2_200_000, TWSquadCount(named: 2, fluff: 1) ),
        ( 3_000_000, TWSquadCount(named: 4, fluff: 0) ),
        ( 4_000_000, TWSquadCount(named: 5, fluff: 0) ),
        ( 5_000_000, TWSquadCount(named: 5, fluff: 0) ),
        ( 10_000_000, TWSquadCount(named: 4, fluff: 2) ),
    ]

    var playerSquadCountOverrides: [String : TWSquadCount] = [
        "Quam" : TWSquadCount(named: 8, fluff: 2)
    ]
}

public enum PlacementInstruction {
    case placeSquad(squad: String, section: String)
    case placeManySquads(squad: String, section: String, count: Int)
    case placePreferred(squad: String, count: Int)
    case placeAllPreferred(squadCounts: [String:Int])
    case placePlayerSquads(player: String, count: Int)
    case removeAll(squad: String, status: TWTrace.Status)
    case removeTop(squad: String, count: Int)
    case removeAbove(squad: String, gp: Int, skip: Set<Int>)
    case removeBelow(squad: String, gp: Int)
    case removeWhere(filter: (SquadEvaluation) -> Bool)
    case removeSquads(squads: Set<String>, player: Model.Player)
}

private func loadTWConfig(_ name: String) -> TWConfig {
    let url = Bundle(for: ModelClass.self).url(forResource: name, withExtension: "twconfig")!
    let data = try! Data(contentsOf: url)
    return try! JSONDecoder().decode(EditableTWConfiguration.self, from: data).compile()
}

public let twConfigByName: [String:() -> TWConfig] = [
    "DEFAULT" : { OptimizedConfig() },
    "exile" : { ExileConfig() },
    "hope" : { HopeConfig2() },
    "kotd" : { loadTWConfig("KOTD") },
    "ebonhand" : { loadTWConfig("EbonHand") },
]

private func loadTWSquadBuilder(_ name: String) -> SquadBuilder {
    return MultipleSquadBuilder(TW.configurations[name]!)
}

public let twSquadsByName: [String:() -> SquadBuilder] = [
    "DEFAULT" : { loadTWSquadBuilder(TW.defaultSquadRuleName) },
    "kotd" : { loadTWSquadBuilder("kotd") }
]
