//
//  Mods.swift
//  swgoh-tool
//
//  Created by David Koski on 1/28/21.
//  Copyright © 2021 David Koski. All rights reserved.
//

/// Structure for the recommendations on the site -- per Character reference data.  This holds
/// info about the characters (PerCharacter) like name and tags, plus information about
/// the mod clusters (see ModRecommendations) per gear level.
///
/// This is meant to be stored at the top level of the site.
public struct SiteModReferenceData : Codable {
    public let characters: [String:PerCharacter]
    
    /// information about one mod recommendation cluster, see ModRecommendation.Cluster
    public struct Cluster : Codable {
        /// names of the stats for priorities
        public let priorities: [String]
        
        /// per gear level targets by priority
        public let targets: [Int:[Int]]
        
        enum CodingKeys: String, CodingKey {
            case priorities = "p"
            case targets = "t"
        }
        
        init(character: Model.Character, cluster: ModRecommendation.Cluster) {
            self.priorities = cluster.priorities.map { $0.c.description }
            
            var targets = [Int:[Int]]()
            for level in Model.GearLevel.levels {
                targets[level] = modStats(character: character, gearLevel: level, priorities: cluster.priorities, mods: cluster.center.mods).map { $0.v }
            }
            self.targets = targets
        }
    }
    
    public struct PerCharacter : Codable {
        /// printable name
        public let name: String
        
        /// e.g. rebels, droid, etc.
        public let tags: [String]
        
        /// the name for use in a url, e.g. admiral-piett
        public let urlName: String
        
        /// potential clusters
        public let clusters: [Cluster]
        
        enum CodingKeys: String, CodingKey {
            case name = "n"
            case urlName = "u"
            case clusters = "c"
            case tags = "t"
        }
        
        init(character: Model.Character, recommendation: ModRecommendation) {
            self.name = character.name
            self.urlName = character.urlName
            self.tags = character.categories.map { $0.lowercased() }
            
            self.clusters = recommendation.clusters.map { Cluster(character: character, cluster: $0) }
        }
    }
    
    public init(recommendations: [String:ModRecommendation]) {
        var result = [String:PerCharacter]()
        
        for (id, rec) in recommendations {
            result[id] = PerCharacter(character: Model.Character.find(id), recommendation: rec)
        }
        
        self.characters = result
    }
}

/// Per-guild mod data for each of the players.
public struct SiteGuildModData : Codable {
    
    public struct Player : Codable {
        public let allyCode: Int
        public let name: String
        
        enum CodingKeys: String, CodingKey {
            case allyCode = "c"
            case name = "n"
        }
        
        init(_ player: Model.Player) {
            self.allyCode = player.stats.allyCode
            self.name = player.name
        }
    }
    
    public struct PerUnit : Codable {
        public let gearLevel: Int
        
        /// tied to the primary cluster in SiteModReferenceData
        public let values: [Int]
        
        /// score of the selected cluster (how good are their mods)
        public let score: Int
        
        /// distance from the selected cluster (how aligned are they)
        public let distance: Int
        
        /// optional alternate cluster (the real target)
        public let alternateClusterIndex: Int?
        public let alternateValues: [Int]?
        
        enum CodingKeys: String, CodingKey {
            case gearLevel = "g"
            case values = "v"
            case score = "s"
            case distance = "d"
            case alternateClusterIndex = "alt"
            case alternateValues = "altv"
        }
    }
    
    /// array of player info
    public let players: [Player]
    
    /// map of character.id -> array of PerUnit structs.  The array is parallel to the players array, e.g. the 2nd player
    /// corresponds to the 2nd entry in the array.
    public let units: [String:[PerUnit?]]
    
    public init(guild: Model.Guild, recommendations: [String:ModRecommendation]) {
        self.players = guild.players.map { Player($0) }
        
        let ids = Array(recommendations.keys)
        let units: [String:[PerUnit?]] = DispatchQueue.concurrentPerform(iterations: ids.count) { index in
            let id = ids[index]
            let rec = recommendations[id]!
            
            if rec.clusters.isEmpty {
                return (id, [])
            }
            
            let character = Model.Character.find(id)
            
            let firstCluster = rec.clusters[0]
            
            var unitData = [PerUnit?]()
            for player in guild.players {
                if let unit = player.units[id], unit.gear >= 8 && unit.mods.count == 6 {
                    let mh = ModHistogram(mods: unit.mods).normalized
                    
                    // figure out which cluster they belong to
                    let distances = rec.clusters.map { $0.centroidNormalized.distance(from: mh) }
                    let targetIndex = distances.firstIndex(of: distances.min() ?? 0) ?? 0
                    let targetCluster = rec.clusters[targetIndex]
                    
                    let gearLevel = unit.combinedGearLevel.value
                    
                    // compute the stats for the first cluster
                    let stats = modStats(character: character, gearLevel: gearLevel, priorities: firstCluster.priorities, mods: unit.mods)
                    
                    // and for the alternate cluster that they are really going after
                    let altStats = targetIndex == 0 ? nil : modStats(character: character, gearLevel: gearLevel, priorities: targetCluster.priorities, mods: unit.mods)
                                                  
                    let targetStats = modStats(character: character, gearLevel: gearLevel, priorities: targetCluster.priorities, mods: targetCluster.center.mods)

                    let score = modScore(priorities: targetCluster.priorities, stats: altStats ?? stats, targets: targetStats)
                    
                    let entry = PerUnit(gearLevel: unit.combinedGearLevel.value, values: stats.map { $0.v }, score: score, distance: distances[targetIndex], alternateClusterIndex: targetIndex == 0 ? nil : targetIndex, alternateValues: altStats?.map { $0.v })
                    unitData.append(entry)

                } else {
                    // does not have unit
                    unitData.append(nil)
                }
            }

            return (id, unitData)
        }
        
        self.units = units.filter { !$1.isEmpty }
    }
}
