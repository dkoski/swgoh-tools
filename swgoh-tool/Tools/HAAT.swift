//
//  HAAT.swift
//  swgoh-tool
//
//  Created by David Koski on 2/9/21.
//  Copyright © 2021 David Koski. All rights reserved.
//

import SWGOH

struct HAAT {
    
    static let rules = [
        "DEFAULT" : TWRule(gear: 25).e,
    ]
    
    // https://gaming-fans.com/star-wars-goh/swgoh-guides/aat-raid-guide/aat-phase-1-teams-strategy/
    
    // MARK:- Phase 1
    
    static let cls1 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS P1", preference: 15, rules: cls1Rules), u("CLS"))
    static let clsHan1 = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS P1", preference: 15, rules: cls1Rules), u("CLS"), u("Han Solo"))
    
    static let youngHan1 = SelectUnitSquadBuilder(properties: TWProperties(name: "Young Han: FULL", preference: 15, rules: youngHanRules), u("Ackbar"), u("CLS"), u("BB8"), u("Hyoda"), u("Young Han"))
    
    static let resistance1234 = SelectUnitSquadBuilder(properties: TWProperties(name: "JTR: FULL", preference: 15, rules: jtrFullRules), u("JTR"), u("BB8"), u("C-3PO"), u("CLS"), u("Finn", "Wampa"))

    
    static let kylo1 = SelectUnitSquadBuilder(properties: TWProperties(name: "Kylo P1", preference: 15, rules: cls1Rules), u("Kylo"))

    static let jtrFullRules = [
        "DEFAULT": TWRule().e,
        "JTR": TWRule(leaderZeta: true).e,
    ]

    static let cls1Rules = [
        "DEFAULT": TWRule().e,
        "CLS" : TWRule(leaderZeta: true, gear: 10).e,
        "Kylo" : TWRule().e,
    ]
    
    static let youngHanRules = [
        "DEFAULT": TWRule().e,
        "Young Han" : TWRule(uniqueZetas: true, gear: 10).e,
    ]

    
    static let p1 = MultipleSquadBuilder(cls1, clsHan1, youngHan1, kylo1, resistance1234)
    
    // MARK:- Phase 2+
    
    static let ns234 = SelectUnitSquadBuilder(properties: TWProperties(name: "NS: 2-4", preference: 15, rules: ns234Rules), u("MT"), u("Daka"), u("Asajj"), u("Spirit", "Nightsister Initiate"), u("Zombie"))
    
    static let jtr234 = SelectUnitSquadBuilder(properties: TWProperties(name: "JTR: 2-4", preference: 15, rules: resistanceJTRRules), u("JTR"), u("Finn"), u("BB8"), u("Resistance Trooper"), u("Poe", "Scav Rey"))
    static let finn234 = SelectUnitSquadBuilder(properties: TWProperties(name: "Finn: 2-4", preference: 15, rules: resistanceFinnRules), u("Finn"), u("Poe"), u("BB8"), u("Resistance Trooper"), u("JTR", "Scav Rey"))

    static let resistanceJTRRules = [
        "DEFAULT": TWRule().e,
        "JTR": TWRule(leaderZeta: true).e
    ]

    static let resistanceFinnRules = [
        "DEFAULT": TWRule().e,
        "Finn": TWRule(leaderZeta: true).e
    ]

    static let ns234Rules = [
        "DEFAULT": TWRule().e,
        "Asajj" : TWRule(uniqueZetas: true, gear: 10).e,
        "MT" : TWRule(leaderZeta: true, gear: 10).e,
    ]

    static let ackbar3 = SelectUnitSquadBuilder(properties: TWProperties(name: "Ackbar 2-4", preference: 4, rules: ackbar3Rules), u("Ackbar"), u("CLS"), u("Thrawn"), u("Asajj"), u("BB8"))
    
    static let ackbar3Rules = [
        "DEFAULT": TWRule().e,
        "Asajj" : TWRule(uniqueZetas: true, gear: 10).e,
    ]

    static let p2 = MultipleSquadBuilder(ns234, jtr234, finn234, ackbar3)
    
}
