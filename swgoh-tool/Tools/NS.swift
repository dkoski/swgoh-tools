//
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

struct NS {
    
    static let playerSelect = PlayerSelectCharacter("ASAJVENTRESS")

    // https://docs.google.com/document/d/1uLDHEihCR71w1N7TByMxlh-WbQi8UArjQZRJJ9Qxpuc/edit
    static let deathStormBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Deathstorm"),
        u("MOTHERTALZIN"),
        u("ASAJVENTRESS"),
        u("DAKA"),
        u("NIGHTSISTERZOMBIE"),
        u("NIGHTSISTERINITIATE", "NIGHTSISTERSPIRIT"))
    
    static let deathStorm2Builder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Deathstorm-JKA"),
        u("MOTHERTALZIN"),
        u("ASAJVENTRESS"),
        u("DAKA"),
        u("JKA"),
        u("NIGHTSISTERINITIATE", "NIGHTSISTERACOLYTE"))

    // https://drive.google.com/file/d/1p5LlfkMVn8G-c8I6NusdWoMKGNozTXyl/view
    static let p4NSBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "NS"),
        u("ASAJVENTRESS"),
        u("DAKA"),
        u("TALIA"),
        u("NIGHTSISTERZOMBIE", "MOTHERTALZIN", "NIGHTSISTERINITIATE", "NIGHTSISTERACOLYTE", "NIGHTSISTERSPIRIT"),
        u("NIGHTSISTERZOMBIE", "MOTHERTALZIN", "NIGHTSISTERINITIATE", "NIGHTSISTERACOLYTE", "NIGHTSISTERSPIRIT"))

    static let deathStormEvaluator = SquadEvaluator(rules: [
        "MOTHERTALZIN" : UnitEvaluator(
            AllOmegasRule(),
            
            OmegaZetaAbilityRule("leaderskill_MOTHERTALZIN"),

            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "ASAJVENTRESS" : UnitEvaluator(

            OmegaZetaAbilityRule("uniqueskill_ASAJVENTRESS01"),
            
            OmegaZetaAbilityRule("basicskill_ASAJVENTRESS"),
            OmegaZetaAbilityRule("specialskill_ASAJVENTRESS02"),

            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "DAKA" : UnitEvaluator(
            
            OmegaZetaAbilityRule("specialskill_DAKA01"),

            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "NIGHTSISTERZOMBIE" : UnitEvaluator(
            
            OmegaZetaAbilityRule("uniqueskill_NIGHTSISTERZOMBIE02"),

            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "NIGHTSISTERINITIATE" : UnitEvaluator(
            
            // this unit works the best
            PreferenceRule(preference: 1),
            
            OmegaZetaAbilityRule("basicskill_NIGHTSISTERINITIATE"),
            
            // gear level is not important
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),

        "NIGHTSISTERSPIRIT" : UnitEvaluator(
            
            OmegaZetaAbilityRule("basicskill_NIGHTSISTERSPIRIT"),

            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        
        "NIGHTSISTERACOLYTE" : UnitEvaluator(            
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),

        "JKA" : UnitEvaluator(
            
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),

        ])
    
    // TODO stat tuning per the battle sheet
    static let p4NSEvaluator = SquadEvaluator(rules: [
        "MOTHERTALZIN" : UnitEvaluator(
            AllOmegasRule(),
            
            // this unit works the best
            PreferenceRule(preference: 1),
            
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "ASAJVENTRESS" : UnitEvaluator(
            AllOmegasRule(),

            OmegaZetaAbilityRule("leaderskill_ASAJVENTRESS"),
            
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "DAKA" : UnitEvaluator(
            AllOmegasRule(),
            
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "NIGHTSISTERZOMBIE" : UnitEvaluator(
            
            // this unit works the best
            PreferenceRule(preference: 1),
            
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        "TALIA" : UnitEvaluator(
            
            AllOmegasRule(),

            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        
        "DEFAULT" : UnitEvaluator(
            AllOmegasRule(),
            
            UnitRule.gear10PlusRule,
            UnitRule.level85Rule,
            UnitRule.sevenStarRule
        ),
        
        ])

}
