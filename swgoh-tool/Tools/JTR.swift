//
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

struct JTR {
    
    static let playerSelect = PlayerSelectCharacter("REYJEDITRAINING")
    
    static let squadBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "JTR"), u("REYJEDITRAINING"), u("REY", "C-3PO"), u("BB8"), u("R2D2_LEGENDARY"), u("RESISTANCETROOPER", "VISASMARR", "BARRISSOFFEE", "Finn"))

    
    static let squadEvaluator = SquadEvaluator(rules: [
        "JTR" : TWRule(leaderZeta: true, uniqueZetas: true, stars: 7, gear: 10).e,
        "REY" : TWRule(gear: 10).e,
        "BB8" : TWRule(gear: 10).e,
        "R2D2_LEGENDARY" : TWRule(uniqueZetas: true, gear: 10).e,
        "RESISTANCETROOPER" : TWRule(gear: 10, preference: 1).e,
        "FINN" : TWRule(gear: 10, preference: 1).e,
        "C3POLEGENDARY" : TWRule(gear: 10, preference: 1).e,
        "VISASMARR" : TWRule(uniqueZetas: true, gear: 10, preference: -1).e,
        "BARRISSOFFEE" : TWRule(uniqueZetas: true, gear: 10, preference: -1).e,
    ])

}
