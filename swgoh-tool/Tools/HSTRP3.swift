//
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

struct HSTRP3 {
    
    /// ChexMix based on https://drive.google.com/file/d/1Pg1goUW-bRebWd8nzJ9es8nF0Nk4S9KC/view
    static let chexMixPlayerSelect = PlayerSelectCharacter("DEATHTROOPER")
    
    static let highDamageAttackerIds = [ "CHIRRUTIMWE", "BOBAFETT", "NIGHTSISTERSPIRIT", "FIRSTORDERTIEPILOT", "TIEFIGHTERPILOT", "PRINCESSLEIA", "NIGHTSISTERACOLYTE" ]
    
    static let highDamageAttackers = u(highDamageAttackerIds)
    
    // Note: the second group could contain "GRANDMASTERYODA" but we want him for P2.  QGJ is another option with offense up, but he is not an attacker.
    static let chexMixBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "ChexMix"),
         u("COMMANDERLUKESKYWALKER"),
         u("HANSOLO"),
         u("DEATHTROOPER"),
         u("PAO", "ANAKINKNIGHT", "EMPERORPALPATINE", "POGGLETHELESSER", "QUIGONJINN"),
         u("chirrut", "CHEWBACCALEGENDARY", "CT7567", "BAZEMALBUS", "STORMTROOPERHAN"))
    
    static let roloBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "ROLO", preference: -4),
        u("HOTHLEIA"),
        u("PAO", "ANAKINKNIGHT", "POGGLETHELESSER"),
        highDamageAttackers, highDamageAttackers, highDamageAttackers)
    
    static let bhBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "BH", preference: -6), u("BOSSK"), u("GREEDO"), u(highDamageAttackerIds.adding("PAO", "ANAKINKNIGHT", "POGGLETHELESSER")), highDamageAttackers, highDamageAttackers)
    
    // harder to use but potentially higher damage
    static let flamingGreedoBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Flaming", preference: 2),
        u("BOBAFETT"),
        u("GREEDO"),
        u("DEATHTROOPER"),
        u("PAO", "ANAKINKNIGHT", "EMPERORPALPATINE", "POGGLETHELESSER"),
        highDamageAttackers)
    
    static let chexMixSquadEvaluator = SquadEvaluator(rules: [
        "CLS" : TWRule(gear: 10).e,
        "Han Solo" : TWRule(uniqueZetas: true, gear: 10).e,
        "DT" : TWRule(gear: 8).e,
        "PAO" : TWRule(gear: 8).e,
        "JKA" : TWRule(gear: 9).e,
        "EP" : TWRule(gear: 10, preference: -1).e,
        "POGGLETHELESSER" : TWRule(gear: 9).e,
        "CHEWBACCALEGENDARY" : TWRule(uniqueZetas: true, gear: 10).e,
        "CHIRRUTIMWE" : TWRule(gear: 9, preference: 1).e,
        "CT7567" : TWRule(gear: 9).e,
        "GREEDO" : TWRule(gear: 10).e,
        "DEFAULT" : TWRule(gear: 10, preference: -3).e,
    ])
    
    static let roloRules = [
        "HOTHLEIA" : TWRule(gear: 8).e,
        "PAO" : TWRule(gear: 9, preference: 1).e,
        "JKA" : TWRule(gear: 9, preference: 1).e,
        "Poggle" : TWRule(gear: 9, preference: 1).e,
        "DEFAULT" : TWRule(gear: 10, preference: -3).e,
    ]
    
    static let bhRules = [
        "PAO" : TWRule(gear: 9, preference: 1).e,
        "JKA" : TWRule(gear: 9, preference: 1).e,
        "Poggle" : TWRule(gear: 9, preference: 1).e,
        "Boba Fett" : TWRule(gear: 10, preference: 1).e,
        "Default" : TWRule(gear: 10, preference: -3).e,
    ]

}
