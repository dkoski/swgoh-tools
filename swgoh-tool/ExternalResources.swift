//
//  ModRecommendations.swift
//  SWGOH
//
//  Created by David Koski on 12/25/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import Foundation
import SWGOH

private struct Entry {
    let u: String
    let n: String
}

private let data: [Entry] = [
    Entry(u: "https://gaming-fans.com/2020/03/swgoh-best-mods-for-rey-galactic-legend/", n: "Rey"),
    Entry(u: "https://gaming-fans.com/2020/10/16/swgoh-best-mods-for-jedi-master-luke-skywalker-galactic-legend/", n: "Jedi Master Luke Skywalker"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-aayla-secura/", n: "Aayla Secura"),
    Entry(u: "https://gaming-fans.com/2020/09/swgoh-best-mods-for-admiral-piett/", n: "Admiral Piett"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-admiral-ackbar/", n: "Admiral Ackbar"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-asajj-ventress/", n: "Asajj Ventress"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-ahsoka-tano/", n: "Ahsoka Tano"),
    Entry(u: "https://gaming-fans.com/2018/09/swgoh-best-mods-for-aurra-sing/", n: "Aurra Sing"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-fulcrum-ahsoka-tano/", n: "Ahsoka Tano (Fulcrum)"),
    Entry(u: "https://gaming-fans.com/2019/02/swgoh-best-mods-for-b1-battle-droid/", n: "B1 Battle Droid"),
    Entry(u: "https://gaming-fans.com/2019/12/swgoh-best-mods-for-arc-trooper/", n: "ARC Trooper"),
    Entry(u: "https://gaming-fans.com/2017/11/swgoh-best-mods-b2-super-battle-droid/", n: "B2 Super Battle Droid"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-barriss-offee/", n: "Barriss Offee"),
    Entry(u: "https://gaming-fans.com/2018/11/swgoh-best-mods-for-bastila-shan-fallen/", n: "Bastila Shan (Fallen)"),
    Entry(u: "https://gaming-fans.com/2018/07/swgoh-best-mods-for-bastila-shan/", n: "Bastila Shan"),
    Entry(u: "https://gaming-fans.com/2017/05/swgoh-best-mods-boba-fett/", n: "Boba Fett"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-baze-malbus/", n: "Baze Malbus"),
    Entry(u: "https://gaming-fans.com/2018/08/swgoh-best-mods-for-bossk/", n: "Bossk"),
    Entry(u: "https://gaming-fans.com/2017/09/swgoh-best-mods-bb-8/", n: "BB-8"),
    Entry(u: "https://gaming-fans.com/2017/11/swgoh-best-mods-cad-bane/", n: "Cad Bane"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-for-biggs-darklighter/", n: "Biggs Darklighter"),
    Entry(u: "https://gaming-fans.com/2018/11/swgoh-best-mods-for-canderous-ordo/", n: "Canderous Ordo"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-bistan/", n: "Bistan"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-captain-phasma/", n: "Captain Phasma"),
    Entry(u: "https://gaming-fans.com/2017/09/swgoh-best-mods-bodhi-rook/", n: "Bodhi Rook"),
    Entry(u: "https://gaming-fans.com/2017/12/swgoh-best-mods-colonel-starck/", n: "Colonel Starck"),
    Entry(u: "https://gaming-fans.com/2018/12/swgoh-best-mods-for-c-3po/", n: "C-3PO"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-for-count-dooku/", n: "Count Dooku"),
    Entry(u: "https://gaming-fans.com/2017/08/swgoh-best-mods-captain-han-solo/", n: "Captain Han Solo"),
    Entry(u: "https://gaming-fans.com/2019/04/swgoh-best-mods-for-darth-malak/", n: "Darth Malak"),
    Entry(u: "https://gaming-fans.com/2020/04/swgoh-best-mods-for-cara-dune/", n: "Cara Dune"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-darth-maul/", n: "Darth Maul"),
    Entry(u: "https://gaming-fans.com/2019/01/swgoh-best-mods-for-carth-onasi/", n: "Carth Onasi"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-darth-nihilus/", n: "Darth Nihilus"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-cassian-andor/", n: "Cassian Andor"),
    Entry(u: "https://gaming-fans.com/2019/04/swgoh-best-mods-for-darth-revan/", n: "Darth Revan"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-commander-cody/", n: "CC-2224"),
    Entry(u: "https://gaming-fans.com/2017/11/swgoh-best-mods-darth-sidious/", n: "Darth Sidious"),
    Entry(u: "https://gaming-fans.com/2018/09/swgoh-best-mods-for-chewbacca/", n: "Chewbacca"),
    Entry(u: "https://gaming-fans.com/2018/03/swgoh-best-mods-darth-sion/", n: "Darth Sion"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-chief-chirpa/", n: "Chief Chirpa"),
    Entry(u: "https://gaming-fans.com/2018/04/swgoh-best-mods-for-darth-traya/", n: "Darth Traya"),
    Entry(u: "https://gaming-fans.com/2017/08/swgoh-best-mods-chief-nebit/", n: "Chief Nebit"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-for-darth-vader/", n: "Darth Vader"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-chirrut-imwe/", n: "Chirrut"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-death-trooper/", n: "Death Trooper"),
    Entry(u: "https://gaming-fans.com/2017/06/swgoh-best-mods-chopper/", n: "Chopper"),
    Entry(u: "https://gaming-fans.com/2017/11/swgoh-best-mods-dengar/", n: "Dengar"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-clone-sergeant-phase-i/", n: "Clone Sergeant"),
    Entry(u: "https://gaming-fans.com/2017/05/swgoh-best-mods-director-krennic/", n: "Director Krennic"),
    Entry(u: "https://gaming-fans.com/2017/08/swgoh-best-mods-clone-wars-chewbacca/", n: "Clone Wars Chewbacca"),
    Entry(u: "https://gaming-fans.com/2019/03/swgoh-best-mods-for-droideka/", n: "Droideka"),
    Entry(u: "https://gaming-fans.com/2017/08/swgoh-best-mods-commander-luke-skywalker/", n: "Commander Luke Skywalker"),
    Entry(u: "https://gaming-fans.com/2018/09/swgoh-best-mods-for-embo/", n: "Embo"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-coruscant-underworld-police/", n: "Coruscant Underworld Police"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-emperor-palpatine/", n: "Emperor Palpatine"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-ct-21-0408-echo/", n: "CT-21-0408"),
    Entry(u: "https://gaming-fans.com/2018/01/swgoh-best-mods-first-order-executioner/", n: "First Order Executioner"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-ct-5555-fives/", n: "CT-5555"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-first-order-officer/", n: "First Order Officer"),
    Entry(u: "https://gaming-fans.com/2017/09/swgoh-best-mods-ct-7567-rex/", n: "CT-7567"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-first-order-stormtrooper/", n: "First Order Stormtrooper"),
    Entry(u: "https://gaming-fans.com/2017/09/swgoh-best-mods-dathcha/", n: "Dathcha"),
    Entry(u: "https://gaming-fans.com/2017/11/swgoh-best-mods-first-order-special-forces-tie-pilot/", n: "FOSFTP"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-eeth-koth/", n: "Eeth Koth"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-first-order-tie-pilot/", n: "FOTP"),
    Entry(u: "https://gaming-fans.com/2018/06/swgoh-best-mods-for-enfys-nest/", n: "Enfys Nest"),
    Entry(u: "https://gaming-fans.com/2018/06/swgoh-best-mods-for-gamorrean-guard/", n: "Gamorrean Guard"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-ewok-elder/", n: "Ewok Elder"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-gar-saxon/", n: "Gar Saxon"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-ewok-scout/", n: "Ewok Scout"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-ezra-bridger/", n: "Ezra Bridger"),
    Entry(u: "https://gaming-fans.com/2019/12/swgoh-best-mods-for-general-hux/", n: "General Hux"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-finn/", n: "Finn"),
    Entry(u: "https://gaming-fans.com/2017/06/swgoh-best-mods-general-kenobi/", n: "General Kenobi"),
    Entry(u: "https://gaming-fans.com/2019/10/swgoh-best-mods-for-general-skywalker/", n: "General Skywalker"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-general-veers/", n: "Veers"),
    Entry(u: "https://gaming-fans.com/2017/08/swgoh-best-mods-grand-master-yoda/", n: "Grand Master Yoda"),
    Entry(u: "https://gaming-fans.com/2019/07/swgoh-best-mods-for-geonosian-brood-alpha/", n: "Geonosian Brood Alpha"),
    Entry(u: "https://gaming-fans.com/2020/04/swgoh-best-mods-for-greef-karga/", n: "Greef Karga"),
    Entry(u: "https://gaming-fans.com/2017/06/swgoh-best-mods-geonosian-soldier/", n: "Geonosian Soldier"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-han-solo/", n: "Han Solo"),
    Entry(u: "https://gaming-fans.com/2018/01/swgoh-best-mods-geonosian-spy/", n: "Geonosian Spy"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-hera-syndulla/", n: "Hera Syndulla"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-grand-admiral-thrawn/", n: "Grand Admiral Thrawn"),
    Entry(u: "https://gaming-fans.com/2017/09/swgoh-best-mods-hermit-yoda/", n: "Hermit Yoda"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-grand-moff-tarkin/", n: "Grand Moff Tarkin"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-hoth-rebel-scout/", n: "Hoth Rebel Scout"),
    Entry(u: "https://gaming-fans.com/2017/12/swgoh-best-mods-greedo/", n: "Greedo"),
    Entry(u: "https://gaming-fans.com/2017/08/swgoh-best-mods-hoth-rebel-soldier/", n: "Hoth Rebel Soldier"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-hk-47/", n: "HK-47"),
    Entry(u: "https://gaming-fans.com/2020/11/20/swgoh-best-mods-for-ig-11/", n: "IG-11"),
    Entry(u: "https://gaming-fans.com/2017/11/swgoh-best-mods-ig-100-magnaguard/", n: "IG-100 MagnaGuard"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-ima-gun-di/", n: "Ima-Gun Di"),
    Entry(u: "https://gaming-fans.com/2017/06/swgoh-best-mods-ig-86/", n: "IG-86"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-jawa/", n: "Jawa"),
    Entry(u: "https://gaming-fans.com/2017/06/swgoh-best-mods-ig-88/", n: "IG-88"),
    Entry(u: "https://gaming-fans.com/2017/09/swgoh-best-mods-jawa-engineer/", n: "Engineer"),
    Entry(u: "https://gaming-fans.com/2017/12/swgoh-best-mods-imperial-probe-droid/", n: "Imperial Probe Droid"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-jawa-scavenger/", n: "JAWASCAVENGER"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-imperial-super-commando/", n: "Imperial Super Commando"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-jedi-consular/", n: "Jedi Consular"),
    Entry(u: "https://gaming-fans.com/2018/09/swgoh-best-mods-for-jango-fett/", n: "Jango Fett"),
    Entry(u: "https://gaming-fans.com/2017/09/swgoh-best-mods-jedi-knight-anakin/", n: "Jedi Knight Anakin"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-kylo-ren/", n: "Kylo Ren"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-jedi-knight-guardian/", n: "Jedi Knight Guardian"),
    Entry(u: "https://gaming-fans.com/2017/11/swgoh-best-mods-kylo-ren-unmasked/", n: "KRU"),
    Entry(u: "https://gaming-fans.com/2020/06/swgoh-best-mods-for-jedi-knight-luke-skywalker/", n: "Jedi Knight Luke Skywalker"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-magmatrooper/", n: "Magmatrooper"),
    Entry(u: "https://gaming-fans.com/2018/10/swgoh-best-mods-for-jedi-knight-revan/", n: "Jedi Knight Revan"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-mob-enforcer/", n: "Mob Enforcer"),
    Entry(u: "https://gaming-fans.com/2018/07/swgoh-best-mods-for-jolee-bindo/", n: "Jolee Bindo"),
    Entry(u: "https://gaming-fans.com/2020/12/04/swgoh-best-mods-for-moff-gideon/", n: "Moff Gideon"),
    Entry(u: "https://gaming-fans.com/2018/12/swgoh-best-mods-for-juhani/", n: "Juhani"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-mother-talzin/", n: "Mother Talzin"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-jyn-erso/", n: "Jyn Erso"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-nightsister-acolyte/", n: "Nightsister Acolyte"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-k2/", n: "K-2SO"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-nightsister-initiate/", n: "Nightsister Initiate"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-kanan-jarrus/", n: "Kanan Jarrus"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-nightsister-spirit/", n: "Nightsister Spirit"),
    Entry(u: "https://gaming-fans.com/2020/06/swgoh-best-mods-for-ki-adi-mundi/", n: "Ki-Adi-Mundi"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-nightsister-zombie/", n: "Nightsister Zombie"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-kit-fisto/", n: "Kit Fisto"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-nute-gunray/", n: "Nute Gunray"),
    Entry(u: "https://gaming-fans.com/2020/11/09/swgoh-best-mods-for-kuiil/", n: "Kuiil"),
    Entry(u: "https://gaming-fans.com/2018/09/swgoh-best-mods-for-l3-37/", n: "L3-37"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-lando-calrissian/", n: "Lando Calrissian"),
    Entry(u: "https://gaming-fans.com/2018/06/swgoh-best-mods-for-lobot/", n: "Lobot"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-old-daka/", n: "Old Daka"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-logray/", n: "Logray"),
    Entry(u: "https://gaming-fans.com/2018/01/swgoh-best-mods-poggle-lesser/", n: "Poggle The Lesser"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-luke-skywalker/", n: "Luke Skywalker (Farmboy)"),
    Entry(u: "https://gaming-fans.com/2018/06/swgoh-best-mods-for-range-trooper/", n: "Range Trooper"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-luminara-unduli/", n: "Luminara Unduli"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-royal-guard/", n: "Royal Guard"),
    Entry(u: "https://gaming-fans.com/2018/01/swgoh-best-mods-mace-windu/", n: "Mace Windu"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-savage-opress/", n: "Savage Opress"),
    Entry(u: "https://gaming-fans.com/2018/09/swgoh-best-mods-for-mission-vao/", n: "Mission Vao"),
    Entry(u: "https://gaming-fans.com/2017/08/swgoh-best-mods-shoretrooper/", n: "Shoretrooper"),
    Entry(u: "https://gaming-fans.com/2020/07/swgoh-best-mods-for-mon-mothma/", n: "Mon Mothma"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-sith-assassin/", n: "Sith Assassin"),
    Entry(u: "https://gaming-fans.com/2017/08/swgoh-best-mods-obi-wan-old-ben-kenobi/", n: "Obi-Wan Kenobi (Old Ben)"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-sith-marauder/", n: "Sith Marauder"),
    Entry(u: "https://gaming-fans.com/2019/05/swgoh-best-mods-for-padme-amidala/", n: "Padme"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-sith-trooper/", n: "Sith Empire Trooper"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-pao/", n: "Pao"),
    Entry(u: "https://gaming-fans.com/2019/12/swgoh-best-mods-for-first-order-sith-trooper/", n: "Sith Trooper"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-paploo/", n: "Paploo"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-snowtrooper/", n: "Snowtrooper"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-plo-koon/", n: "Plo Koon"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-stormtrooper/", n: "Stormtrooper"),
    Entry(u: "https://gaming-fans.com/2017/02/swgoh-best-mods-for-poe-dameron/", n: "Poe Dameron"),
    Entry(u: "https://gaming-fans.com/2020/04/swgoh-best-mods-for-supreme-leader-kylo-ren-galactic-legend/", n: "Supreme Leader Kylo Ren"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-princess-leia/", n: "Leia"),
    Entry(u: "https://gaming-fans.com/2017/01/swgoh-best-mods-for-sun-fac/", n: "Sun Fac"),
    Entry(u: "https://gaming-fans.com/2018/07/swgoh-best-mods-for-qira/", n: "Qira"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-qui-gon-jinn/", n: "Qui-Gon Jinn"),
    Entry(u: "https://gaming-fans.com/2017/03/swgoh-best-mods-for-talia/", n: "Talia"),
    Entry(u: "https://gaming-fans.com/2017/05/swgoh-best-mods-r2-d2/", n: "R2-D2"),
    Entry(u: "https://gaming-fans.com/2017/01/swgoh-best-mods-for-tie-fighter-pilot/", n: "TFP"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-rebel-officer-leia-organa-rolo/", n: "ROLO"),
    Entry(u: "https://gaming-fans.com/2018/07/swgoh-best-mods-for-tusken-raider/", n: "Tusken Raider"),
    Entry(u: "https://gaming-fans.com/2019/12/swgoh-best-mods-for-resistance-hero-finn/", n: "Resistance Hero Finn"),
    Entry(u: "https://gaming-fans.com/2018/04/swgoh-best-mods-for-tusken-shaman/", n: "Tusken Shaman"),
    Entry(u: "https://gaming-fans.com/2020/01/swgoh-best-mods-for-resistance-hero-poe/", n: "Resistance Hero Poe"),
    Entry(u: "https://gaming-fans.com/2018/07/swgoh-best-mods-for-urorrurrr/", n: "URoRRuR'R'R"),
    Entry(u: "https://gaming-fans.com/2017/08/swgoh-best-mods-resistance-pilot/", n: "Resistance Pilot"),
    Entry(u: "https://gaming-fans.com/2018/01/swgoh-best-mods-wampa/", n: "Wampa"),
    Entry(u: "https://gaming-fans.com/2017/06/swgoh-best-mods-resistance-trooper/", n: "Resistance Trooper"),
    Entry(u: "https://gaming-fans.com/2019/10/swgoh-best-mods-for-wat-tambor/", n: "Wat Tambor"),
    Entry(u: "https://gaming-fans.com/2017/11/swgoh-best-mods-zam-wesell/", n: "Zam Wesell"),
    Entry(u: "https://gaming-fans.com/2017/12/swgoh-best-mods-rey-jedi-training/", n: "Rey (Jedi Training)"),
    Entry(u: "https://gaming-fans.com/2017/10/swgoh-best-mods-rey/", n: "Rey (Scavenger)"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-rose-tico/", n: "Rose Tico"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-sabine-wren/", n: "Sabine Wren"),
    Entry(u: "https://gaming-fans.com/2017/08/swgoh-best-mods-scarif-rebel-pathfinder/", n: "Scarif Rebel Pathfinder"),
    Entry(u: "https://gaming-fans.com/2019/06/swgoh-best-mods-for-shaak-ti/", n: "Shaak Ti"),
    Entry(u: "https://gaming-fans.com/2017/06/swgoh-best-mods-stormtrooper-han/", n: "Stormtrooper Han"),
    Entry(u: "https://gaming-fans.com/2018/10/swgoh-best-mods-for-t3-m4/", n: "T3-M4"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-teebo/", n: "Teebo"),
    Entry(u: "https://gaming-fans.com/2020/05/swgoh-best-mods-for-the-mandalorian/", n: "The Mandalorian"),
    Entry(u: "https://gaming-fans.com/2020/07/swgoh-best-mods-for-threepio-chewie/", n: "Chewpio"),
    Entry(u: "https://gaming-fans.com/2017/09/swgoh-best-mods-ugnaught/", n: "Ugnaught"),
    Entry(u: "https://gaming-fans.com/2018/09/swgoh-best-mods-for-vandor-chewbacca/", n: "Vandor Chewbacca"),
    Entry(u: "https://gaming-fans.com/2017/09/swgoh-best-mods-veteran-smuggler-chewbacca/", n: "Veteran Smuggler Chewbacca"),
    Entry(u: "https://gaming-fans.com/2017/09/swgoh-best-mods-veteran-smuggler-han-solo/", n: "Veteran Smuggler Han Solo"),
    Entry(u: "https://gaming-fans.com/2018/02/swgoh-best-mods-vice-admiral-amilyn-holdo/", n: "Holdo"),
    Entry(u: "https://gaming-fans.com/2018/03/swgoh-best-mods-visas-marr/", n: "Visas Marr"),
    Entry(u: "https://gaming-fans.com/2017/06/swgoh-best-mods-wedge-antilles/", n: "Wedge Antilles"),
    Entry(u: "https://gaming-fans.com/2017/07/swgoh-best-mods-wicket-w-warrick/", n: "Wicket"),
    Entry(u: "https://gaming-fans.com/2018/05/swgoh-best-mods-for-young-han-solo/", n: "Young Han Solo"),
    Entry(u: "https://gaming-fans.com/2018/09/swgoh-best-mods-for-young-lando-calrissian/", n: "Young Lando Calrissian"),
    Entry(u: "https://gaming-fans.com/2018/09/swgoh-best-mods-for-zaalbar/", n: "Zaalbar"),
    Entry(u: "https://gaming-fans.com/2017/04/swgoh-best-mods-zeb-orrelios/", n: "Zeb"),
]

private var table: [String:[URL]] = {
    var result = [String:[URL]]()
    for d in data {
        let c = Model.Character.find(d.n)
        result[c.id, default: []].append(URL(string: d.u)!)
    }
    return result
}()

public func modRecommondations(id: String) -> [URL] {
    table[id] ?? []
}

public enum ExternalPage {
    case character(Model.Character)
    
    case characterMeta(Model.Character)
    case modMeta(Model.Character)
    case statsMeta(Model.Character)
        
    var base: String {
        switch self {
        case .character, .characterMeta, .modMeta, .statsMeta:
            return "https://swgoh.gg/characters/"
        }
    }
    
    public var urlString: String {
        switch self {
        case let .character(c):
            return base + c.urlName + "/"
        case let .characterMeta(c):
            return base + c.urlName + "/data/"
        case let .modMeta(c):
            return base + c.urlName + "/data/mods"
        case let .statsMeta(c):
            return base + c.urlName + "/data/stats"
        }
    }
}
