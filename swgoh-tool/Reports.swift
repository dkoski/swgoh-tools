//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SWGOH

/// a report suitable for producing farming assignments -- use this with --filter-farm
struct FarmReport : Report {
    
    func report(player: Model.Player, evaluations: [SquadEvaluation]) -> String {
        var result = ""

        let firstSquad = evaluations[0]
        if evaluations.count == 1 {
            result += "**\(player.name)** \(firstSquad.squad.name) \(Int(firstSquad.scorePercent * 100))/100 \(firstSquad.status.viability)\n"
        } else {
            result += "**\(player.name)** \(firstSquad.status.viability)\n"
        }
        
        var index = 1
        for squadEv in evaluations {
            if evaluations.count > 1 {
                result += "Variation \(index)/\(evaluations.count) \(squadEv.squad.name) \(Int(squadEv.scorePercent * 100))/100 \(squadEv.preference > 0 ? "(preferred) " : "")\(squadEv.status.marker)\n"
            }
            
            var oks = [Model.Unit]()
            let appendOks = {
                result += oks.map { "*\($0.commonName)*" }.joined(separator: ", ")
                result += ": OK\n"
                oks.removeAll()
            }
            
            for (unit, unitEv) in squadEv.unitEvaluations {
                if unitEv.status == .ok {
                    // collect up OK ones in a row
                    oks.append(unit)
                    
                } else {
                    if !oks.isEmpty {
                        appendOks()
                    }
                    
                    result += "*\(unit.commonName)*:\n"
                    for message in unitEv.messages.sorted() where message.status != .ok {
                        result += "\t\(message.message) \(message.status.marker)\n"
                    }
                }
            }
            if !oks.isEmpty {
                appendOks()
            }

            result += "\n"
            
            index += 1
        }

        return result
    }

}

struct CriticalFarmReport : Report {
    
    func report(player: Model.Player, evaluations: [SquadEvaluation]) -> String {
        let lowestStatus = evaluations.map { $0.status }.min()
        if let lowestStatus = lowestStatus, lowestStatus > .needsRequiredGear {
            return ""
        }

        var result = ""
        
        let firstSquad = evaluations[0]
        if evaluations.count == 1 {
            result += "**\(player.name)** \(firstSquad.squad.name) \(Int(firstSquad.scorePercent * 100))/100 \(firstSquad.status.viability)\n"
        } else {
            result += "**\(player.name)** \(firstSquad.status.viability)\n"
        }
        
        var index = 1
        for squadEv in evaluations {
            if evaluations.count > 1 {
                result += "Variation \(index)/\(evaluations.count) \(squadEv.squad.name) \(Int(squadEv.scorePercent * 100))/100 \(squadEv.preference > 0 ? "(preferred) " : "")\(squadEv.status.marker)\n"
            }
            
            for (unit, unitEv) in squadEv.unitEvaluations {
                result += "*\(unit.commonName)*:\n"
                for message in unitEv.messages.sorted() where message.status <= Status.needsRequiredGear {
                    result += "\t\(message.message) \(message.status.marker)\n"
                }
            }
            
            result += "\n"
            
            index += 1
        }
        
        return result
    }
    
}

/// a report that outputs info for a single squad in CSV form
struct CSVSingleSquadFarmReport : Report {
    
    var threshold = Status.needsTuning
    
    func report(data: [Model.Player:[SquadEvaluation]]) -> String {
        var result = ""
        for (_, evaluations) in data {
            if !evaluations.isEmpty {
                result += header(evaluations[0])
                result += "\n"
                break
            }
        }
        for player in data.keys.sorted() {
            result += report(player: player, evaluations: data[player]!)
            result += "\n"
        }
        return result
    }
    
    func header(_ evaluation:SquadEvaluation) -> String {
        return "Player,Score,Viability Score,Viable," + evaluation.squad.units.map { $0.commonName }.joined(separator: ",")
    }

    func report(player: Model.Player, evaluations: [SquadEvaluation]) -> String {
        var result = ""
        
        for squadEv in evaluations {
            result += player.name
            result += ","
            result += Int(squadEv.scorePercent * 100).description
            result += ","
            result += squadEv.viabilityScore.description
            result += ","
            result += squadEv.status.isViable ? "yes" : "no"
            result += ","
            
            for (_, unitEv) in squadEv.unitEvaluations {
                result += "\""
                let messages = unitEv.messages.filter { $0.status <= threshold }.sorted()
                var printedMessage = false
                for message in messages {
                    if printedMessage {
                        result += "\n"
                    }
                    result += "\(message.message) \(message.status.unicodeMarker)"
                    printedMessage = true
                }
                result += "\","
            }
        }
        
        return result
    }
    
}
