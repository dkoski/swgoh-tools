//
//  TBSquads.swift
//  swgoh
//
//  Created by David Koski on 12/2/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

/// configuration for all the squads by Section/Mission
protocol TBSquadConfiguration {
    
    var name: String { get }
    
    var importantSquadNames: [String] { get }
    
    var missionConfigurations: [TB.Key : [TBSquadMissionEstimator.Mission]] { get }
    
    /// estimates of how many waves will be passed.  a map of TB.Key : [ SQUAD_ID : [gp_breakpoint, estimated_waves] ...]
    var waveEstimates: [TB.Key:[String:[(Int, Int)]]] { get }

}

extension TBSquadConfiguration {
    
    // TODO: it would be nice if this was part of the TBSquadMissionEstimator, but missions would probably need to be hashable...
    /// prepare mission configurations (used by TBSquadMissionEstimator)
    func prepareMissionConfigurations(players: [Model.Player]) -> [TB.Key : [TBSquadMissionEstimator.Mission]] {
        
        let preparedMissionConfiguration = MemoizeCache<TB.Key, [TBSquadMissionEstimator.Mission]>()
        
        let sections = Array(missionConfigurations.keys)
        DispatchQueue.concurrentPerform(iterations: sections.count) {
            let section = sections[$0]
            
            var preparedMissions = [TBSquadMissionEstimator.Mission]()
            for var mission in missionConfigurations[section, default:[]] {
                for player in players {
                    mission.cacheSquads(player: player)
                }
                preparedMissions.append(mission)
            }
            
            preparedMissionConfiguration[section] = preparedMissions
        }
        
        return preparedMissionConfiguration.values
    }
    
}

/// this builds the collection of squads per player per section (TB.Key)
struct TBSquadBuilder {
    
    let squadConfiguration: TBSquadConfiguration
    
    /// compute squads that the player has available for the given sector
    func squadsForMission(player: Model.Player, sector: TB.Key) -> [SquadEvaluation] {
        let squadBuilders = squadConfiguration.missionConfigurations[sector, default:[]].map { $0.squadBuilder }
        let squadBuilder = MultipleSquadBuilder(squadBuilders)
        
        let squads = squadBuilder.generateSquads(units: player)

        let evaluator = SquadEvaluator(rules: [:])
        let evaluations = evaluator.evaluate(squads: squads)
        
        let filter = PreferredViabilitySortFilter().compose(ViableSquadFilter()).compose(SquadListFilter())
        let finalEvaluations = filter.filter(evaluations)
        
        return finalEvaluations
    }
    
    func evaluatePlayers(guild: Model.Guild) -> [Model.Player: [TB.Key:[SquadEvaluation]]] {
        var result = [Model.Player: [TB.Key:[SquadEvaluation]]]()

        let queue = DispatchQueue(label: "build")
        func update(player: Model.Player, sector: TB.Key, evaluations: [SquadEvaluation]) {
            queue.sync {
                result[player, default: [:]][sector] = evaluations
            }
        }
        
        DispatchQueue.concurrentPerform(iterations: guild.players.count) {
            let player = guild.players[$0]
            
            for sector in squadConfiguration.missionConfigurations.keys {
                let evaluations = squadsForMission(player: player, sector: sector)
                update(player: player, sector: sector, evaluations: evaluations)
            }
        }
        
        return result
    }

}

struct TBSquadMissionEstimator : TBMissionEstimator {
    
    var name: String { "TBSquadMissionEstimator \(boardConfiguration.name) \(squadConfiguration.name)" }
    
    struct Mission {
        let isSpecial: Bool
        let squadBuilder: SquadBuilder
        
        var squads = [Model.Player:[SquadEvaluation]]()
        
        init(isSpecial: Bool = false, _ squadBuilder: SquadBuilder) {
            self.isSpecial = isSpecial
            self.squadBuilder = squadBuilder
        }
        
        mutating func cacheSquads(player: Model.Player) {
            let missionSquads = squadBuilder.generateSquads(units: player)
            
            let evaluator = SquadEvaluator(rules: [:])
            
            // we will take the best unique named squads -- later when we figure what will be used
            // we will unique by unit
            let filter = PreferredViabilitySortFilter().compose(ViableSquadFilter()).compose(UniqueSquadFilter())

            let missionEvaluations = evaluator.evaluate(squads: missionSquads)
            let finalMissionEvaluations = filter.filter(missionEvaluations)
            
            squads[player] = finalMissionEvaluations
        }
    }

    let guild: Model.Guild
    
    let boardConfiguration: TBBoardConfiguration
    let squadConfiguration: TBSquadConfiguration
    
    var preparedMissions = [TB.Key:[Mission]]()
    
    let missionBonus: [TB.Key: Double]
    let memoizedMissionCounts = MemoizeCache<[TB.Key],[TB.Key:TBMissionStats]>()
        
    init(guild: Model.Guild, boardConfiguration: TBBoardConfiguration, squadConfiguration: TBSquadConfiguration, preparedMissions: [TB.Key:[Mission]]) {
        self.guild = guild
        self.boardConfiguration = boardConfiguration
        self.squadConfiguration = squadConfiguration
        self.preparedMissions = preparedMissions
        self.missionBonus = boardConfiguration.missionBonus
    }

    /// return the estimated number of waves completed and the number of bonus GP
    func estimateWaves(section: TB.Key, squad: Squad) -> (Int, Double) {
        let sectionWaveEstimate = squadConfiguration.waveEstimates[section]!
        let thresholds = sectionWaveEstimate[squad.properties.defenderId ?? squad.name] ?? sectionWaveEstimate["DEFAULT"]!

        let waves = thresholds.last { $0.0 <= squad.gp }?.1 ?? 0

        let fullBonus = missionBonus[section]!
        
        return (waves, fullBonus * Double(waves) / 4.0)
    }
    
    private func sortedSquadNamesFor(sectors: [TB.Key], player: Model.Player) -> [String] {
        // get the distinct teams that might go
        let allEvaluations = sectors.compactMap { preparedMissions[$0] }.flatMap { $0 }.compactMap { $0.squads[player] }.flatMap { $0 }
        let squads = UniqueSquadFilter().filter(allEvaluations)

        // figure out what the best performance from each team is
        var wavesBySquad = [String:Int]()
        
        for sector in sectors {
            for mission in preparedMissions[sector, default: []] {
                let missionEvaluations = mission.squads[player, default:[]]
                for evaluation in missionEvaluations {
                    let (waves, _) = estimateWaves(section: sector, squad: evaluation.squad)
                    if waves > 0 {
                        wavesBySquad[evaluation.squad.name] = max(wavesBySquad[evaluation.squad.name, default: 0], waves)
                    }
                }
            }
        }
        
        // now sort the squads by number of waves and prefer small squads over large (e.g. dooku + assaj)
        let sortedSquads = squads.sorted { lhs, rhs in
            let lw = wavesBySquad[lhs.squad.name, default: 0]
            let rw = wavesBySquad[rhs.squad.name, default: 0]
            
            return lw == rw ? lhs.squad.units.count < rhs.squad.units.count : lw > rw
        }
        
        // filter down to just the distinct squads
        return SquadListFilter().filter(sortedSquads).map { $0.squad.name }
    }

    fileprivate func accumulateMissions(_ sectors: [TB.Key], _ player: Model.Player, _ result: inout [TB.Key : TBMissionStats]) {       
        var availableSquadNames = sortedSquadNamesFor(sectors: sectors, player: player)

        for sector in sectors {
            if availableSquadNames.isEmpty {
                break
            }
            for mission in preparedMissions[sector, default: []] {
                if availableSquadNames.isEmpty {
                    break
                }
                
                // compute the squads for this mission
                let missionEvaluations = mission.squads[player, default:[]]
                
                // use the squads in preference order
                mission:
                for (index, squadName) in availableSquadNames.enumerated() {
                    for evaluation in missionEvaluations {
                        if evaluation.squad.name == squadName {
                            availableSquadNames.remove(at: index)
                            let (waves, points) = estimateWaves(section: sector, squad: evaluation.squad)
                            result[sector, default: TBMissionStats()].add(waves: waves, points: points)
                            break mission
                        }
                    }
                }
            }
        }
    }

    struct TBEstimateResult {
        var stats = TBMissionStats()
        var squads = [Squad]()
    }
    
    /// return a more detailed picture of the missions for a single phase for a single player
    public func missionFor(sectors: [TB.Key], player: Model.Player) -> [TB.Key : TBEstimateResult] {
        var result = [TB.Key: TBEstimateResult]()
        var availableSquadNames = sortedSquadNamesFor(sectors: sectors, player: player)
                
        for sector in sectors {
            if availableSquadNames.isEmpty {
                break
            }
            for mission in preparedMissions[sector, default: []] {
                if availableSquadNames.isEmpty {
                    break
                }
                
                // compute the squads for this mission
                let missionEvaluations = mission.squads[player, default:[]]
                
                // use the squads in preference order
                mission:
                for (index, squadName) in availableSquadNames.enumerated() {
                    for evaluation in missionEvaluations {
                        if evaluation.squad.name == squadName {
                            availableSquadNames.remove(at: index)
                            let (waves, points) = estimateWaves(section: sector, squad: evaluation.squad)
                            result[sector, default: TBEstimateResult()].squads.append(evaluation.squad)
                            result[sector, default: TBEstimateResult()].stats.add(waves: waves, points: points)
                            break mission
                        }
                    }
                }
            }
        }
        
        return result
    }
    
    func missionCountsForSectors(sectors: [TB.Key]) -> [TB.Key:TBMissionStats] {
        if let result = memoizedMissionCounts[sectors] {
            return result
        }
        
        var result = [TB.Key:TBMissionStats]()

        for player in guild.players {
            accumulateMissions(sectors, player, &result)
        }
        
        memoizedMissionCounts[sectors] = result
        
        return result
    }

}
