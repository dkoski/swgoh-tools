//
//  main.swift
//  geoTB
//
//  Created by David Koski on 8/18/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation

protocol TBBoardConfiguration {
    var name: String { get }
    var board: [TB.Section] { get }
}

extension TBBoardConfiguration {
        
    var missionBonus: [TB.Key: Double] {
        return Dictionary(uniqueKeysWithValues: self.board.map { return ($0.key, $0.mission )})
    }
    
}

protocol TBMissionEstimator {
    
    var name: String { get }

    /// estimate the number of missions possible for the given sectors (a single phase)
    func missionCountsForSectors(sectors: [TB.Key]) -> [TB.Key:TBMissionStats]

}

/// count how many missions a player should be able to attempt for a given set of sectors
struct TBMissionStats {
    var attempts = 0
    var waves = 0
    var points = 0.0
    
    mutating func add(waves: Int, points: Double) {
        self.attempts += 1
        self.waves += waves
        self.points += points
    }
}

struct TB {

    let toonGP: Int
    let shipGP: Int
    
    let missionFactor: (Int) -> Double
    let deployFactor: (Int) -> Double
    let planComparator: (Board, Board) -> Bool?
    
    let configuration: TBBoardConfiguration
    let missionEstimator: TBMissionEstimator
    
    init(toonGP: Int, shipGP: Int, configuration: TBBoardConfiguration, missionEstimator: TBMissionEstimator, deployFactor: @escaping (Int) -> Double = { _ in return 1.0 }, missionFactor: @escaping (Int) -> Double = { _ in return 1.0 }, planComparator: @escaping (Board, Board) -> Bool? = { _, _ in return nil }) {
        self.toonGP = toonGP
        self.shipGP = shipGP
        self.configuration = configuration
        self.missionEstimator = missionEstimator
        self.missionFactor = missionFactor
        self.deployFactor = deployFactor
        self.planComparator = planComparator
    }

    struct Section {
        let phase: Int
        let name: String
        let thresholds: [Int]
        let mission: Double
        let enable: [String]
        
        var key: Key { return Key(phase, name) }

        var index = -1
        var enableIndexes = [Int]()
        
        var active = false
        var gp = 0
        
        init(phase: Int, name: String, thresholds: [Int], mission: Double, enable: [String]) {
            self.phase = phase
            self.name = name
            self.thresholds = thresholds
            self.mission = mission
            self.enable = enable
        }
        
        var isFleet: Bool { return name == "Air" || name == "Top" }
        
        func remaining(stars: Int) -> Int {
            let index = stars == 0 ? 0 : stars - 1
            let remaining = thresholds[index] - gp
            
            if remaining > 0 {
                if stars == 0 {
                    return remaining - 1
                } else {
                    return remaining
                }
            } else {
                return 0
            }
        }
        
        var stars: Int {
            var stars = 0
            for threshold in thresholds {
                if gp >= threshold {
                    stars += 1
                }
            }
            return stars
        }
    }

    struct TargetStars {
        
        var target = [Int]()
        
        init(count: Int) {
            target = [Int](repeating: 0, count: count)
        }
        
        mutating func increment(index: Int) -> Bool {
            if index == target.count {
                return false
            }
            
            target[index] += 1
            if target[index] < 4 {
                return true
            } else {
                target[index] = 0
                return increment(index: index + 1)
            }
        }
        
        mutating func next() -> Bool {
            return increment(index: 0)
        }
        
        var toArray: [TargetStars] {
            var copy = self
            var result = [copy]
            
            while copy.increment(index: 0) {
                result.append(copy)
            }
            
            return result
        }
    }
    
    func simulate() -> Board {
        return simulate(board: Board(configuration: configuration), phase: 1)
    }

    func simulate(board: Board, phase: Int) -> Board {
        if phase == 5 {
            return board
        }
            
        let activeSections = board.activeSections()
        
        // build all permutations of stars
        let targetStarsArray = TargetStars(count: activeSections.count).toArray
        
        // keep track of the best we have seen
        var maxBoard = board
        var maxStars = board.stars
        
        // we are going to evaluate the configurations in multiple threads -- this is how we update the max
        let queue = DispatchQueue(label: "build")
        func update(board: Board) {
            queue.sync {
                // keep track of the best we see
                let finishedStars = board.stars
                var takeNewMax = false
                
                // see if the injected comparator wants to keep it -- nil means defer to normal rules, false means reject
                if let result = planComparator(board, maxBoard) {
                    if result {
                        takeNewMax = true
                    } else {
                        return
                    }
                }
                
                // favor layouts that have more leeway or are closer to getting
                // an extra star
                if finishedStars == maxStars {
                    if board.lowestRemaining > maxBoard.lowestRemaining {
                        takeNewMax = true
                    }
                    if !takeNewMax && board.closeCount > maxBoard.closeCount {
                        takeNewMax = true
                    }
                }
                
                if board.stars > maxBoard.stars {
                    takeNewMax = true
                }
                
                if takeNewMax {
                    maxBoard = board
                    maxStars = finishedStars
                }
            }
        }
        
        /// given the target sectors, how many missions can we do?
        let phaseMissions = missionEstimator.missionCountsForSectors(sectors: activeSections.map({$0.key}))

        // attempt the target stars.  return true if we should skip, along with how many
        // ship and toon GP are left
        func attemptActiveSections(targetStars: TB.TargetStars, tryBoard: inout TB.Board) -> (Bool, Int, Int) {
            
            var remainingShipGP = Int(Double(shipGP) * deployFactor(phase))
            var remainingToonGP = Int(Double(toonGP) * deployFactor(phase))
                        
            for (index, section) in activeSections.enumerated() {
                let target = targetStars.target[index]
                var remaining = section.remaining(stars: target)
                let possibleMissions = Int((phaseMissions[section.key]?.points ?? 0) * missionFactor(phase))
                
                if remaining > possibleMissions {
                    // we need both missions and deployments to get the target stars
                    tryBoard.missions(section: section, gp: possibleMissions)
                    remaining -= possibleMissions
                    
                    if section.isFleet {
                        if remaining > remainingShipGP {
                            tryBoard.deploy(section: section, gp: remainingShipGP)
                            remainingShipGP = 0
                            
                        } else {
                            tryBoard.deploy(section: section, gp: remaining)
                            remainingShipGP -= remaining
                        }
                        
                    } else {
                        if remaining > remainingToonGP {
                            tryBoard.deploy(section: section, gp: remainingToonGP)
                            remainingToonGP = 0
                            
                        } else {
                            tryBoard.deploy(section: section, gp: remaining)
                            remainingToonGP -= remaining
                        }
                    }
                    
                } else {
                    // we can hit the mark with just missions
                    tryBoard.missions(section: section, gp: remaining)
                }
                
                if tryBoard.stars(section: section) != target {
                    // failed to make it, we can prune this
                    return (true, remainingShipGP, remainingToonGP)
                }
            }
            return (false, remainingShipGP, remainingToonGP)
        }
        
        // try the last target (all three stars) -- if we can beat that,
        // we dont need to consider the other configurations
        var failedThreeStars: Bool
        do {
            let targetStars = targetStarsArray.last!
            
            var tryBoard = board
            tryBoard.activity.append(.active(phase, activeSections))

            let remainingShipGP, remainingToonGP: Int
            (failedThreeStars, remainingShipGP, remainingToonGP) = attemptActiveSections(targetStars: targetStars, tryBoard: &tryBoard)
            
            if !failedThreeStars {
                tryBoard.activity.append(.remaining(phase, remainingToonGP, remainingShipGP))

                // try the next phase
                let nextBoard = tryBoard.nextPhase()
                let finishedBoard = simulate(board: nextBoard, phase: phase + 1)
                
                update(board: finishedBoard)
            }
        }
        
        if failedThreeStars {
            // we can't max out the phase, try the others
            DispatchQueue.concurrentPerform(iterations: targetStarsArray.count - 1) {
                let targetStars = targetStarsArray[$0]

                // apply the target stars
                var tryBoard = board
                            
                tryBoard.activity.append(.active(phase, activeSections))
                
                var (skip, remainingShipGP, remainingToonGP) = attemptActiveSections(targetStars: targetStars, tryBoard: &tryBoard)
                                        
                // prune out useless configurations, e.g. where we aimed too low
                if !skip {
                    for (index, section) in activeSections.enumerated() {
                        let target = targetStars.target[index]
                        if target == 0 {
                            // fine we are holding it
                        } else if target == 3 {
                            // we maxed it out and since we are not skipping (for failing to meet it), this is ok too
                        } else {
                            if section.isFleet && remainingShipGP > 0 && remainingShipGP > section.remaining(stars: target + 1) {
                                // we can make the next star, do not consider this one
                                skip = true
                                break
                            }
                            
                            if !section.isFleet && remainingToonGP > 0 && remainingToonGP > section.remaining(stars: target + 1) {
                                // we can make the next star, do not consider this one
                                skip = true
                                break
                            }
                        }
                    }
                }
                
                if !skip {
                    tryBoard.activity.append(.remaining(phase, remainingToonGP, remainingShipGP))

                    // try the next phase
                    let nextBoard = tryBoard.nextPhase()
                    let finishedBoard = simulate(board: nextBoard, phase: phase + 1)
                    
                    update(board: finishedBoard)
                }
                
            }
        }
        
        return maxBoard
    }
}

/// board
extension TB {
    
    struct Board : CustomStringConvertible {
        
        var board: [Section]
        var activity = [Activity]()
        
        var currentPhase = 1
        
        var stars = 0
        
        /// how many sections are close to being won
        var closeCount: Int {
            var count = 0
            var remaining = [Int:Int]()
            for item in activity {
                if case .remaining(let phase, let toonGP, _) = item {
                    remaining[phase] = toonGP
                }
            }
            
            for item in activity {
                if case .active(let phase, let sections) = item {
                    let r = remaining[phase, default: 0]
                    if r > 20 {
                        for section in sections where section.stars != 3 && !section.isFleet {
                            let needed = section.remaining(stars: section.stars + 1)
                            if r * 12 >= needed * 10 {
                                count += 1
                                break
                            }
                        }
                    }
                }
            }
            
            return count
        }
        
        var lowestRemaining: Int {
            var minimum = Int.max
            for item in activity {
                if case .remaining(_, let toonGP, let shipGP) = item {
                    minimum = min(minimum, min(toonGP, shipGP))
                }
            }
            return minimum
        }
        
        init(configuration: TBBoardConfiguration) {
            board = configuration.board
            
            // pre-built indices
            for i in board.indices {
                board[i].index = i
            }
            
            for i in board.indices {
                var enableIndexes = [Int]()
                for name in board[i].enable {
                    let phase = board[i].phase + 1
                    
                    for section in board {
                        if section.phase == phase && section.name == name {
                            enableIndexes.append(section.index)
                            break
                        }
                    }
                }
                
                board[i].enableIndexes = enableIndexes
            }
        }
        
        mutating func _nextPhase() {
            for section in board {
                if section.active {
                    activity.append(.phase(currentPhase, section))
                    if section.stars > 0 {
                        board[section.index].active = false
                        for nextSectionIndex in section.enableIndexes {
                            if board[nextSectionIndex].stars == 0 {
                                board[nextSectionIndex].active = true
                            }
                        }
                    }
                }
            }
            
            currentPhase += 1
        }
        
        func nextPhase() -> Board {
            var newBoard = self
            newBoard._nextPhase()
            return newBoard
        }
        
        func activeSections() -> [Section] {
            return board.filter { $0.active }
        }

        /// update a section gp and keep the board stars up-to-date
        @inline(__always)
        private mutating func updateGP(index: Int, gp: Int) -> Int {
            let beforeStars = board[index].stars
            board[index].gp += gp
            let afterStars = board[index].stars
            stars = stars + afterStars - beforeStars
            return board[index].gp
        }
        
        mutating func deploy(section: Section, gp: Int) {
            if gp > 0 {
                let newGP = updateGP(index: section.index, gp: gp)
                activity.append(.deploy(currentPhase, section, gp, newGP))
            }
        }
        
        mutating func missions(section: Section, gp: Int) {
            if gp > 0 {
                let newGP = updateGP(index: section.index, gp: gp)
                activity.append(.missions(currentPhase, section, gp, newGP))
            }
        }
        
        func stars(section: Section) -> Int {
            let index = section.index
            return board[index].stars
        }
        
        subscript(_ key: Key) -> Section {
            for section in board {
                if section.key == key {
                    return section
                }
            }
            fatalError()
        }
        
        var description: String {
            return
                """
                Stars: \(stars)
                Activity:
                \(activity.map { $0.description }.joined(separator: "\n"))
                """
        }
    }

}

/// utility classes
extension TB {
    
    struct Key : Hashable, Comparable, CustomStringConvertible {
        
        let phase: Int
        let name: String
        
        init(_ phase: Int, _ name: String) {
            self.phase = phase
            self.name = name
        }
        
        var isFleet: Bool { return name == "Air" || name == "Top" }
        
        var description: String {
            return "\(phase) \(name)"
        }
        
        static func < (lhs: TB.Key, rhs: TB.Key) -> Bool {
            if lhs.phase == rhs.phase {
                return lhs.name < rhs.name
            }
            
            return lhs.phase < rhs.phase
        }

    }
    
    enum Activity: CustomStringConvertible {
        case active(Int, [Section])
        case deploy(Int, Section, Int, Int)
        case missions(Int, Section, Int, Int)
        case remaining(Int, Int, Int)
        case phase(Int, Section)
        
        var phase: Int {
            switch self {
            case .active(let phase, _):
                return phase
            case .deploy(let phase, _, _, _):
                return phase
            case .missions(let phase, _, _, _):
                return phase
            case .remaining(let phase, _, _):
                return phase
            case .phase(let phase, _):
                return phase
            }
        }
        
        var section: Key? {
            switch self {
            case .active(_, _):
                return nil
            case .deploy(_, let section, _, _):
                return section.key
            case .missions(_, let section, _, _):
                return section.key
            case .remaining(_, _, _):
                return nil
            case .phase(_, let section):
                return section.key
            }
        }

        var description: String {
            switch self {
            case .active(let phase, let sections):
                return "phase \(phase): active \(sections.map { $0.key.description }.joined(separator:", "))"
            case .deploy(let phase, let section, let gp, let finalGP):
                return "phase \(phase): deploy \(section.key) \(gp) -> \(finalGP)m"
            case .missions(let phase, let section, let gp, let finalGP):
                return "phase \(phase): missions \(section.key) \(gp) -> \(finalGP)m"
            case .remaining(let phase, let toonGP, let shipGP):
                return "phase \(phase): remaining GP = \(toonGP) toons, \(shipGP) ships"
            case .phase(let phase, let section):
                if section.stars == 0 {
                    return "phase \(phase): \(section.key) HOLD AT ZERO STARS"
                } else if section.stars == 3 {
                    return "phase \(phase): \(section.key) -> \(section.stars) ⭐"
                } else {
                    return "phase \(phase): \(section.key) -> \(section.stars) ⭐ -- \(section.remaining(stars: section.stars + 1)) to the next"
                }
            }
        }
        
        var discordDescription: String {
            switch self {
            case .active(let phase, let sections):
                return "**phase \(phase): active** \(sections.map { $0.key.description }.joined(separator:", "))"
            case .deploy(let phase, let section, let gp, let finalGP):
                return "phase \(phase): **deploy** \(section.key) \(gp) -> \(finalGP)m"
            case .missions(let phase, let section, let gp, let finalGP):
                return "phase \(phase): **missions** \(section.key) \(gp) -> \(finalGP)m"
            case .remaining(let phase, let toonGP, let shipGP):
                return "phase \(phase): remaining GP = \(toonGP) toons, \(shipGP) ships"
            case .phase(let phase, let section):
                if section.stars == 0 {
                    return "**phase \(phase): \(section.key) HOLD AT ZERO :star:**"
                } else if section.stars == 3 {
                    return "**phase \(phase): \(section.key) -> \(section.stars) :star:**"
                } else {
                    return "**phase \(phase): \(section.key) -> \(section.stars) :star:**"
                }
            }
        }

        var htmlDescription: String {
            switch self {
            case .active(_, let sections):
                return "<b>active</b> \(sections.map { $0.key.description }.joined(separator:", "))"
            case .deploy( _, _, let gp, let finalGP):
                return "<b>deploy</b> \(gp) -> \(finalGP)m"
            case .missions( _, _, let gp, let finalGP):
                return "<b>missions</b> \(gp) -> \(finalGP)m"
            case .remaining( _, let toonGP, let shipGP):
                return "<b>remaining GP:</b> \(toonGP) toons, \(shipGP) ships"
            case .phase( _, let section):
                if section.stars == 0 {
                    return "<b>HOLD AT ZERO &#11088;</b>"
                } else if section.stars == 3 {
                    return "\(section.stars) &#11088;"
                } else {
                    return "\(section.stars) &#11088; -- \(section.remaining(stars: section.stars + 1)) to the next"
                }
            }
        }

    }

}
