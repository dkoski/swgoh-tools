//
//  GeoTB.swift
//  swgoh
//
//  Created by David Koski on 9/7/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

/// DS Geo TB configuration
struct DSGeoTBSquads : TBSquadConfiguration {
    
    // https://genskaar.github.io/tb_geo/html/ds.html
    
    let name = "DSGeoTBSquads"
    
    let importantSquadNames = ["Geos-Special", "Geos", "DR", "FO", "BH", "zEP", "NS", "Traya", "Grievous", "Nute", "Dooku+Asajj", "Dooku+Sep", "Chimaera", "Executrix" ]
    
    private static let nsRules = [
        "DEFAULT" : TWRule().e,
        "Talzin" : TWRule(leaderZeta: true, preference: 5).e,
        "Asajj" : TWRule(uniqueZetas: true, preference: 5).e,
        "Daka" : TWRule(preference: 3).e,
        "Zombie" : TWRule(preference: 3).e,
        "Spirit" : TWRule(preference: 3).e,
        "Acolyte" : TWRule(preference: 1).e,
        "Thrawn" : TWRule(preference: 2).e,
    ]

    private static let nsAdds = u("Spirit", "Acolyte", "Talia", "Thrawn")
    static let nightsisterBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "NS", id: "NS_MT", preference: 15, rules: nsRules), u("MOTHERTALZIN"), u("ASAJVENTRESS"), u("Zombie"), u("Daka"), nsAdds)
    
    private static let foRules = [
        "DEFAULT" : TWRule().e,
        "KRU" : TWRule(leaderZeta: true, gear: 10, preference: 2).e,
        "Hux" : TWRule(leaderZeta: true, gear: 10, preference: 2).e,
        "FOE" : TWRule(preference: 2).e,
        "FOO" : TWRule(preference: 1).e,
        "FOST" : TWRule(preference: 1).e,
        "Sith Trooper" : TWRule(preference: 2).e,
    ]

    private static let foRemainder = u(Model.Character.all.filter { !$0.ship && $0.categories.contains("First Order") }.map(\.id))
    private static let firstOrder1 = SelectUnitSquadBuilder(properties: TWProperties(name: "FO", id: "FO_KRU", rules: foRules), u("KYLORENUNMASKED"), u("KYLOREN"), foRemainder, foRemainder, foRemainder)
    private static let firstOrder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "FO", id: "FO_KRU", preference: 5, rules: foRules), u("Hux"), u("KRU"), u("KYLOREN"), foRemainder, foRemainder)
    private static let firstOrder3 = SelectUnitSquadBuilder(properties: TWProperties(name: "FO", id: "SLKR", preference: 15, rules: foRules), u("SL Kylo"), u("KRU"), u("KYLOREN"), u("Hux"), u("Sith Trooper"))
    static let firstOrderBuilder = MultipleSquadBuilder(firstOrder1, firstOrder2, firstOrder3)

    private static let epRules = [
        "Palpatine" : TWRule(leaderZeta: true).e,
        "Darth Vader" : TWRule(uniqueZetas: true).e,
        
        "Darth Revan" : TWRule(uniqueZetas: true, stars: 7, gear: 12, preference: 2).e,
        "Malak" : TWRule(uniqueZetas: true, gear: 12, preference: 2).e,
        "Thrawn" : TWRule(gear: 12, preference: 2).e,
        "Darth Traya" : TWRule(gear: 12, preference: 1).e,
        "BSF" : TWRule(gear: 12, preference: 2).e,
        "Sith Empire Trooper" : TWRule(preference: 1).e,

        "DEFAULT" : TWRule().e,
    ]
    
    private static let epAdds = u("Darth Revan", "Malak", "Thrawn", "Tarkin", "TFP", "Sith Empire Trooper", "Darth Traya", "BSF")

    static let epBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "zEP", id: "EP", preference: 2, rules: epRules), u("Palpatine"), u("Darth Vader"), epAdds, epAdds, epAdds)

    private static let grievousRules = [
        "DEFAULT" : TWRule().e,
        "GRIEVOUS" : TWRule(leaderZeta: true).e,
        "B1BATTLEDROIDV2" : TWRule(preference: 1).e,
        "B2SUPERBATTLEDROID" : TWRule(preference: 1).e,
        "MAGNAGUARD" : TWRule(preference: 1).e,
        ]

    static let ggBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "Grievous", id: "SEP_DROIDS", preference: 10, rules: grievousRules), u("GRIEVOUS"), u("B2SUPERBATTLEDROID"), u("B1"), u("Droideka", "Nute"), u("Magnaguard"))

                
    //MARK:- Squad Configuration
    
    // see also: https://jetkokos.ru or https://genskaar.github.io/tb_geo/html/ds.html
        
    let missionConfigurations: [TB.Key:[TBSquadMissionEstimator.Mission]] = {
        let defaultRules = [
            "DEFAULT" : TWRule().e,
        ]

        let darthRevanRules = [
            "DEFAULT" : TWRule().e,
            "Darth Revan" : TWRule(leaderZeta: true, uniqueZetas: true, stars: 7, gear: 10, preference: 2).e,
            "Sith Empire Trooper" : TWRule(preference: 2).e,
            "Sith Assassin" : TWRule(preference: 2).e,
            "Malak" : TWRule(uniqueZetas: true, stars: 6, gear: 10, preference: 5).e,
        ]
        
        let drAdds = u("Sith Marauder", "Sith Empire Trooper", "Sith Assassin", "Thrawn")
        let darthRevanSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "DR", id: "SITH_EMPIRE_W_MALAK", preference: 25, rules: darthRevanRules), u("Darth Revan"), u("HK"), u("Bastila Shan (Fallen)"), u("Malak"), drAdds)
        let darthRevanSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "DR", id: "SITH_EMPIRE_WO_MALAK", preference: 25, rules: darthRevanRules), u("Darth Revan"), u("HK"), u("Bastila Shan (Fallen)"), drAdds, drAdds)

        let sithEmpire = MultipleSquadBuilder(darthRevanSquadBuilder, darthRevanSquadBuilder2)

        let firstOrder = DSGeoTBSquads.firstOrderBuilder

        let bosskRules = [
            "BOSSK" : TWRule(leaderZeta: true, preference: 5).e,
            "DENGAR" : TWRule(preference: 1).e,
            "BOBAFETT" : TWRule(preference: 1).e,
            "DEFAULT" : TWRule().e,
        ]
        let jangoRules = [
            "JANGOFETT" : TWRule(leaderZeta: true, preference: 5).e,
            "BOSSK" : TWRule(preference: -1).e,
            "DENGAR" : TWRule(preference: 1).e,
            "BOBAFETT" : TWRule(preference: 1).e,
            "DEFAULT" : TWRule().e,
        ]

        let bhAdds = u(Model.Character.all.filter { !$0.ship && $0.alignment == .dark && $0.categories.contains("Bounty Hunter") }.map(\.id).removing("EMBO", "BOSSK"))

        let bhSquadBuilder = SelectUnitSquadBuilder(properties: TWProperties(name: "BH", id: "BH_BOSSK", preference: 5, rules: bosskRules), u("Bossk"), bhAdds, bhAdds, bhAdds, bhAdds)
        let bhSquadBuilder2 = SelectUnitSquadBuilder(properties: TWProperties(name: "BH", id: "BH_JANGO", preference: 5, rules: jangoRules), u("Jango"), bhAdds, bhAdds, bhAdds, bhAdds)
        
        let bountyHunters = MultipleSquadBuilder(bhSquadBuilder, bhSquadBuilder2)

        let empire = DSGeoTBSquads.epBuilder

        let ns = DSGeoTBSquads.nightsisterBuilder

        let nsNoAsajj = SelectUnitSquadBuilder(properties: TWProperties(name: "NS-NoAssaj", id: "NS_MT", preference: 15, rules: DSGeoTBSquads.nsRules), u("MOTHERTALZIN"), u("Zombie"), u("Daka"), DSGeoTBSquads.nsAdds, DSGeoTBSquads.nsAdds)

        let trayaRules = [
            "DEFAULT" : TWRule().e,
            "Traya" : TWRule(leaderZeta: true, uniqueZetas: true, stars: 7, gear: 11, preference: 2).e,
            "Sith Empire Trooper" : TWRule(preference: 2).e,
            "Dooku" : TWRule(preference: 2).e,
            "Thrawn" : TWRule(preference: -1).e,
            "BSF" : TWRule(preference: -1).e,
        ]

        let trayaAdds = u("BSF", "Count Dooku", "Palpatine", "Sith Assassin", "Sith Empire Trooper", "Thrawn")
        let sithTriumvirate = SelectUnitSquadBuilder(properties: TWProperties(name: "Traya", id: "SITH_TRI", preference: 0, rules: trayaRules), u("Traya"), u("Nihilus"), u("Sion"), trayaAdds, trayaAdds)

        let trayaAddsNoDooku = u("BSF", "Palpatine", "Sith Assassin", "Sith Empire Trooper", "Thrawn")
        let sithTriumvirateNoDooku = SelectUnitSquadBuilder(properties: TWProperties(name: "Traya", id: "SITH_TRI", preference: 0, rules: trayaRules), u("Traya"), u("Nihilus"), u("Sion"), trayaAddsNoDooku, trayaAddsNoDooku)

        let geonosiansSpecial = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Geos-Special", preference: 10, unitMinimumGP: 16500, rules: defaultRules), u("GBA"), u("Poggle"), u("Geo Soldier"), u("Sun Fac"), u("Geo Spy"))

        let geonosians = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Geos", preference: 0, rules: defaultRules), u("GBA"), u("Poggle"), u("Geo Soldier"), u("Sun Fac"), u("Geo Spy"))

        let sepDroids = DSGeoTBSquads.ggBuilder

        // MARK:-
        
        let p1n1 = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Poggle Geos", preference: 10, rules: defaultRules), u("Poggle"), u("GBA"), u("Geo Soldier"), u("Sun Fac"), u("Geo Spy"))
        
        let p1n2 = MultipleSquadBuilder(sithEmpire, firstOrder, bountyHunters, empire)

        let p1s1 = MultipleSquadBuilder(firstOrder, empire, ns, sithTriumvirate, geonosians)

        let p1sSpecial = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Nute", preference: 10, unitMinimumGP: 16500, rules: defaultRules), u("Nute"), u("B1"), u("B2"), u("Droideka"), u("Magnaguard"))

        let dookuAsajjRules = [
            "Dooku" : TWRule(uniqueZetas: true, gear: 10).e,
            "DEFAULT" : TWRule().e,
        ]

        let p2n1 = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Dooku+Asajj", preference: 10, unitMinimumGP: 16500, rules: dookuAsajjRules), u("Dooku"), u("Asajj"))

        let p2n2 = MultipleSquadBuilder(firstOrder, bountyHunters, empire, sithTriumvirateNoDooku, geonosians)

        let p2sSpecial = geonosiansSpecial
        
        let p2s1 = MultipleSquadBuilder(sithEmpire, empire, nsNoAsajj, ns, sithTriumvirateNoDooku, sepDroids)


        let p3n1 = MultipleSquadBuilder(empire, ns, sithTriumvirate)
        
        let p3n2 = sepDroids

        let p3nSpecial = geonosiansSpecial

        let p3s1 = MultipleSquadBuilder(sithEmpire, firstOrder, bountyHunters, empire, geonosians)

        let p4n1 = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Dooku+Sep", preference: 0, unitMinimumGP: 16500, rules: defaultRules), u("Dooku"), u("Grievous"), u("B2"), u("Nute"), u("B1", "Droideka"))

        let p4n2 = MultipleSquadBuilder(empire, ns, sithTriumvirateNoDooku, sepDroids, geonosians)

        let p4s1 = MultipleSquadBuilder(sithEmpire, firstOrder, bountyHunters, empire, sithTriumvirateNoDooku, geonosians)
        
        // TODO
        let p4sSpecial = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Special", preference: 5, unitMinimumGP: 16500, rules: defaultRules), u("Wat Tambor"))

        // MARK:-
        
        let fleetRules = [
            "DEFAULT" : UnitEvaluator(
                UnitRule.level85Rule,
                UnitRule.sevenStarRule
            )
        ]
        
        let chimaeraStandard = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Chimaera", preference: 10, rules: fleetRules), u("Chimaera"), u("HT"))

        let executrixNoHT = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Executrix", preference: 5, rules: fleetRules), u("Executrix"), u("B-28 Extinction-class Bomber"), u("Emperor's Shuttle"), u("Sith Fighter"), u("TIE Advanced x1"), u("Xanadu Blood"))

        let executrixStandard = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Executrix", preference: 10, rules: fleetRules), u("Executrix"), u("Geonosian Soldier's Starfighter"), u("Geonosian Spy's Starfighter"), u("Sun Fac's Geonosian Starfighter"), u("Slave I"))

        let executrixBugs = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Executrix+Bugs", preference: 6, rules: fleetRules), u("Chimaera"), u("Geonosian Soldier's Starfighter"), u("Geonosian Spy's Starfighter"), u("Sun Fac's Geonosian Starfighter"), u("Slave I"))

        let p2air1 = MultipleSquadBuilder(chimaeraStandard, executrixNoHT)

        let p2air2 = MultipleSquadBuilder(executrixStandard, executrixBugs)

        let p3air1 = MultipleSquadBuilder(chimaeraStandard, executrixNoHT)

        let p3air2 = MultipleSquadBuilder(executrixStandard, executrixBugs)

        let chimaeraBugsHT = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Chimaera+Bugs", preference: 10, rules: fleetRules), u("Chimaera"), u("HT"), u("Geonosian Soldier's Starfighter"), u("Geonosian Spy's Starfighter"), u("Sun Fac's Geonosian Starfighter"), u("Slave I"))

        let p4air1 = MultipleSquadBuilder(chimaeraBugsHT, executrixBugs)

        // some methods to help with the nested types
        
        func mission(isSpecial: Bool = false, _ squadBuilder: SquadBuilder) -> TBSquadMissionEstimator.Mission {
            return TBSquadMissionEstimator.Mission(isSpecial: isSpecial, squadBuilder)
        }
        
        return [
            TB.Key(1, "N"): [ mission(p1n1), mission(p1n2) ],
            TB.Key(1, "S"): [ mission(p1s1), mission(p1s1), mission(isSpecial: true, p1sSpecial) ],
            
            TB.Key(2, "Air"): [ mission(p2air1), mission(p2air2) ],
            TB.Key(2, "N"): [ mission(p2n1), mission(p2n2), mission(p2n2) ],
            TB.Key(2, "S"): [ mission(p2s1), mission(p2s1), mission(isSpecial: true, p2sSpecial) ],

            TB.Key(3, "Air"): [ mission(p3air1), mission(p3air2) ],
            TB.Key(3, "N"): [ mission(p3n1), mission(p3n2), mission(isSpecial: true, p3nSpecial) ],
            TB.Key(3, "S"): [ mission(p3s1), mission(p3s1) ],

            TB.Key(4, "Air"): [ mission(p4air1) ],
            TB.Key(4, "N"): [ mission(p4n1), mission(p4n2), mission(p4n2) ],
            TB.Key(4, "S"): [ mission(p4s1), mission(p4s1), mission(isSpecial: true, p4sSpecial) ],
        ]
    }()
    
    let waveEstimates: [TB.Key:[String:[(Int, Int)]]] = [
        TB.Key(1, "N") : [
            "DEFAULT" : [(90000, 1), (95000, 2), (100000, 3), (105000, 4)],
            "Poggle Geos" : [(83000, 1), (86000, 2), (90000, 3), (93000, 4)],
        ],
        TB.Key(1, "S") : [
            "DEFAULT" : [(90000, 1), (95000, 2), (100000, 3), (105000, 4)]
        ],

        TB.Key(2, "Air") : [
            "Chimaera" : [(90000, 4)],
            "DEFAULT" : [(230000, 4)],
        ],
        TB.Key(2, "N") : [
            "DEFAULT" : [(95000, 1), (100000, 2), (105000, 3), (110000, 4)],
            "Dooku+Asajj" : [(40000, 3), (44000, 4)],
            "Geos" : [(85000, 1), (88000, 2), (92000, 3), (93000, 4)],
        ],
        TB.Key(2, "S") : [
            "DEFAULT" : [(95000, 1), (100000, 2), (105000, 3), (110000, 4)],
            "Geos" : [(85000, 1), (88000, 2), (92000, 3), (93000, 4)],
        ],

        TB.Key(3, "Air") : [
            "Chimaera" : [(100000, 4)],
            "DEFAULT" : [(240000, 4)],
        ],
        TB.Key(3, "N") : [
            "DEFAULT" : [(100000, 1), (110000, 2), (115000, 3), (125000, 4)],
            "Geos" : [(87000, 1), (90000, 2), (93000, 3), (95000, 4)],
        ],
        TB.Key(3, "S") : [
            "DEFAULT" : [(100000, 1), (110000, 2), (115000, 3), (125000, 4)],
            "Geos" : [(87000, 1), (90000, 2), (93000, 3), (95000, 4)],
        ],

        TB.Key(4, "Air") : [
            "Chimaera+Bugs" : [(280000, 4)],
            "Executrix+Bugs" : [(280000, 4)],
            "DEFAULT" : [(0, 0)]
        ],
        TB.Key(4, "N") : [
            "DEFAULT" : [(125000, 1), (133000, 2), (145000, 3), (155000, 4)],
            "Dooku+Sep" : [(120000, 2), (155000, 4)],
        ],
        TB.Key(4, "S") : [
            "DEFAULT" : [(125000, 1), (133000, 2), (145000, 3), (155000, 4)],
        ],
    ]
}

/// provides a static estimate of missions based on guild GP.  probably in the ballpark, but not great
extension DSGeoTBStaticMissionEstimator {
        
    init(guild: Model.Guild) {
        self.estimates = DSGeoTBStaticMissionEstimator.defaultMission(toonGP: guild.characterGP, shipGP: guild.shipGP)
    }

}
