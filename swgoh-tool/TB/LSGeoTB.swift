//
//  LSGeoTB.swift
//  swgoh
//
//  Created by David Koski on 11/23/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

struct LSGeoTBSquads : TBSquadConfiguration {
    
    let name = "LSGeoTBSquads"
    let importantSquadNames = ["JTR", "JKR", "Padme", "Clones", "GAS", "CLS", "GL Rey", "KAM Squad", "JMLS", "JKL", "Mon Mothma", "GL Rey", "Negotiator" ]
                
    //MARK:- Squad Configuration
    
    // https://www.bobasalliance.com/a-guide-to-ls-tb-republic-offensive/
    // https://wiki.swgoh.help/wiki/Geonosis_Republic_Offensive
    // http://www.territorybattle.com/ls-elite/
    // https://genskaar.github.io/tb_geo/html/ls.html -- mainly this
            
    let missionConfigurations: [TB.Key:[TBSquadMissionEstimator.Mission]] = {
        let defaultRules = [
            // note the gear level required is much higher -- that is dealt with by the gp thresholds below
            "DEFAULT" : TWRule(gear: 10).e,
            "GK" : TWRule(uniqueZetas: true, gear: 13).e,
            "Old Ben" : TWRule(gear: 12).e,
            "C-3PO" : TWRule(gear: 10, preference: -1).e,
            "Ahsoka" : TWRule(gear: 12, preference: 1).e,
            "Chewpio" : TWRule(leaderZeta: true, preference: 1).e,
        ]
        
        let jtr = SelectUnitSquadBuilder(properties: TWProperties(name: "JTR", preference: 0, rules: defaultRules), u("JTR"), u("BB8"), u("C-3PO"), u("R2-D2"), u("Finn"))

        let jkr = SelectUnitSquadBuilder(properties: TWProperties(name: "JKR", preference: 0, rules: defaultRules), u("JKR"), u("GMY"), u("Hermit Yoda", "GAS"), u("Bastila Shan"), u("Jolee"))
        
        let jmls = SelectUnitSquadBuilder(properties: TWProperties(name: "JMLS", preference: 5, rules: defaultRules), u("JMLS"), u("JKL"), u("Hermit Yoda"), u("Old Ben"), u("Ezra"))
        let jkl = SelectUnitSquadBuilder(properties: TWProperties(name: "JKL", preference: 8, rules: defaultRules), u("JKL"), u("Hermit Yoda"), u("GMY"), u("Old Ben"), u("Aayla Secura"))

        let padme = SelectUnitSquadBuilder(properties: TWProperties(name: "Padme", preference: 2, rules: defaultRules), u("Padme"), u("Ahsoka", "Shaak Ti", "R2-D2"), u("JKA"), u("GK"), u("C-3PO", "Shaak Ti", "R2-D2", "Barriss"))

        let clones_1 = SelectUnitSquadBuilder(properties: TWProperties(name: "Clones", preference: 0, rules: defaultRules), u("Shaak Ti"), u("CT7567"), u("CT210408"), u("CLONESERGEANTPHASEI", "ARCTROOPER501ST"), u("CT5555"))
        let clones_2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Clones", preference: 0, rules: defaultRules), u("CT7567"), u("CT210408"), u("CLONESERGEANTPHASEI", "ARCTROOPER501ST", "CC2224"), u("CLONESERGEANTPHASEI", "ARCTROOPER501ST", "CC2224"), u("CT5555"))

        let gas = SelectUnitSquadBuilder(properties: TWProperties(name: "GAS", preference: 5, rules: defaultRules), u("GAS"), u("CT7567"), u("CT210408"), u("CLONESERGEANTPHASEI", "ARCTROOPER501ST"), u("CT5555"))
        
        let cls = SelectUnitSquadBuilder(properties: TWProperties(name: "CLS", preference: 0, rules: defaultRules), u("CLS"), u("Han Solo"), u("Chewbacca"), u("R2-D2", "Chewpio"), u("Old Ben", "C-3PO"))
        
        let monMothmaRules = [
            "DEFAULT" : TWRule(gear: 12).e,
            "Mon Mothma" : TWRule(leaderZeta: true).e,
            "Chirrut" : TWRule(preference: 1).e,
            "Baze" : TWRule(preference: 1).e,
            "Pao" : TWRule(preference: 2).e,
            "Bistan" : TWRule(preference: 1).e,
            "Hoth Rebel Scout" : TWRule(preference: 2).e,
            ]

        let rebelFighters = Model.Character.all.filter { !$0.ship && $0.categories.contains("Rebel Fighter") }.map(\.id)
        let monMothma = SelectUnitSquadBuilder(properties: TWProperties(name: "Mon Mothma", preference: 0, rules: monMothmaRules), u("Mon Mothma"), u("Chirrut"), u("Baze"), u(rebelFighters), u(rebelFighters))
        

        let glRey = SelectUnitSquadBuilder(properties: TWProperties(name: "GL Rey", preference: 40, rules: defaultRules), u("Rey"), u("Chewbacca", "Holdo"), u("GK"), u("Resistance Hero Poe"), u("Resistance Hero Finn"))

        // phase 2
        
        let padme2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Padme", preference: 0, unitMinimumGP: 21_0000, rules: defaultRules), u("Padme"), u("Ahsoka", "Shaak Ti", "R2-D2", "Fives"), u("JKA"), u("GK"), u("C-3PO", "Shaak Ti", "R2-D2", "Barriss", "Rex"))
        
        let gas2 = SelectUnitSquadBuilder(properties: TWProperties(name: "GAS Special", preference: 0, unitMinimumGP: 21_0000, rules: defaultRules), u("GAS"), u("Ahsoka"))
        
        // phase 3
                        
        let galacticRepublicJedi = SelectUnitSquadBuilder(properties: TWProperties(name: "GR Jedi", preference: 1, unitMinimumGP: 22_0000, rules: defaultRules), u("GK"), u("GAS"), u("GMY"), u("Ahsoka"), u("Barriss"))
        
        let kamSquad = SelectUnitSquadBuilder(properties: TWProperties(name: "KAM Squad", preference: 1, unitMinimumGP: 22_0000, rules: defaultRules), u("Shaak Ti"), u("CT7567"), u("CT210408"), u("CLONESERGEANTPHASEI", "ARCTROOPER501ST"), u("CT5555"))

        // MARK:-
                
        let p1mid1 = MultipleSquadBuilder(jtr, padme, clones_1, clones_2, gas, cls, glRey, monMothma)
        let p1mid2 = MultipleSquadBuilder(jkr, jmls, jkl)
        let p1bot1 = MultipleSquadBuilder(jtr, padme, clones_1, clones_2, gas, cls, glRey, monMothma)
        let p1bot2 = MultipleSquadBuilder(jkr, jmls, jkl)
        
        let p2mid1 = MultipleSquadBuilder(jtr, jkr, clones_1, clones_2, cls, glRey, monMothma)
        let p2mid2 = MultipleSquadBuilder(padme2)
        let p2mid3 = MultipleSquadBuilder(gas2)
        let p2bot1 = MultipleSquadBuilder(jtr, padme, clones_1, clones_2, gas, cls, glRey, monMothma)
        let p2bot2 = MultipleSquadBuilder(minimumGP: 21_000, jkr, jmls, jkl)

        let p3mid1 = MultipleSquadBuilder(jtr, jkr, padme, clones_1, clones_2, gas, cls, glRey, monMothma)
        let p3mid2 = MultipleSquadBuilder(jtr, jkr, padme, clones_1, clones_2, gas, cls, glRey, monMothma)
        let p3mid3 = MultipleSquadBuilder(galacticRepublicJedi)
        let p3bot1 = MultipleSquadBuilder(minimumGP: 22_000, jkr, jmls, jkl)
        let p3bot2 = MultipleSquadBuilder(kamSquad)

        let p4mid1 = MultipleSquadBuilder(jtr, jkr, padme, clones_1, clones_2, gas, cls, glRey, monMothma)
        let p4mid2 = MultipleSquadBuilder(minimumGP: 23_000, jkr, jmls, jkl)
        let p4bot1 = MultipleSquadBuilder(jtr, jkr, padme, clones_1, clones_2, gas, cls, glRey, monMothma)
        let p4bot2 = MultipleSquadBuilder(minimumGP: 23_000, padme)
        let p4bot3 = MultipleSquadBuilder(minimumGP: 23_000, gas)

        // MARK:-
                
        let fleetRules = [
            "DEFAULT" : UnitEvaluator(
                UnitRule.level85Rule,
                UnitRule.sevenStarRule
            )
        ]

        let negotiator = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Negotiator", preference: 10, rules: fleetRules), u("Negotiator"), u("JEDISTARFIGHTERANAKIN"), u("JEDISTARFIGHTERAHSOKATANO"), u("UMBARANSTARFIGHTER"), u("BLADEOFDORIN"), u("ARC170CLONESERGEANT"), u("YWINGCLONEWARS", "ARC170REX"))

        // some methods to help with the nested types
        
        func mission(isSpecial: Bool = false, _ squadBuilder: SquadBuilder) -> TBSquadMissionEstimator.Mission {
            return TBSquadMissionEstimator.Mission(isSpecial: isSpecial, squadBuilder)
        }
        
        return [
            TB.Key(1, "Top"): [ mission(negotiator) ],
            TB.Key(1, "Middle"): [ mission(p1mid1), mission(p1mid2) ],
            TB.Key(1, "Bottom"): [ mission(p1bot1), mission(p1bot2) ],
            
            TB.Key(2, "Top"): [ mission(negotiator) ],
            TB.Key(2, "Middle"): [ mission(p2mid1), mission(p2mid2), mission(p2mid3) ],
            TB.Key(2, "Bottom"): [ mission(p2bot1), mission(p2bot2) ],

            TB.Key(3, "Top"): [ mission(negotiator) ],
            TB.Key(3, "Middle"): [ mission(p3mid1), mission(p3mid2), mission(p3mid3) ],
            TB.Key(3, "Bottom"): [ mission(p3bot1), mission(isSpecial: true, p3bot2) ],

            TB.Key(4, "Top"): [ mission(negotiator) ],
            TB.Key(4, "Middle"): [ mission(p4mid1), mission(p4mid2) ],
            TB.Key(4, "Bottom"): [ mission(p4bot1), mission(p4bot2), mission(p4bot3) ],
        ]
    }()
    
    let waveEstimates: [TB.Key:[String:[(Int, Int)]]] = [
        TB.Key(1, "Top") : [
            "Negotiator" : [(300_000, 4)]
        ],
        TB.Key(1, "Middle") : [
            "DEFAULT" : [(130_000, 1), (138_000, 2), (143_000, 3), (150_000, 4)]
        ],
        TB.Key(1, "Bottom") : [
            "DEFAULT" : [(130_000, 1), (138_000, 2), (143_000, 3), (150_000, 4)]
        ],

        TB.Key(2, "Top") : [
            "Negotiator" : [(300_000, 4)]
        ],
        TB.Key(2, "Middle") : [
            "DEFAULT" : [(133_000, 1), (140_000, 2), (145_000, 3), (151_000, 4)],
            "Padme" : [(130_000, 2), (135_000, 3), (140_000, 4)],
        ],
        TB.Key(2, "Bottom") : [
            "DEFAULT" : [(133_000, 1), (140_000, 2), (145_000, 3), (151_000, 4)]
        ],

        TB.Key(3, "Top") : [
            "Negotiator" : [(300_000, 4)]
        ],
        TB.Key(3, "Middle") : [
            "DEFAULT" : [(136_000, 1), (144_000, 2), (150_000, 3), (158_000, 4)],
        ],
        TB.Key(3, "Bottom") : [
            "DEFAULT" : [(136_000, 1), (144_000, 2), (150_000, 3), (158_000, 4)],
        ],

        TB.Key(4, "Top") : [
            "Negotiator" : [(300_000, 4)]
        ],
        TB.Key(4, "Middle") : [
            "DEFAULT" : [(140_000, 1), (148_000, 2), (155_000, 3), (165_000, 4)],
        ],
        TB.Key(4, "Bottom") : [
            "DEFAULT" : [(140_000, 1), (148_000, 2), (155_000, 3), (165_000, 4)],
        ],
    ]
}

/// provides a static estimate of missions based on guild GP.  probably in the ballpark, but not great
extension LSGeoTBStaticMissionEstimator {
        
    init(guild: Model.Guild) {
        self.estimates = LSGeoTBStaticMissionEstimator.defaultMission(toonGP: guild.characterGP, shipGP: guild.shipGP)
    }

}
