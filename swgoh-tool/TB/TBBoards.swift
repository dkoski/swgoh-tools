//
//  TBBoards.swift
//  swgoh
//
//  Created by David Koski on 12/2/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

struct DSGeoTB : TBBoardConfiguration {

    let name = "DSGeoTB"
    var board: [TB.Section]

    init() {
        board = [
            TB.Section(phase: 1, name: "N", thresholds: [66, 84, 110], mission: 0.8, enable: [ "Air", "N" ]),
            TB.Section(phase: 1, name: "S", thresholds: [36, 68, 104], mission: 0.8, enable: [ "N", "S"]),
            TB.Section(phase: 2, name: "Air", thresholds: [27, 67, 106], mission: 0.825, enable: [ "Air" ]),
            TB.Section(phase: 2, name: "N", thresholds: [61, 95, 191], mission: 1.08, enable: [ "N" ]),
            TB.Section(phase: 2, name: "S", thresholds: [43, 89, 153], mission: 1.08, enable: [ "S"]),
            TB.Section(phase: 3, name: "Air", thresholds: [34, 92, 142], mission: 1.66, enable: [ "Air" ]),
            TB.Section(phase: 3, name: "N", thresholds: [59, 109, 168], mission: 1.352, enable: [ "N" ]),
            TB.Section(phase: 3, name: "S", thresholds: [60, 95, 172], mission: 1.352, enable: [ "S" ]),
            TB.Section(phase: 4, name: "Air", thresholds: [54, 118, 200], mission: 2.53, enable: [ ]),
            TB.Section(phase: 4, name: "N", thresholds: [71, 192, 320], mission: 1.565, enable: [ ]),
            TB.Section(phase: 4, name: "S", thresholds: [68, 145, 241], mission: 1.565, enable: [ ]),
        ]
        board[0].active = true
        board[1].active = true
    }

}

/// provides a static estimate of missions based on guild GP.  probably in the ballpark, but not great
struct DSGeoTBStaticMissionEstimator : TBMissionEstimator {
        
    let name = "DSGeoTBStaticMissionEstimator"
    let estimates: [TB.Key:Int]
    
    init(toonGP: Int, shipGP: Int) {
        self.estimates = DSGeoTBStaticMissionEstimator.defaultMission(toonGP: toonGP, shipGP: shipGP)
    }

    static func defaultMission(toonGP: Int, shipGP: Int) -> [TB.Key : Int] {
        var table: [TB.Key : Int]
        
        switch toonGP + shipGP {
        case 0 ..< 100:
            table = defaultMissions90
            
        case 100 ..< 135:
            table = defaultMissions130

        case 135 ..< 200:
            table = defaultMissions160

        default:
            table = defaultMissions200
        }
        
        return table
    }
    
    func missionCountsForSectors(sectors: [TB.Key]) -> [TB.Key:TBMissionStats] {
        var result = [TB.Key:TBMissionStats]()
        
        for sector in sectors {
            let stats = TBMissionStats(attempts: 0, waves: 0, points: Double(estimates[sector, default: 0]))
            result[sector] = stats
        }
        
        return result
    }

    /// guesses
    private static let defaultMissions200 = [
        TB.Key(1, "N") : 50,
        TB.Key(1, "S") : 40,
        TB.Key(2, "Air") : 45,
        TB.Key(2, "N") : 40,
        TB.Key(2, "S") : 45,
        TB.Key(3, "Air") : 40,
        TB.Key(3, "N") : 35,
        TB.Key(3, "S") : 35,
        TB.Key(4, "Air") : 15,
        TB.Key(4, "N") : 20,
        TB.Key(4, "S") : 20,
    ]

    /// Hope at ~160m GP scores around this
    private static let defaultMissions160 = [
        TB.Key(1, "N") : 40,
        TB.Key(1, "S") : 35,
        TB.Key(2, "Air") : 35,
        TB.Key(2, "N") : 25,
        TB.Key(2, "S") : 35,
        TB.Key(3, "Air") : 30,
        TB.Key(3, "N") : 25,
        TB.Key(3, "S") : 25,
        TB.Key(4, "Air") : 0,
        TB.Key(4, "N") : 10,
        TB.Key(4, "S") : 10,
    ]

    private static let defaultMissions130 = [
        TB.Key(1, "N") : 40,
        TB.Key(1, "S") : 35,
        TB.Key(2, "Air") : 35,
        TB.Key(2, "N") : 25,
        TB.Key(2, "S") : 35,
        TB.Key(3, "Air") : 30,
        TB.Key(3, "N") : 25,
        TB.Key(3, "S") : 25,
        TB.Key(4, "Air") : 0,
        TB.Key(4, "N") : 10,
        TB.Key(4, "S") : 10,
    ]

    private static let defaultMissions90 = [
        TB.Key(1, "N") : 25,
        TB.Key(1, "S") : 18,
        TB.Key(2, "Air") : 25,
        TB.Key(2, "N") : 18,
        TB.Key(2, "S") : 20,
        TB.Key(3, "Air") : 20,
        TB.Key(3, "N") : 18,
        TB.Key(3, "S") : 15,
        TB.Key(4, "Air") : 0,
        TB.Key(4, "N") : 0,
        TB.Key(4, "S") : 0,
    ]

}

/// https://wiki.swgoh.help/wiki/Geonosis_Republic_Offensive
struct LSGeoTB : TBBoardConfiguration {

    let name = "LSGeoTB"
    var board: [TB.Section]
    
    init() {
        // Note: there are some missions with higher rewards that have specific requirements (not modeled)
        board = [
            TB.Section(phase: 1, name: "Top", thresholds: [43, 85, 142], mission: 0.523, enable: [ "Top" ]),
            TB.Section(phase: 1, name: "Middle", thresholds: [110, 167, 257], mission: 1.15, enable: [ "Middle" ]),
            TB.Section(phase: 1, name: "Bottom", thresholds: [86, 120, 180], mission: 1.15, enable: [ "Bottom"]),
            TB.Section(phase: 2, name: "Top", thresholds: [71, 134, 215], mission: 0.9, enable: [ "Top" ]),
            TB.Section(phase: 2, name: "Middle", thresholds: [96, 174, 260], mission: 1.37, enable: [ "Middle" ]),
            TB.Section(phase: 2, name: "Bottom", thresholds: [121, 217, 310], mission: 1.37, enable: [ "Bottom"]),
            TB.Section(phase: 3, name: "Top", thresholds: [92, 152, 217], mission: 1.8, enable: [ "Top" ]),
            TB.Section(phase: 3, name: "Middle", thresholds: [132, 257, 378], mission: 1.63, enable: [ "Middle" ]),
            TB.Section(phase: 3, name: "Bottom", thresholds: [111, 166, 221], mission: 1.63, enable: [ "Bottom" ]),
            TB.Section(phase: 4, name: "Top", thresholds: [123, 340, 454], mission: 2.75, enable: [ ]),
            TB.Section(phase: 4, name: "Middle", thresholds: [153, 271, 437], mission: 1.837, enable: [ ]),
            TB.Section(phase: 4, name: "Bottom", thresholds: [118, 269, 336], mission: 1.837, enable: [ ]),
        ]
        board[0].active = true
        board[1].active = true
        board[2].active = true
    }

}

struct LSGeoTBStaticMissionEstimator : TBMissionEstimator {
        
    let name = "LSGeoTBStaticMissionEstimator"
    let estimates: [TB.Key:Int]
    
    init(toonGP: Int, shipGP: Int, estimates: [TB.Key:Int]? = nil) {
        self.estimates = estimates ?? LSGeoTBStaticMissionEstimator.defaultMission(toonGP: toonGP, shipGP: shipGP)
    }

    static func defaultMission(toonGP: Int, shipGP: Int) -> [TB.Key : Int] {
        return defaultMissions160
    }
    
    func missionCountsForSectors(sectors: [TB.Key]) -> [TB.Key:TBMissionStats] {
        var result = [TB.Key:TBMissionStats]()
        
        for sector in sectors {
            let stats = TBMissionStats(attempts: 0, waves: 0, points: Double(estimates[sector, default: 0]))
            result[sector] = stats
        }
        
        return result
    }

    /// guesses
    private static let defaultMissions160 = [
        TB.Key(1, "Top") : 0,
        TB.Key(1, "Middle") : 8,
        TB.Key(1, "Bottom") : 8,
        TB.Key(2, "Top") : 0,
        TB.Key(2, "Middle") : 3,
        TB.Key(2, "Bottom") : 3,
        TB.Key(3, "Top") : 0,
        TB.Key(3, "Middle") : 0,
        TB.Key(3, "Bottom") : 0,
        TB.Key(4, "Top") : 0,
        TB.Key(4, "Middle") : 0,
        TB.Key(4, "Bottom") : 0,
    ]
}

struct NoMissionEstimator : TBMissionEstimator {
    
    let name = "NoMissionEstimator"
    
    func missionCountsForSectors(sectors: [TB.Key]) -> [TB.Key:TBMissionStats] {
        var result = [TB.Key:TBMissionStats]()
        
        for sector in sectors {
            result[sector] = TBMissionStats(attempts: 0, waves: 0, points: 0.0)
        }
        
        return result
    }

}
