//
//  History.swift
//  swgoh-tool
//
//  Created by David Koski on 1/28/21.
//  Copyright © 2021 David Koski. All rights reserved.
//

import SWGOH

private var sortableDateCache = [String:String]()
private func sortableDate(_ date: String) -> String {
    if let result = sortableDateCache[date] {
        return result
    }
    
    // convert from mm/dd/yyyy -> yyyy/mm/dd
    let components = date.components(separatedBy: "/")
    let result = components[2] + components[0] + components[1]
    sortableDateCache[date] = result
    return result
}

struct AlliancePlayerHistoryPage : PageBuilder {
    var path: String { return "alliance/history.html" }
    
    let guilds: [Model.Guild]
    
    struct HistoryFile : Codable {
        var players: [Int:Player]
        var history: [History]
        
        static func read(from url: URL) throws -> HistoryFile {
            let data: Data
            do {
                data = try Data(contentsOf: url)
            } catch {
                return HistoryFile(players: [:], history: [])
            }
            let decoder = JSONDecoder()
            return try decoder.decode(HistoryFile.self, from: data)
        }
        
        func write(to url: URL) throws {
            let encoder = JSONEncoder()
            let data = try encoder.encode(self)
            try data.write(to: url, options: [.atomic])
        }

    }
    
    struct Player : Codable {
        var name: String
        var allyCode: Int
        var guild: String?
        var gp: Int
                
        /// what date we first saw them.  if we don't know, "inception"
        var firstSeen: String?
        
        /// when was the last time the user joined?
        var lastJoin: String?
        
        /// how many times have they been in BRG
        var visits: Int?
        
        /// if they left, where did they go to?
        var wentTo: String?
        
        init(player: Model.Player, guild: String) {
            self.name = player.name
            self.allyCode = player.stats.allyCode
            self.guild = guild
            self.gp = player.stats.gp
            
            let date = History.formatter.string(from: Date())
            self.firstSeen = date
            self.lastJoin = date
            self.visits = 1
        }
        
        init(name: String, allyCode: Int, gp: Int) {
            self.name = name
            self.allyCode = allyCode
            self.gp = gp
        }
    }
    
    struct History : Codable {
        let date: String
        let name: String
        let allyCode: Int
        let guild: String
        let gp: Int
        let action: String
        let comment: String?
        
        /// optional: how many days were they here?
        var duration: Int?
        
        /// optional: what guild did they go to after they left
        var wentTo: String?
                
        static var formatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            return formatter
        }()
        
        init(date: Date = Date(), name: String, allyCode: Int, guild: String, gp: Int, action: String, comment: String? = nil) {
            self.date = History.formatter.string(from: date)
            self.name = name
            self.allyCode = allyCode
            self.guild = guild
            self.gp = gp
            self.action = action
            self.comment = comment
        }
    }
    
    /// update the wentTo (guild) filed for any player that needs it
    func updateWentTo(history: inout HistoryFile) {
        let currentPlayersByCode = Dictionary(uniqueKeysWithValues: guilds.flatMap { guild in guild.players.map { ( guild.shortName, $0 ) } }.map { ($0.1.stats.allyCode, $0) })
        
        var playersNeedingWentTo = Set<Int>()

        // first get people who just left
        for (code, player) in history.players {
            // player is currently in a guild (hasn't been marked as left yet) and not in the
            // current list of players
            if let _ = player.guild, currentPlayersByCode[code] == nil {
                playersNeedingWentTo.insert(code)
                
            }
        }
        
        // now pick up any stragglers (up to a limit)
        for (code, player) in history.players {
            if playersNeedingWentTo.count > 20 {
                break
            }
            
            // player doesn't have a follow-on guild
            if currentPlayersByCode[code] == nil && player.guild == nil && player.wentTo == nil {
                playersNeedingWentTo.insert(code)
            }
        }
        
        let d = Downloader(user: "dkoski", password: "wazzok-7cyrco-zatwYq")
        let newGuilds = d.getGuildNames(allyCodes: Array(playersNeedingWentTo))
        
        for code in playersNeedingWentTo {
            if let newGuild = newGuilds[code] {
                history.players[code]?.wentTo = newGuild.guild
            }
        }
    }
            
    func generateHistory(history: inout HistoryFile) {
        var result = [History]()

        let currentPlayersByCode = Dictionary(uniqueKeysWithValues: guilds.flatMap { guild in guild.players.map { ( guild.shortName, $0 ) } }.map { ($0.1.stats.allyCode, $0) })

        // backfill any missing wentTo values
        for (index, entry) in history.history.enumerated() {
            if entry.action == "left" && entry.wentTo == nil {
                history.history[index].wentTo = history.players[entry.allyCode]?.wentTo
            }
        }

        // find all the players who are in the HistoryFile but not in the current set of players -- these are removed/left
        for (code, player) in history.players {
            // player is currently in a guild (hasn't been marked as left yet) and not in the
            // current list of players
            if let currentGuild = player.guild, currentPlayersByCode[code] == nil {
                var h = History(name: player.name, allyCode: code, guild: currentGuild, gp: player.gp, action: "left")
                
                h.wentTo = player.wentTo
                
                if let startDateString = player.lastJoin,
                   let start = History.formatter.date(from: startDateString) {
                    let days = Int(round((Date().timeIntervalSinceReferenceDate - start.timeIntervalSinceReferenceDate) / (24 * 3600.0)))
                    h.duration = days
                }
                
                // update the player to indicate that they left
                history.players[code]?.guild = nil
                
                result.append(h)
            }
        }
        
        // players in currentPlayers but not in history are added
        for (code, (guild, player)) in currentPlayersByCode {
            if history.players[code] == nil || history.players[code]?.guild == nil {
                result.append(History(name: player.name, allyCode: code, guild: guild, gp: player.stats.gp, action: "added"))
                
                if history.players[code] == nil {
                    history.players[code] = Player(player: player, guild: guild)
                } else {
                    // a previous player, update them
                    var p = history.players[code]!
                    
                    p.name = player.name
                    p.guild = guild
                    p.gp = player.stats.gp
                    
                    let date = History.formatter.string(from: Date())
                    p.lastJoin = date
                    if let visits = p.visits {
                        p.visits = visits + 1
                    } else {
                        p.visits = 1
                    }
                    history.players[code] = p
                }
            }
        }
        
        // moved guilds, change name
        for (code, (guild, player)) in currentPlayersByCode {
            if let historyPlayer = history.players[code], historyPlayer.guild != nil {
                if guild != historyPlayer.guild {
                    result.append(History(name: player.name, allyCode: code, guild: guild, gp: player.stats.gp, action: "change guild", comment: "previous: \(historyPlayer.guild!)"))
                    
                    history.players[code]?.guild = guild
                }
                if player.name != historyPlayer.name {
                    result.append(History(name: player.name, allyCode: code, guild: guild, gp: player.stats.gp, action: "change name", comment: "previous: \(historyPlayer.name)"))
                    
                    history.players[code]?.name = player.name
                }
            }
        }
        
        // finally update the player stats to current
        for (code, (_, player)) in currentPlayersByCode {
            history.players[code]?.gp = player.stats.gp
        }
        
        history.history.append(contentsOf: result)
    }
        
    func contents(html: HTML) -> String {
        var result = ""
        
        let historyURL = html.baseURL.appendingPathComponent("alliance/history.json")
        var historyFile = try! HistoryFile.read(from: historyURL)
        
        // first update the wentTo fields so we can use that in the history
        updateWentTo(history: &historyFile)
        
        // apply any changes
        generateHistory(history: &historyFile)
        
        try! historyFile.write(to: historyURL)
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += html.dataTable(id: "data", order: #"[0, "desc"], [3, "asc"]"#, fixedHeader: true)

        result += html.header(title: "Alliance Player History", nav: AllianceSite.navigation(html).render())
        
        result += #"History of players in the alliance."#
        
        let guildByShortName = Dictionary(uniqueKeysWithValues: guilds.map { ($0.shortName, $0) })
        let currentPlayersByCode = Dictionary(uniqueKeysWithValues: guilds.flatMap { guild in guild.players }.map { ($0.stats.allyCode, $0) })
        
        result +=
            H.table().id("data") {
                H.thead {
                    H.tr {
                        H.th { "Date" }
                        H.th { "Name" }
                        H.th { "Ally Code" }
                        H.th { "Guild" }
                        H.th { "GP" }
                        H.th { "Action" }
                        H.th { "Comment" }
                        H.th { "Stayed" }.title("Stayed for # days")
                        H.th { "Visited" }.title("Visitor # times")
                    }
                }
                H.foreach(historyFile.history.reversed()) { entry in
                    H.tr {
                        H.td().order(sortableDate(entry.date)) { entry.date }
                        H.td {
                            { () -> HTMLFramgment in
                                if let player = currentPlayersByCode[entry.allyCode] {
                                    return H.a(href: html.p(player, "tw-squads")) { player.name }
                                } else {
                                    return entry.name
                                }
                            }()
                        }
                        H.td { H.a(href: "https://swgoh.gg/p/\(entry.allyCode)/") { entry.allyCode } }
                        H.td {
                            { () -> HTMLFramgment in
                                if let guild = guildByShortName[entry.guild] {
                                    return H.a(href: html.g(guild, "index.html")) { guild.name }
                                } else {
                                    return entry.guild
                                }
                            }()
                        }
                        H.td {
                            H.parent([HTMLAttribute("data-order", entry.gp.description)])
                            entry.gp.millions
                        }
                        H.td { entry.action }
                        H.td {
                            entry.comment ?? ""
                            if entry.wentTo != nil {
                                "went to: "
                                entry.wentTo ?? ""
                            }
                        }
                        H.td { entry.duration?.description ?? "" }
                        H.td {
                            if entry.action == "left" || entry.action == "added" {
                                historyFile.players[entry.allyCode]?.visits?.description ?? ""
                            } else {
                                ""
                            }
                        }
                    }
                }
            }.render()

        result += html.footer()

        return result
    }

}

struct AlliancePlayerTransferPage : PageBuilder {
    var path: String { return "alliance/transfers.html" }
    
    let guilds: [Model.Guild]
                
    func contents(html: HTML) -> String {
        var result = ""
        
        let historyURL = html.baseURL.appendingPathComponent("alliance/history.json")
        let historyFile = try! AlliancePlayerHistoryPage.HistoryFile.read(from: historyURL)
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += html.dataTable(id: "data", order: #"[1, "asc"]"#, fixedHeader: true)

        result += html.header(title: "Alliance Player Current Transfers", nav: AllianceSite.navigation(html).render())
        
        result += #"Recent transfers within the alliance."#
        
        let guildByShortName = Dictionary(uniqueKeysWithValues: guilds.map { ($0.shortName, $0) })
        let currentPlayersByCode = Dictionary(uniqueKeysWithValues: guilds.flatMap { guild in guild.players }.map { ($0.stats.allyCode, $0) })

        let dateThreshold = Date().addingTimeInterval(3600 * 24 * -31)
        
        let transferredPlayers = Set(historyFile.history
            .filter { $0.action == "change guild" }
            .filter { AlliancePlayerHistoryPage.History.formatter.date(from: $0.date)! >= dateThreshold }
            .map { $0.allyCode })
        
        let filteredHistory = historyFile.history
            .filter { transferredPlayers.contains($0.allyCode) }
            .filter { AlliancePlayerHistoryPage.History.formatter.date(from: $0.date)! >= dateThreshold }
        
        result +=
            H.table().id("data") {
                H.thead {
                    H.tr {
                        H.th { "Date" }
                        H.th { "Name" }
                        H.th { "Ally Code" }
                        H.th { "Guild" }
                        H.th { "GP" }
                        H.th { "Action" }
                    }
                }
                H.foreach(filteredHistory.reversed()) { entry in
                    H.tr {
                        H.td().order(sortableDate(entry.date)) { entry.date }
                        H.td {
                            { () -> HTMLFramgment in
                                if let player = currentPlayersByCode[entry.allyCode] {
                                    return H.a(href: html.p(player, "tw-squads")) { player.name }
                                } else {
                                    return entry.name
                                }
                            }()
                        }
                        H.td { H.a(href: "https://swgoh.gg/p/\(entry.allyCode)/") { entry.allyCode } }
                        H.td {
                            { () -> HTMLFramgment in
                                if let guild = guildByShortName[entry.guild] {
                                    return H.a(href: html.g(guild, "index.html")) { guild.name }
                                } else {
                                    return entry.guild
                                }
                            }()
                        }
                        H.td {
                            H.parent([HTMLAttribute("data-order", entry.gp.description)])
                            entry.gp.millions
                        }
                        H.td { entry.action }
                    }
                }
            }.render()

        result += html.footer()

        return result
    }

}
