//
//  Player.swift
//  swgoh
//
//  Created by David Koski on 8/23/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

struct PlayerIndexPage : PageBuilder {
    
    var path: String { return "player/\(guild.shortName).html" }
    
    let guild: Model.Guild

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss

        result += html.header(title: guild.name, nav: "&nbsp;")
        
        result += "<table>\n"
        
        for player in guild.players.sorted() {
            result +=
                """
                <tr>
                <td><a href="\(html.p(player, "info"))">\(player.name)</a></td>
                <td><a href="\(html.p(player, "tw-squads"))">TW+GA Squads</a></td>
                <td><a href="\(html.p(player, "counters"))">Counters</a></td>
                <td><a href="\(html.p(player, "target"))">Targets</a></td>
                </tr>\n
                """
        }

        result += "</table>\n"

        result += html.footer()
        
        return result
    }

}

struct PlayerInfoPage : PageBuilder {
    
    var path: String { return "player/info/" + player.code + ".html" }
    
    let player: Model.Player
    let squads: [SquadEvaluation]
    let guild: Model.Guild
        
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.chartInclude()

        result += html.playerHeader(guild: guild, player:player, title: "\(player.name) Info")
        
        let gp = player.stats.gp
        let charsGP = player.stats.characterGP
        let shipsGP = player.stats.shipGP
        let zetas = player.units.values.reduce(0, { $0 + $1.zetas.count })
        let sevenStar = player.units.values.reduce(0, { $0 + ($1.stars >= 7 ? 1 : 0) })
        let g13 = player.units.values.reduce(0, { $0 + ($1.gear >= 13 ? 1 : 0) })
        let g12 = player.units.values.reduce(0, { $0 + ($1.gear >= 12 ? 1 : 0) })
        let g11 = player.units.values.reduce(0, { $0 + ($1.gear >= 11 ? 1 : 0) })
        let r5 = player.units.values.reduce(0, { $0 + ($1.relic >= (5 + 2) ? 1 : 0) })
        let squadCount = squads.filter { $0.status.isViable }.count
        
        // player-gp-stats

        result +=
            """
            <script>
            c1 = new Chart("\(html.g(guild, "player-gp-stats.json"))", "\(player.code)", null)
            c1.id = "chart"
            c1.title = "GP"
            c1.tightScale = true
            c1.round = 1000
            c1.height = 800
            c1.width = 600
            c1.display()
            c2 = new Chart("\(html.g(guild, "player-counts.json"))", "\(player.code)", null)
            c2.id = "chart2"
            c2.title = "Counts"
            c2.tightScale = true
            c2.round = 10
            c2.height = 400
            c2.width = 600
            c2.display()
            </script>
            """

        result +=
            """
            <div id="chart" style="float: right"></div>
            <br/>
            <table>
            <tr><td>Name</td><td>\(player.name)</td></tr>
            <tr><td>Code</td><td><a href=\"https://swgoh.gg/p/\(player.code)/\">\(player.code)</a></td></tr>
            <tr><td>GP</td><td>\(gp)</td></tr>
            <tr><td>Chars GP</td><td>\(charsGP)</td></tr>
            <tr><td>Ships GP</td><td>\(shipsGP)</td></tr>
            <tr><td>Zetas</td><td>\(zetas)</td></tr>
            <tr><td>7*</td><td>\(sevenStar)</td></tr>
            <tr><td>R5+</td><td>\(r5)</td></tr>
            <tr><td>G13+</td><td>\(g13)</td></tr>
            <tr><td>G12+</td><td>\(g12)</td></tr>
            <tr><td>G11+</td><td>\(g11)</td></tr>
            <tr><td>TW+GA Squads</td><td><a href="\(html.p(player, "tw-squads"))">\(squadCount)</a></td></tr>
            </table>
            <div id="chart2"></div>
            """

        result += html.footer(removeLastPathComponent: true)

        return result
    }
}

struct PlayerSquadsPage : PageBuilder {
    
    var path: String { return "player/tw-squads/" + player.code + ".html" }
    
    let player: Model.Player
    let squads: [SquadEvaluation]
    let distinctSquads: [SquadEvaluation]
    let guild: Model.Guild
        
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head

        result += html.toonCss
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += html.dataTable(id: "squads")
        result += html.chartInclude()

        result += html.playerHeader(guild: guild, player:player, title: "\(player.name) TW+GA Squads", navigation: #"<a href="\#(html.help("player-tw-squads"))">HELP</a> | "#)
        
        let sortedSquads = squads.sorted()
        
        result +=
            """
            <script>
            c1 = new Chart("\(html.g(guild, "player-squad-gp.json"))", "\(player.code)", null)
            c1.id = "chart"
            c1.filter = function(data) {
                data.series = data.series.filter(function (x) {
                    if (x.name == "HansMF") { return false }
                    if (x.name == "Negotiator") { return false }
                    if (x.name == "Malevolence") { return false }
                    if (x.name == "Raddus") { return false }
                    if (x.name == "Executrix") { return false }
                    if (x.name == "NestSolo") { return false }
                    return true
                })
                return data
            }
            c1.title = "Squads"
            c1.tightScale = true
            c1.round = 1000
            c1.height = 400
            c1.width = 600
            c1.display()
            </script>
            """

        result +=
            """
            <style>
            .details {
            }
            .detailsTable {
                width: 100%;
                font-size: 85%;
                margin-top: 15px;
                border-collapse: collapse;
                border: none;
            }
            </style>
            <div id="chart" style="float: right"></div>
            <h2><a href="\(html.help("player-tw-squads"))">HELP</a></h2>
            <p>
            Note: these teams may all be used simultaneously --- there is no overlap.
            This makes them potentially suitable for TW and GA.  You know your teams
            better than the program that generates this, so use this as a guideline,
            not hard facts.
            </p>
            
            <p>
            \(CounterType.offense.symbol): Considered mainly an offensive team
            <br/>
            \(CounterType.defense.symbol): Considered mainly an defensive team
            <br/>
            \(CounterType.both.symbol): Can be offensive or defensive
            <br/>
            <img src=\"\(html.i("wrench24.png"))\">: Team may be usable, but gear is recommended.  Hover or click to see more info.
            <br/>
            <img src=\"\(html.i("tractor24.png"))\">: Probably not usable -- needs farming
            <br/>
            &#11088;: Team is considered well geared
            <br/>
            &#9432;: Show more info about subs, how the team works, counters and targets
            </p>
            
            <button onclick="$('.squadRow').show()">Show All</button>
            <button onclick="$('.squadRow').hide(); $('.gearNeeded').show()">Show Only Gear Needed</button>
            <button onclick="$('.squadRow').hide(); $('.viable').show()">Show Only Viable</button>
            <button onclick="$('.squadRow').hide(); $('.distinct').show()">Show Only Non Overlapping</button>
            
            <table id="squads" class="display">
            <thead>
            <tr>
            <th>Name</th>
            <th>Type</th>
            <th>GP</th>
            <th>Squad</th>
            </tr>
            </thead>
            """
        
        let distinctSquadNames = Set(distinctSquads.map { $0.squad.name })
        
        var index = 0
        for ev in sortedSquads {
            index += 1
            let id = ev.squad.properties.defenderId

            // Column with squadd leader picture & squad name
            let trid: String = "id=\"\(ev.squad.name)\" "
            var toggle: String = ""
            if let id = id {
                toggle = #"onclick="$('#\#(id)_details').toggle()""#
            }
            
            var rowClass = ["squadRow"]
            if distinctSquadNames.contains(ev.squad.name) {
                rowClass.append("distinct")
            }
            if ev.status.isViable {
                rowClass.append("viable")
            }
            if ev.gearNeeded() != nil {
                rowClass.append("gearNeeded")
            }
            
            result += "<tr \(trid)\(toggle) class=\"\(rowClass.joined(separator: " "))\">"
            result += "<td data-order=\"\(ev.squad.name)\" align=\"center\">"
            if let id = id {
                result += "<a href=\"\(html.p(player, "target"))#\(id)\">"
            }
            result += "<img class=\"toI squad \(ev.squad.units[0].character.id)-i\" title=\"\(ev.squad.name)\"/>"
            if let _ = id {
                result += "</a>"
            }
            result += "<br/>\(ev.squad.name)"
            result += "</td>"
            
            // Team type column
            if let id = id {
                let team = CounterTeam.find(id)
                result += "<td data-order=\"\(team.type)\">\(team.type.symbol)</td>"
            } else {
                result += "<td data-order=\"unknown\"></td>"
            }
            
            result += "<td data-order=\"\(ev.squad.gp)\">\(ev.squad.gp.millions)</td>"
            
            // Squad + info
            result += "<td\(ev.squad.units.count <= 5 ? " nowrap" : "") style=\"position: relative\">"
            let gearNeeded = ev.gearNeeded()
            let extra = gearNeeded != nil ? " title=\"\(gearNeeded!)\" onclick=\"$('#squad\(index)').toggle()\"" : ""
            
            result += "<div style=\"display: flex\"\(extra)>"

            result += ev.squad.description(images: true, gearBelow: 11, starsBelow: 7)

            if let _ = gearNeeded {
                let icon = ev.status.isViable ? "wrench24.png" : "tractor24.png"
                result += "<div style=\"margin-left: 10px\"><img src=\"\(html.i(icon))\"></div>"
                
            } else if ev.scorePercent >= 0.90 {
                result += "<div style=\"margin-left: 10px\">&#11088;</div>"
            }
            result += "</div>"
            
            if let gearNeeded = gearNeeded {
                result += "<div id=\"squad\(index)\" class=\"toggleable verbage\"><br/><b>\(gearNeeded)</b></div>"
            }
            
            // info section
            if let id = id {
                let team = CounterTeam.find(id)

                result += "\n"
                result += "<span style=\"position: absolute; top: 10px; right: 10px; font-size: 150%;\">&#9432;</span>\n"

                result += "<table id=\"\(id)_details\" class=\"toggleable detailsTable\">\n"

                if !team.subs.isEmpty {
                    result += "<tr class=\"details\"><td colspan=\"2\"><h3>Subs</h3>\(team.subs)</td></tr>\n"
                }
                if !team.description.isEmpty {
                    result += "<tr class=\"details\"><td colspan=\"2\"><h3>Description</h3>\(team.description)</td></tr>\n"
                }
                                
                result += "<tr class=\"details\">\n"
                
                result += "<td valign=\"top\"><h3>Counter</h3>"
                for counter in team.counters() {
                    var imageClass = "toIs"
                    if counter.hard {
                        imageClass += " hardCounter"
                        result += "<b>"
                    } else {
                        imageClass += " softCounter"
                    }
                    result += #"<img style="vertical-align:middle" class="\#(imageClass) \#(counter.counter.squad[0].id)-i"/>"#
                    result += counter.counter.name
                    if counter.hard {
                        result += "</b>"
                    }
                    result += "<br/>\n"
                }
                result += "</td>"
                
                result += "<td valign=\"top\"><h3><a href=\"\(html.p(player, "target"))#\(id)\">Targets</a></h3>"
                for counter in team.reverseCounters() {
                    var imageClass = "toIs"
                    if counter.hard {
                        imageClass += " hardCounter"
                        result += "<b>"
                    } else {
                        imageClass += " softCounter"
                    }
                    result += #"<img style="vertical-align:middle" class="\#(imageClass) \#(counter.team.squad[0].id)-i"/>"#
                    result += counter.team.name
                    if counter.hard {
                        result += "</b>"
                    }
                    result += "<br/>\n"
                }
                result += "</td>"

                result += "</tr>\n"
                
                result += "</table>"
                result += "\n"

            }
            result += "</td>"
            result += "</tr>\n"
        }
        result +=
            """
            </table>
            """
        
        // TODO compute overlapping counters and suggest groups

        result += html.footer(removeLastPathComponent: true)

        return result
    }
}

struct PlayerCounterPage : PageBuilder {
    
    var path: String { return "player/counters/" + player.code + ".html" }
    
    let player: Model.Player
    let squads: [SquadEvaluation]
    let distinctSquads: [SquadEvaluation]
    let guild: Model.Guild
        
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.toonCss
        result += html.jqueryInclude

        result +=
            """
            <style>
            .hoverTable tr:hover {
                  background-color: #ffff99;
            }
            
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
            }

            td, th {
              border: 1px solid #dddddd;
              padding: 8px;
            }

            .toggleable {
                display: none;
            }
            </style>
            """
        
        result += #"<link rel="stylesheet" type="text/css" href="../teams.css">"#

        result += html.playerHeader(guild: guild, player:player, title: "\(player.name) Counters")

        var squadMap = [String:SquadEvaluation]()
        for ev in squads {
            if let id = ev.squad.properties.defenderId {
                squadMap[id] = ev
            }
        }

        var distinctSquadMap = [String:SquadEvaluation]()
        for ev in distinctSquads {
            if let id = ev.squad.properties.defenderId, let squad = squadMap[id] {
                if ev.squad != squad.squad {
                    // we have a different *distinct* squad -- report that too
                    distinctSquadMap[id] = ev
                }
            }
        }
        
        result +=
            """
            <p>This page shows counters for teams you may encounter, e.g. what teams
            might beat a bounty hunter squad.  Where possible, this shows <i>your</i> squads.  <b>Click on Toon counters to see more info</b></p>
            
            <p>Check out this <a href="https://bobbybaxter.github.io/swgoh-counters/" target="counters">interactive counter page</a> -- this is the source of the counter data here.</p>
            <br/>
            <img src=\"\(html.i("wrench24.png"))\">: Team may be usable, but gear is recommended.  Hover or click to see more info.
            <br/>
            <img src=\"\(html.i("tractor24.png"))\">: Probably not usable -- needs farming
            <br/>
            <br/>
            <button onclick="$('.gearNeeded').toggle()">Show/Hide Gear Needed</button>
            
            <table id="squads" class="hoverTable" width="100%" style="table-layout: fixed;">
            <thead>
            <tr>
            <th width="120px">Name</th>
            <th colspan="3">Counters</th>
            </tr>
            </thead>
            """
        
        for teamId in Counter.counterableIds {
            let team = CounterTeam.find(teamId)
            
            let sortedCountersAndEvs = team.counters().compactMap { counter -> (Counter, SquadEvaluation)? in
                    if let ev = squadMap[counter.counter.id] {
                        return (counter, ev)
                    } else {
                        return nil
                    }
                }.sorted {
                    let (c1, ev1) = $0
                    let (c2, ev2) = $1
                    
                    if c1.hard && !c2.hard {
                        return true
                    }
                    if !c1.hard && c2.hard {
                        return false
                    }
                    return ev1.squad.gp > ev2.squad.gp
                }
                        
            result +=
                """
                <tr id="\(teamId)" style="background: #aaaaff">
                <td onclick="$('.counter').hide(0, function() { $('#\(teamId)_General').show() });"><div class="toT"><img class="toI squad \(team.squad[0].id)-i"/><br/>\(team.name)</div></td>
                """
                        
            if sortedCountersAndEvs.isEmpty {
                result +=
                    """
                    <td class="verbage">
                    No available counter squads.  Click for general strategy and squads to farm.
                    </td>
                    """
            } else {
                result += #"<td colspan="3" class="verbage" style="position: relative"><div style="display: flex">"#
                var wasHard = sortedCountersAndEvs[0].0.hard
                for (counter, ev) in sortedCountersAndEvs {
                    
                    if wasHard && !counter.hard {
                        wasHard = false
                        result += #"<span style="background-color:#000; width:2px">&nbsp;</span>"#
                    }
                    
                    let imageClass = "toI " + (counter.hard ? "hardCounter" : "softCounter")
                    
                    var gearIcon = ""
                    var divClass = ""
                    if let gearNeeded = ev.gearNeeded() {
                        if ev.status.isViable {
                            gearIcon = #"<img title="\#(gearNeeded)" src="\#(html.i("wrench24.png"))">"#
                        } else {
                            gearIcon = #"<img title="\#(gearNeeded)" src="\#(html.i("tractor24.png"))">"#
                        }
                        divClass = #"class="gearNeeded" "#
                    }
                    
                    result +=
                        """
                        <div \(divClass)onclick="$('.counter').hide(0, function() { $('#\(teamId)_\(counter.counter.id)').show() });" class="toT"><img class="\(imageClass) \(ev.squad.units[0].character.id)-i"/>\(gearIcon)<br/>\(ev.squad.name)</div>
                        """
                }

                result += #"</div></td>"#
            }

            // team description (no specific counter)
            result +=
                """
                <tr id="\(teamId)_General" class="counter toggleable">
                <td colspan="4">
                <h3>Opponent</h3>
                <div style="display: flex">
                    \(
                    team.squad.map {
                        #"<div class="toT"><img class="toI squad \($0.id)-i"/><br/>\#($0.commonName)</div>"#
                    }.joined(separator: "")
                    )
                </div>
                
                <h3>Description</h3>
                <div class="\(teamId)-desc"></div>
                
                <h3>General Strategy</h3>
                <div class="\(teamId)-strat"></div>

                <h3>Additional Counter Teams</h3>
                <i>Click one of your counter teams (above) to see details about one of your teams.</i>
                <div style="display: flex">
                \(
                    team.counters().compactMap {
                        // show only squads that we don't have
                        squadMap[$0.counter.id] != nil ? nil :
                        #"<div class="toT"><img class="toI squad \#($0.counter.squad[0].id)-i" title="\#($0.counter.squad.map { $0.commonName }.joined(separator: ", "))"/><br/>\#($0.counter.name)</div>"#
                    }.joined(separator: "")
                )
                </div>
                </td>
                </tr>
                """
            
            // TODO show distinct team as well?
            
            // per counter descriptions
            for (counter, ev) in sortedCountersAndEvs {
                
                // if they have an alternate *distinct* team, report that too
                var distinctTeam = ""
                if let counterTeamId = ev.squad.properties.defenderId, let distinctEv = distinctSquadMap[counterTeamId] {
                    
                    var gearIcon = ""
                    if let gearNeeded = distinctEv.gearNeeded() {
                        if distinctEv.status.isViable {
                            gearIcon = #"<img title="\#(gearNeeded)" src="\#(html.i("wrench24.png"))">"#
                        } else {
                            gearIcon = #"<img title="\#(gearNeeded)" src="\#(html.i("tractor24.png"))">"#
                        }
                        gearIcon = #"<div class="toT">"# + gearIcon + "&nbsp;" + gearNeeded + "</div>"
                    }

                    distinctTeam =
                        """
                        <h3>\(counter.hard ? "Hard " : "")Counter Alternate:  \(distinctEv.squad.gp.millions)</h3>
                        <div style="display: flex">
                            \( distinctEv.squad.description(images: true, zetas: true, gearBelow: 12, starsBelow: 7) )\(gearIcon)
                        </div>
                        """
                }

                var gearIcon = ""
                if let gearNeeded = ev.gearNeeded() {
                    if ev.status.isViable {
                        gearIcon = #"<img title="\#(gearNeeded)" src="\#(html.i("wrench24.png"))">"#
                    } else {
                        gearIcon = #"<img title="\#(gearNeeded)" src="\#(html.i("tractor24.png"))">"#
                    }
                    gearIcon = #"<div class="toT">"# + gearIcon + "&nbsp;" + gearNeeded + "</div>"
                }
                
                result +=
                    """
                    <tr id="\(teamId)_\(counter.counter.id)" class="counter toggleable">
                    <td valign="top" colspan="2">
                    <h3>Opponent</h3>
                    <div style="display: flex">
                        \(
                        team.squad.map {
                            #"<div class="toT"><img class="toI squad \#($0.id)-i" /><br/>\#($0.commonName)</div>"#
                        }.joined(separator: "")
                        )
                    </div>
                    
                    <h3>Description</h3>
                    <div class="\(teamId)-desc"></div>
                    </td>
                    <td colspan="2">
                    <h3>\(counter.hard ? "Hard " : "")Counter: \(ev.squad.name) \(ev.squad.gp.millions)</h3>
                    <div style="display: flex">
                        \( ev.squad.description(images: true, zetas: true, gearBelow: 12, starsBelow: 7) )\(gearIcon)
                    </div>
                    \(distinctTeam)
                    \(
                        counter.counter.subs.isEmpty ? "" :
                        #"<h3>Subs</h3><div class="\#(counter.counter.id)-subs"></div>"#
                    )
                    \(
                        counter.description.isEmpty ? "" :
                        #"<h3>Strategy</h3><div class="\#(counter.id)-strat"></div>"#
                    )
                    <h3>General Strategy</h3>
                    <div class="\(teamId)-strat"></div>
                    </td>
                    </tr>
                    """
            }

            result += #"</tr>"#
        }
        
        result +=
            """
            </table>
            """

        result += html.footer(removeLastPathComponent: true)

        return result
    }
}

struct PlayerTargetPage : PageBuilder {
    
    var path: String { return "player/target/\(player.code).html" }
    
    let player: Model.Player
    let squads: [SquadEvaluation]
    let guild: Model.Guild
        
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.toonCss
        result += html.jqueryInclude

        result +=
            """
            <style>
            .hoverTable tr:hover {
                  background-color: #ffff99;
            }
            
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
            }

            td, th {
              border: 1px solid #dddddd;
              padding: 8px;
            }

            .toggleable {
                display: none;
            }
            </style>
            """
        
        result += #"<link rel="stylesheet" type="text/css" href="../teams.css">"#

        result += html.playerHeader(guild: guild, player:player, title: "\(player.name) Targets")

        let sortedSquads = squads.sorted()

        result +=
            """
            <p>This page shows your squads along with good targets for them to attack, per the <a href="https://bobbybaxter.github.io/swgoh-counters/" target="counters">interactive counter page</a>.  This is customized with <i>your</i> squads!  <b>Click on Toon counters to see more info</b></p>
            <br/>
            <img src=\"\(html.i("wrench24.png"))\">: Team may be usable, but gear is recommended.  Hover or click to see more info.
            <br/>
            <img src=\"\(html.i("tractor24.png"))\">: Probably not usable -- needs farming
            <br/>
            
            <table id="squads" class="hoverTable" width="100%" style="table-layout: fixed;">
            <thead>
            <tr>
            <th width="120px">Name</th>
            <th>Squad</th>
            </tr>
            </thead>
            """
        
        for ev in sortedSquads {
            guard let team = ev.squad.attackTeam else {
                continue
            }
            
            let teamId = team.id
            
            let reverseCounters = team.reverseCounters().sorted() { c1, c2 in
                if c1.hard && !c2.hard {
                    return true
                }
                if !c1.hard && c2.hard {
                    return false
                }
                return c1.team.name < c2.team.name
            }
            if reverseCounters.count == 0 {
                continue
            }
            
            var gearIcon = ""
            if let gearNeeded = ev.gearNeeded() {
                if ev.status.isViable {
                    gearIcon = #"<img title="\#(gearNeeded)" src="\#(html.i("wrench24.png"))">"#
                } else {
                    gearIcon = #"<img title="\#(gearNeeded)" src="\#(html.i("tractor24.png"))">"#
                }
                gearIcon = #"<div class="toT"><a href="\#(html.p(player, "tw-squads"))#\#(ev.squad.name)">"# + gearIcon + "</a></div>"
            }

            result +=
                """
                <tr onclick="$('.target').hide(0, function() { $('.\(teamId)_Target').show() });" id=\"\(team.id)\" style="background: #aaaaff">
                <td><div class="toT"><img class="toI squad \(team.squad[0].id)-i"/><br/>\(team.name)<br/>\(ev.squad.gp)</div></td>
                <td class="verbage" style="position: relative"><div style="display: flex">\(ev.squad.description(images: true, gearBelow: 11, starsBelow: 7))\(gearIcon)</div></td>
                </tr>
                <tr class="target \(teamId)_Target toggleable">
                <td></td>
                <td class="verbage">
                    \(
                        team.subs.isEmpty ? "" :
                        #"<h3>Subs</h3><div class="\#(teamId)-subs"></div>"#
                    )
                    \(
                        team.description.isEmpty ? "" :
                        #"<h3>Strategy</h3><div class="\#(teamId)-desc"></div>"#
                    )
                </td>
                </tr>
                """
            
            for reverseCounter in reverseCounters {
                let imageClass = reverseCounter.hard ? "hardCounter" : "softCounter"
                result +=
                    """
                    <tr onclick="$('.\(teamId)_Target_\(reverseCounter.team.id)').toggle()" class="target \(teamId)_Target toggleable">
                    <td class="verbage">\(reverseCounter.hard ? "<b>" : "")\(reverseCounter.team.name)\(reverseCounter.hard ? "</b>" : "")</td>
                    <td>
                    <div style="display: flex">
                        \(
                        reverseCounter.team.squad.map {
                            #"<div class="toT"><img class="toI \#(imageClass) \#($0.id)-i" /><br/>\#($0.commonName)</div>"#
                        }.joined(separator: "")
                        )
                    </div>
                    </td>
                    </tr>
                    <tr class="target \(teamId)_Target_\(reverseCounter.team.id) toggleable">
                    <td>&nbsp;</td>
                    <td class="verbage">
                    \(
                        reverseCounter.team.description.isEmpty ? "" :
                        #"<h3>Description</h3><div class="\#(reverseCounter.team.id)-desc"></div>"#
                    )
                    \(
                        reverseCounter.description.isEmpty ? "" :
                        #"<h3>Strategy: How to Beat \#(reverseCounter.team.name) with \#(team.name)</h3><div class="\#(reverseCounter.id)-strat"></div>"#
                    )
                    \(
                        reverseCounter.team.strategy.isEmpty ? "" :
                        #"<h3>General Strategy</h3><div class="\#(reverseCounter.team.id)-strat"></div>"#
                    )
                    </td>
                    </tr>
                    """
            }
        }
        
        result +=
            """
            </table>
            """

        result += html.footer(removeLastPathComponent: true)

        return result
    }
}

struct PlayerZetaPage : PageBuilder {
    
    var path: String { return "player/zeta/\(player.code).html" }
    
    let player: Model.Player
    let guild: Model.Guild
        
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.toonCss
        result += html.sortableTableCSS
        result += html.dataTable(id: "zetas")

        result +=
            """
            <style>
            .hoverTable tr:hover {
                  background-color: #ffff99;
            }
            
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
            }

            td, th {
              border: 1px solid #dddddd;
              padding: 8px;
            }
            </style>
            """

        result += html.playerHeader(guild: guild, player:player, title: "\(player.name) Zeta Farming")
        
        result += """
        These are the zetas you are missing from important toons (e.g. not Farmboy) that you have at G8 or higher.  You can sort by various ranks.  \(html.branding.zetaBlurb)  The next five come from the scorpio bot data and are rankings for different uses.  Higher scores are better zetas, at least in theory -- this does <b>not</b> consider your team makeup aside from a few simple rules on leadership zetas.  These are merely guidelines.
        """
    
        let important = characterDemographics.values.filter { $0.important }
        
        var importantZetas = [(Model.Character, Model.Unit, Int, String, ZetaPriority)]()
        for stat in important {
            
            if let unit = player.units[stat.id] {
                if unit.gear >= 8 {
                    for (zeta, score) in stat.zetas {
                        if unit.zetaIds.contains(zeta) {
                            continue
                        }

                        if score > 10 {
                            if let ability = Model.Ability.abilities[zeta] {
                                let name = ability.name
                                let zetaPriority = ZetaPriority.find(name)
                                let updatedScore = ZetaPriority.modifyPopularityScore(player: player, abilityId: zeta, score: Int(score))
                                importantZetas.append((stat.character, unit, updatedScore, zeta, zetaPriority))
                            }
                        }
                    }
                }
            }

        }
        
        let sortedZetas = importantZetas.sorted {
            return $0.2 > $1.2
        }

        result += H.table().id("zetas") {
            H.thead {
                H.tr {
                    H.th { "Toon" }
                    H.th { "Stats" }
                    H.th { "Zeta" }
                    H.th { "Description" }
                    H.th { "Popularity" }.title("Popularity among players")
                    H.th { "HSR" }.title("Scorpio rank for heroic sith raid")
                    H.th { "PVP" }.title("Scorpio rank for PVP")
                    H.th { "Versa" }.title("Scorpio rank for versatility")
                    H.th { "TW" }.title("Scorpio rank for Territory War and Grand Arena")
                    H.th { "TB" }.title("Scorpio rank for Territory Battles")
                }
            }
            "\n"
            H.foreach(sortedZetas) { (character, unit, importance, abilityId, priority ) in
                H.tr {
                    H.td { H.img(src: character.imageURL).class("toI") }
                    H.td().order(unit.gp) {
                        unit.description(gearBelow: 13, starsBelow: 7) + " " + unit.gp.millions }
                    H.td {
                        H.img(src: "https://swgoh.gg" + Model.Ability.abilities[abilityId]!.image ).style("width: 30px")
                        " "
                        Model.Ability.abilities[abilityId]!.name
                    }
                    H.td { Model.Ability.abilities[abilityId]!.zetaDescription ?? "" }
                    H.td { importance }
                    H.td { priority.priority("HSR") }
                    H.td { priority.priority("PVP") }
                    H.td { priority.priority("Versa") }
                    H.td { priority.priority("TW") }
                    H.td { priority.priority("TB") }
                }
                "\n"
            }
        }.render()
        
        result += html.footer(removeLastPathComponent: true)

        return result
    }
}

struct PlayerFarmPage : PageBuilder {
    
    var path: String { return "player/farm/\(player.code).html" }
    
    let player: Model.Player
    let guild: Model.Guild
        
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.toonCss
        result += html.jqueryInclude
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result +=
            """
            <style>
            .hoverTable tr:hover {
                  background-color: #ffff99;
            }
            
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
            }

            td, th {
              border: 1px solid #dddddd;
              padding: 8px;
            }
            </style>
            """
        
        result += html.dataTable(id: "farm", order: "[1, \"desc\"]")

        result += html.playerHeader(guild: guild, player:player, title: "\(player.name) Unit Farming")
        
        result += """
        \(html.branding.farmBlurb)  This does not take into account any squads you may be working on, so just use it as a guideline and scan through it if there are any obvious flaws on characters you already have and use, like missing mods.
        """
    
        let charactersInImportanceOrder = characterDemographics.values.sorted {
            return $0.importance > $1.importance
        }
        
        var data = [(Int, Model.Character, Model.Unit?, String)]()
        for characterDemographic in charactersInImportanceOrder {
            if characterDemographic.unimportant {
                continue
            }
            
            if let unit = player.units[characterDemographic.character.id] {
                var instructions = [String]()
                
                if characterDemographic.important {
                    if unit.stars < 7 {
                        instructions.append("Farm (\(unit.stars) stars).")
                    }
                    if unit.gear < 10 {
                        instructions.append("Gear up (\(unit.gearLevelDescription)).")
                    }
                    if unit.gear > 8 && unit.level < 85 {
                        instructions.append("<b>Level up (level \(unit.level)).</b>")
                    }
                    
                    if unit.mods.count != 6 {
                        instructions.append("<b>Missing mods.</b>")
                    }
                }
                
                if unit.mods.contains(where: { mod in
                    if mod.pips < 5 {
                        return true
                    }
                    return false
                }) {
                    instructions.append("<b>Mod low pips.</b>")
                }
                if unit.mods.contains(where: { mod in
                    if mod.pips == 5 && mod.tier == 1 {
                        return true
                    }
                    return false
                }) {
                    instructions.append("Mod low tier.")
                }

                if unit.mods.contains(where: { mod in
                    if mod.pips >= 5 && mod.level < 15 {
                        return true
                    }
                    return false
                }) {
                    instructions.append("<b>Mod low level.</b>")
                }
                
                if unit.mods.contains(where: { $0.primaryStat.characteristic == .acc }) {
                    instructions.append("Using accuracy arrow -- usually not what you want.")
                }

                if unit.mods.count == 6 && unit.gear > 8 {
                    if let cluster = referenceRecommendations[unit.id]?.clusters[0] {
                        let speedPriority = cluster.priorities.filter { $0.c == .speed }
                        if !speedPriority.isEmpty {
                            let gear = unit.combinedGearLevel.value
                            let actualStats = modStats(character: unit.character, gearLevel: gear, priorities: speedPriority, mods: unit.mods)
                            let desiredStats = modStats(character: unit.character, gearLevel: gear, priorities: speedPriority, mods: cluster.center.mods)
                            
                            if actualStats[0].v < desiredStats[0].v / 2 {
                                instructions.append("<b>Mod low speed: \(actualStats[0].v), desired: \(desiredStats[0].v)</b>")
                            }
                        }
                    }
                }
                
                if !instructions.isEmpty {
                    data.append((characterDemographic.importance, characterDemographic.character, unit, instructions.joined(separator: "  ")))
                }

            } else {
                // not unlocked
                if characterDemographic.important {
                    data.append((characterDemographic.importance, characterDemographic.character, nil, "Needs unlock"))
                }
            }
        }
        
        result += H.table().id("farm") {
            H.thead {
                H.tr {
                    H.th { "Toon" }
                    H.th { "Stats" }
                    H.th { "Instruction" }
                }
            }
            "\n"
            H.foreach(data) { (importance, character, unit, instructions) in
                H.tr {
                    H.td {
                        H.a(href: "https://swgoh.gg/p/\(player.stats.allyCode)/characters/\(character.urlName)") {
                            H.img(src: character.imageURL).class("toI")
                        }
                    }
                    H.td().order(unit?.gp ?? 0) {
                        H.ifLet(unit) { unit in
                            unit.description(gearBelow: 30, starsBelow: 7)
                            " "
                            unit.gp.millions
                            " gp"
                        }
                    }
                    H.td { instructions }
                }
                "\n"
            }
        }.render()
        
        result += html.footer(removeLastPathComponent: true)

        return result
    }
}

struct PlayerGeoTB : PageBuilder {
    
    var path: String { return "player/\(shortName)/\(player.code).html" }
    
    let player: Model.Player
    let guild: Model.Guild
        
    let title: String
    let shortName: String

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.toonCss
        result += html.chartInclude()

        result +=
            """
            <style>
            .hoverTable tr:hover {
                  background-color: #ffff99;
            }
            
            table {
              font-family: arial, sans-serif;
              border-collapse: collapse;
            }

            td, th {
              border: 1px solid #dddddd;
              padding: 8px;
            }
            </style>
            """

        result += html.playerHeader(guild: guild, player:player, title: "\(player.name) \(title) Squads")
        
        let chartName = shortName == "dsgeotb" ? "player-ds-geo-tb-squad-gp" : "player-ls-geo-tb-squad-gp"
        
        result +=
            """
            <script>
            c1 = new Chart("\(html.g(guild, chartName + ".json"))", "\(player.code)", null)
            c1.id = "chart"
            c1.filter = function(data) {
                data.series = data.series.filter(function (x) {
                    if (x.name == "Executrix") { return false }
                    if (x.name == "Chimaera+Bugs") { return false }
                    if (x.name == "Negotiator") { return false }
                    return true
                })
                return data
            }
            c1.title = "TB Squads"
            c1.tightScale = true
            c1.round = 1000
            c1.width = 450
            c1.display()
            </script>
            """
        
        if shortName == "dsgeotb" {
            result +=
            """
            <p>See also <a href="https://genskaar.github.io/tb_geo/html/ds.html">https://genskaar.github.io/tb_geo/html/ds.html</a>.</p>
            """
        } else if shortName == "lsgeotb" {
            result +=
            """
            <p>See also <a href="https://genskaar.github.io/tb_geo/html/ls.html">https://genskaar.github.io/tb_geo/html/ls.html</a>.</p>
            """
        }

        let imageName = shortName == "dsgeotb" ? "ds_geo_tb_labels" : "ls_geo_tb_labels"

        result += """
        <p>Please check platoons before using any of these squads in missions.  Also note that the section names are positions on the map rather than phase numbers:</p>
        <br/>
        <img src="\(html.i(imageName + ".jpeg"))" width="500"/>
        <p>
        The <i>Estimated waves</i> is an estimate of the strength of your team.  0 waves doesn't mean guaranteed failure and 4 waves doesn't mean a full clear.  If you find that your performance greatly differs from the estimate, please let me know!  @gorgatron#3094  Also: a clear of the fleet section is marked as 4 waves (to match the ground missions).
        </p>
        <div id="chart" style="float: right"></div>
        """

        let boardConfiguration: TBBoardConfiguration = shortName == "dsgeotb" ? DSGeoTB() : LSGeoTB()
        let squadConfiguration: TBSquadConfiguration = shortName == "dsgeotb" ? DSGeoTBSquads() : LSGeoTBSquads()
        let tbSquads = TBSquadBuilder(squadConfiguration: squadConfiguration)
        
        let missionEstimator = TBSquadMissionEstimator(guild: guild, boardConfiguration: boardConfiguration, squadConfiguration: squadConfiguration, preparedMissions: [:])

        for section in squadConfiguration.missionConfigurations.keys.sorted() {
            let evaluations = tbSquads.squadsForMission(player: player, sector: section).sorted {
                return $0.squad.gp < $1.squad.gp
            }
            
            result += H.block {
                H.h1 {
                    section.description
                }

                if evaluations.isEmpty {
                    "No known viable squads."
                } else {
                    H.table {
                        H.foreach(evaluations) { ev in
                            H.tr {
                                H.td { ev.squad.gp }
                                H.td {
                                    H.div().style("display: flex") {
                                        ev.squad.description(images: true, gearBelow: 11, starsBelow: 7)
                                    }
                                    ev.gearNeeded() ?? ""
                                }
                                H.td {
                                    "Estimated waves: "
                                    missionEstimator.estimateWaves(section: section, squad: ev.squad).0
                                }
                            }
                            "\n"
                        }
                    }
                    "\n"
                }
            }.render()
        }
        
        result += html.footer(removeLastPathComponent: true)

        return result
    }
}
