//
//  Site.swift
//  swgoh
//
//  Created by David Koski on 8/17/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

struct Site {
    
    let guild: Model.Guild
    let pages: [String]
    
    /// allows repeating toons but only one of each id
    let squads: [Model.Player : [SquadEvaluation]]
    
    /// all squads are distinct -- no toons repeated
    let distinctSquads: [Model.Player : [SquadEvaluation]]
        
    let dsGeoTBSquads: [Model.Player: [TB.Key:[SquadEvaluation]]]
    let lsGeoTBSquads: [Model.Player: [TB.Key:[SquadEvaluation]]]

    let twConfig: TWConfig
    
    let verbose = false
    
    init(guild: Model.Guild, pages: [String] = []) {
        self.guild = guild
        self.pages = pages.map { $0.lowercased() }
    
        twConfig = twConfigByName[guild.shortName, default: { OptimizedConfig() }]()

        (distinctSquads, squads) = Site.evaluatePlayers(guild: guild)
                
        self.dsGeoTBSquads = TBSquadBuilder(squadConfiguration: DSGeoTBSquads()).evaluatePlayers(guild: guild)
        self.lsGeoTBSquads = TBSquadBuilder(squadConfiguration: LSGeoTBSquads()).evaluatePlayers(guild: guild)
    }
    
    static func evaluatePlayers(guild: Model.Guild) -> ([Model.Player:[SquadEvaluation]], [Model.Player:[SquadEvaluation]]) {
        
        let squadBuilder = (twSquadsByName[guild.shortName] ?? twSquadsByName["DEFAULT"]!)()

        var distinctEvaluations = [Model.Player:[SquadEvaluation]]()
        var uniqueEvaluations = [Model.Player:[SquadEvaluation]]()

        var distinctFilter: Filter = PreferredViabilitySortFilter()
        distinctFilter = distinctFilter.compose(SquadListFilter())

        var uniqueFilter: Filter = PreferredViabilitySortFilter()
        uniqueFilter = uniqueFilter.compose(UniqueSquadFilter())

        let queue = DispatchQueue(label: "build")
        func update(player: Model.Player, distinctEvaluations dev: [SquadEvaluation], uniqueEvaluations uev: [SquadEvaluation]) {
            queue.sync {
                distinctEvaluations[player] = dev
                uniqueEvaluations[player] = uev
            }
        }
        
        DispatchQueue.concurrentPerform(iterations: guild.players.count) {
            let player = guild.players[$0]
            
            let squads = squadBuilder.generateSquads(units: player)
            
            let allEvaluations = squadEvaluator.evaluate(squads: squads)
            let distinctEvaluations = distinctFilter.filter(allEvaluations)
            let uniqueEvaluations = uniqueFilter.filter(allEvaluations)
                        
            update(player: player, distinctEvaluations: distinctEvaluations, uniqueEvaluations: uniqueEvaluations)
        }
        
        return (distinctEvaluations, uniqueEvaluations)
    }
        
    struct GuildBuilder {
        let name: String
        let body: (Site, Model.Guild, URL) throws -> Void
        
        init(_ name: String, _ body: @escaping (Site, Model.Guild, URL) throws -> Void) {
            self.name = name
            self.body = body
        }
    }
    
    struct PlayerBuilder {
        let name: String
        let body: (Site, Model.Guild, Model.Player, URL) throws -> Void
        
        init(_ name: String, _ body: @escaping (Site, Model.Guild, Model.Player, URL) throws -> Void) {
            self.name = name
            self.body = body
        }
    }
    
    let serialGuildPages: [GuildBuilder] = [
        GuildBuilder("DS Geo TB") {
            let boardConfiguration = DSGeoTB()
            
            let squadConfiguration = DSGeoTBSquads()
            let preparedMissions = squadConfiguration.prepareMissionConfigurations(players: $1.players)
            let missionEstimator = TBSquadMissionEstimator(guild: $1, boardConfiguration: boardConfiguration, squadConfiguration: squadConfiguration, preparedMissions: preparedMissions)
            try TBPlanPage(guild: $1, title: "DS Geo TB", shortName: "ds-geo", boardConfiguration: boardConfiguration, missionEstimator: missionEstimator).build(base: $2)
        },
        GuildBuilder("LS Geo TB") {
            let boardConfiguration = LSGeoTB()
            
            let squadConfiguration = LSGeoTBSquads()
            let preparedMissions = squadConfiguration.prepareMissionConfigurations(players: $1.players)
            let missionEstimator = TBSquadMissionEstimator(guild: $1, boardConfiguration: boardConfiguration, squadConfiguration: squadConfiguration, preparedMissions: preparedMissions)
            
            struct DiscountedMissions : TBMissionEstimator {
                let multipler: Double
                let other: TBMissionEstimator
                var name: String { "Discounted \(other.name)" }
                
                func missionCountsForSectors(sectors: [TB.Key]) -> [TB.Key : TBMissionStats] {
                    let otherResult = other.missionCountsForSectors(sectors: sectors)
                    var result = [TB.Key : TBMissionStats]()
                    
                    for (key, stats) in otherResult {
                        result[key] = TBMissionStats(attempts: stats.attempts, waves: stats.waves, points: stats.points * multipler)
                    }
                    
                    return result
                }
            }
            
            let discountedEstimator = DiscountedMissions(multipler: 0.75, other: missionEstimator)

            try TBPlanPage(guild: $1, title: "LS Geo TB", shortName: "ls-geo", boardConfiguration: boardConfiguration, missionEstimator: discountedEstimator).build(base: $2)
        },
        GuildBuilder("HSTR") {
            var hstr = HTMLMultiAssignmentReport(name: "hstr", title: "HSTR", guild: $1)
            hstr.buildHSTR(players: $1.players)
            try hstr.build(base: $2)
        },
        GuildBuilder("Rancor") {
            let report = ChallengeRancorPage(guild: $1, info: defaultBrand.challengeRancorInfo)
            try report.build(base: $2)
        },
        GuildBuilder("HAAT") {
            var report = HTMLMultiAssignmentReport(
                name: "haat", title: "HAAT", guild: $1)
            report.buildHAAT(players: $1.players)
            try report.build(base: $2)
        },
    ]
    
    let guildPages: [GuildBuilder] = [
        // do this one early -- the stats page makes use of it
        GuildBuilder("Guild Player GP Chart") { try GuildPlayerGPChart(guild: $1).build(base: $2) },

        GuildBuilder("Index") { try GuildIndex(guild: $1).build(base: $2) },
        GuildBuilder("Rare") { try RareUnitReportHTML(guild: $1).build(base: $2) },
                
        GuildBuilder("TW Planning") { try TWPage(guild: $1, name: "tw-planning", contents: TWPlanningReportHTML(config: $0.twConfig).report(data: $0.distinctSquads)).build(base: $2) },
        GuildBuilder("TW Summary") { try TWPage(guild: $1, name: "tw-summary", contents: TWPlanningSummaryReport(config: $0.twConfig).report(data: $0.distinctSquads)).build(base: $2) },
        GuildBuilder("TW Orders") {
            let squadsPerSection = ($1.players.count + 1) / 2
            let schedule = defaultTWScheduler().schedule(data: $0.distinctSquads, config: $0.twConfig, squadsPerSection: squadsPerSection, trace: TWTrace())
            
            try TWPage(guild: $1, name: "tw-orders", contents: TWPlayerReport.report(schedule: schedule.placedSquads, mentions: siteTWMentions)).build(base: $2)
            
            try TWPage(guild: $1, name: "tw-remainder-players", contents: twRemainderByPlayer(remainder: schedule.remainder, mentions: siteTWMentions)).build(base: $2)
            try TWPage(guild: $1, name: "tw-remainder-teams", contents: twRemainderByTeam(remainder: schedule.remainder, mentions: siteTWMentions)).build(base: $2)
        },
        GuildBuilder("TW Teams") { try GuildTWSquadsPage(guild: $1, squads: $0.distinctSquads).build(base: $2) },
        GuildBuilder("TW Teams CSV") { try GuildSquadsCSVPage(guild: $1, squads: $0.distinctSquads).build(base: $2) },

        GuildBuilder("Guild Info") { try GuildInfoPage(guild: $1, squads: $0.distinctSquads).build(base: $2) },
        GuildBuilder("Guild Info CSV") { try GuildInfoCSV(guild: $1, squads: $0.distinctSquads).build(base: $2) },
        GuildBuilder("Important Toons") { try ImportantToonsPage(guild: $1).build(base: $2) },
        GuildBuilder("Guild Squads Distinct") { try SquadsPage(guild: $1, squads: $0.distinctSquads, filename: "guild-squads", title: "Distinct Squads").build(base: $2) },
        GuildBuilder("Guild Squads") { try SquadsPage(guild: $1, squads: $0.squads, distinct: false, filename: "guild-squads-all", title: "All Squads").build(base: $2) },

        GuildBuilder("Player Index") { try PlayerIndexPage(guild: $1).build(base: $2) },
        GuildBuilder("DS Geo TB Squads By Section") { try TBSquadsBySectionPage(title: "DS Geo TB", shortName: "dsgeotb", guild: $1, squads: $0.dsGeoTBSquads).build(base: $2) },
        GuildBuilder("DS Geo TB Squads By Type") {
            let tb = DSGeoTB()
            let tbSquads = DSGeoTBSquads()
            try TBSquadsByTypePage(title: "DS Geo TB", shortName: "dsgeotb", guild: $1, squads: $0.dsGeoTBSquads, squadNames: tbSquads.importantSquadNames).build(base: $2) },
        GuildBuilder("DS Geo TB Squads By Type") {
            let tb = DSGeoTB()
            let tbSquads = DSGeoTBSquads()
            try TBSquadsChartPage(title: "DS Geo TB", shortName: "dsgeotb", guild: $1, squadNames: tbSquads.importantSquadNames).build(base: $2) },
        
        GuildBuilder("LS Geo TB Squads By Section") { try TBSquadsBySectionPage(title: "LS Geo TB", shortName: "lsgeotb", guild: $1, squads: $0.lsGeoTBSquads).build(base: $2) },
        GuildBuilder("LS Geo TB Squads By Type") {
            let tb = LSGeoTB()
            let tbSquads = LSGeoTBSquads()
            try TBSquadsByTypePage(title: "LS Geo TB", shortName: "lsgeotb", guild: $1, squads: $0.lsGeoTBSquads, squadNames: tbSquads.importantSquadNames).build(base: $2) },
        GuildBuilder("LS Geo TB Squads By Type") {
            let tb = LSGeoTB()
            let tbSquads = LSGeoTBSquads()
            try TBSquadsChartPage(title: "LS Geo TB", shortName: "lsgeotb", guild: $1, squadNames: tbSquads.importantSquadNames).build(base: $2) },

                
        GuildBuilder("Guild GP Chart") { try GuildGPChart(guild: $1).build(base: $2) },
        GuildBuilder("Guild Count Chart") { try GuildCountsChart(guild: $1, squads: $0.distinctSquads).build(base: $2) },
        GuildBuilder("Guild Player Count Chart") { try GuildPlayerCountsChart(guild: $1, squads: $0.distinctSquads).build(base: $2) },
        GuildBuilder("Guild Important Toon Count Chart") { try GuildImportantToonsCountChart(guild: $1).build(base: $2) },
        GuildBuilder("Player Important Toon GP Chart") { try GuildImportantToonsGPChart(guild: $1).build(base: $2) },
        GuildBuilder("Guild Squad Count Chart") { try GuildSquadsCountChart(guild: $1, squads: $0.distinctSquads).build(base: $2) },
        GuildBuilder("Guild Squad DS Geo TB Count Chart") { try GuildTBSquadsCountChart(guild: $1, squads: $0.dsGeoTBSquads).build(base: $2) },
        GuildBuilder("Player Squad GP Chart") { try GuildSquadsGPChart(guild: $1, squads: $0.distinctSquads).build(base: $2) },
        GuildBuilder("Player Squad DS Geo TB GP Chart") { try GuildTBSquadsGPChart(chartName: "player-ds-geo-tb-squad-gp", guild: $1, squads: $0.dsGeoTBSquads).build(base: $2) },
        GuildBuilder("Player Squad LS Geo TB GP Chart") { try GuildTBSquadsGPChart(chartName: "player-ls-geo-tb-squad-gp", guild: $1, squads: $0.lsGeoTBSquads).build(base: $2) },
        GuildBuilder("FarmingUnlockPage") { for unlock in FarmingUnlockPage.unlocks { try FarmingUnlockPage(guild: $1, unlock: unlock).build(base: $2) } },
        GuildBuilder("GuildChartsPage") { try GuildChartsPage(guild: $1, squads: $0.squads).build(base: $2) },

        GuildBuilder("TeamCSS") { try TeamCSSPage().build(base: $2) },
        GuildBuilder("ToonCSS") { try ToonCSSPage().build(base: $2) },
        
        GuildBuilder("GuildFarming") { try GuildFarmingReport(guild: $1, squads: $0.squads).build(base: $2) },
        
        GuildBuilder("ModData") {
            let guildModData = SiteGuildModData(guild: $1, recommendations: referenceRecommendations)
            let data = try JSONEncoder().encode(guildModData)
            let url = $2.appendingPathComponent("guild/\($1.shortName)/modData.json")
            try data.write(to: url)
        },
        GuildBuilder("ModPages") {
            try ModsPlayerPage(guild: $1).build(base: $2)
            try ModsPlayersPage(guild: $1).build(base: $2)
            try ModsStatsPage(guild: $1).build(base: $2)
            try ModsCharacterPage(guild: $1).build(base: $2)
            try ModsCharactersPage(guild: $1).build(base: $2)
        }
    ]
    
    let playerPages: [PlayerBuilder] = [
        PlayerBuilder("Info") { try PlayerInfoPage(player: $2, squads: $0.distinctSquads[$2, default:[]], guild: $1).build(base: $3) },
        PlayerBuilder("Squads") { try PlayerSquadsPage(player: $2, squads: $0.squads[$2, default:[]], distinctSquads: $0.distinctSquads[$2, default:[]], guild: $1).build(base: $3) },
        PlayerBuilder("Counter") { try PlayerCounterPage(player: $2, squads: $0.squads[$2, default:[]], distinctSquads: $0.distinctSquads[$2, default:[]], guild: $1).build(base: $3) },
        PlayerBuilder("Target") { try PlayerTargetPage(player: $2, squads: $0.squads[$2, default:[]], guild: $1).build(base: $3) },
        PlayerBuilder("Zeta") { try PlayerZetaPage(player: $2, guild: $1).build(base: $3) },
        PlayerBuilder("Farm") { try PlayerFarmPage(player: $2, guild: $1).build(base: $3) },
        PlayerBuilder("DSGeoTB") { try PlayerGeoTB(player: $2, guild: $1, title: "DS Geo TB", shortName: "dsgeotb").build(base: $3) },
        PlayerBuilder("LSGeoTB") { try PlayerGeoTB(player: $2, guild: $1, title: "LS Geo TB", shortName: "lsgeotb").build(base: $3) },
        PlayerBuilder("PlayerFarmingUnlockPage") { try PlayerFarmingUnlockPage(player: $2, guild: $1).build(base: $3) },
    ]
    
    func trace(_ value: @autoclosure () -> String) {
        if verbose {
            print(value())
        }
    }
    
    func pageAllowed(_ name: String) -> Bool {
        return pages.isEmpty || pages.contains(name.lowercased())
    }
    
    func build(base: URL) throws {
        
        for builder in serialGuildPages {
            if !pageAllowed(builder.name) {
                continue
            }
            do {
                let t = Date().timeIntervalSinceReferenceDate
                trace("START \(builder.name)")
                try builder.body(self, guild, base)
                trace("DONE  \(builder.name) = \(Date().timeIntervalSinceReferenceDate - t)")
            } catch {
                fatalError("processing \(builder.name): \(error)")
            }
        }
        
        DispatchQueue.concurrentPerform(iterations: guildPages.count) {
            let builder = guildPages[$0]
            
            if !pageAllowed(builder.name) {
                return
            }

            do {
                let t = Date().timeIntervalSinceReferenceDate
                trace("START \(builder.name)")
                try builder.body(self, guild, base)
                trace("DONE  \(builder.name) = \(Date().timeIntervalSinceReferenceDate - t)")
            } catch {
                fatalError("processing \(builder.name): \(error)")
            }
        }
        
        DispatchQueue.concurrentPerform(iterations: guild.players.count) {
            let player = guild.players[$0]
            trace("PLAYER \(player.name)")
            
            let t = Date().timeIntervalSinceReferenceDate
            for builder in playerPages {
                if !pageAllowed(builder.name) {
                    continue
                }

                do {
                    try builder.body(self, guild, player, base)
                } catch {
                    fatalError("processing \(builder.name) \(player.name): \(error)")
                }
            }
            trace("DONE PLAYER \(player.name) = \(Date().timeIntervalSinceReferenceDate - t)")
        }
    }
}
