//
//  StatsRiw.swift
//  swgoh
//
//  Created by David Koski on 9/7/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

struct StatsRowExtractor {

    let name: String
    let title: String?
    
    /// extract single value
    let apply: (Model.Guild, Model.Player, [SquadEvaluation]) -> Int
    
    /// generates a per row title (hover string)
    let rowTitle: (Model.Guild, Model.Player, [SquadEvaluation]) -> String?
    
    /// describe single value
    let describe: (Int) -> String
    
    typealias RowValue = (value: Int, count: Int, title: String?)
    
    /// for combining StatsRows
    let combine: (RowValue, RowValue) -> RowValue
    let finish: (RowValue) -> (RowValue, String)
    
    static func average(sum: RowValue) -> (RowValue, String) {
        let average = sum.value / sum.count
        return ((average, 1, nil), average.millions)
    }
    
    init(name: String, title: String? = nil,
         describe: @escaping (Int) -> String = { v in v.millions },
         
         combine: @escaping (RowValue, RowValue) -> RowValue = { r1, r2 in (r1.value + r2.value, r1.count + r2.count, nil )},
         finish: @escaping (RowValue) -> (RowValue, String) = { r in (r, r.value.millions) },
         
         rowTitle: @escaping (Model.Guild, Model.Player, [SquadEvaluation]) -> String? = { _,_,_ in nil },
         
         apply: @escaping (Model.Guild, Model.Player, [SquadEvaluation]) -> Int
    ) {
        self.name = name
        self.title = title
        self.apply = apply
        self.rowTitle = rowTitle
        self.describe = describe
        
        self.combine = combine
        self.finish = finish
    }
    
    func apply(guild: Model.Guild, player: Model.Player, evaluations: [SquadEvaluation]) -> RowValue {
        return (self.apply(guild, player, evaluations), 1, self.rowTitle(guild, player, evaluations))
    }

}

struct StatsRowExtractors {
    
    static let playerExtractors: [StatsRowExtractor] = [
        StatsRowExtractor(name: "GP") { _, player, _ in return player.stats.gp },
        StatsRowExtractor(name: "Chars GP") { _, player, _ in return player.stats.characterGP },
        StatsRowExtractor(name: "Ships GP") { _, player, _ in return player.stats.shipGP },
        StatsRowExtractor(name: "Ratio", title: "Character:Ships",
            finish: StatsRowExtractor.average(sum:),
            apply: { _, player, _ in return player.stats.characterGP * 100 / (player.stats.shipGP == 0 ? player.stats.characterGP : player.stats.shipGP) }),
        StatsRowExtractor(name: "Balance", title: "Light side or Dark Side",
            describe: { Model.Player.describe(balance: $0) },
            finish: { (($0.value / $0.count, 1, nil), Model.Player.describe(balance: $0.value / $0.count)) },
            apply: { _, player, _ in return player.balance }),
        StatsRowExtractor(name: "Mod Score",
                          title: "DSR Bot style mod score -- higher is better, aim for 3.0.  This is # of mods with secondary speed ≥ 15 / character gp / 100 000",
                          describe: { score in
                            "\(score / 100).\(score % 100 < 10 ? "0\(score % 100)" : "\(score % 100)")"
                          },
                          finish: StatsRowExtractor.average(sum:),
                          rowTitle: { _, player, _ -> String? in
                            "\(player.countSpeedModsGreaterEqual(than: 15)) mods with speed secondaries ≥ 15"
                          },
                          apply: { _, player, _ in
                            Int(player.modScore * 100)
                          }),
        StatsRowExtractor(name: "Gear Score",
                          title: "DSR Bot style gear score -- higher is better.  Number of (>G12 + G13 + relic / 5) / total gp / 100 000.",
                          describe: { score in
                            "\(score / 100).\(score % 100 < 10 ? "0\(score % 100)" : "\(score % 100)")"
                          },
                          finish: StatsRowExtractor.average(sum:),
                          apply: { _, player, _ in
                            Int(player.gearScore * 100)
                          }),
        StatsRowExtractor(name: "Zetas") { _, player, _ in
            return player.units.values.reduce(0, { $0 + $1.zetas.count })
        },
        StatsRowExtractor(name: "Relic Levels") { _, player, _ in
            return player.relicTierCount
        },
        StatsRowExtractor(name: "7*") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.stars >= 7 ? 1 : 0) })
        },
        StatsRowExtractor(name: "R5+", title: "R5 and higher") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.relic >= (5 + 2) ? 1 : 0) })
        },
        StatsRowExtractor(name: "G13+", title: "G13 and higher") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.gear >= 13 ? 1 : 0) })
        },
        StatsRowExtractor(name: "G12+", title: "G12 and higher") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.gear >= 12 ? 1 : 0) })
        },
        StatsRowExtractor(name: "G10+", title: "G10 and higher") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.gear >= 10 ? 1 : 0) })
        },
        StatsRowExtractor(name: "JKR", title: "Jedi Knight Revan") { _, player, _ in
            return player.units["JEDIKNIGHTREVAN"] == nil ? 0 : 1
        },
        StatsRowExtractor(name: "DR", title: "Darth Revan") { _, player, _ in
            return player.units["DARTHREVAN"] == nil ? 0 : 1
        },
        StatsRowExtractor(name: "Malak") { _, player, _ in
            return player.units["DARTHMALAK"] == nil ? 0 : 1
        },
        StatsRowExtractor(name: "Padme") { _, player, _ in
            return player.units["PADMEAMIDALA"] == nil ? 0 : 1
        },
        StatsRowExtractor(name: "GBA", title: "Genosian Brood Alpha") { _, player, _ in
            return player.units["GEONOSIANBROODALPHA"] == nil ? 0 : 1
        },
        StatsRowExtractor(name: "Wat", title: "Wat Tambor") { _, player, _ in
            return player.units["WATTAMBOR"] == nil ? 0 : 1
        },
        StatsRowExtractor(name: "KAM", title: "Ki-Adi-Mundi") { _, player, _ in
            return player.units["KIADIMUNDI"] == nil ? 0 : 1
        },
        StatsRowExtractor(name: "Shaak", title: "Shaak Ti") { _, player, _ in
            return player["Shaak Ti"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "GAS", title: "General Skywalker") { _, player, _ in
            return player["General Skywalker"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "Rey") { _, player, _ in
            return player["Rey"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "SL Kylo", title: "Supreme Leader Kylo Ren") { _, player, _ in
            return player["SL Kylo"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "SEE", title: "Sith Eternal Emperor") { _, player, _ in
            return player["Sith Eternal Emperor"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "JMLS", title: "Jedi Master Luke Skywalker") { _, player, _ in
            return player["Jedi Master Luke Skywalker"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "JMK", title: "Jedi Master Kenobi") { _, player, _ in
            return player["Jedi Master Kenobi"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "JKL", title: "Jedi Knight Luke") { _, player, _ in
            return player["JKL"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "Neg", title: "The Negotiator") { _, player, _ in
            return player["Negotiator"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "Mal", title: "Malevolence") { _, player, _ in
            return player["Malevolence"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "Rad", title: "Raddus") { _, player, _ in
            return player["Raddus"].stars > 0 ? 1 : 0
        },
        StatsRowExtractor(name: "Fin", title: "Finalizer") { _, player, _ in
            return player["Finalizer"].stars > 0 ? 1 : 0
        },
    ]
    
    static let playerEvaluationExtractors: [StatsRowExtractor] = [
        StatsRowExtractor(name: "Squads", title: "TW/GA Squads - farmed but may need gear.  Click player name to view.") { _, player, evaluations in
            return evaluations.filter { $0.status.isViable }.count
        },
        StatsRowExtractor(name: "Def", title: "Squads: defense or both") { _, player, evaluations in
            return evaluations.filter() { $0.status.isViable && $0.squad.counterTeam?.type.isDefense ?? false }.count
        },
        StatsRowExtractor(name: "Off", title: "Squads: offense or both") { _, player, evaluations in
            return evaluations.filter() { $0.status.isViable && $0.squad.counterTeam?.type.isOffense ?? false }.count
        },
    ]
    
    static let guildCountStatsExtractors: [StatsRowExtractor] = [
        StatsRowExtractor(name: "Zetas") { _, player, _ in
            return player.units.values.reduce(0, { $0 + $1.zetas.count })
        },
        StatsRowExtractor(name: "7*") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.stars >= 7 ? 1 : 0) })
        },
        StatsRowExtractor(name: "G13+", title: "G13 and higher") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.gear >= 13 ? 1 : 0) })
        },
        StatsRowExtractor(name: "G12+", title: "G12 and higher") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.gear >= 12 ? 1 : 0) })
        },
        StatsRowExtractor(name: "G10+", title: "G10 and higher") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.gear >= 10 ? 1 : 0) })
        },
        StatsRowExtractor(name: "Squads", title: "TW/GA Squads - farmed but may need gear.  Click player name to view.") { _, player, evaluations in
            return evaluations.filter { $0.status.isViable }.count
        },
        StatsRowExtractor(name: "Def", title: "Squads: defense or both") { _, player, evaluations in
            return evaluations.filter() { $0.status.isViable && $0.squad.counterTeam?.type.isDefense ?? false }.count
        },
        StatsRowExtractor(name: "Off", title: "Squads: offense or both") { _, player, evaluations in
            return evaluations.filter() { $0.status.isViable && $0.squad.counterTeam?.type.isOffense ?? false }.count
        },
    ]

    static let allianceStatsExtractors: [StatsRowExtractor] = [
        StatsRowExtractor(name: "Zetas") { _, player, _ in
            return player.units.values.reduce(0, { $0 + $1.zetas.count })
        },
        StatsRowExtractor(name: "7*") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.stars >= 7 ? 1 : 0) })
        },
        StatsRowExtractor(name: "G13+", title: "G13 and higher") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.gear >= 13 ? 1 : 0) })
        },
        StatsRowExtractor(name: "G12+", title: "G12 and higher") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.gear >= 12 ? 1 : 0) })
        },
        StatsRowExtractor(name: "G10+", title: "G10 and higher") { _, player, _ in
            return player.units.values.reduce(0, { $0 + ($1.gear >= 10 ? 1 : 0) })
        },
    ]

}

struct StatsRowFactory {
    
    var extractors: [StatsRowExtractor]
    
    func emptyRow() -> StatsRow {
        return StatsRow(count: extractors.count)
    }

    func newRow(guild: Model.Guild, player: Model.Player, evaluations: [SquadEvaluation] = []) -> StatsRow {
        var row = StatsRow(count: extractors.count)
        
        var index = 0
        for extractor in extractors {
            let v = extractor.apply(guild: guild, player: player, evaluations: evaluations)
            row.values[index] = v.value
            row.counts[index] = v.count
            row.title[index] = v.title
            row.text[index] = extractor.describe(v.value)
            
            index += 1
        }
        
        return row
    }
    
    func header() -> HTMLFramgment {
        return H.foreach(extractors) { extractor in
            H.th {
                extractor.title.map { title in
                    H.div().title(title) {
                        extractor.name
                    } as HTMLFramgment
                } ?? extractor.name
            }
        }
    }
}

struct StatsRow {
    
    /// the sorting value of the row
    var values: [Int]

    /// text representation of the row
    var text: [String]
    
    var title: [String?]

    /// temporary storage for accumulating
    var counts: [Int]
    
    init(count: Int) {
        values = [Int](repeating: 0, count: count)
        counts = [Int](repeating: 0, count: count)
        text = [String](repeating: "", count: count)
        title = [String?](repeating: nil, count: count)
    }
        
    subscript(_ index: Int) -> StatsRowExtractor.RowValue {
        get { (values[index], counts[index], title[index] ) }
        set {
            values[index] = newValue.value
            counts[index] = newValue.count
            title[index] = newValue.title
        }
    }
    
    mutating func add(row: StatsRow, rowFactory: StatsRowFactory) {
        for i in 0 ..< values.count {
            self[i] = rowFactory.extractors[i].combine(self[i], row[i])
        }
    }
    
    mutating func finishAdd(rowFactory: StatsRowFactory) {
        for i in 0 ..< values.count {
            let (v, t) = rowFactory.extractors[i].finish(self[i])
            self[i] = v
            text[i] = t
        }
    }
}

extension StatsRow : HTMLFramgment {
    
    func render() -> String {
        return H.foreach(0 ..< values.count) { index in
            H.td().order(self.values[index]).align("right") {
                if self.title[index] != nil {
                    // prefer title + text
                    H.div().title(self.title[index]!) {
                        self.text[index]
                    }
                } else if self.values[index].millions == self.text[index] {
                    // if the clean description is the same as the text, no title
                    self.text[index]
                } else {
                    // add a title to get the full value
                    H.div().title(self.values[index].millions) {
                        self.text[index]
                    }
                }
            }
        }.render()
    }
    
    var isEmpty: Bool {
        return values.count > 0
    }
    
    var description: String {
        return render()
    }
    
}
