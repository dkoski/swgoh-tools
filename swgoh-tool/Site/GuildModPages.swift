//
//  GuildModPages.swift
//  swgoh-tool
//
//  Created by David Koski on 2/8/21.
//  Copyright © 2021 David Koski. All rights reserved.
//

import SWGOH

private let faq = """
    <p>
        <b>Q:</b> What does the score mean?  <b>A:</b> Higher is probably better.  If you match the target mod exactly,
        you will get a score of zero.  If you are way below in many of the stats (or the highest priority stats) you
        may get a large negative score.  If you exceed the stats (and take a look at the "great mod" if you click the
        link), you will get a high score.
        <br>
        <b>Q:</b> What if it says I need more speed?  <b>A:</b> You probably do.  Speed is a good thing to have.
        <br>
        <b>Q:</b> It says I seed 8775 protection but I only have 7563 -- what do I do?  <b>A:</b> You can keep working toward the target, or
        since you already have about 85% of the target, call it good enough.  The score is based on priority and percent of
        target.
        <br>
        <b>Q:</b> I like to mod my Daka with CA and Defense but it says otherwise.  <b>A:</b> Me too -- this selects common configurations
        as targets and you may have other ideas.  Be yourself!
        <br>
        <b>Q:</b> Mods optimizer tells me to mod them a different way.  <b>A:</b> Could be -- this is based on what people actually
        use.  You need to decide.
        <br>
        <b>Q:</b> This says my mods on Gar Saxon are no good, do I need to fix them?  <b>A:</b> You need to make that call (but no in this case!).
        This will score any character that is G8 or higher and has a full set of mods.  Maybe you just parked some mods there.  If you don't
        use the toon, I wouldn't bother modding it.
    """

struct ModsPlayerPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/mods-player.html" }
    
    let guild: Model.Guild

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "Roster Mod Viewer", nav: "<a href=\"index.html\">\(guild.name)</a>")
        
        result += """
            <script>
                var urlParams = new URLSearchParams(window.location.search)
                var allyCode = urlParams.get('id')
                var characterUrl = "\(html.url("/c"))/"

                var modReferenceData = {}
                var modData = {}
                
                var table = null
                
                jQuery.ajax({
                type: 'GET',
                async: true,
                url: 'modData.json',
                success: function (d1) {
                    modData = d1
                    
                    jQuery.ajax({
                    type: 'GET',
                    async: true,
                    url: '\(html.url("/modReference.json"))',
                    success: function (d2) {
                        
                        modReferenceData = d2
                        setup()
                    }})
                }})
                
                var s_to_index = {
                    "health": 3,
                    "protection": 4,
                    "defense": 2,
                    "offense": 1,
                    "speed": 0,
                    "crit damage": 6,
                    "crit chance": 5,
                    "crit avoidance": 10,
                    "accuracy": 9,
                    "potency": 7,
                    "tenacity": 8,
                }
                
                function stats(id, gear, index, stats) {
                    // look up the stats for this cluster
                    var c = modReferenceData.characters[id].c[index]
                    var order = c.p
                    var result = [ null, null, null, null, null, null, null, null, null, null, null]
                    
                    // convert the array of stats from the modData -> the array of stat values
                    order.forEach(function(s, i) {
                        result[s_to_index[s]] = [stats[i], c.t[gear][i]]
                    })
                    return result
                }
                    
                function setup() {
                    // find the player index -- this is used for finding the data in the characters structure
                    var playerIndex = modData.players.findIndex(function (p){ return p.c == allyCode })
                    
                    document.getElementById('name').innerHTML = "Mod Data for " + modData.players[playerIndex].n
                    
                    var characterIds = Object.keys(modData.units).sort()
                    
                    var data = []
                    characterIds.forEach(id => {
                        var u = modData.units[id][playerIndex]
                        if (u != null) {
                            var row = [ id, u.g, u.d, u.s, null ]
                            stats(id, u.g, u.alt ?? 0, u.altv ?? u.v).forEach(v => {
                                row.push(v)
                            })
                            data.push(row)
                        }
                    })
                    
                    table = $('#characters').DataTable({
                    "paging":   false,
                    "info":     false,
                    fixedHeader: true,
                        data: data,
                        order: [[1, "desc"], [3, "asc"]],
                        columnDefs: [
                        {
                            targets: 0,
                            searchable: true,
                            render: function (data, type, row, meta) {
                                var u = modData.units[row[0]][playerIndex]
                                return '<a href="' + characterUrl + row[0] + '.html#mod' + (u.alt ?? 0) + '">' + modReferenceData.characters[row[0]].n + '</a>'
                            }
                        },
                        {
                            targets: 1,
                            render: function (data, type, row, meta) {
                                if (type == "sort" || type == "type") {
                                    return row[meta.col]
                                } else {
                                    var n = ""
                                    if (row[meta.col] > 20) {
                                        n = "R" + (row[meta.col] - 20)
                                    } else {
                                        n = "G" + row[meta.col]
                                    }
                                    return '<a href="https://swgoh.gg/p/' + allyCode + '/characters/' + modReferenceData.characters[row[0]].u + '">' + n + '</a>'
                                }
                            }
                        },
                        {
                            targets: 2,
                            visible: false,
                        },
                        {
                            targets: 4,
                            render: function (data, type, row, meta) {
                                var c = 0
                                row.forEach(function(d, i) {
                                    if (d != null && i > 4) {
                                        if (d[0] < d[1]) {
                                            c += 1
                                        }
                                    }
                                })
                                return c
                            }
                        },
                        {
                            targets: [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                            render: function (data, type, row, meta) {
                                var d = row[meta.col]
                                if (d == null) {
                                    return null
                                }
                                if (type == "sort" || type == "type") {
                                    return d[0]
                                } else {
                                    if (d[0] >= d[1]) {
                                        return "+" + d[0]
                                    } else {
                                        return "<b>+" + d[0] + "/" + d[1] + "</b>"
                                    }
                                }
                            }
                        },
                        { targets: '_all', searchable: false }
                        ]
                    });
                }
                
            </script>
                
            <h2 id="name"></h2>
                
            <p>
                All the mods for a single player.  The table shows what the data mining has determined to be important stats.
                You can sort by any of the columns.  The score computes how much over/under you are compared to the target with
                more important stats (click the character name to see the ordering) counting for more.  Generally a higher score
                is better.  Below zero may need some work (if you want that unit to have good mods).  The target is computed
                by looking at mods in the center of the cluster your mods seem to belong to.  If you
                click on the character name you can see which mods were picked as the target.  The stat values are what you
                would get if you put <i>those</i> mods on your character.  These numbers are not necessarily <i>correct</i>,
                but they are an indication of where you might aim.

            \(faq)
                
            <p>
                The stats columns will show a single number if you exceed the target stat or two numbers (in bold) -- the first
                number is your stat increase and the second is the target.
                
            <p>
                If you click on the gear value, you will see your mods on swgoh.gg.
                
            <table id="characters">
                <thead><tr>
                    <th>Character</th>
                    <th>Gear</th>
                    <th>Distance</th>
                    <th>Score</th>
                    <td title="Number of stats that are under the target.">Under</th>
                    
                    <th>Spd</th>
                    <th>Off</th>
                    <th>Def</th>
                    <th>Health</th>
                    <th>Prot</th>
                    <th>CC</th>
                    <th>CD</th>
                    <th>Pot</th>
                    <th>Ten</th>
                    <th>Acc</th>
                    <th>CA</th>
                </tr></thead>
            </table>
            """
        
        result += html.footer()
        
        return result
    }

}

struct ModsPlayersPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/mods-players.html" }
    
    let guild: Model.Guild

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "Guild Mod Report: By Player", nav: "<a href=\"index.html\">\(guild.name)</a>")
        
        result += """
            <script>
                var modReferenceData = {}
                var modData = {}
                
                var scoreThreshold = -25
                var table = null
                
                jQuery.ajax({
                type: 'GET',
                async: true,
                url: 'modData.json',
                success: function (d1) {
                    modData = d1
                    
                    jQuery.ajax({
                    type: 'GET',
                    async: true,
                    url: '\(html.url("/modReference.json"))',
                    success: function (d2) {
                        
                        modReferenceData = d2
                        setup()
                    }})
                }})
                    
                function setup() {
                    var characterIds = Object.keys(modData.units).sort()
                    var data = []
                    modData.players.forEach(function(player, i) {
                        var m = 0
                        var a = 0
                        var p = 0
                        var n = 0
                                    
                        characterIds.forEach(id => {
                            var u = modData.units[id][i]
                            if (u != null) {
                                m += 1
                                if (u.d <= 250) {
                                    // inside the cluster
                                    a += 1
                                }
                                if (u.s >= scoreThreshold) {
                                    p += 1
                                } else {
                                    n += 1
                                }
                            }
                        })
                        
                        data.push([
                            [player.n, player.c],
                            m,
                            a,
                            p,
                            n,
                        ])
                    })
                    
                    table = $('#characters').DataTable({
                    "paging":   false,
                    "info":     false,
                    fixedHeader: true,
                        data: data,
                        columnDefs: [
                        {
                            targets: 0,
                            searchable: true,
                            render: function (data, type, row, meta) {
                                return '<a href="mods-player.html?id=' + row[0][1] + '">' + row[0][0] + "</a>"
                            }
                        },
                        {
                            targets: 2,
                            visible: false,
                        },
                        { targets: '_all', searchable: false }
                        ]
                    });
                }
                
            </script>
                
            <h2 id="name">Mod Report for \(guild.name)</h2>

            <p>
                This table shows summary information for all the mods across all the players in your guild, by player
                (see <a href="mods-characters.html">them by character</a> as well).  If you click on a player
                name, you will see details about their mods.
                
            <table id="characters">
                <thead><tr>
                    <th>Player</th>
                    <th title="Number of units G8+ and modded.">Modded</th>
                    <th>Aligned Units</th>
                    <th title="Number of units with a positive score.">Positive Scores</th>
                    <th title="Number of units with a negative (may need work) score.">Negative Scores</th>
                </tr></thead>
            </table>
            """
        
        result += html.footer()
        
        return result
    }

}

struct ModsStatsPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/mods-stats.html" }
    
    let guild: Model.Guild

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "Guild Mod Stats", nav: "<a href=\"index.html\">\(guild.name)</a>")
        
        result += """
            <h2 id="name">Mod Stats for \(guild.name)</h2>

            <p>
                This table shows summary information for all the mods across all the players in your guild, by player
                (see <a href="mods-characters.html">them by character</a> as well).  If you click on a player
                name, you will see details about their mods.
                
            <p>
                The headers show the character name and the target value for the stat for an R5 unit using the <i>most common</i>
                modding strategy.
                
            <p>
                <form name="params" action="mods-stats.html" method="get">
                <b>Stat:</b>
                <select name="stat" onchange="document.params.submit()">
                    <option value="speed">speed</option>
                    <option value="offense">offense</option>
                    <option value="defense">defense</option>
                    <option value="health">health</option>
                    <option value="protection">protection</option>
                    <option value="crit chance">crit chance</option>
                    <option value="crit damage">crit damage</option>
                    <option value="crit avoidance">crit avoidance</option>
                    <option value="potency">potency</option>
                    <option value="tenacity">tenacity</option>
                    <option value="accuracy">accuracy</option>
                </select>
                <b>Sort By:</b>
                <select name="sort" onchange="document.params.submit()">
                    <option value="name">name</option>
                    <option value="target_asc">target +</option>
                    <option value="target_desc">target -</option>
                </select>
                <b>Gear:</b><input name="gearFilter" type="text" placeholder="Gear >=, R1 = 21" min="1" max="28" pattern="[0-9]+" onchange="document.params.submit()">
                <b>Stat Value:</b><input name="statFilter" type="text" placeholder="Stats <=" pattern="[0-9]+" onchange="document.params.submit()">
                <b>Name:</b><input name="nameFilter" type="text" placeholder="names and tags" onchange="document.params.submit()">
                </form>
                
                
            <table id="characters">
                <thead><tr>
                    <th>Player</th>
                </tr></thead>
            </table>

            <script>
                var urlParams = new URLSearchParams(window.location.search)
                var stat = urlParams.get('stat') ?? "speed"
                
                var gearFilter = urlParams.get('gearFilter')
                var statFilter = urlParams.get('statFilter')
                var nameFilter = urlParams.get('nameFilter')
                var sortBy = urlParams.get('sort') ?? "name"

                document.params.stat.value = stat
                document.params.gearFilter.value = gearFilter
                document.params.statFilter.value = statFilter
                document.params.nameFilter.value = nameFilter
                document.params.sort.value = sortBy
                
                var modReferenceData = {}
                var modData = {}
                
                var scoreThreshold = -25
                var table = null
                var data = []
                var characterIds = []
                
                jQuery.ajax({
                type: 'GET',
                async: true,
                url: 'modData.json',
                success: function (d1) {
                    modData = d1
                    
                    jQuery.ajax({
                    type: 'GET',
                    async: true,
                    url: '\(html.url("/modReference.json"))',
                    success: function (d2) {
                        
                        modReferenceData = d2
                        setup()
                    }})
                }})
                
                function addColumn(title, target, link) {
                    var t = document.getElementById("characters")
                    var header = t.createTHead()
                    var row = header.rows[0]
                    var cell = row.insertCell(-1)
                    cell.innerHTML = '<b><a href="' + link +'">' + title + '</a></b>' + (target == null ? "" : "<br>+ " + target)
                }
                    
                function setup() {
                    // sort character ids by name (not just id)
                    characterIds = Object.keys(modData.units).sort(function (a, b) {
                        if (sortBy == "name") {
                            var aa = modReferenceData.characters[a].n
                            var bb = modReferenceData.characters[b].n
                            if (aa < bb) { return -1 }
                            if (aa > bb) { return 1 }
                            return 0
                        } else {
                            var dir = sortBy == "target_asc" ? 1 : -1
                            var ai = modReferenceData.characters[a].c[0].p.indexOf(stat)
                            var bi = modReferenceData.characters[b].c[0].p.indexOf(stat)
                            var aa = ai == -1 ? 0 : modReferenceData.characters[a].c[0].t[25][ai]
                            var bb = bi == -1 ? 0 : modReferenceData.characters[b].c[0].t[25][bi]
                            if (aa < bb) { return -dir }
                            if (aa > bb) { return dir }
                            return 0
                        }
                    })
                    
                    var names = nameFilter == null ? null : nameFilter.replace(",", " ").split(" ").map(v => {
                        return v.trim().toLowerCase()
                    })
                    
                    characterIds = characterIds.filter(cid => {
                        var rmd = modReferenceData.characters[cid]
                        
                        // honor the name filter
                        if (names != null) {
                            var pass = false
                            names.forEach(n => {
                                // allow any of the names to match
                                if (rmd.n.toLowerCase().indexOf(n) != -1) {
                                    pass = true
                                }
                            })
                            if (!pass) {
                                // not a name, require all of the tags to match
                                var allTagsMatched = true
                                names.every(n => {
                                    var foundTag = false
                                    rmd.t.forEach(t => {
                                        if (t.indexOf(n) != -1) {
                                            foundTag = true
                                        }
                                    })
                                    if (!foundTag) {
                                        allTagsMatched = false
                                        return false
                                    } else {
                                        return true
                                    }
                                })
                                
                                if (allTagsMatched) {
                                    pass = true
                                }
                            }
                            if (!pass) {
                                return false
                            }
                        }
                        var statIndex = rmd.c[0].p.indexOf(stat)
                        return statIndex != -1
                    })
                                    
                    // build the columns
                    characterIds.forEach(cid => {
                        var rmd = modReferenceData.characters[cid]
                        var index = modReferenceData.characters[cid].c[0].p.indexOf(stat)
                        var t = index == -1 ? null : modReferenceData.characters[cid].c[0].t[25][index]
                        
                        addColumn(rmd.n, t, "mods-character.html?id=" + cid)
                    })
                    
                    // gear filter
                    var g = gearFilter
                    if (g == null || g.length == 0) {
                        g = 0
                    } else {
                        g = parseInt(g)
                    }
                    
                    // stat filter
                    var s = statFilter
                    if (s == null || s.length == 0) {
                        s = 10000000
                    } else {
                        s = parseInt(s)
                    }
                            
                    // rows will be player, (gear level, stat) ...
                    modData.players.forEach(function(player, playerIndex) {
                        var keep = false
                        var row = [[player.n, player.c]]
                        
                        characterIds.forEach(cid => {
                            var rmd = modReferenceData.characters[cid]
                            var statIndex = rmd.c[0].p.indexOf(stat)
                            if (statIndex >= 0) {
                                var u = modData.units[cid][playerIndex]
                                if (u == null || u.g < g || u.v[statIndex] > s) {
                                    row.push(null)
                                } else {
                                    row.push([u.g, u.v[statIndex]])
                                    keep = true
                                }
                            }
                        })
                        
                        if (keep) {
                            data.push(row)
                        }
                    })
                    
                    var config = {
                        "paging":   false,
                        "info":     false,
                        bFilter: false,
                        data: data,
                        columnDefs: [
                        {
                            targets: 0,
                            searchable: true,
                            width: 250,
                            render: function (data, type, row, meta) {
                                return '<a href="mods-player.html?id=' + row[0][1] + '">' + row[0][0] + "</a>"
                            }
                        },
                        {
                            targets: '_all',
                            searchable: false,
                            render: function (data, type, row, meta) {
                                var d = row[meta.col]
                                if (d == null) {
                                    return null
                                }
                                if (type == "sort" || type == "type") {
                                    return d[1]
                                } else {
                                    var n = ""
                                    if (d[0] > 20) {
                                        n = "R" + (d[0] - 20)
                                    } else {
                                        n = "G" + d[0]
                                    }
                                    
                                    return "+" + d[1] + "&nbsp;" + n
                                }
                            }
                        },
                        ]
                    }
                    
                    if (nameFilter == null) {
                        config['fixedColumns'] = { leftColumns: 1 }
                        config['scrollX'] = true
                    }

                    table = $('#characters').DataTable(config);
                }
                
                function updateFilter() {
                    // gear filter
                    var g = $('#gear-filter').val()
                    if (g == null || g.length == 0) {
                        g = 0
                    } else {
                        g = parseInt(g)
                    }
                    
                    // stat filter
                    var s = $('#stat-filter').val()
                    if (s == null || s.length == 0) {
                        s = 10000000
                    } else {
                        s = parseInt(s)
                    }
                    
                    var n = $('#name-filter').val()
                    if (n.length == 0) {
                        n = null
                    } else {
                        n = n.toLowerCase()
                    }
                            
                    var columns_to_show = {}
                    var columns_to_hide = []
                    characterIds.forEach(function(id, i) {
                        if (n != null && modReferenceData.characters[id].n.toLowerCase().indexOf(n) == -1) {
                            columns_to_hide.push(i + 1)
                        } else {
                            columns_to_show[i + 1] = true
                        }
                    })

                    table.columns(columns_to_hide).visible(false)
                    table.columns(Object.keys(columns_to_show)).visible(true)

                    var filteredData = []
                    
                    data.forEach(inputRow => {
                        var keep = false
                        var row = []
                        
                        inputRow.forEach(function(v, i) {
                            if (i == 0) {
                                row.push(v)
                            } else if (columns_to_show[i]) {
                                if (v == null) {
                                    row.push(null)
                                } else  if (v[0] >= g && v[1] < s) {
                                    keep = true
                                    row.push(v)
                                } else {
                                    row.push(null)
                                }
                            } else {
                                row.push(null)
                            }
                        })
                        
                        if (keep) {
                            filteredData.push(row)
                        }
                    })
                    
                    table.clear()
                    table.rows.add(filteredData)
                    table.draw(false)
                }
                
            </script>
            """
        
        result += html.footer()
        
        return result
    }
}

struct ModsCharacterPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/mods-character.html" }
    
    let guild: Model.Guild

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "Guild Mod Report: Single Character", nav: "<a href=\"index.html\">\(guild.name)</a>")
        
        result += """
            <script>
                var urlParams = new URLSearchParams(window.location.search)
                var id = urlParams.get('id') ?? 'ADMIRALACKBAR'
                var characterUrl = "\(html.url("/c"))/"

                var modReferenceData = {}
                var modData = {}
                
                var table = null
                    
                
                jQuery.ajax({
                type: 'GET',
                async: true,
                url: 'modData.json',
                success: function (d1) {
                    modData = d1
                    
                    jQuery.ajax({
                    type: 'GET',
                    async: true,
                    url: '\(html.url("/modReference.json"))',
                    success: function (d2) {
                        
                        modReferenceData = d2
                        setup()
                    }})
                }})
                
                function addColumn(title) {
                    var t = document.getElementById("characters")
                    var header = t.createTHead()
                    var row = header.rows[0]
                    var cell = row.insertCell(-1)
                    cell.innerHTML = "<b>" + title + "</b>"
                }
                            
                function setup() {
                    document.getElementById('name').innerHTML = 'Mod Data for <a href="' + characterUrl + id + '.html#mod0">' + modReferenceData.characters[id].n + '</a>'
                    
                    // sort character ids by name (not just id)
                    var characterIds = Object.keys(modData.units).sort(function (a, b) {
                        var aa = modReferenceData.characters[a].n
                        var bb = modReferenceData.characters[b].n
                        if (aa < bb) { return -1 }
                        if (aa > bb) { return 1 }
                        return 0
                    })
                    
                    // set up the list to show characters to vie
                    select = document.getElementById('select')
                    characterIds.forEach(c => {
                        var option = document.createElement("option");
                        option.text = modReferenceData.characters[c].n
                        option.value = c
                        if (c == id) {
                            option.selected = true
                        }
                        select.add(option)
                    })
                    
                    // add the columns for the stats for this character
                    modReferenceData.characters[id].c[0].p.forEach(addColumn)
                    
                    var data = []
                    modData.players.forEach(function(player, i) {
                        var u = modData.units[id][i]
                        if (u != null) {
                            var row = [
                                [player.n, player.c],
                                u.g,
                                u.d,
                                u.s,
                                null,
                            ]
                        
                            u.v.forEach(function (v, i) {
                                // the stat and the target stat for this gear level
                                row.push([v, modReferenceData.characters[id].c[0].t[u.g][i]])
                            })
                        
                            data.push(row)
                        } else {
                            var row = [
                                [player.n, player.c],
                                null,
                                null,
                                null,
                                null,
                            ]
                            modReferenceData.characters[id].c[0].p.forEach(v => {
                                row.push(null)
                            })
                            data.push(row)
                        }
                    })
                    
                    table = $('#characters').DataTable({
                    "paging":   false,
                    "info":     false,
                    fixedHeader: true,

                        data: data,
                        columnDefs: [
                        {
                            targets: 0,
                            searchable: true,
                            render: function (data, type, row, meta) {
                                var u = modData.units[id][meta.row]
                                if (u == null) {
                                    return '<a href="mods-player.html?id=' + row[0][1] + '">' + row[0][0] + '</a>'
                                } else {
                                    return '<a href="mods-player.html?id=' + row[0][1] + '">' + row[0][0] + '</a>' + ' [<a href="' + characterUrl + id + '.html#mod' + (u.alt ?? 0) + '">mods</a>]'
                                }
                            }
                        },
                        {
                            targets: 1,
                            render: function (data, type, row, meta) {
                                if (type == "sort" || type == "type") {
                                    return row[meta.col]
                                } else {
                                    if (row[meta.col] == null) {
                                        return null
                                    }
                                    var n = ""
                                    if (row[meta.col] > 20) {
                                        n = "R" + (row[meta.col] - 20)
                                    } else {
                                        n = "G" + row[meta.col]
                                    }
                                    return '<a href="https://swgoh.gg/p/' + row[0][1] + '/characters/' + modReferenceData.characters[id].u + '">' + n + '</a>'
                                }
                            }
                        },
                        {
                            targets: [2, 3],
                            render: function (data, type, row, meta) {
                                return row[meta.col]
                            }
                        },
                        {
                            targets: 2,
                            visible: false,
                        },
                        {
                            targets: 4,
                            render: function (data, type, row, meta) {
                                var c = 0
                                row.forEach(function(d, i) {
                                    if (d != null && i > 4) {
                                        if (d[0] < d[1]) {
                                            c += 1
                                        }
                                    }
                                })
                                return c
                            }
                        },
                        {
                            targets: '_all',
                            render: function (data, type, row, meta) {
                                var d = row[meta.col]
                                if (d == null) {
                                    return null
                                }
                                
                                // the stat index
                                var i = meta.col - 4
                                if (type == "sort" || type == "type") {
                                    return d[0]
                                } else {
                                    if (d[0] >= d[1]) {
                                        return "+" + d[0]
                                    } else {
                                        return "<b>+" + d[0] + "/" + d[1] + "</b>"
                                    }
                                }
                            },
                        },
                        { targets: '_all', searchable: false }
                        ]
                    });
                }
                
                function reload() {
                    select = document.getElementById('select')
                    window.location = "?id=" + select.value
                }
                
            </script>
                
            <h2 id="name"></h2>
                
            <p>
                All the mods for a single character across all the players.  The table shows what the data mining has
                determined to be important stats (in particular the primary configuration -- there may be multiple).
                You can sort by any of the columns.  The score computes how much over/under you are compared to the target with
                more important stats (click the [mods] link to see more info) counting for more.  Generally a higher score
                is better.  Below zero may need some work (if you want that unit to have good mods).  The target is computed
                by looking at mods in the center of the cluster your mods seem to belong to.  If you
                click on the [mods] link you can see which mods were picked as the target.  The stat values are what you
                would get if you put <i>those</i> mods on your character.  These numbers are not necessarily <i>correct</i>,
                but they are an indication of where you might aim.
                
            \(faq)

            <p>
                The stats columns will show a single number if you exceed the target stat or two numbers (in bold) -- the first
                number is the stat increase and the second is the target.
                
            <p>
                If you click on the gear value, you will see your mods on swgoh.gg.

            <p>
            <select name="id" id="select" onchange="if (this.selectedIndex) reload();">
            </select>
                
            <table id="characters">
                <thead><tr>
                    <th>Character</th>
                    <th>Gear</th>
                    <th>Distance</th>
                    <th>Score</th>
                    <th>Under</th>
                </tr></thead>
            </table>
            """
        
        result += html.footer()
        
        return result
    }
}

struct ModsCharactersPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/mods-characters.html" }
    
    let guild: Model.Guild

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "Guild Mod Report: By Character", nav: "<a href=\"index.html\">\(guild.name)</a>")
        
        result += """
            <script>
                var modReferenceData = {}
                var modData = {}
                
                var scoreThreshold = -25
                var table = null
                
                jQuery.ajax({
                type: 'GET',
                async: true,
                url: 'modData.json',
                success: function (d1) {
                    modData = d1
                    
                    jQuery.ajax({
                    type: 'GET',
                    async: true,
                    url: '\(html.url("/modReference.json"))',
                    success: function (d2) {
                        
                        modReferenceData = d2
                        setup()
                    }})
                }})
                    
                function setup() {
                    // sort character ids by name (not just id)
                    var characterIds = Object.keys(modData.units).sort(function (a, b) {
                        var aa = modReferenceData.characters[a].n
                        var bb = modReferenceData.characters[b].n
                        if (aa < bb) { return -1 }
                        if (aa > bb) { return 1 }
                        return 0
                    })
                    
                    
                    var data = []
                    characterIds.forEach(id => {
                        var units = modData.units[id]
                        
                        var m = 0
                        var a = 0
                        var p = 0
                        var n = 0
                        
                        units.forEach(u => {
                            if (u != null) {
                                m += 1
                                if (u.d <= 250) {
                                    // inside the cluster
                                    a += 1
                                }
                                if (u.s >= 0) {
                                    p += 1
                                } else {
                                    n += 1
                                }
                            }
                        })
                        
                        data.push([
                            [modReferenceData.characters[id].n, id],
                            m,
                            a,
                            p,
                            n,
                        ])
                        
                    })
                    
                    table = $('#characters').DataTable({
                    "paging":   false,
                    "info":     false,
                    fixedHeader: true,
                        
                        data: data,
                        columnDefs: [
                        {
                            targets: 0,
                            searchable: true,
                            render: function (data, type, row, meta) {
                                return '<a href="mods-character.html?id=' + row[0][1] + '">' + row[0][0] + "</a>"
                            }
                        },
                        {
                            targets: 2,
                            visible: false,
                        },
                        { targets: '_all', searchable: false }
                        ]
                    });
                }
                
            </script>
                
            <h2 id="name">Mod Report for \(guild.name)</h2>

            <p>
                This table shows summary information for all the mods across all the players in your guild, by character
                (see <a href="mods-players.html">them by player</a> as well).  If you click on a character
                name, you will see details about all the players mods for that character.

                
            <table id="characters">
                <thead><tr>
                    <th>Character</th>
                    <th>Modded</th>
                    <th>Aligned Units</th>
                    <th>Positive Scores</th>
                    <th>Negative Scores</th>
                </tr></thead>
            </table>
            """
        
        result += html.footer()
        
        return result
    }
}
