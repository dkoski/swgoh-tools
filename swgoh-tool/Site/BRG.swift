//
//  Copyright © 2021 David Koski. All rights reserved.
//

import SWGOH

struct BRG : AllianceBranding {
    
    let baseURL = URL(string: "https://gorgatron1.github.io/brg/")
    
    let zetaBlurb = "The first one is popularity within the BRG Alliance."
    let farmBlurb = "These are farming recommendations based on the popularity of toons in the BRG Alliance and are shown in order of popularity."
    
    func backgroundImageCss(_ html: HTML) -> String {
        return
            """
            <style type="text/css">
            body {
             background-image: url("\(html.i("more_faded_logo.jpg"))");
             background-position: center;
             background-repeat: no-repeat;
             background-size: cover;
            }
            </style>
            """
    }

    func playerHeader(_ html: HTML, guild: Model.Guild, player: Model.Player, title: String, navigation: String) -> String {
        return
            """
            <title>BRG Alliance - \(guild.name)\(title.isEmpty ? "" : ": " + title)</title>
            <div style="display: flex; align-items:baseline"><img src="\(html.i("simple_logo_48.jpg"))"/>&nbsp;<h1>Alliance - \(guild.name)\(title.isEmpty ? "" : ": " + title)</h1></div>
            
            <div style="background-color: #f0f0f0; border: solid red; padding: 10px; border-radius: 10px">
            \(navigation)
            <a href="\(html.alliance("index.html"))">Alliance</a> |
            <a href="\(html.g(guild, "index.html"))">\(guild.name)</a> |
            <a href="\(html.p(player, "info"))">Info</a> |
            <a href="\(html.g(guild, "mods-player.html"))?id=\(player.stats.allyCode)">Mods</a> |
            <a href="\(html.p(player, "tw-squads"))">TW+GA Squads</a> |
            <a href="\(html.p(player, "counters"))">Counters</a> |
            <a href="\(html.p(player, "target"))">Targets</a> |
            <a href="\(html.p(player, "unlock"))">Unlocks</a> |
            <a href="\(html.p(player, "zeta"))">Zetas</a> |
            <a href="\(html.p(player, "farm"))">Farming</a> |
            <a href="\(html.p(player, "dsgeotb"))">DS Geo TB</a> |
            <a href="\(html.p(player, "lsgeotb"))">LS Geo TB</a> |
            <a href="\(html.g(guild, "rancor.html?filter=\(player.id)"))">CPIT</a>
            \(
                H.div().style("float:right;") {
                    H.a(href: html.resources()) { "Resources" }
                }.render()
            )
            </div>
            <br/>
            """
    }
    
    func header(_ html: HTML, title: String, nav: String) -> String {
        return
            """
            <title>BRG Alliance Guild Reports\(title.isEmpty ? "" : ": " + title)</title>
            <div style="display: flex; align-items:baseline"><img src="\(html.i("simple_logo_48.jpg"))"/>&nbsp;<h1>Alliance Guild Reports\(title.isEmpty ? "" : ": " + title)</h1></div>
            
            <div style="background-color: #f0f0f0; border: solid red; padding: 10px; border-radius: 10px">
            <a href="\(html.alliance("index.html"))">Alliance</a> |
            \(
                H.div().style("float:right;") {
                    H.a(href: html.resources()) { "Resources" }
                }.render()
            )
            \(nav)
            </div>
            <br/>
            """
    }

    var alliancePages: [AllianceSite.GuildBuilder] {
        [
            AllianceSite.GuildBuilder("POI") {
                try PersonOfInterestPage().build(base: $2)
            }
        ]
    }
    
    // MARK: - Challenge Rancor info
    
    // pbpaste | awk 'BEGIN {print "<ul>"} END { print "</ul>"} { gsub(/^[ \t]*-[ \t]+/, ""); printf("<li>%s\n", $0) }'
                
    let challengeRancorInfo = ChallengeRancor.Info(
        intro:
            """
            <b>Thanks to jraenar#4239 (Porgs) for contributing advice for the teams and phases!</b>
            
            <p>
            This page is full of information about the Challenge Rancor Raid.  Links in the
            header of the table give more information about the teams, where available.
            <a href="https://www.youtube.com/watch?v=7uczv4W7AVk&feature=youtu.be">Darth Kermit's full raid video</a> is always good places to look as well.  Be aware that the raid
            rules changed in April of 2021, so videos and materials about the raid from before
            that time are suspect.
            
            <p>
            Note that the damage estimates are used to build teams, give planners a rough estimate of available damage and give players something to shoot for.  These numbers are usually at the high end of what you will get, but if you consistently fall short, you should ask your guild mates about how to play it!
            <br/>
            <br/>
            Note: some teams may overlap between phases.
            <p>
            <b>Click on a name to filter to only that player.</b>  Click <a href="?">here</a> to show all players again.
            <p>
            Teams shown in <span style="background-color: #ceebfd">blue</span> are suggested teams to play that phase.  A team might not be suggested because it has units that are preferred in later phases or the suggested team would have been used in an earlier phase.
            Teams shown in <span style="background-color: yellow;">yellow</span> need additional gear.
            Counts are shown with <i># ready / # not ready</i>.  Teams that need gear indicate the number of gear levels with <tt>[#]</tt>.  You can
            <button onclick="$('.not-viable').text('&nbsp;').css('background-color', '')">remove not ready</button><br/>
            
            <p>
            An <b>Estimated Score</b> is given for each phase.  This adds up
            the nominal score for each of the suggested play teams.  Performance
            may vary depending on mods and RNG.  The rating right below gives
            a simple interpretation of the score.  Different teams played will give different results -- this is merely meant as a guide.  Good luck!
            """,
        
        summary:
            """
            Summary information by player, by phase.  Counts show number of ready &#11088; teams / need gear &#11088; teams (preferred) and total ready / total that need gear.
            """,
        
        phases: [
            ChallengeRancor.PhaseInfo(
                id: "p1", name: "P1", squads: ChallengeRancor.p1,
                intro:
                    """
                    Prefer these teams for phase 1 (marked with a &#11088; in the table):
                    
                    <ul>
                    <li>Vader, KRU, C-3PO, Wat, (BB8 or Shaak) - 8-11%, depending on relics/mods
                    <li>SEE, Malak, (any 3 Sith, only need to check on SEE+Malak being R5 because of Sith requirements for SEE) - 4-6%
                    <li>Padme, JKA, Ahsoka, GK, GMY (can sub last three with zBarriss, Sarge, R2, C-3PO, if using on other teams) - 3%
                    </ul>
                    
                    <p>
                    Vader also has a preferred P2 team.  SEE is really only useful in this phase.  Padme is best in this phase.
                    """,
                teams: [
                    // MARK: - P1
                    ChallengeRancor.TeamInfo(
                        id: "VADER1", name: "Vader P1", marker: "&#11088;", order: 100,
                        body:
                            """
                            Thanks to jraenar#4239 (Porgs)!
                            
                            <p>
                            Team: Vader, Wat, BB-8 &#11088; / Shaak Ti, C-3PO, KRU

                            <p>
                            <b>How it works:</b> The Vader team is built around stacking up a lot of Damage over Time debuffs (DOTs), then maximizing spike damage from Vader's Culling Blade ability before the boss murders your entire team.
                            
                            <ul>
                            <li>Vader lead re-applies DOTs after they expire.
                            <li>KRU and BB-8 apply Tenacity Down debuff on their basic, to ensure DOTs are not resisted.
                            <li>KRU also takes reduced damage from % Max Health attacks, and taunts, to take the bulk of the attacks for the team.
                            <li>Wat will assist whenever an ally with Tech uses a special ability, and applies more DOTs on this assist.
                            </ul>
                            
                            <b>Strategy:</b> Use the team placed in exactly the order shown in the image: Vader, Wat, BB, 3PO, KRU.
                            <ul>
                            <li>Streaming runs helps to get used to how this team works faster. Ask for help until you get the hang of how the team works with your mods.
                            <li>The run can go bad quickly if you make a mistake. Be ready to retreat or force quit to try again.
                            <li>Wat gives Weapon Tech to Vader, Medpack Tech to BB, and Shield Tech to 3PO, then will only ever use basics for the rest of the run.
                            <li>Be sure to always target the Captain when using specials, to get extra DOTs.
                            <li>Target the side Guards when not adding Tenacity Down or DOTs to the Captain, to prevent feeding extra turn meter.
                            <li>If the Captain loses Tenacity Down, use a KRU basic, BB basic, or 3PO special ASAP to re-apply.
                            <li>When 3PO uses special, always call Wat to assist for double application of DOTs.
                            <li>Vader should use Merciless Massacre and Culling Blade early to reduce other cooldowns, but make sure both are off cooldown for the final Culling Blade! Having Merciless up boosts Vader's offense and crit damage by 50% each on the toss!
                            <li>Save Illuminated Destiny on BB until just before the final Culling Blade. There won't be enough time to use it twice.
                            <li>A good run will have between 40 and 50 DOTs stacked on the Captain at the end.
                            </ul>
                            
                            <b>Mods:</b> On everyone except KRU, try to avoid Health primary and secondaries, to limit damage taken by the % Max Health damage the enemy abilities have.
                            <ul>
                            <li>Vader wants an Offense set, Crit Damage triangle, Offense cross, with lots of offense and speed secondaries, and a bit of crit chance. Target at least +120 speed, +2000 physical damage, and 75%+ crit chance.
                            <li>Wat wants a Speed set, and the fastest speed secondaries you can manage. Adding potency secondaries and a Potency set and/or cross can help land DOTs when Tenacity Down is not applied. Target at least +135 speed, more is better.
                            <li>BB wants a Speed set, and the fastest speed secondaries you can manage. Target at least +150 speed, more is better.
                            <li>3PO wants a Speed set, and the fastest speed secondaries you can manage. Add some protection secondaries to handle splash damage. Target at least +160 speed, more is better.
                            <li>KRU wants Health and Defense sets, with one or two Defense primaries on arrow, triangle, or cross. Target 75%+ armor and 85k+ health.
                            </ul>
                            """,
                        videoURL: "https://www.youtube.com/embed/1xaqCbK7EOU"),
                    
                    ChallengeRancor.TeamInfo(
                        id: "PADME1", name: "Padme P1", marker: "&#11088;", order: 99,
                        body:
                            """
                            Thanks to jraenar#4239 (Porgs)!

                            <p>
                            Team: Padme, JKA, Ahsoka, GK, GMY (can sub last three with zBarriss, Sarge, R2, C-3PO, if using on other teams)
                            
                            <p>
                            <b>How it works:</b> Heals from Padme and Ahsoka specials, and GK taking hits, allows the team to survive just long enough to get big stacks of Courage to convert into true damage on the Captain.
                            <p>
                            <b>Strategy:</b> The strategy in the raid is much the same as general strategy when using Padme. Careful planning on use of specials gets a lot of assists out of Ahsoka for extra damage, and sustains the team longer.
                            
                            <ul>
                            <li>Try to limit mass assist attacks from Padme's kick, GK's flip, and 3PO's special (when not used with Vader) to times when the Captain is already close to full turn meter.
                            <li>Target the side Guards with basics when there are no stacks of courage to convert to true damage, to prevent feeding more turn meter to the Captain.
                            <li>Yoda's flip attack is a good way to fit in a few more hits when the Captain is about to take a turn. Ahsoka will assist, and Yoda's 100% turn meter gain will take priority over the Captain's turn meter, so you get to use another special and get another Ahsoka hit for free.
                            </ul>
                            
                            <p>
                            <b>Mods:</b> Typical Padme team mods optimize for Health, since Protection Up buffs scale with Max Health instead of Max Protection. Any Protection Up that survives damage gets converted to Courage. However, since this phase has attacks that deal % Max Health damage, you want to avoid adding Health from mods. You'll want to at least change GK's mods, so that he does not immediately die. The rest of the team can be OK with just focusing on Speed and Offense.
                            <p>
                            If using outside of P1: Mod strategy changes, favoring Health over Protection again. Only the P1 boss does % Max Health damage on attacks, the Rancor instead reduces Max Protection by 50%, making protection quickly useless.
                            
                            <p>
                            Here is a variant that uses an alternate lineup with Wrecker and C-3PO to keep GK and Ahsoka for later on - it scores 2.7%: <a href="https://www.youtube.com/watch?v=dhvbpgtuqSY">video</a>
                            """,
                        videoURL: "https://www.youtube.com/embed/Ob3AXEz7i7A"),

                    ChallengeRancor.TeamInfo(
                        id: "SEE1", name: "SEE P1", marker: "&#11088;", order: 98,
                        body:
                            """
                            Thanks to jraenar#4239 (Porgs)!

                            <p>
                            Team: SEE, Malak, 3 Sith

                            <p>
                            <b>How it works:</b> SEE lead will dispel all debuffs and heal to full other Sith allies when a Sith ally is defeated. That lets Malak tank out all of the hits, then heal back to full as each other ally eventually gets defeated by splash damage. The team gets reset with one fewer ally each time, so a lot of hits on the Captain can rack up a lot of damage. NOTE: make sure to run this team with at least one of the side guards active at the start; otherwise, there's nobody to use SEE's Link ability and get Malak taunting.
                            <p>
                            <b>Strategy:</b> It can take a few tries to get an optimal run. Back out and try again if more than one Sith gets defeated, or if you lose SEE or Malak before they are the last two standing. If SEE has unlocked ultimate, do not use it immediately on full charge; you get more damage from the mass assist special until you are down to only two or fewer Sith allies.
                            <p>
                            <b>Mods:</b> Typically there will be little need to change mods for this team. Malak should already be modded with Protection and Defense primaries, with Defense and Health sets. All others should be OK with normal mods. An optimized set of mods would avoid Health and prefer Protection, and prioritize Offense and Speed.
                            
                            <p>
                            <b>Other Examples:</b>
                            <ul>
                            <li><a href="https://www.youtube.com/watch?v=YVXMsEoZZmY">Run with everyday mods</a>
                            </ul>
                            """,
                        videoURL: "https://www.youtube.com/embed/4yhkm-CibcM"),
                    
                    ChallengeRancor.TeamInfo(
                        id: "SLKR-WAT1", name: "SLKR + Wat P1",
                        videoURL: "https://www.youtube.com/watch?v=4bko-AMKdco"),

                    ChallengeRancor.TeamInfo(
                        id: "DRMALAK1", name: "DR + Malak P1",
                        videoURL: "https://www.youtube.com/watch?v=LJZJmeq0Tfg"),

                ],
                customPlayOrder: { player, evs in
                    var result = [String]()

                    let evs = evs.filter { $0.status.isViable }
                    let squads = Dictionary(uniqueKeysWithValues: evs.map { ($0.squad.properties.attackerId ?? $0.squad.name, $0) })
                    
                    // SEE can really only fit in P1, so try to use him here
                    if let ev = squads["SEE1"] {
                        result.append(ev.squad.name)
                    }
                    
                    // if they have the P2 shaak team, prefer padme over it
                    if let vader = squads["VADER1"], let padme = squads["PADME1"] {
                        if vader.squad.units.contains(where: { $0.name == "Shaak Ti" }) {
                            result.append(padme.squad.name)
                        }
                    }
                    
                    return result
                }
            ),
            
            // MARK: - P2
            ChallengeRancor.PhaseInfo(
                id: "p2", name: "P2", squads: ChallengeRancor.p23,
                intro:
                    """
                    Prefer these teams for phase 2 (marked with a &#11088; in the table).  Note that the first team makes use of the Devour to trigger Fives and the other teams may need a sacrifice character to eat the devour.
                    
                    <p>
                    The door/topple triggers once per phase -- the best scores require the use of the door.  Plan accordingly.
                    
                    <ul>
                    <li>Shaak, ARC, 501st Echo, Rex, Fives - (depends on Devour being available) 5-6%
                    <li>Veers, Piett, Gideon, Range, Dark (sub Starck for one of last three if needed) - 4-7%
                    <li>Vader, Nute, Thrawn, Traya, Wat - requires special modding, can do around 12%
                    </ul>
                    
                    <p>Note that the teams for P2 and P3 are the same but different teams are recommended.
                    """,
                teams: [
                    BRG.rancorShaak(prefer: true),
                    BRG.rancorSLKRNS(prefer: false),
                    BRG.rancorSLKR(prefer: false),
                    ChallengeRancor.TeamInfo(
                        id: "TROOPERS23", name: "Imperial Troopers P2", marker: "&#11088;", order: 99,
                        videoURL: "https://www.youtube.com/watch?v=KEFUkww8oxo"),
                    ChallengeRancor.TeamInfo(
                        id: "VADER-NUTE2", name: "Vader/Nute P2", marker: "&#11088;", order: 98,
                        videoURL: "https://www.youtube.com/watch?v=LHYzr39r7EE"),

                ]
            ),
            
            // MARK: - P3
            ChallengeRancor.PhaseInfo(
                id: "p3", name: "P3", squads: ChallengeRancor.p23,
                intro:
                    """
                    Prefer these teams for phase 3 (marked with a &#11088; in the table).
                    
                    <p>
                    The door/topple triggers once per phase -- the best scores require the use of the door.  Plan accordingly.
                    
                    <ul>
                    <li>SLKR, Daka, Zombie, Thrawn, Hux - 10-12%
                    <li>SLKR, FOST, Red, Thrawn, Hux - 8-10%
                    <li>Veers, Piett, Gideon, Range, Dark (sub Starck for one of last three if needed) - 4-7%
                    </ul>
                    """,
                teams: [
                    BRG.rancorShaak(prefer: false),
                    BRG.rancorSLKRNS(prefer: true),
                    BRG.rancorSLKR(prefer: true),
                    ChallengeRancor.TeamInfo(
                        id: "TROOPERS23", name: "Imperial Troopers P2", marker: "&#11088;", order: 97,
                        videoURL: "https://www.youtube.com/watch?v=KEFUkww8oxo"),
                    ChallengeRancor.TeamInfo(
                        id: "VADER-NUTE2", name: "Vader/Nute P2",
                        videoURL: "https://www.youtube.com/watch?v=LHYzr39r7EE"),
                ]
            ),
            
            // MARK: - P4
            ChallengeRancor.PhaseInfo(
                id: "p4", name: "P4", squads: ChallengeRancor.p4,
                intro:
                    """
                    Prefer these teams for phase 4 (marked with a &#11088; in the table).
                    
                    <ul>
                    <li>Rey, Jawa Scav, Armorer, Jawas (can do 4 Jawas) - not sure on typical/max damage
                    <li>JKR, Rey, 3x Jedi or Resistance Bros - 5-10% depending on comp, relics, mods
                    <li>JKL, JML, 3x Jedi - 4-8% depending on comp, relics, mods
                    <li>CLS, Han, Chewie, Chewpio, R2 - 1-2%
                    <li>GG, B1, B2, Deka, Magna (sub any other droid except GG) - 1-2%
                    <li>JMK, CAT, GK, Ahsoka, GAS - ask @cherno (Porgs GL)
                    <li>Dooku, Jango, (any seps you feel like, none necessary) - 1%
                    </ul>
                    
                    This is the last phase -- hit it with everything you have!
                    """,
                teams: [
                    ChallengeRancor.TeamInfo(
                        id: "JMK4", name: "JMK P4", marker: "&#11088;", order: 101,
                            videoURL: "https://www.youtube.com/watch?v=UnAlS6oLQK0"),
                    ChallengeRancor.TeamInfo(
                        id: "REYJAWAS4", name: "Rey + Jawas P4", marker: "&#11088;", order: 100),
                    ChallengeRancor.TeamInfo(
                        id: "JKRREY4", name: "JKR + Rey P4", marker: "&#11088;", order: 99),
                    ChallengeRancor.TeamInfo(
                        id: "JKLJMLS4", name: "JKL + JMLS P4", marker: "&#11088;", order: 98),
                    ChallengeRancor.TeamInfo(
                        id: "REBELS4", name: "Rebels P4", marker: "&#11088;", order: 97,
                        videoURL: "https://www.youtube.com/watch?v=Ru3B6xiPeig"),
                    ChallengeRancor.TeamInfo(
                        id: "GG4", name: "Rebels P4", marker: "&#11088;", order: 96,
                        videoURL: "https://www.youtube.com/watch?v=BuC3_he3BQE"),
                    ChallengeRancor.TeamInfo(
                        id: "SEPS4", name: "Rebels P4", videoURL: "https://www.youtube.com/watch?v=IjZNkeiy2-8"),
                ]
            ),
        ]
    )
    
    static func rancorShaak(prefer: Bool) -> ChallengeRancor.TeamInfo {
        return ChallengeRancor.TeamInfo(
            id: "SHAAK23", name: "Shaak Ti & Clones P2 (preferred) or P3", marker: prefer ? "&#11088;" : "", order: prefer ? 100 : nil,
            body:
                """
                Thanks to jraenar#4239 (Porgs)!

                <p>
                Team: Shaak, ARC, 501st Echo, Rex, Fives
                
                <p>
                <b>NOTE: this requires the Devour to be used to trigger Fives.</b>
                
                <p>
                This team can be used on P2 or P3 but P2 is preferred.

                <p>
                <b>How it works:</b> This team intentionally lets Randy eat someone other than Shaak Ti or Fives, so that Fives gives stats to the other clones. The extra boost of speed lets Rex get out two Aerial Advantage attacks, which accounts for the bulk of the team's damage.
                
                <p>
                <b>Strategy:</b> Use only basic attacks until Randy eats someone. If he chomps Shaak Ti or Fives (i.e. no Fives sacrifice gets triggered), retreat and try again. Delay dropping the door as long as possible, to give time for Rex to use first Aerial Advantage.
                <ul>
                <li>Shaak Ti can basic the door panel once without dropping; doing this as her first move prevents feeding TM to Randy and makes it more likely to get first Aerial Advantage before door drop.
                <li>Use Shaak Ti Assault Team special only in topple, to prevent feeding turn meter. Target Rex, to give Speed Up buff, and ensure getting to second Aerial Advantage.
                <li>Use Shaak Ti Training Exercises special whenever needed to heal a clone in yellow health.
                <li>Save Echo EMP Grenade to drop the door. After, use it whenever available, as it does more damage than his basic.
                </ul>
                
                <p>
                <b>Mods:</b> Typical mods for this team should be OK, but optimizing may be needed if your team has trouble getting to the second Aerial Advantage and getting to the 5% mark. Priorities are in order listed below:
                <ul>
                <li>Fives optimally is as fast as you can manage; Speed and Health sets, Speed arrow, Offense triangle and cross, Health circle. Tiebreaker secondaries for roughly equivalent speed mods are flat and percent Offense, Health, and Protection, in that order.
                <li>Rex gets your absolute fastest speed secondaries. Use Speed and Health sets, and additional Health secondaries ensure he survives long enough to get second special out.
                <li>Shaak Ti should focus Health if R5, and can afford to focus Speed if R7+.
                <li>Echo and ARC Trooper both go all out on Offense. The higher damage they have, the closer this team can get to 6% on the phase.
                </ul>
                """,
            videoURL: "https://www.youtube.com/embed/qH8dyoVF2o0")
    }
    
    static func rancorSLKRNS(prefer: Bool) -> ChallengeRancor.TeamInfo {
        return ChallengeRancor.TeamInfo(
            id: "SLKRNS23", name: "SLKR + NS P2 or P3 (preferred)", marker: prefer ? "&#11088;" : "", order: prefer ? 100 : nil,
            body:
                """
                Thanks to jraenar#4239 (Porgs)!

                <p>
                Team: SLKR, Daka, Zombie, Thrawn, Hux
                                
                <p>
                This team can be used on P2 or P3 but P3 is preferred.

                <p>
                <b>How it works:</b> Revive mechanics from Old Daka and Nightsister Zombie helps avoid needing to send in a sacrifice R5, and delays long enough for SLKR to build up big stacks of Siphon before ending with an ultimate stance and some last big hits. Multiple hits from Old Daka and Thrawn help build up ultimate charge. If you do not have the ultimate unlocked, this team will do significantly less damage.
                
                <p>
                <b>Strategy:</b> Use SLKR Stasis Strike ("poke") attack often, to get more stacks of Siphon built up, and to get higher Mastery from Furious Onslaught later in the battle.
                <ul>
                <li>If both Hux and Thrawn are fast enough, you can open the battle with two SLKR pokes, then use the AoE to drop the door. Exact speeds needed are not yet known, but it should work with Hux at 329+ and Thrawn at 328+ speed. If you can get it with lower speeds, please report so others can have an exact target to reach.
                <li>Do NOT use the SLKR ultimate with less than 100% charge. Doing so will limit the total amount of Mastery gained from Siphon, and result in much lower overall damage.
                <li>The first ultimate stance should do two pokes and an AoE, instead of all AoE. This will reduce the total Mastery gained from the first ultimate stance, but it will greatly scale up Siphon and result in higher overall Mastery by the end of battle.
                <li>The second ultimate stance should do poke, AoE, basic. This maximizes Mastery gained, and the basic attack does the highest damage so the battle ends on the biggest possible hit.
                <li>This team is set up the best for bridging the end of P1 into P2, but the coordination and multiple devices required to do so may not be feasible. All teams should go in with around 5% or less remaining in P1, and nobody should post until all report in as getting to second ultimate stance.
                </ul>
                
                <p>
                <b>Mods:</b> Typical mods are probably OK. Some suggested targets are listed below, if you have success with lower stats, please report to us so others developing this team have a better idea of the minimum stats needed.
                <ul>
                <li>SLKR wants speed, then offense. Use a Speed set, to ensure that he can get to the second ultimate stance, and add a Crit Damage triangle to boost his damage output. Target around 570 Speed.
                <li>Hux should be as fast as possible, and preferably at least one speed faster than Thrawn. Target 329 Speed or better.
                <li>Thrawn should be as fast as possible, and preferably at least one speed slower than Hux. Target 328 Speed or better.
                <li>Old Daka should be stacked with health, while still being reasonably fast. Aim for over 120k Health and 280 Speed.
                <li>Zombie is similar to Old Daka, stack with health while having some speed. Aim for over 100k Health and 240 Speed.
                </ul>
                """,
            videoURL: "https://www.youtube.com/embed/dgOyxqKo3Pg")
    }

    static func rancorSLKR(prefer: Bool) -> ChallengeRancor.TeamInfo {
        return ChallengeRancor.TeamInfo(
            id: "SLKR23", name: "SLKR + FO P2 or P3 (preferred)", marker: prefer ? "&#11088;" : "", order: prefer ? 99 : nil,
            body:
                """
                Thanks to jraenar#4239 (Porgs)!

                <p>
                Team: SLKR, FOST, Red, Thrawn, Hux
                                
                <p>
                This team can be used on P2 or P3 but P3 is preferred.

                <p>
                <b>How it works:</b> This team needs to have Devour on cooldown, so is better used in P3 if possible. To use in P2, send in a sacrifice R5 character – traditionally, Rose Tico, but anyone you won't be using in another team is fine. The team is there to support SLKR in getting big stacks of Siphon and then ending the battle with big hits from having much higher Mastery.
                <p>
                <b>Strategy:</b> The strategy for this team is essentially the same as for the Nightsisters variant of the team. Without the additional survivability and revives coming from the Nightsisters, this team has a bit more trouble generating the ultimate charge for the second ultimate stance. Using the First Order and Sith Troopers specifically help to generate more critical hits to build up ultimate charge, from all their assists.
                <p>
                <b>Mods:</b> The mods for this team are the same as for the Nightsisters variant on SLKR, Hux and Thrawn. There are no specific recommendations on targets for the other First Order characters. Please report any health or speed thresholds that seem to make a difference on getting to higher scores.
                """,
            videoURL: "https://www.youtube.com/embed/CXPNf_9iLxY")
    }


}
