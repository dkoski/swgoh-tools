//
//  Copyright © 2021 David Koski. All rights reserved.
//

import SWGOH

protocol AllianceBranding {
    
    var zetaBlurb: String { get }
    var farmBlurb: String { get }

    func backgroundImageCss(_ html: HTML) -> String
    
    func playerHeader(_ html: HTML, guild: Model.Guild, player: Model.Player, title: String, navigation: String) -> String
    
    func header(_ html: HTML, title: String, nav: String) -> String
    
    var alliancePages: [AllianceSite.GuildBuilder] { get }
    
    var challengeRancorInfo: ChallengeRancor.Info { get }
    
    var baseURL: URL? { get }
}

extension AllianceBranding {
    var alliancePages: [AllianceSite.GuildBuilder] { [] }
    
    var challengeRancorInfo: ChallengeRancor.Info { ChallengeRancor.Info() }
    
    var baseURL: URL? { nil }
}

struct Unbranded : AllianceBranding {
    
    let zetaBlurb = "The first one is popularity amongst players."

    let farmBlurb = "These are farming recommendations based on the popularity of toons among players and are shown in order of popularity."

    func backgroundImageCss(_ html: HTML) -> String {
        return ""
    }
    
    func playerHeader(_ html: HTML, guild: Model.Guild, player: Model.Player, title: String, navigation: String) -> String {
        return
            """
            <title>\(guild.name)\(title.isEmpty ? "" : ": " + title)</title>
            <div style="display: flex; align-items:baseline"><h1>\(guild.name)\(title.isEmpty ? "" : ": " + title)</h1></div>
            
            <div style="background-color: #f0f0f0; border: solid red; padding: 10px; border-radius: 10px">
            \(navigation)
            <a href="\(html.alliance("index.html"))">Alliance</a> |
            <a href="\(html.g(guild, "index.html"))">\(guild.name)</a> |
            <a href="\(html.p(player, "info"))">Info</a> |
            <a href="\(html.g(guild, "mods-player.html"))?id=\(player.stats.allyCode)">Mods</a> |
            <a href="\(html.p(player, "tw-squads"))">TW+GA Squads</a> |
            <a href="\(html.p(player, "counters"))">Counters</a> |
            <a href="\(html.p(player, "target"))">Targets</a> |
            <a href="\(html.p(player, "unlock"))">Unlocks</a> |
            <a href="\(html.p(player, "zeta"))">Zetas</a> |
            <a href="\(html.p(player, "farm"))">Farming</a> |
            <a href="\(html.p(player, "dsgeotb"))">DS Geo TB</a>
            <a href="\(html.p(player, "lsgeotb"))">LS Geo TB</a>
            </div>
            <br/>
            """
    }
    
    func header(_ html: HTML, title: String, nav: String) -> String {
        return
            """
            <title>Guild Reports\(title.isEmpty ? "" : ": " + title)</title>
            <div style="display: flex; align-items:baseline"><h1>Guild Reports\(title.isEmpty ? "" : ": " + title)</h1></div>
            
            <div style="background-color: #f0f0f0; border: solid red; padding: 10px; border-radius: 10px">
            <a href="\(html.alliance("index.html"))">Alliance</a> |
            \(nav)
            </div>
            <br/>
            """
    }

}
