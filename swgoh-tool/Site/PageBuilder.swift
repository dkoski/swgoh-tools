//
//  PageBuilder.swift
//  swgoh
//
//  Created by David Koski on 9/8/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

protocol PageBuilder {

    var path: String { get }
    
    func contents(html: HTML) -> String
    
}

extension PageBuilder {
    
    func build(base: URL) throws {
        let html = HTML(baseURL: base, pageURL: base.appendingPathComponent(path))
        let destination = base.appendingPathComponent(path)
        try FileManager().createDirectory(at: destination.deletingLastPathComponent(), withIntermediateDirectories: true)
        
        var contents = self.contents(html: html)
        if !contents.hasSuffix("\n") {
            contents += "\n"
        }
        
        try contents.data(using: .utf8)!.write(to: destination)
    }
    
}

struct StringPageBuilder : PageBuilder {
    
    let path: String
    let string: String
    
    func contents(html: HTML) -> String {
        return string
    }
}

var defaultBrand: AllianceBranding = Unbranded()

extension URL {

    func pathRelative(to target: URL) -> String {
        var pathComponents = self.absoluteURL.pathComponents
        var targetPathComponents = target.absoluteURL.pathComponents

        let targetFileName = targetPathComponents.removeLast()
        pathComponents.removeLast()

        // remove the common path
        while !pathComponents.isEmpty && !targetPathComponents.isEmpty && pathComponents[0] == targetPathComponents[0] {
            pathComponents.removeFirst()
            targetPathComponents.removeFirst()
        }

        var result = [String]()

        for _ in 0 ..< pathComponents.count {
            result.append("..")
        }

        result.append(contentsOf: targetPathComponents)
        result.append(targetFileName)

        return result.joined(separator: "/")
    }

}

struct HTML {
    
    let baseURL: URL
    let pageURL: URL
    
    let branding: AllianceBranding
    
    init(baseURL: URL, pageURL: URL, branding: AllianceBranding = defaultBrand) {
        self.baseURL = baseURL
        self.pageURL = pageURL
        self.branding = branding
    }
    
    let head =
        """
        <meta charset="UTF-8">
        <meta name="robots" content="noindex">
        """
    
    var backgroundImageCss: String {
        return branding.backgroundImageCss(self)
    }
    
    var toonCss: String {
        """
        <link rel="stylesheet" type="text/css" href="\(url("toons.css"))">
        <style type="text/css">
        .toT { text-align:center; font-size: 60%; }
        .toI {
            width: 65px;
            height: 65px;
            border-radius: 50%;
            margin: 0 2.5px;
        }
        .toIs {
            width: 25px;
            height: 25px;
            border-radius: 50%;
            margin: 0 2.5px;
        }
        .opponent {
            border: 2px solid grey;
        }
        .hardCounter {
            border: 2px solid red;
        }
        .softCounter {
            border: 2px solid blue;
        }
        .squad {
            border: 2px solid black;
        }
        .verbage {
            font-size: 70%;
        }
        </style>
        """
    }
    
    let alternatingRowsCss =
        """
        <style type="text/css">

        table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
        }

        td, th {
          border: 1px solid #dddddd;
          padding: 8px;
        }

        tr:nth-child(even) {background: #dddddd}
        tr:nth-child(odd) {background: #ffffff}

        .toggleable {
            display: none;
        }
        </style>
        """

    let frozenHeadersCss =
        """
        <style type="text/css">
        table {
          border-collapse: separate;
          border-spacing: 0;
          border-top: 1px solid gray;
        }

        td, th {
          margin: 0;
          border: 1px solid grey;
          white-space: nowrap;
          border-top-width: 0px;
        }

        .frozenColumns {
          width: 83%;
          overflow-x: scroll;
          margin-left: 16%;
          overflow-y: visible;
          padding: 0;
        }

        .frozenColumn {
          position: absolute;
          left: 0;
          top: auto;
          width: 15%;
          border-top-width: 1px;
          /*only relevant for first row*/
          margin-top: -1px;
          /*compensate for top border*/
        }
        </style>
        """
    
    let jqueryInclude =
        """
        <script type="text/javascript" language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        """
    
    /// https://datatables.net/
    let sortableTableCSS =
        """
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.5/css/fixedHeader.dataTables.min.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.dataTables.min.css" />
        <script type="text/javascript" language="javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
        """
    
    func chartInclude() -> String {
        return
            """
            <script src="https://d3js.org/d3.v5.min.js"></script>
            <script src="\(url("j/graph.js"))"></script>
            """
    }
    
    func plotlyInclude() -> String {
        """
        <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        """
    }
    
    func dataTable(id: String, ordering: Bool = true, order: String? = nil, fixedHeader: Bool = false, fixedColumns: Int = 0, search: Bool = true) -> String {
        return
            """
                <script>
                $(document).ready( function () {
                    $('#\(id)').DataTable({
                    "paging":   false,
                    "info":     false,
                    \(ordering ? "" : "ordering: false,")
                    \(order == nil ? "" : "order : [\(order!)],")
                    \(fixedHeader ? "fixedHeader: true," : "")
                    \(fixedColumns > 0 ? "fixedColumns: { leftColumns: \(fixedColumns) }, scrollX: true," : "")
                    \(search ? "" : "bFilter: false,")
                    });
                } );
                </script>
            """
    }
    
    func playerHeader(guild: Model.Guild, player: Model.Player, title: String = "", navigation: String = "") -> String {
        return branding.playerHeader(self, guild: guild, player: player, title: title, navigation: navigation)
    }
    
    func header(title: String = "", nav: String = "<a href=\"index.html\">Back</a>") -> String {
        return branding.header(self, title: title, nav: nav)
    }
    
    // <div style="float:right;">Questions: @gorgatron#3094</div>


    func footer(removeLastPathComponent: Bool = false) -> String {
        // previously did this:: <div style="position: fixed; bottom: 0px">
        var result = ""
        
        if removeLastPathComponent {
            result +=
                """
                <script>
                    window.goatcounter = { path: function(p) { return p.replace(/[^/]+\\.html$/, '') } }
                </script>
                """
        }
        
        result +=
            """
            <br/>
            <font size="-2">Generated \(Date().description)</font>
            <script data-goatcounter="https://gorgatron.goatcounter.com/count" async src="//gc.zgo.at/count.js"></script>
            """
        return result
    }
    
    func i(_ name: String) -> String {
        return url("i/" + name)
    }
    
    func p(_ player: Model.Player, _ name: String) -> String {
        return url("player/\(name)/\(player.code).html")
    }
    
    func g(_ guild: Model.Guild, _ name: String) -> String {
        return url("guild/\(guild.shortName)/\(name)")
    }
    
    func alliance(_ name: String) -> String {
        return url("alliance/\(name)")
    }
    
    func help(_ name: String) -> String {
        return url("help/\(name).html")
    }
    
    func url(_ path: String) -> String {
        if path.starts(with: "http") {
            return path
        } else {
            return pageURL.pathRelative(to: baseURL.appendingPathComponent(path))
        }
    }
    
    func resources() -> String {
        url("/r")
    }
    
    func character(_ character: Model.Character) -> String {
        return url("c/\(character.id).html")
    }
}

