//
//  Copyright © 2021 David Koski. All rights reserved.
//

import Foundation

import SWGOH

private let allyCodes = [
    // tin foil, xoar
    151953838,
    795554531,
    673976751,
    569694924,
    
    // aqua related
    893687125,
    445419677,
    573692553,
]

private struct PersonOfInterestFile : Codable {
    
    struct PersonOfInterest : Codable {
        var name: String
        var allyCode: Int
        
        struct History : Codable {
            var date: Date
            var guild: String
            var gp: Int
            var name: String
        }
        
        var history: [History]
    }

    var players: [PersonOfInterest]
    
    static func read(from url: URL) throws -> PersonOfInterestFile {
        let data: Data
        do {
            data = try Data(contentsOf: url)
        } catch {
            return PersonOfInterestFile(players: [])
        }
        let decoder = JSONDecoder()
        return try decoder.decode(PersonOfInterestFile.self, from: data)
    }
    
    func write(to url: URL) throws {
        let encoder = JSONEncoder()
        let data = try encoder.encode(self)
        try data.write(to: url, options: [.atomic])
    }
}

struct PersonOfInterestPage : PageBuilder {
    
    var path: String { return "alliance/poi.html" }
    
    func contents(html: HTML) -> String {
        var result = ""
        
        let poiURL = html.baseURL.appendingPathComponent("alliance/poi.json")
        var poi = try! PersonOfInterestFile.read(from: poiURL)
        
        let d = Downloader(user: "dkoski", password: "wazzok-7cyrco-zatwYq")
        let guilds = d.getGuildNames(allyCodes: Array(allyCodes))
        
        var updated = false
        for allyCode in allyCodes {
            if let player = guilds[allyCode] {
                if let personIndex = poi.players.firstIndex(where: { $0.allyCode == allyCode }) {
                    if poi.players[personIndex].history.last!.guild != player.guild {
                        let name: String
                        if player.guild == "No Guild" {
                            name = poi.players[personIndex].name
                        } else {
                            name = player.name
                        }
                        poi.players[personIndex].history.append(PersonOfInterestFile.PersonOfInterest.History(date: Date(), guild: player.guild, gp: player.gp, name: name))
                        updated = true
                    }
                } else {
                    // new person
                    let person = PersonOfInterestFile.PersonOfInterest(name: player.name, allyCode: player.allyCode, history: [PersonOfInterestFile.PersonOfInterest.History(date: Date(), guild: player.guild, gp: player.gp, name: player.name)])
                    poi.players.append(person)
                    updated = true
                }
            }
        }
        
        if updated {
            try! poi.write(to: poiURL)
        }
                
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += html.dataTable(id: "data", order: #"[2, "asc"]"#, fixedHeader: true)

        result += html.header(title: "Alliance POI History", nav: AllianceSite.navigation(html).render())
        
        let sortableFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy/MM/dd"
            return formatter
        }()
        
        let printableFormatter: DateFormatter = {
            let formatter = DateFormatter()
            formatter.dateFormat = "MM/dd/yyyy"
            return formatter
        }()
                        
        result +=
            H.table().id("data") {
                H.thead {
                    H.tr {
                        H.th { "Date" }
                        H.th { "Name" }
                        H.th { "Ally Code" }
                        H.th { "Guild" }
                        H.th { "GP" }
                    }
                }
                H.foreach(poi.players) { player in
                    H.foreach(player.history) { history in
                        H.tr {
                            H.td().order(sortableFormatter.string(from: history.date)) {
                                printableFormatter.string(from: history.date)
                            }
                            H.td() {
                                history.name
                            }
                            H.td { H.a(href: "https://swgoh.gg/p/\(player.allyCode)/") { player.allyCode } }
                            H.td() {
                                history.guild
                            }
                            H.td() {
                                history.gp
                            }
                        }
                    }
                }
            }.render()

        result += html.footer()

        return result
    }

}
