//
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

/// a report suitable for producing assignments -- use this with --filter-viable and the default sort order
struct HTMLMultiAssignmentReport : PageBuilder {
    
    var headers = [String]()
    var reportByPlayer = [Model.Player:[String]]()
    var colors = [Model.Player:[String?]]()
    var sort = [Model.Player:[Int?]]()
    var countByHeader = [String:Int]()

    let name: String
    let title: String
    let guild: Model.Guild
    
    var augmentHeader: () -> String = { return "" }
    var augmentFooter: () -> String = { return "" }
    var augmentPlayer: (Model.Player) -> String = { _ in return "" }

    var path: String { return "guild/\(guild.shortName)/\(name).html" }
    
    var message = ""
    
    var partTwo: (HTML, Model.Guild) -> String = { _, _ in return "" }
    
    mutating func add(header:String, data: [Model.Player:[SquadEvaluation]]) {
        
        let columns = data.map { $0.value.count }.max()!
        
        for _ in 0 ..< columns {
            headers.append(header)
        }
        
        for (player, evaluations) in data {
            for squadEv in evaluations {
                var result = ""
                
                result += "(\(squadEv.squad.name)) "
                result += squadEv.squad.description(images: false, zetas: true, gearBelow: 100, starsBelow: 7)

                reportByPlayer[player, default: []].append(result)
                
                sort[player, default: []].append(squadEv.squad.gp)

                if squadEv.status <= .needsFarming {
                    colors[player, default: []].append("red")
                    
                } else if squadEv.status <= .needsRequiredGear {
                    colors[player, default: []].append("yellow")
                    
                } else {
                    colors[player, default: []].append(nil)
                    countByHeader[header, default: 0] += 1
                }

            }
            
            for _ in 0 ..< (columns - evaluations.count) {
                reportByPlayer[player, default: []].append("")
                colors[player, default: []].append(nil)
                sort[player, default: []].append(nil)
            }
        }
    }
        
    func contents(html: HTML) -> String {
        var result = ""
                
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        if headers.count < 5 {
            result += html.dataTable(id: "schedule")
        } else {
            result += html.dataTable(id: "schedule", fixedHeader: false, fixedColumns: 1)
        }

        result += html.header(title: "\(title) Schedule")
        
        var table = ""
        
        table +=
            """
            \(message)
            Note: some teams may overlap.
            <br/>
            <br/>
            <button onclick="$('.not-viable').text('&nbsp;').css('background-color', '')">remove not ready</button><br/>


            <table id="schedule" class="display" style="font-size: 75%">
                <thead>
                    <tr>
                    <th>Player</th>
                    <th>Team Count</th>
            """
        
        table += augmentHeader()
        
        for header in headers {
            table += "<th>"
            table += header
            table += "</th>"
        }

        table +=
            """
                    </tr>
                </thead>
                <tbody>
            """
        
        var sumTeams = 0

        for player in reportByPlayer.keys.sorted() {
            table += "<tr><td nowrap>"
            table += player.name
            table += "</td>"
            
            table += "<td>"
            // count the number of non duplicate teams (by description) that are viable
            let count = Set(zip(reportByPlayer[player, default: []], colors[player, default: []]).filter { $0.1 == nil && !$0.0.isEmpty }.map { $0.0 }).count
            table += count.description
            table += "</td>"
            sumTeams += count
            
            table += augmentPlayer(player)

            for ((item, color), sort) in zip(zip(reportByPlayer[player, default: []], colors[player, default:[]]), sort[player, default: []]) {
                let sort = sort ?? 0
                if let color = color {
                    table += "<td nowrap data-order=\"\(sort)\" class=\"not-viable\" style=\"background-color: \(color);\">"
                } else {
                    table += "<td nowrap data-order=\"\(sort)\">"
                }
                table += item.isEmpty ? "&nbsp;" : item
                table += "</td>"
            }
            table += "</tr>\n"
        }
        
        let footer = augmentFooter()
        table += "<tfoot><tr>"
        table += "<td></td>"
        table += "<td>\(sumTeams)</td>"
        table += footer
        var lastHeader: String? = nil
        for header in headers {
            if header != lastHeader {
                table += "<td>\(countByHeader[header, default: 0])</td>"
                lastHeader = header
            } else {
                table += "<td></td>"
            }
        }
        table += "</tr></tfoot>\n"

        table +=
            """
                </tbody>
            </table>
            """
        
        result += table
        
        result += partTwo(html, guild)
        
        result += html.footer()

        return result
    }
        
    mutating func buildHSTR(players: [Model.Player]) {
        add(header: "P1", data: evaluatePlayers(players: players, squadBuilder: JTR.squadBuilder, squadEvaluator: JTR.squadEvaluator))
        add(header: "P2", data: evaluatePlayers(players: players, squadBuilder: HSTRP2.configurations["all"]!))
        add(header: "P3", data: evaluatePlayers(players: players, squadBuilder: MultipleSquadBuilder(HSTRP3.chexMixBuilder, HSTRP3.roloBuilder, HSTRP3.bhBuilder), squadEvaluator: HSTRP3.chexMixSquadEvaluator))
        add(header: "P3", data: evaluatePlayers(players: players, squadBuilder: MultipleSquadBuilder(NS.deathStormBuilder, NS.deathStorm2Builder), squadEvaluator: NS.deathStormEvaluator))
        add(header: "P4", data: evaluatePlayers(players: players, squadBuilder: NS.p4NSBuilder, squadEvaluator: NS.p4NSEvaluator))
        add(header: "P4", data: evaluatePlayers(players: players, squadBuilder: TW.foSquadBuilder))
    }

    mutating func buildHAAT(players: [Model.Player]) {
        add(header: "P1", data: evaluatePlayers(players: players, squadBuilder: HAAT.p1))
        add(header: "P2+", data: evaluatePlayers(players: players, squadBuilder: HAAT.p2))
        
        message =
        """
        <p>
        Information about the teams: <a href="https://gaming-fans.com/star-wars-goh/swgoh-guides/aat-raid-guide/aat-phase-1-teams-strategy/">gaming fans</a>
        <br/>
        You may need to do some research on how to play these teams, especially the Ackbar/NS teams
        have particular ways that you use them.
        <br/>
        """
        
        partTwo = { html, guild in
            let toons = [ HAAT.p1.asSquadRules(), HAAT.p2.asSquadRules() ]
                .flatMap { $0 }
                .map { $0.rules.keys }
                .flatMap { $0 }
                .map { Model.Character.find($0).commonName }

            return
                """
                <br/>
                <br/>
                Here are all the toons used across all the potential teams:
                <br/>
                <br/>
                """ +
                ImportantToonsPage.formatTable(html: html, guild: guild, toons: Set(toons).sorted(), countIf: { $0.relic >= (5 + 2) })
        }
    }

}

struct RareUnitReportHTML : PageBuilder {
    let skipCharacters = Set([ "ROLO" ])
    let limit = 10
    
    var path: String { return "guild/\(guild.shortName)/rare-units.html" }
    
    let guild: Model.Guild
        
    struct RarityRow : Comparable {
                
        var counts = [Int](repeating: 0, count: 7)
        var players = [[(Model.Player,Int)]](repeating: [], count: 7)
        
        mutating func record(player:Model.Player, unit: Model.Unit) {
            for i in 0 ..< unit.stars {
                counts[i] += 1
                players[i].append((player, unit.gp))
            }
        }
        
        static func < (lhs: RareUnitReportHTML.RarityRow, rhs: RareUnitReportHTML.RarityRow) -> Bool {
            for i in (0 ..< 7).reversed() {
                if lhs.counts[i] < rhs.counts[i] {
                    return true
                } else if lhs.counts[i] > rhs.counts[i] {
                    return false
                }
            }
            return false
        }
        
        static func == (lhs: RareUnitReportHTML.RarityRow, rhs: RareUnitReportHTML.RarityRow) -> Bool {
            if lhs.counts == rhs.counts {
                return true
            }
            return false
        }

    }
    
    /// return a dictionary of character -> RarityRow (histogram by star)
    func tabulate(players: [Model.Player], alignment: Model.Alignment, ships: Bool, skipCharacters: Set<String>) -> [Model.Character:RarityRow] {
        var result = [Model.Character:RarityRow]()
        
        for player in players {
            for unit in player.units.values {
                let character = Model.Character.find(unit.id)
                
                if skipCharacters.contains(unit.id) || character.role == "Capital Ship" {
                    continue
                }
                if ships != character.ship {
                    continue
                }
                if !ships && character.alignment != alignment {
                    continue
                }

                result[character, default: RarityRow()].record(player:player, unit: unit)
            }
        }
        
        return result
    }
    
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.alternatingRowsCss
        result += html.header(title: "Rare Units by Star Rating")
        
        result += report(players: guild.players, alignment: .light, ships: false)
        result += report(players: guild.players, alignment: .dark, ships: false)
        result += report(players: guild.players, alignment: .dark, ships: true)
        
        result += html.footer()

        return result
    }
    
    func report(players: [Model.Player], alignment: Model.Alignment, ships: Bool) -> String {
        let rareSubReports = 5
        let table = tabulate(players: players, alignment: alignment, ships: ships, skipCharacters: skipCharacters)
        
        var result = ""
        
        result += "<h2>\(ships ? "Ships" : alignment.rawValue + " Characters")</h2>"

        result +=
            """
            <table>
            <tr>
            <td>Character</td><td>2*</td><td>3*</td><td>4*</td><td>5*</td><td>6*</td><td>7*</td>
            </tr>
            """
        
        for tuple in table.sorted(by: { $0.1 < $1.1 }) where tuple.value.counts[6] <= limit {
            
            result += "<tr><td nowrap>"
            result += tuple.key.name
            result += "</td>"
            
            for i in 1 ..< 7 {
                var titleAttribute = ""
                if tuple.value.counts[i] > 0 && tuple.value.counts[i] <= rareSubReports {
                    titleAttribute += " title=\""
                    titleAttribute += tuple.value.players[i].sorted(by: { $0.1 < $1.1 }).map { $0.0.name }.joined(separator: ", ")
                    titleAttribute += "\""
                }
                
                result += "<td align=\"right\"\(titleAttribute)>"
                result += tuple.value.counts[i].description
                result += "</td>"
            }
            
            result += "</tr>\n"
        }
        
        result +=
            """
            </table>
            """

        return result
    }

}
