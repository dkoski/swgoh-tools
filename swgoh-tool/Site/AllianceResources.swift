//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SWGOH

private enum Content {
    case link(String, String)
    case dash(String)
    case comment(String)
    case image(String, String)
    case youtube(String)

    func render() -> HTMLFramgment {
        switch self {
        case let .link(url, title):
            return H.a(href: url).target("resource") { title }
        case let .dash(c):
            return " -- " + c
        case let .comment(c):
            return "<p>" + c
        case let .image(url, title):
            return H.a(href: url).target("resource") {
                H.img(src: url).width("128").style("vertical-align:middle")
                " "
                title
            }
        case let .youtube(id):
            return """
            <iframe width="560" height="315" src="https://www.youtube.com/embed/\(id)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            """
        }
    }
}

private struct Entry {
    let content: [Content]
    let tags: [String]

    init(_ dateAdded: String, _ content: Content, _ tags: String...) {
        self.content = [content]
        self.tags = tags
    }
    
    init(_ dateAdded: String, _ content: Content..., tags: [String]) {
        self.content = content
        self.tags = tags
    }

}

private let database: [Entry] = [
    // This is the original DS Geo TB page
    // Entry("2020-11-15", .link("https://jetkokos.ru", "Squads"), "DS Geo TB"),
    
    Entry("2020-11-17", .link("https://genskaar.github.io/tb_geo/html/ds.html", "Squads"),
          .dash("up-to-date DS Geo TB squads."),
          tags: ["DS Geo TB"]),
    Entry("2020-11-15", .link("https://wiki.swgoh.help/wiki/Geonosis_Separatist_Might", "Geonosis Separtist Might"),
          .dash("Details about the TB and the rewards."),
          tags: ["DS Geo TB"]),

    Entry("2020-11-15", .link("https://genskaar.github.io/tb_geo/html/ls.html", "Squads"),
          .dash("up-to-date LS Geo TB squads."),
          tags: ["LS Geo TB"]),
    Entry("2020-11-15", .link("https://www.reddit.com/r/SWGalaxyOfHeroes/comments/esiyiy/tips_for_completing_missions_with_padme_l_in/", "Using a Padme team"),
          .dash("Very detailed write about using Padme in LS Geo TB."),
          tags: ["LS Geo TB", "padme"]),
    Entry("2020-11-15", .link("https://wiki.swgoh.help/wiki/Geonosis_Republic_Offensive", "Geonosis Republic Offensive"),
          .dash("Details about the TB and the rewards."),
          tags: ["LS Geo TB"]),
    Entry("2020-11-15", .image("https://cdn.discordapp.com/attachments/613961868452691978/648932734487494674/BRG_LS_Geo_TB_Character_Farming_Guide.png", "LS Geo TB Farming"), "LS Geo TB"),
    Entry("2020-11-15", .youtube("QPfu0URQliU"),
          .comment("How to beat fleet missions with no Y-Wing."),
          tags: ["LS Geo TB", "Missions" ]),
    Entry("2021-01-17", .image("https://gaming-fans.com/wp-content/uploads/2019/12/swgoh-kam-infographics_1600.jpg", "KAM Infographic"), tags: ["LS Geo TB", "KAM"]),
    Entry("2021-01-17", .link("https://gaming-fans.com/2019/12/21/swgoh-geonosis-republic-offensive-phase-3-ki-adi-mundi-shard-combat-mission-walkthrough/", "KAM Mission"),
          .dash("talks about gear and tactics."),
          tags: ["LS Geo TB", "KAM"]),
    Entry("2021-04-22", .link("https://docs.google.com/spreadsheets/d/1x2znC9DqLxQ-_ziALzsVn8y4_r6cKVGwKIv8pwty05k/edit#gid=1158483498", "KAM Mission Very Detailed"),
          .dash("flow chart on running the mission."),
          tags: ["LS Geo TB", "KAM"]),
    Entry("2021-04-22", .link("https://docs.google.com/presentation/d/11RxsuevIB9waF6Zb63TpyuzRgDc2Ir7Mnp-6B58aKjk/edit#slide=id.gc6f9544c1_0_0", "KAM Mission (The Dipster Way)"),
          .dash("presentation on how to play it."),
          tags: ["LS Geo TB", "KAM"]),


    Entry("2020-11-15", .link("https://swgoh.gg/characters/data/g13/", "swgoh.gg: G13 Player Data"), "G13", "Relics"),
    Entry("2020-11-15", .link("https://swgoh.gg/characters/data/relics/", "swgoh.gg: Relic Player Data"), "G13", "Relics"),
    Entry("2020-11-15", .link("https://swgoh.gg/characters/relic-comparison/", "swgoh.gg: Relic Comparison"), "Relics"),
    Entry("2020-11-15", .link("https://www.reddit.com/r/SWGalaxyOfHeroes/comments/c51wpy/gear_13_winners_losers_final/?utm_source=amp&utm_medium=&utm_content=post_body", "G13 Wnners/Losers"), "G13"),
    Entry("2020-11-15", .image("https://i.redd.it/rzx6nk4qepw51.png", "Relic Cost Chart"), "Relics"),
    
    Entry("2020-11-15", .link("https://docs.google.com/spreadsheets/d/1GT3vFYVDVt4sPpjAaay-Ujo7NvRlh5e-WaIuQ1HAI0Y/edit#gid=148778672", "SWGoH Relics Scavenger Guide"), "Relics"),
    Entry("2020-11-15", .link("https://docs.google.com/spreadsheets/d/1G7fRR1taw83uBzsh5Ss9LDNqHXplvDK4cHZ6pBQ4jns/edit#gid=1679694163", "Swgoh Gear & Mods"), "Relics"),
    Entry("2020-11-15", .link("https://docs.google.com/spreadsheets/d/1-rcyQoeKFE2YoNeU01-WbExOj9S1jWMwyPWKqCNxvlI/edit#gid=0", "Relics: what to scrap"), "Relics"),
    Entry("2020-11-15", .link("https://docs.google.com/document/d/1dV5PuktqgdLvC-Best8sD3VENyz0CWhcbgTWWNv1en8/edit#heading=h.62vaulqa2j2p", "Best exchange rates for gear/currency"), "Relics", "Currency", "Gear"),

    Entry("2020-11-15", .link("https://mods-optimizer.swgoh.grandivory.com", "Mods Optimizer"), "Mods"),
    Entry("2020-11-15", .link("https://gaming-fans.com/star-wars-goh/mods/", "Gaming Fans: How to mod"), "Mods"),
    Entry("2020-11-15", .link("https://gaming-fans.com/star-wars-goh/swgoh-101/swgoh-101-comprehensive-mod-guide/", "Gaming Fans: Mods 101"), "Mods"),
    Entry("2021-02-14", .link("https://docs.google.com/document/d/1BTvjpWo61Xsybj_Rf70fP1M2fMpOj2qNathKWMqu5XQ/edit#heading=h.pjdsj46wo4j", "Modding Guide with explanations"), "Mods"),

    Entry("2020-11-15", .image("https://cdn.discordapp.com/attachments/220041366288203777/660528808801206273/15775525933923097103248563900018.jpg", "GAS Phase 2 Modding"), "GAS", "Unlock"),
    Entry("2020-11-15", .image("https://cdn.discordapp.com/attachments/220041366288203777/660529093170692097/phase_4_mod_guide_final.jpg", "GAS Phase 4 Modding"), "GAS", "Unlock"),
    Entry("2020-11-15", .youtube("72tM3A-0uL4"), "GAS", "Unlock"),

    Entry("2020-11-15", .link("https://swgohcounters.com", "Counters 5v5 (up to date)"), "Counters", "5v5"),
    Entry("2020-11-15", .link("https://swgohcounters.com/3v3", "Counters 3v3 (up to date)"), "Counters", "3v3"),
    Entry("2020-11-15", .youtube("FAczLg-cnrc"), "Counters", "GL"),
    Entry("2020-11-15", .link("https://swgoh.gg/gac/counters/", "GAC counter information"), "Counters", "GAC"),
    Entry("2020-11-15", .link("https://docs.google.com/spreadsheets/d/1ovD9OCkfe48H9Uu7_Y5hrgQ46O8brWqGQUOmCxQzHQk/edit#gid=0", "Counters Spreadsheet with Videos"), "Counters", "3v3", "5v5"),
    Entry("2020-12-01", .link("https://counters.bobasalliance.com", "Boba's Alliance Counters -- has some explanations"), "Counters"),

    Entry("2020-11-15", .image("https://cdn.discordapp.com/attachments/549727213046464512/773389480164851743/image0.jpg", "GAC 3v3 Teams"), "GAC", "3v3"),

    Entry("2020-11-15", .link("https://docs.google.com/spreadsheets/d/17NTcqrWm_EAUgEgdNuPgC2uc7bYd_yrR0JlO8-c7VUw/edit", "TB Planning Spreadsheet"), "TB"),

    Entry("2020-11-15", .link("https://docs.google.com/document/d/1w3LI2RLOHimGire6IoTHL5WS7kwV5UxXXhNicBQ7ESU/edit", "Fleet Arena Suggestions"), "Fleet", "Ships"),
    Entry("2020-11-15", .link("https://docs.google.com/document/d/1as0OER6Pc14RdoyVpOd0trSlUcMMhZpRTv1RbM9sqKo/edit", "Fleet Discord Guide to Ships"), "Fleet", "Ships"),
    Entry("2020-11-15", .link("https://www.redventbard.com/swgoh-the-state-of-fleet", "Fleet Tiers: what beats what"), "Fleet"),
    Entry("2020-11-15", .link("https://www.redventbard.com/swgoh-the-separatist-fleet", "How to use Malevolence"), "Fleet", "Malevolence"),
    Entry("2020-11-15", .link("https://www.redventbard.com/swgoh-finalizer-fleet", "How to use Finalizer"), "Fleet", "Finalizer"),

    Entry("2020-11-15", .link("https://wiki.swgoh.help/wiki/Scavenger_Guide", "Farming for Relic Salvage: Guide"), "Relics", "Gear"),
    Entry("2020-11-15", .link("https://wiki.swgoh.help/wiki/Chromium_Transistor_Crafting_Tool", "Farming for Relic Salvage: Calculator"), "Relics", "Gear"),

    Entry("2020-12-13", .link("https://docs.google.com/spreadsheets/d/1-8kSEWRn9RKs-cbs08l2yaaDTsE7YN9Ee00qL8iyOKw/htmlview#gid=0", "Pit Teams Spreadsheet"), "Crancor"),
    Entry("2020-12-13", .youtube("z8uGcIQHvSQ"), "Crancor"),
    
    Entry("2020-12-19", .link("https://www.reddit.com/r/SWGalaxyOfHeroes/comments/kghn5v/rancor_challenge_teams_updated/", "Pit Teams Graphic (1.0 - beware)"), "Crancor"),
    Entry("2020-12-19", .image("https://i.redd.it/69kgax5ex7661.png", "Pit Teams"), "Crancor"),
    
    
    Entry("2020-12-13", .link("https://www.reddit.com/r/SWGalaxyOfHeroes/comments/kc6sjk/geonosian_mass_assist_order/", "Squad Toon Order"), "Theorycraft"),
    
    Entry("2020-12-13", .link("https://www.reddit.com/r/SWGalaxyOfHeroes/comments/lh147m/assault_battles_challenge_tier_guide_updated_v11/", "Challenge Tier Teams"), "Challenges"),

    Entry("2021-03-08", .youtube("Yz9HYvQLE8Q"),
          .comment("DETAILED strategy GUIDE for REBEL Fleets - EASILY beat Negotiator & Malevolence - Dominate Arena!!!!"),
          tags: ["Fleet" ]),

]

struct AllianceResourcesPage : PageBuilder {
    
    var path: String { return "r/index.html" }
    
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "BRG Alliance Resources")
        
        let tags = Set(database.flatMap { $0.tags }).sorted()
        
        result +=
            """
            <script>
            $(document).ready( function () {
                            
              var table = $('#resources').DataTable({
                dom: 'lrtip',
                "columnDefs": [
                        { "targets": [1], "searchable": false }
                    ]
              });

                function search(s) {
                     if (s.length == 0) {
                        $('#resources_wrapper').hide();
                        $('#tags').show();
                     } else {
                        $('#resources_wrapper').show();
                        $('#tags').hide();
                        table.search(s).draw();
                     }
                }
              
              $('#resources_wrapper').hide();

                const urlParams = new URLSearchParams(window.location.search);
                const q = urlParams.get('q');
              if (q) {
                $('#search').val(q);
                search(q);
              }
              
              $('#search').keyup( function() {
                 search($(this).val());
              } );
            """
        
        result +=
            """
            } );
            </script>
            """

        result +=
            """
            <p>
            Please contact @gorgatron#3094 to add an item to the resource database, or mention it
            in the #ask-an-expert channel!

            <p>
            Search: <input type="text" id="search" placeholder="topics">
            """
        
        result +=
            H.div().id("tags") {
                "<br/>"
                "Tags: "
                H.foreach(tags.enumerated()) { (i, t) in
                    H.a(href: "?q=\(t)") { t }
                    " &#09; "
                }
            }.render()
        
        result +=
            H.table().id("resources").width("100%") {
                H.thead {
                    H.tr {
                        H.th {
                            "Tags"
                        }
                        H.th {
                            "Resource"
                        }
                    }
                }

                H.foreach(database) { e in
                    H.tr {
                        H.td {
                            e.tags.joined(separator: ", ")
                        }
                        H.td {
                            H.foreach(e.content) { c in
                                c.render()
                            }
                        }
                    }
                }
            }.render()
        
        result += html.footer()

        return result
    }
}
