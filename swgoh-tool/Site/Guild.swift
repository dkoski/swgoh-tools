//
//  Guild.swift
//  swgoh
//
//  Created by David Koski on 8/23/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

struct TeamCSSPage : PageBuilder {
    
    var path: String { return "player/teams.css" }
    
    func contents(html: HTML) -> String {
        var result = ""
        
        func q(_ string: String) -> String {
            string
                .replacingOccurrences(of: "\"", with: "\\\"")
                .replacingOccurrences(of: "\n", with: " ")
        }
        
        for teamId in Counter.counterableIds {
            let team = CounterTeam.find(teamId)

            result += ".\(teamId)-desc:before { content: \"\(q(team.description))\"; }\n"
            result += ".\(teamId)-strat:before { content: \"\(q(team.strategy))\"; }\n"
            result += ".\(teamId)-subs:before { content: \"\(q(team.subs))\"; }\n"
        }
        
        for counter in Counter.counters.values.flatMap({ $0 }) {
            result += ".\(counter.id)-strat:before { content: \"\(q(counter.description))\"; }\n"
        }
        
        return result
    }
}

struct ToonCSSPage : PageBuilder {
    
    var path: String { return "toons.css" }
    
    func contents(html: HTML) -> String {
        var result = ""
        
        for c in Model.Character.all.sorted() {
            result += ".\(c.id)-i { content:url(\"\(c.imageURL)\"); }\n"
        }
        
        return result
    }
}

struct GuildIndex : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/index.html" }
    
    let guild: Model.Guild
    
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        
        result += html.header(title: guild.name, nav: "&nbsp;")
        
        result +=
            """
            <b>Players:</b> \(guild.players.count)<br/>
            <b>GP:</b> \(guild.info.gp.millions)<br/>
            <b>Units GP:</b> \(guild.players.reduce(0) { $0 + $1.stats.characterGP }.millions)<br/>
            <b>Ships GP:</b> \(guild.players.reduce(0) { $0 + $1.stats.shipGP }.millions)<br/>

            <h2>Guild</h2>

            <ul>
            <li><a href="../../player/\(guild.shortName).html">Player Index</a></li>
            <li><a href="guild-farming.html">Farming Report</a></li>
            <li><a href="guild-stats.html">Guild Statistics</a></li>
            <li><a href="guild-charts.html">Guild Charts</a></li>
            <li><a href="guild-stats.csv">Guild Statistics (CSV)</a></li>
            <li><a href="guild-important-toons.html">Important Toons</a></li>
            <li><a href="guild-squads.html">Squads (non-overlapping)</a></li>
            <li><a href="guild-squads-all.html">Squads (all)</a></li>
            </ul>

            <h2>Mods</h2>
            <ul>
            <li><a href="mods-stats.html">Guild Mod Stats -- e.g. view mod speed across all BH</a></li>
            <li><a href="mods-players.html">Guild Mod Player Report -- counts and stats by player</a></li>
            <li><a href="mods-characters.html">Guild Mod Character Report -- counts and stats by character</a></li>
            </ul>

            <h2>Farming / Unlock</h2>
            
            <ul>
            \( H.foreach(FarmingUnlockPage.unlocks) { u in
                H.li {
                    H.a(href: html.g(self.guild, "farm-unlock-\(u.target.id).html")) {
                        u.target.name
                    }
                }
            }.render()
            )
            </ul>
            
            <h2>TW</h2>
            
            <ul>
            <li><a href="tw-squads.html">TW Squad Inventory</a></li>
            <li><a href="guild-squads.csv">TW Squad Inventory (CSV)</a></li>
            </ul>

            Note: the following pages use particular assumptions about how you run your TW.  Contact @gorgatron#3094 if you would like to configure these for your own style.

            <ul>
            <li><a href="tw-planning.html">TW Planning</a></li>
            <li><a href="tw-summary.html">TW Summary</a></li>
            <li><a href="tw-orders.html">TW Orders</a></li>
            <li><a href="tw-remainder-players.html">Remaining Teams by Player</a></li>
            <li><a href="tw-remainder-teams.html">Remaining Teams by Team</a></li>
            </ul>
            
            <h2>TB</h2>

            <ul>
            <li><a href="rare-units.html">Rare Units</a></li>
            </ul>
            <h2>DS Geo TB</h2>
            
            <ul>
            <li><a href="ds-geo-tb.html">DS Geo TB Plan</a></li>
            <li><a href="dsgeotb-squads-section.html">DS Geo TB Squads by Section</a></li>
            <li><a href="dsgeotb-squads-type.html">DS Geo TB Squads by Type</a></li>
            <li><a href="dsgeotb-squads-chart.html">DS Geo TB Squads Chart</a></li>
            </ul>
            <h2>LS Geo TB</h2>
            
            <ul>
            <li><a href="ls-geo-tb.html">LS Geo TB Plan</a></li>
            <li><a href="lsgeotb-squads-section.html">LS Geo TB Squads by Section</a></li>
            <li><a href="lsgeotb-squads-type.html">LS Geo TB Squads by Type</a></li>
            <li><a href="lsgeotb-squads-chart.html">LS Geo TB Squads Chart</a></li>
            </ul>
            
            <h2>Raids</h2>
            <ul>
            <li><a href="hstr.html">HSTR schedule</a></li>
            <li><a href="rancor.html">Challenge Rancor schedule</a></li>
            <li><a href="haat.html">HAAT schedule</a></li>
            </ul>

            """
        
        result += html.footer()
        
        return result
    }
}

struct GuildInfoPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/guild-stats.html" }
    
    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]
        
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "Guild Statistics")
        result += html.dataTable(id: "stats", fixedHeader: true)

        var rowFactory = StatsRowFactory(extractors: StatsRowExtractors.playerExtractors.adding(StatsRowExtractors.playerEvaluationExtractors))
        
        do {
            let gpChartData = try GuildPlayerGPChart(guild: guild).loadChartData(base: html.baseURL)
            
            let dateRange2Weeks = Date() - (24 * 3600 * 14) ... Date()
            let deltaGP2Weeks = StatsRowExtractor(name: "Δ gp/d short",
                title: "Delta gp per day over the last 14 days",
                describe: { $0.millions },
                apply: { (g, p, evs) -> Int in
                    let (values, dates) = gpChartData.data(in: dateRange2Weeks, key: p, valueKey: "total")
                    let days = Int(round(dates.lowerBound.distance(to: dates.upperBound) / (24 * 3600)))
                    if values.isEmpty || days == 0 {
                        return 0
                    } else {
                        return (values.last! - values.first!) / days
                    }
                })
            
            let dateRange90Days = Date() - (24 * 3600 * 90) ... Date()
            let deltaGP90Days = StatsRowExtractor(name: "Δ gp/d long",
                title: "Delta gp per day over the last 90 days",
                describe: { $0.millions },
                apply: { (g, p, evs) -> Int in
                    let (values, dates) = gpChartData.data(in: dateRange90Days, key: p, valueKey: "total")
                    let days = Int(round(dates.lowerBound.distance(to: dates.upperBound) / (24 * 3600)))
                    if values.isEmpty || days == 0 {
                        return 0
                    } else {
                        return (values.last! - values.first!) / days
                    }
                })

            
            rowFactory.extractors.insert(deltaGP2Weeks, at: 1)
            rowFactory.extractors.insert(deltaGP90Days, at: 1)
        } catch {
            print("Error loading chart data: \(error)")
        }
        
        var rows = [(Model.Player, StatsRow)]()
        var sum = rowFactory.emptyRow()
        
        for player in guild.players {
            if let evaluations = squads[player] {
                let row = rowFactory.newRow(guild: guild, player: player, evaluations: evaluations)
                sum.add(row: row, rowFactory: rowFactory)
                rows.append((player, row))
            }
        }
        sum.finishAdd(rowFactory: rowFactory)
        
        result +=
            H.table().id("stats").style("font-size: 85%") {
                H.thead {
                    H.tr {
                        H.th { "Player" }
                        H.th { "Code" }
                        rowFactory.header()
                    }
                }
                H.foreach(rows) { (player, row) in
                    H.tr {
                        H.td { H.a(href: html.p(player, "tw-squads")) { player.name } }
                        H.td { H.a(href: "https://swgoh.gg/p/\(player.code)/") { player.code } }
                        row.render()
                    }
                }
                H.tfoot {
                    H.td { guild.players.count }
                    H.td { "" }
                    sum.render()
                }
            }.render()
                
        result += html.footer()
        
        return result
    }
}

struct TWPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/\(name).html" }

    let guild: Model.Guild
    let name: String
    let contents: String
        
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss

        result += html.header(title: "Guild Statistics")
        
        result +=
            """
            <img src="\(html.i("tw-map.png"))" width="622" height="441"/>
            <pre>
            """
        result += contents
        result += "</pre>"
                
        result += html.footer()
        
        return result
    }
}

struct GuildInfoCSV : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/guild-stats.csv" }
    
    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]
        
    func contents(html: HTML) -> String {
        var result = ""
        
        let rowFactory = StatsRowFactory(extractors: StatsRowExtractors.playerExtractors.adding(StatsRowExtractors.playerEvaluationExtractors))
        var rows = [(Model.Player, StatsRow)]()
        
        for player in guild.players {
            if let evaluations = squads[player] {
                let row = rowFactory.newRow(guild: guild, player: player, evaluations: evaluations)
                rows.append((player, row))
            }
        }

        result += #"Name,Ally Code,"#
        result += rowFactory.extractors.map { $0.title ?? $0.name }.joined(separator: ",")
        result += "\n"
        
        for (player, row) in rows {
            result += "\""
            result += player.name
            result += "\","
            result += player.code
            result += ","
            
            result += row.values.map { $0.description }.joined(separator: ",")

            result += "\n"
        }
        
        return result
    }
}

struct ImportantToonsPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/guild-important-toons.html" }
    
    let guild: Model.Guild
    
    static let importantToons = [
        "DR", "Malak",
        "JKR", "GMY",
        "GAS",
        "Rey", "SL Kylo",
        "SEE", "JMLS",
        "JMK",
        
        "JKL",
        "Padme", "GK", "JKA",
        "Grievous",
        "Brood Alpha",
        "Wat Tambor", "Ki-Adi-Mundi",
        "CLS", "Chewbacca", "Han Solo", "C-3PO", "R2-D2",
        "Han's MF", "HT", "Negotiator", "Malevolence", "Raddus", "Finalizer",
        "JTR",
        "Asajj", "MT", "Daka", "Zombie",
        "Nest", "Wampa", "IPD",
        "Bossk", "Boba Fett", "Jango Fett",
        "EP", "Vader", "Thrawn",
        "Traya", "Sion", "DN",
    ]
    
    static let galacticLegendToons = [
        "Rey",
        "SL Kylo",
        "Sith Eternal Emperor",
        "Jedi Master Luke Skywalker",
    ]
    
    static func formatTable(html: HTML, guild: Model.Guild, toons: [String], countIf: (Model.Unit) -> Bool = { _ in true }) -> String {
        var result = ""
        
        result += html.dataTable(id: "toons", fixedColumns: 1)
        
        var counts = [String:Int]()
        
        result +=
            """
            <table id="toons" class="display" style="font-size:85%">
                <thead>
                    <tr>
                    <th>Player</th>
                    \(toons.map { "<th>" + $0 + "</th>" }.joined() )
                    </tr>
                </thead>
                <tbody>
            """
        
        for player in guild.players {
            result += "<tr>"
            result += "<td><a href=\"\(html.p(player, "tw-squads"))\">\(player.name)</a></td>"
            for name in toons {
                let id = Model.Character.find(name).id
                if let unit = player.units[id] {
                    
                    let include = countIf(unit)
                    if include {
                        counts[name, default: 0] += 1
                    }
                    
                    var z = ""
                    for _ in unit.zetas {
                        z += "z"
                    }

                    let g = unit.gearLevelDescription
                    let gp = unit.gp.millions
                    
                    var style = ""
                    if !include {
                        style = " style=\"background-color: yellow;\""
                    }
                    result += "<td nowrap data-order=\"\(unit.gp)\"\(style)>\(gp) \(g) \(z)</td>"
                } else {
                    result += "<td data-order=\"0\"></td>"
                }
            }
            result += "</tr>\n"
        }
        
        result +=
            """
                </tbody>
                <tfoot>
                <tr>
                <td></td>
                \(toons.map { "<td>" + counts[$0, default:0].description + "</td>" }.joined())
                </tr>
                </tfoot>
            </table>
            <br/>
            """
        
        return result
    }

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "\(guild.name) Important Toons")
        
        let importantToons = ImportantToonsPage.importantToons
        
        result += ImportantToonsPage.formatTable(html: html, guild: guild, toons: importantToons)
        
        result += html.footer()
        
        return result
    }

}

struct SquadsPage : PageBuilder {
    
    var path: String { "guild/\(guild.shortName)/\(filename).html" }

    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]
    var distinct = true
    
    var filename: String
    var title: String
    
    var playerLink = "tw-squads"

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.toonCss
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "\(guild.name) \(title)")
        result += html.dataTable(id: "squads", fixedColumns: 1)
                
        var counts = [String:Int]()
        
        var counterTeams = [String:CounterTeam]()
        var squadImageClass = [String:String]()
        var squadNamesSet = Set<String>()
        for evaluations in squads.values {
            squadNamesSet.formUnion(evaluations.compactMap({ $0.squad.name }))
            for evaluation in evaluations {
                let name = evaluation.squad.name
                if squadImageClass[name] == nil {
                    squadImageClass[name] = evaluation.squad.units[0].character.id + "-i"
                }
                if counterTeams[name] == nil {
                    if let counterTeam = evaluation.squad.counterTeam {
                        counterTeams[name] = counterTeam
                    }
                }
            }
        }
        let squadNames = squadNamesSet.sorted() { $0.uppercased() < $1.uppercased() }

        result +=
            """
            <p>
            \(distinct ? "These are non-overlapping teams." : "These teams may overlap with each other.")
            <p>
            Note: You know your teams
            better than the program that generates this, so use this as a guideline,
            not hard facts.
            <p>
            \(CounterType.offense.symbol): Considered mainly an offensive team
            <br/>
            \(CounterType.defense.symbol): Considered mainly an defensive team
            <br/>
            \(CounterType.both.symbol): Can be offensive or defensive
            <br/>
            <img src=\"\(html.i("wrench24.png"))\">: Team may be usable, but gear is recommended.
            <br/>
            <img src=\"\(html.i("tractor24.png"))\">: Probably not usable -- needs farming.
            <br/>
            &#11088;: Team is considered well geared
            </p>
            <br/>
            <table id="squads" class="display" style="font-size:85%">
                <thead>
                    <tr>
                    <th>Player</th>
                    <th>Count</th>
                    \(squadNames.map { "<th align=\"center\"><div style=\"vertical-align:middle\"><img class=\"toI squad \(squadImageClass[$0]!)\" title=\"\($0)\"/>\(counterTeams[$0]?.type.symbol ?? "")</div><br/>\($0)</th>" }.joined() )
                    </tr>
                </thead>
                <tbody>
            """
        
        for player in guild.players {
            result += "<tr>"
            result += "<td><a href=\"\(html.p(player, playerLink))\">\(player.name)</a></td>"
            
            var viableSquadCount = 0
            
            for squadName in squadNames {
                if let squadEv = squads[player]?.first(where: { $0.squad.name == squadName }) {
                    
                    if squadEv.status.isViable {
                        counts[squadName, default: 0] += 1
                        counts["TOTAL", default: 0] += 1
                        viableSquadCount += 1
                    }
                }
            }
            
            result += "<td>\(viableSquadCount)</td>"

            for squadName in squadNames {
                if let squadEv = squads[player]?.first(where: { $0.squad.name == squadName }) {
                    
                    let gp = squadEv.squad.gp.millions

                    result += "<td nowrap style=\"font-size:75%\" data-order=\"\(gp)\">"
                    result += "\(gp) - "
                    
                    let gearNeeded = squadEv.gearNeeded()
                    if let gearNeeded = gearNeeded {
                        result += "<span title=\"\(gearNeeded)\">"
                    }

                    result += squadEv.squad.description(gearBelow: 11, starsBelow: 7)
                    
                    if let _ = gearNeeded {
                        let icon = squadEv.status.isViable ? "wrench24.png" : "tractor24.png"
                        result += " <img style=\"margin-left: 10px\" src=\"\(html.i(icon))\"></span>"
                        
                    } else if squadEv.scorePercent >= 0.90 {
                        result += " &#11088;"
                    }

                    result += "</td>"

                } else {
                    result += "<td data-order=\"0\"></td>"
                }
            }
            result += "</tr>\n"
        }
        
        result +=
            """
                </tbody>
                <tfoot>
                <tr>
                <td>Viable Count</td>
                <td>\(counts["TOTAL", default: 0])</td>
                \(squadNames.map { "<td>" + counts[$0, default:0].description + "</td>" }.joined())
                </tr>
                </tfoot>
            </table>
            <br/>
            """
        
        result += html.footer()
        
        return result
    }

}

struct GuildSquadsCSVPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/guild-squads.csv" }

    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]

    func contents(html: HTML) -> String {
        var result = "Player,Offense,Defense,GP,Name,Squad,Viable\n"
        
        func quote(_ str: String) -> String {
            var result = str
            if result.contains("\"") {
                result = result.replacingOccurrences(of: "\"", with: "\"\"")
            }
            result = "\"" + result + "\""
            return result
        }
        
        for (player, squadEvaluations) in squads {
            let quotedName = quote(player.name)
            
            for squadEv in squadEvaluations {
                result += quotedName + ","
                if let type = squadEv.squad.counterTeam?.type {
                    result += (type == .offense || type == .both) ? "1" : "0"
                    result += ","
                    result += (type == .defense || type == .both) ? "1" : "0"
                    result += ","
                } else {
                    result += ",,"
                }
                result += squadEv.squad.gp.description
                result += ","
                result += squadEv.squad.name
                result += ","
                result += quote(squadEv.squad.description(images: false, zetas: true, gearBelow: 10, starsBelow: 7))
                result += ","
                result += squadEv.status.isViable ? "1" : "0"
                result += "\n"
            }
        }
        
        return result
    }

}

struct GuildTWSquadsPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/tw-squads.html" }
    
    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.toonCss
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += html.dataTable(id: "squads", fixedHeader: true)

        result += html.header(title: "\(guild.name) TW Squad Inventory")

        var counterTeams = [String:CounterTeam]()
        var squadImageClass = [String:String]()
        var squadsByName = [String:[(Model.Player,SquadEvaluation)]]()

        var squadNamesSet = Set<String>()
        for (player, evaluations) in squads {
            squadNamesSet.formUnion(evaluations.compactMap({ $0.status.isViable ? $0.squad.name : nil }))
            for evaluation in evaluations {
                if !evaluation.status.isViable {
                    continue
                }
                let name = evaluation.squad.name
                if squadImageClass[name] == nil {
                    squadImageClass[name] = evaluation.squad.units[0].character.id + "-i"
                }
                if counterTeams[name] == nil {
                    if let counterTeam = evaluation.squad.counterTeam {
                        counterTeams[name] = counterTeam
                    }
                }
                squadsByName[name, default:[]].append((player, evaluation))
            }
        }
        let squadNames = squadNamesSet.sorted() { $0.uppercased() < $1.uppercased() }

        result +=
            """
            <p>
            You know your teams
            better than the program that generates this, so use this as a guideline,
            not hard facts.
            </p>
            
            <p>
            \(CounterType.offense.symbol): Considered mainly an offensive team
            <br/>
            \(CounterType.defense.symbol): Considered mainly an defensive team
            <br/>
            \(CounterType.both.symbol): Can be offensive or defensive
            <br/>
            <img src=\"\(html.i("wrench24.png"))\">: Team may be usable, but gear is recommended.  Hover or click to see more info.
            </p>
            <button onclick=\"$('.toggle-all').toggle()\">show/hide all</button><br/>
            
            <table id="squads" class="display">
            <thead>
            <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Count</th>
            <th>Squads</th>
            </tr>
            </thead>
            """
        
        var index = 0
        for squadName in squadNames {
            index += 1
            
            result += "<tr valign=\"top\">"
            result += "<td data-order=\"\(squadName)\" align=\"center\">"           
            result += "<img class=\"toI squad \(squadImageClass[squadName]!)\" onclick=\"$('#squads\(index)').toggle()\"  title=\"\(squadName)\"/>"
            result += "</td>"
            
            if let team = counterTeams[squadName] {
                result += "<td data-order=\"\(team.type)\">\(team.type.symbol)</td>"
            } else {
                result += "<td data-order=\"unknown\"></td>"
            }
            
            let squads = squadsByName[squadName]!.sorted {
                $0.1.squad.gp > $1.1.squad.gp
            }
                        
            result += "<td>\(squads.count)</td>"
            
            result += "<td class=\"verbage\">"
                        
            result += "<a onclick=\"$('#squads\(index)').toggle()\">show/hide</a><br/>"
            result += "<table id=\"squads\(index)\" class=\"toggle-all\">"
            for (player, ev) in squads {
                result += "<td><b><a href=\"\(html.p(player, "tw-squads"))#\(squadName)\">\(player.name)</a></b></td>"
                result += "<td>\(ev.squad.gp.millions)</td>"
                result += "<td nowrap>"
                result += ev.squad.description(gearBelow: 12, starsBelow: 7)
                if let gearNeeded = ev.gearNeeded() {
                    result += "<img title=\"\(gearNeeded)\" style=\"margin-left: 10px\" src=\"\(html.i("wrench24.png"))\">"
                }
                result += "</td>"
                result += "</tr>"
            }
            result += "</table>"

            result += "</td>"

            result += "</tr>\n"
        }
        result +=
            """
            </table>
            """
        
        // TODO compute overlapping counters and suggest groups

        result += html.footer()
        
        return result
    }
}

struct TBPlanPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/\(shortName)-tb.html" }
    
    let guild: Model.Guild
    
    let title: String
    let shortName: String
    
    let boardConfiguration: TBBoardConfiguration
    let missionEstimator: TBMissionEstimator
    
    func format(board: TB.Board, id: String, title: String, html: HTML) -> String {
        var result = ""

        result +=
            """
            <h2>\(title) Estimated: \(board.stars) &#11088;</h2>
            <br/>
            <table style="margin: 10px">
            <thead>
                <tr>
                <th>Phase</th>
                <th>Section</th>
                <th>Action</th>
                </tr>
            </thead>
            <tbody>
            """
        
        var currentPhase = 0
        for activity in board.activity {
            if activity.phase != currentPhase {
                currentPhase = activity.phase
                result += "<tr class=\"newPhase\">"
            } else {
                result += "<tr>"
            }
            
            result += "<td>\(activity.phase)</td>"
            
            if let section = activity.section {
                result += "<td>\(section)</td>"
                result += "<td>\(activity.htmlDescription)</td>"
            } else {
                result += "<td>&nbsp;</td>"
                result += "<td>\(activity.htmlDescription)</td>"
            }
        }

        result +=
            """
            </tbody>
            </table>
            <br/>
            <br/>
            """
        
        var discord = "**Estimated \(board.stars) :star:**\n"
        currentPhase = 0
        for activity in board.activity {
            if activity.phase != currentPhase {
                currentPhase = activity.phase
                discord += "\n"
            }
            
            discord += activity.discordDescription
            discord += "\n"
        }

        result +=
            """
            <textarea id=\"discord\(id)\" style="display:none" rows="8" cols="40">
            \(discord)
            </textarea>
            <button onclick="copyText('discord\(id)')">Copy For Discord</button>
            """
                
        return result
    }

    func simulate(deployFactor: @escaping (Int) -> Double, missionFactor: @escaping (Int) -> Double, missionEstimator: TBMissionEstimator? = nil, planComparator: @escaping (TB.Board, TB.Board) -> Bool? = { _, _ in return nil }) -> TB.Board {
        let toonsGP = guild.players.reduce(0) { $0 + $1.stats.characterGP } / 1_000_000
        let shipsGP = guild.players.reduce(0) { $0 + $1.stats.shipGP } / 1_000_000
        
        let tb = TB(toonGP: toonsGP, shipGP: shipsGP, configuration: boardConfiguration, missionEstimator: missionEstimator ?? self.missionEstimator, deployFactor: deployFactor, missionFactor: missionFactor, planComparator: planComparator)
        let board = tb.simulate()
        
        return board
    }

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.jqueryInclude

        result += html.header(title: "\(guild.name) \(title) Plan")
        
        struct BoardPlus {
            let board: TB.Board
            let title: String
            let id: String
            let description: String
        }
                
        var boards = [BoardPlus]()

        let decreasingFactor = { (phase: Int) -> Double in return 1.0 - (Double(phase) - 1) * 0.04 }

        if shortName == "ds-geo" {
            boards.append(BoardPlus(
                board: simulate(deployFactor: { _ in return 1.0 }, missionFactor: { _ in return 1.0 }),
                title: "Optimal", id: "optimal",
                description: "assumes that everybody deploys and you have reasonable success with the missions.  It is not necessarily a lower bound -- the missions are just estimates of how well you do, but it is an optimistic view.  You might score a star or two higher, especially if you are close on some sections.  You won't make it if participation is low."))
            boards.append(BoardPlus(
                board: simulate(deployFactor: { _ in return 0.9 }, missionFactor: { _ in return 0.9 }),
                title: "90%", id: "percent90",
                description: "is a more pessimistic view:  it assumes that 90% of your gp is deployed and that missions are only 90% as effective as the estimate.  Not a lower bound, but this should realisticly be possible."))
            boards.append(BoardPlus(
                board: simulate(deployFactor: decreasingFactor, missionFactor: decreasingFactor),
                title: "Decreasing", id: "decreasing",
                description: "is in between.  It assumes full participation and good performance on the early phases and tapers off on the later phases (this matches what I observe)."))

            // for ds-geo, if we don't have a plan that generates 3N (Wat Tambor shards), try and make one
            let activityContains3N = { (board: TB.Board) in
                board.activity.contains {
                    if let section = $0.section {
                        return section.phase == 3 && section.name == "N"
                    } else {
                        return false
                    }
                }
            }
            
            if !boards.contains(where: { activityContains3N($0.board) }) {
                let specialBoard = simulate(deployFactor: decreasingFactor, missionFactor: decreasingFactor, planComparator: { (new, old) in
                    // if it doesn't have 3N, toss it, otherwise follow the normal rules
                    if !activityContains3N(new) { return false }
                    return nil
                })
                
                boards.append(BoardPlus(board: specialBoard,
                    title: "Wat Shards", id: "wat",
                    description: "is the same as <b>Decreasing</b> but it tries to force the TB to reach 3N to obtain Wat shards."))
            }
        } else {
            boards.append(BoardPlus(
                board: simulate(deployFactor: { _ in return 1.0 }, missionFactor: { _ in return 1.0 }),
                title: "Optimal", id: "optimal",
                description: "assumes that everybody deploys and you have reasonable success with the missions.  It is not necessarily a lower bound -- the missions are just estimates of how well you do, but it is an optimistic view.  You might score a star or two higher, especially if you are close on some sections.  You won't make it if participation is low."))
            boards.append(BoardPlus(
                board: simulate(deployFactor: decreasingFactor, missionFactor: decreasingFactor),
                title: "Decreasing", id: "decreasing",
                description: "is in between.  It assumes full participation and good performance on the early phases and tapers off on the later phases (this matches what I observe)."))
            boards.append(BoardPlus(
                board: simulate(deployFactor: { _ in return 1.0 }, missionFactor: { _ in return 1.0 }, missionEstimator: NoMissionEstimator()),
                title: "No Missions", id: "nomission",
                description: "is just 100% deploys, nothing from missions."))

            let activityContains3S = { (board: TB.Board) in
                board.activity.contains {
                    if let section = $0.section {
                        return section.phase == 3 && section.name == "Bottom"
                    } else {
                        return false
                    }
                }
            }
            
            if !boards.contains(where: { activityContains3S($0.board) }) {
                let specialBoard = simulate(deployFactor: decreasingFactor, missionFactor: decreasingFactor, planComparator: { (new, old) in
                    // if it doesn't have 3S, toss it, otherwise follow the normal rules
                    if !activityContains3S(new) { return false }
                    return nil
                })
                
                boards.append(BoardPlus(board: specialBoard,
                    title: "KAM Shards", id: "kam",
                    description: "is the same as <b>Decreasing</b> but it tries to force the TB to reach 3 Bottom to obtain KAM shards."))
            }
        }
        
        result +=
            """
            <style type="text/css">
            .newPhase td{
              border-top: 2px solid black;
            }
            .column {
              float: left;
              width: \(100 / boards.count)%;
            }
            .row:after {
              content: "";
              display: table;
              clear: both;
            }
            </style>
            <script>
            function copyText(name) {
              var copyText = document.getElementById(name);
              $("#"+name).show();
              copyText.select();
              copyText.setSelectionRange(0, 99999)
              document.execCommand("copy");
            }
            </script>
            There are three models of activity:
            <ul>
            """
        
        for board in boards {
            result += H.li() {
                H.b() { board.title }
                " "
                board.description
            }.description
        }
        
        result +=
            """
            </ul>
            Contact @gorgatron#3094 to tailor these assumptions.
            <br/>
            <img src="\(html.i(shortName.replacingOccurrences(of: "-", with: "_") + "_tb_labels.jpeg"))" width="500"/>
            <br/>
            <br/>
            """
        
        if shortName == "ds-geo" {
            result += #"<p>See also <a href="https://genskaar.github.io/tb_geo/html/ds.html">https://genskaar.github.io/tb_geo/html/ds.html</a>.</p><br/>"#
        } else if shortName == "ls-geo" {
            result += #"<p>See also <a href="https://genskaar.github.io/tb_geo/html/ls.html">https://genskaar.github.io/tb_geo/html/ls.html</a>.</p><br/>"#
        }

        result += "<div class=\"row\">"
        
        for board in boards {
            result += H.div(["class" : "column"]) {
                format(board: board.board, id: board.id, title: board.title, html: html)
            }.description
        }

        result += "</div>"

        result += html.footer()
        
        return result
    }

    
}

struct TBSquadsBySectionPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/\(shortName)-squads-section.html" }

    let title: String
    let shortName: String
    
    let guild: Model.Guild
    let squads: [Model.Player: [TB.Key:[SquadEvaluation]]]

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.toonCss
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "\(guild.name) \(title) Squads By Section")
        result += html.dataTable(id: "squads", fixedColumns: 1)
                
        var counts = [TB.Key:Int]()
        
        let columns = squads.randomElement()!.value.keys.sorted()

        result += """
        <a href="\(shortName)-squads-section.html">Squads by Section</a>&nbsp;|&nbsp;
        <a href="\(shortName)-squads-type.html">Squads by Type</a>&nbsp;|&nbsp;
        <a href="\(shortName)-squads-chart.html">Squads Chart</a>

        \(shortName == "dsgeotb" ? #"<p>See also <a href="https://genskaar.github.io/tb_geo/html/ds.html">https://genskaar.github.io/tb_geo/html/ds.html</a>.</p>"# : "")
        \(shortName == "lsgeotb" ? #"<p>See also <a href="https://genskaar.github.io/tb_geo/html/ls.html">https://genskaar.github.io/tb_geo/html/ls.html</a>.</p>"# : "")
        <p>Please check platoons before using any of these squads in missions.  Also note that the section names are positions on the map rather than phase numbers:</p>
        <br/>
        <img src="\(html.i("ds_geo_tb_labels.jpeg"))" width="500"/>
        """

        result +=
            """
            <br/>
            <table id="squads" class="display" style="font-size:85%">
                <thead>
                    <tr>
                    <th>Player</th>
                    <th>Count</th>
                    \(columns.map { "<th align=\"center\"><div style=\"vertical-align:middle\">\($0)</th>" }.joined() )
                    </tr>
                </thead>
                <tbody>
            """
        
        for player in guild.players {
            result += "<tr>"
            result += "<td><a href=\"\(html.p(player, shortName))\">\(player.name)</a></td>"
            
            var distinctSquads = Set<Squad>()
            
            for section in columns {
                let evaluations = squads[player, default:[:]][section, default: []]
                for squadEv in evaluations {
                    if squadEv.status.isViable {
                        distinctSquads.insert(squadEv.squad)
                    }
                }
                counts[section, default: 0] += evaluations.filter { $0.status.isViable }.count
            }
                        
            result += "<td>\(distinctSquads.count)</td>"

            for section in columns {
                let evaluations = squads[player, default:[:]][section, default: []]
                let totalGP = evaluations.reduce(0) { $0 + $1.squad.gp }
                
                result += "<td nowrap style=\"font-size:75%\" data-order=\"\(totalGP)\">"

                var first = true
                for squadEv in squads[player, default:[:]][section, default: []] {
                    if !first {
                        result += "<br/>"
                    }
                    let gp = squadEv.squad.gp.millions

                    result += "\(gp) - "
                    
                    result += squadEv.squad.description(gearBelow: 30, starsBelow: 7)
                    
                    first = false
                }
            }
            result += "</tr>\n"
        }
        
        result +=
            """
                </tbody>
                <tfoot>
                <tr>
                <td>Viable Count</td>
                <td>&nbsp;</td>
                \(columns.map { "<td>" + counts[$0, default:0].description + "</td>" }.joined())
                </tr>
                </tfoot>
            </table>
            <br/>
            """
        
        result += html.footer()
        
        return result
    }

}

struct TBSquadsByTypePage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/\(shortName)-squads-type.html" }

    let title: String
    let shortName: String

    let guild: Model.Guild
    let squads: [Model.Player: [TB.Key:[SquadEvaluation]]]
    let squadNames: [String]

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.toonCss
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "\(guild.name) \(title) Squads By Type")
        result += html.dataTable(id: "squads", fixedColumns: 1)
                
        var counts = [String:Int]()
        let squadNamesSet = Set(squadNames)
        
        result +=
            """
            <a href="\(shortName)-squads-section.html">Squads by Section</a>&nbsp;|&nbsp;
            <a href="\(shortName)-squads-type.html">Squads by Type</a>&nbsp;|&nbsp;
            <a href="\(shortName)-squads-chart.html">Squads Chart</a>
            <br/>
            <table id="squads" class="display" style="font-size:85%">
                <thead>
                    <tr>
                    <th>Player</th>
                    <th>Count</th>
                    \(squadNames.map { "<th align=\"center\">\($0)</th>" }.joined() )
                    </tr>
                </thead>
                <tbody>
            """
        
        for player in guild.players {
            result += "<tr>"
            result += "<td><a href=\"\(html.p(player, shortName))\">\(player.name)</a></td>"

            var viableSquadCount = 0
            
            var squadsByName = [String:SquadEvaluation]()
            
            let playerSquads = squads[player, default:[:]]
            for section in playerSquads.keys.sorted() {
                for squadEv in playerSquads[section, default:[]] {
                    let name = squadEv.squad.name
                    if squadsByName[name] == nil && squadNamesSet.contains(name) {
                        if squadEv.status.isViable {
                            viableSquadCount += 1
                            counts[name, default: 0] += 1
                        }
                        squadsByName[name] = squadEv
                    }
                }
            }
            
            result += "<td>\(viableSquadCount)</td>"

            for squadName in squadNames {
                if let squadEv = squadsByName[squadName] {
                    
                    let gp = squadEv.squad.gp.millions

                    result += "<td nowrap style=\"font-size:75%\" data-order=\"\(gp)\">"
                    result += "\(gp) - "
                    
                    result += squadEv.squad.description(gearBelow: 30, starsBelow: 7)
                    
                    result += "</td>"

                } else {
                    result += "<td data-order=\"0\"></td>"
                }
            }
            result += "</tr>\n"
        }
        
        result +=
            """
                </tbody>
                <tfoot>
                <tr>
                <td>Viable Count</td>
                <td>\(counts["TOTAL", default: 0])</td>
                \(squadNames.map { "<td>" + counts[$0, default:0].description + "</td>" }.joined())
                </tr>
                </tfoot>
            </table>
            <br/>
            """
        
        result += html.footer()
        
        return result
    }

}

struct TBSquadsChartPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/\(shortName)-squads-chart.html" }

    let title: String
    let shortName: String

    let guild: Model.Guild
    let squadNames: [String]

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss

        result += html.header(title: "\(guild.name) \(title) Squads Charts")
        result += html.chartInclude()
        
        result +=
            """
            <script>
            function displayChart(name) {
                d3.select("#chart").html("")
                c1 = new Chart("player-ds-geo-tb-squad-gp.json", null, name)
                c1.id = "chart"
                c1.title = "Guild TB Squads: " + name
                c1.tightScale = true
                c1.round = 1000
                c1.display()
            }
            displayChart("\(squadNames[0])")
            </script>
            
            <a href="\(shortName)-squads-section.html">Squads by Section</a>&nbsp;|&nbsp;
            <a href="\(shortName)-squads-type.html">Squads by Type</a>&nbsp;|&nbsp;
            <a href="\(shortName)-squads-chart.html">Squads Chart</a>
            <br/><br/>
            """

        for name in squadNames {
            result +=
                """
                <a href="#" onclick="displayChart('\(name)')">\(name)</a> |&nbsp;
                """
        }
        
        result +=
            """
            <br/><br/>
            <div id="chart"></div>
            """

        result += html.footer()
        
        return result
    }

}

struct GuildChartsPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/guild-charts.html" }

    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss

        result += html.header(title: "\(guild.name) Charts")
        result += html.chartInclude()
        
        let squadNames = Set(squads.flatMap { $0.value }.map { $0.squad.name }).sorted()
        
        result +=
            """
            <script>
            c1 = new Chart("guild-gp-stats.json", null, "total")
            c1.id = "guild-gp"
            c1.title = "Guild GP"
            c1.tightScale = true
            c1.round = 1000
            c1.height = 300
            c1.width = 600
            c1.display()

            c2 = new Chart("guild-counts.json", null, "players")
            c2.id = "guild-players"
            c2.title = "Player Count"
            c2.height = 300
            c2.width = 600
            c2.display()

            c3 = new Chart("player-gp-stats.json", null, "total")
            c3.id = "player-gp"
            c3.title = "Player GP"
            c3.tightScale = true
            c3.round = 1000
            c3.height = 800
            c3.width = 1200
            c3.display()

            c4 = new Chart("guild-counts.json", null, "Zetas")
            c4.id = "zetas"
            c4.title = "Guild Zetas"
            c4.height = 300
            c4.width = 600
            c4.display()

            c5 = new Chart("guild-counts.json", null, "G12+")
            c5.id = "g12"
            c5.title = "Guild G12+"
            c5.height = 300
            c5.width = 600
            c5.display()
                
            c51 = new Chart("guild-counts.json", null, "G13+")
            c51.id = "g13"
            c51.title = "Guild G13+"
            c51.height = 300
            c51.width = 600
            c51.display()

            c6 = new Chart("player-counts.json", null, "Zetas")
            c6.id = "pzetas"
            c6.title = "Player Zetas"
            c6.height = 500
            c6.width = 1200
            c6.display()

            c7 = new Chart("player-counts.json", null, "G12+")
            c7.id = "pg12"
            c7.title = "Player G12+"
            c7.height = 500
            c7.width = 600
            c7.display()

            c71 = new Chart("player-counts.json", null, "G13+")
            c71.id = "pg13"
            c71.title = "Player G13+"
            c71.height = 500
            c71.width = 600
            c71.display()

            c8 = new Chart("guild-important-toon-count.json", "hope", null)
            c8.id = "important"
            c8.title = "Guild Important Toon Counts"
            c8.height = 500
            c8.width = 1200
            c8.display()

            function displayImportantToons(name) {
                d3.select("#pimportant").html("")
                c9 = new Chart("player-important-toon-gp.json", null, name)
                c9.id = "pimportant"
                c9.title = "Player Toons GP: " + name
                c9.tightScale = true
                c9.round = 1000
                c9.width = 1200
                c9.display()
            }

            function displaySquads(name) {
                d3.select("#squads").html("")
                c10 = new Chart("player-squad-gp.json", null, name)
                c10.id = "squads"
                c10.title = "Player Squad GP: " + name
                c10.tightScale = true
                c10.round = 1000
                c10.width = 1200
                c10.display()
            }
            """
        
        result += "\n\ndisplayImportantToons('\(ImportantToonsPage.importantToons.sorted()[0])')\n"
        result += "displaySquads('\(squadNames[0])')\n"

        result +=
            """
            </script>
            <br/><br/>
            <div id="guild-gp" style="float: left"></div>
            <br clear="all"/>

            <div id="guild-players" style="float: left"></div>
            <br clear="all"/>

            <div id="player-gp" style="float: left"></div>

            <br clear="all"/>
            <div id="zetas" style="float: left"></div>

            <br clear="all"/>
            <div id="g12" style="float: left"></div>
            <br clear="all"/>
            <div id="g13" style="float: left"></div>

            <br clear="all"/>
            <div id="pzetas" style="float: left"></div>

            <br clear="all"/>
            <div id="pg12" style="float: left"></div>
            <br clear="all"/>
            <div id="pg13" style="float: left"></div>

            <br clear="all"/>
            <div id="important" style="float: left"></div>
            
            <br clear="all"/>
            """
        
        for name in ImportantToonsPage.importantToons.sorted() {
            let commonName = Model.Character.find(name).commonName
            result +=
                """
                <a href="#pimportant" onclick="displayImportantToons('\(commonName)')">\(name)</a> |&nbsp;
                """
        }
        
        result +=
            """
            <div id="pimportant" style="float: left"></div>

            <br clear="all"/>
            """
        
        for name in squadNames {
            if name.contains("Counter") {
                continue
            }
            result +=
                """
                <a href="#squads" onclick="displaySquads('\(name)')">\(name)</a> |&nbsp;
                """
        }

        result +=
            """
            <div id="squads" style="float: left"></div>

            <br clear="all"/>
            """

        result += html.footer()
        
        return result
    }

}

//MARK:-

struct GuildGPChart : ChartGenerator {
    
    var path: String { return "guild/\(guild.shortName)/guild-gp-stats.json" }
    let yAxisLabel = "gp"
    let valueKeys = [ "ships", "toons", "total" ]
    var keys: [ChartKey] { return [guild] }
    
    let guild: Model.Guild
    
    func build(chart: inout ChartData) {
        chart[guild].append(valueKey: "ships", value: guild.shipGP)
        chart[guild].append(valueKey: "toons", value: guild.characterGP)
        chart[guild].append(valueKey: "total", value: guild.info.gp)
    }
}

struct GuildPlayerGPChart : ChartGenerator {
    
    var path: String { return "guild/\(guild.shortName)/player-gp-stats.json" }
    let yAxisLabel = "gp"
    let valueKeys = [ "ships", "toons", "total" ]
    var keys: [ChartKey] { return guild.players }

    let guild: Model.Guild

    func build(chart: inout ChartData) {
        for player in guild.players {
            chart[player].append(valueKey: "ships", value: player.stats.shipGP)
            chart[player].append(valueKey: "toons", value: player.stats.characterGP)
            chart[player].append(valueKey: "total", value: player.stats.gp)
        }
    }
    
}

struct GuildPlayerCountsChart : ChartGenerator {
    
    var path: String { return "guild/\(guild.shortName)/player-counts.json" }
    let yAxisLabel = "count"
    var valueKeys: [String] { return StatsRowExtractors.guildCountStatsExtractors.map { $0.name } }
    var keys: [ChartKey] { return guild.players }

    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]

    func build(chart: inout ChartData) {
        let rowFactory = StatsRowFactory(extractors: StatsRowExtractors.guildCountStatsExtractors)

        for player in guild.players {
            if let evaluations = squads[player] {
                let row = rowFactory.newRow(guild: guild, player: player, evaluations: evaluations)
                for (index, value) in row.values.enumerated() {
                    let extractor = StatsRowExtractors.guildCountStatsExtractors[index]
                    chart[player].append(valueKey: extractor.name, value: value)
                }
            }
        }
    }
}

struct GuildCountsChart : ChartGenerator {
    
    var path: String { return "guild/\(guild.shortName)/guild-counts.json" }
    let yAxisLabel = "count"
    var valueKeys: [String] {
        var result = [ "players" ]
        result.append(contentsOf: StatsRowExtractors.guildCountStatsExtractors.map { $0.name })
        return result
    }
    var keys: [ChartKey] { return [guild] }

    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]

    func build(chart: inout ChartData) {
        chart[guild].append(valueKey: "players", value: guild.players.count)
                
        let rowFactory = StatsRowFactory(extractors: StatsRowExtractors.guildCountStatsExtractors)
        var sum = rowFactory.emptyRow()

        for player in guild.players {
            if let evaluations = squads[player] {
                let row = rowFactory.newRow(guild: guild, player: player, evaluations: evaluations)
                sum.add(row: row, rowFactory: rowFactory)
            }
        }
        sum.finishAdd(rowFactory: rowFactory)
        
        for (index, value) in sum.values.enumerated() {
            let extractor = StatsRowExtractors.guildCountStatsExtractors[index]
            chart[guild].append(valueKey: extractor.name, value: value)
        }
    }
}

struct GuildImportantToonsCountChart : ChartGenerator {
    
    var path: String { return "guild/\(guild.shortName)/guild-important-toon-count.json" }
    let yAxisLabel = "count"
    var valueKeys: [String] {
        return ImportantToonsPage.importantToons.map { Model.Character.find($0).commonName }
    }
    var keys: [ChartKey] { return [guild] }

    let guild: Model.Guild

    func build(chart: inout ChartData) {
        var counts = [String:Int]()
        let importantToons = ImportantToonsPage.importantToons
        for player in guild.players {
            for name in importantToons {
                let id = Model.Character.find(name).id
                if let unit = player.units[id], unit.gear >= 10 {
                    counts[name, default: 0] += 1
                }
            }
        }
        
        for name in importantToons {
            chart[guild].append(valueKey: name, value: counts[name, default: 0])
        }
    }
}

struct GuildImportantToonsGPChart : ChartGenerator {
    
    var path: String { return "guild/\(guild.shortName)/player-important-toon-gp.json" }
    let yAxisLabel = "gp"
    var valueKeys: [String] {
        return ImportantToonsPage.importantToons.map { Model.Character.find($0).commonName }
    }
    var keys: [ChartKey] { return guild.players }

    let guild: Model.Guild

    func build(chart: inout ChartData) {
        let importantToons = ImportantToonsPage.importantToons
        for player in guild.players {
            for name in importantToons {
                let id = Model.Character.find(name).id
                if let unit = player.units[id] {
                    chart[player].append(valueKey: unit.commonName, value: unit.gp)
                }
            }
        }
    }
}

struct GuildSquadsCountChart : ChartGenerator {
    
    var path: String { return "guild/\(guild.shortName)/guild-squad-counts.json" }
    let yAxisLabel = "count"
    
    var valueKeys: [String] {
        var squadNamesSet = Set<String>()
        for evaluations in squads.values {
            squadNamesSet.formUnion(evaluations.compactMap({ $0.squad.name }))
        }
        return Array(squadNamesSet)
    }
    var keys: [ChartKey] { return [guild] }

    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]

    func build(chart: inout ChartData) {
        let squadNames = self.valueKeys

        var counts = [String:Int]()
        for player in guild.players {
            for evaluation in squads[player, default:[]] {
                if evaluation.status.isViable {
                    counts[evaluation.squad.name, default: 0] += 1
                }
            }
        }
        
        for name in squadNames {
            chart[guild].append(valueKey: name, value: counts[name, default: 0])
        }
    }
}

struct GuildTBSquadsCountChart : ChartGenerator {
    
    var path: String { return "guild/\(guild.shortName)/guild-ds-geo-tb-squad-count.json" }
    let yAxisLabel = "count"
    
    var valueKeys: [String] {
        var squadNamesSet = Set<String>()
        for map in squads.values {
            for (_, evaluations) in map {
                squadNamesSet.formUnion(evaluations.map { $0.squad.name })
            }
        }
        return Array(squadNamesSet)
    }
    var keys: [ChartKey] { return [guild] }

    let guild: Model.Guild
    let squads: [Model.Player: [TB.Key:[SquadEvaluation]]]
    
    func build(chart: inout ChartData) {
        let squadNames = self.valueKeys
        var counts = [String:Int]()
        
        for player in guild.players {
            
            let playerSquads = squads[player, default:[:]]
            for section in playerSquads.keys.sorted() {
                for squadEv in playerSquads[section, default:[]] {
                    if squadEv.status.isViable {
                        let name = squadEv.squad.name
                        counts[name, default: 0] += 1
                    }
                }
            }
        }
        
        for name in squadNames {
            chart[guild].append(valueKey: name, value: counts[name, default: 0])
        }
    }
}


struct GuildSquadsGPChart : ChartGenerator {
    
    var path: String { return "guild/\(guild.shortName)/player-squad-gp.json" }
    let yAxisLabel = "gp"
    
    var valueKeys: [String] {
        var squadNamesSet = Set<String>()
        for evaluations in squads.values {
            squadNamesSet.formUnion(evaluations.compactMap({ $0.squad.name }))
        }
        return Array(squadNamesSet)
    }
    var keys: [ChartKey] { return guild.players }

    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]

    func build(chart: inout ChartData) {
        for player in guild.players {
            // a player might have two BH squads -- we only want to record the highest
            var tally = [String:Int]()
            for evaluation in squads[player, default: []] {
                if evaluation.squad.gp > tally[evaluation.squad.name, default: 0] {
                    tally[evaluation.squad.name] = evaluation.squad.gp
                }
            }
            for (key, value) in tally {
                chart[player].append(valueKey: key, value: value)
            }
        }
    }
}

// TODO
struct GuildTBSquadsGPChart : ChartGenerator {
    
    var path: String { return "guild/\(guild.shortName)/\(chartName).json" }
    let yAxisLabel = "gp"
    let chartName: String
    
    var valueKeys: [String] {
        var squadNamesSet = Set<String>()
        for map in squads.values {
            for (_, evaluations) in map {
                squadNamesSet.formUnion(evaluations.map { $0.squad.name })
            }
        }
        return Array(squadNamesSet)
    }
    var keys: [ChartKey] { return guild.players }

    let guild: Model.Guild
    let squads: [Model.Player: [TB.Key:[SquadEvaluation]]]
    
    func build(chart: inout ChartData) {
        for player in guild.players {
            var squadsByName = [String:SquadEvaluation]()
            
            let playerSquads = squads[player, default:[:]]
            for section in playerSquads.keys.sorted() {
                for squadEv in playerSquads[section, default:[]] {
                    let name = squadEv.squad.name
                    if squadsByName[name] == nil {
                        squadsByName[name] = squadEv
                    }
                }
            }
            
            for (name, squadEv) in squadsByName {
                chart[player].append(valueKey: name, value: squadEv.squad.gp)
            }
        }
    }
}
