//
//  Characters.swift
//  swgoh-tool
//
//  Created by David Koski on 1/26/21.
//  Copyright © 2021 David Koski. All rights reserved.
//

import SWGOH

extension Sequence where Element == String {
    func comma() -> String {
        self.joined(separator: ", ")
    }
}

extension Sequence where Element: AdditiveArithmetic {
    
    func cumulative() -> [Element] {
        var sum = Element.zero
        return self.map { v in
            sum += v
            return sum
        }
    }
    
}

struct CharactersPages : PageBuilder {

    var path: String { return "c/\(character.id).html" }

    let character: Model.Character
    let guilds: [Model.Guild]
    
    func showPriorities(_ priorities: [ModHistogram.Priority], counts: Bool = false) -> HTMLFramgment {
        H.block {
            H.foreach(priorities.enumerated()) { (index, priority) in
                if index > 0 {
                    ", "
                }
                priority.c.description
                
                if counts {
                    "(\(priority.v))"
                }
            }
        }
    }

    func showStats(_ stats: [ModHistogram.Value]) -> HTMLFramgment {
        H.block {
            H.foreach(stats.enumerated()) { (index, stat) in
                if index > 0 {
                    ", "
                }
                stat.c.description
                " +\(stat.v)"
            }
        }
    }
    
    func showMod(mod: Model.Mod, priorities: Set<Model.Mod.Characteristic>) -> HTMLFramgment {
        H.div().class("mod") {
            if priorities.contains(mod.characteristic) {
                "<img class=\"mod-set priority m\(mod.set)\" />"
            } else {
                "<img class=\"mod-set m\(mod.set)\" />"
            }
            
            if priorities.contains(mod.primaryStat.characteristic) {
                H.b { mod.primaryStat.description }
            } else {
                mod.primaryStat.description
            }
            
            H.ul {
                H.foreach(mod.secondaryStat) { stat in
                    H.li {
                        if priorities.contains(stat.characteristic) {
                            H.b { stat.description }
                        } else {
                            stat.description
                        }
                        if let roll = stat.roll {
                            " (\(roll))"
                        }
                    }
                }
            }
        }
    }
    
    func showAttributeHistograms(id: String, attributeHistograms: [ModRecommendation.AttributeHistogram], title: String? = nil) -> HTMLFramgment {
        
        func axis(_ c: Model.Mod.Characteristic) -> String? {
            switch c {
            case .health, .protection: return "x2"
            case .offense: return "x3"
            case .defense, .cc: return "x4"
            default: return nil
            }
        }
        
        func skip(_ c: Model.Mod.Characteristic) -> Bool {
            switch c {
            // these graphs are just 1 or 2 points
            case .cd, .acc, .ca: return true
            default: return false
            }
        }
        
        func extract(attributeHistogram: ModRecommendation.AttributeHistogram) -> [(Int, Int, Double)] {
            let bin = attributeHistogram.binSize
            let histogram = attributeHistogram.histogram
            
            let range = stride(from: max(histogram.keys.min() ?? 0 - bin, 0), through: (histogram.keys.max() ?? 0 + bin), by: bin)
            
            var result = [(Int, Int, Double)]()
            var previousWasZero = true
            for statValue in range {
                let rawValue = histogram[statValue]
                let percentDouble = Double(rawValue) / Double(histogram.count)
                let percentValue = Int(Double(rawValue) / Double(histogram.count) * 100)
                
                if percentValue == 0 && previousWasZero {
                    // skip repeated zeros
                } else {
                    if previousWasZero && statValue != 0 {
                        result.append((statValue - bin, 0, 0))
                    }
                    previousWasZero = percentValue == 0
                    result.append((statValue, percentValue, percentDouble))
                }
            }
            
            return result
        }
        
        let chart = H.block {
            H.div().id(id).style("height:500px") { }

            """
            <script>
            {
            let traces = [];
            """
            
            H.foreach(attributeHistograms) { ah in
                let c = ah.characteristic
                let data = extract(attributeHistogram: ah)
                if !skip(c) && !data.isEmpty {
                    """
                    traces.push({
                        name: "\(c)",
                        x: [ \(data.map { $0.0.description }.comma()) ],
                        y: [ \(data.map { $0.1.description }.comma()) ],
                        text: [ \(data.map { $0.2 }.cumulative().map { round($0 * 100).description}.comma()) ],
                        hovertemplate: '%{y}% of players have +%{x} \(c)<br>%{text}% of players have +%{x} or less<br>\(c) range: +\(data[0].0) - +\(data.last!.0)',
                    """
                    
                    if let axis = axis(c) {
                        """
                        xaxis: "\(axis)",
                        """
                    }
                    
                    """
                    });
                    """
                }
            }
            
            let title = title ?? "Attribute distribution for R5 unit prioritizing \(attributeHistograms.map { $0.characteristic.description}.comma())."
            
            """
            let layout = {
                  title: "\(title)",
                  hovermode: "closest",
                  yaxis: {
                    title: '% of players',
                  },
                  xaxis: {
                      zeroline: false,
                      showline: false,
                      autotick: true,
                      ticks: '',
                      showticklabels: false
                  },
                  xaxis2: {
                      overlaying: 'x',
                      zeroline: false,
                      showline: false,
                      autotick: true,
                      showgrid: false,
                      ticks: '',
                      showticklabels: false
                  },
                  xaxis3: {
                      overlaying: 'x',
                      zeroline: false,
                      showline: false,
                      autotick: true,
                      showgrid: false,
                      ticks: '',
                      showticklabels: false
                  },
                  xaxis4: {
                      overlaying: 'x',
                      zeroline: false,
                      showline: false,
                      autotick: true,
                      showgrid: false,
                      ticks: '',
                      showticklabels: false
                  },
                  margin: {
                      t: 60,
                      b: 0,
                  }
            }
            Plotly.newPlot('\(id)', traces, layout)
            }
            </script>
            """
        }

        return chart
    }
    
    func showMods(mods: [Model.Mod], priorities: Set<Model.Mod.Characteristic>) -> HTMLFramgment {
        func find(_ index: Int) -> Model.Mod {
            mods.first { $0.slot == index }!
        }
                
        return H.table().class("mods") {
            H.foreach(stride(from: 1, to: 6, by: 2)) { index in
                H.tr {
                    H.td().class("mod-cell") {
                        showMod(mod: find(index), priorities: priorities)
                    }
                    H.td().class("mod-cell") {
                        showMod(mod: find(index + 1), priorities: priorities)
                    }
                }
            }
        }
    }

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        
        result += html.sortableTableCSS
        result += html.dataTable(id: "zetas", search: false)
        result += html.dataTable(id: "gearLevels", search: false)
        
        result += html.plotlyInclude()
        
        result +=
            """
            <link rel="stylesheet" type="text/css" href="\(html.url("mods.css"))">
            <style>
            #zetas {
                width: 800;
                margin-left: 10;
            }
            #gearLevels {
                width: 400;
                margin-left: 10;
            }
            #picture {
                float: right;
            }
            table.mods {
                margin-top: 5px;
            }
            td.examples {
                padding: 10px;
                font-size: 90%;
                width: 430px;
            }
            .mod-set {
                width: 24px;
                height: 24px;
            }
            .priority {
                border: 1px solid black;
                margin: 2px;
                margin-right: 10px;
                background-color: #c0c0c0;
            }
            .mod-cell {
                border: 1px solid black;
                padding: 2px;
                font-size: 85%;
            }
            </style>
            """


        result += html.header(title: "Alliance Character Data: \(character.name)", nav: AllianceSite.navigation(html).render())
        
        let allUnits = guilds
            .flatMap { $0.players }
            .compactMap { player -> (Model.Player, Model.Unit)? in
                if let unit = player.units[character.id] {
                    return ( player, unit )
                } else {
                    return nil
                }
            }
        
        var countByGearLevel = CountedSet<Int>()
        for (_, unit) in allUnits {
            countByGearLevel.add(unit.combinedGearLevel.value)
        }
        var zetaCounts = CountedSet<String>()
        for (_, unit) in allUnits {
            for zeta in unit.zetaIds {
                zetaCounts.add(zeta)
            }
        }
        
        func q<V>(_ value: V) -> String {
            "\"\(value)\""
        }
        
        result += H.block {
            H.h1 {
                H.img(src: character.imageURL).id("picture")
                
                "Alliance Character Data: \(character.name)"
            }
            
            H.h2 {
                "swgoh.gg data"
            }
            
            H.ul {
                H.li { H.a(href: ExternalPage.character(character).urlString) { "abilities / character data" } }
                H.li { H.a(href: ExternalPage.characterMeta(character).urlString) { "character meta report" } }
                H.li { H.a(href: ExternalPage.modMeta(character).urlString) { "mods meta report" } }
                H.li { H.a(href: ExternalPage.statsMeta(character).urlString) { "stats meta report" } }
            }
            
            H.h2 {
                "Mod Data"
            }
            
            if let modRecommendations = referenceRecommendations[character.id] {
                H.h3 {
                    "Mod Stat Distributions"
                }
                
                """
                Mod stat distributions across \(modRecommendations.totalCount) units from the
                alliance and top guilds.  The bonuses are shown as applied to an R5 unit.
                Specific distributions are shown below by common mod configurations.
                """
                
                showAttributeHistograms(id: "mc-all", attributeHistograms: modRecommendations.attributeHistograms, title: "")
            }
            
            if let modRecommendations = modRecommondations(id: character.id), !modRecommendations.isEmpty {
                H.h3 {
                    "Mod Recommendations (External)"
                }
                
                H.ul {
                    H.foreach(modRecommendations) { url in
                        H.li { H.a(href: url.description) { url.description } }
                    }
                }
            }
            
            if let modRecommendations = referenceRecommendations[character.id] {
                H.h3 {
                    "Mod Recommendations (Alliance + Top Guilds)"
                }
                
                """
                <p>
                Based on \(modRecommendations.totalCount) units.  This shows the priorities
                used in assigning mods to the characters.  For example: <b>speed, offense</b>
                would prefer speed over all else and then offense next.  These are not
                necessarily good mod setups, but they are commonly used.

                <p>
                The example stats are shown for these mods being applied to an R5 character.
                """
                
                H.foreach(modRecommendations.clusters.enumerated()) { (index, cluster) in
                    let prioritiesSet = Set(cluster.priorities.map { $0.c })
                    
                    H.h4().id("mod\(index)") {
                        "#\(index + 1) - \(NumberFormatter.percent.string(from: Double(cluster.count) / Double(modRecommendations.totalCount))) - "
                        showPriorities(cluster.priorities)
                    }
                    
                    H.table() {
                        H.tr {
                            H.td().class("examples") {
                                H.b { "Typical Mods: " }
                                "<br/>"
                                showStats(cluster.center.stats)
                                showMods(mods: cluster.center.mods, priorities: prioritiesSet)
                            }
                            H.td().class("examples") {
                                H.b { "Great Mods: " }
                                "<br/>"
                                showStats(cluster.maximal.stats)
                                showMods(mods: cluster.maximal.mods, priorities: prioritiesSet)
                            }
                        }
                    }
                    
                    showAttributeHistograms(id: "mc-\(index)", attributeHistograms: cluster.attributeHistograms)
                }
            }

            
            H.h2 {
                "Alliance Demographic Data"
            }
            
            "Based on \(allUnits.count) units."
            
            if character.abilities.contains(where: { $0.isZeta }) {
                H.table().id("zetas") {
                    H.thead {
                        H.tr {
                            H.th { "Name" }
                            H.th { "Description" }
                            H.th { "Count" }
                            H.th { "Percent" }
                        }
                    }
                    H.foreach(zetaCounts.keys.sorted()) { zeta in
                        H.tr {
                            H.td().order(zeta) {
                                H.img(src: "https://swgoh.gg" + Model.Ability.abilities[zeta]!.image ).style("width: 30px")
                                " "
                                Model.Ability.abilities[zeta]!.name
                            }
                            H.td {
                                Model.Ability.abilities[zeta]!.zetaDescription ?? ""
                            }
                            H.td {
                                zetaCounts[zeta]
                            }
                            H.td {
                                NumberFormatter.percent.string(from: (Double(zetaCounts[zeta]) / Double(allUnits.count)))
                            }
                        }
                    }
                }
                
                "<br/><br/>"
            }
            
            H.div().id("gearLevels-chart").style("width:80%;height:800px") { }
            
            """
            <script>
            let trace = {
                y: [ \(Model.GearLevel.levels.map { countByGearLevel[$0].description }.comma() ) ],
                type: "bar",
                name: "Number of Players"
            }
            let trace2 = {
                y: [ \(Model.GearLevel.levels.map { Double(countByGearLevel[$0]) / Double(allUnits.count) * 100.0 }.cumulative().map { $0.description }.comma() ) ],
                yaxis: "y2",
                type: "line",
                name: "Cumulative % Players"
            }
            let layout = {
                title: "Gear Levels",
                xaxis: {
                    tickvals: [ \((0 ..< Model.GearLevel.levels.count).map { $0.description }.comma()) ],
                    ticktext: [ \(Model.GearLevel.gearLevels.map { q($0)}.comma()) ]
                },
                yaxis: { title: "Number of Players" },
                yaxis2: {
                    title: "Cumulative % Players",
                    overlaying: "y",
                    side: "right"
                }
            }
            Plotly.newPlot('gearLevels-chart', [trace, trace2], layout)
            </script>
            """
            
        }.render()
        
        result += html.footer()
        
        return result
    }

}
