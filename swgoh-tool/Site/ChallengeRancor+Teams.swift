//
//  Copyright © 2021 David Koski. All rights reserved.
//

import SWGOH

struct ChallengeRancor {
    
    struct PhaseInfo {
        let id: String
        let name: String
        let squads: SquadBuilder
        
        let intro: String
        
        let teams: [String:TeamInfo]
        
        /// provides custom logic for team play order -- these are checked first
        let customPlayOrder: (Model.Player, [SquadEvaluation]) -> [String]
        
        init(id: String, name: String, squads: SquadBuilder, intro: String = "", teams: [ChallengeRancor.TeamInfo] = [TeamInfo](), customPlayOrder: @escaping (Model.Player, [SquadEvaluation]) -> [String] = { _,_ in [] }) {
            self.id = id
            self.name = name
            self.squads = squads
            self.intro = intro
            self.teams = Dictionary(uniqueKeysWithValues: teams.map { ($0.id, $0) })
            self.customPlayOrder = customPlayOrder
        }
    }
    
    struct TeamInfo {
        var id: String
        var name: String
        
        var marker: String = ""
        var order: Int? = nil
        
        var body: String? = nil
        var videoURL: String? = nil
        
        var needsPage: Bool {
            body != nil
        }
        
        var link: String? {
            if body != nil {
                return "alliance/crancor/\(id).html"
            } else if videoURL != nil {
                return videoURL
            }
            return nil
        }
    }
    
    struct Info {
        var intro =
            """
            <p>
            The raid has new rules and some new teams -- information from before April 2021 is suspect, see <a href="https://www.youtube.com/watch?v=4bko-AMKdco">this video</a> and <a href="https://www.youtube.com/watch?v=1xaqCbK7EOU&list=PLV3ihsc-3CokKvROCKUnZjwd5nC_Zcqv6">Skelturix collection</a> for more information.  The teams below give damage estimates from the updated teams or direct measurement.  If the team is marked with <i>(?)</i> it means that I have no information on its ability with the new rules.
            <p>
            Note that the damage estimates are used to prioritize teams, give planners a rough estimate of available damage and give players something to shoot for.  These numbers are usually at the high end of what you will get, but if you consistently fall short, you should ask your guild mates about how to play it!
            <br/>
            <br/>
            Note: some teams may overlap between phases.
            <br/>
            <br/>
            Teams shown in <span style="background-color: yellow;">yellow</span> need additional gear.
            Counts are shown with <i># ready / # not ready</i>.  You can
            <button onclick="$('.not-viable').text('&nbsp;').css('background-color', '')">remove not ready</button><br/>
            """
        
        var summary =
            """
            Note that there may be overlap between the teams in each phase.  Numbers are ready / need gear.
            """
        
        var phases = [
            PhaseInfo(id: "p1", name: "P1", squads: p1),
            PhaseInfo(id: "p23", name: "P2/3", squads: p23),
            PhaseInfo(id: "p4", name: "P4", squads: p4),
        ]
    }
    
    static let rules = [
        "DEFAULT" : TWRule(gear: 25).e,
    ]
    
    // https://www.reddit.com/r/SWGalaxyOfHeroes/comments/kghn5v/rancor_challenge_teams_updated/
        
    /*
     
     https://www.youtube.com/watch?v=4bko-AMKdco
     
     P1:
     ultKylo r7, Hux, KRU, Wat, Thrawn ~ 14%
     Vader, KRU, Wat, C3PO, Shaak/BB8 ~ 10%
     Rey Rex, C3P0, Hoda, Fives ~7%
     Padme, GK, JKA, AT, C3P0/R2
     GAS 501st

     P2/3:
     Vader, Wat, Thrawn, Traya, Nute ~11%
     SLKR, Hux, Thrawn, Daka, Zombie ~10%
     SLKR, Hux, ST, KRU, Thrawn ~6%
     Veers, Piett, Starck, Gideon, DarkT ~4-6%
     Veers, Piett, Starck, Range, Death ~4-6%
     JKL/JKR, GAS, JKA, GK, Hoda ~4-7%
     Shaak 501st, ~5-7%
     Padme ~2%
     Aurra BH ~2%
     GAS 501 ~2%
     Sith Empire ~2%

     P4:
     GAS, Rey, Echo, Hoda, Fives ~4%

     */

    
    // MARK:- Phase 1
        
    static let reyClones1 = SelectUnitSquadBuilder(properties: TWProperties(name: "Rey+Clones 7%", preference: 7, rules: rules), u("Rey"), u("CT-5555"), u("Rex"), u("C-3PO"), u("Hyoda"))
    
    // not used for P1
    static let slkr1NS = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR+NS (?)", preference: 5, rules: rules), u("SL Kylo"), u("Hux"), u("Sith Trooper", "Hyoda"), u("Daka"), u("Zombie"))
    
    // dkoski: 3.6m
    static let slkrMore1 = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR 8%", id: "SLKR1", preference: 8, rules: rules), u("SL Kylo"), u("Hux"), u("Sith Trooper"), u("KRU"), u("FOST"))
    
    // https://www.youtube.com/watch?v=RwDnhhMZEb8&feature=youtu.be
    // https://www.youtube.com/watch?v=4bko-AMKdco -- crancor2.0 14%
    // Note: preference is highest among SLKR teams, but less than Vader (which shares a lot of units)
    static let sklrWat1 = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR+Wat (14%)", id: "SLKR-WAT1", preference: 9, rules: rules), u("SL Kylo"), u("Hux"), u("Wat"), u("KRU"), u("Thrawn"))
    
    // dkoski: 1.6
    static let padme1 = SelectUnitSquadBuilder(properties: TWProperties(name: "Padme 3%", id: "PADME1", preference: 3, rules: padmeRules), u("Padme"), u("JKA"), u("GK"), u("Ahsoka"), u("GMY", "C-3PO", "R2-D2"))
    static let padme1b = SelectUnitSquadBuilder(properties: TWProperties(name: "Padme-b 2%", id: "PADME1", preference: 2, rules: padmeRules), u("Padme"), u("JKA"), u("Clone Sergeant"), u("R2-D2"), u("Barriss"))

    static let padmeRules = [
        "DEFAULT" : TWRule(gear: 25).e,
        // prefer GMY -- C3PO is used in other teams
        "GMY" : TWRule(gear: 25, preference: 1).e,
        "Barriss": TWRule(uniqueZetas: true, gear: 25).e,
    ]


    // not used for P1
    static let gas1 = SelectUnitSquadBuilder(properties: TWProperties(name: "GAS 5%", preference: 5, rules: gasRules), u("GAS"), u("CT7567"), u("CT210408"), u("CT5555"), u("Ahsoka Tano", "ARCTROOPER501ST"))
    
    static let gasRules = [
        "DEFAULT" : TWRule(gear: 25).e,
        "Ahsoka Tano" : TWRule(gear: 25, preference: -3).e,
    ]
    
    // https://www.youtube.com/watch?v=1xaqCbK7EOU
    static let vader1 = SelectUnitSquadBuilder(properties: TWProperties(name: "Vader P1 10%", id: "VADER1", preference: 10, rules: vader1Rules), u("Darth Vader"), u("KRU"), u("Wat"), u("C-3PO"), u("Shaak Ti", "BB-8"))
    
    static let vader1Rules = [
        "DEFAULT" : TWRule(gear: 25).e,
        // prefer BB8 over Shaak if available -- we can use Shaak in P2/3 with clones
        "BB-8" : TWRule(gear: 25, preference: 1).e,
    ]

    
    // https://www.youtube.com/watch?v=4yhkm-CibcM
    static let sithAddIds = Model.Character.matchesCategory("Sith").map(\.id)
    static let see1 = SelectUnitSquadBuilder(properties: TWProperties(name: "SEE P1 5%", id: "SEE1", preference: 5, rules: rules), u("Sith Eternal Emperor"), u("Malak"), u(sithAddIds), u(sithAddIds), u(sithAddIds))

    // https://www.youtube.com/watch?v=LJZJmeq0Tfg
    static let drMalak1 = SelectUnitSquadBuilder(properties: TWProperties(name: "DR/Malak 3%", id: "DRMALAK1", preference: 3, rules: rules), u("Darth Revan"), u("Malak"), u("BSF"), u("HK-47"), u("Sith Marauder"))

    
    static let p1 = MultipleSquadBuilder(reyClones1, slkrMore1, sklrWat1, padme1, padme1b, vader1, see1, drMalak1)
    
    // MARK:- Phase 2, 3
    
    static let slkrNS2 = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR+NS 11%", id: "SLKRNS23", preference: 11, rules: slkr2Rules), u("SL Kylo"), u("Zombie"), u("Daka"), u("Hux"), u("Wat", "Thrawn", "Hyoda"))
    
    static let slkr2 = SelectUnitSquadBuilder(properties: TWProperties(name: "SLKR+FO 9%", id: "SLKR23", preference: 9, rules: slkr2Rules), u("SL Kylo"), u("Hux"), u("FOST"), u("Sith Trooper"), u("Thrawn"))
    
    static let slkr2Rules = [
        "DEFAULT" : TWRule(gear: 25).e,
        "Thrawn" : TWRule(gear: 25, preference: 1).e,
    ]
    
    // not sure on the 8%
    static let vader2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Vader+Wat 8%", preference: 8, rules: rules), u("Darth Vader"), u("EP"), u("Thrawn"), u("Wat"), u("Malak", "Piett", "Starck"))
    
    // https://www.youtube.com/watch?v=A-HRcTs7Ok0
    static let vaderNute2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Vader+Nute 12%", id: "VADER-NUTE2", preference: 12, rules: rules), u("Darth Vader"), u("Nute"), u("Thrawn"), u("Wat"), u("Traya"))
    
    // not used
    static let padme2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Padme 2%", preference: 2, rules: rules), u("Padme"), u("JKA"), u("GK"), u("Ahsoka"), u("GMY", "C-3PO", "R2-D2"))

    static let bh2 = SelectUnitSquadBuilder(properties: TWProperties(name: "BH 2%", preference: 2, rules: rules), u("Aurra"), u("Greef"), u("Bossk"), u("Jango"), u("Kuiil", "Boba Fett", "The Mandalorian"))

    static let shaak2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Shaak 6%", id: "SHAAK23", preference: 6, rules: rules), u("Shaak Ti"), u("CT-5555"), u("Rex"), u("CT210408"), u("Arc Trooper"))

    // not used
    static let gas2 = SelectUnitSquadBuilder(properties: TWProperties(name: "GAS 2%", preference: 2, rules: rules), u("GAS"), u("CT-5555"), u("Rex"), u("CT210408"), u("C-3PO", "Arc Trooper"))

    static let jedi2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Jedi 5%", preference: 5, rules: rules), u("JKR", "JKL"), u("JKA"), u("GAS"), u("GK"), u("Hyoda"))
    
    static let sithEmpire2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Sith Empire 2%", preference: 2, rules: rules), u("DR"), u("Malak"), u("BSF"), u("HK-47"), u("Sith Marauder", "Sith Empire Trooper", "Sith Assassin"))

    static let imperialTroopers12 = SelectUnitSquadBuilder(properties: TWProperties(name: "Imperial Troopers 5%", id: "TROOPERS23", preference: 5, rules: troopersRules), u("Veers"), u("Piett"), u("Gideon", "Starck"), u("Range", "Starck"), u("Dark Trooper", "Starck"))

    static let troopersRules = [
        "DEFAULT" : TWRule(gear: 25).e,
        "Starck" : TWRule(gear: 25, preference: -1).e,
    ]


    static let jkrJMLS2 = SelectUnitSquadBuilder(properties: TWProperties(name: "JKR+JMLS (?)", preference: 0, rules: rules), u("JKR"), u("JMLS"), u("JKL"), u("GAS"), u("GMY", "Hyoda"))

    static let jklJMLS2 = SelectUnitSquadBuilder(properties: TWProperties(name: "JKL+JMLS (?)", preference: 0, rules: rules), u("JKL"), u("JMLS"), u("Hyoda"), u("Aayla", "Ki-Adi-Mundi", "Ezra", "JKA", "Barriss"), u("Aayla", "Ki-Adi-Mundi", "Ezra", "JKA", "Barriss"))

    static let lukes2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Skywalkers (?)", preference: 0, rules: rules), u("JKL"), u("JMLS"), u("GK"), u("JKA"), u("GAS"))

    static let reyClones2 = SelectUnitSquadBuilder(properties: TWProperties(name: "Rey+Clones (?)", preference: 0, rules: rules), u("Rey"), u("CT-5555"), u("Rex"), u("C-3PO"), u("Hyoda"))

    static let jkrJKL2 = SelectUnitSquadBuilder(properties: TWProperties(name: "JKR+JKL (?)", preference: 0, rules: rules), u("JKR"), u("JKL"), u("GAS"), u("JMLS"), u("GMY", "Hyoda"))

    static let clones2 = SelectUnitSquadBuilder(properties: TWProperties(name: "501st (?)", preference: 0, rules: rules), u("Shaak Ti", "GAS"), u("CT-5555"), u("Rex"), u("CT210408"), u("C-3PO", "Arc Trooper"))
    
    static let p23 = MultipleSquadBuilder(slkrNS2, slkr2, vader2, vaderNute2, bh2, shaak2, imperialTroopers12)
    
    // https://www.youtube.com/watch?v=iW_6Aor38eM
    // JKL, GK, GAS, Hyoda, JKA - 10%

    // MARK:- Phase 4

    static let jediAddIds = Model.Character.matchesCategory("Jedi").map(\.id).removing("GRANDMASTERLUKE")
    static let droidAddIds = Model.Character.matchesCategory("Droid").map(\.id)
    static let sepAddIds = Model.Character.matchesCategory("Separatist").map(\.id)
    
    static let reyJawa4 = SelectUnitSquadBuilder(properties: TWProperties(name: "Rey+Jawa (?)", id: "REYJAWAS4", preference: 10, rules: rules), u("Rey"), u("Armorer", "DATHCHA"), u("Nebit"), u("Jawa Engineer"), u("Jawa Scavenger", "Jawa"))
    
    static let rey4 = SelectUnitSquadBuilder(properties: TWProperties(name: "JKR + Rey (7%)", id: "JKRREY4", preference: 7, rules: rules), u("JKR"), u("Rey"), u(jediAddIds.adding("Resistance Hero Finn")), u(jediAddIds.adding("Resistance Hero Poe")), u(jediAddIds))

    static let lukes4 = SelectUnitSquadBuilder(properties: TWProperties(name: "JKL + JML (6%)", id: "JKLJMLS4", preference: 6, rules: rules), u("JKL"), u("JMLS"), u("GAS"), u("JKA", "Shaak Ti", "Hyoda", "JKR"), u("JKA", "Shaak Ti", "Hyoda", "JKR"))

    static let rebels4 = SelectUnitSquadBuilder(properties: TWProperties(name: "Rebels (2%)", id: "REBELS4", preference: 2, rules: rules), u("CLS"), u("Chewbacca"), u("Han Solo"), u("C3POCHEWBACCA"), u("R2-D2", "C-3PO"))
    
    static let gg4 = SelectUnitSquadBuilder(properties: TWProperties(name: "GG (2%)", id: "GG4", preference: 2, rules: rules), u("GG"), u("B1"), u("MAGNAGUARD"), u("B2"), u(droidAddIds.removing("C-3PO", "R2-D2").adding("Dooku", "Nute", "Droideka")))

    static let jmk4 = SelectUnitSquadBuilder(properties: TWProperties(name: "JMK (8%)", id: "JMK4", preference: 8, rules: rules), u("Jedi Master Kenobi"), u("GK"), u("Ahsoka Tano"), u("Commander Ahsoka Tano"), u("JKA", "GAS"))
    
    static let seps4 = SelectUnitSquadBuilder(properties: TWProperties(name: "Seps (1%)", id: "SEPS4", preference: 1, rules: rules), u("Dooku"), u("Jango"), u(sepAddIds), u(sepAddIds), u(sepAddIds))

    static let p4 = MultipleSquadBuilder(lukes4, rebels4, gg4, rey4, reyJawa4, jmk4)


}
