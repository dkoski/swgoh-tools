//
//  Charts.swift
//  swgoh
//
//  Created by David Koski on 11/18/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

/*

{
"y" : "gp",
"dates" : [
   "11/20/2019"
],
"series" : [
   {
      "name" : "PlayerName",
      "id" : "12345678",
      "values" : {
         "R2-D2" : [
            22671
         ],
         "Nest" : [
            1983
         ],
         "Han's MF" : [
            // -1 means None
            -1
         ],

*/

protocol ChartKey {
    var name: String { get }
    var id: String { get }
}

struct SimpleChartKey : ChartKey {
    let name: String
    let id: String
}

struct ChartData : Codable {

    struct ChartDataSeries : Codable, Comparable {
        
        var name: String
        let id: String
        
        var values: [String:[Int]]
        
        init(key: ChartKey) {
            name = key.name
            id = key.id
            values = [:]
        }

        mutating func ensureValues(valueKeys: [String], count: Int) {
            let keysToRemove = Set(values.keys).subtracting(valueKeys)
            for key in keysToRemove {
                values[key] = nil
            }
            
            for key in valueKeys {
                if let array = values[key] {
                    if array.count < count {
                        var copy = array
                        copy.insert(contentsOf: Array(repeating: -1, count: count - array.count), at: 0)
                        values[key] = copy
                    }
                } else {
                    values[key] = Array(repeating: -1, count: count)
                }
            }
        }
        
        mutating func append(valueKey: String, value: Int) {
            values[valueKey, default: []].append(value)
        }

        static func < (lhs: ChartData.ChartDataSeries, rhs: ChartData.ChartDataSeries) -> Bool {
            if lhs.id < rhs.id {
                return true
            } else if lhs.id == rhs.id {
                return lhs.name < rhs.name
            } else {
                return false
            }
        }
        
        mutating func repair(_ expectedCount: Int) -> Bool {
            // in some cases I was getting two pieces of data per day, e.g. if you had two
            // BH squads, both would be in the data set.  fix this
            
            var repaired = false
            for key in values.keys {
                if values[key, default: []].count > expectedCount {
                    let source = values[key, default: []]
                    var result = [Int]()
                    for index in stride(from: source.count - 1, to: 0, by: -1) {
                        if index > 1 && index < source.count - 1 {
                            let previous = source[index - 1]
                            let v = source[index]
                            let next = source[index - 1]
                            
                            // looking for a series like 118949, 88833, 118949 -- if we find that
                            // then suppress the middle value
                            if v < previous && v < next {
                                // skip it
                            } else {
                                result.append(v)
                            }
                        } else {
                            result.append(source[index])
                        }
                        
                        if result.count == expectedCount {
                            break
                        }
                    }
                    
                    // built in reverse order
                    result.reverse()
                    
                    values[key] = result
                    repaired = true
                }
            }
            return repaired
        }
        
        var indexRange: ClosedRange<Int> {
            return 0 ... 0
        }
    }

    let yAxisLabel: String
    var dates: [String]
    
    private var seriesMap: [String:ChartDataSeries]
    
    private let formatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter
    }()
        
    enum CodingKeys: String, CodingKey {
        case yAxisLabel = "y"
        case dates
        case series
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        yAxisLabel = try values.decode(String.self, forKey: .yAxisLabel)
        dates = try values.decode([String].self, forKey: .dates)
        
        let series = try values.decode([ChartDataSeries].self, forKey: .series)
        
        seriesMap = Dictionary(uniqueKeysWithValues: series.map({ ($0.id, $0) }))
    }
    
    init(_ yAxisLabel: String) {
        self.yAxisLabel = yAxisLabel
        self.dates = []
        self.seriesMap = [:]
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(yAxisLabel, forKey: .yAxisLabel)
        try container.encode(dates, forKey: .dates)
        try container.encode(seriesMap.keys.sorted().map { seriesMap[$0]! }, forKey: .series)
    }
    
    static func read(from url: URL) throws -> ChartData? {
        let data: Data
        do {
            data = try Data(contentsOf: url)
        } catch {
            return nil
        }
        let decoder = JSONDecoder()
        return try decoder.decode(ChartData.self, from: data)
    }
    
    func write(to url: URL) throws {
        let encoder = JSONEncoder()
        let data = try encoder.encode(self)
        try data.write(to: url, options: [.atomic])
    }
    
    mutating func addDate(date: Date = Date()) -> Bool {
        let key = formatter.string(from: date)
        
        if dates.isEmpty || dates.last! != key {
            dates.append(key)
            return true
        }
        
        return false
    }
    
    mutating func ensureKeys(keys: [ChartKey]) {
        let newKeys = keys.map { $0.id }
        
        let keysToRemove = Set(seriesMap.keys).subtracting(newKeys)
        let keysToAdd = Set(newKeys).subtracting(seriesMap.keys)
        
        for key in keysToRemove {
            seriesMap[key] = nil
        }
        
        for key in keys {
            if keysToAdd.contains(key.id) {
                seriesMap[key.id] = ChartDataSeries(key: key)
            }
        }
    }
    
    mutating func ensureValues(valueKeys: [String]) {
        for key in seriesMap.keys {
            seriesMap[key]!.ensureValues(valueKeys: valueKeys, count: dates.count)
        }
    }   
    
    subscript(_ key: ChartKey) -> ChartDataSeries {
        get {
            if let value = seriesMap[key.id] {
                var copy = value
                copy.name = key.name
                return copy
            }
            
            return ChartDataSeries(key: key)
        }
        
        set {
            seriesMap[newValue.id] = newValue
        }
    }

    /// return the date range of valid data for the given key
    func dateRange(_ key: ChartKey, valueKey: String) -> ClosedRange<Date>? {
        if let dataSeries = seriesMap[key.id],
            let values = dataSeries.values[valueKey] {
            
            if let firstIndex = values.firstIndex(where: { $0 != -1 }),
                let firstDate = formatter.date(from: dates[firstIndex]),
                let lastDate = formatter.date(from: dates.last!) {
                // found a row and a valueKey with a non -1 value -- the index matches the dates array
                return firstDate ... lastDate
            }
        }
        return nil
    }
    
    /// return the date in the date range for the given key and valueKey
    func data(in range: ClosedRange<Date>, key: ChartKey, valueKey: String) -> ([Int], ClosedRange<Date>) {
        var result = [Int]()
        var start: Date? = nil
        var end: Date? = nil

        if let dataSeries = seriesMap[key.id],
            let values = dataSeries.values[valueKey] {
            for (value, dateString) in zip(values, dates) {
                if value == -1 {
                    // skip the sentinel value (no data)
                    continue
                }
                if let date = formatter.date(from: dateString), range.contains(date) {
                    if start == nil {
                        start = date
                    }
                    end = date
                    result.append(value)
                }
            }
        }
        
        return (result, (start ?? Date()) ... (end ?? Date()))
    }

    mutating func repair() -> Bool {
        // in some cases I was getting two pieces of data per day, e.g. if you had two
        // BH squads, both would be in the data set.  fix this
        
        var repaired = false
        for key in seriesMap.keys {
            if seriesMap[key]!.repair(dates.count) {
                repaired = true
            }
        }
        return repaired
    }
}

extension Model.Player : ChartKey {
    
    var id: String {
        return stats.allyCode.description
    }
    
}

extension Model.Guild : ChartKey {
    
    var id: String {
        return shortName
    }
    
}

protocol ChartGenerator {
    
    var path: String { get }
    var yAxisLabel: String { get }
    var valueKeys: [String] { get }
    var keys: [ChartKey] { get }

    func build(chart: inout ChartData)
    
    func build(base: URL) throws
    
}

extension ChartGenerator {
    
    func chartURL(base: URL) -> URL {
        base.appendingPathComponent(self.path)
    }
    
    func loadChartData(base: URL) throws -> ChartData {
        let url = chartURL(base: base)
        return try ChartData.read(from: url) ?? ChartData(self.yAxisLabel)
    }
    
    func build(base: URL) throws {
        var chart = try loadChartData(base: base)
        
        if chart.addDate() {
            build(chart: &chart)
                        
            chart.ensureKeys(keys: self.keys)
            chart.ensureValues(valueKeys: self.valueKeys)
            
            let url = chartURL(base: base)
            try chart.write(to: url)
        }
    }
    
}
