//
//  Copyright © 2021 David Koski. All rights reserved.
//

import SWGOH
import SwiftUI

private let star = "&#11088;"

struct ChallengeRancorPage : PageBuilder {
    
    let guild: Model.Guild

    var squadsByPhase = [String:[Model.Player:[SquadEvaluation]]]()
    
    public init(guild: Model.Guild, info: ChallengeRancor.Info) {
        self.guild = guild
        
        for phase in info.phases {
            let squads = evaluatePlayers(players: guild.players, squadBuilder: phase.squads)
            squadsByPhase[phase.id] = squads
        }
    }

    var path: String { return "guild/\(guild.shortName)/rancor.html" }
                
    private func copyData(baseURL: URL, data: PhaseData, computed: ComputedPhaseData) -> HTMLFramgment {
        let pageURL = baseURL.appendingPathComponent(self.path)
        let phaseId = data.phase.id
        
        let farm = computed.farm
            .map { (player, ev) -> (Model.Player, SquadEvaluation, Int) in
                (player, ev, gearLevelsNeeded(team: ev))
            }
            .sorted { $0.2 < $1.2 }
        
        return H.block {
            // text for farming
            H.pre().id("\(phaseId)-farm").style("display: none !important;") {
                H.foreach(farm) { (player, ev, gearNeeded) in
                    """
                    \(player.name) \(pageURL)?filter=\(player.code)#\(phaseId)
                    gear levels: \(gearNeeded) -- \(ev.squad.description(images: false, zetas: false, gearBelow: 100, starsBelow: 7))
                    
                    
                    """
                }
            }
            
            // text for attack
            H.pre().id("\(phaseId)-attack").style("display: none !important;") {
                "\(pageURL)#\(phaseId)\n\n"
                H.foreach(computed.teamsPlayed) { player, evs in
                    player.name
                    "\n"
                    H.foreach(evs) { ev in
                        ev.squad.name
                        ": "
                        ev.squad.description(images: false, zetas: false, gearBelow: 100, starsBelow: 7)
                        "\n"
                    }
                    "\n"
                }
            }
            H.pre().id("\(phaseId)-attack-csv").style("display: none !important;") {
                "phase,name,allycode,team-name,team,percent,gp\n"
                H.foreach(computed.teamsPlayed) { player, evs in
                    H.foreach(evs) { ev in
                        phaseId
                        ","
                        player.name
                        ","
                        player.code
                        ","
                        ev.squad.name
                        ","
                        ev.squad.description(images: false, zetas: false, gearBelow: 100, starsBelow: 7).replacingOccurrences(of: ",", with: "")
                        ","
                        ev.squad.properties.preference
                        ","
                        ev.squad.gp
                        "\n"
                    }
                }
            }
            
            H.a(href: "#").onclick("CopyToClipboard('\(phaseId)-farm'); return false;") {
                "Copy Farming Info"
            }
            " | "
            H.a(href: "#").onclick("CopyToClipboard('\(phaseId)-attack'); return false;") {
                "Copy Attack Info"
            }
            " | "
            H.a(href: "#").onclick("CopyToClipboard('\(phaseId)-attack-csv'); return false;") {
                "Copy Attack Info (CSV)"
            }
        }
    }
    
    private func intro(html: HTML) -> String {
        var result = ""
                        
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += H.script(src: "https://cdn.datatables.net/plug-ins/1.10.25/sorting/absolute.js").description
        
        result += html.header(title: "Challenge Rancor Schedule")
        
        // https://www.arclab.com/en/kb/htmlcss/how-to-copy-text-from-html-element-to-clipboard.html
        result += """
            <script>
            function CopyToClipboard(id)
            {
            document.getElementById(id).style.display = 'block';
            var r = document.createRange();
            r.selectNode(document.getElementById(id));
            window.getSelection().removeAllRanges();
            window.getSelection().addRange(r);
            document.execCommand('copy');
            window.getSelection().removeAllRanges();
            document.getElementById(id).style.display = 'none';
            }
            </script>
            """

        result += html.branding.challengeRancorInfo.intro

        return result
    }
    
    func contents(html: HTML) -> String {
        var result = intro(html: html)
        
        let info = html.branding.challengeRancorInfo
        let players = guild.players
        
        // the per phase squad data
        let dataByPhase = Dictionary(
            uniqueKeysWithValues: info.phases.map { phase in (phase.id, PhaseData(phase: phase, squadsByPlayer: squadsByPhase[phase.id]!)) })
        
        // the per phase computed data (e.g. who plays what)
        var unitsUsed = [Model.Player:Set<String>]()
        var buildComputedByPhase = [String: ComputedPhaseData]()
        for phase in info.phases {
            let data = dataByPhase[phase.id]!
            let computed = ComputedPhaseData(data: data, unitsUsed: unitsUsed)
            
            // consume the played units
            for (player, evs) in computed.teamsPlayed {
                unitsUsed[player, default:[]].formUnion(evs.flatMap { $0.squad.units }.map { $0.id })
            }
            buildComputedByPhase[phase.id] = computed
        }
        let computedByPhase = buildComputedByPhase

        result += H.block {
            H.h2 { "Breakdown By Player and Phase and Team" }

        }.description
                
        for phase in info.phases {
            let data = dataByPhase[phase.id]!
            let computed = computedByPhase[phase.id]!
            
            // keep track of which units are playing
            for (player, evs) in computed.teamsPlayed {
                for ev in evs {
                    unitsUsed[player, default: []].formUnion(ev.squad.units.map { $0.id })
                }
            }
            
            let squadNames = data.squadNames
                                    
            func showSquad(_ ev: SquadEvaluation) -> String {
                ev.squad.description(images: false, zetas: false, gearBelow: 100, starsBelow: 7)
            }
            
            func counts() -> HTMLFramgment {
                H.block {
                    H.th { "Counts" }

                    H.th {
                        data.viableCounts.count
                        H.span().class("not-viable") {
                            "/"
                            data.nonViableCounts.count
                        }
                    }
                    
                    // farming
                    H.th { "" }

                    H.foreach(squadNames) { name in
                        H.th {
                            data.viableCounts[name]
                            H.span().class("not-viable") {
                                "/"
                                data.nonViableCounts[name]
                            }
                        }
                    }
                }
            }
                        
            func gearNeededCell(ev: SquadEvaluation) -> HTMLFramgment {
                let maximumGearNeeded = 5 * (13 + 5)
                let gearNeeded = gearLevelsNeeded(team: ev)
                let fraction = (maximumGearNeeded - gearNeeded) * 255 / maximumGearNeeded
                let color = "#ff\(String(fraction, radix: 16))00"

                return H.td()
                    .class("not-viable").style("background-color: \(color);")
                    .order(-gearNeeded) {
                        if let info = data.teamInfos[ev.squad.name] {
                            info.marker
                        }
                        showSquad(ev)
                        " <tt>[\(gearNeeded)]</tt>"
                }
            }
            
            result += H.block {
                H.h3().id(phase.id) { phase.name }
                phase.intro
                
                "<p><b>Estimated Score: \(computed.score)</b><br/>"
                computed.scoreDescription
                
                if let baseURL = html.branding.baseURL {
                    // links to copy farming, attack info
                    copyData(baseURL: baseURL, data: data, computed: computed)
                }

                // table with custom numeric sorting -- https://www.datatables.net/blog/2016-12-22
                H.script {
                    """
                    $(document).ready(function () {
                        sortType = $.fn.dataTable.absoluteOrderNumber({ value: "", position: "bottom"});
                        $('#schedule-\(phase.id)').DataTable({
                            "paging": false,
                            "info": false,
                            "columnDefs": [
                                {
                                    targets: 0,
                                    type: "html",
                                },
                                {
                                    targets: 1,
                                    type: "num",
                                },
                                {
                                    targets: "_all",
                                    type: sortType,
                                },
                            ],
                        });
                    });
                    """
                }
                H.table().id("schedule-\(phase.id)").style("font-size: 75%;") {
                    // top header row
                    H.thead {
                        H.th { "Player" }
                        H.th { "Counts" }
                        H.th { "Farming Suggestion" }

                        H.foreach(squadNames) { name in
                            H.th {
                                if let team = data.teamInfos[name] {
                                    name
                                    team.marker
                                } else {
                                    name
                                }
                            }
                        }
                    }
                    
                    // optional second header row if there are preferred teams
                    if data.teamInfos.values.contains { $0.link != nil } {
                        H.thead {
                            H.th { "Links" }
                            H.th { "" }
                            H.th { "" }

                            H.foreach(squadNames) { name in
                                H.th {
                                    if let team = data.teamInfos[name] {
                                        if let link = team.link {
                                            H.a(href: html.url(link)) {
                                                "info ..."
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // third header row for counts
                    H.thead {
                        counts()
                    }
                    
                    // per player data -- body of the table
                    H.foreach(players) { player in
                        H.tr {
                            H.td().order(player.name).filter(player.code) { H.a(href: "?filter=\(player.code)") { player.name } }
                            
                            let (viableCount, notViableCount) = data.countTeams(player: player)
                            H.td().order(viableCount) {
                                viableCount
                                H.span().class("not-viable") {
                                    "/"
                                    notViableCount
                                }
                            }
                            
                            if let ev = computed.farm[player] {
                                gearNeededCell(ev: ev)
                            } else {
                                H.td().order(-10000) { "" }
                            }
                            
                            H.foreach(squadNames) { name in
                                if let ev = data.namedSquadsByPlayer[player]?[name] {
                                    if ev.status.isViable {
                                        H.td()
                                            .order(ev.squad.gp)
                                        
                                            // color it blue if the player should
                                            // use it this phase
                                            .style(computed.played(player, name) ? "background-color: #ceebfd" : "") {
                                            showSquad(ev)
                                        }
                                    } else {
                                        gearNeededCell(ev: ev)
                                    }
                                } else {
                                    H.td().order("") { "" }
                                }
                            }
                        }
                    }
                    
                    H.tfoot {
                        counts()
                    }
                }
                
            }.description
        }
        
        // per player summary info

        result += H.block {
            H.h2 { "Summary By Player and Phase" }

            info.summary
            
            """
            <script>
            $(document).ready(function() {
                $('#by-player').DataTable({
                    "paging": false,
                    "info": false,
                });
            });
            </script>
            """
            
            
            H.table().id("by-player").class("display").style("font-size: 75%; width: 80%;") {
                H.thead {
                    H.th { "Player" }
                    H.th { "Teams Played" }
                    H.foreach(info.phases) { phase in
                        H.th { phase.name }
                    }
                }
                
                // team counts
                H.thead {
                    H.th { "Totals" }
                    
                    H.th {
                        let playedCount = computedByPhase.values
                            .flatMap { $0.teamsPlayed.values }
                            .count
                        playedCount
                    }

                    H.foreach(info.phases) { phase in
                        H.th {
                            let data = dataByPhase[phase.id]!
                            let computed = computedByPhase[phase.id]!
                            if data.hasStarredTeams {
                                let (viable, notViable) = data.countTeams(starred: true)

                                star
                                
                                viable
                                " / "
                                notViable
                                
                                "<br>all: "
                            }
                            
                            let (viable, notViable) = data.countTeams()

                            viable
                            " / "
                            notViable
                            
                            "<br>played: "
                            computed.teamsPlayed.values.flatMap { $0 }.count
                        }
                    }
                }
                
                // score per phase
                H.thead {
                    H.th {
                        "Score"
                    }
                    H.th {
                        ""
                    }
                    H.foreach(info.phases) { phase in
                        H.th {
                            let computed = computedByPhase[phase.id]!
                            computed.score
                        }
                    }
                }
                
                H.foreach(players) { player in
                    H.tr {
                        H.td().filter(player.code) { player.name }
                        
                        H.td {
                            let playedCount = computedByPhase.values.reduce(0) {
                                $0 + ($1.teamsPlayed[player]?.count ?? 0)
                            }
                            playedCount
                        }
                                                
                        H.foreach(info.phases) { phase in
                            let data = dataByPhase[phase.id]!
                            let computed = computedByPhase[phase.id]!
                            
                            let playedCount = computed.teamsPlayed[player]?.count ?? 0

                            H.td()
                                // color it blue if they have a team to play
                                .style(playedCount >  0 ? "background-color: #ceebfd" : "") {
                                    
                                // if we have starred teams, count those
                                if data.hasStarredTeams {
                                    let (viable, notViable) = data.countTeams(player: player, starred: true)

                                    star
                                    
                                    viable
                                    " / "
                                    notViable
                                    
                                    "  all: "
                                }
                                
                                let (viable, notViable) = data.countTeams(player: player)

                                viable
                                " / "
                                notViable
                                
                                if (playedCount > 0) {
                                    "  play: \(playedCount)"
                                }
                            }
                        }
                    }
                }
            }
            
            // filter by name via query string
            H.script {
                """
                $(document).ready( function() {
                
                var urlParams = new URLSearchParams(window.location.search)
                var filter = urlParams.get('filter')
                if (filter) {
                """
                
                H.foreach(info.phases) { phase in
                    """
                    $('#schedule-\(phase.id)').DataTable().search(filter).draw()
                    
                    """
                }
                
                """
                $('#by-player').DataTable().search(filter).draw()
                }
                });
                """
            }
        }.description
                
        result += html.footer()

        return result
    }
}

struct ChallengeRancorTeamInfoPage : PageBuilder {
    
    let team: ChallengeRancor.TeamInfo

    var path: String { team.link! }
            
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        
        result += html.header(title: "Challenge Rancor Team Info: \(team.name)", nav: "")

        if let body = team.body {
            result += body
        }
        if let videoURL = team.videoURL {
            result +=
                """
                <br/>
                <br/>
                <iframe src="\(videoURL)" width="560" height="315" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                """
        }
        
        result += html.footer()

        return result
    }
}

private struct PhaseData {
    let phase: ChallengeRancor.PhaseInfo
    let players: [Model.Player]
    
    let squadsByPlayer: [Model.Player : [SquadEvaluation]]
    let namedSquadsByPlayer: [Model.Player:[String:SquadEvaluation]]
    let allSquads: [SquadEvaluation]
    
    /// optional per-team info (by team name)
    let teamInfos: [String : ChallengeRancor.TeamInfo]
    
    /// true if there are any teams with stars (preferred)
    let hasStarredTeams: Bool
    
    /// squad names in sorted order
    let squadNames: [String]
    
    /// counts by squad name
    let viableCounts: CountedSet<String>
    let nonViableCounts: CountedSet<String>
            
    init(phase: ChallengeRancor.PhaseInfo, squadsByPlayer: [Model.Player : [SquadEvaluation]]) {
        self.phase = phase
        self.squadsByPlayer = squadsByPlayer
        self.players = Array(squadsByPlayer.keys)
        
        self.namedSquadsByPlayer = Dictionary(
            uniqueKeysWithValues:
                squadsByPlayer.map {
                    ($0.key, Dictionary(uniqueKeysWithValues:
                        $0.value.map { ($0.squad.name, $0) })) }
        )
        
        // produce a squad name -> TeamInfo map (not all teams will have TeamInfo)
        let teams = Dictionary(
            squadsByPlayer
                .values.flatMap { $0 }
                .compactMap { ev -> (String, ChallengeRancor.TeamInfo)? in
                    if let id = ev.squad.properties.attackerId,
                       let team = phase.teams[id] {
                        return (ev.squad.name, team)
                    } else {
                        return nil
                    }
                },
            uniquingKeysWith: { (first, _) in first })
        
        self.teamInfos = teams
        self.hasStarredTeams = teams.contains { $0.value.marker == star }
        
        self.allSquads = squadsByPlayer.values.flatMap { $0 }

        // build the sort order by squad name
        let preferences = Dictionary(
            allSquads.map { ev -> (String, Int) in
                let name = ev.squad.name
                return (name, teams[name]?.order ?? ev.preference)
            },
            uniquingKeysWith: { (first, _) in first })
                    
        self.squadNames = preferences.sorted { $0.value > $1.value }.map { $0.key }
        
        self.viableCounts = CountedSet(items:
            allSquads.filter { $0.status.isViable }.map { $0.squad.name })
        self.nonViableCounts = CountedSet(items:
            allSquads.filter { !$0.status.isViable }.map { $0.squad.name })
    }
    
    /// figure out which team should be farmed, if any
    func farmSquad(player: Model.Player) -> SquadEvaluation? {
                                
        // convert the squadNames into squad evaluations (preference) order
        let evs = squadNames
            .compactMap { name in
                namedSquadsByPlayer[player]?[name]
            }
        let gearNeeded = evs.map { gearLevelsNeeded(team: $0) }
        
        let preferred = zip(evs, gearNeeded)
            .filter { teamInfos[$0.0.squad.name]?.marker == "&#11088;" }
        
        // if they have a preferred team, no need to farm
        if preferred.contains(where: { $0.1 == 0 }) {
            return nil
        }
        
        // if they have a preferred team that can be geared, suggest that
        if let (ev, _) = preferred.min(by: { $0.1 < $1.1 }) {
            return ev
        }

        // if they have any team, that is enough
        if zip(evs, gearNeeded).contains(where: { $0.1 == 0 }) {
            return nil
        }
        
        // otherwise suggest the closest team
        if let (ev, _) = zip(evs, gearNeeded).min(by: { $0.1 < $1.1 }) {
            return ev
        }

        return nil
    }

    /// given a list of squads, squad names in preference order and a set of already used unit ids, return the squad to play (if any)
    private func playSquad(player: Model.Player, squadNames: [String], used: Set<String>) -> SquadEvaluation? {
        for name in squadNames {
            if let ev = namedSquadsByPlayer[player]?[name],
               ev.status.isViable {
                if ev.squad.units
                    .map({ $0.id })
                    .contains(where: { used.contains($0) }) {
                    // we already used a unit
                    continue
                }
                
                return ev
            }
        }
        return nil
    }
        
    /// given a set of squads and preferences (squadNames + teams), figure out what teams each player is going to use and compute the score for the phase
    func phaseScore(unitsUsed: [Model.Player : Set<String>]) -> (Int, String, [Model.Player:[SquadEvaluation]]) {
        // figure out the score for the phase
        var phaseScore = 0
        
        // and track what we think they will play
        var play = [Model.Player:[SquadEvaluation]]()
        
        // figure out if we want to play:
        // - only preferred teams (if we can easily beat it)
        // - best teams (in preference order)
        // - everything (p4)
        enum SelectPlay {
            case preferred, best, all
        }
        let selectPlay: SelectPlay
        
        let preferredSquadNames = Set(teamInfos.filter { $0.value.order != nil }.map { $0.key })
        if phase.id != "p4" {
            var preferredOnlyScore = 0
            for (player, evs) in squadsByPlayer {
                // check to see how many points we would get using only preferred squads
                let squadNames = phase.customPlayOrder(player, evs).adding(squadNames).filter { preferredSquadNames.contains($0) }

                if let ev = playSquad(player: player, squadNames: squadNames, used: unitsUsed[player, default: []]) {
                    preferredOnlyScore += ev.squad.properties.preference
                }
            }
            if preferredOnlyScore > 110 {
                selectPlay = .preferred
            } else {
                selectPlay = .best
            }
        } else {
            selectPlay = .all
        }
        
        switch selectPlay {
        case .preferred:
            // play only preferred squads
            for (player, evs) in squadsByPlayer {
                let squadNames = phase.customPlayOrder(player, evs).adding(squadNames).filter { preferredSquadNames.contains($0) }
                if let ev = playSquad(player: player, squadNames: squadNames, used: unitsUsed[player, default: []]) {
                    phaseScore += ev.squad.properties.preference
                    play[player, default: []].append(ev)
                }
            }

        case .best:
            // play whatever shows up as the best
            for (player, evs) in squadsByPlayer {
                let squadNames = phase.customPlayOrder(player, evs).adding(squadNames)
                if let ev = playSquad(player: player, squadNames: squadNames, used: unitsUsed[player, default: []]) {
                    phaseScore += ev.squad.properties.preference
                    play[player, default: []].append(ev)
                }
            }
            
        case .all:
            // play all available squads
            for (player, evs) in squadsByPlayer {
                let squadNames = phase.customPlayOrder(player, evs).adding(squadNames)
                let unitsUsed = unitsUsed[player, default:[]]
                
                for name in squadNames {
                    if let ev = namedSquadsByPlayer[player]?[name],
                       ev.status.isViable,
                       !ev.squad.units.contains(where: { unitsUsed.contains($0.id) }) {
                        phaseScore += ev.squad.properties.preference
                        play[player, default: []].append(ev)
                    }
                }
            }
        }
                    
        let phaseGrade: String
        switch phaseScore {
        case 0 ... 75: phaseGrade = "Youngling: probably not going to make it"
        case 76 ... 100: phaseGrade = "Padawan: doable if you ping every single player all day"
        case 101 ... 150: phaseGrade = "Knight: it's gonna take half the day"
        case 151 ... 200: phaseGrade = "General: it's gonna take an hour"
        default: phaseGrade = "Master: better get your teams in because the raid's gonna finish in 20 minutes"
        }

        return (phaseScore, phaseGrade, play)
    }

    func countTeams(player: Model.Player? = nil, starred: Bool = false) -> (Int, Int) {
        let squads: [SquadEvaluation]
        if let player = player {
            squads = squadsByPlayer[player, default: []]
        } else {
            squads = allSquads
        }
        
        let filteredSquads: [SquadEvaluation]
        if starred {
            filteredSquads = squads.filter { teamInfos[$0.squad.name]?.marker == star }
        } else {
            filteredSquads = squads
        }
        
        return (
            filteredSquads.filter { $0.status.isViable }.count,
            filteredSquads.filter { !$0.status.isViable }.count
            )
    }
}

private struct ComputedPhaseData {
    /// how much of the phase we expect to complete -- 100 is a clear with perfect runs and full participation
    let score: Int
    
    /// text description of how we feel about the score
    let scoreDescription: String
    
    /// teams that are being played per-player
    let teamsPlayed: [Model.Player:[SquadEvaluation]]
    
    /// for quick lookup of (player, team) -> bool
    private let teamNamesPlayed: [Model.Player:Set<String>]
    
    /// optional team to farm
    let farm: [Model.Player:SquadEvaluation]

    init(data: PhaseData, unitsUsed: [Model.Player : Set<String>]) {
        self.farm =
            Dictionary(uniqueKeysWithValues:
                data.players
                .compactMap { player in
                    if let farm = data.farmSquad(player: player) {
                        return (player, farm)
                    } else {
                        return nil
                    }
                }
            )
        (self.score, self.scoreDescription, self.teamsPlayed) = data.phaseScore(unitsUsed: unitsUsed)
        
        self.teamNamesPlayed =
            Dictionary(uniqueKeysWithValues:
                self.teamsPlayed
                .map { player, evs in
                    (player, Set(evs.map { $0.squad.name }))
                }
            )
    }
    
    func played(_ player: Model.Player, _ name: String) -> Bool {
        teamNamesPlayed[player, default: []].contains(name)
    }
}

private func gearLevelsNeeded(team: SquadEvaluation) -> Int {
    func needed(_ unit: Model.Unit) -> Int {
        let gearLevel = unit.combinedGearLevel.value
        if gearLevel >= 25 {
            return 0
        } else {
            if gearLevel < 13 {
                return (13 - gearLevel) + 5
            } else {
                return 25 - gearLevel
            }
        }
    }
    return team.squad.units.reduce(0, { $0 + needed($1) })
}
