//
//  Alliance.swift
//  swgoh
//
//  Created by David Koski on 9/6/19.
//  Copyright © 2019 David Koski. All rights reserved.
//

import SWGOH

struct AllianceGuildIndex : PageBuilder {

    var path: String { return "alliance/index.html" }
    
    let guilds: [Model.Guild]

    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += html.dataTable(id: "data")
        
        result += html.chartInclude()
        
        result +=
            """
            <script>
            c1 = new Chart("alliance-gp-stats.json", "alliance", "total")
            c1.id = "alliance-gp-chart"
            c1.title = "Alliance Total GP"
            c1.tightScale = true
            c1.round = 100000000
            c1.height = 400
            c1.display()

            c3 = new Chart("alliance-player-counts.json", "alliance", "players")
            c3.id = "alliance-player-chart"
            c3.title = "Alliance Total Players"
            c3.tightScale = true
            c3.round = 100
            c3.height = 400
            c3.display()

            c2 = new Chart("alliance-guilds-gp-stats.json", null, "total")
            c2.id = "guild-gp-chart"
            c2.title = "Guild Total GP"
            c2.tightScale = true
            c2.round = 10000000
            c2.display()
            </script>
            """
        
        result += html.header(title: "Alliance Guild Index", nav: AllianceSite.navigation(html).render())
        
        result +=
            H.table().id("data") {
                H.thead {
                    H.tr {
                        H.th { "Guild" }
                        H.th { "Members" }
                        H.th { "Units GP" }
                        H.th { "Ships GP" }
                        H.th { "Total GP" }
                    }
                }
                H.foreach(guilds) { guild in
                    H.tr {
                        H.td().order(guild.name) {
                            H.a(href: html.g(guild, "index.html")) { guild.name }
                        }
                        H.td().order(guild.players.count) {
                            H.a(href: "../player/\(guild.shortName).html") { guild.players.count }
                        }
                        H.td().order(guild.characterGP) { guild.characterGP.millions }
                        H.td().order(guild.shipGP) { guild.shipGP.millions }
                        H.td().order(guild.info.gp) { guild.info.gp.millions }
                    }
                }
                H.tfoot {
                    H.tr {
                        H.td { guilds.count }
                        H.td { guilds.reduce(0) { $0 + $1.players.count } }
                        H.td { guilds.reduce(0) { $0 + $1.characterGP }.millions }
                        H.td { guilds.reduce(0) { $0 + $1.shipGP }.millions }
                        H.td { guilds.reduce(0) { $0 + $1.info.gp }.millions }
                    }
                }
            }.render()
        
        result +=
            """
            <div id="alliance-gp-chart"></div><div id="alliance-player-chart"></div><div id="guild-gp-chart"></div>
            """
        
        result += html.footer()
        
        return result
    }

}

struct AllianceCharacterStats : PageBuilder {
    
    var path: String { return "alliance/characters.html" }
    
    let guilds: [Model.Guild]
    
    struct CharacterInfo {
        var count = 0
        var viableCount = 0
        var g10 = 0
        var g13 = 0
        var r5 = 0
        var r8 = 0
        var leaderZeta = 0
        var uniqueZeta = 0
        
        var zetas = [String:Int]()
        
        var g13Players = [Model.Player]()
        var zetaPlayers = [Model.Player]()
        
        var mods: String
        
        init(character: Model.Character) {
            if let rec = referenceRecommendations[character.id], !rec.clusters.isEmpty {
                mods = rec.clusters[0].priorities.map { $0.c.description }.joined(separator: ", ")
            } else {
                mods = "&nbsp;"
            }
        }
        
        mutating func accumulate(player: Model.Player, unit: Model.Unit) {
            count += 1
            
            if unit.gear >= 10 {
                viableCount += 1
                
                g10 += 1
                if unit.gear >= 13 {
                    g13 += 1
                    
                    g13Players.append(player)
                    
                    if unit.relic >= (5 + 2) {
                        r5 += 1
                    }
                    
                    if unit.relic >= (8 + 2) {
                        r8 += 1
                    }
                }
            }

            for ability in unit.skills {
                if ability.isZeta && ability.tier == ability.maxTier {
                    if ability.abilityType == .leader {
                        leaderZeta += 1
                    } else {
                        uniqueZeta += 1
                    }
                }
            }
            
            for zeta in unit.zetaIds {
                self.zetas[zeta, default:0] += 1
            }
            
            if !unit.zetaIds.isEmpty {
                zetaPlayers.append(player)
            }
        }
        
        static func header() -> HTMLFramgment {
            return H.block {
                H.th { "Count" }
                H.th().title("Number of units with G10+") { "Viable" }
                H.th().title("% of players with a viable unit") { "Viable %" }
                H.th().title("% of viable units with the leader zeta") { "Leader Zeta %" }
                H.th().title("% of viable units with the other zeta(s)") { "Other Zeta %" }
                H.th { "G13+" }
                H.th { "R5+" }
                H.th { "G13+ %" }
                H.th { "R5+ %" }
                H.th { "R8 %" }
                H.th { "Common Mods" }
            }
        }
        
        func percent(_ numerator: Int, _ denominator: Int) -> String {
            if denominator == 0 { return "" }
            let percent = Double(numerator) / Double(denominator) * 100.0
            let roundedPercent = min((percent * 100.0).rounded() / 100.0, 100.0)
            return roundedPercent.description
        }
        
        func row(playerCount: Int) -> HTMLFramgment {
            return H.block {
                H.td { count }
                H.td { viableCount }
                H.td { percent(viableCount, playerCount) }
                H.td {
                    if zetaPlayers.count <= 10 {
                        H.div().title(zetaPlayers.map { $0.name }.sorted().joined(separator: ", ")) {
                            percent(leaderZeta, viableCount)
                        }
                    } else {
                        percent(leaderZeta, viableCount)
                    }
                }
                H.td {
                    if zetaPlayers.count <= 10 {
                        H.div().title(zetaPlayers.map { $0.name }.sorted().joined(separator: ", ")) {
                            percent(uniqueZeta, viableCount)
                        }
                    } else {
                        percent(uniqueZeta, viableCount)
                    }
                }
                H.td {
                    if g13 <= 5 {
                        H.div().title(g13Players.map { $0.name }.sorted().joined(separator: ", ")) {
                            g13
                        }
                    } else {
                        g13
                    }
                }
                H.td { r5 }
                H.td { percent(g13, playerCount) }
                H.td { percent(r5, playerCount) }
                H.td { H.div().title(r8.description) { percent(r8, playerCount) } }
                H.td { mods }
            }
        }
    }
    
    static func compile(players: [Model.Player]) -> [Model.Character:CharacterInfo] {
        var result = [Model.Character:CharacterInfo]()
        
        for player in players {
            for unit in player.units.values where !unit.isShip {
                let c = unit.character
                result[c, default: CharacterInfo(character: c)].accumulate(player: player, unit: unit)
            }
        }
        
        return result
    }
    
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += html.dataTable(id: "data", fixedHeader: true)

        result += html.header(title: "Alliance Character Data", nav: AllianceSite.navigation(html).render())
        
        result += #"All characters owned by players in the alliance.  Viable means gear 10 or higher.  The percent values are percent of players, not percent of the character.  Common mods are based on mining mod data and are not necessarily a recommendation -- this is what people use.  Click on the character name to see more information.  Use the search field to narrow it down.  Click on column headers to sort.  Want to mess with the data on your own?  Get the <a href="characters.csv">CSV</a>."#

        let playerCount = guilds.reduce(0) { $0 + $1.players.count }
        let data = AllianceCharacterStats.compile(players: guilds.flatMap { $0.players })
                        
        result +=
            H.table().id("data") {
                H.thead {
                    H.tr {
                        H.th { "Name" }
                        CharacterInfo.header()
                    }
                }
                H.foreach(data.map { ($0, $1) }) { (character, info) in
                    H.tr {
                        H.td().order(character.name) { H.a(href: html.character(character)) { character.name } }
                        info.row(playerCount: playerCount)
                    }
                }
            }.render()

        result += html.footer()

        return result
    }

}

struct AllianceCharacterCSV : PageBuilder {
    
    var path: String { return "alliance/characters.csv" }
    
    let guilds: [Model.Guild]
    
    func contents(html: HTML) -> String {
        var result = ""
        
        let data = AllianceCharacterStats.compile(players: guilds.flatMap { $0.players })

        result += #"Name,Count,G10+,G13+,R5,leader zeta,other zeta"#
        result += "\n"
        
        for character in data.keys.sorted() {
            let info = data[character]!
            
            result += "\""
            result += character.name
            result += "\","
            
            result += info.count.description
            result += ","
            result += info.g10.description
            result += ","
            result += info.g13.description
            result += ","
            result += info.r5.description
            result += ","
            result += info.leaderZeta.description
            result += ","
            result += info.uniqueZeta.description

            result += "\n"
        }

        return result
    }

}


struct AlliancePlayerIndex : PageBuilder {
    
    var path: String { return "alliance/players.html" }
    
    let guilds: [Model.Guild]
    
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += html.dataTable(id: "data", fixedHeader: true)

        result += html.header(title: "Alliance Player Index", nav: AllianceSite.navigation(html).render())
        
        result += #"All players in the alliance.  Use the search field to narrow it down.  Click on column headers to sort.  Totals are at the bottom.  Want to mess with the data on your own?  Get the <a href="players.csv">CSV</a>."#

        let rowFactory = StatsRowFactory(extractors: StatsRowExtractors.playerExtractors)
        var rows = [(Model.Guild, Model.Player, StatsRow)]()
        var sum = rowFactory.emptyRow()
        for guild in guilds.sorted() {
            for player in guild.players.sorted() {
                let row = rowFactory.newRow(guild: guild, player: player)
                sum.add(row: row, rowFactory: rowFactory)
                rows.append((guild, player, row))
            }
        }
        sum.finishAdd(rowFactory: rowFactory)
        
        result +=
            H.table().id("data") {
                H.thead {
                    H.tr {
                        H.th { "Name" }
                        H.th { "Guild" }
                        H.th { "Ally Code" }
                        rowFactory.header()
                    }
                }
                H.foreach(rows) { (guild, player, row) in
                    H.tr {
                        H.td { H.a(href: html.p(player, "tw-squads")) { player.name } }
                        H.td { H.a(href: html.g(guild, "index.html")) { guild.name } }
                        H.td { H.a(href: "https://swgoh.gg/p/\(player.code)/") { player.code } }
                        row.render()
                    }
                }
                H.tfoot {
                    H.tr {
                        H.td { rows.count }
                        H.td { guilds.count }
                        H.td { "" }
                        sum.render()
                    }
                }
            }.render()

        result += html.footer()

        return result
    }

}

struct AlliancePlayerCSV : PageBuilder {
    
    var path: String { return "alliance/players.csv" }
    
    let guilds: [Model.Guild]
    
    func contents(html: HTML) -> String {
        var result = ""
        
        let rowFactory = StatsRowFactory(extractors: StatsRowExtractors.playerExtractors)
        var rows = [(Model.Guild, Model.Player, StatsRow)]()
        for guild in guilds {
            for player in guild.players {
                let row = rowFactory.newRow(guild: guild, player: player)
                rows.append((guild, player, row))
            }
        }
        
        result += #"Name,Guild,Ally Code,"#
        result += rowFactory.extractors.map { $0.title ?? $0.name }.joined(separator: ",")
        result += "\n"
        
        for (guild, player, row) in rows {
            result += "\""
            result += player.name
            result += "\","
            result += guild.name
            result += ","
            result += player.code
            result += ","
            
            result += row.values.map { $0.description }.joined(separator: ",")

            result += "\n"
        }

        return result
    }

}

struct AllianceSite {
    
    let guilds: [Model.Guild]
    let pages: [String]
    
    let verbose = true
    
    init(guilds: [Model.Guild], pages: [String] = []) {
        self.guilds = guilds
        self.pages = pages.map { $0.lowercased() }
    }
    
    static func navigation(_ html: HTML) -> HTMLFramgment {
        return H.block {
            H.a(href: html.alliance("players.html")) { "Player" }
            " | "
            H.a(href: html.alliance("characters.html")) { "Characters" }
            " | "
            H.a(href: html.alliance("history.html")) { "History" }
            " | "
            H.a(href: html.alliance("transfers.html")) { "Transfers" }
        }
    }
            
    struct GuildBuilder {
        let name: String
        let body: (AllianceSite, [Model.Guild], URL) throws -> Void
        
        init(_ name: String, _ body: @escaping (AllianceSite, [Model.Guild], URL) throws -> Void) {
            self.name = name
            self.body = body
        }
    }
        
    let guildPages: [GuildBuilder] = [
        GuildBuilder("Resources") {
            try AllianceResourcesPage().build(base: $2)
        },
        GuildBuilder("Guild Index") {
            try AllianceGuildIndex(guilds: $1).build(base: $2)
        },
        GuildBuilder("Player Index") {
            try AlliancePlayerIndex(guilds: $1).build(base: $2)
        },
        GuildBuilder("Player Index CVS") {
            try AlliancePlayerCSV(guilds: $1).build(base: $2)
        },
        GuildBuilder("Character Stats") {
            try AllianceCharacterStats(guilds: $1).build(base: $2)
        },
        GuildBuilder("Character Stats") {
            try AllianceCharacterCSV(guilds: $1).build(base: $2)
        },
        GuildBuilder("GP Chart") {
            try AllianceGPChart(guilds: $1).build(base: $2)
        },
        GuildBuilder("GP Chart") {
            try AllianceGPChart(guilds: $1).build(base: $2)
        },
        GuildBuilder("Guilds GP Chart") {
            try AllianceGuildsGPChart(guilds: $1).build(base: $2)
        },
        GuildBuilder("Guild Counts Chart") {
            try AllianceGuildsCountChart(guilds: $1).build(base: $2)
        },
        GuildBuilder("Alliance Counts Chart") {
            try AllianceCountsChart(guilds: $1).build(base: $2)
        },
        GuildBuilder("Alliance Important Toons Count Chart") {
            try AllianceImportantToonsCountChart(guilds: $1).build(base: $2)
        },
        GuildBuilder("Alliance Player Important Toons GP Chart") {
            try AlliancePlayerImportantToonsGPChart(guilds: $1).build(base: $2)
        },
        GuildBuilder("History") {
            try AlliancePlayerHistoryPage(guilds: $1).build(base: $2)
            try AlliancePlayerTransferPage(guilds: $1).build(base: $2)
        },
        GuildBuilder("ModReferenceData") {
            let modReference = SiteModReferenceData(recommendations: referenceRecommendations)
            let data = try JSONEncoder().encode(modReference)
            let url = $2.appendingPathComponent("modReference.json")
            try data.write(to: url)
        },
        GuildBuilder("Characters") {
            for c in Model.Character.all where !c.ship {
                try CharactersPages(character: c, guilds: $1).build(base: $2)
            }
        },
        GuildBuilder("Rancor") {
            for phase in defaultBrand.challengeRancorInfo.phases {
                for team in phase.teams.values {
                    if team.needsPage {
                        try ChallengeRancorTeamInfoPage(team: team).build(base: $2)
                    }
                }
            }
        }
    ]
    
    func trace(_ value: @autoclosure () -> String) {
        if verbose {
            print(value())
        }
    }
    
    func pageAllowed(_ name: String) -> Bool {
        return pages.isEmpty || pages.contains(name.lowercased())
    }
    
    func build(base: URL) throws {
        
        let pages = guildPages + defaultBrand.alliancePages
                
        DispatchQueue.concurrentPerform(iterations: pages.count) {
            let builder = pages[$0]
            
            if !pageAllowed(builder.name) {
                return
            }

            do {
                trace("START \(builder.name)")
                try builder.body(self, guilds, base)
                trace("DONE  \(builder.name)")
            } catch {
                fatalError("processing \(builder.name): \(error)")
            }
        }
    }
}

struct AllianceDataMine {

    let guilds: [Model.Guild]
    
    func buildCharacterData(url: URL) throws {
        var result =
            """
            public let characterDemographics = [
            
            """
        
        let playerCount = guilds.reduce(0) { $0 + $1.players.count }
        let data = AllianceCharacterStats.compile(players: guilds.flatMap { $0.players })
        
        for character in data.keys.sorted() {
            if let stats = data[character] {
                if stats.viableCount == 0 {
                    result +=
                        """
                        "\(character.id)" : CharacterDeomographics(id: "\(character.id)", viable: 0, zetas: [:], g12: 0, g13: 0),
                        
                        """
                } else {
                    result +=
                        """
                        "\(character.id)" : CharacterDeomographics(id: "\(character.id)", viable: \(stats.percent(stats.viableCount, playerCount)), zetas: [\(stats.zetas.isEmpty ? ":" : stats.zetas.map { "\"" + $0.0 + "\":" + stats.percent($0.1, stats.viableCount)  }.joined(separator: ", "))], g13: \(stats.percent(stats.g13, stats.viableCount)), r5: \(stats.percent(stats.r5, stats.viableCount))),
                        
                        """
                }
            }
        }
        
        result +=
            """
            ]
            """
        
        try result.write(to: url, atomically: true, encoding: .utf8)
    }

    func build(base: URL) throws {
        try buildCharacterData(url: base.appendingPathComponent("Demographics.swift"))
    }

}

struct AllianceGPChart : ChartGenerator {
    
    var path: String { return "alliance/alliance-gp-stats.json" }
    let yAxisLabel = "gp"
    let valueKeys = [ "ships", "toons", "total" ]
    let allianceKey = SimpleChartKey(name: "BRG Alliance", id: "alliance")
    var keys: [ChartKey] { return [allianceKey] }
    
    let guilds: [Model.Guild]

    func build(chart: inout ChartData) {
        chart[allianceKey].append(valueKey: "ships", value: guilds.reduce(0) { $0 + $1.shipGP })
        chart[allianceKey].append(valueKey: "toons", value: guilds.reduce(0) { $0 + $1.characterGP })
        chart[allianceKey].append(valueKey: "total", value: guilds.reduce(0) { $0 + $1.info.gp })
    }
}

struct AllianceGuildsGPChart : ChartGenerator {
    
    var path: String { return "alliance/alliance-guilds-gp-stats.json" }
    let yAxisLabel = "gp"
    let valueKeys = [ "ships", "toons", "total" ]
    var keys: [ChartKey] { return guilds }
    
    let guilds: [Model.Guild]

    func build(chart: inout ChartData) {
        for guild in guilds {
            chart[guild].append(valueKey: "ships", value: guild.shipGP)
            chart[guild].append(valueKey: "toons", value: guild.characterGP)
            chart[guild].append(valueKey: "total", value: guild.info.gp)
        }
    }
}

struct AllianceGuildsCountChart : ChartGenerator {
    
    var path: String { return "alliance/alliance-guilds-counts-stats.json" }
    let yAxisLabel = "count"
    let valueKeys = [ "players" ]
    var keys: [ChartKey] { return guilds }
    
    let guilds: [Model.Guild]

    func build(chart: inout ChartData) {
        for guild in guilds {
            chart[guild].append(valueKey: "players", value: guild.players.count)
        }
    }
}

struct AllianceCountsChart : ChartGenerator {
    
    var path: String { return "alliance/alliance-player-counts.json" }
    let yAxisLabel = "count"
    var valueKeys: [String] {
        var result = [ "players" ]
        result.append(contentsOf: StatsRowExtractors.allianceStatsExtractors.map { $0.name })
        return result
    }
    let allianceKey = SimpleChartKey(name: "BRG Alliance", id: "alliance")
    var keys: [ChartKey] { return [allianceKey] }
    
    let guilds: [Model.Guild]

    func build(chart: inout ChartData) {
        chart[allianceKey].append(valueKey: "players", value: guilds.reduce(0) { $0 + $1.players.count })
                
        let rowFactory = StatsRowFactory(extractors: StatsRowExtractors.allianceStatsExtractors)
        var sum = rowFactory.emptyRow()

        for guild in guilds {
            for player in guild.players {
                let row = rowFactory.newRow(guild: guild, player: player, evaluations: [])
                sum.add(row: row, rowFactory: rowFactory)
            }
        }
        sum.finishAdd(rowFactory: rowFactory)
        
        for (index, value) in sum.values.enumerated() {
            let extractor = StatsRowExtractors.guildCountStatsExtractors[index]
            chart[allianceKey].append(valueKey: extractor.name, value: value)
        }
    }
}

struct AllianceImportantToonsCountChart : ChartGenerator {
    
    var path: String { return "alliance/alliance-important-toon-count.json" }
    let yAxisLabel = "count"
    var valueKeys: [String] {
        return ImportantToonsPage.importantToons.map { Model.Character.find($0).commonName }
    }
    let allianceKey = SimpleChartKey(name: "BRG Alliance", id: "alliance")
    var keys: [ChartKey] { return [allianceKey] }

    let guilds: [Model.Guild]

    func build(chart: inout ChartData) {
        var counts = [String:Int]()
        let importantToons = ImportantToonsPage.importantToons
        for guild in guilds {
            for player in guild.players {
                for name in importantToons {
                    let id = Model.Character.find(name).id
                    if let unit = player.units[id], unit.gear >= 10 {
                        counts[unit.commonName, default: 0] += 1
                    }
                }
            }
        }
        
        for name in importantToons {
            chart[allianceKey].append(valueKey: name, value: counts[name, default: 0])
        }
    }
}

struct AlliancePlayerImportantToonsGPChart : ChartGenerator {
    
    var path: String { return "alliance/alliance-player-important-toon-gp.json" }
    let yAxisLabel = "gp"
    var valueKeys: [String] {
        return ImportantToonsPage.importantToons.map { Model.Character.find($0).commonName }
    }
    var keys: [ChartKey] { return guilds.flatMap { $0.players } }

    let guilds: [Model.Guild]

    func build(chart: inout ChartData) {
        let importantToons = ImportantToonsPage.importantToons
        for guild in guilds {
            for player in guild.players {
                for name in importantToons {
                    let id = Model.Character.find(name).id
                    if let unit = player.units[id] {
                        chart[player].append(valueKey: unit.commonName, value: unit.gp)
                    }
                }
            }
        }
    }
}
