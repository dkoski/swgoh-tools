//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SWGOH

private struct FarmingReportConfiguration {
    let importantSquadBuilders: [SquadBuilder]
    let unlocks: [FarmingUnlockPage.Unlock]
    
    let intro: String
}

// MARK: - Standard Config

private let defaultRules = [
    "DEFAULT" : TWRule().e,
]

// a private geo definition -- we aim a little higher
private let geonosians = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Geos", rules: geoRules), u("GBA"), u("Poggle"), u("Geo Soldier"), u("Sun Fac"), u("Geo Spy"))

private let geoRules = [
    "GBA" : TWRule(leaderZeta: true, gear: 13, gp: 16500).e,
    "DEFAULT" : TWRule(gear: 12, gp: 16500).e,
]

private let kamMissions = SelectUnitSquadBuilder(properties: Squad.Properties(name: "KAM Mission", rules: kamRules), u("Shaak Ti"), u("Rex"), u("CT210408"), u("Fives"), u("Arc Trooper"))

private let kamRules = [
    "Fives" : TWRule(gear: 27).e,
    "DEFAULT" : TWRule(gear: 25).e,
]

/// allowed to enter KAM mission, probably not viable
private let possibleKamMission = SelectUnitSquadBuilder(properties: Squad.Properties(name: "KAM Mission", unitMinimumGP: 22_0000, rules: defaultRules), u("Shaak Ti"), u("Rex"), u("CT210408"), u("Fives"), u("Arc Trooper"))

private let importantSquadBuilders: [SquadBuilder] = [
    geonosians,
    TW.padmeBuilder,
    TW.darthRevanSquadBuilder,
    TW.gasBuilder,
    kamMissions,
    
    TW.negotiatorBuilder,
    TW.malevolanceBuilder,
]

private let templarSquadBuilders: [SquadBuilder] = [
    geonosians,
    TW.padmeBuilder,
    TW.darthRevanSquadBuilder,
    TW.gasBuilder,
    possibleKamMission,
    
    TW.negotiatorBuilder,
    TW.malevolanceBuilder,
]

private let noGeosSquadBuilders: [SquadBuilder] = [
    TW.padmeBuilder,
    TW.darthRevanSquadBuilder,
    TW.gasBuilder,
    kamMissions,
    
    TW.negotiatorBuilder,
    TW.malevolanceBuilder,
]


private let unlocks = FarmingUnlockPage.unlocks.filter { $0.target.categories.contains("Galactic Legend") || $0.target.commonName == "JKL" || $0.target.commonName == "Galactic Legend Lord Vader"}

private let lsGeoTBIntro = """
    <p>
    The squads selected are useful in most game modes, especially LS/DS Geo TB:

    <ol>
    <li><b>Geos</b> -- a great free-to-play (FTP) team that is key for DS Geo TB.
    It is pretty easy to farm, only has one zeta
    and can get Wat shards with just G12 units (though a relic GBA is handy).  This may be useful in
    TW and GAC as well.  The ships are crucial for DS Geo TB and work well with Malevolence.
    Finally, these can be used to unlock Padme.
    <li><b>padme</b> -- another great FTP team.  This is viable in many arena shards,
    is a powerful counter in TW and GAC and is a key team for LS Geo TB.  Well worth farming
    up to relic levels in my opinion.  Some of the units are key for unlocking GAS.
    <li><b>DRMalak</b> -- a strong sith team that is viable in many arenas, is a powerful counter
    and useful in pretty much all areas of the game.  It is a long farm, but well worth having.
    This was the top-end team for quite a while.
    <li><b>GAS</b> -- this is another long farm and requires some Galactic Republic (Padme team),
    Separatists (GG team), and Asajj (why not have a nice NS team to use with her?).  This was
    the top-end team until the Galactic Legends came along and is still powerful and useful in
    most areas of the game.
    </ol>

    <p>
    The next two items are the two most important fleets to have (Home One / Rebels is pretty nice too!).
    You should have at least one of these farmed and well geared.  Many ship arenas have these fleets
    at the top -- this is an excellent source of crystals!

    <p>
    The last section is Galactic Legends and Jedi Knight Luke (sort of in-between GAS and GL).  These are
    long farms, but your opponents are getting them and so should you.  These will help unlock more
    starts in the Geo TBs, are required for most arena shards.  Since they take so long to farm, you
    might as well pick your favorite and start chipping away now!
    """

private let standardConfiguration = FarmingReportConfiguration(
    importantSquadBuilders: importantSquadBuilders, unlocks: unlocks,
    intro: lsGeoTBIntro)

// MARK: - DS GEO TB Config
private let fleetRules = [
    "DEFAULT" : UnitEvaluator(
        UnitRule.level85Rule,
        UnitRule.sevenStarRule
    )
]

/// special fleet for DS Geo TB
private let chimaeraSpecial = SelectUnitSquadBuilder(properties: Squad.Properties(name: "Chimaera-TB", preference: 10, rules: fleetRules), u("Chimaera"), u("HT"), u("Geonosian Soldier's Starfighter"), u("Geonosian Spy's Starfighter"), u("Sun Fac's Geonosian Starfighter"))

private let dsGeoTBSquadBuilders: [SquadBuilder] = [
    geonosians,
    
    DSGeoTBSquads.nightsisterBuilder,
    DSGeoTBSquads.firstOrderBuilder,
    DSGeoTBSquads.epBuilder,

    TW.padmeBuilder,
    
    DSGeoTBSquads.ggBuilder,
    TW.darthRevanSquadBuilder,

    chimaeraSpecial,
    TW.negotiatorBuilder,
    TW.malevolanceBuilder,
]

private let dsGeoTBIntro = """
    <p>
    The squads selected are focused on DS Geo TB, but are useful in other areas as well:

    <ol>
    <li><b>Geos</b> -- a great free-to-play (FTP) team that is key for DS Geo TB.
    It is pretty easy to farm, only has one zeta
    and can get Wat shards with just G12 units (though a relic GBA is handy).  This may be useful in
    TW and GAC as well.  The ships are crucial for DS Geo TB and work well with Malevolence.
    Finally, these can be used to unlock Padme.
    <li><b>NS</b> -- Nightsisters are extremely durable, with a lot of revives, escalating power
    and perhaps the best (my opinion) tank in the game: NS Zombie.  She can't die!  MT or
    Asajj make good leads.  I prefer MT in the DS Geo TB for more chances to revive, but
    pick the style that fits you.
    <li><b>FO</b> -- First Order do well in DS Geo TB.  KRU or Hux make fantastic leads.  KRU
    is <i>extremely</i> durable while Hux prevents counters (useful in the TB!) and boosts offense.
    <li><b>zEP</b> -- After a few character reworks, the Emperor Palpatine and Vader combo
    is an offensive powerhouse.
    <li><b>padme</b> -- another great FTP team.  This is viable in many arena shards,
    is a powerful counter in TW and GAC and is a key team for LS Geo TB.  Well worth farming
    up to relic levels in my opinion.  Some of the units are key for unlocking GAS.
    <li><b>DRMalak</b> -- a strong sith team that is viable in many arenas, is a powerful counter
    and useful in pretty much all areas of the game.  It is a long farm, but well worth having.
    This was the top-end team for quite a while.
    </ol>

    <p>
    The next three items are the ships required for the DS Geo TB plus
    the two most important fleets to have (Home One / Rebels is pretty nice too!).
    You should aim to have at least one of these farmed and well geared.  Many ship arenas have these fleets
    at the top -- this is an excellent source of crystals!

    <p>
    The last section is Galactic Legends and Jedi Knight Luke (sort of in-between GAS and GL).  These are
    long farms, but your opponents are getting them and so should you.  These will help unlock more
    starts in the Geo TBs, are required for most arena shards.  Since they take so long to farm, you
    might as well pick your favorite and start chipping away now!
    """

/// for guilds who are focusing on DS Geo TB:  geos, NS, FO, EP, padme, GG
private let dsGeoTBConfiguration = FarmingReportConfiguration(
    importantSquadBuilders: dsGeoTBSquadBuilders, unlocks: unlocks,
    intro: dsGeoTBIntro)

private let templarConfiguration = FarmingReportConfiguration(
    importantSquadBuilders: importantSquadBuilders, unlocks: unlocks,
    intro: lsGeoTBIntro)

/// same as standardConfiguration with no geos
private let noGeoConfiguration = FarmingReportConfiguration(
    importantSquadBuilders: noGeosSquadBuilders, unlocks: unlocks,
    intro: lsGeoTBIntro)


private let configurations = [
    "DEFAULT" : standardConfiguration,
    
    "tumbleweed" : dsGeoTBConfiguration,
    "honor" : dsGeoTBConfiguration,
    "hoth" : dsGeoTBConfiguration,
    "phoenixhawk" : dsGeoTBConfiguration,
    "rambos" : dsGeoTBConfiguration,
    "sbr" : dsGeoTBConfiguration,
    "mandalorians" : dsGeoTBConfiguration,
    "kotor" : dsGeoTBConfiguration,    
    "han" : dsGeoTBConfiguration,
    "holocron": dsGeoTBConfiguration,
    "ahsoka": dsGeoTBConfiguration,
    "mandos_periperi": dsGeoTBConfiguration,
    "youngling": dsGeoTBConfiguration,

    "twd": noGeoConfiguration,
    "hope": noGeoConfiguration,

    "templar" : templarConfiguration,

    "report" : standardConfiguration,
    "dr" : dsGeoTBConfiguration,
]

private struct TBInfo {
    
    let missionEstimator: TBSquadMissionEstimator
    let result: TB.Board
    
    init(boardConfiguration: TBBoardConfiguration, squadConfiguration: TBSquadConfiguration, guild: Model.Guild) {
        let preparedMissions = squadConfiguration.prepareMissionConfigurations(players: guild.players)
        let missionEstimator = TBSquadMissionEstimator(guild: guild, boardConfiguration: boardConfiguration, squadConfiguration: squadConfiguration, preparedMissions: preparedMissions)

        let toonsGP = guild.players.reduce(0) { $0 + $1.stats.characterGP } / 1_000_000
        let shipsGP = guild.players.reduce(0) { $0 + $1.stats.shipGP } / 1_000_000

        let tb = TB(toonGP: toonsGP, shipGP: shipsGP, configuration: boardConfiguration, missionEstimator: missionEstimator, planComparator: { _, _ in return nil })

        self.missionEstimator = missionEstimator
        self.result = tb.simulate()
    }
    
    func estimatedMissions(player: Model.Player) -> (Int, Int) {
        var combatWaves = 0
        var missionGp = 0
        
        for phase in 1 ... 4 {
            guard let active = result.activity.first(where: { $0.phase == phase }) else {
                return (combatWaves, missionGp)
            }
            let sections: [TB.Key]
            switch active {
            case .active(_, let s):
                sections = s.map { $0.key }
            default:
                fatalError("found wrong activity: \(active)")
            }
            
            let result = missionEstimator.missionFor(sectors: sections, player: player)
                        
            for (_, info) in result {
                combatWaves += info.stats.waves
                missionGp += Int(info.stats.points * 1_000_000)
            }
        }

        return (combatWaves, missionGp)
    }
}

private struct PlayerInfo {
    let player: Model.Player
    let importantSquads: [SquadEvaluation?]
    let bestSquad: SquadEvaluation?
    let legends: [Legend]
    let recommendation: String
    
    struct Legend {
        let name: String
        let unit: Model.Unit?
        let gearLevelsNeeded: Int
        
        init(player: Model.Player, unlock: FarmingUnlockPage.Unlock) {
            self.name = unlock.target.commonName
            self.unit = player.units[unlock.target.id]
            
            var gearNeeded = 0
            
            for unitId in unlock.requiredGear!.keys {
                if let unit = player.units[unitId] {
                    gearNeeded += unlock.needGearLevels(unitId: unitId, gearLevel: unit.gear, relic: unit.relic)
                } else {
                    gearNeeded += unlock.needGearLevels(unitId: unitId, gearLevel: 0, relic: 0)
                }
            }
            self.gearLevelsNeeded = gearNeeded
        }
    }
    
    init(player: Model.Player, allSquads: [SquadEvaluation], configuration: FarmingReportConfiguration) {
        self.player = player
        
        let filter = PreferredViabilitySortFilter()
        let evaluator = SquadEvaluator(rules: [:])
        
        var importantSquads = [SquadEvaluation?]()
        for builder in configuration.importantSquadBuilders {
            let squads = filter.filter(evaluator.evaluate(squads: builder.generateSquads(units: player)))
            if let squad = squads.first {
                importantSquads.append(squad)
            } else {
                importantSquads.append(nil)
            }
        }
        self.importantSquads = importantSquads
        
        self.bestSquad = allSquads.filter { !$0.squad.isFleet && !$0.squad.name.contains("Counter") }.sorted { $0.squad.gp > $1.squad.gp }.first
        self.legends = configuration.unlocks.map { Legend(player: player, unlock: $0) }
        
        // now reccomend something
        var recommendation: String? = nil
        
        // prefer a GL if you are really close
        if recommendation == nil {
            if let gl = legends.filter({ $0.unit == nil && $0.gearLevelsNeeded < 10 }).sorted(by: { $0.gearLevelsNeeded < $1.gearLevelsNeeded }).first {
                recommendation = gl.name
            }
        }

        // prefer something that is farmed but not yet viable
        if recommendation == nil {
            for squad in importantSquads {
                if let squad = squad {
                    if !squad.status.isViable && !squad.squad.isFleet {
                        recommendation = squad.squad.name
                        break
                    }
                }
            }
        }
        
        // if not, prefer something not farmed
        if recommendation == nil {
            for (builder, squad) in zip(importantSquadBuilders, importantSquads) {
                if squad == nil {
                    let name = builder.name
                    if name == "Negotiator" || name == "Malevolence" {
                        // skip it -- i have to test the name because the squad didn't resolve
                        continue
                    }
                    recommendation = name
                    break
                }
            }
        }
        // otherwise prefer a GL if you are close
        if recommendation == nil {
            if let gl = legends.filter({ $0.unit == nil && $0.gearLevelsNeeded < 50 }).sorted(by: { $0.gearLevelsNeeded < $1.gearLevelsNeeded }).first {
                recommendation = gl.name
            }
        }
        // otherwise a missing fleet
        if recommendation == nil {
            let noWorkingFleet = importantSquads.filter { ev in
                if let ev = ev {
                    return ev.squad.isFleet && ev.status.isViable
                } else {
                    return false
                }
            }.isEmpty
            
            if noWorkingFleet {
                // try a fleet that is farmed but not viable
                for squad in importantSquads {
                    if let squad = squad {
                        if !squad.status.isViable && squad.squad.isFleet {
                            recommendation = squad.squad.name
                            break
                        }
                    }
                }
            }
            
            if recommendation == nil {
                for (builder, squad) in zip(importantSquadBuilders, importantSquads) {
                    let name = builder.name
                    if name == "Negotiator" || name == "Malevolence" {
                        if squad == nil {
                            recommendation = name
                            break
                        }
                    }
                }
            }
        }

        // or the next gl
        if recommendation == nil {
            if let gl = legends.filter({ $0.unit == nil}).sorted(by: { $0.gearLevelsNeeded < $1.gearLevelsNeeded }).first {
                recommendation = gl.name
            }
        }

        self.recommendation = recommendation ?? "N/A"
    }
}

struct GuildFarmingReport : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/guild-farming.html" }
    
    let guild: Model.Guild
    let squads: [Model.Player : [SquadEvaluation]]

    func contents(html: HTML) -> String {
        let configuration = configurations[guild.shortName] ?? standardConfiguration
        
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS

        result += html.header(title: "Guild Farming Report")
        result += html.dataTable(id: "farming", fixedHeader: true)
        
        result +=
            """
            <p>
            Note: these farming scores are specific to Hope but are likely generally applicable.  If you want
            to customize them, please contact @gorgatron#3094.

            <p>
            The DS/LS TB columns show how many waves each player is estimated to pass and how many bonus gp those will
            be worth.  This is just an estimate -- with careful play and luck you may be able to beat it!  Press auto
            or don't try and you will score lower.  If you want more details, click on the name and go to the LS / DS
            tabs at the end of the top menu.

            <p>
            The next column shows your best squad, just for reference.  The <b>Suggested Next</b> is the
            algorithm's best guess at what you should farm next according to its rules.  If you are close
            on unlocking a Galactic Legend, it will recommend that, otherwise it will pick the best among
            the suggested squads (again, contact me to customize them!), having one of the key fleets, or
            a GL.

            """
        
        result += configuration.intro
                
        let dsTB = TBInfo(boardConfiguration: DSGeoTB(), squadConfiguration: DSGeoTBSquads(), guild: guild)
        let lsTB = TBInfo(boardConfiguration: LSGeoTB(), squadConfiguration: LSGeoTBSquads(), guild: guild)
        
        let playerInfos = DispatchQueue.concurrentPerform(iterations: guild.players.count) { (index) -> (Model.Player, PlayerInfo) in
            let player = guild.players[index]
            return (player, PlayerInfo(player: player, allSquads: squads[player, default: []], configuration: configuration))
        }.values
                        
        result +=
            H.table().id("farming").style("font-size: 85%") {
                H.thead {
                    H.tr {
                        H.th { "Player" }
                        H.th { "GP" }
                        H.th().title("Estimated DS Geo TB Waves") { "DS TB Waves" }
                        H.th().title("Estimated DS Geo TB Mission GP (millions)") { "DS TB Mission" }
                        H.th().title("Estimated LS Geo TB Waves") { "LS TB Waves" }
                        H.th().title("Estimated LS Geo TB Mission GP (millions)") { "LS TB Mission" }
                        H.th { "Best Squad" }
                        H.th { "Suggested Next" }
                        H.foreach(configuration.importantSquadBuilders) { sb in
                            H.th { sb.name }
                        }
                        H.foreach(configuration.unlocks) { u in
                            H.th {
                                u.target.commonName
                                H.a(href: html.g(self.guild, "farm-unlock-\(u.target.id).html")) {
                                    "&#x1f512;"
                                }
                            }
                        }
                    }
                }
                H.foreach(playerInfos) { p in
                    H.tr {
                        H.td { H.a(href: html.p(p.player, "tw-squads")) { p.player.name } }
                        H.td {
                            H.parent([HTMLAttribute("data-order", p.player.stats.gp.description)])
                            p.player.stats.gp.millions
                        }
                        
                        H.ifLet(dsTB.estimatedMissions(player: p.player)) { (waves, gp) in
                            H.td {
                                colorWaves(waves)
                                waves
                            }
                            H.td {
                                dataOrder(gp)
                                colorWaves(waves)
                                gp.millions
                            }
                        }
                        H.ifLet(lsTB.estimatedMissions(player: p.player)) { (waves, gp) in
                            H.td {
                                colorWaves(waves)
                                waves
                            }
                            H.td {
                                dataOrder(gp)
                                colorWaves(waves)
                                gp.millions
                            }
                        }
                        
                        if p.bestSquad == nil {
                            H.td { "none" }
                        } else {
                            H.td().title(p.bestSquad!.squad.description(images: false, zetas: true, gearBelow: 20, starsBelow: 7)) {
                                dataOrder(p.bestSquad!.squad.gp)
                                p.bestSquad!.squad.name
                                " - "
                                p.bestSquad!.squad.gp.millions
                            }
                        }
                        
                        H.td {
                            H.parent([HTMLAttribute("style", "background-color: #f0f0f0")])
                            p.recommendation
                        }
                        
                        H.foreach(p.importantSquads) { ev in
                            H.td {
                                H.parent(requiredAttributes(ev))
                                if ev == nil {
                                    "&nbsp;"
                                } else {
                                    ev!.squad.gp.millions
                                }
                            }
                        }
                        H.foreach(p.legends) { l in
                            H.td {
                                if l.unit == nil {
                                    dataOrder(l.gearLevelsNeeded)
                                    colorGearLevelNeeded(l.gearLevelsNeeded)
                                    l.gearLevelsNeeded

                                } else {
                                    dataOrder(l.unit!.gp)
                                    l.unit!.gp.millions
                                }
                            }
                        }

                    }
                }
            }.render()

        result += html.footer()

        return result
    }
    
    func requiredAttributes(_ ev: SquadEvaluation?) -> [HTMLAttribute] {
        if let ev = ev {
            var attributes = [HTMLAttribute("data-order", ev.squad.gp.description)]
            
            attributes.append(HTMLAttribute("title", ev.squad.description(images: false, zetas: true, gearBelow: 20, starsBelow: 7)))
            if !ev.status.isViable {
                attributes.append(HTMLAttribute("style", "background-color: #ffff00"))
            }
                            
            return attributes
        } else {
            return [HTMLAttribute("data-order", "0"), HTMLAttribute("style", "background-color: red")]
        }
    }
    
    func colorWaves(_ waves: Int) -> ParentHTMLModifier {
        if waves == 0 {
            return H.parent([HTMLAttribute("style", "background-color: red")])
        } else if waves < 10 {
            return H.parent([HTMLAttribute("style", "background-color: ffff00")])
        } else {
            return H.parent([])
        }
    }
    
    func colorGearLevelNeeded(_ needed: Int) -> ParentHTMLModifier {
        if needed < 50 {
            return H.parent([HTMLAttribute("style", "background-color: #ffffa0")])
        } else {
            return H.parent([HTMLAttribute("style", "background-color: #ff8080")])
        }
    }
    
    func dataOrder(_ value: Int) -> ParentHTMLModifier {
        H.parent([HTMLAttribute("data-order", value.description)])
    }

}
