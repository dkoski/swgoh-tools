//
//  Copyright © 2020 David Koski. All rights reserved.
//

import SWGOH

// TODO: switch to Model.GearLevel

struct FarmingUnlockPage : PageBuilder {
    
    var path: String { return "guild/\(guild.shortName)/farm-unlock-\(unlock.target.id).html" }
    
    let guild: Model.Guild

    let unlock: Unlock
    
    struct Unlock {
        let target: Model.Character
        let requires: [Model.Character]
        let unlockStars: Int
        let requiredGP: Int?
        let requiredGear: [ String: Int ]?
        let comment: String?
        let guides: [String]?
        
        init(_ target: String, _ requires: [String], unlockStars: Int = 7, requiredGP: Int? = nil, requiredGear: [String: Int]? = nil, comment: String? = nil, guides: [String]? = nil) {
            self.target = Model.Character.find(target)
            self.requires = requires.map { Model.Character.find($0) }
            self.unlockStars = unlockStars
            self.requiredGP = requiredGP
            if let requiredGear = requiredGear {
                self.requiredGear = Dictionary(uniqueKeysWithValues: requiredGear.map { (Model.Character.find($0).id, $1) })
            } else {
                self.requiredGear = nil
            }
            self.comment = comment
            self.guides = guides
        }
        
        func exceedsRequiredGP(unit: Model.Unit) -> Bool {
            if let requiredGP = self.requiredGP {
                return unit.gp >= requiredGP
            } else {
                return true
            }
        }
        
        func exceedsGearRequirements(unit: Model.Unit) -> Bool {
            if let requiredGear = requiredGear?[unit.id] {
                if requiredGear > 20 {
                    return unit.relic - 2 >= requiredGear - 20
                } else {
                    return unit.gear >= requiredGear
                }
            } else {
                return true
            }
        }
        
        var totalRequiredGearLevels: Int {
            requiredGear?.keys.map { needGearLevels(unitId: $0, gearLevel: 0, relic: 0) }.reduce(0, +) ?? 0
        }
        
        func needGearLevels(unitId: String, gearLevel: Int, relic: Int) -> Int {
            if let requiredGear = requiredGear?[unitId] {
                if requiredGear > 20 && relic - 2 >= requiredGear - 20 {
                    return 0
                } else if gearLevel >= requiredGear {
                    return 0
                }

                if requiredGear > 20 {
                    if gearLevel < 13 {
                        return (13 - gearLevel) + requiredGear - 20
                    } else {
                        return (requiredGear - 20) - (relic - 2)
                    }
                } else {
                    return requiredGear - gearLevel
                }
            } else {
                return 0
            }

        }
    }

    /// https://i.redd.it/1ncf3mmxjlm21.png
    static let unlocks = [
        Unlock("JKR", [ "T3-M4", "Mission", "Zaalbar", "Jolee", "Bastila Shan" ]),
        Unlock("DR", [ "BSF", "Canderous", "Carth", "Juhani", "HK-47" ]),
        
        Unlock("JEDIKNIGHTLUKE", [ "CLS", "ROLO", "Captain Han", "Wampa", "Chewbacca", "Darth Vader", "C-3PO", "Lando", "Hermit Yoda", "MILLENNIUMFALCON", "XWINGRED2" ], unlockStars: 7, requiredGear: [
                "CLS" : 23,
                "ROLO" : 23,
                "Captain Han" : 23,
                "Wampa" : 23,
                "Chewbacca" : 23,
                "Darth Vader" : 23,
                "C-3PO" : 23,
                "Lando" : 23,
                "Hermit Yoda" : 23,
            ], comment: "The listed gear is minimum required.", guides: [
                "<a href=\"https://forums.galaxy-of-heroes.starwars.ea.com/discussion/231366/the-journey-continues-for-luke-skywalker\">The Journey Continues For Luke Skywalker</a>"
        ]),

        Unlock("GAS", [ "Padme", "Asajj", "GK", "Ahsoka", "Shaak Ti", "C-3PO", "B1", "B2", "Droideka", "Magnaguard" ], unlockStars: 5, requiredGP: 17_700, requiredGear: [
                "Padme" : 12,
                "Asajj" : 12,
                "GK" : 24,
                "Ahsoka" : 12,
                "Shaak Ti" : 12,
                "C-3PO" : 12,
                "B1" : 23,
                "B2" : 12,
                "Droideka" : 12,
                "Magnaguard" : 12,
            ], comment: "The listed gear levels are suggestions.", guides: [
                "<a href=\"https://www.reddit.com/r/SWGalaxyOfHeroes/comments/dh1nde/how_i_unlocked_gas_as_100_f2p_a_short_guide/\">How I Unlocked GAS as 100% F2P: A Short Guide</a>"
        ]),
        Unlock("Malak", [ "JKR", "DR", "Jolee", "Bastila Shan", "Mission", "Zaalbar", "T3-M4", "Carth", "BSF", "Canderous", "HK-47", "Juhani" ], unlockStars: 5, requiredGP: 17_500, requiredGear: [
            "JKR" : 12,
            "DR" : 12,
            "Jolee" : 11,
            "Bastila Shan" : 11,
            "Mission" : 12,
            "Zaalbar" : 12,
            "Carth" : 11,
            "BSF" : 12,
            "Canderous" : 11,
            "HK-47" : 11,
        ], comment: "This event requires each of the Revans plus 4 of the 5 from each of their unlock teams at 17500 gp.  The listed gear levels are suggestions.", guides: [
            "<a href=\"https://www.reddit.com/r/SWGalaxyOfHeroes/comments/bc03qa/gear_level_requirements_for_175k_gp_based_on/\">Gear level requirements for 17.5k GP based on ability levels</a>"
        ]),

        Unlock("Rey", [ "JTR", "R2-D2", "C-3PO", "BB-8", "RT", "Scav Rey", "Finn", "Poe", "Vet Chewbacca", "Vet Han", "Resistance Pilot", "Holdo", "Rose", "Resistance Hero Poe", "Resistance Hero Finn" ],
           requiredGear: [
                "JTR" : 27,
                "Finn" : 25,
                "RT" : 25,
                "Scav Rey" : 27,
                "Resistance Pilot" : 23,
                "Poe" : 25,
                "Resistance Hero Finn" : 25,
                "Holdo" : 25,
                "Rose" : 25,
                "Resistance Hero Poe" : 25,
                "BB-8" : 27,
                "Vet Chewbacca" : 23,
            ]),
        Unlock("SL Kylo", [ "KRU", "Kylo", "FOST", "FOE", "FOO", "FOTP", "Phasma", "Hux", "FOSFTP", "Sith Trooper", "Vet Han", "EP", "Finalizer" ],
               requiredGear: [
                "KRU" : 27,
                "FOST" : 25,
                "FOO" : 25,
                "Kylo" : 27,
                "Phasma" : 25,
                "FOE" : 25,
                "Vet Han" : 23,
                "Sith Trooper" : 25,
                "FOSFTP" : 23,
                "Hux" : 25,
                "FOTP" : 23,
                "EP" : 27,
            ]),
        Unlock("Sith Eternal Emperor", [ "EP", "Vader", "Royal Guard", "Admiral Piett", "Krennic", "Darth Sidious", "Darth Maul", "Count Dooku", "Sith Marauder", "Imperial Tie Bomber", "JKA", "Thrawn", "Tarkin", "Veers", "Starck" ],
               requiredGear: [
                "EP" : 27,
                "Vader" : 27,
                "Royal Guard" : 23,
                "Admiral Piett" : 25,
                "Krennic" : 24,
                "Darth Sidious" : 27,
                "Darth Maul" : 24,
                "Count Dooku" : 26,
                "Sith Marauder" : 27,
                "JKA" : 27,
                "Thrawn" : 26,
                "Tarkin" : 23,
                "Veers" : 23,
                "Starck" : 23,
            ]),
        Unlock("Jedi Master Luke Skywalker", [ "Old Ben", "JTR", "C-3PO", "Mon Mothma", "C3POCHEWBACCA", "JKL", "R2-D2", "Han Solo", "Chewbacca", "Rebel Y-wing", "Leia", "Hermit Yoda", "Wedge", "Biggs", "Lando" ],
               requiredGear: [
                "Old Ben" : 25,
                "JTR" : 27,
                "C-3PO" : 25,
                "Mon Mothma" : 25,
                "C3POCHEWBACCA" : 25,
                "JKL" : 27,
                "R2-D2" : 27,
                "Han Solo" : 26,
                "Chewbacca" : 26,
                "Leia" : 23,
                "Hermit Yoda" : 25,
                "Wedge" : 23,
                "Biggs" : 23,
                "Lando" : 25,
            ]),
        Unlock("Jedi Master Kenobi", [ "GK", "Bo-Katan Kryze", "Aayla Secura", "Mace Windu", "Negotiator", "General Grievous", "Wat Tambor", "Magnaguard", "Clone Sergeant - Phase I", "QGJ", "GMY", "Jango", "Shaak Ti", "Cad Bane", "Cody" ],
               requiredGear: [
                "GK": 28,
                "Bo-Katan Kryze": 25,
                "Aayla Secura": 23,
                "Mace Windu": 23,
                "General Grievous": 27,
                "Wat Tambor": 27,
                "Magnaguard": 25,
                "Clone Sergeant - Phase I": 25,
                "QGJ": 23,
                "GMY": 28,
                "Jango": 27,
                "Shaak Ti": 27,
                "Cad Bane": 25,
                "Cody": 25,
            ]),
        Unlock("Lord Vader", [ "Hunter", "Tech", "Wrecker", "Tusken Raider", "Padme", "GAS", "Embo", "CT210408", "BADBATCHECHO", "Count Dooku", "Zam Wesell", "BTL-B Y-wing Starfighter", "Grand Moff Tarkin", "ARC Trooper", "Nute Gunray"],
               requiredGear: [
                "Hunter": 25,
                "Tech": 25,
                "Wrecker": 25,
                "Tusken Raider": 25,
                "Padme": 28,
                "GAS": 28,
                "Embo": 25,
                "CT210408": 27,
                "BADBATCHECHO": 25,
                "Count Dooku": 28,
                "Zam Wesell": 27,
                "Grand Moff Tarkin": 27,
                "ARC Trooper": 28,
                "Nute Gunray": 27,
                
            ], guides: [
                "<a href=\"https://forums.galaxy-of-heroes.starwars.ea.com/discussion/247334/road-ahead-july-2021\">Announcement</a>"
            ]),


        Unlock("Thrawn", [ "Kanan", "Ezra", "Zeb", "Hera", "Chopper", "Sabine"], comment: "5 of the 6 required at 7 stars."),
        Unlock("Chimaera", [ "Biggs", "Wedge", "Kanan", "Ezra", "Zeb", "Hera", "Chopper", "Sabine", "Ackbar", "Home One", "Ghost", "Phantom", "Biggs X-Wing", "Wedge Antilles's X-wing" ]),
        Unlock("Executor", [ "Darth Vader", "Piett", "Boba Fett", "Dengar", "IG-88", "Bossk", "TIE Fighter Pilot", "TIE Advanced x1", "Imperial TIE Bomber", "HT", "Slave I", "IG-2000", "Imperial TIE Fighter", "Razor Crest" ],
               requiredGear: [
                "Darth Vader": 27,
                "Piett": 28,
                "Boba Fett": 28,
                "Dengar": 25,
                "IG-88": 25,
                "Bossk": 25,
                "TIE Fighter Pilot": 25,
                
            ]),
        Unlock("CLS", [ "Old Ben", "Stormtrooper Han", "R2-D2", "Farmboy", "Princess Leia" ]),
        Unlock("GMY", [ "Jolee", "Bastila Shan", "Kanan", "Old Ben", "Ezra", "Juhani", "Aayla", "Ahsoka", "Barriss", "General Kenobi", "JKA", "Lumi", "Mace" ], comment: "Any 5 jedi are needed at 7 stars to unlock GMY."),
        Unlock("Chewbacca", [ "Bossk", "Boba Fett", "Dengar", "Greedo", "Cad Bane", "Embo", "Jango", "IG-88", "Aurra" ], comment: "Any 5 bounty hunters are required.  Typically Bossk lead with Dengar."),
        Unlock("Han's MF", [ "Chewbacca", "Han Solo", "Bossk", "Boba Fett", "IG-88", "Cad", "HT", "Slave I", "IG-2000", "Xanadu Blood" ]),
        Unlock("R2-D2", [ "EP", "Vader", "Thrawn", "Veers", "Tarkin", "DT", "TFP", "Starck", "Stormtrooper", "Range Trooper", "Shoretrooper", "Snowtrooper", "Magmatrooper", "Royal Guard" ], comment: "Any 5 Imperial units are needed at 7 stars to unlock R2-D2."),
        Unlock("C-3PO", [ "Chief Chirpa", "Ewok Elder", "Logray", "Paploo", "Wicket", "Ewok Scout", "Teebo" ], comment: "Any 5 ewoks are needed at 7 stars.  Typically Chirpa, Elder, Logray, Paploo and Wicket"),
        Unlock("BB-8", [ "KRU", "Kylo", "FOST", "FOE", "FOO", "FOTP", "Phasma", "Hux", "FOSFTP", "Sith Trooper"], comment: "Any 5 First Order are required at 7 stars to unlock BB-8."),
        Unlock("JTR", [ "BB-8", "Scav Rey", "Finn", "Vet Han", "Vet Chewbacca" ]),
    ]
    
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += html.toonCss

        result += html.header(title: "\(guild.name) Unlock \(unlock.target.commonName)")
        result += html.dataTable(id: "toons")
        
        result += H.p {
            "Guild readiness on unlocking \(unlock.target.name)."
            "<br/>"
            if unlock.comment != nil {
                "<br/>"
                unlock.comment!
            }
            if unlock.guides != nil {
                "<h2>Guides</h2>"
                "<ul>"
                H.foreach(unlock.guides!) { g in
                    "<li>\(g)</li>"
                }
                "</ul>"
            }
        }
        .render()
        
        func unit(_ unit: Model.Unit?) -> HTMLFramgment {
            if let unit = unit {
                return unit.description(images: false, zetas: true, gearBelow: 20, starsBelow: 8) + " \(unit.gp.millions)"
            } else {
                return ""
            }
        }
        
        func targetAttributes(_ unit: Model.Unit?) -> [HTMLAttribute] {
            if let unit = unit {
                var attributes = [HTMLAttribute("data-order", unit.gp.description)]
                
                if unit.stars < self.unlock.unlockStars {
                    attributes.append(HTMLAttribute("style", "background-color: yellow"))
                }
                
                return attributes
            } else {
                return [HTMLAttribute("style", "background-color: yellow")]
            }
        }
        
        func requiredAttributes(_ unit: Model.Unit?) -> [HTMLAttribute] {
            if let unit = unit {
                var attributes = [HTMLAttribute("data-order", unit.gp.description)]
                
                if unit.stars < 7 {
                    attributes.append(HTMLAttribute("style", "background-color: #ff0070"))
                    
                } else if !self.unlock.exceedsGearRequirements(unit: unit) {
                    let maximumGearNeeded = self.unlock.needGearLevels(unitId: unit.id, gearLevel: 0, relic: 0)
                    let gearNeeded = self.unlock.needGearLevels(unitId: unit.id, gearLevel: unit.gear, relic: unit.relic)
                    let fraction = (maximumGearNeeded - gearNeeded) * 255 / maximumGearNeeded
                    attributes.append(HTMLAttribute("style", "background-color: #ff\(String(fraction, radix: 16))00"))

                } else if !self.unlock.exceedsRequiredGP(unit: unit) {
                    attributes.append(HTMLAttribute("style", "background-color: #f0f000"))

                }
                                
                return attributes
            } else {
                return [HTMLAttribute("style", "background-color: red")]
            }
        }
        
        func requiredGearAttributes(_ player: Model.Player) -> [HTMLAttribute] {
            let maximumGearNeeded = unlock.totalRequiredGearLevels
            let gearNeeded = gearLevelsNeeded(player: player)

            var attributes = [HTMLAttribute("data-order", gearNeeded.description)]
            
            if gearNeeded > 1 {
                let fraction = (maximumGearNeeded - gearNeeded) * 255 / maximumGearNeeded
                attributes.append(HTMLAttribute("style", "background-color: #ff\(String(fraction, radix: 16))00"))
            }

            return attributes
        }

        func gearLevelsNeeded(player: Model.Player) -> Int {
            var gearNeeded = 0
            
            for unitId in unlock.requiredGear!.keys {
                if let unit = player.units[unitId] {
                    gearNeeded += unlock.needGearLevels(unitId: unitId, gearLevel: unit.gear, relic: unit.relic)
                } else {
                    gearNeeded += unlock.needGearLevels(unitId: unitId, gearLevel: 0, relic: 0)
                }
            }
            
            return gearNeeded
        }

        let count = guild.players.reduce(0, {
            if let unit = $1.units[unlock.target.id] {
                return $0 + (unit.stars >= unlock.unlockStars ? 1 : 0)
            } else {
                return $0
            }
        })
        
        let hasRequiredGear = unlock.requiredGear != nil
        
        result += H.table().id("toons") {
            H.thead {
                H.tr {
                    H.th { "Player" }
                    H.th {
                        unlock.target.commonName
                        "<br/>"
                        H.img(src: unlock.target.imageURL).class("toI")
                    }
                    if hasRequiredGear {
                        H.th { "Gear Needed" }
                    }
                    H.foreach(unlock.requires) { c in
                        H.th {
                            c.commonName
                            H.ifLet(self.unlock.requiredGear?[c.id]) { gear in
                                " \(gear > 20 ? "R" : "G")\(gear > 20 ? gear - 20 : gear)"
                            }
                            "<br/>"
                            H.img(src: c.imageURL).class("toI")
                        }
                    }
                }
            }
            "\n"

            H.foreach(guild.players.sorted()) { player in
                H.tr {
                    H.td {
                        H.a(href: html.p(player, "unlock") + "#" + self.unlock.target.id + "_jump") {
                            player.name
                        }
                    }
                    H.td {
                        H.parent(targetAttributes(player.units[self.unlock.target.id]))
                        unit(player.units[self.unlock.target.id])
                    }
                    if hasRequiredGear {
                        H.td {
                            H.parent(requiredGearAttributes(player))
                            gearLevelsNeeded(player: player)
                        }
                    }
                    H.foreach(self.unlock.requires) { c in
                        H.td {
                            H.parent(requiredAttributes(player.units[c.id]))
                            unit(player.units[c.id])
                        }
                    }
                }
                "\n"
            }
            
            H.tfoot {
                H.tr {
                    H.td { "" }
                    H.td { count }
                    if hasRequiredGear {
                        H.td { "" }
                    }
                    H.foreach(unlock.requires) { _ in
                        H.td { "" }
                    }
                }
            }
            "\n"
        }
        .render()
                
        result += html.footer()
        
        return result
    }

}

struct PlayerFarmingUnlockPage : PageBuilder {
    
    var path: String { return "player/unlock/\(player.code).html" }
    
    let player: Model.Player
    let guild: Model.Guild
    
    func contents(html: HTML) -> String {
        var result = ""
        
        result += html.head
        result += html.backgroundImageCss
        result += html.alternatingRowsCss
        result += html.sortableTableCSS
        result += html.toonCss

        result += html.playerHeader(guild: guild, player:player, title: "\(player.name) Unlock / Farming")
        
        for unlock in FarmingUnlockPage.unlocks {
            result += html.dataTable(id: unlock.target.id)
        }
        
        for unlock in FarmingUnlockPage.unlocks {
            
            func requiredAttributes(_ unit: Model.Unit?) -> [HTMLAttribute] {
                if let unit = unit {
                    var attributes = [HTMLAttribute("data-order", unit.gp.description)]
                    
                    if unit.stars < 7 {
                        attributes.append(HTMLAttribute("style", "background-color: #ff0070"))
                        
                    } else if !unlock.exceedsGearRequirements(unit: unit) {
                        let maximumGearNeeded = unlock.needGearLevels(unitId: unit.id, gearLevel: 0, relic: 0)
                        let gearNeeded = unlock.needGearLevels(unitId: unit.id, gearLevel: unit.gear, relic: unit.relic)
                        let fraction = (maximumGearNeeded - gearNeeded) * 255 / maximumGearNeeded
                        attributes.append(HTMLAttribute("style", "background-color: #ff\(String(fraction, radix: 16))00"))

                    } else if !unlock.exceedsRequiredGP(unit: unit) {
                        attributes.append(HTMLAttribute("style", "background-color: #f0f000"))

                    }
                                    
                    return attributes
                } else {
                    return [HTMLAttribute("style", "background-color: red")]
                }
            }

            func gearLevelAttributes(_ unit: Model.Unit?) -> [HTMLAttribute] {
                if let unit = unit {
                    return [HTMLAttribute("data-order", (unit.gear + unit.relic).description)]
                } else {
                    return []
                }
            }
            
            func gearLevelsNeeded(player: Model.Player) -> Int {
                var gearNeeded = 0
                
                for unitId in unlock.requiredGear!.keys {
                    if let unit = player.units[unitId] {
                        gearNeeded += unlock.needGearLevels(unitId: unitId, gearLevel: unit.gear, relic: unit.relic)
                    } else {
                        gearNeeded += unlock.needGearLevels(unitId: unitId, gearLevel: 0, relic: 0)
                    }
                }
                
                return gearNeeded
            }
            
            let hasRequiredGear = unlock.requiredGear != nil
            
            func icon(_ c: Model.Character) -> HTMLFramgment {
                H.block {
                    H.parent([HTMLAttribute("data-order", c.commonName)])
                    H.img(src: c.imageURL).class("toI")
                    "<br/>"
                    c.commonName
                    H.ifLet(unlock.requiredGear?[c.id]) { gear in
                        " \(gear > 20 ? "R" : "G")\(gear > 20 ? gear - 20 : gear)"
                    }
                }
            }
                        
            result += H.block {
                "<hr/>"
                
                if player.units[unlock.target.id]?.stars ?? 0 >= unlock.unlockStars {
                    H.h2().id(unlock.target.id + "_jump") {
                        H.img(src: unlock.target.imageURL).class("toI")
                        
                        "Unlocked!  "
                        player.units[unlock.target.id]!.description(images: false, zetas: true, gearBelow: 20, starsBelow: 8)
                        " "
                        player.units[unlock.target.id]!.gp.millions
                        " gp"
                    }
                } else {
                    H.h2().id(unlock.target.id + "_jump") {
                        H.img(src: unlock.target.imageURL).class("toI")

                        "Unlock "
                        unlock.target.commonName
                    }
                }
                
                if hasRequiredGear {
                    "Estimated "
                    gearLevelsNeeded(player: player)
                    " gear levels needed."
                    "<br/><br/>"
                }
                
                H.ifLet(unlock.comment) { comment in
                    comment
                    "<br/><br/>"
                }
                
                H.table().id(unlock.target.id).width("400px").style("float: left;") {
                    H.thead {
                        H.tr {
                            H.th { "Character" }
                            H.th { "GP" }
                            H.th { "Stars" }
                            H.th { "Gear" }
                            if hasRequiredGear {
                                H.th { "Gear Needed" }
                            }
                            H.th { "Zetas" }
                        }
                    }
                    "\n"
                    H.foreach(unlock.requires) { c in
                        H.tr {
                            H.parent(requiredAttributes(self.player.units[c.id]))
                            H.td {
                                icon(c)
                            }
                            H.td {
                                H.parent([HTMLAttribute("data-order", (self.player.units[c.id]?.gp ?? 0).description)])
                                self.player.units[c.id]?.gp.millions ?? ""
                            }
                            H.td {
                                self.player.units[c.id]?.stars.description ?? ""
                            }
                            H.td {
                                H.parent(gearLevelAttributes(self.player.units[c.id]))
                                self.player.units[c.id]?.gearLevelDescription ?? ""
                            }
                            if hasRequiredGear {
                                H.td {
                                    unlock.needGearLevels(unitId: c.id, gearLevel: self.player.units[c.id]?.gear ?? 0, relic: self.player.units[c.id]?.relic ?? 0)
                                }
                            }
                            H.td {
                                self.player.units[c.id]?.zetasDescription ?? ""
                            }
                        }
                        "\n"
                    }
                }
            }.render()
        }
                        
        result += html.footer()
        
        return result
    }
}
