//
//  Copyright © 2019 David Koski. All rights reserved.
//

import Foundation
import SWGOH

let t = NSDate.timeIntervalSinceReferenceDate

//var ruleSet = SquadRuleSet(name: "TW.default")
//for squadBuilder in TW.configurations[TW.defaultSquadRuleName]! {
//    for r in squadBuilder.asSquadRules() {
//        ruleSet.add(rule: r)
//    }
//}
//let e = JSONEncoder()
//let data = try! e.encode(ruleSet)
//try! data.write(to: URL(fileURLWithPath: "/tmp/rules.json"))
//exit(0)

//2
func usage(_ argument: String?) -> Never {
    let message = argument == nil ? "" : "Unknown argument: \(argument!)\n\n"
    print(
        """
        \(message)USAGE: swgoh <arguments>
        
        --guild <guild.json>
        
        --character <match>
        
        --allPlayers
        --player <player name,...>
        --skip-player <player name,...>
        --hasUnit <unit id>
        --listUnits
        --listPlayers
        --dumpUnit <unit id or name>
        
        --snapshot <snapshot.json>
        --save-snapshot <snapshot.json>
        
        --tw-config config
        --tw-squads 25

        --filter-tw
        --filter-viable
        --filter-only-nonviable -- list players who have teams that might work but are not viable
        --filter-preferred
        --filter-farm
        --filter-squad-list

        --report-debug
        --report-farm
        --report-farm-csv
        --report-farm-critical
        --report-tw-planning
        --report-tw-teams
        --report-tw-players
        
        --report-tw-simulate <opponent.snapshot.json> <sections>
        
        --jtr
        
        --p2 <type>
        
        --chexmix
        --p3-mix
        --p3-ns
        
        --p4-ns
        --p4-fo
        
        --tw [FO]
        --ga-3v3
        """)
    exit(1)
}

var guildURL: URL?
var charactersURL: URL?

var allPlayers = false

var playerSelector: PlayerSelect = PlayerSelectAll()
var squadBuilder: SquadBuilder = SelectUnitSquadBuilder(properties: Squad.Properties(name: ""), unitSelectors: [])
var squadEvaluator = SquadEvaluator(rules: [:])
var evaluationFilter: Filter = PreferredViabilitySortFilter()
var report: Report = DebugReport()
var twConfig: TWConfig = OptimizedConfig()
var squadsPerSection = 25

func load(_ url:URL) throws -> Model.Guild {
    guard let jsonData = try? Data(contentsOf: url) else {
        fatalError("Unable to open \(url)")
    }
    
    // we are using the swgoh.help (SWGOHHelp) to download the data, but we actually store it in
    // something closer to the swgoh.gg format
    
    return try JSONDecoder().decode(Model.Guild.self, from: jsonData)
}

func evaluatePlayers(players: [Model.Player], squadBuilder: SquadBuilder = squadBuilder, squadEvaluator: SquadEvaluator = squadEvaluator, evaluationFilter: Filter = evaluationFilter, allPlayers: Bool = allPlayers) -> [Model.Player:[SquadEvaluation]] {
    var result = [Model.Player:[SquadEvaluation]]()
    
    let queue = DispatchQueue(label: "build")
    func update(player: Model.Player, evaluations: [SquadEvaluation]) {
        queue.sync {
            result[player] = evaluations
        }
    }
    
    DispatchQueue.concurrentPerform(iterations: players.count) {
        let player = players[$0]
        
        let squads = squadBuilder.generateSquads(units: player)
        
        let evaluations = squadEvaluator.evaluate(squads: squads)
        let finalEvaluations = evaluationFilter.filter(evaluations)
        
        if !finalEvaluations.isEmpty || allPlayers {
            update(player: player, evaluations: finalEvaluations)
        }
    }
    
    return result
}

var prepare = { (players: [Model.Player]) throws -> [Model.Player:[SquadEvaluation]] in
    return evaluatePlayers(players: players)
}

var run = { (guildFile: Model.Guild ) throws in print("no action selected") }

func squadRun() -> (Model.Guild) throws -> Void {
    return { (guildFile: Model.Guild) throws in
        let players = guildFile.players.filter { playerSelector.matches(player: $0) }
        
        let result = try prepare(players)
        
        print(report.report(data: result))
    }
}

var arguments = CommandLine.arguments.dropFirst()
while !arguments.isEmpty {
    let argument = arguments.removeFirst()
    
    switch argument {
    case "-NSDocumentRevisionsDebugMode":
        // we get this when running from xcode (because it is now an "app")
        arguments.removeFirst()
        
    case "--guild":
        guildURL = URL(fileURLWithPath: arguments.removeFirst())
        
    case "--allPlayers":
        allPlayers = true
        
    case "--player":
        let names = arguments.removeFirst().split(separator: ",").map { String($0) }
        playerSelector = PlayersSelectMatch(names)
        
    case "--skip-player":
        let names = arguments.removeFirst().split(separator: ",").map { String($0) }
        playerSelector = PlayersSelectMatch(names, invert: true)
        
    case "--hasUnit":
        playerSelector = PlayerSelectCharacter(arguments.removeFirst())
        
    case "--listUnits":
        run = { guildFile in
            let units = guildFile.allUnits()
            var namedUnits = [String:String]()
            for (id, unit) in units {
                namedUnits[unit.name] = id
            }
            for name in namedUnits.keys.sorted() {
                print("\(namedUnits[name]!) : \(name)")
            }
        }
        
    case "--dumpUnit":
        let match = arguments.removeFirst().lowercased()
        run = { guildFile in
            let units = guildFile.allUnits()
            for unit in units.values {
                if unit.id.lowercased().contains(match) || unit.name.lowercased().contains(match) {
                    print(unit.structure())
                    print("")
                }
            }
        }
                
    case "--unbrand":
        defaultBrand = Unbranded()
                
    case "--brg":
        defaultBrand = BRG()
                
    case "--site":
        let base = URL(fileURLWithPath: arguments.removeFirst())
        
        var pages = [String]()
        if let argument = arguments.first, !argument.starts(with: "-") {
            arguments.removeFirst()
            pages = Array(argument.split(separator: ",").map(String.init))
        }
        
        run = { guildFile in
            var guild = guildFile
            
            guild.players = guild.players.filter { playerSelector.matches(player: $0) }
            
            try Site(guild: guild, pages: pages).build(base: base)
        }
        
    case "--alliance":
        let base = URL(fileURLWithPath: arguments.removeFirst())
        var guildURLs = [URL]()
        while let first = arguments.first, first.hasSuffix(".json") {
            let url = URL.init(fileURLWithPath: arguments.removeFirst())
            guildURLs.append(url)
        }
        
        var pages = [String]()
        if let argument = arguments.first, !argument.starts(with: "-") {
            arguments.removeFirst()
            pages = Array(argument.split(separator: ",").map(String.init))
        }

        // set this to satisfy the argument handling code
        guildURL = guildURLs[0]

        let guilds = Array(DispatchQueue.concurrentPerform(iterations: guildURLs.count) { index -> (Int, Model.Guild) in
            let url = guildURLs[index]
            let guild = try! load(url)
            return (index, guild)
        }.values)
                    
        run = { _ in
            try AllianceSite(guilds: guilds, pages: pages).build(base: base)
        }

    case "--alliance-data":
        let base = URL(fileURLWithPath: arguments.removeFirst())
        
        var urls = [URL]()
        while let first = arguments.first, first.hasSuffix(".json") {
            let url = URL.init(fileURLWithPath: arguments.removeFirst())
            urls.append(url)
        }
        
        guildURL = urls.first!
        
        let guilds = Array(DispatchQueue.concurrentPerform(iterations: urls.count) { index -> (Int, Model.Guild) in
            let url = urls[index]
            let data = try! Data(contentsOf: url)
            return (index, try! JSONDecoder().decode(Model.Guild.self, from: data))
        }.values)

        
        run = { _ in
            try AllianceDataMine(guilds: guilds).build(base: base)
        }
        
    case "--report-debug":
        report = DebugReport()
        run = squadRun()
        
    case "--tw":
        let name = arguments.removeFirst()
        squadBuilder = MultipleSquadBuilder(TW.configurations[name]!)
        
    case "--tw-config":
        let namedConfig = arguments.removeFirst()
        twConfig = twConfigByName[namedConfig, default: { OptimizedConfig() }]()
        
    case "--tw-squads":
        if let count = Int(arguments.removeFirst()) {
            squadsPerSection = count
        } else {
            fatalError("Unable to read --tw-squads")
        }

    case "--report-tw-planning":
        evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
        evaluationFilter = evaluationFilter.compose(SquadListFilter())
        report = TWPlanningReport(config: twConfig, squadsPerSection: squadsPerSection)
        
    case "--report-tw-planning-summary":
        evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
        evaluationFilter = evaluationFilter.compose(SquadListFilter())
        report = TWPlanningSummaryReport(config: twConfig, squadsPerSection: squadsPerSection)
        
    case "--report-tw-teams":
        evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
        evaluationFilter = evaluationFilter.compose(SquadListFilter())
        report = TWPlanningReport(scheduler: TeamNameScheduler(), config: twConfig, squadsPerSection: squadsPerSection)
        
    case "--report-tw-player":
        evaluationFilter = evaluationFilter.compose(ViableSquadFilter())
        evaluationFilter = evaluationFilter.compose(SquadListFilter())
        report = TWPlayerReport(config: twConfig, squadsPerSection: squadsPerSection)
                
    case "--fetch":
        let playerId = Int(arguments.popFirst() ?? "369351434")!
        let shortName = arguments.popFirst() ?? "hope"
        let downloader = Downloader(user: "dkoski", password: "wazzok-7cyrco-zatwYq")
        for _ in 0 ..< 3 {
            do {
                let guild = try downloader.fetchGuild(shortName: shortName, playerId: playerId).await()
                let encoder = JSONEncoder()
                let data = try! encoder.encode(guild)
                try! data.write(to: URL(fileURLWithPath: shortName + ".json"))
                exit(0)
            } catch DownloadError.rateLimit {
                print("rate limit")
                Thread.sleep(forTimeInterval: 180)
            } catch {
                print("Exception during download: \(error)")
            }
        }
        print("repeated failures to download")
        exit(1)

    default:
        usage(argument)
    }
}

guard let guildURL = guildURL else {
    fatalError("Must specify --guild")
}

if allPlayers {
    playerSelector = PlayerSelectAll()
}

do {
    let guildFile = try load(guildURL)
    try run(guildFile)
} catch {
    print(error)
}

// print(NSDate.timeIntervalSinceReferenceDate - t)

