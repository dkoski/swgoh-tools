//
//  main.swift
//  modStats
//
//  Created by David Koski on 12/24/20.
//  Copyright © 2020 David Koski. All rights reserved.
//

import SWGOH

// usage:
// modStats *.json

let urls = Array(CommandLine.arguments.dropFirst())
let guilds = DispatchQueue.concurrentPerform(iterations: urls.count) { index -> (Int, Model.Guild) in
    let url = URL(fileURLWithPath: urls[index])
    let data = try! Data(contentsOf: url)
    return (index, try! JSONDecoder().decode(Model.Guild.self, from: data))
}

let players = guilds.values.flatMap { $0.players }

let url = URL(fileURLWithPath: "/Users/dkoski/personal/swgoh-tools/SWGOH/Reference Data/mods.json")
let seeds = try? JSONDecoder().decode([String : ModRecommendation].self, from: Data(contentsOf: url))

let recommendations = ModRecommendation.build(players: players, seedClusters: seeds, threshold: 0.05)

try! JSONEncoder().encode(recommendations).write(to: url)
