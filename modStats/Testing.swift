//
//  Testing.swift
//  modStats
//
//  Created by David Koski on 1/27/21.
//  Copyright © 2021 David Koski. All rights reserved.
//

import Foundation
import SWGOH

//let id = "GENERALSKYWALKER"
//let cs = 200
//
//var c: ModHistogram!
//for p in players {
//    if p.code == "369351434" {
//        print(p.units[id]!.mods)
//        let mh = ModHistogram(mods: p.units["GENERALSKYWALKER"]!.mods).normalized
//        print(mh)
//        print(mh.priorities())
//        c = mh
//    }
//}
//
//for p in players {
//    if let unit = p.units[id] {
//        if unit.gear <= 10 || unit.mods.count != 6 {
//            continue
//        }
//
//        let mh = ModHistogram(mods: unit.mods).normalized
////        print("\(p.code) \(mh.priorities()) = \(mh.distance(from: c))")
//
//        if mh.distance(from: c) < cs || p.code == "369351434" {
//            if p.code == "369351434" {
//                print("XXXXX \(p.code) \(mh.priorities()) = \(mh.distance(from: c))")
//            } else {
//                print("\(p.code) \(mh.priorities()) = \(mh.distance(from: c))")
//            }
//        }
//    }
//}
//exit(0)

//printPlayers(players: players)

//interchangableModsSinglePlayer2(players: players)

//printClusters(players: players)

//let player = players.first { $0.name == "Gorgatron" }!
//let recommendations = ModRecommendation.build(players: players)

func showPriorities(_ priorities: [ModHistogram.Priority]) -> HTMLFramgment {
    H.block {
        H.foreach(priorities.enumerated()) { (index, priority) in
            if index > 0 {
                ", "
            }
            priority.c.description
            "(\(priority.v))"
        }
    }
}

func showStats(_ stats: [ModHistogram.Value]) -> HTMLFramgment {
    H.block {
        H.foreach(stats.enumerated()) { (index, stat) in
            if index > 0 {
                ", "
            }
            stat.c.description
            "(+ \(stat.v))"
        }
    }
}


let ids = Model.Character.all.sorted().map { $0.id }
let template = H.div() {
    H.foreach(ids) { id in
        if let rec = recommendations[id],
           let c = Model.Character.find(id),
           !c.ship {
            
            H.h2 { c.name }
            
            H.foreach(rec.clusters.enumerated()) { (index, cluster) in
                let percent = Double(cluster.count) / Double(rec.totalCount)
                
                if index == 1 || percent >= 0.1 {
                    H.h3 {
                        showPriorities(cluster.priorities)
                        " ("
                        NumberFormatter.percent.string(from: percent)
                        ")"
                    }
                    
                    showStats(cluster.center.stats)
                }
            }
        }
    }
}.render()

// try! template.write(to: URL(fileURLWithPath: "/tmp/recs.html"), atomically: false, encoding: .utf8)

let player: Model.Player! = nil
let interchange = interchangeableMods(player: player, recommendations: recommendations)
