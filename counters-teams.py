#!/usr/bin/python

# curl https://docs.google.com/spreadsheets/d/1RVo7ej1PE06FKkwS1q5_slB9YLLQX3EF-dN98MkFmOM/export\?exportFormat\=csv\&gid\=0 > counters-teams.csv
#
# python counters-teams.py > SWGOH/Generated/Counters+Teams.swift

import csv

print("// see counters.py")
print("extension CounterTeam {")
print(" static let counterTeams = [")

def q(x):
    if len(x) > 0:
        return '\n"""\n{x}\n"""'.format(x = x)
    else:
        return '""'

with open('counters-teams.csv') as f:
    reader = csv.reader(f)
    
    # skip the header
    next(reader)
    
    for row in reader:
        print('"{id}": CounterTeam(id: "{id}", name: "{name}", type: .{type}, squad: [{squad}], zetas: [{zetas}], subs: #"{subs}"#, description:{description}, strategy:{strategy}),'.format(
                id = row[0],
                name = row[1],
                type = row[2] or "offense",
                squad = ", ".join(map(lambda x: "#\"" + x + "\"#",row[3:8])),
                subs = row[8],
                description = q(row[9]),
                strategy = q(row[10]),
                zetas = ", ".join(map(lambda x: "true" if x == "TRUE" else "false", row[11:16])),
            ))

print(" ]")
print("}")
