# Disclaimer

This is *not* production code.  Have fun!

#  Notes

This is how I pull the guild json file

	curl https://swgoh.gg/api/guild/46657/ > guild.json
	
	curl https://swgoh.gg/api/characters/ > characters.json
	curl https://swgoh.gg/api/ships/ > ships.json
	
	curl https://swgoh.gg/api/abilities/ > abilities.json
	curl https://swgoh.gg/api/gear/ > gear.json

	curl https://swgoh.gg/api/player/369351434/ > gorgatron.json
	
Counters

	curl https://docs.google.com/spreadsheets/d/1RVo7ej1PE06FKkwS1q5_slB9YLLQX3EF-dN98MkFmOM/export\?exportFormat\=csv
	
	curl https://docs.google.com/spreadsheets/d/1RVo7ej1PE06FKkwS1q5_slB9YLLQX3EF-dN98MkFmOM/export\?exportFormat\=csv\&gid\=0

# How It Works

There is a pipeline that runs like this -- each step builds an array of something that the next step consumes:

PlayerSelect [Players] -> SquadBuilder [Squad] -> SquadEvaluator [SquadEvaluation] -> Filter [SquadEvaluation] -> Report

The SquadBuilder can find single units or try units from a list.  You can combine squad builders with MultipleSquadBuilder, e.g. asking for a several different types of squads to be generated.

The SquadEvaluator just applies EvaluationRules to each unit to produce an array of SquadEvaluation values.  These contain the Squad itself, scoring, viability and messages about what changes might need to be made.

The Filter stage does both sorting and filtering and typically uses composition to do its work.  Here are some of the more useful filters:

- PreferredSquadFilter -- the default sort that goes by preference, viabilityScore and scorePercent 
- FarmFilter -- produces a list of farming suggestions:  preferred, viable, preferred non viable, non viable -- it will produce one or two
- SquadListFilter -- removes squads that reuse units from earlier squads.  this works fine with the PreferredSquadFilter, but be aware that order does matter

Finally the Report stage takes a player and an array of evaluations and returns a string.

- AssignmentReport -- produces a CSV with players and squads
- TRReport -- produces a list with one line per squad
- FarmReport -- produces a detailed report showing what to farm
- CriticalFarmReport -- similar to the FarmReport but only reports *critical* issues (.needsRequiredGear or worse)

# Cookbook

--filter-viable --filter-squad-list --tw jedi
	The TW report for jedi (offense and defense) filtering only the viable ones and then by non-reused units in the squad list.
	
--filter-only-nonviable --filter-farm --report-farm-critical --jtr
	Show players who qualify for the team but are missing a critical piece (a missing zeta or toon that makes it work)
	
--report-assign --filter-viable --filter-squad-list --chexmix
	Show an assignment report with the top viable team
	
# Recipes

--guild $(SRCROOT)/guild.json --tw all --report-tw-teams --save-snapshot $(SRCROOT)/snapshot.json
	Produce a snapshot.json with all the offense and defense teams for the "all" TW
	
--guild $(SRCROOT)/guild.json --snapshot $(SRCROOT)/snapshot.json --report-tw-planning
	Using the previously produced snapshot, do the planning report for TW
	
python swgoh.help.py 156549916
	Dump an opponent guild from swgoh.help -- the number is an id from a player in that guild
	
--guild $(SRCROOT)/guild.json --swgoh.help $(SRCROOT)/opponent-guild.json $(SRCROOT)/opponent-units.json 134 --tw all --report-tw-teams --save-snapshot $(SRCROOT)/snapshot-opponent.json
	Produce a snapshot from the enemy team, capping it to 134m gp
	
--guild $(SRCROOT)/guild.json --snapshot $(SRCROOT)/snapshot.json --report-tw-planning --report-tw-simulate $(SRCROOT)/snapshot-opponent.json T1,T2,T3,T4,B1,B2,B3,B4
	Run a TW simulation
	
--guild $(SRCROOT)/guild.json --snapshot $(SRCROOT)/snapshot.json --tw-ga $(SRCROOT)/snapshot-opponent.json $(SRCROOT)/ga.json 
	Run the GA to produce orders.

# TODO

- consider something like this: https://guill118.github.io/squads/

- relic gp: $inc = [ 0, 759, 1594, 2505, 3492, 4554, 6072, 7969 ];

- need players endpoint for the relic numbers

- missing common units (find units that most people have at good gear)

- better tw display - gear, gp, z

- updated tw squad builder

- table mode for suggestions

- ga teams, ga counters, suggest teams given an opponent

- alternate JTR squads?

- webhook integration

Command Line

- redo the command line system -- too many options

- switch from units query to players: get relic tiers, arena and fleet settings

TW

- do not consider TW units with < 6000 gp
- simpler filtering system for tw -- we don't need the fulll grading system that I built for HSTR P1

TW GA

- how many of each type of squad, e.g. 3 ewoks, 10 BH
- where each squad will be placed in order of power, e.g. T3, T3, T1, B1

- farming / squad / counter report.  can I show missing zetas?  even if required?

- tuning for JTR
- formatted counters list (guild)
- has counters-in-common-with

- farming priority

- squads list
- custom counters
- custom reverse counters

- more P3 options NS + JKA

- i can get equipped gear from swgoh.help
- and from the characters data I can get all the gear/stats from levels equipped
- could compute speed, etc.
- same for mods
- https://glitch.com/edit/#!/crinolo-swgoh?path=statCalc/statCalculator.js:1:0

- geo tb map -- https://cdn.discordapp.com/attachments/511328953764675586/617098792856191005/image0.jpg
- phase 3 middle: Traya L, palp, vader, DN and DS

- missing zetas page
- mine zeta info from the guild, figure out recommendations

- mods page
- mine mod info from the guild
- find unusual configurations (mismatched sets, things that don't conform to other squads)

- low hanging fruit farming: padme team that just needs padme geared
- GBA

- mine toon info: popular toons, well geared, have zetas

# From Branmark

I usually put this on defence
DR with malak
Padme with GR and Anakin
Bossk with BH
Embo with BH
Ewoks
FO
Carth with wampa and old republic
Traya trio with sith
NS with nest


Leaves revan, cls, JTR, empire as top tier offence

