# -*- coding: utf-8 -*-
"""
Created on Tue Sep  4 20:49:07 2018

https://github.com/platzman/swgoh.help.python

@author: platzman
"""

import requests
import sys
from json import loads, dumps

class SWGOHhelp():
    def __init__(self, settings):
        self.user = "username="+settings.username     
        self.user += "&password="+settings.password
        self.user += "&grant_type=password"
        self.user += "&client_id="+settings.client_id
        self.user += "&client_secret="+settings.client_secret
    	    	
        self.token = str()
        
        self.urlBase = 'https://api.swgoh.help'
        self.signin = '/auth/signin'
        self.data_type = {'guild':'/swgoh/guild/',
                          'player':'/swgoh/player/',
                          'data':'/swgoh/data/',
						  'units':'/swgoh/units',
						  'roster':'/swgoh/roster',
                          'battles':'/swgoh/battles',
                          'zetas':'/swgoh/zetas'}

        
    def get_token(self):
        sign_url = self.urlBase+self.signin
        payload = self.user
        head = {"Content-type": "application/x-www-form-urlencoded",
                'Content-Length': str(len(payload))}
        r = requests.request('POST',sign_url, headers=head, data=payload, timeout = 10)
        if r.status_code != 200:
            error = 'Cannot login with these credentials'
            return  {"status_code" : r.status_code,
                     "message": error}
        _tok = loads(r.content.decode('utf-8'))['access_token']
        self.token = { 'Authorization':"Bearer "+_tok} 
        return(self.token)

    def get_data(self, data_type, spec):
        token = self.get_token()
        head = {'Method': 'POST','Content-Type': 'application/json','Authorization': token['Authorization']}
        if data_type == 'data':
            payload = {'collection': str(spec)}
        else:
            payload = {'allycode': spec}
        data_url = self.urlBase+self.data_type[data_type]
        try:
            r = requests.request('POST',data_url, headers=head, data = dumps(payload))
            if r.status_code != 200:
                error = 'Cannot fetch data - error code'
                data = {"status_code" : r.status_code,
                         "message": error}
            data = loads(r.content.decode('utf-8'))
        except:
            data = {"message": 'Cannot fetch data'}
        return data

class settings():
    def __init__(self, _username, _password, _client_id, _client_secret):
        self.username = _username
        self.password = _password
        self.client_id = _client_id
        self.client_secret = _client_secret


# USAGE: python zetas.py > swgoh-tool/Generated/ZetaPriorities.swift

creds = settings('dkoski','wazzok-7cyrco-zatwYq','123','abc')
client = SWGOHhelp(creds)

data = client.get_data('zetas','369351434')

# print(dumps(data, indent=4))

print("// see zetas.py")
print("extension ZetaPriority {")
print("static let data = [")

for zeta in data['zetas']:
    print(u"""
            ZetaPriority(character: #"{toon}"#, type: "{type}", name: "{name}", priorities: [{sith}, {pvp}, {versa}, {tw}, {tb}]),
        """.format(
            toon = zeta["toon"],
            type = zeta["type"],
            name = zeta["name"],
            
            sith = zeta["sith"],
            pvp = zeta["pvp"],
            versa = zeta["versa"],
            tw = zeta["tw"],
            tb = zeta["tb"],
        ).encode("utf-8"))

print "]"
print "}"

print("extension Model.Ability {")
print("static let zetaDescriptions = [")

for detail in data['details']:
    print(u"""
            "{name}" : #"{description}"#,
        """.format(
            name = detail["Name"],
            description = detail["Description"],
        ).encode("utf-8"))


print "]"
print "}"

